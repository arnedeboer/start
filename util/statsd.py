import socket
import os

STATSD_IP = os.environ.get('STATSD_IP', '0.0.0.0')
STATSD_PORT = os.environ.get('STATSD_PORT', 8125)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.bind((STATSD_IP, STATSD_PORT))

while True:
    data, addr = sock.recvfrom(1024)
    decoded = data.decode('utf-8')
    print(f"Received from {addr[0]}:{addr[1]}: {decoded}")
