#!/bin/python3
import gitlab
import os
import time
from datetime import datetime
from typing import Final

project_id: Final[str] = os.getenv("CI_PROJECT_ID")
pipeline_id: Final[str] = os.getenv("CI_PIPELINE_ID")
ref: Final[str] = os.getenv("CI_COMMIT_REF_NAME")
private_token: Final[str] = os.getenv("ACCESS_TOKEN_TO_CHECK_PIPELINE")


def _print(*arg, **kwarg):
    print(datetime.now().strftime("%H:%M:%S.%f")[:-3], *arg, **kwarg)


def _is_another_pipeline_running():
    gl = gitlab.Gitlab("https://gitlab.com", private_token=private_token)
    project = gl.projects.get(project_id)

    pipelines = project.pipelines.list(page=1, per_page=50)

    # Reverse the list and check from old to new
    for pipeline in reversed(pipelines):
        if pipeline.attributes["status"] in [
            "success",
            "failed",
            "canceled",
            "skipped",
        ]:
            # Pipeline is not running continue checking other pipelines
            continue

        if int(pipeline.id) == int(pipeline_id):
            _print(
                "There is no older pipeline running, "
                "so finish this job and continue running"
            )
            return False

        _print(
            "Checking if the pipeline should wait on "
            f"pipeline {pipeline.id} with status {pipeline.attributes['status']} "
            f"on branch {pipeline.ref}"
        )

        if pipeline.ref == ref:
            _print(
                f"Should wait on pipeline with id {pipeline.id} and "
                f"status {pipeline.attributes['status']} on branch {pipeline.ref}"
            )
            return True

    _print(f"No other pipeline is running on branch {ref}")
    return False


_print(f"Check if another pipeline is running for project {project_id} on branch {ref}")
_print(f"Current pipeline id: {pipeline_id}")

while _is_another_pipeline_running():
    _print("Another pipeline is running on the same branch, waiting one minute")
    time.sleep(60)
