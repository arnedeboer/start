#!/bin/bash

bold="\x1B[1m"
reset="\x1B[0m"


## The basics:

DEVMODE_USAGE="

    # devmode: Development mode settings
    devmode show # Show the current development modes for frontend and backend

    devmode init # Rewrite devmode configuration

    devmode frontend {type} # Set frontend development style configuration
        [local|container|image]

    devmode backend {type} # Set backend development style configuration
        [container|image]

    devmode helpers {type} # Set helpers on or off
        [on|off]

    devmode auth {type} # Set status of Keycloak container for debugging
                        # authentication and authorization
        [on|off]

    devmode wopi {type} # Set status of WOPI proxy container port forwarding
        [on|off]
"
CMD_USAGE="$CMD_USAGE $DEVMODE_USAGE"

function devmode_usage {
    echo "$BASE_USAGE $DEVMODE_USAGE"
}

function devmode_run {
    case "$CHOSENACTION" in
        show)
            show_devmode
            exit
            ;;

        init)
            helper_write_env
            exit
            ;;

        frontend)
            set_frontend_mode
            ;;

        backend)
            set_backend_mode
            ;;

        helpers)
            set_helpers_mode
            ;;

        auth)
            set_auth_mode
            ;;

        wopi)
            set_wopi_mode
            ;;

        *)
            devmode_usage
            exit 1
            ;;
    esac

    helper_write_env
}

function set_frontend_mode {
    cd "$REPOROOT" || exit 1

    local mode="${CMD_ARGUMENTS[0]}"
    case "$mode" in
        local|container|image)
            set_zsconfig_value development_mode.frontend $mode
            ;;
        *)
            devmode_usage
            ;;
    esac

    [ "$mode" = "local" ] && myrepos_install_frontend_dependencies
}

function set_backend_mode {
    cd "$REPOROOT" || exit 1

    local mode="${CMD_ARGUMENTS[0]}"
    case "$mode" in
        container|image)
            set_zsconfig_value development_mode.backend $mode
            ;;
        *)
            devmode_usage
            ;;
    esac

    [ "$mode" = "container" ] && myrepos_install_backend_dependencies
}

function set_helpers_mode {
    cd "$REPOROOT" || exit 1

    local mode="${CMD_ARGUMENTS[0]}"
    case "$mode" in
        on|off)
            set_zsconfig_value development_mode.helpers $mode
            ;;

        *)
            devmode_usage
            ;;
    esac
}

function set_auth_mode {
    cd "$REPOROOT" || exit 1

    local mode="${CMD_ARGUMENTS[0]}"
    case "$mode" in
        on|off)
            set_zsconfig_value development_mode.auth $mode
            ;;

        *)
            devmode_usage
            ;;
    esac
}

function set_wopi_mode {
    cd "$REPOROOT" || exit 1

    local mode="${CMD_ARGUMENTS[0]}"
    case "$mode" in
        on|off)
            set_zsconfig_value development_mode.wopi $mode
            ;;

        *)
            devmode_usage
            ;;
    esac
}


function show_devmode {
  [ ! -e "$ZSCFG" ] && helper_write_env

  BE_MODE=$(get_zsconfig_value development_mode.backend)
  FE_MODE=$(get_zsconfig_value development_mode.frontend)
  HELPERS_MODE=$(get_zsconfig_value development_mode.helpers)
  AUTH_MODE=$(get_zsconfig_value development_mode.auth)
  WOPI_MODE=$(get_zsconfig_value development_mode.wopi)

  echo -e "Current ${bold}backend${reset} development mode: ${bold}$BE_MODE${reset}"
  echo -e "Current ${bold}frontend${reset} development mode: ${bold}$FE_MODE${reset}"
  echo -e "Current perl ${bold}helpers${reset} development mode: ${bold}$HELPERS_MODE${reset}"
  echo -e "Current ${bold}authentication${reset} development mode: ${bold}$AUTH_MODE${reset}"
  echo -e "Current ${bold}WOPI${reset} development mode: ${bold}$WOPI_MODE${reset}"
}
