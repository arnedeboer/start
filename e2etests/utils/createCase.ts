import { expect, Page } from '@playwright/test';
import { locators } from './locators';

const applicantNameToClick = '14-05-1956';

export const createCase = async ({
  page,
  caseType = 'Basiszaaktype dun',
  applicantType = 'Persoon',
  applicantName = 'Gerard Kuipers 14-05-1956',
  channel = 'email',
}: {
  page: Page;
  caseType?: string;
  applicantType?: string;
  applicantName?: string;
  channel?: string;
}) => {
  await page.locator('[aria-label="Groene plus knop menu openen"]').click();
  try {
    await expect(
      page.getByRole('menuitem', { name: 'Zaak aanmaken' })
    ).toBeVisible();
  } catch {
    await page.locator('[aria-label="Groene plus knop menu openen"]').click();
  }
  await page.getByRole('menuitem', { name: 'Zaak aanmaken' }).click();
  await page.getByLabel('Zaaktype*').fill(caseType);
  await page.locator(locators.resultFound(caseType)).click();
  await page.getByLabel(applicantType, { exact: true }).click();
  await page.getByLabel(`Aanvrager (${applicantType.toLowerCase()})*`).click();
  await page
    .getByLabel(`Aanvrager (${applicantType.toLowerCase()})* `)
    .type(applicantName);
  await page.waitForResponse(res => res.status() === 200);
  await page.locator(locators.resultFound(applicantNameToClick)).click();
  await page
    .getByRole('combobox', { name: 'ContactkanaalDit veld is verplicht' })
    .selectOption(`string:${channel}`);
  try {
    await expect(page.locator('option[selected="selected"]')).toHaveValue(
      `string:${channel}`
    );
  } catch {
    await page
      .getByRole('combobox', { name: 'ContactkanaalDit veld is verplicht' })
      .selectOption(`string:${channel}`);
  }
  await page.getByText('Volgende').click();
  await page.waitForLoadState('domcontentloaded');
  // eslint-disable-next-line playwright/no-networkidle
  await page.waitForLoadState('networkidle');
};
