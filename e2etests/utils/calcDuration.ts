import { intervalToDuration } from 'date-fns';

interface Props {
  miliseconds: number;
}

export const calcDuration = ({ miliseconds }: Props) => {
  const milisecondsLeft = miliseconds - Math.floor(miliseconds / 1000) * 1000;
  const duration = intervalToDuration({ start: 0, end: miliseconds });
  const durationFormatted = `${duration.minutes ?? 0}:${zeroPad(
    duration.seconds
  )}.${milisecondsLeft.toString()}`;

  return durationFormatted;
};

function zeroPad(number: number | undefined) {
  return String(number).padStart(2, '0');
}
