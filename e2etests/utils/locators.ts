export const locators = {
  persoonLink: (textToReplace: string) =>
    `//td[.//text()="${textToReplace}"]//parent::tr//a[.//text()="Persoon"]`,
  text: (text: string) => `text=${text}`,
  labelLeftRichTextFatt: (testToReplace: string) =>
    `//label[contains(.//text(),"${testToReplace}")]/ancestor::vorm-field//div/p`,
  labelLeftTextInContact: (textToReplace: string) =>
    `(//div[contains(.//text(),"${textToReplace}")]/following::div//div)[1]`,
  labelLeftOnForm: (textToReplace: string, intern = false) =>
    `//label[.//text()="${textToReplace}"]/parent::div/parent::${
      intern ? 'vorm-field' : 'div'
    }/div[2]`,
  labelLeftOnPersonFormOverview: (
    textToReplace: string,
    numberOfOccurence: string
  ) =>
    `(//*[contains(.//text(),"${textToReplace}")]/parent::div/parent::div/parent::div//div[2]/div)[${numberOfOccurence}]`,
  labelLeftOnOverview: (textToReplace: string) =>
    `//*[contains(.//text(), "${textToReplace}")]/ancestor::li//ul//span`,
  resultFound: (textToReplace: string) =>
    `(//*[contains(@class,"suggestion") and contains(.//text(),"${textToReplace}")])[1]`,
  checkNothingSelected: (textToReplace: string) =>
    `//*[contains(.//text(),'${textToReplace}')]//ancestor::li//ul[contains(@class,'empty')]`,
  componentJson: (text: string) =>
    `//div[@class="ace_gutter-cell" and .//text()="${text}"]`,
  inputLabelLeftLocator: (textToReplace: string) =>
    `(//*[.//text()="${textToReplace}"]//parent::vorm-field/descendant::input)[1]`,
  partialTextLocator: (textToReplace: string) =>
    `(//*[contains(.//text(),"${textToReplace}")])[1]`,
  partialTextLocatorByNumber: (
    textToReplace: string,
    numberOfOccurence: string
  ) => `(//*[contains(.//text(),"${textToReplace}")])[${numberOfOccurence}]`,
  inputLabelAboveLocator: (textToReplace: string) =>
    `(//*[contains(.//text(),"${textToReplace}")]/following::input)[1]`,
  linkLocator: (textToReplace: string) => `//a[.//text()="${textToReplace}"]`,
  inputVormFieldLocator: (textToReplace: string) =>
    `//vorm-field[.//text()="${textToReplace}"]//input`,
  iconByDataTestid: (textToReplace: string, numberOfOccurence: string) =>
    `(//*[@data-testid="${textToReplace}"]/ancestor::a)[${numberOfOccurence}]`,
  fieldByAriaLabel: (textToReplace: string, numberOfOccurence: string) =>
    `(//*[@aria-label="${textToReplace}"])[${numberOfOccurence}]`,
  dropDownSelection: (textToReplace: string) =>
    `//label[contains(.//text(),"${textToReplace}")]//parent::div//div[@class="form-control ui fluid selection dropdown"]`,
  checkbox: (textToReplace: string) =>
    `//*[contains(.//text(),"${textToReplace}")]//parent::label/input`,
};
