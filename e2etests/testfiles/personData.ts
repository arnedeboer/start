export interface Person {
  BSN: string;
  firstNames: string;
  insertions: string;
  familyName: string;
  nobleTitle: string;
  birthDate: string;
  gender: string;
  residenceCountry: string;
  residenceStreet: string;
  residenceZipcode: string;
  residenceHouseNumber: string;
  residenceHouseNumberLetter: string;
  residenceHouseNumberSuffix: string;
  residenceCity: string;
  foreignAddress1: string;
  foreignAddress2: string;
  foreignAddress3: string;
  addressInCommunity: string;
  correspondenceStreet: string;
  correspondenceZipcode: string;
  correspondenceHouseNumber: string;
  correspondenceHouseLletter: string;
  correspondenceHouseNumberSuffix: string;
  correspondenceCity: string;
  correspondenceCountry: string;
  correspondenceForeignAddress1: string;
  correspondenceForeignAddress2: string;
  correspondenceForeignAddress3: string;
  phoneNumber: string;
  mobileNumber: string;
  email: string;
  preferenceChannel: string;
  internalNote: string;
}

interface PersonData {
  [environment: string]: {
    [BSN: string]: Person;
  };
}
const personData: PersonData = {
  development: {
    '010082426': {
      BSN: '010082426',
      firstNames: 'Gerard',
      insertions: '',
      familyName: 'Kuipers',
      nobleTitle: 'Bardon',
      birthDate: '14-05-1956',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Vaewribzxuaoqevti',
      residenceZipcode: '1721KB',
      residenceHouseNumber: '88',
      residenceHouseNumberLetter: 'Y',
      residenceHouseNumberSuffix: 'IB',
      residenceCity: 'Testgemeente',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLletter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      correspondenceCountry: '',
      correspondenceForeignAddress1: '',
      correspondenceForeignAddress2: '',
      correspondenceForeignAddress3: '',
      phoneNumber: '0512345667',
      mobileNumber: '0699887700',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'email',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '547608238': {
      BSN: '547608238',
      firstNames: 'Jan',
      insertions: '',
      familyName: 'Janssen',
      nobleTitle: 'Bardon',
      birthDate: '10-05-1954',
      gender: 'Man',
      residenceCountry: '',
      residenceStreet: 'Ozyweyvbydptzskit',
      residenceZipcode: '',
      residenceHouseNumber: '19',
      residenceHouseNumberLetter: 'S',
      residenceHouseNumberSuffix: 'PC',
      residenceCity: 'Testgemeente',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      correspondenceStreet: 'Ozyweyvbydptzskit',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '19',
      correspondenceHouseLletter: 'S',
      correspondenceHouseNumberSuffix: 'PC',
      correspondenceCity: 'Testgemeente',
      correspondenceCountry: 'Nederland',
      correspondenceForeignAddress1: '',
      correspondenceForeignAddress2: '',
      correspondenceForeignAddress3: '',
      phoneNumber: '0301234567',
      mobileNumber: '0687654321',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'webformulier',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
    '132915947': {
      BSN: '132915947',
      firstNames: 'Chris',
      insertions: '',
      familyName: 'Brouwer',
      nobleTitle: 'Bardon',
      birthDate: '12-05-1922',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Cgukahfjlxclhlkck',
      residenceZipcode: '1020JZ',
      residenceHouseNumber: '40',
      residenceHouseNumberLetter: 'S',
      residenceHouseNumberSuffix: 'FI',
      residenceCity: 'Testgemeente',
      foreignAddress1: 'Rue de Gozée 706',
      foreignAddress2: '6110 Montigny-le-Tilleul',
      foreignAddress3: 'België',
      addressInCommunity: 'true',
      correspondenceStreet: 'Tmrxpypymjuxlivxw',
      correspondenceZipcode: '76',
      correspondenceHouseNumber: 'X',
      correspondenceHouseLletter: 'GZ',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: 'Testgemeente',
      correspondenceCountry: 'Nederland',
      correspondenceForeignAddress1: '',
      correspondenceForeignAddress2: '',
      correspondenceForeignAddress3: '',
      phoneNumber: '06123456789',
      mobileNumber: '',
      email: 'test@xxllnc.nl',
      preferenceChannel: 'telefoon',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
  },
  preprod: {
    '010082426': {
      BSN: '010082426',
      firstNames: 'Delajah',
      insertions: '',
      familyName: 'Vos',
      nobleTitle: 'Bardon',
      birthDate: '10-05-1972',
      gender: 'Man',
      residenceCountry: 'Nederland',
      residenceStreet: 'Tvpprcjdcaisglgoo',
      residenceZipcode: '1476WT',
      residenceHouseNumber: '93',
      residenceHouseNumberLetter: 'G',
      residenceHouseNumberSuffix: 'KG',
      residenceCity: 'Sint-Michielsgestel',
      foreignAddress1: '',
      foreignAddress2: '',
      foreignAddress3: '',
      addressInCommunity: 'true',
      correspondenceStreet: '',
      correspondenceZipcode: '',
      correspondenceHouseNumber: '',
      correspondenceHouseLletter: '',
      correspondenceHouseNumberSuffix: '',
      correspondenceCity: '',
      correspondenceCountry: '',
      correspondenceForeignAddress1: '',
      correspondenceForeignAddress2: '',
      correspondenceForeignAddress3: '',
      phoneNumber: '0512345667',
      mobileNumber: '0699887700',
      email: 'burgerdiakriet@xxllnc.nl',
      preferenceChannel: 'email',
      internalNote:
        'DO NOT USE OR CHANGE!!! This person is used in the automated e2e test.',
    },
  },
};

export default personData;
