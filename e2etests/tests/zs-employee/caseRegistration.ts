/* eslint-disable playwright/no-conditional-in-test */
import { Page, expect } from '@playwright/test';
import { locators } from '../../utils/locators';
import { Attribute, testFiles } from './testrondeData';

export const fillAttribute = (page: Page) => async (attribute: Attribute) => {
  await page
    .getByText(`${attribute.publicName}`)
    .first()
    .scrollIntoViewIfNeeded();

  attribute.fillValue !== undefined
    ? await page.getByLabel(`${attribute.publicName}`).fill(attribute.fillValue)
    : attribute.docCategory !== undefined
    ? await uploadDocument({ page, attribute })
    : attribute.checkValue !== undefined
    ? await page.getByLabel(`${attribute.checkValue}`).check()
    : attribute.addressValue !== undefined
    ? await selectAddress({ page, attribute })
    : attribute.selectValue !== undefined
    ? await page
        .getByRole('combobox', { name: `${attribute.publicName}` })
        .selectOption(`string:${attribute.selectValue}`)
    : '';

  attribute.checkValue2 !== undefined
    ? await page.getByLabel(`${attribute.checkValue2}`).click()
    : '';

  attribute.addressValue2 !== undefined
    ? await selectAddress({ page, attribute, fieldNumber: 2 })
    : '';
};

const locatorPlaceholder = 'Zoek op adres (bv. Amsterdam, Ellermanstraat 23)';

const selectAddress = async ({
  page,
  attribute,
  fieldNumber = 1,
}: {
  page: Page;
  attribute: Attribute;
  fieldNumber?: number;
}) => {
  const addressLocator =
    attribute.inputType === 'Adres (dmv postcode)' ||
    attribute.inputType === 'Adressen (dmv postcode)' ||
    attribute.inputType === 'Adres (dmv straatnaam)' ||
    attribute.inputType === 'Adressen (dmv straatnaam)' ||
    attribute.inputType === 'Straten' ||
    attribute.inputType === 'Straat'
      ? page.getByLabel(`${attribute.publicName}*`).first()
      : attribute.inputType === 'Locatie met kaart' ||
        attribute.inputType === 'Adres (Google Maps)'
      ? page.locator(`//vorm-field[.//text()="${attribute.publicName}"]//input`)
      : attribute.inputType === 'Adres V2'
      ? page.locator('zs-address').getByPlaceholder(`${locatorPlaceholder}`)
      : page.getByLabel(`${attribute.publicName}*`).first();

  await addressLocator.click();
  const addressToFill =
    fieldNumber === 1 ? attribute.addressValue : attribute.addressValue2;
  await addressLocator.type(
    addressToFill !== undefined ? addressToFill.replace('\\', '') : '3311BV 60'
  );
  // eslint-disable-next-line playwright/no-networkidle
  await page.waitForLoadState('networkidle');

  const addressLocatorSelect =
    attribute.inputType === 'Adres V2' ||
    attribute.inputType === 'Locatie met kaart' ||
    attribute.inputType === 'Adres (Google Maps)'
      ? addressLocator
      : page.getByRole('textbox', { name: `${attribute.publicName}` });
  const addressToSelectUncorrected =
    fieldNumber === 1 ? attribute.overviewValue : attribute.overviewValue2;
  const addressToSelect = addressToSelectUncorrected?.endsWith(' + −')
    ? addressToSelectUncorrected.substring(
        0,
        addressToSelectUncorrected.length - 4
      )
    : addressToSelectUncorrected;

  await expect(
    page.getByText(
      addressToSelect !== undefined
        ? addressToSelect.replace('\\', '')
        : "Bunninkh'plein 60, 3311BV Dordrecht"
    )
  ).toBeVisible();
  await addressLocatorSelect.press('ArrowDown', { delay: 50 });
  await Promise.all([
    page.waitForRequest(new RegExp('api.pdok.nl')),
    page.waitForResponse(res => res.status() === 200),
    addressLocatorSelect.press('Enter'),
  ]);
  await page.waitForLoadState('domcontentloaded');
  // eslint-disable-next-line playwright/no-networkidle
  await page.waitForLoadState('networkidle');
  // eslint-disable-next-line playwright/no-wait-for-timeout
  await page.waitForTimeout(300);
};

const testFileDir = './testfiles/';

const uploadDocument = async ({
  page,
  attribute,
}: {
  page: Page;
  attribute: Attribute;
}) => {
  const fileChooserPromise = page.waitForEvent('filechooser');
  await page
    .getByRole('listitem')
    .filter({
      hasText: `${attribute.publicName}* Bestand toevoegen Of sleep je bestanden hierheen Veld t`,
    })
    .getByRole('button', { name: 'Bestand toevoegen' })
    .click();
  const fileChooser = await fileChooserPromise;
  await fileChooser.setFiles(
    testFileDir +
      `${attribute.testDoc !== undefined ? attribute.testDoc : testFiles[1]}`
  );
  await expect(
    page.getByText(
      `${attribute.testDoc !== undefined ? attribute.testDoc : testFiles[1]}`
    )
  ).toBeVisible();
  // eslint-disable-next-line playwright/no-networkidle
  await page.waitForLoadState('networkidle');
};

const getInputValue = async ({
  page,
  attribute,
}: {
  page: Page;
  attribute: Attribute;
}) => {
  if (attribute.inputType === 'Afbeelding') {
    return await page.getByLabel(`${attribute.publicName}*`).inputValue();
  } else if (attribute.inputType === 'Rich text') {
    return await page.locator('//*[@class="ql-editor"]').innerText();
  } else if (attribute.inputType === 'Enkelvoudige keuze') {
    return await page
      .locator(
        `//*[.//text()="${attribute.publicName}"]/parent::li//input[@aria-checked="true"]/parent::label/span`
      )
      .inputValue();
  } else if (attribute.inputType === 'Groot tekstveld') {
    return await page.locator('textarea').inputValue();
  } else {
    return await page.getByLabel(`${attribute.publicName}`).inputValue();
  }
};

export const expectAttributeValueOnForm =
  (page: Page, rulesTriggered = false) =>
  async (attribute: Attribute) => {
    const readOnlyField =
      rulesTriggered && attribute.becomesReadonlyAfterRuleTrigger;

    const locator = readOnlyField
      ? page
          .locator('span.delegate-value-display')
          .filter({ hasText: attribute.expectedFormValueAfterRuleTrigger })
      : page.getByText(`${attribute.publicName}`);
    await locator.scrollIntoViewIfNeeded();
    const inputValue = readOnlyField
      ? (await locator.isVisible())
        ? attribute.expectedFormValueAfterRuleTrigger || ''
        : ''
      : await getInputValue({ attribute, page });

    expect(inputValue.replace(/\s+/g, ' ').replace('string:', '')).toEqual(
      `${
        rulesTriggered
          ? attribute.expectedFormValueAfterRuleTrigger ||
            attribute.valueDefault
          : attribute.valueDefault
      }`
    );
  };

export const expectAttributeValueOnOverview =
  (page: Page, rulesTriggered = false) =>
  async (attribute: Attribute) => {
    const overviewExtraText = '';
    await page
      .getByText(`${attribute.publicName}`, { exact: true })
      .scrollIntoViewIfNeeded();

    const valueFound = await page
      .locator(locators.labelLeftOnForm(`${attribute.publicName}`, true))
      .innerText();
    // eslint-disable-next-line playwright/no-conditional-in-test
    const overviewValue2 =
      attribute.overviewValue2 !== undefined
        ? ' ' + attribute.overviewValue2
        : '';
    const ovvValue =
      // eslint-disable-next-line playwright/no-conditional-in-test
      attribute.overviewValue !== undefined
        ? `${attribute.overviewValue}${overviewValue2}${overviewExtraText}`
        : 'no overviewValue!';
    const value = rulesTriggered
      ? attribute.expectedOverviewValueAfterRuleTrigger || ovvValue
      : ovvValue;
    expect(valueFound.replace(/\s+/g, ' ').replace('string:', '')).toEqual(
      `${value}`
    );
  };
