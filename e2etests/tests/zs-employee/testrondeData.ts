/* eslint-disable camelcase */
import { format } from 'date-fns';

export const testrondeObjectTypeV2 = 'Auto TESTRONDE Objecttype V2';
export const defaultTextChar500 = new Array(51).join('Char 500. ');
export const defaultTextChar500Overview =
  defaultTextChar500.substring(0, 494) + '... MEER';
export const testFiles = {
  1: "'t Testdocument éüð®þüøµñ©ðß[(Word)].docx",
  2: "'t Testdocument éüð®þüøµñ©ðß[ODT].odt",
  3: "'t Testdocument éüð®þüøµñ©ðß [PDF].pdf",
};

const dateToFill = format(new Date(), 'yyyy-MM-dd');
const dateOnOverview =
  dateToFill.substring(8) +
  '-' +
  dateToFill.substring(5, 8) +
  dateToFill.substring(0, 4);
const year = dateToFill.substring(0, 4);

export type Attribute = {
  nameAttribute: string;
  magicString: string;
  publicName: string;
  title?: string;
  mandatory?: boolean;
  explanationInternal: string;
  sensitiveData?: boolean;
  inputType: string;
  systemAttribute?: true;
  inObject?: true;
  valueDefault?: string;
  fillValue?: string;
  docCategory?: string;
  testDoc?: string;
  checkValue?: string;
  addressValue?: string;
  selectValue?: string;
  checkValue2?: string;
  addressValue2?: string;
  overviewValue?: string;
  overviewValue2?: string;
  afterChoiceValueFixed?: boolean;
  expectedFormValueAfterRuleTrigger?: string;
  becomesReadonlyAfterRuleTrigger: boolean;
  expectedOverviewValueAfterRuleTrigger?: string;
  multipleValuesAllowed?: boolean;
  explanationExternal?: string;
  objectType?: any;
  relationType?: any;
  onPages: string[];
  choices?: string[];
  docDirection?: string;
};

export const testrondeData: Record<string, Attribute> = {
  auto_tst_o0001_object_naam: {
    nameAttribute: 'auto_tst_o0001_object_naam',
    magicString: 'auto_tst_o0001_object_naam',
    publicName: 'auto_tst_o0001_object_naam',
    title: 'Object naamdeel',
    explanationInternal: 'Geef hier een tekst die het object uniek maakt',
    sensitiveData: false,
    inputType: 'Tekstveld',
    explanationExternal: 'Dit is het unieke deel van de object naam',
    systemAttribute: true,
    inObject: true,
    valueDefault: `${format(new Date(), 'HH:mm:ss.SSS')}`,
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_k0000_datum_test: {
    nameAttribute: 'auto_tst_k0000_datum_test',
    magicString: 'auto_tst_k0000_datum_test',
    publicName: 'auto_tst_k0000_datum_test',
    title: 'Datum test',
    explanationInternal: 'Dit is de datum van de test',
    sensitiveData: false,
    inputType: 'Datum',
    explanationExternal: 'De datum dat de test start',
    systemAttribute: true,
    inObject: true,
    fillValue: dateToFill,
    overviewValue: dateOnOverview,
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form1', 'overview'],
  },
  auto_tst_k1000_datum_release_tag: {
    nameAttribute: 'auto_tst_k1000_datum_release_tag',
    magicString: 'auto_tst_k1000_datum_release_tag',
    publicName: 'Release Tag (of andere reden van test)',
    title: 'Release datumtag',
    explanationInternal: 'Dit is de release datum tag',
    sensitiveData: false,
    inputType: 'Tekstveld',
    explanationExternal: 'Een datumtag',
    systemAttribute: true,
    inObject: true,
    fillValue: `${year}.E2E`,
    overviewValue: `${year}.E2E`,
    valueDefault: `${format(new Date(), 'yyyy')}.`,
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form1', 'overview'],
  },
  auto_tst_k1001_adres_postcode: {
    nameAttribute: 'auto_tst_k1001_adres_postcode',
    magicString: 'auto_tst_k1001_adres_postcode',
    publicName: 'auto_tst_k1001_adres_postcode',
    title: 'Adres (dmv postcode)',
    explanationInternal: 'Vul een postcode in om het adres te vinden',
    sensitiveData: false,
    inputType: 'Adres (dmv postcode)',
    explanationExternal: 'Geef een addres dmv postcode',
    mandatory: false,
    addressValue: '3311bv 20',
    overviewValue: "Buddingh'plein 20, 3311BV Dordrecht",
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1001_v2_adres: {
    nameAttribute: 'auto_tst_k1001_v2_adres',
    magicString: 'auto_tst_k1001_v2_adres',
    publicName: 'auto_tst_k1001_v2_adres',
    title: 'V2 adres',
    explanationInternal:
      'Vul een straat met woonplaats, of een postcode in om het adres te vinden',
    sensitiveData: false,
    inputType: 'Adres V2',
    explanationExternal: 'Een postcode of straat en woonplaats is voldoende',
    mandatory: true,
    addressValue: '3311bv 10',
    overviewValue: "Buddingh'plein 10, 3311BV Dordrecht",
    inObject: true,
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1002_adres_postcode_plus: {
    nameAttribute: 'auto_tst_k1002_adres_postcode_plus',
    magicString: 'auto_tst_k1002_adres_postcode_plus',
    publicName: 'auto_tst_k1002_adres_postcode_plus',
    title: 'Postcode plus',
    explanationInternal: 'Dit zijn de adressen dmv de postcode',
    sensitiveData: false,
    inputType: 'Adressen (dmv postcode)',
    explanationExternal: 'Geef alle adressen dmv de postcode',
    addressValue: '3311bv 21',
    overviewValue: "Buddingh'plein 21, 3311BV Dordrecht",
    addressValue2: '3311BV 22',
    overviewValue2: "Buddingh'plein 22, 3311BV Dordrecht",
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1002_v2_geolocatie: {
    nameAttribute: 'auto_tst_k1002_v2_geolocatie',
    magicString: 'auto_tst_k1002_v2_geolocatie',
    publicName: 'auto_tst_k1002_v2_geolocatie',
    title: 'Geolocatie',
    explanationInternal: 'Een locatie gebaseerd op x en y coördinaten',
    sensitiveData: false,
    inputType: 'Geolocatie',
    explanationExternal: 'Dit is een geolocatie',
    mandatory: true,
    inObject: true,
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1003_straat: {
    nameAttribute: 'auto_tst_k1003_straat',
    magicString: 'auto_tst_k1003_straat',
    publicName: 'auto_tst_k1003_straat',
    title: 'Straat',
    explanationInternal: 'Dit is de straat',
    sensitiveData: false,
    inputType: 'Straat',
    explanationExternal: 'Een straat',
    mandatory: true,
    addressValue: 'Gabriëllegang',
    overviewValue: 'Gabriëllegang, Zoetermeer',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1004_straat_plus: {
    nameAttribute: 'auto_tst_k1004_straat_plus',
    magicString: 'auto_tst_k1004_straat_plus',
    publicName: 'auto_tst_k1004_straat_plus',
    title: 'Straat plus',
    explanationInternal: 'Dit is de straat plus',
    sensitiveData: false,
    inputType: 'Straten',
    explanationExternal: 'Een straat plus',
    addressValue: 'Gabriëllaan',
    overviewValue: 'Gabriëllaan, Utrecht',
    addressValue2: "'t Padje",
    overviewValue2: "'t Padje, 't Veld",
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1005_straatnaam: {
    nameAttribute: 'auto_tst_k1005_straatnaam',
    magicString: 'auto_tst_k1005_straatnaam',
    publicName: 'auto_tst_k1005_straatnaam',
    title: 'Straatnaam',
    explanationInternal: 'Dit is de straatnaam',
    sensitiveData: false,
    inputType: 'Adres (dmv straatnaam)',
    explanationExternal: 'Een straatnaam',
    addressValue: 'Mees toxopeüsstraat 2b',
    overviewValue: 'Mees Toxopeüsstraat 2B, 3317WK Dordrecht',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1006_straatnaam_plus: {
    nameAttribute: 'auto_tst_k1006_straatnaam_plus',
    magicString: 'auto_tst_k1006_straatnaam_plus',
    publicName: 'auto_tst_k1006_straatnaam_plus',
    title: 'Straatnaam plus',
    explanationInternal: 'Dit is de straatnaam plus',
    sensitiveData: false,
    inputType: 'Adressen (dmv straatnaam)',
    explanationExternal: 'Een straatnaam plus',
    addressValue: 'Laan van Indië 1c',
    overviewValue: 'Laan van Indië 1c, 7602DA Almelo',
    addressValue2: '5701TM 4',
    overviewValue2: 'To Hölscherplein 4, 5701TM Helmond',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1007_rekeningnummer: {
    nameAttribute: 'auto_tst_k1007_rekeningnummer',
    magicString: 'auto_tst_k1007_rekeningnummer',
    publicName: 'auto_tst_k1007_rekeningnummer',
    title: 'Rekeningnummer',
    explanationInternal: 'Dit is het rekeningnummer',
    sensitiveData: false,
    inputType: 'Rekeningnummer',
    explanationExternal: 'Een rekeningnummer',
    expectedFormValueAfterRuleTrigger: 'NL91ABNA0417164300',
    inObject: true,
    overviewValue: 'NL65FTSB0126256810',
    valueDefault: 'NL65FTSB0126256810',
    becomesReadonlyAfterRuleTrigger: false,
    expectedOverviewValueAfterRuleTrigger: 'NL91ABNA0417164300',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1008_meervoudige_keuze: {
    nameAttribute: 'auto_tst_k1008_meervoudige_keuze',
    magicString: 'auto_tst_k1008_meervoudige_keuze',
    publicName: 'auto_tst_k1008_meervoudige_keuze',
    title: 'Meervoudige keuze',
    explanationInternal: 'Dit is de meervoudige keuze',
    sensitiveData: false,
    inputType: 'Meervoudige keuze',
    explanationExternal: 'Een meervoudige keuze',
    mandatory: true,
    choices: ['Keuzelijst 1', 'Keuzelijst 2', 'Keuzelijst 3'],
    inObject: true,
    checkValue: 'Keuzelijst 2',
    checkValue2: 'Keuzelijst 3',
    overviewValue: 'Keuzelijst 2',
    overviewValue2: 'Keuzelijst 3',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1009_datum: {
    nameAttribute: 'auto_tst_k1009_datum',
    magicString: 'auto_tst_k1009_datum',
    publicName: 'auto_tst_k1009_datum',
    title: 'Datum',
    explanationInternal: 'Dit is een datum',
    sensitiveData: false,
    inputType: 'Datum',
    explanationExternal: 'Een datum',
    mandatory: true,
    fillValue: dateToFill,
    overviewValue: dateOnOverview,
    inObject: true,
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1010_email: {
    nameAttribute: 'auto_tst_k1010_email',
    magicString: 'auto_tst_k1010_email',
    publicName: 'auto_tst_k1010_email',
    title: 'E-mail',
    explanationInternal: 'Dit is het e-mail adres',
    sensitiveData: false,
    inputType: 'E-mail',
    explanationExternal: 'Een e-mail adres',
    expectedFormValueAfterRuleTrigger: 'autobehandelaar@xxllnc.nl',
    inObject: true,
    overviewValue: 'burgerdiacriet@xxllnc.nl',
    valueDefault: 'burgerdiacriet@xxllnc.nl',
    becomesReadonlyAfterRuleTrigger: false,
    expectedOverviewValueAfterRuleTrigger: 'autobehandelaar@xxllnc.nl',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1011_document: {
    nameAttribute: 'auto_tst_k1011_document',
    magicString: 'auto_tst_k1011_document',
    publicName: 'auto_tst_k1011_document',
    title: 'Document',
    explanationInternal: 'Dit is een document',
    sensitiveData: false,
    inputType: 'Document',
    explanationExternal: 'Een document',
    mandatory: true,
    docCategory: 'Aangifte',
    inObject: true,
    testDoc: testFiles[1],
    overviewValue: testFiles[1],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1011_document_2: {
    nameAttribute: 'auto_tst_k1011_document_2',
    magicString: 'auto_tst_k1011_document_2',
    publicName: 'auto_tst_k1011_document_2',
    title: 'Document_2',
    explanationInternal: 'Dit is een document_2',
    sensitiveData: false,
    inputType: 'Document',
    explanationExternal: 'Een document_2',
    mandatory: true,
    docCategory: 'Advies',
    inObject: true,
    testDoc: testFiles[2],
    overviewValue: testFiles[2],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1011_document_3: {
    nameAttribute: 'auto_tst_k1011_document_3',
    magicString: 'auto_tst_k1011_document_3',
    publicName: 'auto_tst_k1011_document_3',
    title: 'Document_3',
    explanationInternal: 'Dit is een document_3',
    sensitiveData: false,
    inputType: 'Document',
    explanationExternal: 'Een document_3',
    mandatory: true,
    docCategory: '',
    inObject: true,
    testDoc: testFiles[3],
    overviewValue: testFiles[3],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1012_locatie_met_kaart: {
    nameAttribute: 'auto_tst_k1012_locatie_met_kaart',
    magicString: 'auto_tst_k1012_locatie_met_kaart',
    publicName: 'auto_tst_k1012_locatie_met_kaart',
    title: 'Locatie met kaart',
    explanationInternal: 'Dit is een locatie met kaart',
    sensitiveData: false,
    inputType: 'Locatie met kaart',
    explanationExternal: 'Een locatie met kaart',
    mandatory: true,
    addressValue: '3311BV 23',
    overviewValue: "Buddingh'plein 23, 3311BV Dordrecht",
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1013_google_maps: {
    nameAttribute: 'auto_tst_k1013_google_maps',
    magicString: 'auto_tst_k1013_google_maps',
    publicName: 'auto_tst_k1013_google_maps',
    title: 'Adres (Google Maps)',
    explanationInternal: 'Dit is een adres (Google Maps)',
    sensitiveData: false,
    inputType: 'Adres (Google Maps)',
    explanationExternal: 'Een adres (Google Maps)',
    addressValue: '3311BV 24',
    overviewValue: "Buddingh'plein 24, 3311BV Dordrecht + −",
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1014_afbeelding_url: {
    nameAttribute: 'auto_tst_k1014_afbeelding_url',
    magicString: 'auto_tst_k1014_afbeelding_url',
    publicName: 'auto_tst_k1014_afbeelding_url',
    title: 'Afbeelding',
    explanationInternal: 'Dit is een afbeelding',
    sensitiveData: false,
    inputType: 'Afbeelding',
    explanationExternal: 'Een afbeelding',
    overviewValue:
      'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=standaard&STYLE=default&TILEMATRIXSET=EPSG%3A28992&TILEMATRIX=9&TILEROW=277&TILECOL=227&FORMAT=image%2Fpng',
    valueDefault:
      'https://service.pdok.nl/brt/achtergrondkaart/wmts/v2_0?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=standaard&STYLE=default&TILEMATRIXSET=EPSG%3A28992&TILEMATRIX=9&TILEROW=277&TILECOL=227&FORMAT=image%2Fpng',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1015_numeriek: {
    nameAttribute: 'auto_tst_k1015_numeriek',
    magicString: 'auto_tst_k1015_numeriek',
    publicName: 'auto_tst_k1015_numeriek',
    title: 'Numeriek',
    explanationInternal: 'Dit is een numerieke waarde',
    sensitiveData: false,
    inputType: 'Numeriek',
    explanationExternal: 'Een numerieke waarde',
    expectedFormValueAfterRuleTrigger: '10',
    afterChoiceValueFixed: true,
    mandatory: true,
    inObject: true,
    overviewValue: '1122334455',
    valueDefault: '1122334455',
    becomesReadonlyAfterRuleTrigger: true,
    expectedOverviewValueAfterRuleTrigger: '10',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1016_enkelvoudige_keuze: {
    nameAttribute: 'auto_tst_k1016_enkelvoudige_keuze',
    magicString: 'auto_tst_k1016_enkelvoudige_keuze',
    publicName: 'auto_tst_k1016_enkelvoudige_keuze',
    title: 'Enkelvoudige keuze',
    explanationInternal: 'Dit is een enkelvoudige keuze',
    sensitiveData: false,
    inputType: 'Enkelvoudige keuze',
    explanationExternal: 'Een enkelvoudige keuze',
    choices: ['Ja', 'Nee'],
    inObject: true,
    overviewValue: 'Nee',
    expectedFormValueAfterRuleTrigger: 'Ja',
    becomesReadonlyAfterRuleTrigger: false,
    expectedOverviewValueAfterRuleTrigger: 'Ja',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1017_keuzelijst: {
    nameAttribute: 'auto_tst_k1017_keuzelijst',
    magicString: 'auto_tst_k1017_keuzelijst',
    publicName: 'auto_tst_k1017_keuzelijst',
    title: 'Keuzelijst',
    explanationInternal: 'Dit is een keuzelijst',
    sensitiveData: false,
    inputType: 'Keuzelijst',
    explanationExternal: 'Een keuzelijst',
    choices: ['Keuzelijst 1', 'Keuzelijst 2', 'Keuzelijst 3'],
    inObject: true,
    selectValue: 'Keuzelijst 2',
    overviewValue: 'Keuzelijst 2',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1018_rich_text: {
    nameAttribute: 'auto_tst_k1018_rich_text',
    magicString: 'auto_tst_k1018_rich_text',
    publicName: 'auto_tst_k1018_rich_text',
    title: 'Rich text',
    explanationInternal: 'Dit is een rich text',
    sensitiveData: false,
    inputType: 'Rich text',
    explanationExternal: 'Een rich text',
    expectedFormValueAfterRuleTrigger: 'BOLD lijst 1 BULLET 1 Italic',
    mandatory: true,
    overviewValue: 'Dit is een rijke tekst',
    inObject: true,
    valueDefault: 'Dit is een rijke tekst',
    becomesReadonlyAfterRuleTrigger: false,
    expectedOverviewValueAfterRuleTrigger: 'BOLD lijst 1 BULLET 1 Italic',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1019_tekstveld: {
    nameAttribute: 'auto_tst_k1019_tekstveld',
    magicString: 'auto_tst_k1019_tekstveld',
    publicName: 'auto_tst_k1019_tekstveld',
    title: 'Tekstveld',
    explanationInternal: 'Dit is een tekstveld',
    sensitiveData: false,
    inputType: 'Tekstveld',
    explanationExternal: 'Een tekstveld',
    expectedFormValueAfterRuleTrigger: 'Zaaksysteem.nl',
    overviewValue: 'Dit is mijn tekst',
    inObject: true,
    valueDefault: 'Dit is mijn tekst',
    becomesReadonlyAfterRuleTrigger: false,
    expectedOverviewValueAfterRuleTrigger: 'Zaaksysteem.nl',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1020_tekstveld_caps: {
    nameAttribute: 'auto_tst_k1020_tekstveld_caps',
    magicString: 'auto_tst_k1020_tekstveld_caps',
    publicName: 'auto_tst_k1020_tekstveld_caps',
    title: 'Tekstveld (HOOFDLETTERS)',
    explanationInternal: 'Dit is een hoofdletters tekstveld',
    sensitiveData: false,
    inputType: 'Tekstveld (HOOFDLETTERS)',
    explanationExternal: 'Hoofdletters typen niet nodig',
    fillValue: 'hoofdletters!!!',
    overviewValue: 'HOOFDLETTERS!!!',
    valueDefault: 'haal een ! weg en je moet dit nu in hoofdletters zien!!!',
    expectedFormValueAfterRuleTrigger: 'ZAAKSYSTEEM.NL',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1021_groot_tekstveld: {
    nameAttribute: 'auto_tst_k1021_groot_tekstveld',
    magicString: 'auto_tst_k1021_groot_tekstveld',
    publicName: 'auto_tst_k1021_groot_tekstveld',
    title: 'Groot tekstveld',
    explanationInternal: 'Dit is een groot tekstveld',
    sensitiveData: false,
    inputType: 'Groot tekstveld',
    explanationExternal: 'Een groot tekstveld',
    expectedFormValueAfterRuleTrigger:
      '100% zaakgericht werken Voor vernieuwers binnen de overheid die ambities waar willen maken! ',
    overviewValue: defaultTextChar500Overview,
    inObject: true,
    valueDefault: defaultTextChar500,
    becomesReadonlyAfterRuleTrigger: false,
    expectedOverviewValueAfterRuleTrigger:
      '100% zaakgericht werken Voor vernieuwers binnen de overheid die ambities waar willen maken!... MEER',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1022_webadres: {
    nameAttribute: 'auto_tst_k1022_webadres',
    magicString: 'auto_tst_k1022_webadres',
    publicName: 'auto_tst_k1022_webadres',
    title: 'Webadres',
    explanationInternal: 'Dit is een webadres',
    sensitiveData: false,
    inputType: 'Webadres',
    explanationExternal: 'Een webadres',
    expectedFormValueAfterRuleTrigger: 'https://zaaksysteem.nl',
    overviewValue: 'https://xxllnc.nl',
    inObject: true,
    valueDefault: 'https://xxllnc.nl',
    becomesReadonlyAfterRuleTrigger: false,
    expectedOverviewValueAfterRuleTrigger: 'https://zaaksysteem.nl',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1023_valuta: {
    nameAttribute: 'auto_tst_k1023_valuta',
    magicString: 'auto_tst_k1023_valuta',
    publicName: 'auto_tst_k1023_valuta',
    title: 'Valuta',
    explanationInternal: 'Dit is een valuta',
    sensitiveData: false,
    inputType: 'Valuta',
    explanationExternal: 'Een valuta',
    expectedFormValueAfterRuleTrigger: '20,00',
    overviewValue: '1222333,44',
    afterChoiceValueFixed: true,
    mandatory: true,
    systemAttribute: true,
    inObject: true,
    valueDefault: '1222333,44',
    becomesReadonlyAfterRuleTrigger: true,
    expectedOverviewValueAfterRuleTrigger: '20,00',
    onPages: ['form2', 'overview'],
  },
  auto_tst_k1024_aanbestedingsdocumenten: {
    nameAttribute: 'auto_tst_k1024_aanbestedingsdocument(en)',
    magicString: 'auto_tst_k1024_aanbestedingsdocumenten',
    publicName: 'auto_tst_k1024_aanbestedingsdocument(en)',
    explanationInternal: 'Test aanbestedingsdocumenten',
    inputType: 'Document',
    explanationExternal: 'Aanbestedingsdocumenten',
    docCategory: 'Rapport',
    docDirection: 'Uitgaand',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_k1026_doc_kenmerk_label_test_1: {
    nameAttribute: 'auto_tst_k1026_doc_kenmerk_label_test_1',
    magicString: 'auto_tst_k1026_doc_kenmerk_label_test_1',
    publicName: 'Label 1',
    explanationInternal: 'Label test',
    inputType: 'Document',
    explanationExternal: 'Test met labels',
    docCategory: 'Aanvraag',
    docDirection: 'Inkomend',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_k1027_BBV_bijlagen: {
    nameAttribute: 'auto_tst_k1027_BBV_bijlagen',
    magicString: 'auto_tst_k1027_BBV_bijlagen',
    publicName: 'auto_tst_k1027_BBV_bijlagen',
    explanationInternal: 'Geef hier uw BBV bijlagen',
    inputType: 'Document',
    explanationExternal: 'Relatie met betrokkene',
    docCategory: 'Rapport',
    docDirection: 'Uitgaand',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_regel_uitvoeren: {
    nameAttribute: 'AUTO Regel uitvoeren',
    magicString: 'auto_regel_uitvoeren',
    publicName: 'AUTO Regel uitvoeren',
    explanationInternal:
      'Bij ja, worden regels uitgevoerd bij de fase afronding',
    inputType: 'Enkelvoudige keuze',
    explanationExternal: 'De regels zijn automatisch uitgevoerd',
    choices: ['Ja', 'Nee'],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_relatie_kenmerk_testronde_v2_object: {
    nameAttribute: 'auto_relatie_kenmerk_testronde_v2_object',
    magicString: 'auto_relatie_kenmerk_testronde_v2_object',
    publicName: 'auto_relatie_kenmerk_testronde_v2_object',
    multipleValuesAllowed: true,
    explanationInternal: 'Dit legt een relatie met een v2 object',
    sensitiveData: false,
    title: 'TESTRONDE V2 object',
    inputType: 'Relatie',
    explanationExternal: 'Relatie met v2 object',
    relationType: 'Object',
    objectType: testrondeObjectTypeV2,
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_relatie_kenmerk_testronde_contact: {
    nameAttribute: 'auto_relatie_kenmerk_testronde_contact',
    magicString: 'auto_relatie_kenmerk_testronde_contact',
    publicName: 'auto_relatie_kenmerk_testronde_contact',
    explanationInternal: 'Dit legt een relatie met een betrokkene',
    sensitiveData: false,
    inputType: 'Relatie',
    explanationExternal: 'Relatie met betrokkene',
    relationType: 'Contact',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t1000_tonen_instructie_en_resultaat: {
    nameAttribute: 'auto_tst_t1000_tonen_instructie_en_resultaat',
    magicString: 'auto_tst_t1000_tonen_instructie_en_resultaat',
    publicName: 'auto_tst_t1000_tonen_instructie_en_resultaat',
    explanationInternal: 'Een tonen instructie en resultaat',
    inputType: 'Meervoudige keuze',
    choices: ['Toon instructies', 'Toon resultaat'],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t1001_sjablonen_aangemaakt: {
    nameAttribute: 'auto_tst_t1001_sjablonen_aangemaakt',
    magicString: 'auto_tst_t1001_sjablonen_aangemaakt',
    publicName: 'auto_tst_t1001_sjablonen_aangemaakt',
    explanationInternal: 'Zijn de sjablonen aangemaakt?',
    inputType: 'Enkelvoudige keuze',
    choices: ['Ja', 'Nee'],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t1002_deelzaak_object_aangemaakt: {
    nameAttribute: 'auto_tst_t1002_deelzaak_object_aangemaakt',
    magicString: 'auto_tst_t1002_deelzaak_object_aangemaakt',
    publicName: 'auto_tst_t1002_deelzaak_object_aangemaakt',
    explanationInternal: 'Is er een deelzaak object aangemaakt',
    inputType: 'Enkelvoudige keuze',
    explanationExternal:
      'Indien ja, dan heeft de medewerker een deelzaak object aangemaakt',
    choices: ['Ja', 'Nee'],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t1003_gerelateerde_zaak_object_gewijzigd: {
    nameAttribute: 'auto_tst_t1003_gerelateerde_zaak_object_gewijzigd',
    magicString: 'auto_tst_t1003_gerelateerde_zaak_object_gewijzigd',
    publicName: 'auto_tst_t1003_gerelateerde_zaak_object_gewijzigd',
    explanationInternal:
      'Is het wijzigen van het object in de gerelateerde zaak getest?',
    inputType: 'Enkelvoudige keuze',
    choices: ['Ja', 'Nee'],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t1004_vervolgzaak_object_verwijderd: {
    nameAttribute: 'auto_tst_t1004_vervolgzaak_object_verwijderd',
    magicString: 'auto_tst_t1004_vervolgzaak_object_verwijderd',
    publicName: 'auto_tst_t1004_vervolgzaak_object_verwijderd',
    explanationInternal:
      'Is het verwijderen van het object uit de vervolgzaak getest?',
    inputType: 'Enkelvoudige keuze',
    choices: ['Ja', 'Nee'],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t2001_saml_nps_nnp_inlog_geslaagd: {
    nameAttribute: 'auto_tst_t2001_saml_nps_nnp_inlog_geslaagd',
    magicString: 'auto_tst_t2001_saml_nps_nnp_inlog_geslaagd',
    publicName: 'auto_tst_t2001_saml_nps_nnp_inlog_geslaagd',
    explanationInternal: 'Is het inloggen met een NNP en NP succesvol getest?',
    inputType: 'Enkelvoudige keuze',
    choices: ['Ja', 'Nee'],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t2002_saml_adfs_inlog_geslaagd: {
    nameAttribute: 'auto_tst_t2002_saml_adfs_inlog_geslaagd',
    magicString: 'auto_tst_t2002_saml_adfs_inlog_geslaagd',
    publicName: 'auto_tst_t2002_saml_adfs_inlog_geslaagd',
    explanationInternal:
      'Geef Geef aan welke ADFS inlog methodes succesvol zijn getest',
    inputType: 'Meervoudige keuze',
    choices: ['Digid', 'E-herkenning', 'eIDAS', 'Alternatieve authenticatie'],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t3001_stuf_berichten_verstuurd: {
    nameAttribute: 'auto_tst_t3001_stuf_berichten_verstuurd',
    magicString: 'auto_tst_t3001_stuf_berichten_verstuurd',
    publicName: 'auto_tst_t3001_stuf_berichten_verstuurd',
    explanationInternal: 'Zijn de volgende stuf berichten succesvol verstuurd',
    inputType: 'Meervoudige keuze',
    choices: [
      'Reserveer zaaknummer',
      'Zaak aanmaken',
      'Documentnummer reserveren',
      'Document toevoegen',
      'Resultaat plaatsen',
      'Zaak afhandelen',
    ],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t3002_stuf_nps_geimporteerd: {
    nameAttribute: 'auto_tst_t3002_stuf_nps_geimporteerd',
    magicString: 'auto_tst_t3002_stuf_nps_geimporteerd',
    publicName: 'auto_tst_t3002_stuf_nps_geimporteerd',
    inputType: 'Meervoudige keuze',
    choices: ['Persoon bevragen', 'Persoon inporteren'],
    explanationInternal: 'Kan een persoon geïmporteerd en bevraagd worden?',
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
  auto_tst_t4001_api_calls_uitgevoerd: {
    nameAttribute: 'auto_tst_t4001_api_calls_uitgevoerd',
    magicString: 'auto_tst_t4001_api_calls_uitgevoerd',
    publicName: 'auto_tst_t4001_api_calls_uitgevoerd',
    explanationInternal: 'Zijn de volgende api calls uitgevoerd',
    inputType: 'Meervoudige keuze',
    choices: [
      'api/v1/case',
      'api/v1/case/create',
      'api/v1/case/update',
      'api/v1/case/prepare_file',
      'api/v1/case/transition',
    ],
    becomesReadonlyAfterRuleTrigger: false,
    onPages: [],
  },
};
