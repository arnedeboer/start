/* eslint-disable camelcase */
import { expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import { createCase } from '../../utils/createCase';
import { testrondeData } from './testrondeData';
import {
  fillAttribute,
  expectAttributeValueOnForm,
  expectAttributeValueOnOverview,
} from './caseRegistration';
import { executeSteps } from '../../utils/executeSteps';

const caseTypeName = 'Auto TESTRONDE V2';
const addressLines =
  '1114AD 1051JLH.J.E. Wenckebachweg 90Donker Curtiusstraat 7';

test.describe
  .serial(`Fill main test case (${caseTypeName}) by employee`, () => {
  test.use({ storageState: authPathBeheerder });
  test.beforeEach(async ({ page }) => {
    await page.goto(testSettings.baseUrl);
    await page.waitForLoadState('domcontentloaded');
  });

  const firstPageAttributes = Object.values(testrondeData).filter(attribute =>
    attribute.onPages.toString().includes('form1')
  );
  const secondPageAttributes = Object.values(testrondeData).filter(attribute =>
    attribute.onPages.toString().includes('form2')
  );
  const overviewPageAttributes = Object.values(testrondeData).filter(
    attribute => attribute.onPages.toString().includes('overview')
  );
  const firstPageDefaultedAttributes = firstPageAttributes.filter(
    attribute => attribute.valueDefault !== undefined
  );
  const secondPageDefaultedAttributes = secondPageAttributes.filter(
    attribute => attribute.valueDefault !== undefined
  );
  const firstPageNonFixedAttributes = firstPageAttributes.filter(attribute => {
    return attribute.afterChoiceValueFixed !== true;
  });
  const overviewPageCheckableAttributes = overviewPageAttributes.filter(
    attribute => {
      return (
        attribute.inputType !== 'Geolocatie' &&
        attribute.inputType !== 'Locatie met kaart'
      );
    }
  );

  test('Fill it without triggering rules', async ({ page }) => {
    await test.step('Start a new case', async () => {
      await createCase({ page, caseType: caseTypeName });
    });

    await executeSteps(
      attribute =>
        `Ensure default value of ${attribute.nameAttribute} is present`,
      firstPageDefaultedAttributes,
      expectAttributeValueOnForm(page)
    );

    await executeSteps(
      attribute => `Fill value for ${attribute.nameAttribute}`,
      firstPageNonFixedAttributes,
      fillAttribute(page)
    );

    await test.step('Go to next page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      // eslint-disable-next-line playwright/no-networkidle
      await page.waitForLoadState('networkidle');
      await expect(page.getByText('Vul ze allemaal in!')).toBeVisible();
    });

    await test.step('Ensure address is visibile before rules execute', async () => {
      // eslint-disable-next-line playwright/no-conditional-in-test
      if (await page.getByText('3311BV Meer Veld toevoegen').isVisible()) {
        await page.getByText('3311BV Meer Veld toevoegen').click();
      }
      await expect(page.getByText(addressLines)).toBeVisible();
    });

    await executeSteps(
      attribute =>
        `Ensure default value of ${attribute.nameAttribute} is present`,
      secondPageDefaultedAttributes,
      expectAttributeValueOnForm(page)
    );

    await test.step("Set 'Nee' to K1016 field, so that rules don't trigger", async () => {
      await page
        .getByText('auto_tst_k1016_enkelvoudige_keuze')
        .scrollIntoViewIfNeeded();
      await page.getByLabel('Nee').click();
      await page.getByLabel('Nee').press('Enter');
    });

    await test.step(`Ensure address is visible after rules were not triggered`, async () => {
      // eslint-disable-next-line playwright/no-conditional-in-test
      if (await page.getByText('3311BV Meer Veld toevoegen').isVisible()) {
        await page.getByText('3311BV Meer Veld toevoegen').click();
      }
      await expect(page.getByText(addressLines)).toBeVisible();
    });

    await executeSteps(
      attribute =>
        `Ensure default value of ${attribute.nameAttribute} is present`,
      secondPageDefaultedAttributes,
      expectAttributeValueOnForm(page)
    );

    await executeSteps(
      attribute => `Fill value for ${attribute.nameAttribute}`,
      secondPageAttributes,
      fillAttribute(page)
    );

    await test.step('Go to overview page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.allocation-picker-option-button');
    });

    await executeSteps(
      attribute =>
        `Ensure value of ${attribute.nameAttribute} is displayed correctly on the overview page`,
      overviewPageCheckableAttributes,
      expectAttributeValueOnOverview(page)
    );
  });

  test('Fill it and trigger the rules', async ({ page }) => {
    await test.step('Start a new case', async () => {
      await createCase({ page, caseType: caseTypeName });
    });

    await executeSteps(
      attribute =>
        `Ensure default value of ${attribute.nameAttribute} is present`,
      firstPageDefaultedAttributes,
      expectAttributeValueOnForm(page)
    );

    await executeSteps(
      attribute => `Fill value for ${attribute.nameAttribute}`,
      firstPageNonFixedAttributes,
      fillAttribute(page)
    );

    await test.step('Go to next page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      // eslint-disable-next-line playwright/no-networkidle
      await page.waitForLoadState('networkidle');
      await expect(page.getByText('Vul ze allemaal in!')).toBeVisible();
    });

    await test.step(`Ensure address is visibile before rules execute`, async () => {
      // eslint-disable-next-line playwright/no-conditional-in-test
      if (await page.getByText('3311BV Meer Veld toevoegen').isVisible()) {
        await page.getByText('3311BV Meer Veld toevoegen').click();
      }
      await expect(page.getByText(addressLines)).toBeVisible();
    });

    await executeSteps(
      attribute =>
        `Ensure default value of ${attribute.nameAttribute} is present`,
      secondPageDefaultedAttributes,
      expectAttributeValueOnForm(page)
    );

    await test.step("Set 'Ja' to K1016 field, so that rules do trigger", async () => {
      await page
        .getByText('auto_tst_k1016_enkelvoudige_keuze')
        .scrollIntoViewIfNeeded();
      await page.getByLabel('Ja').click();
      await page.getByLabel('Ja').press('Enter');
    });

    await test.step('Ensure address is not visible after rules were triggered', async () => {
      // eslint-disable-next-line playwright/no-conditional-in-test
      if (await page.getByText('3311BV Meer Veld toevoegen').isVisible()) {
        await page.getByText('3311BV Meer Veld toevoegen').click();
      }
      await expect(page.getByText(addressLines)).toBeHidden();
    });

    await executeSteps(
      attribute =>
        `Ensure default value of ${attribute.nameAttribute} is present`,
      secondPageDefaultedAttributes,
      expectAttributeValueOnForm(page, true)
    );

    await executeSteps(
      attribute => `Fill value for ${attribute.nameAttribute}`,
      secondPageAttributes,
      fillAttribute(page)
    );

    await test.step('Go to overview page', async () => {
      await page
        .getByRole('button', { name: 'Volgende', exact: true })
        .dblclick();
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.allocation-picker-option-button');
    });

    await executeSteps(
      attribute =>
        `Ensure value of ${attribute.nameAttribute} is displayed correctly on the overview page`,
      overviewPageCheckableAttributes,
      expectAttributeValueOnOverview(page, true)
    );
  });
});
