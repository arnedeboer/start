/* eslint-disable playwright/no-networkidle */
import { Page, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../playwright.config';
import { locators } from '../utils';

const { baseUrl, longTimeout } = testSettings;

const cleanUpZoekopdrachtWidgets = async (page: Page) => {
  const widgets = await page
    .getByRole('region', { name: 'Widget Kies een zoekopdracht' })
    .all();
  for (const widget of widgets) {
    await widget.getByRole('button', { name: 'Widget verwijderen' }).click();
    await page.waitForLoadState('networkidle');
  }
};

test.describe('Starting basic tests, where user is logged in', () => {
  test.use({ storageState: authPathBeheerder });

  test.beforeEach(async ({ page }) => {
    await page.goto(baseUrl);
    await page.waitForLoadState('domcontentloaded');
    await page.waitForLoadState('networkidle');
  });

  test('Add and remove Zoekopdracht widget', async ({ page }) => {
    // first cleanup or else an error is generated when adding to much widgets
    await cleanUpZoekopdrachtWidgets(page);

    await expect(
      page.locator('[aria-label="Groene plus knop menu openen"]')
    ).toBeVisible();

    await page.locator('[aria-label="Groene plus knop menu openen"]').hover();
    await page.getByRole('menuitem', { name: 'Widget aanmaken' }).click();
    await page.getByRole('button', { name: ' Zoekopdracht' }).click();

    await cleanUpZoekopdrachtWidgets(page);
  });

  test('Test if about is shown without errors', async ({ page }) => {
    await page.getByRole('button', { name: 'Hoofdmenu openen' }).click();
    await page.getByRole('menuitem', { name: 'Over xxllnc Zaken' }).click();

    await expect(
      page.locator(locators.partialTextLocator('Applicatie:'))
    ).toBeVisible({
      timeout: longTimeout,
    });
    await page.getByRole('button', { name: 'Dialoog sluiten' }).click();
  });
});
