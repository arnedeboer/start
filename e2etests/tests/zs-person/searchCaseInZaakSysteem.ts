import { Page, expect, test } from '@playwright/test';

export const searchCaseInZaakSysteem = async (
  page: Page,
  caseNumber: string
) => {
  await test.step('Search case', async () => {
    await page.getByRole('button', { name: 'Hoofdmenu openen' }).click();

    await expect(
      page.getByRole('menuitem', { name: ' Logboek' })
    ).toBeVisible();
    await page.getByRole('menuitem', { name: ' Logboek' }).click();

    await expect(page.getByPlaceholder('Zaaknummer')).toBeVisible();
    await page.getByPlaceholder('Zaaknummer').click();
    await page
      .getByPlaceholder('Zaaknummer')
      .type(`${caseNumber.padStart(3, '0')}`);
    await expect(page.getByText('Component')).toBeVisible();
  });
};
