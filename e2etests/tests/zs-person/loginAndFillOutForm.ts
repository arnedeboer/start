/* eslint-disable playwright/no-networkidle */
import { Locator, Page, expect, test } from '@playwright/test';
import { Person } from '../../testfiles';
import { TestSettings } from '../../types';
import { locators } from '../../utils';

const clickOnAndWaitForResponse = async (
  page: Page,
  locator: Locator,
  url: RegExp,
  delay = 0
) => {
  const responsePromise = page.waitForResponse(url);
  await locator.click({ delay });
  await responsePromise;
  await page.waitForLoadState();
};

interface Props {
  formPage: Page;
  person: Person;
  reason: string;
  amount: string;
  testSettings: TestSettings;
}

export const loginAndFillOutForm = async ({
  formPage,
  person,
  reason,
  amount,
  testSettings,
}: Props): Promise<string> => {
  const { baseUrl, personAuthentication } = testSettings;

  // after submitting the form the casenumber will be set
  let caseNumber = '';

  await test.step('Login', async () => {
    await formPage.goto(`${baseUrl}form`);
    await formPage.waitForLoadState('networkidle');
    await expect(
      formPage.locator(locators.persoonLink(personAuthentication.loginForm))
    ).toBeVisible();

    await clickOnAndWaitForResponse(
      formPage,
      formPage.locator(locators.persoonLink(personAuthentication.loginForm)),
      /persoon/
    );
    await formPage.waitForLoadState('networkidle');
    await expect(
      formPage.getByRole('heading', { name: 'Inloggen met DigiD' })
    ).toBeVisible();

    const pipLoginButton = formPage.getByText('SAML IdP:');
    await expect(pipLoginButton).toBeVisible();

    await clickOnAndWaitForResponse(formPage, pipLoginButton, /auth/);
    await formPage.waitForLoadState('networkidle');

    await expect(formPage.getByLabel('BSN:')).toBeVisible();

    await formPage.getByLabel('BSN:').fill(person.BSN);

    const spoofButton = formPage.getByRole('button', { name: 'Spoof!' });
    await expect(spoofButton).toHaveText('Spoof!');

    await clickOnAndWaitForResponse(formPage, spoofButton, /auth/, 300);
    await expect(formPage.locator('h2')).toHaveText('Uw gegevens');
  });

  await test.step('Check contactgegevens tab', async () => {
    const getFieldValue = async (fieldName: string): Promise<string> =>
      formPage.locator(locators.labelLeftOnForm(fieldName)).nth(0).innerText();

    await expect(formPage.locator('h2')).toHaveText('Uw gegevens');

    expect(await getFieldValue('Burgerservicenummer')).toEqual(person.BSN);
    expect(await getFieldValue('Voornamen')).toEqual(person.firstNames);
    expect(await getFieldValue('Tussenvoegsel')).toEqual(person.insertions);
    expect(await getFieldValue('Achternaam')).toEqual(person.familyName);
    expect(await getFieldValue('Adellijke titel')).toEqual(person.nobleTitle);
    expect(await getFieldValue('Geboortedatum')).toEqual(person.birthDate);

    expect(await getFieldValue('Woonplaats')).toEqual(person.residenceCity);
    expect(await getFieldValue('Straatnaam')).toEqual(person.residenceStreet);
    expect(await getFieldValue('Huisnummer')).toEqual(
      person.residenceHouseNumber
    );
    expect(await getFieldValue('Huisnummertoevoeging')).toEqual(
      person.residenceHouseNumberSuffix
    );
    expect(await getFieldValue('Huisletter')).toEqual(
      person.residenceHouseNumberLetter
    );
    expect(await getFieldValue('Postcode')).toEqual(person.residenceZipcode);

    await expect(
      formPage.getByLabel('Telefoonnummer', { exact: true })
    ).toHaveValue(person.phoneNumber);
    await expect(
      formPage.getByLabel('Telefoonnummer (mobiel)', { exact: true })
    ).toHaveValue(person.mobileNumber);
    await expect(
      formPage.getByLabel('E-mailadres*', { exact: true })
    ).toHaveValue(person.email);
  });

  await test.step('Check explanation page', async () => {
    //  Click on next button to go to the explanation page
    const responsePromise = formPage.waitForResponse(
      `${baseUrl}zaak/create/webformulier/aanvrager`
    );
    const button = formPage.getByRole('button', { name: 'Volgende' });
    await button.scrollIntoViewIfNeeded();
    await button.click();
    await responsePromise;
    await formPage.waitForLoadState();
    await new Promise(rez => setTimeout(rez, 1000));

    const onCaseResetScreen = await formPage
      .getByText('Nee, ik wil alle gegevens opnieuw invullen')
      .isVisible();

    // eslint-disable-next-line playwright/no-conditional-in-test
    if (onCaseResetScreen) {
      const button2 = formPage.getByRole('button', { name: 'Volgende' });
      await button2.scrollIntoViewIfNeeded();
      await button2.click({ delay: 1000 });
      await new Promise(rez => setTimeout(rez, 1000));
      await formPage.waitForLoadState();
    }

    // Check if test is on payment explanation page
    await expect(formPage.locator('h2')).toHaveText('Uitleg test');
    await expect(
      formPage.getByText(
        'Dit formulier dient om de ogone/inigenico betalings koppeling te testen'
      )
    ).toBeVisible();
    await expect(
      formPage.getByText('Dit is een test voor de Ogone betaling')
    ).toBeVisible();

    // Click next to go to the next page and check if the next page is loaded
    await formPage.getByRole('button', { name: 'Volgende' }).click();
    await expect(
      formPage.getByText('Donatie bedrag', { exact: true })
    ).toBeVisible();
  });

  await test.step('Fill donation page', async () => {
    await formPage.getByRole('textbox').fill(amount);

    const redenDonatie = formPage
      .locator('//*[@class="ql-editor ql-blank"]')
      .first();
    await redenDonatie.press('Control+b');
    await redenDonatie.fill(reason);

    await formPage.getByRole('button', { name: 'Volgende' }).click();
    await expect(
      formPage.getByRole('heading', {
        name: 'Controleer uw invoer en overige acties',
      })
    ).toBeVisible();
  });

  await test.step('Check overview page and submit form', async () => {
    await expect(
      formPage
        .locator(
          locators.labelLeftOnPersonFormOverview('Reden van donatie', '2')
        )
        .first()
    ).toHaveText(`${reason}`);
    await expect(
      formPage
        .locator(locators.labelLeftOnPersonFormOverview('Donatie bedrag', '2'))
        .first()
    ).toHaveText(`${amount}`);

    await formPage.getByRole('button', { name: 'Versturen' }).click();
    await expect(
      formPage.locator(locators.labelLeftOnForm('Zaaknummer'))
    ).toBeVisible({ timeout: 10000 });

    caseNumber = await formPage
      .locator(locators.labelLeftOnForm('Zaaknummer'))
      .innerText();

    await expect(
      formPage.locator(locators.labelLeftOnForm('Product of dienst'))
    ).toHaveText(personAuthentication.loginForm);
    await formPage
      .locator(locators.labelLeftOnForm('Bedrag'))
      .scrollIntoViewIfNeeded();
    const amountFound = await formPage
      .locator(locators.labelLeftOnForm('Bedrag'))
      .innerText();
    expect(
      amountFound === `€ ${amount}` ||
        amountFound === `€ ${amount.replace(',', '.')}`
    ).toBeTruthy();
  });

  await test.step('Open ideal', async () => {
    await formPage.getByRole('button', { name: 'Betalen' }).first().click();
    await expect(
      formPage.getByText('Betaalbevestiging').locator('visible=true')
    ).toBeVisible();

    await formPage.getByRole('button', { name: 'iDEAL' }).click();
    await expect(
      formPage.getByText(
        'Kies uw bank en klik op "Ga verder" om bij uw bank met iDEAL te betalen.'
      )
    ).toBeVisible();

    await formPage
      .getByRole('combobox', { name: 'Selecteer uw bank' })
      .selectOption('9999+TST');
    await formPage.getByRole('button', { name: 'Ga verder' }).click();
    await expect(
      formPage.getByRole('heading', { name: 'iDEAL simulator' })
    ).toBeVisible();
  });

  return Promise.resolve(caseNumber);
};
