import { BrowserContext, Page, expect, test } from '@playwright/test';
import { authPathBeheerder, testSettings } from '../../playwright.config';
import personData from '../../testfiles/personData';
import { loginAndFillOutForm } from './loginAndFillOutForm';
import { locators } from '../../utils';
import { searchCaseInZaakSysteem } from './searchCaseInZaakSysteem';

const paymentTests = [
  {
    paymentResult: 'SUCCESS',
    reason: 'Ik betaal dit bedrag om SUCCESS van de betaling te testen',
    amount: '888,00',
    resultHeaderText: 'Uw betaling is gelukt',
    resultText1:
      'Bedankt voor het aangaan van een Betaling internetkassa Ingenico automatische test.' +
      ' Uw registratie is bij ons bekend onder zaaknummer ',
    resultText2:
      '. Wij verzoeken u om bij verdere communicatie ' +
      'dit zaaknummer te gebruiken. De behandeling van deze zaak zal spoedig plaatsvinden.',
    resultText3:
      'Ook kunt u op elk moment van de dag de voortgang en inhoud inzien via de persoonlijke internetpagina',
    betaalStatus: 'Betaalstatus: Geslaagd',
  },
  {
    paymentResult: 'EXCEPTION',
    reason:
      'Ik betaal dit bedrag NIET om een EXCEPTION tijdens het betalen te testen',
    amount: '381,00',
    resultHeaderText: 'Uw betaling is mislukt.',
    resultText1: ' voor meer informatie.Gebruik zaaknummer ',
    resultText2: ' als referentie.',
    resultText3: '',
    betaalStatus: 'Betaalstatus: Wachten op bevestiging',
  },
];

paymentTests.forEach(paymentTest => {
  const {
    amount,
    paymentResult,
    reason,
    resultText1,
    resultText2,
    resultText3,
    resultHeaderText,
    betaalStatus,
  } = paymentTest;

  test.describe
    .serial(`Test payment with result ${paymentResult}. Person pays through form and Digid login. Employee treats case.`, () => {
    const { baseUrl, options, personAuthentication, environment } =
      testSettings;

    test.skip(
      !['default', 'development'].includes(environment),
      'This test is only configured on the development environmnent so skipping it'
    );

    let formContext: BrowserContext;
    let formPage: Page;

    // caseNumber is set in loginAndFillOutForm and it is used to check the payment
    let caseNumber = '';

    test.beforeAll(async ({ browser }) => {
      // page context as person that is logged in with DigiD
      formContext = await browser.newContext(options);
      formPage = await formContext.newPage();
    });

    test.afterAll(async () => {
      await formContext.close();
    });

    test(`Login, fill out form, submit and initiate payment for test ${paymentResult}`, async () => {
      caseNumber = await loginAndFillOutForm({
        formPage,
        amount,
        person: personData.development['132915947'],
        reason,
        testSettings,
      });
      expect(caseNumber.length).toBeGreaterThan(0);
    });

    test(`Test clicking on button ${paymentResult}`, async () => {
      await formPage.getByRole('button', { name: paymentResult }).click();

      await expect(formPage.getByRole('button', { name: 'OK' })).toBeVisible();
      await formPage.getByRole('button', { name: 'OK' }).click();

      await expect(
        formPage.getByRole('heading', { name: resultHeaderText })
      ).toBeVisible();
      await expect(
        formPage.getByText(`${resultText1}${caseNumber}${resultText2}`)
      ).toBeVisible();
      // eslint-disable-next-line playwright/no-conditional-in-test
      if (resultText3 !== '')
        await expect(formPage.getByText(resultText3)).toBeVisible();
    });

    test(`Check payment on mijn pagina for test ${paymentResult}`, async () => {
      // eslint-disable-next-line playwright/no-conditional-in-test
      if (paymentResult === 'SUCCESS') {
        await formPage.getByRole('link', { name: 'Ga naar Mijn ' }).click();
        await expect(
          formPage.getByRole('heading', { name: 'Zaken', exact: true })
        ).toBeVisible();
      } else {
        await formPage.goto(`${baseUrl}/pip`);
      }

      await expect(
        formPage.getByText(`${caseNumber}: ${personAuthentication.loginForm}`)
      ).toBeVisible();

      await formPage
        .getByRole('link', {
          name: `${caseNumber}: ${personAuthentication.loginForm}`,
        })
        .click();

      await expect(formPage.getByText('Registreren')).toBeVisible();
      await formPage.getByText('Registreren').click();

      await formPage.getByText('Reden van donatie').scrollIntoViewIfNeeded();
      await expect(formPage.getByText(reason)).toBeVisible();
      await expect(formPage.getByText(amount.replace(',', '.'))).toBeVisible();
    });

    test.describe
      .serial(`Check case and payment result in zaaksysteem for payment result ${paymentResult}`, () => {
      test.use({ storageState: authPathBeheerder });

      test(`Open case in zaaksysteem and check if it is registered for test ${paymentResult}`, async ({
        page,
      }) => {
        await test.step('Open zaaksysteem', async () => {
          await page.goto(baseUrl);
          await expect(
            page.getByRole('button', { name: 'Hoofdmenu openen' })
          ).toBeVisible();
        });

        await searchCaseInZaakSysteem(page, caseNumber);

        await test.step('Open the case for test', async () => {
          await expect(
            page.getByText(`Zaak ${caseNumber} geregistreerd`)
          ).toBeVisible();

          const registeredCase = page
            .getByRole('row', { name: 'row' })
            .filter({ hasText: `Zaak ${caseNumber} geregistreerdZaak-` })
            .getByRole('link', { name: `${caseNumber}` });
          await expect(registeredCase).toBeVisible();
          await registeredCase.click();

          await expect(page.getByText('Afhandelen').nth(1)).toBeVisible();
        });

        await test.step('Open phase registration', async () => {
          await page
            .getByRole('link', { name: 'Afgeronde fase Registreren' })
            .click();
          await expect(page.getByText('Uitleg test')).toBeVisible();
        });

        await test.step('Check reason and amount in phase registration', async () => {
          await page
            .locator(locators.labelLeftRichTextFatt('Reden van donatie'))
            .scrollIntoViewIfNeeded();
          await expect(
            page.locator(locators.labelLeftRichTextFatt('Reden van donatie'))
          ).toHaveText(reason);

          await page.getByLabel('Donatie bedrag*').scrollIntoViewIfNeeded();
          const amountFound = await page
            .getByLabel('Donatie bedrag*')
            .inputValue();
          expect(amountFound).toEqual(amount);
        });

        await test.step('Open information panel left if it is closed', async () => {
          const panelIsClosed = await page
            .locator('//zs-icon[@icon-type="chevron-right"]')
            .nth(1)
            .isVisible();
          // eslint-disable-next-line playwright/no-conditional-in-test
          if (panelIsClosed)
            await page.getByRole('button', { name: '' }).click();
        });

        await test.step('Check amount, status symbol and information message and close panal', async () => {
          const amountFound = await page
            .getByRole('listitem', { name: betaalStatus })
            .locator('a')
            .innerText();
          expect(amountFound).toEqual(amount);

          await page
            .getByRole('listitem', { name: betaalStatus })
            .locator('a')
            .hover();
          await expect(page.getByText(betaalStatus)).toBeVisible();
          await expect(
            page.getByRole('listitem', { name: betaalStatus }).locator('a')
          ).toBeVisible();
        });

        await test.step('Close information panel left if it is open', async () => {
          const panelIsOpen = await page
            .locator('//zs-icon[@icon-type="chevron-left"]')
            .nth(0)
            .isVisible();
          // eslint-disable-next-line playwright/no-conditional-in-test
          if (panelIsOpen)
            await page.getByRole('button', { name: '' }).nth(0).click();
        });

        await test.step('Finish case treatment', async () => {
          await page
            .getByRole('link', { name: 'Huidige fase Afhandelen' })
            .click();

          await expect(page.getByLabel('Afgehandeld').first()).toBeVisible();
          await page.getByLabel('Afgehandeld').first().check();
          await page
            .getByRole('listitem')
            .filter({
              hasText:
                'Deze zaak heeft nog geen behandelaar. In behandeling nemen',
            })
            .getByRole('button', { name: 'In behandeling nemen' })
            .click();

          await expect(
            page.getByRole('button', { name: ' Zaak afhandelen' })
          ).toBeVisible();
          await page.getByRole('button', { name: ' Zaak afhandelen' }).click();
          await expect(
            page.getByLabel('Widget Mijn openstaande zaken (')
          ).toBeVisible();
        });

        await searchCaseInZaakSysteem(page, caseNumber);

        await test.step('Check if case is "Afgehandeld"', async () => {
          await expect(
            page.getByText(
              `Zaak ${caseNumber} afgehandeld: Afgehandeld (afgehandeld)`
            )
          ).toBeVisible();
        });
      });
    });
  });
});
