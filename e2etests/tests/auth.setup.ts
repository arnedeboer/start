import { Locator, Page, expect, test as setup } from '@playwright/test';
import {
  authPathBeheerder,
  authPathPerson,
  testSettings,
} from '../playwright.config';
import { isUrlOpened } from '../utils';

const {
  baseUrl,
  beheerderUser,
  beheerderPassword,
  pageTitle,
  personAuthentication,
  longTimeout,
} = testSettings;

const clickOnAndWaitForResponse = async (
  page: Page,
  locator: Locator,
  url: RegExp
) => {
  const responsePromise = page.waitForResponse(url);
  await locator.click();
  await responsePromise;
  await page.waitForLoadState();
};

setup('authenticate beheerder', async ({ page }) => {
  await page.goto(baseUrl);
  await isUrlOpened(page, pageTitle);

  await expect(page.getByPlaceholder('Gebruikersnaam')).toBeVisible({
    timeout: longTimeout,
  });
  await page.getByPlaceholder('Gebruikersnaam').fill(beheerderUser);
  await page.getByPlaceholder('Wachtwoord').fill(beheerderPassword);
  await page.getByRole('button', { name: 'Inloggen' }).click();

  await page.waitForURL(/intern/);

  await page.context().storageState({ path: authPathBeheerder });
});

setup('authenticate person with login into pip', async ({ page }) => {
  await page.goto(`${baseUrl}pip/login`);
  // eslint-disable-next-line playwright/no-networkidle
  await page.waitForLoadState('networkidle');

  await expect(page.locator('h2')).toHaveText([
    'Inloggen als persoon',
    'Inloggen als organisatie',
  ]);

  const pipLoginButton = page.locator('[pip-login]');
  await clickOnAndWaitForResponse(page, pipLoginButton, /auth/);
  // eslint-disable-next-line playwright/no-networkidle
  await page.waitForLoadState('networkidle');

  const spoofButton = page.getByRole('button', { name: 'Spoof!' });
  await expect(spoofButton).toHaveText('Spoof!');

  await page.getByLabel('BSN:').fill(personAuthentication.bsn);
  await spoofButton.click();
  // eslint-disable-next-line playwright/no-networkidle
  await page.waitForLoadState('networkidle');
  await page.waitForURL(/pip/);

  await page.context().storageState({ path: authPathPerson });
});
