# E2e: Getting started

To run the e2e tests locally the packages must to be installed and a settings.json file must to be created.

Follow these steps to start testing

> Make sure [nodejs 16 or higher with npm](https://nodejs.org/en/download/current), [npx](https://www.npmjs.com/package/npx) and [yarn](https://classic.yarnpkg.com/lang/en/docs/install) are installed

Go to the e2etests directory and install the packages from within that directory:

```yarn
cd e2etests
yarn install --frozen-lockfile
```

In some cases it is necessary to run

```bash
npx playwright install
```

Create a settings.json file:

```powershell
cp settings.example.json settings.json
```

Then edit the settings.json file and fill in the correct values
In the settings.json file there are multiple environment defined.
These environments correspondent with the branch names in Gitlab. In the pipeline
the environment variable `CI_COMMIT_REF_SLUG` contains the branch name. When testing
locally set the environment variable `CI_COMMIT_REF_SLUG` to switch settings.
For example use `export CI_COMMIT_REF_SLUG=development` to test the
development environment

For more information about the settings.json file see the description in the file

## Start test from shell

Open a shell and run the following command from the e2etest directory to start
the tests locally:

```npx
npx playwright test
```

## Start test from vs-code

Make sure the playwright extension is installed. It is added as a recommended
extension in this project

Open the test explorer in vs-code and start a test by clicking on the play button

If no tests are shown sometimes vs-code need to be restarted.

### Start test from vs-code in WSL

Headed Playwright with WSL based on

- <https://medium.com/@matthewkleinsmith/headful-playwright-with-wsl-4bf697a44ecf>
- <https://stackoverflow.com/questions/61110603/how-to-set-up-working-x11-forwarding-on-wsl2>
  
Steps:

1. Have an already existing Node.js Playwright project.
2. Ensure your system packages are up to date:
  
```bash
sudo apt update
sudo apt upgrade
```

3. Install these:

```bash
sudo apt install libgtk-3-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libgstreamer-plugins-bad1.0-0 gstreamer1.0-libav -y
```

4. Download and install [VcXsrc](https://sourceforge.net/projects/vcxsrv/)
  
5. Put the following in ~/.bashrc:
  
```bash
# Get the IP Address of the Windows 10 Host and use it in Environment.

HOST_IP=$(host `hostname` | grep -oP '(\s)\d+(\.\d+){3}' | tail -1 | awk '{ print $NF }' | tr -d '\r')
export LIBGL_ALWAYS_INDIRECT=1
export DISPLAY=$HOST_IP:0.0
export NO_AT_BRIDGE=1
export PULSE_SERVER=tcp:$HOST_IP
```

6. Reload .bashrc. In a terminal, enter: `source ~/.bashrc`

Start test:

1. Launch program manually: `C:\Program Files\VcXsrv\xlaunch.exe`
2. Select display settings: choose `Multiple windows` and `Display number` is `-1`
3. Select how to start clients: choose `Start no client` Extra settings: `Enable` "Clipboard", `Disable` "Native opengl", `Enable` "Disable access control" and no additional parameters.
4. Open linux terminal (bash)
5. `npx playwright test --debug`
  
## Update playwright

Playwright is actively maintained. So new versions are released often. If you want to update the whole project:

```yarn
yarn upgrade playwright
```

This updates both locally your project as well as the yarn.lock.

## Installing yarn

Installation depends on whether you want to install it on linux/ubuntu or on windows. Follow for example the instructions on:

<https://www.hostinger.com/tutorials/how-to-install-yarn>

## Docker compose

You can run the end to end tests locally by running the following `docker compose` command

from the root of the `start` repository directly:

```bash
docker compose run --rm e2etests
```

make sure the settings.json file is created first
