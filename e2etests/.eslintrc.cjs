const ERROR = 'error';
const OFF = 'off';
const ID_LENGTH = 2;

module.exports = {
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
    babelOptions: {
      babelrc: false,
      configFile: false,
      compact: true,
      presets: ['@babel/preset-typescript'],
    },
  },
  extends: ['prettier', 'eslint:recommended', 'plugin:playwright/recommended'],
  plugins: ['prettier', '@typescript-eslint'],
  settings: {},
  env: {
    browser: true,
    node: true,
    es6: true,
    jest: true,
  },
  overrides: [
    {
      files: ['**/*.ts?(x)'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
        ecmaFeatures: {
          jsx: true,
        },

        // typescript-eslint specific options
        warnOnUnsupportedTypeScriptVersion: false,
      },
      plugins: ['@typescript-eslint'],
      // If adding a typescript-eslint version of an existing ESLint rule,
      // make sure to disable the ESLint rule here.
      rules: {
        // TypeScript's `noFallthroughCasesInSwitch` option is more robust (#6906)
        'default-case': OFF,
        // 'tsc' already handles this (https://github.com/typescript-eslint/typescript-eslint/issues/291)
        'no-dupe-class-members': OFF,
        // 'tsc' already handles this (https://github.com/typescript-eslint/typescript-eslint/issues/477)
        'no-undef': OFF,
        'no-use-before-define': OFF,
        '@typescript-eslint/no-use-before-define': [OFF],
        'no-unused-vars': OFF,
        '@typescript-eslint/no-unused-vars': [
          ERROR,
          {
            args: 'none',
            ignoreRestSiblings: true,
          },
        ],
        'no-useless-constructor': OFF,
        '@typescript-eslint/no-useless-constructor': 'warn',
      },
    },
  ],
  rules: {
    'id-length': [
      ERROR,
      {
        min: ID_LENGTH,
        exceptions: ['t'],
      },
    ],
    'no-magic-numbers': 0,
    'no-confusing-arrow': [OFF],
    'no-inline-comments': [OFF],
    'prettier/prettier': [ERROR],
    'import/named': [OFF],
    'import/default': [OFF],
    'import/no-named-as-default': [OFF],
    'import/no-named-as-default-member': [OFF],
    'import/no-unresolved': [OFF],
    'import/namespace': [OFF],
    'import/export': [OFF],
    'import/no-duplicates': [OFF],
    'playwright/no-skipped-test': [OFF],
  },
};
