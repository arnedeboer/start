# Introduction

This repository contains all the `zaaksysteem` components.

Working with our `zaaksysteem` requires working in a lot of different libraries
/ packages / services. This repository is a "mono-repo", which makes working
on different parts of our `zaaksysteem` a lot easier by providing a lot of
helper utils. These utils are described below.

## Getting started

If you're running Docker Desktop on Windows or MacOS, make sure Docker can use
at least 10GB of memory. By default the virtual machines on those operating
systems only have 2GB of memory.

### TLDR

Go to [yq](https://github.com/mikefarah/yq/tags) and find the
appropriate download link. For example `https://github.com/mikefarah/yq/releases/download/v4.33.2/yq_linux_amd64`.

_Note:_ yq version 4.x is required.

```Shell
git clone git@gitlab.com:xxllnc/zaakgericht/zaken/start.git
cd start

mkdir -p ~/.local/bin
curl -L -o ~/.local/bin/yq <<< DOWNLOAD LINK >>> && chmod +x ~/.local/bin/yq

bin/zs zaaksysteem setup
bin/zs devmode init

docker-compose up -d

bin/extract_ca_certificate.sh
```

The `zaaksysteem` development environment is now available at
[dev.zaaksysteem.nl](https://dev.zaaksysteem.nl/)

To get rid of the security error (unknown CA), you can tell your browser or
operating system to trust the certificate stored in `dev.zaaksysteem.nl.pem`.

By default, pre-built Docker images are used. See below for how to set up
development mode for frontend and backend.

### Long Version

Clone the `zaaksysteem` repository and change the current working directory
to `start`.

```Shell
git clone git@gitlab.com:xxllnc/zaakgericht/zaken/start.git
cd start
```

If yq _4.x_ is not installed then install it in `HOME/.local/bin`. Go to [yq](https://github.com/mikefarah/yq/tags)
and find the appropriate download link. For example `https://github.com/mikefarah/yq/releases/download/v4.33.2/yq_linux_amd64`.

_Note:_ yq version 4.x is required.

```Shell
mkdir -p ~/.local/bin
curl -L -o ~/.local/bin/yq <<< DOWNLOAD LINK >>> && chmod +x ~/.local/bin/yq
# example: curl -L -o ~/.local/bin/yq https://github.com/mikefarah/yq/releases/download/v4.33.2/yq_linux_amd64 && chmod +x ~/.local/bin/yq
```

The next step is to setup the development environment by preparing
configuration. This command will create the required configuration files from
their `.dist` counterparts in `etc/`, and create the storage bucket in the
`Minio` container. `Minio` is a service we use to store files in our local
development environments. Go to [MinIO](https://hub.docker.com/r/minio/minio)
for details.

```Shell
bin/zs zaaksysteem setup
```

Now we set the development mode to the initial state. See [Development mode](#development-mode)
for more information.

```Shell
bin/zs devmode init
```

That's it. Now start your development `zaaksysteem` by running
`docker-compose up`, like this. To run the containers in the background option
`-d` is used.

```Shell
docker-compose up -d
```

This could take a while. Find yourself some coffee, and when Docker tells you
everything is ok, wait two minutes more, and browse to [dev.zaaksysteem.nl](https://dev.zaaksysteem.nl)
for your first encounter.

In case you cannot access the page, make sure to update your `HOSTS` file on
your os with the following entries:

> 127.0.0.1 dev.zaaksysteem.nl
> 127.0.0.1 mailhog.dev.zaaksysteem.nl
> 127.0.0.1 rabbitmq.dev.zaaksysteem.nl
> 127.0.0.1 minio.dev.zaaksysteem.nl
> 127.0.0.1 minio-ui.dev.zaaksysteem.nl
> 127.0.0.1 redis.dev.zaaksysteem.nl

If you have DNS rebind protection you might want to list `dev.zaaksysteem.nl`
and its subdomains as exceptions. Or you add them in your `HOSTS` file.

```Shell
# This list is not full list of hosts
127.0.0.1   localhost dev.zaaksysteem.nl mailhog.dev.zaaksysteem.nl rabbitmq.dev.zaaksysteem.nl minio.dev.zaaksysteem.nl minio-ui.dev.zaaksysteem.nl redis.dev.zaaksysteem.nl
```

You can log in with the following user accounts:

| Username    | Password    | Description                                                             |
| ----------- | ----------- | ----------------------------------------------------------------------- |
| `admin`     | `admin`     | Main "administrator" user; this one we use the most during development. |
| `beheerder` | `beheerder` | Secondary administrator account                                         |
| `gebruiker` | `gebruiker` | Normal user account with few rights.                                    |

Of course you're free to create more users, roles and groups in your development
environment as needed.

You'll probably see a HTTPS security error (unknown CA). To extract the CA
certificate used by your dev environment, run this:

```Shell
bin/extract_ca_certificate.sh
```

This puts the CA certificate in `dev.zaaksysteem.nl.pem` -- tell your browser
or OS to trust it and the error should disappear. For chrome users make sure
to import the cert in the `trusted root certification authorities` / `Vertrouwde basiscertificeringsinstanties` folder.

### Development mode

By default, Docker images are used, which means changes in the source code
won't be picked up by the services. To make that work, select a frontend and/or
backend development mode:

```Shell
bin/zs devmode frontend {mode}
bin/zs devmode backend {mode}
```

The frontend development supports 3 modes:

- `image` (use a pre-build container image, default)
- `container` (runs the development server in a container)
- `local` (to run the development server locally).

Run `docker maintenance` after switching modes:

```Shell
bin/zs docker maintenance
```

To run the development server for `local` frontend development, use the
following:

```Shell
# And then run the local development server:
cd frontend-mono
yarn install
WEBPACK_BUILD_TARGET=development yarn lerna:start
```

The backend development supports 2 modes:

- `image` (use a pre-build container image, default)
- `container` (runs the backend in a container)

So if you're a backend developer, you can use:

```Shell
bin/zs devmode frontend image
bin/zs devmode backend container
```

A frontend developer might want:

```Shell
bin/zs devmode frontend local
bin/zs devmode backend image
```

Or if you're running Linux on your development machine:

```Shell
bin/zs devmode frontend local
bin/zs devmode backend container
```

_Note:_ don't forget to run `docker maintenance` after switching modes:

```Shell
bin/zs docker maintenance
```

For Visual Studio Code users workspace settings files (`.code-workspace`)
exists.

- Use `frontend-dev.code-workspace` for frontend development.
- Use `python-dev.code-workspace` for backend development.

## Typical Development

Below are examples to start development on our systems, depending on the part
of the system you're working on, here are some options.

### Python

For python development, you are probably working on domain code, combined with
a HTTP daemon or RabbitMQ consumer. Make sure you have installed python version
3.11. For linux users, more information can be found [here](https://tecadmin.net/how-to-install-python-3-11-on-debian/).
Make sure you have installed the `venv` package for Python 3.11 (python3.11-venv).
There are more options to install the `venv` package. For example use the `apt`
package manager to install the package:

- Use the command `apt list --installed | grep python3*` to see what packages
  are installed on Ubuntu linux.
- To install on Ubuntu linux the `venv` package for Python 3.11 use the command
  `sudo apt install python3.11-venv`.

Typical development involves the daemons, the domain (business logic) and the
database (infra). For the HTTP daemon of the domain "case management", you
would work in:

- http-cm
- domains

#### Using Docker

When using Docker, it is useful to have a requirements file containing links to
the local copies of dependencies.

For instance, if you are starting work on the backend python service `http-cm`,
you should run the following commands from the start folder:

```Shell
./bin/zs python dinstall http-cm # Install the local dependencies in "editable" mode in the Docker container for the backend python service `http-cm`
docker-compose logs http-cm # View output from the Docker container for the backend python service `http-cm`
```

This command will generate requirements files containing links to the other
packages, so you do not have to wait for the `domains` changes to be pushed
before you can work on the HTTP service.

A quick way to generate the requirements for every python package, simply run
the same command in the main `start` directory. You can also do this with the
`vinstall` command, to update all virtual environments.

Generally, you do not want to run `bin/zs python dinstall <subproject>` for all
python services, because then each service sets up file watches for all source
files, causing them to restart every time a source file is edited anywhere,
leading to a lot of (unnecessary) extra CPU usage, as most projects touch max.
one or two services at once.

Run `bin/zs python` from the main `start` directory to get more information
and help.

#### Using a virtual environment

If you want to use Visual Studio Code (VSCode) and all of the helper utilities
for testing, without running everything inside containers, you can also set up
virtual environments.

_Note:_ Remove the (old) virtual environment when using a new Python version.

We will use the `http-cm` service as an example again. First go to the main
`start` directory. Run the commands from this folder.

```Shell
bin/zs python setup http-cm
source .venv/http-cm/bin/activate
```

This will create a virtual environment in `.venv/http-cm` in the `start` folder
(repository root folder) and activates it in your current shell, but this does
not yet install the requirements. To do that, run the following command for
that. Run the commands from the `start` folder.

```Shell
bin/zs python vinstall http-cm
```

This will run `pip install -r REQUIREMENTSFILES` for this service.

### Perl

Perl is the most straightforward way for developing. Just edit some files in
`backend/perl-api` folder in the `start` folder (repository root folder) and
run the necessary `docker-compose` commands to reload the engine.

```Shell
vim backend/perl-api/lib/Zaaksysteem/Constants.pm

docker-compose stop perl-api
docker-compose up -d perl-api
```

### JavaScript (React)

Running the React codebase in `local` development mode requires the following
to be installed locally:

- [Brew](https://brew.sh/) (optional) to make managing/installing packages easier
- [Node](https://github.com/nvm-sh/nvm) a recent version of Node
- [Yarn](https://classic.yarnpkg.com/en/docs/install/) a recent version of Yarn

Once installed, you can run the development environment from the the `start`
folder (repository root folder).

```Shell
cd frontend-mono
yarn install
WEBPACK_BUILD_TARGET=development yarn lerna:start
```

This will serve the React apps locally, and will refresh any changes you make
to the code via Hot Module Reloading.

To use the frontend `container` development mode, all you need to do is enable
it (see [Development mode](#development-mode)) and rebuild/restart your
containers. This will run the above `yarn lerna:start` command inside a
container.

See `./docs/` for more details on installation and running of the
frontend development tools.

### Javascript (Angular)

Use this script to start the app / area you wish to develop in:

```Shell
./bin/run-frontend.sh {command}
```

where `{command}` is one of the following:

- reset
- gulp
- styles
- frontend
- intern
- mor
- vergadering
- wordaddin
- init

This will start the development server in the `frontend-frontendclient`
container, and start watching files. Your changes should then be made available
on your local development environment.

_Note:_ Use `./bin/run-frontend.sh` to list all commands that are available.

## End to End (e2e) tests

See [e2etests Readme](e2etests/README.md)

## Development Certificate

Follow these steps to install the Development Certificate:

### MacOS / Chrome

In Chrome, go to Settings -> Privacy and Security, Manage Certificates. This will open up the Keychain.

Drag the textfile into this window.

Right-click on the 'Zaaksysteem Development CA' and select 'Get Info'.

Open 'Trust' and set 'When using this certificate' to 'Always Trust'.

## How does it fit together

Todo, but let's start with this diagram:

![Overview of repositories](assets/repositories_zoom_70percent.png)

## Admin Interfaces

Some of the services used by Zaaksysteem come with web-based admin tools. These are available at
the following URLs:

- [RabbitMQ](https://rabbitmq.dev.zaaksysteem.nl/) (message queue; use guest / guest to log in)
- [Minio](https://minio.dev.zaaksysteem.nl/) (S3-compatible file store; see etc/minty_config.conf for login keys)
- [Redis](https://redis.dev.zaaksysteem.nl/) (temporary storage for session data)
- [Mailhog](https://mailhog.dev.zaaksysteem.nl/) (development web-mail server)

## Settings

Editorsettings are, when possible, described in the editorconfig. Here are some
advised settings for some common editors,

### Visual Studio Code (VSCode)

To use VSCode efficiently, we've added workspace files to this repository and,
extension recommendations and pre-configured settings in all (sub-)repositories.

These settings will enable automatic formatting using `black` and sorting of
imports using `isort` on save, and will run the `ruff` linter while typing.

#### Recommended Settings

Use the provided `.code-workspace` files to get the recommended VSCode settings.

## Contributing

Please read [CONTRIBUTING](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/CONTRIBUTING.rst)
for details on our code of conduct, and the process for submitting pull requests to us.

## License

Copyright (c) 2020, Minty Team and all persons listed in
[CONTRIBUTORS](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/CONTRIBUTORS.rst)

This project is licensed under the EUPL, v1.2. See the
[EUPL-1.2.txt](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/LICENSES/EUPL-1.2.txt)
in the `LICENSES` directory for details.

Selected components are licensed under Commercial license from Material-UI SAS. See the
[MUI-Commercial.txt](https://gitlab.com/xxllnc/zaakgericht/zaken/start/-/blob/master/LICENSES/MUI-Commercial.txt)
in the `LICENSES` directory for details.
