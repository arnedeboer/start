{{/*
Expand the name of the chart.
*/}}
{{- define "zaaksysteem.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "zaaksysteem.fullname" -}}
{{- printf "%s-%s" .Release.Name "zaaksysteem" | trunc 63 | trimSuffix "-" }}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "zaaksysteem.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "zaaksysteem.labels" -}}
helm.sh/chart: {{ include "zaaksysteem.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "zaaksysteem.affinity" -}}
podAntiAffinity:
  preferredDuringSchedulingIgnoredDuringExecution:
    - weight: 100
      podAffinityTerm:
        topologyKey: topology.kubernetes.io/zone
        labelSelector:
          matchExpressions:
          - key: app.kubernetes.io/name
            operator: In
            values:
              - "{{- .name }}"
          - key: app.kubernetes.io/instance
            operator: In
            values:
              - "{{ .Release.Name }}"
{{- end }}

{{- define "zaaksysteem.default_security_context" -}}
capabilities:
  drop:
    - ALL
readOnlyRootFilesystem: true
allowPrivilegeEscalation: false
{{- end }}

{{- define "zaaksysteem.default_pod_security_context" -}}
runAsNonRoot: true
runAsUser: 1000
{{- end }}
