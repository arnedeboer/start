{{- define "zaaksysteem.ingress.paths" -}}
paths:
# Fallback: perl-api
- path: /
  pathType: Prefix
  backend:
    service:
      name: {{ .Values.backend.perl_api.name }}
      port:
        number: {{ .Values.backend.perl_api.service.port }}
- path: /api/v2/admin
  pathType: Prefix
  backend:
    service:
      name: {{ .Values.backend.http.admin.name }}
      port:
        number: {{ .Values.backend.http.admin.service.port }}
- path: /api/v2/cm
  pathType: Prefix
  backend:
    service:
      name: {{ .Values.backend.http.cm.name }}
      port:
        number: {{ .Values.backend.http.cm.service.port }}
- path: /api/v2/communication
  pathType: Prefix
  backend:
    service:
      name: {{ .Values.backend.http.communication.name }}
      port:
        number: {{ .Values.backend.http.communication.service.port }}
- path: /api/v2/document
  pathType: Prefix
  backend:
    service:
      name: {{ .Values.backend.http.document.name }}
      port:
        number: {{ .Values.backend.http.document.service.port }}
- path: /api/v2/file
  pathType: Prefix
  backend:
    service:
      name: {{ .Values.backend.http.document.name }}
      port:
        number: {{ .Values.backend.http.document.service.port }}
- path: /api/v2/geo
  pathType: Prefix
  backend:
    service:
      name: {{ .Values.backend.http.geo.name }}
      port:
        number: {{ .Values.backend.http.geo.service.port }}
- path: /apidocs
  pathType: Prefix
  backend:
    service:
      name: {{ .Values.backend.swagger_ui.name }}
      port:
        number: {{ .Values.backend.swagger_ui.service.port }}
- path: /(main|admin|my\-pip|external\-components|objection\-app|meeting|intern|mor|pdc|vergadering)
  pathType: ImplementationSpecific
  backend:
    service:
      name: {{ $.Values.frontend.frontend_mono.name }}
      port:
        number: {{ $.Values.frontend.frontend_mono.service.port }}
- path: /(assets|css|data\/|doc|error_pages|examples|favicon\.ico|flexpaper|fonts|html|images|js|offline|partials|pdf\.js-with-viewer|robots\.txt|tpl|webodf)
  pathType: ImplementationSpecific
  backend:
    service:
      name: {{ $.Values.frontend.frontend_client.name }}
      port:
        number: {{ $.Values.frontend.frontend_client.service.port }}
{{- end }}