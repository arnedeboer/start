{{ define "zaaksysteem.log4perl.conf" }}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
log4perl.rootLogger = {{ $.Values.global.logLevel }}, Syncer

log4perl.oneMessagePerAppender = 1

log4perl.appender.Screen = Log::Log4perl::Appender::Screen
log4perl.appender.Screen.stderr = 0
log4perl.appender.Screen.utf8 = 1
log4perl.appender.Screen.autoflush = 1
log4perl.appender.Screen.layout = Log::Log4perl::Layout::JSON
log4perl.appender.Screen.layout.field.message = %m{chomp}
log4perl.appender.Screen.layout.field.timestamp = %d{yyyy-MM-dd HH:mm:ss}
log4perl.appender.Screen.layout.field.class = %C
log4perl.appender.Screen.layout.field.sub = %M{1}
log4perl.appender.Screen.layout.field.line = %L{1}
log4perl.appender.Screen.layout.field.level = %p
log4perl.appender.Screen.layout.field.zs_component = {{ $app.name }}
log4perl.appender.Screen.layout.field.zs_pod_name = %H
log4perl.appender.Screen.layout.include_mdc = 1
log4perl.appender.Screen.layout.name_for_mdc = req

log4perl.appender.Syncer = Log::Log4perl::Appender::Synchronized
log4perl.appender.Syncer.appender = Screen
{{- end }}