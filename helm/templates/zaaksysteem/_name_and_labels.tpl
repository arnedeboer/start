{{/*
Function to create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
give $ as the first parameter and name as the second
Example: {{ include "fullname" (list $ .Values.apps.rootOverviewPage.name) }}
*/}}
{{- define "fullname" -}}
{{- $ := index . 0 }}
{{- $name := index . 1 }}
{{- printf "%s-%s" $.Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "selectorLabels" -}}
{{- $ := index . 0 -}}
{{- $name := index . 1 -}}
app.kubernetes.io/name: {{ $name | quote }}
app.kubernetes.io/instance: {{ $.Release.Name | quote}}
{{- end }}

{{/*
Common labels
*/}}
{{- define "common.labels" -}}
{{- $ := index . 0 -}}
{{- $name := index . 1 -}}
helm.sh/chart: {{ include "chart" $ }}
{{ include "selectorLabels" (list $ $name) }}
{{- if $.Chart.AppVersion }}
app.kubernetes.io/version: {{ $.Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ $.Release.Service }}
{{- end }}


{{/*
Create the name of the service account to use
*/}}
{{- define "serviceAccountName" -}}
{{- $ := index . 0 -}}
{{- $name := index . 1 -}}
{{- if $.Values.global.serviceAccount.create }}
{{- default ( include "fullname" (list $ $name)) $.Values.global.serviceAccount.name }}
{{- else }}
{{- default "default" $.Values.global.serviceAccount.name }}
{{- end }}
{{- end }}