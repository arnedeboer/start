{{/*
ServiceMonitor template
*/}}
{{- define "default.servicemonitor" -}}
{{- $ := index . 0 }}
{{- $app := index . 1 }}
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: {{ include "fullname" (list $ $app.name) }}
  namespace: {{ $.Release.Namespace }}
  labels:
    {{- include "common.labels" (list $ $app.name) | nindent 4 }}
spec:
  endpoints:
    - interval: 30s
      port: metrics
  namespaceSelector:
    matchNames:
      - {{ $.Release.Namespace }}
  selector:
    matchLabels:
      {{- include "selectorLabels" (list $ $app.name) | nindent 6 }}
{{- end }}

