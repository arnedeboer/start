# Helm Charts

This directory contains alle the helm charts that are used to deploy
Zaaksysteem to the Xxllnc Cloud Platform (XCP)

## manual deploy

To manually deploy to the `zaaksysteem-development-manual` namespace the first
thing todo is create a var/values.yaml file and a var/sec.yaml file

example of var/values.yaml

```yaml
.image_tag: &image_tag
  tag: "development-latest"

global:
  ingress:
    enabled: true
    host: development-manual.zaaksystemen.nl
  configmaps:
    enabled: true
  statsd:
    enabled: false
  instance_config_type: file

backend:
  consumer_queue_runner:
    enabled: false
    image: *image_tag
  consumers:
    cm:
      enabled: true
      image: *image_tag
    communication:
      enabled: true
      image: *image_tag
    document:
      enabled: true
      image: *image_tag
    geo:
      enabled: true
      image: *image_tag
    logging:
      enabled: true
      image: *image_tag
  document_converter:
    enabled: false
    image: *image_tag
  http:
    admin:
      enabled: true
      image: *image_tag
    cm:
      enabled: true
      image: *image_tag
    communication:
      enabled: true
      image: *image_tag
    document:
      enabled: true
      image: *image_tag
    geo:
      enabled: true
      image: *image_tag
  olo_syncer:
    enabled: true
    image: *image_tag
  perl_api: &perl-api
    enabled: true
    image: *image_tag
    resources:
      limits:
        cpu: 1
        memory: 4Gi
      requests:
        cpu: 1
        memory: 4Gi
  perl_api_internal: *perl-api
  queue_cleaner:
    enabled: false
    image: *image_tag
  schedule_runner:
    enabled: false
    image: *image_tag
  swagger_ui:
    enabled: true
  virus_scanner:
    enabled: false
    image: *image_tag
  smtp_relay:
    enabled: false

frontend:
  frontend_client:
    enabled: true
    image: *image_tag
  frontend_mono:
    enabled: true
    image: *image_tag
```

second create a the var/sec.yaml file. Use the example below and add all the
secrets.

var/sec.yaml example:

```yaml
global:
  instances:
    - hostname: "development-manual.zaaksystemen.nl"
      database:
        name: <name>
        host: "<database host>"
        username: "<user_name>"
        password: "<password>"
      database_ro:
        name: <name>
        host: "<database host>"
        username: "<user_name>"
        password: "<password>"
  redis:
    configuration:
      host: "<configuration host>"
      port: <port>
    session:
      host: "<session host>"
      port: <port>
  rabbitmq:
    username: "<user_name>"
    password: <password>
    host: <host>
    port: <port>
    virtualhost: <virtual host>
    exchange: <ex>
    event_exchange: <event>
  email:
    dkim_selector: "<dkim selector>"
    dkim_private_key: "--- PRIVATE KEY GOES HERE ---"
  filestores:
    - name: "<name>"
      type: "<type>"
      bucket: "<bucket>"
      endpoint_url: "<endpoint>"
      region: "<aws-region>"
      sts_region: "<region>"
      signature_version: <X>
  bucket_host: <host>
backend:
  consumers:
    cm:
      serviceAccount:
        annotations:
          eks.amazonaws.com/role-arn: "<role>"
    communication:
      serviceAccount:
        annotations:
          eks.amazonaws.com/role-arn: "<role>"
    document:
      serviceAccount:
        annotations:
          eks.amazonaws.com/role-arn: "<role>"
  http:
    cm:
      serviceAccount:
        annotations:
          eks.amazonaws.com/role-arn: "<role>"
    communication:
      serviceAccount:
        annotations:
          eks.amazonaws.com/role-arn: "<role>"
    document:
      serviceAccount:
        annotations:
          eks.amazonaws.com/role-arn: "<role>"
  perl_api:
    serviceAccount:
      annotations:
        eks.amazonaws.com/role-arn: "<role>"

```

After creating both files make sure that:

* VPN connection is active
* kubectl is installed and configured
* helm is installed and configured

Use the following command to deploy zaaksysteem:

```bash
helm upgrade --install --wait -f var/values.yaml -f var/sec.yaml --debug zaaksysteem-development --namespace zaaksysteem-development-manual .
```

Before deploy it is possible to check the template that is created by helm.

```bash
helm template -f var/values.yaml -f var/sec.yaml . > deployChart.yaml
```

After testing unistall with the following command:

```bash
helm delete --namespace zaaksysteem-development-manual zaaksysteem-development 
```
