#!/usr/bin/env python3.11

import psycopg2 as pg
from pathlib import Path
import time
import argparse
import json
import logging
from datetime import datetime
import sys
import threading
from queue import Queue
import sqlparse
from colorlog import ColoredFormatter

# Parse command line arguments
def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=Path, action='append', help='sql file(s) to run, otherwise checks directory for sql files')
    parser.add_argument('--directory', type=Path, help='directory to run sql files from, defaults to script directory')
    parser.add_argument('-d', '--database', type=str, help="database(s) to run queries on, if not specified runs on all database on the server", action='append')
    parser.add_argument('-c', '--credentials', type=Path, help="credentials file, json format, defaults to credentials.json in script directory")
    parser.add_argument('-n', '--namespace', type=str, help="namespace to run queries on", required=True, choices=['zaaksysteem-development', 'zaaksysteem-mlb', 'zaaksysteem-preprod', 'zaaksysteem-gov', 'zaaksysteem-com', 'gov', 'gov1', 'com', 'prd'])
    parser.add_argument('-w', '--workers', type=int, default=50, help="number of worker threads to use")
    parser.add_argument('-l', '--loglevel', default='INFO', choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'])
    return parser.parse_args()

# Setup logger
def setup_logger() -> logging.Logger:
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    NAMESPACE_LOG_DIR = LOG_DIR / f"{namespace}"
    NAMESPACE_LOG_DIR.mkdir(exist_ok=True, parents=True)
    file_handler = logging.FileHandler(str(NAMESPACE_LOG_DIR / f"{today}.log"))
    stream_handler = logging.StreamHandler(sys.stdout)
    file_handler.setLevel(logging.DEBUG)
    stream_handler.setLevel(arguments.loglevel)
    file_formatter = logging.Formatter('[%(asctime)s] [%(levelname)-7s]: %(message)s', datefmt='%d-%m-%Y %H:%M:%S')
    stream_formatter = ColoredFormatter(
        '%(log_color)s[%(asctime)s] [%(levelname)-7s]: %(message)s%(reset)s',
        datefmt='%d-%m-%Y %H:%M:%S',
        log_colors={
            'DEBUG':    'white',
            'INFO':     'green',
            'WARNING':  'yellow',
            'ERROR':    'red',
            'CRITICAL': 'red,bg_white',
        })
    file_handler.setFormatter(file_formatter)
    stream_handler.setFormatter(stream_formatter)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)
    return logger

# Get credentials
def get_credentials() -> dict:
    CREDENTIALS_FILE = arguments.credentials if arguments.credentials else SCRIPT_DIR / 'credentials.json'
    logger.debug(f"Got credentials file {CREDENTIALS_FILE}")

    # Check if credentials file exists
    if not CREDENTIALS_FILE.exists():
        logger.critical(f"Credentials file {CREDENTIALS_FILE} does not exist, or not specified with -c --credentials")
        credential_template = {
            namespace: {
                "host": "localhost",
                "port": 5432,
                "user": "postgres",
                "password": "PASSWORD"
            }
        }
        with open(CREDENTIALS_FILE, 'w') as f:
            json.dump(credential_template, f, indent=2)
        logger.info(f"Created credentials file {CREDENTIALS_FILE}, add credentials")
        exit(1)

    # Load credentials from file
    with open(CREDENTIALS_FILE, 'r') as f:
        creds: dict = json.load(f)

    # Check if namespace credentials are in the file
    if namespace not in creds:
        logger.critical(f"Credentials file {CREDENTIALS_FILE} does not contain credentials for namespace {namespace}, please add them")
        creds[namespace] = {
                "host": "localhost",
                "port": "5432",
                "user": "postgres",
                "password": "password"
        }
        with open(CREDENTIALS_FILE, 'w') as f:
            json.dump(creds, f, indent=2)
            exit(1)

    return creds

def find_sql_files() -> list[Path]:
    if not arguments.directory:
        files: list[Path] = arguments.file if arguments.file else list(SCRIPT_DIR.glob('*.sql'))
    else:
        files: list[Path] = list(arguments.directory.glob('*.sql'))

    if not files:
        logger.critical(f"No sql files found, checked --directory, --file and script directory")
        exit(1)
    return files

def get_database_list() -> list[str]:
    if not arguments.database:
        try:
            conn = pg.connect(database='template1', **creds.get(namespace))
            cur = conn.cursor()
        except pg.OperationalError as e:
            logger.critical(f"Could not connect to database {namespace}:\n{e}")
            logger.critical(f"Did you connect to the VPN? And specified the correct credentials for {namespace} in the credentials file?")
            exit(1)
        
        logger.info(f"Connected to template1 on {namespace}")

        GET_ALL_DATABASES_QUERY = "SELECT datname FROM pg_database where datconnlimit=-1 and datname not in ('template1', 'postgres', 'template0', 'rdsadmin') order by 1 ASC;"
        logger.debug(f"Running query to get all databases")
        cur.execute(GET_ALL_DATABASES_QUERY)
        databases: list[str] = [database_name[0] for database_name in cur.fetchall()]
    else:
        databases: list[str] = arguments.database
    return databases

def split_file_into_statements(file: Path) -> list[str]:
    file_contents = file.read_text()

    # split file into statements using sqlparse module
    statements = sqlparse.split(file_contents)
    logger.info(f"{file.name} - contains {len(statements)} statements")

    # return a list [file, *statements]
    return [file, [*statements]]

def get_connection_to_database(database_name: str) -> None:
    try:
        conn = pg.connect(database=database_name, **creds.get(namespace))
        return conn
    except pg.OperationalError as e:
        logger.error(f"Could not connect to database {database_name}:\n{e}")
        return None
    
def run_statement_on_database(statement: str, database_name: str, cur, log_file_handle) -> bool:
    # start statement timer
    start_statement = time.perf_counter()

    # write statement to run to database logfile
    log_file_handle.write(f"{statement}\n\n")
    
    # execute statement, catch potential exceptions
    try:
        cur.execute(statement)
    except pg.Error as e:
        logger.error(f"Below statement threw exception:\n{statement}\n\n{e}")
        log_file_handle.write(f"##### EXECUTION OF FILE FAILED ON THIS STATEMENT ^^ #####\n\n")

        # if statement throws exception return False to indicate that statement failed
        return False
    
    # no exception, so continue to write results to database logfile

    # need to check if statement returns rows or just statusmessage

    # if statement returns less than 1 row (0 or -1) 
    # OR 
    # statusmessage does not start with SELECT 
    # THEN
    # write statusmessage to database logfile

    if cur.rowcount < 1 or not cur.statusmessage.startswith("SELECT"):
        log_file_handle.write(f'{cur.statusmessage}\n\n')
    
    # if rowcount > 1 it means that multiple rows are returned so use fetchall
    elif cur.rowcount > 1:
        try:
            for row in cur.fetchall():
                log_file_handle.write(f"{row}\n")
            log_file_handle.write("\n")
        except pg.ProgrammingError as e:
            # handle edge case where rowcount > 1 but no actual rows are returned
            log_file_handle.write(f'{cur.statusmessage}\n\n')

    elif cur.rowcount == -1 and cur.statusmessage.startswith('SELECT'):
        log_file_handle.write(f'{cur.statusmessage}\n\n')

    # else we only got 1 result and we can simply use fetchone
    else:
        try:
            log_file_handle.write(f"{cur.fetchone()}\n\n")
        except pg.ProgrammingError as e:
            logger.error(f"Something went wrong, row_count: {cur.rowcount}, statusmessage: {cur.statusmessage}\nTried cursor.fetchone() but got no results\n{e}")
            log_file_handle.write(f"{cur.statusmessage}\n\n")

    # end statement timer
    end_statement = time.perf_counter()
    logger.debug(f"[worker-{worker_index}]: finished {statement[:30]} ... on {database_name} in {(end_statement - start_statement)*1000:.2f} ms")

    # finally return True to indicate statement succeeded
    return True

def run_file_on_database(conn, database_name: str, file: Path, statements: list[str], log_file_path: Path) -> bool:
    # check if there is an entry in the succes dict, if not add it
    if database_name not in success_dict:
        success_dict[database_name] = {file.name: -1}
    
    # check if the file was already successfully run on the database, return True to indicate success
    if success_dict[database_name].get(file.name) == 1:
        logger.warning(f"skipping {file.name} on {database_name} as it already ran succesfully {file.absolute()}")
        return True
    
    # create cursor on the database
    with conn.cursor() as cur:

        # open database log path for writing statement results
        with open(log_file_path, 'a') as f:
            # start file timer
            file_start = time.perf_counter()

            # loop over statements and execute them
            for statement in statements:
                statement_ran_succesfully = run_statement_on_database(statement, database_name, cur, f)

                # handle failed statement
                if not statement_ran_succesfully:
                    file_end = time.perf_counter()
                    logger.debug(f"file: {file.name} failed after {(file_end - file_start)*1000:.2f} ms")
                    logger.error(f"file: {file.name} failed on database {database_name}, rolling back")
                    run_statement_on_database("ROLLBACK;", database_name, cur, f)
                    return False
            file_end = time.perf_counter()
            logger.debug(f"Finished {file.name} on database {database_name} in {(file_end - file_start)*1000:.2f} ms")
    return True

def add_to_success_dict(database_name: str, file: Path):
    # check if database already exists in success_dict
    if database_name in success_dict:
        # add file success to database dict
        success_dict[database_name][file.name] = True
    
    # else no files had success on this database yet, so create the success dict for this database
    else:
        success_dict[database_name] = {file.name: True}

def add_to_failed_dict(database_name: str, file: Path):
    # check if the database already exists in failed_dict
    if database_name in failed_dict:
        # add file fail to database dict
        failed_dict[database_name][file.name] = True

    # else no files failed yet on this database, so create failed dict for this database
    else:
        failed_dict[database_name] = {file.name: True}

def worker(worker_index: int):
    logger.info(f"[worker-{worker_index}]: Started")

    # get a database_name and database_log_directory from the database_queue until queue is empty
    while not database_queue.empty():
        database_name, database_log_dir = database_queue.get()
        
        # setup connection to database
        logger.debug(f"[worker-{worker_index}]: connecting to {database_name}")
        conn = get_connection_to_database(database_name)
        if not conn:
            logger.error(f"Could not connect to database {database_name}, correct database name?")
            continue
        logger.debug(f"[worker-{worker_index}]: connected to {database_name}")

        # start all files timer
        files_start = time.perf_counter()

        # loop over all files and their statements
        for file, statements in split_files:
            logger.info(f"[worker-{worker_index}]: start with {file.name} on {database_name}")

            # set log_path for the file on this database
            log_path = database_log_dir / f"{file.name}.txt"

            # run file on the database
            ran_succesfully = run_file_on_database(conn, database_name, file, statements, log_path)

            # handle success
            if ran_succesfully:
                add_to_success_dict(database_name, file)

                # if it was previously in the failed dict, remove it
                # returns None if the database was not in the failed dict
                failed_files: dict = failed_dict.get(database_name)

                # if there are failed files for the database and the file is in the failed dict, remove it
                if failed_files and failed_files.get(file.name, False):
                    failed_files.pop(file.name)
                    logger.info(f"[worker-{worker_index}]: Previously failed file: {file.name} now succeeded on database: {database_name}")

                    # if all failed files are now removed, remove the database from the failed_dict
                    if not failed_dict[database_name]:
                        failed_dict.pop(database_name)
                        logger.info(f"[worker-{worker_index}]: All failed files now succeeded on database: {database_name}")

            # handle failure
            else:
                add_to_failed_dict(database_name, file)
        
        # handle finished work
        files_end = time.perf_counter()
        logger.info(f"[worker-{worker_index}]: finished all files on {database_name} in {(files_end - files_start)*1000:.2f} ms")

if __name__ == '__main__':
    # set global variables
    SCRIPT_DIR = Path(__file__).parent.absolute()
    today = datetime.today().strftime("%d-%m-%Y")
    LOG_DIR = SCRIPT_DIR / f'logs/{today}'
    LOG_DIR.mkdir(exist_ok=True, parents=True)

    # get the command line arguments
    arguments = parse_args()
    namespace = arguments.namespace

    # setup the global logger
    logger = setup_logger()
    logger.debug(f"Starting database update script. Got arguments:\n{arguments.__dict__}")

    # get database credentials
    creds = get_credentials()

    # find the sql files to be executed
    files = find_sql_files()
    files_string = '\n'.join([file.name for file in files])

    # get database list on which to execute the sql files
    databases = get_database_list()
    database_count = len(databases)

    # hold execution till user agrees on parameters
    logger.info(f"Found {database_count} databases to run {len(files)} file(s) on\nfiles:\n{files_string}")
    input('Press enter to continue...')

    # split files into statements
    split_files: list[list[str]] = [split_file_into_statements(file) for file in files]

    # create and fill the database queue
    database_queue = Queue()
    for database in databases:
        database_log_dir = LOG_DIR / f"{namespace}/{database}"
        database_log_dir.mkdir(exist_ok=True, parents=True)
        database_queue.put((database, database_log_dir))

    # adjust worker_count if needed
    worker_count = arguments.workers
    if database_count < worker_count:
        worker_count = database_count
        logger.warning(f"Adjusted worker count to {worker_count} since there are only {database_count} databases to work on.")

    # setup succes and fail paths
    FAILED_MIGRATIONS_PATH = LOG_DIR / f"{namespace}/failed_migrations.json"
    SUCCESSFUL_MIGRATIONS_PATH = LOG_DIR / f"{namespace}/successful_migrations.json"

    # load the success_dict from file if it exists, otherwise create empty dict
    if not SUCCESSFUL_MIGRATIONS_PATH.exists():
        success_dict = {}
    else:
        with open(SUCCESSFUL_MIGRATIONS_PATH, 'r') as f:
            success_dict = json.load(f)

    # load the failed_dict from file if it exists, otherwise create empty dict
    if not FAILED_MIGRATIONS_PATH.exists():
        failed_dict = {}
    else:
        with open(FAILED_MIGRATIONS_PATH, 'r') as f:
            failed_dict = json.load(f)
        
    # start all the workers and timer
    migration_start = time.perf_counter()
    threads = []
    for worker_index in range(worker_count):
        thread = threading.Thread(target=worker, args=(worker_index,))
        thread.start()
        threads.append(thread)

    # wait for all workers to finish
    for thread in threads:
        thread.join()

    # stop timer, log results, save successful and failed migrations and exit
    migration_end = time.perf_counter()
    logger.info(f"Finished running {len(files)} file(s) on {database_count} database(s) in {migration_end - migration_start:.2f} seconds")
    with open(SUCCESSFUL_MIGRATIONS_PATH, 'w') as f:
        json.dump(success_dict, f, indent=2)

    with open(FAILED_MIGRATIONS_PATH, 'w') as f:
        json.dump(failed_dict, f, indent=2)