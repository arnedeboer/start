# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import logging
from .constants import (
    CASE_LEGACY_PREFIX,
    CASE_PREFIX,
    EXPORT_FILE_PREFIX,
    SUBJECT_RELATION_PREFIX,
    TIMELINE_EXPORT_PREFIX,
)
from minty.exceptions import CQRSException
from minty_amqp.consumer import BaseHandler

logger = logging.getLogger(__name__)


class CaseManagementBaseHandler(BaseHandler):
    @property
    def domain(self):
        return "zsnl_domains.case_management"


class CaseCreatedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.CaseCreated"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        command_instance.enqueue_subcases(case_uuid=event.entity_id)
        command_instance.enqueue_documents(case_uuid=event.entity_id)
        command_instance.enqueue_subjects(case_uuid=event.entity_id)
        command_instance.enqueue_emails(case_uuid=event.entity_id)
        command_instance.transition_case(case_uuid=event.entity_id)


class CaseAttributesChangedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [
            "zsnl.v2.legacy.Case.CaseCustomFieldsUpdated",
            CASE_PREFIX + "CustomFieldSync",
        ]

    def handle(self, event):
        changes = event.format_changes()
        custom_fields = changes["custom_fields"]
        command_instance = self.get_command_instance(event)

        if event.event_name == "CustomFieldSync":
            custom_fields_updated = {}
            for cf in custom_fields:
                custom_fields_updated[cf] = []
                custom_fields_updated[cf].append(
                    custom_fields[cf].get("value")
                )
            custom_fields = custom_fields_updated

        try:
            command_instance.synchronize_relations_for_case(
                case_uuid=str(event.entity_id), custom_fields=custom_fields
            )
        except CQRSException as e:
            self.logger.warning(
                f"Error during relation sync; ignored: {e}", exc_info=True
            )


class CaseV2AttributeChangedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [CASE_PREFIX + "CustomFieldUpdated"]

    def _remove_relationship_type_wrapper(self, value):
        # when retrieving the case custom_fields, a wrapper is added around
        # the field value. This does not match with the input format of
        # synchronize_relations_for_case cmd. So it is removed here to match
        # the expected input.
        result = value
        if (
            "type" in value
            and value["type"] == "relationship"
            and isinstance(value["value"], dict)
        ):
            result = value["value"]
        return result

    def handle(self, event):
        changes = event.format_changes()

        # Need to apply transformation of the value to a json dump array
        # so it matches the exepected input for synchronize_relations_for_case
        custom_fields = {
            key: [json.dumps(self._remove_relationship_type_wrapper(value))]
            for key, value in changes["custom_fields"].items()
        }
        command_instance = self.get_command_instance(event)

        try:
            command_instance.synchronize_relations_for_case(
                case_uuid=str(event.entity_id), custom_fields=custom_fields
            )
        except CQRSException as e:
            self.logger.warning(
                f"Error during relation sync; ignored: {e}", exc_info=True
            )


class SubcasesEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.SubcasesEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.create_subcases(
            case_uuid=event.entity_id,
            queue_ids=list(
                json.loads(changes["enqueued_subcases_data"]).keys()
            ),
        )


class DocumentsEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.DocumentsEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.generate_documents(
            case_uuid=event.entity_id,
            queue_ids=list(
                json.loads(changes["enqueued_documents_data"]).keys()
            ),
        )


class EmailsEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.EmailsEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.generate_emails(
            case_uuid=event.entity_id,
            queue_ids=list(json.loads(changes["enqueued_emails_data"]).keys()),
        )


class SubjectsEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return ["zsnl.v2.zsnl_domains_case_management.Case.SubjectsEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance.generate_subjects(
            case_uuid=event.entity_id,
            queue_ids=list(
                json.loads(changes["enqueued_subjects_data"]).keys()
            ),
        )


class SubjectRelationCreatedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [SUBJECT_RELATION_PREFIX + "SubjectRelationCreated"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        if changes.get("send_confirmation_email"):
            # Send notification for employee/person/organization
            # when send_confirmation_email value is True
            # in the changes of SubjectRelationCreated event
            command_instance.enqueue_subject_relation_email(
                subject_relation_uuid=event.entity_id
            )
        # Sync subject and subject extern for the case
        if changes.get("case"):
            command_instance.synchronize_subject(
                case_uuid=changes["case"]["id"]
            )


class SubjectRelationEmailEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [SUBJECT_RELATION_PREFIX + "SubjectRelationEmailEnqueued"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        command_instance.send_subject_relation_email(
            subject_relation_uuid=event.entity_id
        )


class CaseAssigneeChangedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [CASE_PREFIX + "CaseAssigneeChanged"]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        if changes["send_email_to_assignee"]:
            command_instance = self.get_command_instance(event)
            command_instance.enqueue_case_assignee_email(
                case_uuid=event.entity_id
            )


class CaseAssigneeEmailEnqueuedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [CASE_PREFIX + "CaseAssigneeEmailEnqueued"]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        queue_id = changes["assignee_email_queue_id"]
        command_instance = self.get_command_instance(event)
        command_instance.send_case_assignee_email(
            case_uuid=event.entity_id, queue_id=queue_id
        )


class TimelineExportRequestedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [TIMELINE_EXPORT_PREFIX + "TimelineExportRequested"]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        command_instance = self.get_command_instance(event)
        command_instance.run_export(
            uuid=changes["uuid"],
            type=changes["type"],
            period_start=changes["period_start"],
            period_end=changes["period_end"],
        )


class ObjectActionTriggeredRequestedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [CASE_LEGACY_PREFIX + "ObjectActionTriggered"]

    def handle(self, event):
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        active_status: bool = (
            changes["system_attribute_changes"].get("status", "active")
            == "active"
        )
        self.get_command_instance(event).custom_object_action_trigger(
            case_uuid=event.entity_id,
            action_type=changes["action_type"],
            relation_field_magic_string=changes["attribute_magic_string"],
            custom_object_fields=changes["attribute_changes"],
            status=active_status,
        )


class SubjectRelationChangedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [
            SUBJECT_RELATION_PREFIX + "SubjectRelationDeleted",
            SUBJECT_RELATION_PREFIX + "SubjectRelationUpdated",
        ]

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        entity_data = event.entity_data

        # Sync subject and subject extern for the case
        if entity_data.get("case"):
            command_instance.synchronize_subject(
                case_uuid=entity_data["case"]["id"]
            )


class ExportFileDownloadedHandler(CaseManagementBaseHandler):
    @property
    def routing_keys(self):
        return [EXPORT_FILE_PREFIX + "ExportFileDownloaded"]

    def handle(self, event):
        command_instance = self.get_command_instance(event)

        command_instance.export_file_increase_download_counter(
            export_file_uuid=event.entity_id
        )
