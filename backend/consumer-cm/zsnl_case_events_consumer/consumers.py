# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .handlers import (
    CaseAssigneeChangedHandler,
    CaseAssigneeEmailEnqueuedHandler,
    CaseAttributesChangedHandler,
    CaseCreatedHandler,
    CaseV2AttributeChangedHandler,
    DocumentsEnqueuedHandler,
    EmailsEnqueuedHandler,
    ExportFileDownloadedHandler,
    ObjectActionTriggeredRequestedHandler,
    SubcasesEnqueuedHandler,
    SubjectRelationChangedHandler,
    SubjectRelationCreatedHandler,
    SubjectRelationEmailEnqueuedHandler,
    SubjectsEnqueuedHandler,
    TimelineExportRequestedHandler,
)
from minty_amqp.consumer import BaseConsumer


class CaseEventsConsumer(BaseConsumer):
    def _register_routing(self):
        self._known_handlers = [
            CaseAssigneeChangedHandler(self.cqrs),
            CaseAssigneeEmailEnqueuedHandler(self.cqrs),
            CaseAttributesChangedHandler(self.cqrs),
            CaseCreatedHandler(self.cqrs),
            CaseV2AttributeChangedHandler(self.cqrs),
            DocumentsEnqueuedHandler(self.cqrs),
            EmailsEnqueuedHandler(self.cqrs),
            ExportFileDownloadedHandler(self.cqrs),
            ObjectActionTriggeredRequestedHandler(self.cqrs),
            SubcasesEnqueuedHandler(self.cqrs),
            SubjectRelationChangedHandler(self.cqrs),
            SubjectRelationCreatedHandler(self.cqrs),
            SubjectRelationEmailEnqueuedHandler(self.cqrs),
            SubjectsEnqueuedHandler(self.cqrs),
            TimelineExportRequestedHandler(self.cqrs),
        ]

        self.routing_keys = []
        for handler in self._known_handlers:
            self.routing_keys.extend(handler.routing_keys)
