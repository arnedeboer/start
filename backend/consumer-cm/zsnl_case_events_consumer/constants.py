# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from typing import Final

CASE_PREFIX: Final[str] = "zsnl.v2.zsnl_domains_case_management.Case."
CASE_LEGACY_PREFIX: Final[str] = "zsnl.v2.legacy.Case."
EXPORT_FILE_PREFIX: Final[
    str
] = "zsnl.v2.zsnl_domains_case_management.ExportFile."
SUBJECT_RELATION_PREFIX: Final[
    str
] = "zsnl.v2.zsnl_domains_case_management.SubjectRelation."
TIMELINE_EXPORT_PREFIX: Final[
    str
] = "zsnl.v2.zsnl_domains_case_management.TimelineExport."
