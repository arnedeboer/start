# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import logging
import pika
import pytest
import sys
from queue_runner_v2.queue_consumer import (
    QueueConsumer,
    queue_consumer_factory,
)
from types import SimpleNamespace
from unittest import mock

# Mock classes and objects needed for Testing


class HandlerGeneric:
    """HandlerGeneric handles JSON encoded messages."""

    def __init__(self, raise_exception):
        self._raise_exception = raise_exception

    def execute(self, message, routing_key):
        if self._raise_exception:
            # logger.warning("Queue item not handled succesfully. -- nack message")
            raise Exception
        else:
            # logger.info("Queue item handled successfully. -- ack message")
            print(  # noqa: T201
                "HandlerGeneric -  Queue item handled succesfully."
            )


class PikaBlockingConnectionMock(pika.BlockingConnection):
    """Pika.BlockingConnection mock to test the consumer object."""

    def __init__(self):
        # self.is_open = True
        pass

    def channel(self):
        return ChannelMock(
            connection=self, channel_number=1, on_open_callback=""
        )

    def add_callback_threadsafe(self, callback):
        sys.stdout.write("add callback threadsafe called")
        pass

    def callbacks(self):
        pass


class ChannelMock(pika.channel.Channel):
    is_open = True

    def start_consuming(self):
        pass

    def basic_qos(self, prefetch_count: int):
        self.prefetch_count = prefetch_count
        pass

    def queue_declare(self, queue: str, auto_delete: bool, durable: bool):
        self.channel_number = 1
        self._is_open = True
        pass

    def queue_bind(self, queue: str, exchange: str, routing_key: str):
        pass

    def _validate_channel_and_callback(self, consumer_callback):
        pass

    def basic_consume(self, callback, queue: str, no_ack: bool):
        pass

    def basic_reject(self, delivery_tag):
        pass

    def basic_ack(self, delivery_tag):
        pass


class TestQueueConsumer:
    def setup_method(self):
        self.conn_mock = mock.MagicMock(auto_spec=pika.BlockingConnection)
        self.queue_consumer = QueueConsumer(connection=self.conn_mock)

    def test_create_channel(self):
        self.queue_consumer.create_channel(prefetch_count=10)
        self.conn_mock.channel().basic_qos.assert_called_once_with(
            prefetch_count=10
        )

    def test_connect_to_queue(self, caplog):
        caplog.set_level(logging.INFO)
        self.queue_consumer.create_channel(prefetch_count=10)
        queue = "Test-Queue"
        exchange = "amq.topic"
        routing_key = "mock_testing"
        handler_class = HandlerGeneric(raise_exception=False)
        self.queue_consumer.connect_to_queue(
            queue=queue,
            exchange=exchange,
            routing_key=routing_key,
            handler_class=handler_class,
        )
        assert queue in caplog.records[0].message
        assert exchange in caplog.records[0].message
        assert routing_key in caplog.records[0].message

    def test_run_method(self):
        self.queue_consumer.create_channel(prefetch_count=10)
        self.queue_consumer.start_consuming()
        pass

    @mock.patch("queue_runner_v2.queue_consumer.QueueConsumer.execute_handler")
    def test_on_message(self, mock_method):
        # init objects
        mock_channel = mock.MagicMock()
        mock_handler = mock.MagicMock()
        method_frame = SimpleNamespace(
            delivery_tag="tag", routing_key="zs.v0.some_env.some_type"
        )
        self.queue_consumer.on_message(
            channel=mock_channel,
            method_frame=method_frame,
            header_frame="",
            body="",
            args=mock_handler,
        )
        mock_method.assert_called()

    @mock.patch("queue_runner_v2.queue_consumer.QueueConsumer.nack_message")
    def test_parse_routing_key_FAIL(self, nack_mock):
        qc = QueueConsumer(connection=PikaBlockingConnectionMock())
        mock_channel = mock.MagicMock()

        method_frame = SimpleNamespace(
            delivery_tag="tag", routing_key="invalid"
        )

        qc.on_message(
            channel=mock_channel,
            method_frame=method_frame,
            header_frame="",
            body="",
            args="",
        )
        nack_mock.assert_called_with(mock_channel, "tag")

    def test_parse_message_to_json_OK(self):
        conn = PikaBlockingConnectionMock()
        queue_consumer = QueueConsumer(connection=conn)
        msg = '{"foo": "bar"}'

        res = queue_consumer.parse_json__message(msg)
        assert res

    def test_parse_message_to_json_FAIL(self):
        with pytest.raises(json.JSONDecodeError):
            msg = "\ufeff"
            self.queue_consumer.parse_json__message(msg)

        with pytest.raises(TypeError):
            msg = {"foo"}
            self.queue_consumer.parse_json__message(msg)

    @mock.patch("queue_runner_v2.queue_consumer.QueueConsumer.nack_message")
    def test_execute_handler_FAIL(self, mock_nack_method):
        h = HandlerGeneric(raise_exception=True)
        self.queue_consumer.execute_handler(
            channel="channel",
            delivery_tag="delivery tag 1",
            body='{"foo":"bar"}',
            handler_class=h,
            routing_key="testing",
        )
        mock_nack_method.assert_called_with("channel", "delivery tag 1")

    @mock.patch("queue_runner_v2.queue_consumer.QueueConsumer.ack_message")
    def test_execute_handler_OK(self, mock_ack_method):
        h = HandlerGeneric(raise_exception=False)
        self.queue_consumer.execute_handler(
            channel="channel",
            delivery_tag="delivery tag 1",
            body='{"foo":"bar"}',
            handler_class=h,
            routing_key="testing_routing",
        )
        mock_ack_method.assert_called_with("channel", "delivery tag 1")

    def test_ack_message_FAIL(self):
        mock_channel = mock.MagicMock()
        mock_connection = mock.MagicMock()
        self.queue_consumer._channel = mock_connection
        mock_channel.is_open = False

        self.queue_consumer._connection = mock_channel
        delivery_tag = "12345"
        self.queue_consumer.ack_message(
            channel=mock_channel, delivery_tag=delivery_tag
        )
        mock_connection.add_callback_threadsafe.assert_not_called()

    def test_ack_message_OK(self, capsys):
        mock_channel = mock.MagicMock()
        mock_connection = mock.MagicMock()
        self.queue_consumer._connection = mock_connection
        mock_channel.is_open = True

        delivery_tag = "12345"
        self.queue_consumer.ack_message(
            channel=mock_channel, delivery_tag=delivery_tag
        )
        mock_connection.add_callback_threadsafe.assert_called()

    def test_nack_message_FAIL(self):
        mock_channel = mock.MagicMock()
        mock_connection = mock.MagicMock()
        self.queue_consumer._connection = mock_connection
        mock_channel.is_open = False

        delivery_tag = "12345"
        self.queue_consumer.nack_message(
            channel=mock_channel, delivery_tag=delivery_tag
        )
        mock_connection.add_callback_threadsafe.assert_not_called()

    def test_nack_message_OK(self, capsys):
        mock_channel = mock.MagicMock()
        mock_connection = mock.MagicMock()
        self.queue_consumer._connection = mock_connection
        mock_channel.is_open = True

        delivery_tag = "12345"
        self.queue_consumer.nack_message(
            channel=mock_channel, delivery_tag=delivery_tag
        )
        mock_connection.add_callback_threadsafe.assert_called()

    def test_clean_thread_store(self):
        import threading

        # create queue consumer
        conn = PikaBlockingConnectionMock()
        queue_consumer = QueueConsumer(connection=conn)

        # threading function
        def run_thread():
            pass

        # start threads
        t = threading.Thread(target=run_thread)
        t.start()
        queue_consumer._thread_store.append(t)
        assert len(queue_consumer._thread_store) == 1
        queue_consumer.clean_thread_store()
        assert len(queue_consumer._thread_store) == 0


class TestQueueConsumerFactory:
    def test_queue_consumer_factory(self):
        from queue_runner_v2.handlers.config import handler_config

        h_configs = handler_config()
        connect = mock.MagicMock(spec=pika, return_value=True)
        queue_consumer = queue_consumer_factory(
            url="localhost",
            handlers=h_configs,
            heartbeats=60,
            connection_module=connect,
            number_of_threads=10,
        )
        assert isinstance(queue_consumer, QueueConsumer)
