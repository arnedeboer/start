# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import pytest
from minty.cqrs import Event
from minty.exceptions import ConfigurationConflict
from unittest import mock
from uuid import uuid4
from zsnl_domains.database.schema import ObjectData, Queue
from zsnl_perl_migration import (
    LegacyEventBroadcastMiddleware,
    LegacyEventBroadcastMiddlewareBase,
    LegacyEventQueueMiddleware,
    LegacyEventQueueMiddlewareBase,
    clear_queue_item_cache,
    get_new_values_from_event,
    get_queue_item_cache,
    is_previewable,
    select_interesting_objects,
)


class MockInfrastructure:
    def __init__(self, infrastructure: dict):
        self.infrastructure = infrastructure

    def get_infrastructure(self, infrastructure_name, context):
        return self.infrastructure[(infrastructure_name, context)]

    def get_config(self, context):
        return {
            "logging_id": "id123",
            "virus_scanner_url": "https://test_virus_scanner.dev",
        }


def test_get_uuid_cache_empty():
    queue_item_cache = get_queue_item_cache()
    assert len(queue_item_cache) == 0
    assert isinstance(queue_item_cache, list)


def test_is_previewable():
    non_previewable_items_to_test = [
        {"mimetype": "audio/aac", "extension": ".aac"},
        {"mimetype": "audio/mp3", "extension": "mp3"},
        {"mimetype": "video/mvk", "extension": ".mvk"},
        {"mimetype": "video/mp4", "extension": ".mp4"},
        {"mimetype": "application/octet-stream", "extension": ""},
        {"mimetype": "text/css", "extension": ".css"},
        {"mimetype": "application/msaccess", "extension": ""},
    ]

    for item in non_previewable_items_to_test:
        assert is_previewable(item["mimetype"], item["extension"]) is False

    previewable_items_to_test = [
        {"mimetype": "application/pdf", "extension": ".pdf"},
        {"mimetype": "text/plain", "extension": ".txt"},
    ]

    for item in previewable_items_to_test:
        assert is_previewable(item["mimetype"], item["extension"]) is True


def test_get_new_values_from_event():
    event = mock.MagicMock()
    event.changes = [
        {"key": "case_uuid", "old_value": None, "new_value": str(uuid4())},
        {"key": "mimetype", "old_value": None, "new_value": "text/plain"},
        {"key": "extension", "old_value": "", "new_value": ".txt"},
    ]

    new_values = get_new_values_from_event(event)
    assert new_values["case_uuid"] == event.changes[0]["new_value"]
    assert new_values["mimetype"] == event.changes[1]["new_value"]
    assert new_values["extension"] == event.changes[2]["new_value"]

    # empty changes
    event.changes = []
    current_changes = get_new_values_from_event(event)
    assert current_changes == {}


class TestSelectObjectsToTouch:
    def test_sort_events_case(self):
        interesting_events = [
            ("testdomain", "case_event"),
            ("testdomain", "case_event2"),
        ]
        interesting_casetype_events = [
            ("testdomain", "casetype_event"),
            ("testdomain", "casetype_event2"),
        ]

        interesting_document_events = [
            ("testdomain", "document_event"),
            ("testdomain", "document_event2"),
        ]

        interesting_case_relation_events = [
            ("testdomain", "case_relation_event"),
            ("testdomain", "case_relation_event2"),
        ]

        interesting_subject_relation_events = [
            ("case_management", "SubjectRelationCreated"),
            ("case_management", "SubjectRelationDeleted"),
        ]

        case_entity_id = str(uuid4())
        case_type_entity_id = str(uuid4())
        document_entity_id = str(uuid4())

        selected_casetouch = Event(
            domain="testdomain",
            event_name="case_event",
            context="testcontext",
            user_uuid=str(uuid4()),
            uuid=str(uuid4()),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="Case",
            entity_id=case_entity_id,
            changes={},
            entity_data={},
        )
        selected_casetype_touch = Event(
            domain="testdomain",
            event_name="casetype_event",
            context="testcontext",
            user_uuid=str(uuid4()),
            uuid=str(uuid4()),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="CaseType",
            entity_id=case_type_entity_id,
            changes={},
            entity_data={},
        )

        selected_document_taks = Event(
            domain="testdomain",
            event_name="document_event",
            context="testcontext",
            user_uuid=str(uuid4()),
            uuid=str(uuid4()),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="DocumentType",
            entity_id=document_entity_id,
            changes={},
            entity_data={},
        )

        case_relation_uuid = str(uuid4())
        case_relation_event = Event(
            domain="testdomain",
            event_name="case_relation_event",
            context="testcontext",
            user_uuid=str(uuid4()),
            uuid=str(uuid4()),
            created_date="2019-01-02",
            correlation_id="req-1235",
            entity_type="CaseRelation",
            entity_id=case_relation_uuid,
            changes={},
            entity_data={},
        )

        subject_relation_uuid = str(uuid4())
        subject_relation_event = Event(
            domain="case_management",
            event_name="SubjectRelationDeleted",
            context="testcontext",
            user_uuid=str(uuid4()),
            uuid=str(uuid4()),
            created_date="2019-01-02",
            correlation_id="req-1235",
            entity_type="SubjectRelation",
            entity_id=subject_relation_uuid,
            changes={},
            entity_data={},
        )

        event_list = [
            selected_casetouch,
            selected_casetype_touch,
            selected_document_taks,
            Event(
                domain="testdomain",
                event_name="casetype_event2",
                context="testcontext",
                user_uuid=str(uuid4()),
                uuid=str(uuid4()),
                created_date="2019-01-02",
                correlation_id="req-1234",
                entity_type="CaseType",
                entity_id=case_type_entity_id,
                changes={},
                entity_data={},
            ),
            Event(
                domain="testdomain",
                event_name="case_event2",
                context="testcontext",
                user_uuid=str(uuid4()),
                uuid=str(uuid4()),
                created_date="2019-01-02",
                correlation_id="req-1234",
                entity_type="Case",
                entity_id=case_entity_id,
                changes={},
                entity_data={},
            ),
            Event(
                domain="testdomain",
                event_name="document_event2",
                context="testcontext",
                user_uuid=str(uuid4()),
                uuid=str(uuid4()),
                created_date="2019-01-02",
                correlation_id="req-1234",
                entity_type="DocumentType",
                entity_id=document_entity_id,
                changes={},
                entity_data={},
            ),
            case_relation_event,
            subject_relation_event,
        ]
        objects_to_touch = select_interesting_objects(
            interesting_casetype_events=interesting_casetype_events,
            interesting_events=interesting_events,
            interesting_document_events=interesting_document_events,
            event_list=event_list,
            interesting_case_relation_events=interesting_case_relation_events,
            interesting_subject_relation_events=interesting_subject_relation_events,
        )

        assert objects_to_touch == {
            "case_touches": {case_entity_id: selected_casetouch},
            "case_type_touches": {
                case_type_entity_id: selected_casetype_touch
            },
            "document_tasks": {document_entity_id: selected_document_taks},
            "case_relations": {case_relation_uuid: case_relation_event},
            "subject_relations": {
                subject_relation_uuid: subject_relation_event
            },
        }
        assert (
            objects_to_touch["case_touches"][case_entity_id].uuid
            == selected_casetouch.uuid
        )
        assert (
            objects_to_touch["case_type_touches"][case_type_entity_id].uuid
            == selected_casetype_touch.uuid
        )
        assert (
            objects_to_touch["document_tasks"][document_entity_id].uuid
            == selected_document_taks.uuid
        )


class TestLegacyEventQueueMiddleware:
    """Test the legacy "queue" middleware"""

    def setup_method(self):
        self.mock_db = mock.MagicMock()
        mock_infra = {("testdb", "testcontext"): self.mock_db}
        infrastructure_factory = MockInfrastructure(mock_infra)
        self.mock_event_service = mock.MagicMock()

        middleware_class = LegacyEventQueueMiddleware(
            database_infra_name="testdb",
            interesting_events=[("testdomain", "testevent")],
            interesting_casetype_events=[
                ("testdomain", "AttributeEdited"),
                ("testdomain", "CaseTypeSetOnline"),
            ],
            interesting_document_events=[
                ("testdomain", "test_document_event")
            ],
            interesting_case_relation_events=[
                ("testdomain", "CaseRelationCreated")
            ],
            interesting_subject_relation_events=[
                ("case_management", "SubjectRelationCreated")
            ],
        )

        self.middleware = middleware_class(
            infrastructure_factory,
            event_service=self.mock_event_service,
            correlation_id="req-12345",
            domain="testdomain",
            context="localhost",
            user_uuid="bd8da4c4-65d7-11e9-b7e1-87b8a6f9602f",
        )
        self.middleware.database_infra_name = "testdb"

    def test_get_file_id(self):
        self.mock_db.execute().fetchone().id = 31337
        self.mock_db.execute().fetchone().extension = ".pdf"

        event = mock.MagicMock()
        event.entity_id = str(uuid4())
        event.context = "testcontext"

        file = self.middleware.get_file(event)
        assert file == {"id": 31337, "extension": ".pdf"}

    def test_no_interesting_events(self):
        LegacyEventQueueMiddleware(database_infra_name="testdb")

    def test_case_touch_call(self):
        test_called = False

        def test_call():
            nonlocal test_called
            test_called = True
            return

        event = Event(
            domain="testdomain",
            event_name="testevent",
            context="testcontext",
            user_uuid=str(uuid4()),
            uuid=str(uuid4()),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="Case",
            entity_id=str(uuid4()),
            changes={},
            entity_data={},
        )
        self.mock_event_service.event_list = [event]

        self.middleware(test_call)
        assert test_called
        self.mock_db.add.assert_called_once()

        (args, kwargs) = self.mock_db.add.call_args
        queue_row = args[0]
        assert isinstance(queue_row, Queue)
        assert queue_row.id == event.uuid
        assert queue_row.type == "touch_case"
        assert queue_row.status == "pending"
        assert queue_row.data == {"case_object_id": str(event.entity_id)}
        assert queue_row._metadata == {
            "disable_acl": 1,
            "require_object_model": 1,
            "target": "backend",
        }

    def test_casetype_touch_call(self):
        clear_queue_item_cache()

        test_called = False

        def test_call():
            nonlocal test_called
            test_called = True
            return

        case_type_uuid = str(uuid4())
        casetype_event1 = Event(
            domain="testdomain",
            event_name="CaseTypeSetOnline",
            context="testcontext",
            user_uuid=uuid4(),
            uuid=uuid4(),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="CaseTypeEntity",
            entity_id=case_type_uuid,
            changes={},
            entity_data={},
        )
        casetype_event2 = Event(
            domain="testdomain",
            event_name="AttributeEdited",
            context="testcontext",
            user_uuid=uuid4(),
            uuid=uuid4(),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="Attribute",
            entity_id=uuid4(),
            changes={},
            entity_data={},
        )

        self.mock_event_service.event_list = [casetype_event1, casetype_event2]
        test_called = False

        case_type_uuid2 = str(uuid4())

        mock_execute_result = mock.MagicMock()
        mock_execute_result.fetchall.return_value = [
            ObjectData(uuid=case_type_uuid2)
        ]
        self.mock_db.execute.return_value = mock_execute_result

        self.middleware(test_call)
        assert test_called

        call_args = self.mock_db.add.call_args_list

        queue_row = call_args[0][0][0]
        assert isinstance(queue_row, Queue)
        assert queue_row.status == "pending"
        assert queue_row.type == "touch_casetype"
        assert queue_row.data == {"uuid": case_type_uuid}
        assert queue_row._metadata == {
            "disable_acl": 1,
            "require_object_model": 1,
            "target": "backend",
        }

        queue_row = call_args[1][0][0]
        assert isinstance(queue_row, Queue)
        assert queue_row.status == "pending"
        assert queue_row.type == "touch_casetype"
        assert queue_row.data == {"uuid": case_type_uuid2}
        assert queue_row._metadata == {
            "disable_acl": 1,
            "require_object_model": 1,
            "target": "backend",
        }

    def test_casetype_touch_fail(self):
        clear_queue_item_cache()

        case_type_uuid = str(uuid4())
        casetype_event1 = Event(
            domain="testdomain",
            event_name="CaseTypeSetOnline",
            context="testcontext",
            user_uuid=uuid4(),
            uuid=uuid4(),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="UnknownEntity",
            entity_id=case_type_uuid,
            changes={},
            entity_data={},
        )
        self.mock_event_service.event_list = [casetype_event1]

        def inner():
            pass

        with pytest.raises(ConfigurationConflict):
            self.middleware(inner)

    def test_document_type_touch_call(self):
        test_called = False

        def test_call():
            nonlocal test_called
            test_called = True
            return

        document_event = Event(
            domain="testdomain",
            event_name="test_document_event",
            context="testcontext",
            user_uuid=uuid4(),
            uuid=uuid4(),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="DocumentType",
            entity_id=uuid4(),
            changes=[
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": str(uuid4()),
                },
                {
                    "key": "mimetype",
                    "old_value": None,
                    "new_value": "text/plain",
                },
                {"key": "extension", "old_value": "", "new_value": ".txt"},
            ],
            entity_data={},
        )

        self.middleware._process_document = mock.MagicMock()

        self.mock_event_service.event_list = [document_event]
        test_called = False
        self.middleware(test_call)
        assert test_called

        self.middleware._process_document.assert_called_once_with(
            document_event
        )

    def _mock_insert(self, **args):
        self.insert_params.append(args)

    def test_case_relation_call(self):
        self.insert_params = []
        case_relation_uuid = str(uuid4())
        case_relation_event = Event(
            domain="testdomain",
            event_name="CaseRelationReordered",
            context="testcontext",
            user_uuid=uuid4(),
            uuid=uuid4(),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="CaseRelaton",
            entity_id=case_relation_uuid,
            changes=[
                {
                    "key": "current_case_uuid",
                    "old_value": None,
                    "new_value": "fake_case_uuid",
                },
            ],
            entity_data={},
        )

        self.mock_event_service.event_list = [case_relation_event]
        test_called = False

        def test_call():
            nonlocal test_called
            test_called = True
            return

        self.middleware(test_call)
        assert test_called

        self.middleware.insert_queue_item = self._mock_insert
        with mock.patch("zsnl_perl_migration.uuid4") as mock_uuid4:
            mock_uuid4.return_value = "fake_event_uuid"
            self.middleware._touch_case_relation(case_relation_event)
            assert self.insert_params == [
                {
                    "event_context": "testcontext",
                    "event_uuid": "fake_event_uuid",
                    "object_id": "fake_case_uuid",
                    "data": {"case_object_id": "fake_case_uuid"},
                    "event_type": "update_object_relationships",
                    "label": "Case relation update",
                },
            ]

        # test touch_case_relations for CaseRelationCreated event
        case_relation_created_event = Event(
            domain="testdomain",
            event_name="CaseRelationCreated",
            context="testcontext",
            user_uuid=uuid4(),
            uuid=uuid4(),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="CaseRelaton",
            entity_id=case_relation_uuid,
            changes=[
                {
                    "key": "object1_uuid",
                    "old_value": None,
                    "new_value": "fake_uuid1",
                },
                {
                    "key": "object2_uuid",
                    "old_value": None,
                    "new_value": "fake_uuid2",
                },
                {
                    "key": "current_case_uuid",
                    "old_value": None,
                    "new_value": "fake_case_uuid",
                },
            ],
            entity_data={},
        )
        self.insert_params = []
        self.mock_event_service.event_list = [case_relation_created_event]
        with mock.patch("zsnl_perl_migration.uuid4") as mock_uuid4:
            mock_uuid4.side_effect = ["fake_uuid1", "fake_uuid2", "fake_uuid3"]
            self.middleware._touch_case_relation(case_relation_created_event)
            assert self.insert_params == [
                {
                    "event_context": "testcontext",
                    "event_uuid": "fake_uuid1",
                    "object_id": "fake_case_uuid",
                    "data": {"case_object_id": "fake_case_uuid"},
                    "event_type": "update_object_relationships",
                    "label": "Case relation update",
                },
                {
                    "event_context": "testcontext",
                    "event_uuid": "fake_uuid2",
                    "object_id": None,
                    "data": {"case_object_id": "fake_uuid1"},
                    "event_type": "touch_case",
                    "label": "Legacy zaak synchronization",
                },
                {
                    "event_context": "testcontext",
                    "event_uuid": "fake_uuid3",
                    "object_id": None,
                    "data": {"case_object_id": "fake_uuid2"},
                    "event_type": "touch_case",
                    "label": "Legacy zaak synchronization",
                },
            ]

    @mock.patch("zsnl_perl_migration.is_previewable")
    def test__process_document(self, mock_is_previewable):
        mock_self = mock.MagicMock()
        mock_self.get_file.return_value = {
            "id": 31337,
            "extension": "extension",
        }

        case_uuid = str(uuid4())

        event = mock.MagicMock()
        event.entity_id = uuid4()
        event.changes = [
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid},
            {"key": "extension", "old_value": None, "new_value": "extension"},
            {
                "key": "mimetype",
                "old_value": None,
                "new_value": "application/pdf",
            },
        ]

        mock_is_previewable.return_value = True

        LegacyEventQueueMiddlewareBase._process_document(mock_self, event)

        mock_is_previewable.assert_called_once_with(
            "application/pdf", "extension"
        )

        (args, kwargs) = mock_self.insert_queue_item.call_args_list[0]

        assert args == ()
        assert kwargs == {
            "data": {"case_object_id": case_uuid},
            "event_context": event.context,
            "event_type": "touch_case",
            "event_uuid": event.uuid,
            "label": "Legacy zaak synchronization",
            "object_id": None,
        }

        (args, kwargs) = mock_self.insert_queue_item.call_args_list[1]

        assert args == ()
        assert {k: v for k, v in kwargs.items() if k != "event_uuid"} == {
            "data": {"file_id": 31337},
            "event_context": event.context,
            "event_type": "generate_preview_pdf",
            "label": f"Generate preview PDF for file {event.entity_id}",
            "object_id": None,
        }

        (args, kwargs) = mock_self.insert_queue_item.call_args_list[2]

        assert args == ()
        assert {k: v for k, v in kwargs.items() if k != "event_uuid"} == {
            "data": {"file_id": 31337},
            "event_context": event.context,
            "event_type": "set_search_terms",
            "label": f"Extract search terms for file {event.entity_id}",
            "object_id": None,
        }

        (args, kwargs) = mock_self.insert_queue_item.call_args_list[3]

        assert args == ()
        assert {k: v for k, v in kwargs.items() if k != "event_uuid"} == {
            "data": {"file_id": 31337},
            "event_context": event.context,
            "event_type": "generate_thumbnail",
            "label": f"Generate thumbnail for file {event.entity_id}",
            "object_id": None,
        }

        mock_self.get_file.return_value = {"id": 31337, "extension": ".msg"}
        event.changes = [
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid},
            {"key": "extension", "old_value": None, "new_value": ".msg"},
            {
                "key": "mimetype",
                "old_value": None,
                "new_value": "application/octet-stream",
            },
            {"key": "accepted", "old_value": None, "new_value": True},
        ]

        mock_is_previewable.reset_mock()
        mock_self.insert_queue_item.reset_mock()
        mock_is_previewable.return_value = False
        LegacyEventQueueMiddlewareBase._process_document(mock_self, event)

        mock_is_previewable.assert_called_once_with(
            "application/octet-stream", ".msg"
        )

        assert len(mock_self.insert_queue_item.call_args_list) == 1

        event.changes = [
            {"key": "case_uuid", "old_value": None, "new_value": None},
            {"key": "extension", "old_value": None, "new_value": ".msg"},
            {
                "key": "mimetype",
                "old_value": None,
                "new_value": "application/octet-stream",
            },
            {"key": "accepted", "old_value": None, "new_value": True},
        ]

        mock_is_previewable.reset_mock()
        mock_self.insert_queue_item.reset_mock()
        mock_is_previewable.return_value = False
        LegacyEventQueueMiddlewareBase._process_document(mock_self, event)

        mock_is_previewable.assert_called_once_with(
            "application/octet-stream", ".msg"
        )

        assert len(mock_self.insert_queue_item.call_args_list) == 0

    def test__process_document_file(self):
        mock_self = mock.MagicMock()
        event = mock.MagicMock()
        event.entity_type = "File"

        assert (
            LegacyEventQueueMiddlewareBase._process_document(mock_self, event)
            is None
        )

    @mock.patch(
        "zsnl_perl_migration.LegacyEventQueueMiddlewareBase._get_database"
    )
    def test__get_case_types_for_event(self, mock_get_database):
        mock_session = mock.MagicMock()
        mock_get_database.return_value = mock_session

        event = mock.MagicMock()
        event.entity_id = "fake_entity_id"
        event.entity_type = "DocumentTemplate"
        res = self.middleware._get_case_types_for_event(event)
        assert res == []

        event.entity_type = "EmailTemplate"
        res = self.middleware._get_case_types_for_event(event)
        assert res == []

    def test_subject_relation_touch(self):
        test_called = False

        def test_call():
            nonlocal test_called
            test_called = True
            return

        case_uuid = str(uuid4())
        event = Event(
            domain="case_management",
            event_name="SubjectRelationCreated",
            context="testcontext",
            user_uuid=str(uuid4()),
            uuid=str(uuid4()),
            created_date="2019-01-02",
            correlation_id="req-1234",
            entity_type="SubjectRelation",
            entity_id=str(uuid4()),
            changes={},
            entity_data={"case": {"id": case_uuid}},
        )
        self.mock_event_service.event_list = [event]

        self.middleware(test_call)
        assert test_called
        assert len(self.mock_db.add.call_args_list) == 2

        (args, kwargs) = self.mock_db.add.call_args_list[0]
        queue_row = args[0]
        assert isinstance(queue_row, Queue)
        assert queue_row.type == "update_object_acl"
        assert queue_row.object_id == case_uuid
        assert queue_row.status == "pending"
        assert queue_row.data == {"case_object_id": case_uuid}
        assert (
            queue_row.label
            == "Legacy zaak ACL sync for changed subject_relation"
        )
        assert queue_row._metadata == {
            "disable_acl": 1,
            "require_object_model": 1,
            "target": "backend",
        }

        (args, kwargs) = self.mock_db.add.call_args_list[1]
        queue_row = args[0]
        assert isinstance(queue_row, Queue)
        assert queue_row.type == "case_update_system_attribute"
        assert queue_row.object_id is None
        assert queue_row.status == "pending"
        assert queue_row.data == {
            "case_object_id": case_uuid,
            "attributes": [
                {"name": "coordinator", "is_group": True},
                {"name": "recipient", "is_group": True},
            ],
        }
        assert queue_row.label == "Legacy zaak synchronization"
        assert queue_row._metadata == {
            "disable_acl": 1,
            "require_object_model": 1,
            "target": "backend",
        }


class TestLegacyEventBroadcastMiddleware:
    def setup_method(self):
        self.mock_amqp = mock.MagicMock()
        self.mock_message = mock.MagicMock()

        self.mock_event_service = mock.MagicMock()

        mock_infra = {("testamqp", "testcontext"): self.mock_amqp}
        infrastructure_factory = MockInfrastructure(mock_infra)

        middleware_class = LegacyEventBroadcastMiddleware(
            amqp_infra_name="testamqp",
            interesting_events=[("testdomain", "testevent")],
            interesting_casetype_events=[
                ("testdomain", "AttributeEdited"),
                ("testdomain", "CaseTypeSetOnline"),
            ],
            interesting_document_events=[
                ("testdomain", "test_document_event")
            ],
            exchange_name="test_exchange",
            message_class=self.mock_message,
        )

        self.middleware = middleware_class(
            infrastructure_factory,
            event_service=self.mock_event_service,
            correlation_id="req-12345",
            domain="testdomain",
            context="localhost",
            user_uuid="bd8da4c4-65d7-11e9-b7e1-87b8a6f9602f",
        )

    def test_no_interesting_events(self):
        LegacyEventBroadcastMiddleware(
            amqp_infra_name="testamqp",
            exchange_name="test_exchange",
            message_class=self.mock_message,
        )

    def test_publish_event(self):
        self.mock_message.reset_mock()
        self.middleware.publish_event(
            routing_key_suffix="suffix",
            context="testcontext",
            logging_id="logging",
            data={"data": "here"},
        )

        self.mock_message.assert_called_once_with(
            channel=self.mock_amqp, body='{"data": "here"}'
        )
        self.mock_message().publish.assert_called_once_with(
            routing_key="zs.v0.logging.suffix", exchange="test_exchange"
        )

    def test_middleware_call(self):
        self.mock_message.reset_mock()
        clear_queue_item_cache()

        item_cache = get_queue_item_cache()

        mock_func = mock.MagicMock()

        event1 = mock.MagicMock()
        event1.uuid = str(uuid4())
        event1.domain = "testdomain"
        event1.event_name = "testevent"
        event1.context = "testcontext"

        event2 = mock.MagicMock()
        event2.uuid = str(uuid4())
        event2.domain = "testdomain"
        event2.event_name = "CaseTypeSetOnline"
        event2.entity_type = "CaseTypeEntity"
        event2.context = "testcontext"

        item_cache.append(
            {
                "uuid": event2.uuid,
                "context": event2.context,
                "event_type": "touch_casetype",
            }
        )

        event3 = mock.MagicMock()
        event3.uuid = str(uuid4())
        event3.domain = "testdomain"
        event3.event_name = "test_document_event"
        event3.context = "testcontext"
        event3.entity_id = str(uuid4())

        case_uuid = uuid4()
        event3.changes = [
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        ]

        item_cache.append(
            {
                "uuid": event3.uuid,
                "context": event3.context,
                "event_type": "touch_case",
            }
        )

        self.mock_event_service.event_list = [event1, event2, event3]

        self.mock_message().publish.reset_mock()
        self.mock_message.reset_mock()

        assert self.mock_message().publish.call_args_list == []
        self.middleware(mock_func)

        assert self.mock_message.call_args_list == [
            mock.call(),
            mock.call(
                channel=self.mock_amqp,
                body=json.dumps(
                    {
                        "instance_hostname": "testcontext",
                        "url": f"https://testcontext/api/queue/{event1.uuid}/run?type=touch_case",
                    }
                ),
            ),
            mock.call(
                channel=self.mock_amqp,
                body=json.dumps(
                    {
                        "file_id": event3.entity_id,
                        "instance_hostname": "testcontext",
                        "url": "https://test_virus_scanner.dev/filestore",
                    }
                ),
            ),
            mock.call(
                channel=self.mock_amqp,
                body=json.dumps(
                    {
                        "instance_hostname": "testcontext",
                        "url": f"https://testcontext/api/queue/{event2.uuid}/run?type=touch_casetype",
                    }
                ),
            ),
            mock.call(
                channel=self.mock_amqp,
                body=json.dumps(
                    {
                        "instance_hostname": "testcontext",
                        "url": f"https://testcontext/api/queue/{event3.uuid}/run?type=touch_case",
                    }
                ),
            ),
        ]

        mock_func.assert_called_once_with()

        assert self.mock_message().publish.call_args_list == [
            mock.call(
                exchange="test_exchange", routing_key="zs.v0.id123.touch_case"
            ),
            mock.call(
                exchange="test_exchange", routing_key="zs.v0.id123.virus_scan"
            ),
            mock.call(
                exchange="test_exchange",
                routing_key="zs.v0.id123.touch_casetype",
            ),
            mock.call(
                exchange="test_exchange",
                routing_key="zs.v0.id123.touch_case",
            ),
        ]

    def test_publish_document_events(self):
        clear_queue_item_cache()

        mock_self = mock.MagicMock()
        mock_self.infrastructure_factory.get_config.return_value = {
            "logging_id": "logging",
            "virus_scanner_url": "http://virusscanner",
        }

        file_store_id = str(uuid4())
        mock_event = mock.MagicMock()
        mock_event.changes = [
            {"key": "store_uuid", "old_value": "", "new_value": file_store_id}
        ]

        LegacyEventBroadcastMiddlewareBase.publish_document_events(
            mock_self, mock_event
        )

        mock_self.publish_event.assert_called_once_with(
            routing_key_suffix="virus_scan",
            context=mock_event.context,
            data={
                "url": "http://virusscanner/filestore",
                "instance_hostname": mock_event.context,
                "file_id": file_store_id,
            },
            logging_id="logging",
        )

        mock_event.changes = []
        mock_self.publish_event.reset_mock()

        LegacyEventBroadcastMiddlewareBase.publish_document_events(
            mock_self, mock_event
        )

        mock_self.publish_event.assert_called_once_with(
            routing_key_suffix="virus_scan",
            context=mock_event.context,
            data={
                "url": "http://virusscanner/filestore",
                "instance_hostname": mock_event.context,
                "file_id": str(mock_event.entity_id),
            },
            logging_id="logging",
        )

    def test_publish_document_events_multiple(self):
        clear_queue_item_cache()
        cache = get_queue_item_cache()

        queue_item_uuid = str(uuid4())
        cache.append(
            {
                "uuid": queue_item_uuid,
                "context": "testcontext",
                "event_type": "test",
            }
        )

        mock_self = mock.MagicMock()
        mock_self.infrastructure_factory.get_config.return_value = {
            "logging_id": "logging",
            "virus_scanner_url": "http://virusscanner",
        }

        file_store_id = str(uuid4())
        mock_event = mock.MagicMock()
        mock_event.changes = [
            {"key": "store_uuid", "old_value": "", "new_value": file_store_id}
        ]
        mock_event.context = "testcontext"
        mock_event.entity_id = str(uuid4())

        LegacyEventBroadcastMiddlewareBase.publish_document_events(
            mock_self, mock_event
        )

        (args, kwargs) = mock_self.publish_event.call_args_list[0]

        assert args == ()

        assert kwargs == {
            "routing_key_suffix": "virus_scan",
            "context": mock_event.context,
            "data": {
                "url": "http://virusscanner/filestore",
                "instance_hostname": mock_event.context,
                "file_id": file_store_id,
            },
            "logging_id": "logging",
        }

    @mock.patch("zsnl_perl_migration.is_previewable")
    @mock.patch("zsnl_perl_migration.uuid4")
    def test__get_queue_item_cache_for_document_events(
        self, mock_uuid, mock_is_previewable
    ):
        clear_queue_item_cache()

        test_uuid = uuid4()
        mock_uuid.return_value = test_uuid
        mock_is_previewable.return_value = True

        mock_self = mock.MagicMock()
        mock_self.get_file.return_value = {
            "id": 31337,
            "extension": "extension",
        }

        case_uuid = str(uuid4())
        event = mock.MagicMock()
        event.entity_id = uuid4()
        event.changes = [
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid},
            {"key": "extension", "old_value": None, "new_value": "extension"},
            {
                "key": "mimetype",
                "old_value": None,
                "new_value": "application/pdf",
            },
        ]

        LegacyEventQueueMiddlewareBase._process_document(mock_self, event)

        queue_items = get_queue_item_cache()
        assert queue_items[0] == {
            "uuid": event.uuid,
            "context": event.context,
            "event_type": "touch_case",
        }

        assert queue_items[1] == {
            "uuid": str(test_uuid),
            "context": event.context,
            "event_type": "generate_preview_pdf",
        }

        assert queue_items[2] == {
            "uuid": str(test_uuid),
            "context": event.context,
            "event_type": "set_search_terms",
        }

        assert queue_items[3] == {
            "uuid": str(test_uuid),
            "context": event.context,
            "event_type": "generate_thumbnail",
        }
