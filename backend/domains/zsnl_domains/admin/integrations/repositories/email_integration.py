# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import json
import minty.cqrs
import minty.exceptions
from .... import ZaaksysteemRepositoryBase
from ..entities.integration import EmailIntegration
from minty.repository import Repository
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class EmailIntegrationRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "EmailIntegration"
    _events_to_calls = {
        "LoginStateSaved": "_save_internal_config",
        "RefreshTokenSaved": "_save_internal_config",
    }

    def get(self, uuid: UUID) -> EmailIntegration:
        """Retrieve an 'email' type integration."""
        query = (
            sql.select(
                schema.Interface.uuid,
                schema.Interface.name,
                schema.Interface.module,
                schema.Interface.interface_config,
                schema.Interface.internal_config,
            )
            .where(
                sql.and_(
                    schema.Interface.module.in_(
                        ["emailconfiguration", "pop3client"]
                    ),
                    schema.Interface.date_deleted.is_(None),
                )
            )
            .where(schema.Interface.uuid == uuid)
        )

        integration = self.session.execute(query).fetchone()

        if integration is None:
            raise minty.exceptions.NotFound(
                "No active email integration found with specified UUID",
                "integration/email/not_found",
            )

        return self._transform_to_entity(integration)

    def get_by_state(self, state_param: str):
        """
        Retrieve an 'email' type integration, based on the 'state' variable
        returned by the OAuth2 identity service.
        """

        decoded_state = base64.urlsafe_b64decode(state_param)
        parsed_state = json.loads(decoded_state)
        state = parsed_state["state"].encode("ascii")

        integration = self.get(parsed_state["uuid"])

        # If the state does not match what we sent, it's not a valid request
        # from the OAuth2 service.
        db_state = base64.urlsafe_b64decode(
            integration.internal_config["login_state"]
        )
        if db_state != state:
            self.logger.info(f"State mismatch: db={db_state}, request={state}")
            raise minty.exceptions.NotFound(
                "No active OAuth2 login session with that state"
                f"db={db_state}, request={state}",
                "integration/email/sequence_error",
            )

        return integration

    def _transform_to_entity(self, query_result):
        """
        Transform an email integration database row to an EmailIntegration entity
        """

        return EmailIntegration.parse_obj(
            {
                "entity_id": query_result.uuid,
                "name": query_result.name,
                "module": query_result.module,
                "user_config": query_result.interface_config,
                "internal_config": query_result.internal_config,
                # Internals:
                "_event_service": self.event_service,
            }
        )

    def _save_internal_config(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = event.format_changes()
        uuid = str(event.entity_id)

        update_statement = (
            sql.update(schema.Interface)
            .where(schema.Interface.uuid == uuid)
            .values(
                internal_config=schema.Interface.internal_config
                + changes["internal_config"]
            )
        )

        self.session.execute(update_statement)
