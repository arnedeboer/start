# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..entities.attribute import AttributeDetailSearchResult
from minty.cqrs import QueryBase
from minty.validation import validate_with
from pkgutil import get_data
from uuid import UUID


class Queries(QueryBase):
    """Queries class for the case type (and related) catalog"""

    @property
    def repository(self):
        return self.repository_factory.get_repository(
            "catalog", context=self.context, event_service=None
        )

    def get_folder_contents(
        self, folder_uuid: UUID, page: int = None, page_size: int = None
    ):
        """Retrieve catalog folder contents"""

        contents = self.repository.get_folder_contents(
            folder_uuid, page, page_size
        )
        return contents

    @validate_with(get_data(__name__, "../validation/search_catalog.json"))
    def search_catalog(
        self,
        keyword: str = None,
        type: str = None,
        page: int = None,
        page_size: int = None,
    ):
        """Retrieve catalog contents by search keyword"""

        contents = self.repository.search_catalog(
            keyword=keyword, type=type, page=page, page_size=page_size
        )

        return contents

    def get_folder_details(self, uuid: UUID):
        """Retrieve detailed information about a specific folder

        :param uuid: Identifier of the folder to retrieve details for
        :type uuid: UUID
        """

        details = self.repository.get_folder_details(uuid)
        return details

    def get_case_type_details(self, uuid: UUID):
        """Retrieve detailed information about a specific case type

        :param uuid: Identifier of the case type to retrieve details for
        :type uuid: UUID
        """
        case_type_repo = self.get_repository("case_type")
        case_type = case_type_repo.get_case_type(uuid=uuid)
        case_type.used_in_case_types = case_type_repo.get_related_case_types(
            case_type_id=case_type.id
        )
        return case_type

    def get_object_type_details(self, uuid: UUID):
        """Retrieve detailed information about a specific object type

        :param uuid: Identifier of the object type to retrieve details for
        :type uuid: UUID
        """

        details = self.repository.get_object_type_details(uuid)
        return details

    def get_custom_object_type_details(self, uuid: UUID):
        """Retrieve detailed information about a specific object type

        :param uuid: Identifier of the object type to retrieve details for
        :type uuid: UUID
        """

        details = self.repository.get_custom_object_type_details(uuid)
        return details

    def get_attribute_details(self, uuid: UUID):
        """Retrieve detailed information about a specific attribute

        :param uuid: Identifier of the attribute to retrieve details for
        :type uuid: UUID
        """

        details = self.repository.get_attribute_details(uuid)
        return details

    def get_email_template_details(self, uuid: UUID):
        """Retrieve detailed information about a specific email template

        :param uuid: Identifier of the email template to retrieve details for
        :type uuid: UUID
        """

        details = self.repository.get_email_template_details(uuid)
        return details

    def get_document_template_details(self, uuid: UUID):
        """Retrieve detailed information about a specific document template

        :param uuid: Identifier of the document template to retrieve details for
        :type uuid: UUID
        """

        details = self.repository.get_document_template_details(uuid)
        return details

    def get_attribute_details_for_edit(self, uuid: UUID):
        """Retrieve detailed information about an attribute to edit

        :param uuid: Identifier of the attribute to retrieve details for
        :type uuid: UUID
        """
        attribute_repo = self.get_repository("attribute")
        details = attribute_repo.get_attribute_details_by_uuid(uuid)
        return details

    def generate_magic_string(self, string_input: str):
        """Generate a valid magic_string from string_input.

        :param string_input: User inputted string
        :type string_input: str
        :return: Valid magic_string
        :rtype: str
        """
        attribute_repo = self.get_repository("attribute")

        return attribute_repo.generate_magic_string(string_input=string_input)

    @validate_with(
        get_data(__name__, "../validation/search_attribute_by_name.json")
    )
    def search_attribute_details_by_name(
        self, search_string: str, attribute_type: str
    ) -> list[AttributeDetailSearchResult]:
        """Search for attribute details by name.

        :param search_string: string to search for
        :type search_string: str
        :param attribute_type: value_type to filter by
        :type attribute_type: str
        :return: list of `AttributeSearchResult`
        :rtype: List[AttributeSearchResult]
        """
        attribute_repo = self.get_repository("attribute")
        results = attribute_repo.search_attribute_details_by_name(
            search_string=search_string, attribute_type=attribute_type
        )
        return results

    @validate_with(
        get_data(
            __name__, "../validation/get_email_template_details_for_edit.json"
        )
    )
    def get_email_template_details_for_edit(self, uuid: UUID):
        """Retrieve detailed information about an email template for edit.

        :param uuid: UUID of the email template
        :type uuid: UUID
        :return: Detailed information about an email template
        :rtype: EmailTemplate
        """

        email_template_repo = self.get_repository("email_template")
        email_template = (
            email_template_repo.get_email_template_details_by_uuid(uuid)
        )
        return email_template

    @validate_with(
        get_data(__name__, "../validation/case_type_version_history.json")
    )
    def get_case_type_history(self, uuid: UUID):
        """Retrieve detailed version history information about a case type version changes.

        :param uuid: UUID of the case type
        :type uuid: UUID
        :return: Versions history of a case type
        :rtype: CaseTypeVersion
        """

        case_type_repo = self.get_repository("case_type_version")
        case_type_versions = (
            case_type_repo.get_case_type_versions_history_by_uuid(uuid)
        )

        return case_type_versions

    @validate_with(
        get_data(
            __name__,
            "../validation/get_document_template_details_for_edit.json",
        )
    )
    def get_document_template_details_for_edit(self, uuid: UUID):
        """Retrieve detailed information about an document_template for edit.

        :param uuid: UUID of the document template
        :type uuid: UUID
        :return: Detailed information about an document template
        :rtype: DocumentTemplate
        """

        document_template_repo = self.get_repository("document_template")
        document_template = (
            document_template_repo.get_document_template_details_by_uuid(uuid)
        )
        return document_template
