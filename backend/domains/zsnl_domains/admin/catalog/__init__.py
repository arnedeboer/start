# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .commands import Commands
from .queries import Queries
from .repositories import (
    Catalog,
    attribute,
    case_type,
    case_type_version,
    document_template,
    email_template,
    folder,
    folder_entries,
    object_type,
)

REQUIRED_REPOSITORIES = {
    "catalog": Catalog,
    "case_type": case_type.CaseTypeRepository,
    "case_type_version": case_type_version.CaseTypeVersionRepository,
    "folder_entries": folder_entries.FolderEntriesRepository,
    "folder": folder.FolderRepository,
    "attribute": attribute.AttributeRepository,
    "email_template": email_template.EmailTemplateRepository,
    "document_template": document_template.DocumentTemplateRepository,
    "object_type": object_type.ObjectTypeRepository,
}


def get_query_instance(repository_factory, context, user_uuid):
    return Queries(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
    )


def get_command_instance(
    repository_factory, context, user_uuid, event_service
):
    return Commands(
        repository_factory=repository_factory,
        context=context,
        user_uuid=user_uuid,
        event_service=event_service,
    )
