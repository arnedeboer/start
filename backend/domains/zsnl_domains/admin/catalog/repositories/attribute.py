# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import re
from .... import ZaaksysteemRepositoryBase
from ..entities.attribute import Attribute, AttributeDetailSearchResult
from .database_queries import attribute_detail_query, attribute_values_query
from .search_queries import attribute_search_query
from minty.exceptions import Conflict, NotFound
from pkgutil import get_data
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import and_, delete, func, insert, join, select, update
from uuid import UUID
from zsnl_domains.database import schema

APPOINTMENT_ATTRIBUTE_TYPES = {"appointment", "appointment_v2"}


class AttributeRepository(ZaaksysteemRepositoryBase):
    cache = {}

    def get_attribute_details_by_uuid(self, uuid: UUID):
        """Get attribute details by uuid.

        :param DatabaseRepositoryBase.
        :type DatabaseRepositoryBase: DatabaseRepositoryBase
        :param uuid: UUID of the atttribute
        :type uuid: UUID
        :raises NotFound: When attribute with uuid not found
        :return: details of an attribute
        :rtype: dict
        """

        try:
            query_result = (
                self.session.query(
                    schema.BibliotheekKenmerk,
                    schema.BibliotheekCategorie,
                    schema.FileMetaData,
                )
                .outerjoin(schema.BibliotheekCategorie)
                .outerjoin(schema.FileMetaData)
                .filter(
                    and_(
                        schema.BibliotheekKenmerk.uuid == uuid,
                        schema.BibliotheekKenmerk.deleted.is_(None),
                    )
                )
                .one()
            )
        except NoResultFound as err:
            raise NotFound(
                f"Attribute with uuid {uuid} not found",
                "attribute/not_found",
            ) from err

        attribute_values = self._get_attribute_values(
            attibute_id=query_result[0].id,
            attribute_type=query_result[0].value_type,
        )
        return self._transform_to_entity(
            query_result=query_result, attribute_values=attribute_values
        )

    def _transform_to_entity(self, query_result, attribute_values):
        """Transform attribute query result to Attribute Entity.

        :param query_result: Query rsult
        :type query_result: Tuple
        :return: Attribute Entity
        :rtype: Atribute
        """
        attribute_object = query_result[0]
        bibliotheek_categorie = query_result[1]
        file_metadata = query_result[2]

        attribute_properties = attribute_object.properties
        sensitive_field = (
            True
            if attribute_properties.get("sensitive_field") == "on"
            else False
        )

        appointment_interface_uuid = attribute_properties.get(
            "appointment_interface_uuid"
        )

        appointment_location_id = (
            attribute_properties.get("location_id")
            if attribute_properties.get("location_id") != "?"
            else None
        )

        appointment_product_id = (
            attribute_properties.get("product_id")
            if attribute_properties.get("product_id") != "?"
            else None
        )

        attribute = Attribute(
            uuid=attribute_object.uuid,
            id=attribute_object.id,
            attribute_type=attribute_object.value_type,
            name=attribute_object.naam,
            public_name=attribute_object.naam_public,
            magic_string=attribute_object.magic_string,
            type_multiple=attribute_object.type_multiple,
            value_default=attribute_object.value_default,
            help=attribute_object.help,
            sensitive_field=sensitive_field,
            document_origin=getattr(file_metadata, "origin", None),
            document_trust_level=getattr(file_metadata, "trust_level", None),
            document_category=getattr(
                file_metadata, "document_category", None
            ),
            relationship_type=attribute_object.relationship_type,
            relationship_name=attribute_object.relationship_name,
            relationship_uuid=attribute_object.relationship_uuid,
            category_uuid=getattr(bibliotheek_categorie, "uuid", None),
            category_name=getattr(bibliotheek_categorie, "naam", None),
            attribute_values=attribute_values,
            appointment_location_id=appointment_location_id,
            appointment_product_id=appointment_product_id,
            appointment_interface_uuid=appointment_interface_uuid,
            commit_message=None,
        )
        attribute.event_service = self.event_service
        return attribute

    def _get_attribute_values(self, attibute_id, attribute_type):
        """Get list of values for an attribute of type checkbox or option.

        :param attibute_id: Id of the attribute.
        :type attibute_id: int
        :return: list of attribute values
        :rtype: list
        """
        values_list = []

        if attribute_type in ["checkbox", "option", "select"]:
            qry_statement = attribute_values_query.where(
                schema.BibliotheekKenmerkenValues.bibliotheek_kenmerken_id
                == attibute_id
            )
            qry_result = self.session.execute(qry_statement).fetchall()

            for each in qry_result:
                values_list.append(
                    {
                        "value": each.value,
                        "active": each.active,
                        "sort_order": each.sort_order,
                    }
                )
            # sort attribute_values with sort_order
            values_list = sorted(values_list, key=lambda i: i["sort_order"])
        return values_list

    def create_new_attribute(self, uuid: UUID, fields: dict):
        """Get a new attribute entity.

        :return: Attribute entity
        :rtype: Attribute
        """
        attribute = Attribute(
            uuid=uuid,
            id=None,
            attribute_type=None,
            name=None,
            public_name=None,
            magic_string=None,
            type_multiple=None,
            value_default=None,
            help=None,
            sensitive_field=False,
            document_origin=None,
            document_trust_level=None,
            document_category=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            category_uuid=None,
            category_name=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            commit_message=None,
        )
        attribute.event_service = self.event_service
        attribute.create(fields)
        return attribute

    def delete_attribute(self, uuid: UUID, reason: str):
        attribute = Attribute(
            uuid=uuid,
            id=None,
            attribute_type=None,
            name=None,
            public_name=None,
            magic_string=None,
            type_multiple=None,
            value_default=None,
            help=None,
            sensitive_field=False,
            document_origin=None,
            document_trust_level=None,
            document_category=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            category_uuid=None,
            category_name=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            commit_message=None,
        )
        attribute.event_service = self.event_service
        attribute.delete(reason=reason)
        return attribute

    def save(self):
        for event in self.event_service.event_list:
            if event.entity_type == "Attribute":
                if event.event_name == "AttributeEdited":
                    self._edit_attribute(event)
                elif event.event_name == "AttributeCreated":
                    self._insert_attribute(event)
                elif event.event_name == "AttributeDeleted":
                    self._delete_attribute(event)

    def _verify_correct_relationship_data(self, update_values):
        if not update_values["relationship_type"]:
            raise Conflict(
                "Attribute with type 'relationship' has no type",
                "attribute/no_relationship_type_set",
            )

        db_entry = None

        if update_values["relationship_type"] == "custom_object":
            if not update_values["relationship_uuid"]:
                raise Conflict(
                    "Attribute with type 'relationship' has no uuid",
                    "attribute/no_relationship_uuid_set",
                )

            db_entry = (
                self.session.query(schema.CustomObjectTypeVersion)
                .join(
                    schema.CustomObjectType,
                    schema.CustomObjectType.custom_object_type_version_id
                    == schema.CustomObjectTypeVersion.id,
                )
                .filter(
                    schema.CustomObjectType.uuid
                    == update_values["relationship_uuid"]
                )
                .one_or_none()
            )

            if db_entry:
                update_values["relationship_name"] = db_entry.name
            else:
                raise NotFound(
                    f"Relationship {update_values['relationship_uuid']} not found "
                    + f"for type {update_values['relationship_type']}",
                    "attribute/relationship_uuid_not_found",
                )
        # case attribute
        elif update_values["relationship_type"] == "case":
            update_values["relationship_uuid"] = None
            update_values["relationship_name"] = None

        elif update_values["relationship_type"] == "document":
            update_values["relationship_uuid"] = None
            update_values["relationship_name"] = None

        elif update_values["relationship_type"] == "subject":
            update_values["relationship_uuid"] = None
            update_values["relationship_name"] = None

    def _edit_attribute(self, event):
        """Edit an attribute with changes"""
        attribute_uuid = event.entity_id
        attribute_type = event.entity_data["attribute_type"]
        update_values = {}
        update_values["properties"] = {}

        if attribute_type == "file":
            # update file_metadata
            qry_statement = attribute_detail_query.where(
                schema.BibliotheekKenmerk.uuid == attribute_uuid
            )
            attribute = self.session.execute(qry_statement).fetchall()[0]
            file_metadata_id = attribute.file_metadata_id
            self._save_file_metadata(file_metadata_id, event.changes)

        elif attribute_type in ["option", "checkbox", "select"]:
            # update attribute values
            qry_statement = attribute_detail_query.where(
                schema.BibliotheekKenmerk.uuid == attribute_uuid
            )
            attribute = self.session.execute(qry_statement).fetchall()[0]
            self._save_attribute_values(attribute.id, event.changes)

        elif attribute_type in APPOINTMENT_ATTRIBUTE_TYPES:
            # save appointment details in database
            self._save_appointment_details(event.changes, update_values)

        self._generate_attribute_save_values(
            event.changes, values=update_values
        )

        # Verify correct data
        if attribute_type == "relationship":
            self._verify_correct_relationship_data(update_values)

        attribute_update = (
            update(schema.BibliotheekKenmerk)
            .where(and_(schema.BibliotheekKenmerk.uuid == attribute_uuid))
            .values(**update_values)
        )
        self.session.execute(attribute_update)

    def _insert_attribute(self, event):
        """Insert an attribute in database.

        :raises Conflict: When an attribute with uuid already exits in the system.
        """
        attribute_type = event.entity_data["attribute_type"]

        insert_values = {"uuid": event.entity_id}
        insert_values["properties"] = {}

        if attribute_type == "file":
            # insert file_metadata in database
            file_metadata_id = self._save_file_metadata(None, event.changes)
            insert_values["file_metadata_id"] = file_metadata_id

        elif attribute_type in APPOINTMENT_ATTRIBUTE_TYPES:
            # save appointment details in database
            self._save_appointment_details(event.changes, insert_values)

        self._generate_attribute_save_values(
            event.changes, create=True, values=insert_values
        )

        # Verify correct data
        if attribute_type == "relationship":
            self._verify_correct_relationship_data(insert_values)

        attribute_insert = insert(schema.BibliotheekKenmerk).values(
            **insert_values
        )

        try:
            attribute_id = self.session.execute(
                attribute_insert
            ).inserted_primary_key[0]
        except IntegrityError as err:
            uuid = event.entity_id
            raise Conflict(
                f"Attribute with uuid {uuid} already exists",
                "attribute/already_exists_with_uuid",
            ) from err

        if attribute_type in ["option", "checkbox", "select"]:
            # insert attribute_values in database
            self._save_attribute_values(attribute_id, event.changes)

    def _delete_attribute(self, event):
        """Delete an attribute.

        :raises Conflict: When attribute is part existing case type
        """
        attribute_uuid = event.entity_id
        if self._get_case_types_related_to_attribute(uuid=attribute_uuid):
            raise Conflict(
                "Attribute is used in case types",
                "attribute/used_in_case_types",
            )
        delete_values = self._generate_attribute_save_values(
            event.changes, values={}
        )
        delete_values["bibliotheek_categorie_id"] = None
        delete_stmt = (
            update(schema.BibliotheekKenmerk)
            .where(and_(schema.BibliotheekKenmerk.uuid == attribute_uuid))
            .values(**delete_values)
        )
        self.session.execute(delete_stmt)

    def _generate_attribute_save_values(
        self, changes, *, values, create=False
    ):
        """Generate attribute save values for update/insert from event changes.

        :param changes: event changes
        :type changes: list
        :param create: flag to check if its a create or update, defaults to False
        :type create: bool, optional
        :param values: values to update or insert attribute
        :type values: dict, optional
        :raises Conflict: When there is a conflict in saving
        :return: attribute values for save.
        :rtype: dict
        """
        attribute_mapping = {
            "uuid": "uuid",
            "id": "id",
            "attribute_type": "value_type",
            "name": "naam",
            "public_name": "naam_public",
            "type_multiple": "type_multiple",
            "value_default": "value_default",
            "help": "help",
            "deleted": "deleted",
            "relationship_type": "relationship_type",
            "relationship_name": "relationship_name",
            "relationship_uuid": "relationship_uuid",
        }

        for change in changes:
            if change["key"] in attribute_mapping:
                db_fieldname = attribute_mapping[change["key"]]
                values[db_fieldname] = change["new_value"]

            elif change["key"] == "category_uuid" and create:
                category_uuid = change["new_value"]
                values["bibliotheek_categorie_id"] = None

                if category_uuid:
                    bibliotheek_categorie = self._get_bibliotheek_category(
                        category_uuid=category_uuid
                    )
                    values[
                        "bibliotheek_categorie_id"
                    ] = bibliotheek_categorie.id

            elif change["key"] == "magic_string" and create:
                magic_string = change["new_value"]
                if not self._is_valid_magic_string(magic_string):
                    raise Conflict(
                        f"Magic string '{magic_string}' is invalid.",
                        "attribute/invalid_magic_string",
                    )
                values["magic_string"] = magic_string

            elif change["key"] == "sensitive_field":
                if change["new_value"]:
                    values["properties"]["sensitive_field"] = "on"

        if values.get("naam"):
            values["search_term"] = values["naam"]
            values["search_order"] = values["naam"]

        return values

    def _get_bibliotheek_category(self, category_uuid: UUID):
        """Get a bibliotheek category with uuid.

        :param category_uuid: uuid of bibliotheek category
        :type category_uuid: UUID
        :raises NotFound: When a bibliotheek category with uuid not found
        :return: BibliotheekCategory
        :rtype: schema.BibliotheekCategory
        """

        try:
            bibliotheek_categorie = (
                self.session.query(schema.BibliotheekCategorie)
                .filter(
                    and_(schema.BibliotheekCategorie.uuid == category_uuid)
                )
                .one()
            )
        except NoResultFound as err:
            raise NotFound(
                f"Bibliotheek category with uuid {category_uuid} not found",
                "bibliotheek_category/not_found",
            ) from err
        return bibliotheek_categorie

    def _save_attribute_values(self, attribute_id, changes):
        """Save attribute_values in database.

        :param attribute_id: id of the attribute
        :type attribute_id: int
        :param changes: event changes
        :type changes: list
        """

        for change in changes:
            if (
                change["key"] == "attribute_values"
                and change["new_value"] != change["old_value"]
            ):
                # delete existing row of attribute values from BibliotheekKenmerkenValues table
                attribute_values_delete = delete(
                    schema.BibliotheekKenmerkenValues
                ).where(
                    schema.BibliotheekKenmerkenValues.bibliotheek_kenmerken_id
                    == attribute_id
                )
                self.session.execute(attribute_values_delete)

                # for each attribute_values insert new row in BibliotheekKenmerkenValues table
                attribute_values = change["new_value"]
                for each_value in attribute_values:
                    if each_value.get("id"):
                        del each_value["id"]
                    each_value["bibliotheek_kenmerken_id"] = attribute_id
                    attribute_value_insert = insert(
                        schema.BibliotheekKenmerkenValues
                    ).values(**each_value)
                    self.session.execute(attribute_value_insert)

    def _save_file_metadata(self, file_metadata_id, changes):
        """Save file_metadata in FileMetadata table.

        :param file_metadata_id: id of file_metadata
        :type file_metadata_id: int
        :param changes: event changes
        :return: file_metadata id
        :rtype: int
        """

        file_metadata_mapping = {
            "document_category": "document_category",
            "document_trust_level": "trust_level",
            "document_origin": "origin",
        }

        values = {}

        for change in changes:
            if change["key"] in file_metadata_mapping:
                db_fieldname = file_metadata_mapping[change["key"]]
                values[db_fieldname] = change["new_value"]

        if file_metadata_id:
            # update file_metadata row with new values
            file_metadata_update = (
                update(schema.FileMetaData)
                .where(and_(schema.FileMetaData.id == file_metadata_id))
                .values(**values)
            )
            self.session.execute(file_metadata_update)
        else:
            # insert new file_metadata in FileMetadata table
            if not values.get("document_category"):
                values["document_category"] = "Aangifte"
            if not values.get("trust_level"):
                values["trust_level"] = "Openbaar"

            file_metadata_insert = insert(schema.FileMetaData).values(**values)
            file_metadata_id = self.session.execute(
                file_metadata_insert
            ).inserted_primary_key[0]

        return file_metadata_id

    def _save_appointment_details(self, changes, values):
        """Save appointment details in database.

        :param changes: event_changes
        :type changes: list
        :param values: values to be saved
        :type values: dict
        """
        appointment_mapping = {
            "appointment_location_id": "location_id",
            "appointment_product_id": "product_id",
            "appointment_interface_uuid": "appointment_interface_uuid",
        }

        for change in changes:
            if change["key"] == "appointment_interface_uuid":
                if not change["new_value"]:
                    raise Conflict(
                        "No active appointment integrations found",
                        "attribute/no_active_appointment_integrations",
                    )
                values["properties"]["appointment_interface_uuid"] = change[
                    "new_value"
                ]

            elif change["key"] in [
                "appointment_product_id",
                "appointment_location_id",
            ]:
                property_name = appointment_mapping[change["key"]]
                values["properties"][property_name] = (
                    change["new_value"] if change["new_value"] else "?"
                )

    def generate_magic_string(self, string_input: str):
        """Generate a valid magic string from inputted string.

        :param string_input: String input
        :type string_input: str
        :return: Valid magic_string
        :rtype: str
        """

        number_of_tries = 0

        # Lowercase string_input and replace space with underscore
        # Remove special characters
        formatted_string = string_input.replace(" ", "_").lower()
        formatted_string = re.sub("[^a-z0-9_]", "", formatted_string)
        if not formatted_string:
            formatted_string = "default_magic_string"

        magic_string = formatted_string

        while number_of_tries < 25:
            if self._is_valid_magic_string(magic_string):
                return magic_string

            number_of_tries = number_of_tries + 1

            # split the formatted_string into string and end_digits
            split_list = re.split(r"(\d+$)", formatted_string)
            string = split_list[0]

            # end_digits of formatted_string
            end_digits = 0
            if len(split_list) > 1:
                end_digits = split_list[1]

            # increment end_digits of formatted_string by number_of_tries
            magic_string = string + str(int(end_digits) + number_of_tries)

        raise TimeoutError(
            "Cannot generate magic_string. Try with different magic_string",
            "attribute/magic_string_cannot_be_generated",
        )

    def _is_valid_magic_string(self, string_input: str):
        """Check if the inputted string is valid magic_string.
        If the string_input is alreay taken or is a reserved magic_string or is empty return False.

        :param string_input: Inputted string
        :type magic_string: str
        :return: True if string_input is valid else False
        :rtype: boolean
        """
        if string_input == "":
            return False

        query_result = (
            self.session.query(schema.BibliotheekKenmerk)
            .filter(
                and_(
                    schema.BibliotheekKenmerk.magic_string == string_input,
                    schema.BibliotheekKenmerk.deleted.is_(None),
                )
            )
            .all()
        )

        if not self.cache.get("reserved_magic_strings"):
            self.cache["reserved_magic_strings"] = (
                get_data(__name__, "../reserved_magic_strings.txt")
                .decode("utf-8")
                .split("\n")
            )

        if (
            query_result
            or string_input in self.cache["reserved_magic_strings"]
        ):
            return False
        return True

    def search_attribute_details_by_name(
        self, search_string: str, attribute_type: str
    ) -> list[AttributeDetailSearchResult]:
        """Search for attribute details by name.

        :param search_string: string to search for
        :type search_string: str
        :param attribute_type: value_type to filter by
        :type attribute_type: str
        :return: list of `AttributeSearchResult`
        :rtype: List[AttributeSearchResult]
        """
        search_string = search_string.lower()
        stmt = attribute_search_query(
            search_string=search_string, attr_type=attribute_type
        )
        results = self.session.execute(stmt).fetchall()

        out = []
        for res in results:
            atr = AttributeDetailSearchResult(
                id=res.id,
                uuid=res.uuid,
                name=res.name,
                magic_string=res.magic_string,
                value_type=res.value_type,
            )
            out.append(atr)

        return out

    def _get_case_types_related_to_attribute(self, uuid: UUID):
        """Get case types related to an attribute.

        :param uuid: attribute uuid
        :type uuid: UUID
        :return: List of case types related to an attribute
        :rtype: list
        """
        attribute_id = (
            select(schema.BibliotheekKenmerk.id)
            .where(schema.BibliotheekKenmerk.uuid == uuid)
            .scalar_subquery()
        )

        zaaktype_joined = join(
            schema.ZaaktypeKenmerk,
            schema.ZaaktypeNode,
            schema.ZaaktypeKenmerk.zaaktype_node_id == schema.ZaaktypeNode.id,
        ).join(
            schema.Zaaktype,
            schema.ZaaktypeNode.zaaktype_id == schema.Zaaktype.id,
        )

        case_types_qry_stmt = (
            select(func.count())
            .select_from(zaaktype_joined)
            .where(
                and_(
                    schema.Zaaktype.deleted.is_(None),
                    schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id
                    == attribute_id,
                )
            )
        )

        related_case_types = self.session.execute(
            case_types_qry_stmt
        ).fetchone()
        return related_case_types[0]
