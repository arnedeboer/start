# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import ZaaksysteemRepositoryBase
from ..entities import Folder
from minty.exceptions import Conflict, NotFound
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import aliased
from sqlalchemy.sql import (
    alias,
    delete,
    func,
    insert,
    join,
    select,
    union_all,
    update,
)
from uuid import UUID
from zsnl_domains.database import schema


class FolderRepository(ZaaksysteemRepositoryBase):
    def get_folder(self, uuid: UUID, with_parent_folder_ids=False) -> Folder:
        """Get Folder entity from database.

        :param uuid: folder uuid
        :type uuid: UUID
        :param with_parent_folder_ids: included parent_folder_ids, defaults to False
        :param with_parent_folder_ids: bool, optional
        :raises NotFound: When record can't be found in database
        :return: folder entity
        :rtype: Folder
        """
        folder_statement = get_folder_query(uuid=uuid)

        q_result = self.session.execute(folder_statement).fetchone()
        if not q_result:
            raise NotFound(
                f"Folder with uuid: {uuid} not found.", "folder/not_found"
            )
        parent_ids = []
        if with_parent_folder_ids:
            parent_ids = self.get_parent_folder_ids(query_result=q_result)

        folder = self._transform_to_entity(
            query_result=q_result, parent_ids=parent_ids
        )

        return folder

    def get_parent_folder_ids(self, query_result) -> list[int]:
        """Get ids for parent folders.

        :param query_result: query result
        :type query_result: sqla object
        :return: list of parent ids
        :rtype: List[int]
        """
        parents = []

        if query_result.parent_id:
            q = parent_folder_ids_query(target_id=query_result.id)
            result = self.session.query(q)
            parents = [r.pid for r in result.all()]

        return parents

    def _transform_to_entity(self, query_result, parent_ids: list) -> Folder:
        """Initialize case Folder entity from query results.

        :param query_result: sqla query results
        :type query_result: object
        :param parents_ids: list of parent folder ids(int)
        :type parents_ids: list(int)
        :return: folder entity
        :rtype: Folder
        """
        folder = Folder(
            id=query_result.id,
            uuid=query_result.uuid,
            name=query_result.name,
            parent_uuid=query_result.parent_uuid,
            parent_name=query_result.parent_name,
            last_modified=query_result.last_modified,
            parent_ids=parent_ids,
        )

        folder.event_service = self.event_service
        return folder

    def get_new_folder(
        self, uuid: UUID, parent_uuid: UUID, name: str
    ) -> Folder:
        """Initialize new folder from given parameters.

        :param uuid: folder uuid
        :type uuid: UUID
        :param parent_uuid: parent folder uuid
        :type parent_uuid: UUID
        :param name: name
        :type name: str
        :return: Initialized folder entity
        :rtype: Folder
        """
        f = Folder(
            id=None,
            uuid=uuid,
            name=None,
            parent_uuid=None,
            parent_name=None,
            last_modified=None,
        )
        f.event_service = self.event_service
        f.create(parent_uuid=parent_uuid, name=name)
        return f

    def delete_folder(self, uuid: UUID, reason: str):
        name = self._get_folder_name(uuid=uuid)
        f = Folder(
            id=None,
            uuid=uuid,
            name=name,
            parent_uuid=None,
            parent_name=None,
            last_modified=None,
        )
        f.event_service = self.event_service
        f.delete(reason=reason)
        return f

    def save(self):
        for event in self.event_service.event_list:
            f_changes = {}
            for c in event.changes:
                f_changes[c["key"]] = c["new_value"]

            if event.event_name == "FolderCreated":
                self._create_record(
                    uuid=event.entity_id, formatted_changes=f_changes
                )
            elif event.event_name == "FolderDeleted":
                self._delete_record(uuid=event.entity_id)
            else:
                self._update_record(
                    uuid=event.entity_id, formatted_changes=f_changes
                )

    def _update_record(self, uuid: UUID, formatted_changes: dict):
        """Update folder values for `formatted_changes`.

        :param uuid: folder uuid
        :type uuid: UUID
        :param formatted_changes: changes to update
        :type formatted_changes: dict
        :raises Conflict: foldername is in use.
        """
        values = {"naam": formatted_changes["name"]}
        stmt = (
            update(schema.BibliotheekCategorie)
            .values(values)
            .where(schema.BibliotheekCategorie.uuid == uuid)
        )
        try:
            self.session.execute(stmt)
        except IntegrityError as err:
            raise Conflict(
                "Folder name already in use in parent folder",
                "folder/name_in_use",
            ) from err

    def _create_record(self, uuid: UUID, formatted_changes: dict):
        """Create new folder record.

        :param uuid: folder uuid
        :type uuid: UUID
        :param formatted_changes: captured changes
        :type formatted_changes: dict
        :raises Conflict: when folder name is already in use
        """
        values = {
            "uuid": uuid,
            "pid": self.get_folder_id(formatted_changes["parent_uuid"]),
            "naam": formatted_changes["name"],
        }
        stmt = insert(schema.BibliotheekCategorie).values(values)
        try:
            self.session.execute(stmt)
        except IntegrityError as err:
            raise Conflict(
                "Folder name already in use in parent folder",
                "folder/name_in_use",
            ) from err

    def _get_folder_contents(self, uuid):
        folder_id = self.get_folder_id(uuid=uuid)

        specific_folder_query = select(func.count()).where(
            schema.BibliotheekCategorie.pid == folder_id
        )

        specific_case_type_query = select(func.count()).where(
            schema.Zaaktype.bibliotheek_categorie_id == folder_id
        )

        specific_attribute_query = select(func.count()).where(
            schema.BibliotheekKenmerk.bibliotheek_categorie_id == folder_id
        )

        specific_email_template_query = select(func.count()).where(
            schema.BibliotheekNotificatie.bibliotheek_categorie_id == folder_id
        )

        specific_document_template_query = select(func.count()).where(
            schema.BibliotheekSjabloon.bibliotheek_categorie_id == folder_id
        )

        specific_object_type_query = select(func.count()).where(
            schema.ObjectBibliotheekEntry.bibliotheek_categorie_id == folder_id
        )

        contents_qry_stmt = union_all(
            specific_folder_query,
            specific_case_type_query,
            specific_attribute_query,
            specific_email_template_query,
            specific_document_template_query,
            specific_object_type_query,
        )

        list_contents = self.session.execute(contents_qry_stmt).fetchall()
        contents_count = 0
        for each in list_contents:
            contents_count += each[0]
        return contents_count

    def _delete_record(self, uuid: UUID):
        """Delete a folder record from database.

        :param uuid: UUID of the folder
        :type uuid: UUID
        :raises Conflict: When folder is not empty
        """
        if self._get_folder_contents(uuid=uuid):
            raise Conflict("Folder is not empty.", "folder/not_empty")

        delete_stmt = delete(schema.BibliotheekCategorie).where(
            schema.BibliotheekCategorie.uuid == uuid
        )
        self.session.execute(delete_stmt)

    def get_folder_id(self, uuid: UUID):
        """Select folder `id` for given `uuid`

        :param uuid: uuid
        :type uuid: UUID
        :return: select statement
        :rtype: Select
        """
        stmt = None
        if uuid is not None:
            stmt = (
                select(schema.BibliotheekCategorie.id)
                .where(schema.BibliotheekCategorie.uuid == uuid)
                .scalar_subquery()
            )
        return stmt

    def _get_folder_name(self, uuid: UUID) -> str:
        """Get folder name.

        :param uuid: folder uuid
        :type uuid: UUID
        :raises NotFound: When record can't be found in database
        :return: A folder name
        :rtype: str
        """

        folder_name_query_stmt = select(
            schema.BibliotheekCategorie.naam.label("name")
        ).where(schema.BibliotheekCategorie.uuid == uuid)
        result = self.session.execute(folder_name_query_stmt).fetchone()
        if not result:
            raise NotFound(
                f"Folder with uuid: {uuid} not found.", "folder/not_found"
            )
        return result.name


def parent_folder_ids_query(target_id: int):
    """Recursive query to fetch all parent folders for given id.

    :param target_id: target folder id
    :type target_id: int
    :return: query to get parent folder ids
    :rtype: sqla query
    """
    q_recursive = (
        select(schema.BibliotheekCategorie.pid)
        .where(schema.BibliotheekCategorie.id == target_id)
        .cte(recursive=True, name="cte")
    )
    q2_alias = aliased(schema.BibliotheekCategorie, name="q2")
    parent_folder_query = q_recursive.union_all(
        select(q2_alias.pid).where(q2_alias.id == q_recursive.c.pid)
    )
    return parent_folder_query


def get_folder_query(uuid: UUID):
    """Query to get folder information for given uuid.

    :param uuid: uuid
    :type uuid: UUID
    :return: query to get folder information
    :rtype: sqla query
    """
    parent_cat = alias(schema.BibliotheekCategorie)
    folder_detail_query = (
        select(
            schema.BibliotheekCategorie.id,
            schema.BibliotheekCategorie.uuid,
            schema.BibliotheekCategorie.naam.label("name"),
            schema.BibliotheekCategorie.last_modified,
            parent_cat.c.uuid.label("parent_uuid"),
            parent_cat.c.naam.label("parent_name"),
            parent_cat.c.id.label("parent_id"),
        )
        .select_from(
            join(
                schema.BibliotheekCategorie,
                parent_cat,
                schema.BibliotheekCategorie.pid == parent_cat.c.id,
                isouter=True,
            )
        )
        .where(schema.BibliotheekCategorie.uuid == uuid)
    )
    return folder_detail_query
