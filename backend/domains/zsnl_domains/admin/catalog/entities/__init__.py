# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .case_type import CaseTypeEntity
from .case_type_version import CaseTypeVersionEntity
from .catalog_entities import (
    AttributeDetail,
    CaseTypeVersion,
    CustomObjectTypeDetail,
    DocumentTemplateDetail,
    EmailTemplateDetail,
    EntryType,
    Folder,
    ObjectTypeDetail,
    RelatedCaseType,
)
from .email_template import EmailTemplate
from .folder_entries import FolderEntry

__all__ = [
    "CaseTypeEntity",
    "CaseTypeVersionEntity",
    "CaseTypeVersion",
    "CustomObjectTypeDetail",
    "EntryType",
    "FolderEntry",
    "Folder",
    "RelatedCaseType",
    "ObjectTypeDetail",
    "AttributeDetail",
    "EmailTemplate",
    "EmailTemplateDetail",
    "DocumentTemplateDetail",
]
