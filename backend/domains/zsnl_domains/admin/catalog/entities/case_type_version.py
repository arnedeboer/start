# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import EntityBase
from uuid import UUID


class CaseTypeVersionEntity(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        id: int,
        uuid: UUID,
        case_type_uuid: UUID,
        name: str,
        active: bool,
        version: int,
        created: datetime,
        last_modified: datetime,
        username: str = None,
        display_name: str = None,
        reason: str = "",
    ):
        self.id = id
        self.uuid = uuid
        self.case_type_uuid = case_type_uuid
        self.name = name
        self.username = username
        self.display_name = display_name
        self.active = active
        self.version = version
        self.last_modified = last_modified
        self.created = created
        self.reason = reason
