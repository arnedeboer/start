# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..entities import Folder
from ..repositories.case_type import CaseTypeRepository
from minty.cqrs import CommandBase
from minty.validation import validate_with
from pkgutil import get_data
from typing import cast
from uuid import UUID


class Commands(CommandBase):
    """Admin domain commands."""

    @validate_with(
        get_data(__name__, "../validation/change_case_type_online_status.json")
    )
    def change_case_type_online_status(
        self, case_type_uuid: UUID, active: bool, reason: str
    ):
        """Change online status on case_type entity.

        :param case_type_uuid: case type uuid
        :type case_type_uuid: UUID
        :param active: status to set to
        :type active: bool
        :param reason: reason for change
        :type reason: str
        """
        repo = self.get_repository("case_type")
        case_type = repo.get_case_type(uuid=case_type_uuid)
        case_type.change_online_status(active=active, reason=reason)
        repo.save()

    @validate_with(
        get_data(__name__, "../validation/catalog_move_folder_entries.json")
    )
    def move_folder_entries(
        self, destination_folder_id: UUID, folder_entries: list
    ):
        """Move folder entries to destination folder.

        :param destination_folder_id: destination folder
        :type destination_folder_id: UUID
        :param folder_entries: list of folder entries
        :type folder_entries: List[{"type": (type), "id":(uuid)}]
        """
        folder_entries_repo = self.get_repository("folder_entries")
        folder_entries = folder_entries_repo.get_folder_entries(
            folder_entries=folder_entries
        )

        if destination_folder_id is None:
            destination_folder = Folder(
                id=None,
                uuid=None,
                name="root_folder",
                parent_uuid=None,
                parent_name=None,
                last_modified=None,
                parent_ids=[],
            )
        else:
            folder_repo = self.get_repository("folder")
            destination_folder = folder_repo.get_folder(
                uuid=destination_folder_id, with_parent_folder_ids=True
            )
        for entry in folder_entries:
            entry.move(destination_folder=destination_folder)

        folder_entries_repo.save_list(folder_entries=folder_entries)

    @validate_with(get_data(__name__, "../validation/edit_attribute.json"))
    def edit_attribute(self, attribute_uuid: UUID, fields: dict):
        """Edit/Update an attribute.

        :param attribute_uuid: UUID of the attribute
        :type attribute_uuid: UUID
        :param fields: Fields of the attribute to be changed
        :type fields: dict
        """
        attribute_repo = self.get_repository("attribute")
        attribute = attribute_repo.get_attribute_details_by_uuid(
            uuid=attribute_uuid
        )
        attribute.edit(fields)
        attribute_repo.save()

    @validate_with(get_data(__name__, "../validation/create_attribute.json"))
    def create_attribute(self, attribute_uuid: str, fields: dict):
        """Create an attribute.

        :param attribute_uuid: UUID of the new attribute
        :type attribute_uuid: UUID
        :param fields: fields of the attribute to be created.
        :type fields: dict
        """
        attribute_repo = self.get_repository("attribute")
        attribute_repo.create_new_attribute(uuid=attribute_uuid, fields=fields)
        attribute_repo.save()

    @validate_with(
        get_data(__name__, "../validation/catalog_rename_folder.json")
    )
    def rename_folder(self, folder_uuid: UUID, name: str):
        """Rename folder.

        :param folder_uuid: folder uuid
        :type folder_uuid: UUID
        :param name: name
        :type name: str
        """
        folder_repo = self.get_repository("folder")
        folder = folder_repo.get_folder(
            uuid=folder_uuid, with_parent_folder_ids=False
        )
        folder.rename(name=name)
        folder_repo.save()

    @validate_with(
        get_data(__name__, "../validation/catalog_create_folder.json")
    )
    def create_folder(self, folder_uuid: UUID, parent_uuid: UUID, name: str):
        """Create new folder by for given parameters.

        :param folder_uuid: folder  uuid
        :type folder_uuid: UUID
        :param parent_uuid: uuid of parent folder
        :type parent_uuid: UUID
        :param name: name
        :type name: str
        """
        folder_repo = self.get_repository("folder")
        folder_repo.get_new_folder(
            uuid=folder_uuid, parent_uuid=parent_uuid, name=name
        )
        folder_repo.save()

    @validate_with(
        get_data(__name__, "../validation/edit_and_create_email_template.json")
    )
    def create_email_template(self, uuid: UUID, fields: dict):
        """Create a new email template.

        :param uuid: email template uuid
        :type uuid: UUID
        :param fields: fields to create email template with
        :type fields: dict
        """
        email_template_repo = self.get_repository("email_template")
        email_template_repo.create_new_email_template(uuid=uuid, fields=fields)
        email_template_repo.save()

    @validate_with(
        get_data(__name__, "../validation/edit_and_create_email_template.json")
    )
    def edit_email_template(self, uuid: UUID, fields: dict):
        """Edit an email template.

        :param uuid: email_template uuid
        :type uuid: UUID
        :param fields: fields to update
        :type fields: dict
        """
        email_template_repo = self.get_repository("email_template")
        email_template = (
            email_template_repo.get_email_template_details_by_uuid(uuid=uuid)
        )
        email_template.edit(fields=fields)
        email_template_repo.save()

    @validate_with(
        get_data(
            __name__, "../validation/edit_and_create_document_template.json"
        )
    )
    def create_document_template(self, uuid: UUID, fields: dict):
        """Create a document template.

        :param uuid: document template uuid
        :type uuid: UUID
        :param fields: fields to create document template with
        :type fields: dict
        """
        document_template_repo = self.get_repository("document_template")
        document_template_repo.create_new_document_template(
            uuid=uuid, fields=fields
        )
        document_template_repo.save()

    @validate_with(
        get_data(
            __name__, "../validation/edit_and_create_document_template.json"
        )
    )
    def edit_document_template(self, uuid: UUID, fields: dict):
        """Edit a document template.

        :param uuid: document template uuid
        :type uuid: UUID
        :param fields: fields to edit docume template with
        :type fields: dict
        """
        document_template_repo = self.get_repository("document_template")
        document_template = (
            document_template_repo.get_document_template_details_by_uuid(
                uuid=uuid
            )
        )
        document_template.edit(fields=fields)
        document_template_repo.save()

    @validate_with(
        get_data(__name__, "../validation/update_case_type_version.json")
    )
    def update_case_type_version(
        self, case_type_uuid: UUID, version_uuid: UUID, reason: str
    ):
        """Set a specific version on a case type.

        :param case_type_uuid: case type version uuid
        :type case_type_uuid: UUID
        :param version_uuid: case type version uuid
        :type version_uuid: UUID
        :param reason: reason for the update
        :type reason: str
        """
        case_type_repo = cast(
            CaseTypeRepository, self.get_repository("case_type")
        )
        case_type = case_type_repo.get_case_type(uuid=case_type_uuid)

        (
            new_case_type_version_uuid,
            new_version,
        ) = case_type_repo.create_new_version_from_existing_version(
            version_uuid
        )

        case_type.update_case_type_version(
            new_version_uuid=new_case_type_version_uuid,
            new_version=new_version,
            reason=reason,
        )

        case_type_repo.save()

    @validate_with(
        get_data(__name__, "../validation/delete_catalog_elements.json")
    )
    def delete_attribute(self, uuid: UUID, reason: str):
        """Delete an attribute.

        :param uuid: attribute uuid
        :type uuid: UUID
        :param reason: reason for deleting the attribute
        :type fields: str
        """
        attribute_repo = self.get_repository("attribute")
        attribute_repo.delete_attribute(uuid=uuid, reason=reason)
        attribute_repo.save()

    @validate_with(
        get_data(__name__, "../validation/delete_catalog_elements.json")
    )
    def delete_email_template(self, uuid: UUID, reason: str):
        """Delete an email_template.

        :param uuid: email_template uuid
        :type uuid: UUID
        :param reason: reason for deleting the email_template
        :type fields: str
        """
        email_template_repo = self.get_repository("email_template")
        email_template_repo.delete_email_template(uuid=uuid, reason=reason)
        email_template_repo.save()

    @validate_with(
        get_data(__name__, "../validation/delete_catalog_elements.json")
    )
    def delete_folder(self, uuid: UUID, reason: str):
        """Delete a folder.

        :param uuid: folder uuid
        :type uuid: UUID
        :param reason: reason for deleting the folder
        :type fields: str
        """
        folder_repo = self.get_repository("folder")
        folder_repo.delete_folder(uuid=uuid, reason=reason)
        folder_repo.save()

    @validate_with(
        get_data(__name__, "../validation/delete_catalog_elements.json")
    )
    def delete_case_type(self, uuid: UUID, reason: str):
        """Delete a case_type.

        :param uuid: case_type uuid
        :type uuid: UUID
        :param reason: reason for deleting the case_type
        :type fields: str
        """
        case_type_repo = self.get_repository("case_type")
        case_type_repo.delete_case_type(uuid=uuid, reason=reason)
        case_type_repo.save()

    @validate_with(
        get_data(__name__, "../validation/delete_catalog_elements.json")
    )
    def delete_object_type(self, uuid: UUID, reason: str):
        """Delete a object_type.

        :param uuid: folder uuid
        :type uuid: UUID
        :param reason: reason for deleting the case_type
        :type fields: str
        """
        object_type_repo = self.get_repository("object_type")
        object_type_repo.delete_object_type(uuid=uuid, reason=reason)
        object_type_repo.save()

    @validate_with(
        get_data(__name__, "../validation/delete_catalog_elements.json")
    )
    def delete_document_template(self, uuid: UUID, reason: str):
        """Delete a case_typer.

        :param uuid: folder uuid
        :type uuid: UUID
        :param reason: reason for deleting the case_type
        :type fields: str
        """
        document_template_repo = self.get_repository("document_template")
        document_template_repo.delete_document_template(
            uuid=uuid, reason=reason
        )
        document_template_repo.save()
