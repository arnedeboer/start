# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, ValueObject
from pydantic import Field
from uuid import UUID


class TransactionRecord(ValueObject):
    uuid: UUID | None = Field(
        None, title="Internal Identifier this transaction record"
    )
    transaction_id: int = Field(None, title="Id of related transaction")
    is_error: bool | None = Field(None, title="Boolean representing error")
    input_data: str | None = Field(..., title="Input data for the transaction")
    output_data: str | None = Field(
        ..., title="Output data for the transaction"
    )
    preview_string: str | None = Field(
        ..., title="Text preview for transaction record"
    )


class Transaction(Entity):
    entity_type = "transaction"
    entity_id__fields = ["uuid"]

    uuid: UUID | None = Field(..., title="Internal Identifier for this role")
    input_data: str | None = Field(..., title="Input data for the transaction")
    integration_uuid: UUID | None = Field(
        ..., title="Status of the transaction"
    )
    transaction_records: list[TransactionRecord] | None = Field(
        default_factory=list, title="Detailed record of transaction"
    )

    @Entity.event(name="TransactionCreated", fire_always=True)
    def create(self, integration_uuid: UUID, input_data: str):
        """Create Transaction entry"""
        self.integration_uuid = integration_uuid
        self.input_data = input_data

    @Entity.event(name="TransactionRecordAdded", fire_always=True)
    def add_transaction_record(
        self,
        is_error: bool,
        input_data: str,
        output_data: str,
        preview_string: str,
    ):
        """Add transaction record for given transaction"""

        record = TransactionRecord(
            is_error=False,
            input_data=str(input_data),
            output_data=str(output_data),
            preview_string=preview_string,
        )

        self.transaction_records = [record]
