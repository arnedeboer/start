# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.6.247"

import amqpstorm
import minty.cqrs.events
import minty.infrastructure
import operator
from .shared.types import ComparisonFilterCondition, FilterOperator
from minty.repository import RepositoryBase
from minty_infra_amqp import AMQPInfrastructure
from minty_infra_sqlalchemy import DatabaseSessionInfrastructure
from sqlalchemy import sql
from typing import cast


class ZaaksysteemRepositoryBase(RepositoryBase):
    REQUIRED_INFRASTRUCTURE = {"amqp": AMQPInfrastructure()}
    REQUIRED_INFRASTRUCTURE_RW = {
        "database": DatabaseSessionInfrastructure("zaaksysteemdb.")
    }
    REQUIRED_INFRASTRUCTURE_RO = {
        "database": DatabaseSessionInfrastructure("zaaksysteemdb_ro.")
    }

    MAX_QUERYTIME_COUNT_MS = 100

    def __init__(
        self,
        infrastructure_factory: minty.infrastructure.InfrastructureFactory,
        context: str,
        event_service,
    ):
        super().__init__(
            infrastructure_factory=infrastructure_factory,
            context=context,
            event_service=event_service,
        )

        self.session = self.infrastructure_factory.get_infrastructure(
            context=self.context, infrastructure_name="database"
        )

    def apply_comparison_filter(
        self,
        column,
        comparison_filters: list[ComparisonFilterCondition],
    ) -> sql.expression.Select:
        """
        Apply a list of `ComparisonFilterCondition` conditions to a
        SQLAlchemy query.

        Example:

        ```
        subquery = self.apply_comparison_filter(
            column=schema.SomeTable.id,
            comparison_filters=[
                ComparisonFilterCondition[int].new_from_str("gt 10"),
                ComparisonFilterCondition[int].new_from_str("le 100")
            ]
        )
        ```

        Returns a subquery with the added conditions.
        """
        subqueries = []
        for filter in comparison_filters:
            subqueries.append(
                getattr(operator, filter.operator)(column, filter.operand)
            )
        query = sql.and_(*subqueries)
        return query

    def _get_count(self, query: sql.expression.Select) -> int:
        """Get the total number of results for this query."""
        count_query = (
            query.with_only_columns(sql.func.count(sql.literal(1)))
            .limit(None)
            .offset(None)
            .order_by(None)
        )

        return self.session.execute(count_query).scalar()

    def _calculate_offset(self, page: int, page_size: int):
        """Given a page number and page size, calculate the offset at which it
        starts"""
        return (page * page_size) - page_size

    def apply_operator_to_filters(
        self,
        operator_filters: FilterOperator,
        filter_subquery,
    ) -> sql.expression.Select:
        if operator_filters == FilterOperator.and_operator:
            query = sql.and_(*filter_subquery)
        elif operator_filters == FilterOperator.or_operator:
            query = sql.and_(sql.or_(*filter_subquery))
        return query

    def send_query_event(self, event: minty.cqrs.events.Event) -> None:
        """
        Send out an event on the message bus

        Note that this code does not wait for the rest of the domain code to
        finish before sending the event. This means any active database
        transaction will still be uncommitted and the consumer that picks up
        the event will not be able to see the changes made inside the
        transaction yet.

        That makes this method of limited/no use in commands, and it should
        only be used in queries.

        Examples use cases: "a case was retrieved and this needs to be logged",
        or "please make a thumbnail for this file".
        """
        config = self.infrastructure_factory.get_config(context=event.context)
        publish_to_exchange = config["amqp"]["publish_settings"]["exchange"]

        amqp_channel = cast(
            amqpstorm.Channel,
            self.infrastructure_factory.get_infrastructure(
                context=self.context, infrastructure_name="amqp"
            ),
        )
        message = amqpstorm.Message.create(
            channel=amqp_channel,
            body=event.as_json(),
            properties={"content_type": "application/json"},
        )
        message.publish(
            routing_key=event.routing_key(), exchange=publish_to_exchange
        )

        return
