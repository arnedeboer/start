# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from .. import entities
from ..entities import Country
from minty.repository import Repository
from sqlalchemy import sql
from zsnl_domains.database import schema

countries_list_query = sql.select(
    schema.CountryCode.uuid.label("uuid"),
    schema.CountryCode.label.label("name"),
    schema.CountryCode.dutch_code.label("code"),
).select_from(schema.CountryCode)


class CountryRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "Country"
    _events_to_calls: dict[str, str] = {}

    def get_countries_list(self, active: bool | None = None) -> list[Country]:
        """Get list of countries.
        :return: List[Country]
        :rtype: list
        """
        if active:
            query = countries_list_query.where(
                schema.CountryCode.historical.is_(False)
            )
            countries = self.session.execute(query).fetchall()
        else:
            countries = self.session.execute(countries_list_query).fetchall()

        return [self._entity_from_row(row=country) for country in countries]

    def _entity_from_row(self, row) -> Country:
        country = entities.Country(
            uuid=row.uuid,
            name=row.name,
            code=row.code,
        )
        return country
