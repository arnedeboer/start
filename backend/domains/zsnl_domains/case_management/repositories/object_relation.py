# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import ObjectRelation
from minty.repository import Repository
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


class ObjectRelationRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "ObjectRelation"
    _events_to_calls: dict[str, str] = {}

    def find_object_relations_for_case(
        self, case_uuid: UUID
    ) -> list[ObjectRelation]:
        """Get list of attributes of type relationship with custom object for a case."""
        attributes_query = sql.select(
            schema.BibliotheekKenmerk.id.label("attribute_id"),
            schema.BibliotheekKenmerk.magic_string,
            schema.BibliotheekKenmerk.uuid.label(
                "source_custom_field_type_id"
            ),
            schema.CustomObjectRelationship.uuid,
            schema.CustomObjectRelationship.custom_object_id,
            schema.CustomObject.uuid.label("custom_object_uuid"),
        ).select_from(
            sql.join(
                schema.ZaakKenmerk,
                schema.BibliotheekKenmerk,
                schema.ZaakKenmerk.bibliotheek_kenmerken_id
                == schema.BibliotheekKenmerk.id,
            )
            .join(
                schema.Case,
                schema.ZaakKenmerk.zaak_id == schema.Case.id,
            )
            .join(
                schema.CustomObjectRelationship,
                schema.BibliotheekKenmerk.id
                == schema.CustomObjectRelationship.source_custom_field_type_id,
                isouter=True,
            )
            .join(
                schema.CustomObject,
                schema.CustomObject.id
                == schema.CustomObjectRelationship.custom_object_id,
                isouter=True,
            )
        )
        query = attributes_query.where(
            sql.and_(
                schema.CustomObjectRelationship.related_uuid == case_uuid,
                schema.CustomObjectRelationship.relationship_type == "case",
                schema.BibliotheekKenmerk.value_type == "relationship",
                schema.BibliotheekKenmerk.relationship_type == "custom_object",
                schema.ZaakKenmerk.zaak_id
                == sql.select(schema.Case.id)
                .where(schema.Case.uuid == case_uuid)
                .scalar_subquery(),
            )
        )

        rows = self.session.execute(query).fetchall()

        return [self._entity_from_row(row=row) for row in rows]

    def _entity_from_row(self, row) -> ObjectRelation:
        result = ObjectRelation(
            attribute_id=row.attribute_id,
            magic_string=row.magic_string,
            source_custom_field_type_id=row.source_custom_field_type_id,
            uuid=row.uuid,
            custom_object_uuid=row.custom_object_uuid,
        )
        return result
