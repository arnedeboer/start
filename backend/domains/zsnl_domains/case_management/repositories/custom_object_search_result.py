# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import datetime
from ... import ZaaksysteemRepositoryBase
from ...shared.types import (
    ComparisonFilterCondition,
    ComparisonFilterOperator,
    FilterOperator,
)
from ..entities import custom_object_search_result as search_entities
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from minty.repository import Repository
from sqlalchemy import sql
from zsnl_domains.case_management.entities import total_result_count
from zsnl_domains.case_management.entities.custom_object import (
    AuthorizationLevel,
)
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.object_acl import (
    allowed_object_v2_subquery,
)
from zsnl_domains.shared.util import (
    escape_term_for_like,
    get_operator_values_from_filter,
)

co = sql.alias(schema.CustomObject, name="co")
ot = sql.alias(schema.CustomObjectType, name="ot")
otv = sql.alias(schema.CustomObjectTypeVersion, name="otv")
cov = sql.alias(schema.CustomObjectVersion, name="cov")
covc = sql.alias(schema.CustomObjectVersionContent, name="covc")

custom_object_search_query = sql.select(
    otv.c.uuid.label("object_type_uuid"),
    otv.c.name.label("object_type_name"),
    co.c.uuid.label("version_independent_uuid"),
    cov.c.uuid,
    cov.c.title,
    cov.c.subtitle,
    cov.c.external_reference,
    cov.c.status,
    cov.c.version,
    cov.c.date_created,
    cov.c.last_modified,
    cov.c.date_deleted,
    covc.c.archive_status,
    covc.c.archive_ground,
    covc.c.archive_retention,
    covc.c.custom_fields,
).select_from(
    sql.join(
        co,
        cov,
        sql.and_(
            cov.c.id == co.c.custom_object_version_id,
            sql.or_(
                cov.c.date_deleted.is_(None),
                cov.c.date_deleted > datetime.datetime.now(datetime.UTC),
            ),
        ),
    )
    .join(covc, cov.c.custom_object_version_content_id == covc.c.id)
    .join(otv, otv.c.id == cov.c.custom_object_type_version_id)
    .join(ot, ot.c.id == otv.c.custom_object_type_id)
)


SORT_ORDER = {
    search_entities.CustomObjectSearchOrder.title_asc: [
        sql.asc(cov.c.title),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.title_desc: [
        sql.desc(cov.c.title),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.subtitle_asc: [
        sql.asc(cov.c.subtitle),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.subtitle_desc: [
        sql.desc(cov.c.subtitle),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.external_reference_asc: [
        sql.asc(cov.c.external_reference),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.external_reference_desc: [
        sql.desc(cov.c.external_reference),
        sql.desc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.date_created_asc: [
        sql.asc(cov.c.date_created),
    ],
    search_entities.CustomObjectSearchOrder.date_created_desc: [
        sql.desc(cov.c.date_created),
    ],
    search_entities.CustomObjectSearchOrder.last_modified_asc: [
        sql.asc(cov.c.last_modified),
    ],
    search_entities.CustomObjectSearchOrder.last_modified_desc: [
        sql.desc(cov.c.last_modified),
    ],
}


class CustomObjectSearchResultRepository(
    Repository, ZaaksysteemRepositoryBase
):
    _for_entity = "CustomObjectSearchResult"

    def search(
        self,
        page: int,
        page_size: int,
        sort: search_entities.CustomObjectSearchOrder,
        filters: dict,
        user_info: UserInfo,
    ) -> EntityCollection[search_entities.CustomObjectSearchResult]:
        """Get list of Custom objects based on given filter."""

        query = self._search_query_stmt(
            filters,
            user_info,
        )

        offset = self._calculate_offset(page, page_size)
        sort_order = SORT_ORDER[sort]

        custom_objects_list = self.session.execute(
            query.order_by(*sort_order).limit(page_size).offset(offset)
        ).fetchall()

        rows = [self._entity_from_row(row=row) for row in custom_objects_list]
        return EntityCollection[search_entities.CustomObjectSearchResult](rows)

    def search_total_results_count(
        self,
        filters: dict,
        user_info: UserInfo,
    ) -> total_result_count.TotalResultCount:
        """Get total count of Custom objects based on given filter."""

        query = self._search_query_stmt(
            filters,
            user_info,
        )

        total_results = total_result_count.TotalResultCount(
            total_results=self._get_count(query)
        )
        return total_results

    def _search_query_stmt(
        self,
        filters: dict,
        user_info: UserInfo,
    ):
        # Update filters dict with only requested filters(Remove None)
        updated_filters = {k: v for k, v in filters.items() if v is not None}
        filters.clear()
        filters.update(updated_filters)

        custom_object_type_uuid = filters.get("custom_object_type_uuid")
        del filters["custom_object_type_uuid"]

        if not (operator := filters.get("operator")):
            operator = FilterOperator.and_operator
        else:
            del filters["operator"]

        query = custom_object_search_query.where(
            allowed_object_v2_subquery(
                user_info=user_info,
                authorization=AuthorizationLevel.read,
                object_alias=co,
            )
        )

        query = query.where(ot.c.uuid == custom_object_type_uuid)

        search_filters_subquery = []

        for filter in filters:
            search_filters_subquery.append(
                FILTERS_MAPPING[filter](self, filters[filter])
            )

        if search_filters_subquery:
            co_search_filters_query = self.apply_operator_to_filters(
                operator, search_filters_subquery
            )
            query = query.where(co_search_filters_query)

        return query

    def _apply_filter_status(self, filter):
        return cov.c.status.in_(filter)

    def _apply_filter_archive_status(self, filter):
        return covc.c.archive_status.in_(filter)

    def _apply_filter_last_modified(self, filter_last_modified):
        query = self.apply_comparison_filter(
            column=cov.c.last_modified,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(filter)
                for filter in filter_last_modified
            ],
        )
        return query

    def _apply_filter_keyword(self, filter_keyword):
        operator, keyword_values = get_operator_values_from_filter(
            filter_keyword
        )
        keyword_filters_subquery = []
        for keyword in keyword_values:
            keyword = f"%{escape_term_for_like(keyword)}%"
            keyword_filters_subquery.append(
                sql.or_(
                    cov.c.title.ilike(keyword, escape="~"),
                    cov.c.subtitle.ilike(keyword, escape="~"),
                    cov.c.external_reference.ilike(keyword, escape="~"),
                )
            )
        query = self.apply_operator_to_filters(
            operator_filters=operator,
            filter_subquery=keyword_filters_subquery,
        )
        return query

    def _apply_filter_title(self, filter_title):
        escaped_keyword = escape_term_for_like(filter_title)
        return cov.c.title.ilike(escaped_keyword, escape="~")

    def _apply_filter_subtitle(self, filter_subtitle):
        escaped_keyword = escape_term_for_like(filter_subtitle)
        return cov.c.subtitle.ilike(escaped_keyword, escape="~")

    def _apply_filter_external_reference(self, filter_external_reference):
        escaped_keyword = escape_term_for_like(filter_external_reference)
        return cov.c.external_reference.ilike(escaped_keyword, escape="~")

    def _apply_filter_attributes_value(self, filter_attributes_value):
        for attributes_value in filter_attributes_value:
            (
                comparison_filters,
                magic_string,
                custom_field_value,
            ) = self._get_values_from_attribute_filter(attributes_value)

            escaped_keyword = escape_term_for_like(custom_field_value)

            query = self._define_custom_fields_filter(
                comparison_filters=comparison_filters,
                magic_string=magic_string,
                custom_field_value=escaped_keyword,
            )
        return query

    def _entity_from_row(
        self, row
    ) -> search_entities.CustomObjectSearchResult:
        mapping = {
            "version_independent_uuid": "version_independent_uuid",
            "uuid": "uuid",
            "name": "object_type_name",
            "title": "title",
            "subtitle": "subtitle",
            "external_reference": "external_reference",
            "custom_fields": "custom_fields",
            "date_created": "date_created",
            "last_modified": "last_modified",
            "date_deleted": "date_deleted",
            "archive_status": "archive_status",
            "status": "status",
            "entity_id": "uuid",
            "entity_meta_summary": "title",
        }

        object_type_mapping = {
            "uuid": "object_type_uuid",
            "entity_id": "object_type_uuid",
        }

        entity_obj = {}
        for key, objkey in mapping.items():
            entity_obj[key] = getattr(row, objkey)

        entity_obj["custom_object_type"] = {}
        for key, objkey in object_type_mapping.items():
            entity_obj["custom_object_type"][key] = getattr(row, objkey)

        return search_entities.CustomObjectSearchResult.parse_obj(
            {**entity_obj, "_event_service": self.event_service}
        )

    def _define_custom_fields_filter(
        self, comparison_filters, magic_string, custom_field_value
    ):
        if comparison_filters == ComparisonFilterOperator.eq:
            query = (
                covc.c.custom_fields[magic_string]
                .op("->>")("value")
                .ilike(custom_field_value, escape="~")
            )

        elif comparison_filters == ComparisonFilterOperator.ne:
            query = sql.or_(
                sql.not_(
                    covc.c.custom_fields[magic_string]
                    .op("->>")("value")
                    .ilike(custom_field_value, escape="~")
                ),
                covc.c.custom_fields[magic_string]
                .op("->>")("value")
                .is_(None),
            )

        return query

    def _get_values_from_attribute_filter(self, attributes_value):
        comparison_filters = attributes_value.get(
            "operator", ComparisonFilterOperator.eq
        )
        magic_string = attributes_value.get("magic_string")
        custom_field_value = attributes_value.get("value")

        return comparison_filters, magic_string, custom_field_value


FILTERS_MAPPING = {
    "filter_status": CustomObjectSearchResultRepository._apply_filter_status,
    "filter_archive_status": CustomObjectSearchResultRepository._apply_filter_archive_status,
    "filter_last_modified": CustomObjectSearchResultRepository._apply_filter_last_modified,
    "filter_keyword": CustomObjectSearchResultRepository._apply_filter_keyword,
    "filter_title": CustomObjectSearchResultRepository._apply_filter_title,
    "filter_subtitle": CustomObjectSearchResultRepository._apply_filter_subtitle,
    "filter_external_reference": CustomObjectSearchResultRepository._apply_filter_external_reference,
    "filter_attributes_value": CustomObjectSearchResultRepository._apply_filter_attributes_value,
}
