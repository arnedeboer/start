# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import datetime
import html
import json
import pytz
import re
from ... import ZaaksysteemRepositoryBase
from ..constants import CASE_EVENTS, DOCUMENT_EVENTS
from ..entities import TimelineEntry
from collections.abc import Iterable
from datetime import timezone
from minty.cqrs import UserInfo
from minty.exceptions import NotFound
from minty.repository import Repository
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from uuid import UUID
from zsnl_domains.case_management.entities.case import ValidEventCategory
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)

EXPORT_PAGE_SIZE = 2500

custom_object_events_query = sql.select(
    schema.Logging.uuid.label("uuid"),
    schema.Logging.event_type.label("type"),
    schema.Logging.onderwerp.label("description"),
    schema.Logging.created_by_name_cache.label("user"),
    sql.func.coalesce(
        schema.Logging.created,
        schema.Logging.last_modified,
    ).label("created"),
    sql.cast(schema.Logging.event_data, postgresql.JSON)[
        "exception"
    ].astext.label("exception"),
    schema.Logging.zaak_id,
    schema.Logging.component,
    sql.cast(schema.Logging.event_data, postgresql.JSON)[
        "metadata"
    ].astext.label("metadata"),
).where(schema.Logging.component == "custom_object")


def _generate_content_for_document_events(entity_data):
    content = None
    document_metadata_mapping = {
        "description": "Omschrijving",
        "trust_level": "Vertrouwelijkheid",
        "document_category": "Documentcategorie",
        "origin": "Richting",
        "origin_date": "Ontvangst/verzenddatum",
        "pronom_format": "PRONOM formaat",
        "appearance": "Verschijningsvorm",
        "structure": "Structuur",
        "creation_date": "Aanmaakdatum",
    }

    if entity_data["type"] == "case/document/metadata/update":
        changes = []
        for key, value in json.loads(entity_data["metadata"]).items():
            changes.append(
                f'{document_metadata_mapping[key]} ingesteld op "{value}"'
            )

        content = "\n".join(changes)
        return content


def _generate_description_for_case_events(entity_data):
    description = entity_data["description"]
    attribute_value = entity_data["attribute_value"]

    if attribute_value and attribute_value != "":
        attribute_value = re.sub(re.compile("<.*?>"), " ", attribute_value)
        description = description + " : " + attribute_value
    return description


def _generate_content_for_case_events(entity_data):
    content = None
    changes = []
    if entity_data["type"] == "case/document/sign":
        changes.append(f'Koppelingnaam : {entity_data["interface_name"]}')
        changes.append(f'Documentnaam: {entity_data["document_name"]}')
        changes.append(f'Naam bij ValidSign: {entity_data["pkg_name"]}')
    elif entity_data["type"] == "case/postex/send":
        changes.append(f'Aan: {entity_data["recipient_display_name"]}')
        changes.append(f'Adres: {entity_data["recipient_address"]}')
        changes.append(f'Onderwerp: {entity_data["subject"]}')
        changes.append(f'{entity_data["body"]}')

    content = "\n".join(changes)

    return content


class TimelineEntryRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "TimelineEntry"
    _events_to_calls: dict[str, str] = {}

    def get_custom_object_event_logs(
        self,
        page: int,
        page_size: int,
        period_start: datetime.datetime | None,
        period_end: datetime.datetime | None,
        custom_object_uuid: UUID,
    ) -> list[TimelineEntry]:
        """Get list of Custom object event logs."""

        offset = self._calculate_offset(page, page_size)

        query = custom_object_events_query.where(
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "custom_object_uuid"
            ].astext
            == str(custom_object_uuid)
        )

        if period_start:
            query = query.where(schema.Logging.created >= period_start)

        if period_end:
            query = query.where(schema.Logging.created <= period_end)

        events = self.session.execute(
            query.order_by(schema.Logging.created.desc())
            .limit(page_size)
            .offset(offset)
        ).fetchall()

        return [self._entity_from_row(row=event_row) for event_row in events]

    def _get_subject_query(self, contact_type: str, contact_uuid: str):
        if contact_type == "person":
            return sql.select(
                sql.literal("natuurlijk_persoon").label("type"),
                schema.NatuurlijkPersoon.id,
            ).where(sql.and_(schema.NatuurlijkPersoon.uuid == contact_uuid))
        elif contact_type == "organization":
            return sql.select(
                sql.literal("bedrijf").label("type"), schema.Bedrijf.id
            ).where(sql.and_(schema.Bedrijf.uuid == contact_uuid))
        else:
            return sql.select(
                sql.literal("medewerker").label("type"), schema.Subject.id
            ).where(
                sql.and_(
                    schema.Subject.uuid == contact_uuid,
                    schema.Subject.subject_type == "employee",
                )
            )

    def get_contact_event_logs_generator(
        self,
        contact_type: str,
        contact_uuid: str,
        user_info: UserInfo,
        period_start: datetime.datetime | None = None,
        period_end: datetime.datetime | None = None,
    ) -> Iterable[TimelineEntry]:
        page = 1
        page_size = EXPORT_PAGE_SIZE

        while log_entries := self.get_contact_event_logs(
            contact_type=contact_type,
            contact_uuid=contact_uuid,
            user_info=user_info,
            period_start=period_start,
            period_end=period_end,
            page=page,
            page_size=page_size,
        ):
            self.logger.debug(
                "Exporting timeline entries for contact "
                f"{contact_uuid}; page {page}"
            )

            yield from log_entries
            page += 1

        self.logger.debug(
            f"Finished exporting timeline entries for contact {contact_uuid}"
        )

        return

    def get_contact_event_logs(
        self,
        contact_type: str,
        contact_uuid: str,
        user_info: UserInfo,
        period_start: datetime.datetime | None = None,
        period_end: datetime.datetime | None = None,
        page: int | None = None,
        page_size: int | None = None,
    ) -> list[TimelineEntry]:
        contact_query = self._get_subject_query(contact_type, contact_uuid)
        row = self.session.execute(contact_query).fetchone()

        if not row:
            raise NotFound(
                f"Contact of type '{contact_type}' not found for uuid '{contact_uuid}'",
                "contact/not_found",
            )

        contact_unique_name = "betrokkene-" + row.type + "-" + str(row.id)

        events = []

        contact_events_query = self._get_contact_events_query(
            contact_unique_name, contact_type, contact_uuid, user_info
        )

        if period_start:
            contact_events_query = contact_events_query.where(
                sql.literal_column("created") >= period_start
            )

        if period_end:
            contact_events_query = contact_events_query.where(
                sql.literal_column("created") <= period_end
            )

        if page and page_size:
            offset = self._calculate_offset(page, page_size)
            contact_events_query = contact_events_query.limit(
                page_size
            ).offset(offset)

        events = self.session.execute(contact_events_query).fetchall()

        return [self._entity_from_row(row=event_row) for event_row in events]

    def _entity_from_row(self, row) -> TimelineEntry:
        mapping = {
            "entity_id": "uuid",
            "uuid": "uuid",
            "type": "type",
            "date": "created",
            "description": "description",
            "exception": "exception",
            "entity_meta_summary": "description",
            "user": "user",
            "component": "component",
            "metadata": "metadata",
        }

        entity_data = {}
        for key, objkey in mapping.items():
            entity_data[key] = getattr(row, objkey)

        entity_data["description"] = html.unescape(entity_data["description"])
        entity_data["entity_meta_summary"] = html.unescape(
            entity_data["entity_meta_summary"]
        )

        entity_data["interface_name"] = getattr(row, "interface_name", None)
        entity_data["document_name"] = getattr(row, "document_name", None)
        entity_data["pkg_name"] = getattr(row, "pkg_name", None)
        entity_data["recipient_display_name"] = getattr(
            row, "recipient_display_name", None
        )
        entity_data["recipient_address"] = getattr(
            row, "recipient_address", None
        )
        entity_data["subject"] = getattr(row, "subject", None)
        entity_data["body"] = getattr(row, "body", None)

        case_mapping = {
            "case_id": "zaak_id",
            "entity_id": "case_uuid",
            "entity_meta_summary": "zaak_id",
        }

        if row.zaak_id is not None:
            entity_data["case"] = {}
            for key, objkey in case_mapping.items():
                entity_data["case"][key] = getattr(row, objkey)

        if getattr(row, "comment", None):
            entity_data["description"] += ", reden: " + str(row.comment)

        if entity_data["date"] is not None:
            display_date = (
                entity_data["date"]
                .replace(tzinfo=timezone.utc)
                .astimezone(tz=pytz.timezone("Europe/Amsterdam"))
            )

            entity_data["date"] = display_date

        if entity_data["type"] == "case/attribute/update":
            entity_data["attribute_value"] = getattr(
                row, "attribute_value", None
            )
            entity_data["description"] = _generate_description_for_case_events(
                entity_data
            )

        if entity_data["component"] == "document":
            entity_data["content"] = _generate_content_for_document_events(
                entity_data
            )

        if entity_data["component"] == "zaak":
            entity_data["content"] = _generate_content_for_case_events(
                entity_data
            )
        return TimelineEntry.parse_obj(
            {**entity_data, "_event_service": self.event_service}
        )

    def _get_contact_events_query(
        self,
        contact_unique_name: str,
        contact_type: str,
        contact_uuid: str,
        user_info: UserInfo,
    ):
        event_types = [
            "auth/alternative",
            "auth/alternative/account",
            "auth/alternative/activate",
            "auth/alternative/mail",
            "auth/alternative/username",
            "case/accept",
            "case/attribute/update",
            "case/create",
            "case/close",
            "case/contact_moment/created",
            "case/checklist/item/update",
            "case/document/assign",
            "case/document/create",
            "case/document/label",
            "case/document/metadata/update",
            "case/document/publish",
            "case/document/reject",
            "case/document/rename",
            "case/document/trash",
            "case/document/unpublish",
            "case/document/sign",
            "case/email/created",
            "case/note/create",
            "case/pip/feedback",
            "case/postex/send",
            "case/relation/update",
            "case/update/purge_date",
            "case/update/registration_date",
            "case/update/status",
            "case/update/subject",
            "case/update/target_date",
            "case/update/allocation",
            "document/assign",
            "document/create",
            "document/delete_document",
            "document/metadata/update",
            "email/send",
            "object/view",
            "subject/contactmoment/create",
            "subject/create",
            "subject/inspect",
            "subject/note/create",
            "subject/update",
            "subject/update_contact_data",
        ]

        contact_entries = sql.select(
            schema.Logging.uuid.label("uuid"),
            schema.Logging.event_type.label("type"),
            schema.Logging.onderwerp.label("description"),
            schema.Logging.created_by_name_cache.label("user"),
            sql.func.coalesce(
                schema.Logging.created,
                schema.Logging.last_modified,
            ).label("created"),
            schema.Logging.zaak_id,
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "comment"
            ].astext.label("comment"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "exception"
            ].astext.label("exception"),
            sql.literal(None).label("case_uuid"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "attribute_value"
            ].astext.label("attribute_value"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "metadata"
            ].astext.label("metadata"),
            schema.Logging.component,
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "interface_name"
            ].astext.label("interface_name"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "document_name"
            ].astext.label("document_name"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)["pkg"][
                "name"
            ].astext.label("pkg_name"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)["recipient"][
                "display_name"
            ].astext.label("recipient_display_name"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)["recipient"][
                "address"
            ].astext.label("recipient_address"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "subject"
            ].astext.label("subject"),
            sql.cast(schema.Logging.event_data, postgresql.JSON)[
                "body"
            ].astext.label("body"),
        ).where(
            sql.and_(
                schema.Logging.restricted.is_(False),
                schema.Logging.event_type.in_(event_types),
                sql.or_(
                    sql.cast(schema.Logging.event_data, postgresql.JSON)[
                        "subject_id"
                    ].astext
                    == contact_unique_name,
                    schema.Logging.created_for == contact_unique_name,
                ),
            )
        )

        natuurlijk_persoon_for_case_entries = (
            sql.select(
                schema.Logging.uuid.label("uuid"),
                schema.Logging.event_type.label("type"),
                schema.Logging.onderwerp.label("description"),
                schema.Logging.created_by_name_cache.label("user"),
                sql.func.coalesce(
                    schema.Logging.created,
                    schema.Logging.last_modified,
                ).label("created"),
                schema.Logging.zaak_id,
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "comment"
                ].astext.label("comment"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "exception"
                ].astext.label("exception"),
                schema.Case.uuid.label("case_uuid"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "attribute_value"
                ].astext.label("attribute_value"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "metadata"
                ].astext.label("metadata"),
                schema.Logging.component,
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "interface_name"
                ].astext.label("interface_name"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "document_name"
                ].astext.label("document_name"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)["pkg"][
                    "name"
                ].astext.label("pkg_name"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "recipient"
                ]["display_name"].astext.label("recipient_display_name"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "recipient"
                ]["address"].astext.label("recipient_address"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "subject"
                ].astext.label("subject"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "body"
                ].astext.label("body"),
            )
            .select_from(
                sql.join(
                    schema.Logging,
                    schema.Case,
                    sql.and_(
                        schema.Logging.zaak_id == schema.Case.id,
                        user_allowed_cases_subquery(user_info, "read"),
                    ),
                ).join(
                    schema.ZaakBetrokkenen,
                    sql.and_(
                        schema.Case.aanvrager == schema.ZaakBetrokkenen.id,
                        schema.ZaakBetrokkenen.subject_id == contact_uuid,
                    ),
                )
            )
            .where(
                sql.and_(
                    schema.Logging.restricted.is_(False),
                    schema.Logging.event_type.in_(event_types),
                )
            )
        )
        uni = sql.union(
            contact_entries, natuurlijk_persoon_for_case_entries
        ).cte()
        return sql.select(uni).order_by(sql.literal_column("created").desc())

    def _get_case_events_query(
        self,
        case_uuid: UUID,
        user_info: UserInfo,
    ):
        case_events_query = (
            sql.select(
                schema.Logging.uuid.label("uuid"),
                schema.Logging.event_type.label("type"),
                schema.Logging.onderwerp.label("description"),
                schema.Logging.created_by_name_cache.label("user"),
                sql.func.coalesce(
                    schema.Logging.created,
                    schema.Logging.last_modified,
                ).label("created"),
                schema.Logging.zaak_id,
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "comment"
                ].astext.label("comment"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "exception"
                ].astext.label("exception"),
                schema.Case.uuid.label("case_uuid"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "attribute_value"
                ].astext.label("attribute_value"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "metadata"
                ].astext.label("metadata"),
                schema.Logging.component,
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "interface_name"
                ].astext.label("interface_name"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "document_name"
                ].astext.label("document_name"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)["pkg"][
                    "name"
                ].astext.label("pkg_name"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "recipient"
                ]["display_name"].astext.label("recipient_display_name"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "recipient"
                ]["address"].astext.label("recipient_address"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "subject"
                ].astext.label("subject"),
                sql.cast(schema.Logging.event_data, postgresql.JSON)[
                    "body"
                ].astext.label("body"),
            )
            .select_from(
                sql.join(
                    schema.Logging,
                    schema.Case,
                    schema.Logging.zaak_id == schema.Case.id,
                )
            )
            .where(
                sql.and_(
                    schema.Case.uuid == case_uuid,
                    user_allowed_cases_subquery(user_info, "read"),
                )
            )
        )
        return case_events_query

    def get_case_event_logs_generator(
        self,
        period_start: datetime.datetime | None,
        period_end: datetime.datetime | None,
        case_uuid: UUID,
        user_info: UserInfo,
    ) -> Iterable[TimelineEntry]:
        page = 1
        page_size = EXPORT_PAGE_SIZE

        while log_entries := self.get_case_event_logs(
            period_start=period_start,
            period_end=period_end,
            case_uuid=case_uuid,
            user_info=user_info,
            page=page,
            page_size=page_size,
        ):
            self.logger.debug(
                f"Exporting timeline entries for case {case_uuid}; page {page}"
            )
            yield from log_entries
            page += 1

        self.logger.debug(
            f"Finished exporting timeline entries for case {case_uuid}"
        )
        return

    def get_case_event_logs(
        self,
        period_start: datetime.datetime | None,
        period_end: datetime.datetime | None,
        case_uuid: UUID,
        user_info: UserInfo,
        page: int | None = None,
        page_size: int | None = None,
        filter_attributes_category: set[ValidEventCategory] | None = None,
    ) -> list[TimelineEntry]:
        query = self._get_case_events_query(
            case_uuid=case_uuid, user_info=user_info
        )

        if filter_attributes_category:
            query = self._apply_attributes_category_filter(
                query, filter_attributes_category
            )
        if period_start:
            query = query.where(schema.Logging.created >= period_start)

        if period_end:
            query = query.where(schema.Logging.created <= period_end)

        query = query.order_by(schema.Logging.created.desc())
        if page and page_size:
            offset = self._calculate_offset(page, page_size)
            query = query.limit(page_size).offset(offset)

        events = self.session.execute(query).fetchall()

        return [self._entity_from_row(row=event_row) for event_row in events]

    def _apply_attributes_category_filter(
        self, query, filter_attributes_category: set[ValidEventCategory]
    ):
        include_types = []
        if "document" in filter_attributes_category:
            include_types += DOCUMENT_EVENTS

        elif "case_update" in filter_attributes_category:
            include_types += CASE_EVENTS

        query = query.where(schema.Logging.event_type.in_(include_types))
        return query
