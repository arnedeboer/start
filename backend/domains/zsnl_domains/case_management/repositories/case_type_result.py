# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import CaseTypeResult
from minty import repository
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from minty.exceptions import NotFound
from sqlalchemy import sql
from sqlalchemy.engine.row import Row
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)

CASE_ALIAS = sql.alias(schema.Case)
ZAAK_TYPE_RESULTATEN_ALIAS = sql.alias(schema.ZaaktypeResultaten)


class CaseTypeResultRepository(
    repository.Repository, ZaaksysteemRepositoryBase
):
    ZAAK_TYPE_RESULT_BASE_QUERY = sql.select(
        ZAAK_TYPE_RESULTATEN_ALIAS.c.uuid.label("uuid"),
        ZAAK_TYPE_RESULTATEN_ALIAS.c.label.label("name"),
        ZAAK_TYPE_RESULTATEN_ALIAS.c.resultaat.label("result"),
        ZAAK_TYPE_RESULTATEN_ALIAS.c.trigger_archival.label(
            "trigger_archival"
        ),
        schema.ResultPreservationTerms.label.label("preservation_term_label"),
        schema.ResultPreservationTerms.unit.label("preservation_term_unit"),
        schema.ResultPreservationTerms.unit_amount.label(
            "preservation_term_unit_amount"
        ),
    ).select_from(
        sql.join(
            ZAAK_TYPE_RESULTATEN_ALIAS,
            CASE_ALIAS,
            ZAAK_TYPE_RESULTATEN_ALIAS.c.zaaktype_node_id
            == CASE_ALIAS.c.zaaktype_node_id,
        ).join(
            schema.ResultPreservationTerms,
            schema.ResultPreservationTerms.code
            == ZAAK_TYPE_RESULTATEN_ALIAS.c.bewaartermijn,
        )
    )

    _for_entity = "CaseTypeResult"
    _events_to_calls = {"SavedSearchCreated": "_save_create"}

    def get_multiple(
        self, case_uuid: UUID, user_info: UserInfo, permission
    ) -> EntityCollection[CaseTypeResult]:
        multiple_query = self.ZAAK_TYPE_RESULT_BASE_QUERY.where(
            sql.and_(
                CASE_ALIAS.c.uuid == case_uuid,
                CASE_ALIAS.c.status != "deleted",
            )
        )
        multiple_query = multiple_query.where(
            sql.and_(
                user_allowed_cases_subquery(
                    user_info=user_info,
                    permission=permission,
                    case_alias=CASE_ALIAS.c,
                ),
            )
        )

        query_result = self.session.execute(multiple_query).fetchall()
        entities = [self._inflate(row) for row in query_result]
        return EntityCollection(entities=entities)

    def get(
        self,
        case_uuid: UUID,
        case_type_result_uuid: UUID,
        user_info: UserInfo,
        permission,
    ) -> CaseTypeResult:
        single_query = self.ZAAK_TYPE_RESULT_BASE_QUERY.where(
            sql.and_(
                CASE_ALIAS.c.uuid == case_uuid,
                CASE_ALIAS.c.status != "deleted",
                ZAAK_TYPE_RESULTATEN_ALIAS.c.uuid == case_type_result_uuid,
            )
        )
        single_query = single_query.where(
            sql.and_(
                user_allowed_cases_subquery(
                    user_info=user_info,
                    permission=permission,
                    case_alias=CASE_ALIAS.c,
                ),
            )
        )

        row = self.session.execute(single_query).fetchone()
        if not row:
            raise NotFound(
                f"case_type_result not found {case_type_result_uuid} for case_uuid {case_uuid}"
            )
        return self._inflate(row)

    def _inflate(self, row: Row) -> CaseTypeResult:
        return CaseTypeResult(
            uuid=row.uuid,
            name=row.name,
            result=row.result,
            trigger_archival=row.trigger_archival,
            preservation_term_label=row.preservation_term_label,
            preservation_term_unit=row.preservation_term_unit,
            preservation_term_unit_amount=row.preservation_term_unit_amount,
        )
