# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


from ... import ZaaksysteemRepositoryBase
from ..entities import PersonSensitiveData
from ..entities.person_sensitive_data import PersonSensitiveDataType
from minty.exceptions import NotFound
from minty.repository import Repository
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema

person_info_query = (
    sql.select(
        schema.NatuurlijkPersoon.uuid,
        schema.NatuurlijkPersoon.burgerservicenummer.label("personal_number"),
        schema.NatuurlijkPersoon.persoonsnummer.label("sedula_number"),
    )
    .where(schema.NatuurlijkPersoon.deleted_on.is_(None))
    .select_from(
        sql.join(
            schema.NatuurlijkPersoon,
            schema.Subject,
            schema.Subject.uuid == schema.NatuurlijkPersoon.uuid,
            isouter=True,
        )
    )
)


class PersonSensitiveDataRepository(Repository, ZaaksysteemRepositoryBase):
    def find_person_sensitive_data_by_uuid(
        self, uuid: UUID, sensitive_data: str
    ) -> PersonSensitiveData:
        """Find sensitive information for a person by uuid."""
        qry_stmt = person_info_query.where(
            schema.NatuurlijkPersoon.uuid == uuid
        )

        query_result = self.session.execute(qry_stmt).fetchone()
        if not query_result:
            raise NotFound(
                f"Person with uuid '{uuid}' not found.", "person/not_found"
            )

        return self._sqla_to_entity(
            query_result=query_result, sensitive_data=sensitive_data
        )

    def _sqla_to_entity(
        self, query_result, sensitive_data
    ) -> PersonSensitiveData:
        person_sensitive_data = PersonSensitiveData(
            entity_id=query_result.uuid, uuid=query_result.uuid
        )

        if sensitive_data == PersonSensitiveDataType.personal_number:
            person_sensitive_data.personal_number = (
                query_result.personal_number
            )
        elif sensitive_data == PersonSensitiveDataType.sedula_number:
            person_sensitive_data.sedula_number = query_result.sedula_number

        return person_sensitive_data
