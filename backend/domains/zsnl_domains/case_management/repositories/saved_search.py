# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
import minty.exceptions
from ... import ZaaksysteemRepositoryBase
from ..entities import saved_search as entity
from ..entities import saved_search_label
from collections.abc import Sequence
from minty import repository
from sqlalchemy import exc as sqlalchemy_exc
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared import util as zsnl_util


def _unpack_permissions(permissions: list[dict]):
    unpacked = set()

    for permission in permissions:
        for p in permission["permission"]:
            unpacked.add((permission["group_id"], permission["role_id"], p))

    return unpacked


def _saved_search_authorizations_subquery(user_uuid: UUID):
    return (
        sql.select(
            sql.case(
                # Map database values to allowed "API" values for authorization
                # levels
                (
                    schema.SavedSearchPermission.permission == "read",
                    sql.literal("read"),
                ),
                (
                    schema.SavedSearchPermission.permission == "write",
                    sql.literal("readwrite"),
                ),
            )
        )
        .select_from(
            sql.join(
                schema.SavedSearchPermission,
                schema.SubjectPositionMatrix,
                sql.and_(
                    schema.SavedSearchPermission.group_id
                    == schema.SubjectPositionMatrix.group_id,
                    schema.SavedSearchPermission.role_id
                    == schema.SubjectPositionMatrix.role_id,
                ),
            )
        )
        .where(
            schema.SavedSearchPermission.saved_search_id
            == schema.SavedSearch.id
        )
        .group_by(schema.SavedSearchPermission.permission)
        .scalar_subquery()
    )


saved_search_permissions = (
    sql.select(
        schema.SavedSearchPermission.saved_search_id,
        sql.func.array_agg(
            sql.func.json_build_object(
                "group_id",
                schema.Group.uuid,
                "role_id",
                schema.Role.uuid,
                "permission",
                schema.SavedSearchPermission.permission,
            )
        ).label("permission_rows"),
    )
    .select_from(
        sql.join(
            schema.SavedSearchPermission,
            schema.Group,
            schema.SavedSearchPermission.group_id == schema.Group.id,
        ).join(
            schema.Role, schema.SavedSearchPermission.role_id == schema.Role.id
        )
    )
    .where(
        schema.SavedSearchPermission.saved_search_id == schema.SavedSearch.id
    )
    .group_by(schema.SavedSearchPermission.saved_search_id)
    .lateral()
)

saved_search_labels = (
    sql.select(
        schema.SavedSearchLabelsMapping.saved_search_id,
        sql.func.array_agg(
            sql.func.json_build_object(
                "uuid",
                schema.SavedSearchLabels.uuid,
                "name",
                schema.SavedSearchLabels.label,
            )
        ).label("labels"),
    )
    .select_from(
        sql.join(
            schema.SavedSearchLabels,
            schema.SavedSearchLabelsMapping,
            schema.SavedSearchLabels.id
            == schema.SavedSearchLabelsMapping.label_id,
        )
    )
    .where(
        schema.SavedSearchLabelsMapping.saved_search_id
        == schema.SavedSearch.id
    )
    .group_by(schema.SavedSearchLabelsMapping.saved_search_id)
    .lateral()
)


def get_saved_search_query(user_uuid: UUID, permission: str = "read"):
    subject_subquery = (
        sql.select(schema.Subject.id)
        .where(schema.Subject.subject_type == "employee")
        .where(schema.Subject.uuid == user_uuid)
        .scalar_subquery()
    )

    subject_a = sql.alias(schema.Subject)
    subject_b = sql.alias(schema.Subject)

    return (
        sql.select(
            schema.SavedSearch.uuid,
            schema.SavedSearch.name,
            schema.SavedSearch.kind,
            sql.func.json_build_object(
                "uuid",
                subject_a.c.uuid,
                "summary",
                sql.cast(subject_a.c.properties, postgresql.JSON)[
                    "displayname"
                ].astext.label("summary"),
            ).label("owner"),
            sql.func.json_build_object(
                "uuid",
                subject_b.c.uuid,
                "summary",
                sql.cast(subject_b.c.properties, postgresql.JSON)[
                    "displayname"
                ].astext.label("summary"),
            ).label("updated_by"),
            schema.SavedSearch.filters,
            schema.SavedSearch.columns,
            schema.SavedSearch.sort_column,
            schema.SavedSearch.sort_order,
            schema.SavedSearch.date_created,
            schema.SavedSearch.date_updated,
            sql.func.coalesce(
                saved_search_permissions.c.permission_rows,
                sql.cast(
                    postgresql.array([]), postgresql.ARRAY(postgresql.JSON)
                ),
            ).label("permission_rows"),
            saved_search_labels.c.labels.label("labels"),
            sql.func.array(
                _saved_search_authorizations_subquery(
                    user_uuid=user_uuid,
                )
            ).label("authorizations"),
            sql.case(
                (
                    schema.SavedSearch.owner_id == subject_subquery,
                    sql.literal(True),
                ),
                else_=sql.literal(False),
            ).label("user_is_owner"),
            saved_search_labels.c.labels.label("labels"),
        )
        .select_from(
            sql.join(
                schema.SavedSearch,
                subject_a,
                schema.SavedSearch.owner_id == subject_a.c.id,
            )
            .join(
                subject_b,
                schema.SavedSearch.updated_by == subject_b.c.id,
                isouter=True,
            )
            .join(
                saved_search_permissions,
                schema.SavedSearch.id
                == saved_search_permissions.c.saved_search_id,
                isouter=True,
            )
            .join(
                saved_search_labels,
                saved_search_labels.c.saved_search_id == schema.SavedSearch.id,
                isouter=True,
            )
        )
        .where(
            sql.or_(
                schema.SavedSearch.owner_id == subject_subquery,
                schema.SavedSearch.permissions.overlap(
                    sql.func.array(
                        sql.select(
                            schema.SubjectPositionMatrix.position
                            + f"|{permission}"
                        )
                        .where(
                            schema.SubjectPositionMatrix.subject_id
                            == subject_subquery
                        )
                        .scalar_subquery()
                    )
                ),
            )
        )
    )


class SavedSearchRepository(repository.Repository, ZaaksysteemRepositoryBase):
    _for_entity = "SavedSearch"
    _events_to_calls = {
        "SavedSearchCreated": "_save_create",
        "SavedSearchDeleted": "_save_delete",
        "SavedSearchUpdated": "_save_update",
        "SavedSearchPermissionsUpdated": "_save_update_permissions",
        "SavedSearchLabelsApplied": "_set_labels",
    }

    def get(
        self, uuid: UUID, user_info: minty.cqrs.UserInfo
    ) -> entity.SavedSearch:
        saved_search_query = get_saved_search_query(user_info.user_uuid)

        result = self.session.execute(
            saved_search_query.where(schema.SavedSearch.uuid == uuid)
        ).fetchone()

        if not result:
            raise minty.exceptions.NotFound(
                "No saved search found with that UUID",
                "saved_search/not_found",
            )

        return self._inflate(result)

    def get_multiple(
        self,
        page: int,
        page_size: int,
        user_info: minty.cqrs.UserInfo,
        kind: entity.SavedSearchKind | None = None,
        labels: list[str] | None = None,
    ) -> minty.entity.EntityCollection[entity.SavedSearch]:
        offset = self._calculate_offset(page, page_size)

        query = get_saved_search_query(user_info.user_uuid)

        if kind:
            query = query.where(schema.SavedSearch.kind == kind)

        if labels:
            query = query.where(
                sql.exists(
                    sql.select(sql.literal(1))
                    .select_from(
                        sql.join(
                            schema.SavedSearchLabelsMapping,
                            schema.SavedSearchLabels,
                            sql.and_(
                                schema.SavedSearchLabelsMapping.saved_search_id
                                == schema.SavedSearch.id,
                                schema.SavedSearchLabelsMapping.label_id
                                == schema.SavedSearchLabels.id,
                            ),
                        )
                    )
                    .where(schema.SavedSearchLabels.label.in_(labels)),
                )
            )
        with zsnl_util.TimedInMilliseconds(
            "saved_search_get_multiple", self.MAX_QUERYTIME_COUNT_MS
        ) as executed_in_time:
            result = self.session.execute(
                query.order_by(schema.SavedSearch.name)
                .limit(page_size)
                .offset(offset)
            ).fetchall()

        total_results = None
        if executed_in_time():
            total_results = self._get_count(query)

        entities = [self._inflate(row) for row in result]

        return minty.entity.EntityCollection(
            entities=entities, total_results=total_results
        )

    def _assert_valid_permissions(
        self, permissions: list[entity.SavedSearchPermissionDefinition]
    ) -> None:
        for permission in permissions:
            if permission.permission not in [
                {entity.SavedSearchPermission.read},  # read-only
                {
                    entity.SavedSearchPermission.read,
                    entity.SavedSearchPermission.write,
                },  # read + write
                # "write only" is not allowed, neither is an empty set
            ]:
                raise minty.exceptions.Conflict(
                    "Invalid combination of permissions for group/role "
                    f"{permission.group_id}/{permission.role_id}",
                    "saved_search/create/invalid_permission",
                )

    def create(
        self,
        uuid: UUID,
        name: str,
        kind: entity.SavedSearchKind,
        owner: UUID,
        filters: (
            entity.CustomObjectSearchFilterDefinition
            | entity.CaseSearchFilterDefinition
        ),
        permissions: list[entity.SavedSearchPermissionDefinition],
        columns: list[entity.SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: entity.SortOrder,
    ):
        self._assert_valid_permissions(permissions=permissions)

        employee = self._get_owner_entity(owner)

        search = entity.SavedSearch.create(
            uuid=uuid,
            name=name,
            kind=kind,
            owner=employee,
            filters=filters,
            permissions=permissions,
            columns=columns,
            sort_column=sort_column,
            sort_order=sort_order,
            event_service=self.event_service,
        )

        return search

    def _get_owner_entity(self, owner_uuid: UUID):
        employee = self.session.execute(
            sql.select(
                sql.cast(schema.Subject.properties, postgresql.JSON)[
                    "displayname"
                ].astext.label("summary")
            )
            .where(schema.Subject.subject_type == "employee")
            .where(schema.Subject.uuid == owner_uuid)
        ).fetchone()

        return entity.SavedSearchOwner.parse_obj(
            {
                "uuid": owner_uuid,
                "entity_id": owner_uuid,
                "entity_meta_summary": employee.summary,
            }
        )

    def _inflate(self, db_row) -> entity.SavedSearch:
        labels = []
        if db_row.labels:
            for label in db_row.labels:
                labels.append(
                    saved_search_label.SavedSearchLabel(
                        entity_id=label["uuid"],
                        uuid=label["uuid"],
                        name=label["name"],
                    )
                )
        if db_row.user_is_owner:
            authorizations = [
                entity.AuthorizationLevel.read,
                entity.AuthorizationLevel.readwrite,
                entity.AuthorizationLevel.admin,
            ]
        else:
            authorizations = db_row.authorizations

        permissions = {}
        for row in db_row.permission_rows:
            key = (row["group_id"], row["role_id"])
            if key not in permissions:
                permissions[key] = {
                    "group_id": row["group_id"],
                    "role_id": row["role_id"],
                    "permission": set(),
                }

            permissions[key]["permission"] |= {row["permission"]}

        filters = db_row.filters
        if "filter_type" not in filters:
            filters["filter_type"] = db_row.kind

        if updated_by := db_row.updated_by:
            updated_by = {
                "entity_id": updated_by["uuid"],
                "uuid": updated_by["uuid"],
                "summary": updated_by["summary"],
            }

        return entity.SavedSearch.parse_obj(
            {
                "entity_id": db_row.uuid,
                "uuid": db_row.uuid,
                "name": db_row.name,
                "kind": db_row.kind,
                "owner": {
                    "entity_id": db_row.owner["uuid"],
                    "uuid": db_row.owner["uuid"],
                    "entity_meta_summary": db_row.owner["summary"],
                },
                "updated_by": updated_by,
                "permissions": list(permissions.values()),
                "filters": filters,
                "labels": labels,
                "columns": db_row.columns,
                "sort_column": db_row.sort_column,
                "sort_order": db_row.sort_order,
                "entity_meta_authorizations": authorizations,
                "_event_service": self.event_service,
                "date_created": db_row.date_created,
                "date_updated": db_row.date_updated,
            }
        )

    def _save_create(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = event.format_changes()
        filters = changes["filters"]
        updated_filters = {k: v for k, v in filters.items() if v is not None}

        # Create the Saved Search itself
        try:
            saved_search_id = (
                self.session.execute(
                    sql.insert(schema.SavedSearch)
                    .values(
                        uuid=event.entity_id,
                        name=changes["name"],
                        kind=changes["kind"],
                        owner_id=sql.select(schema.Subject.id)
                        .where(schema.Subject.subject_type == "employee")
                        .where(schema.Subject.uuid == changes["owner"]["uuid"])
                        .scalar_subquery(),
                        filters=updated_filters,
                        columns=changes["columns"],
                        sort_column=changes["sort_column"],
                        sort_order=changes["sort_order"],
                        date_created=changes["date_created"],
                    )
                    .returning(schema.SavedSearch.id)
                )
                .fetchone()
                .id
            )
        except sqlalchemy_exc.IntegrityError as e:
            raise minty.exceptions.Conflict(
                "A saved search already exists with the specified UUID or "
                "name and owner",
                "saved_search/not_unique",
            ) from e

        # Create records for the permissions ("ACL")
        permissions_to_insert = []
        for permission in changes["permissions"]:
            for authorization in permission["permission"]:
                permissions_to_insert.append(
                    {
                        "saved_search_id": saved_search_id,
                        "group_id": sql.select(schema.Group.id)
                        .where(schema.Group.uuid == permission["group_id"])
                        .scalar_subquery(),
                        "role_id": sql.select(schema.Role.id)
                        .where(schema.Role.uuid == permission["role_id"])
                        .scalar_subquery(),
                        "permission": authorization,
                    }
                )

            self.session.execute(
                sql.insert(schema.SavedSearchPermission).values(
                    permissions_to_insert
                )
            )

        # Update the `permissions` column in `saved_search` based on the
        # `saved_search_permission` rows we just inserted.
        self.session.execute(
            sql.text("CALL saved_search_permission_sync(:saved_search_id)"),
            {"saved_search_id": saved_search_id},
        )

    def _save_delete(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        self.session.execute(
            sql.delete(schema.SavedSearch).where(
                schema.SavedSearch.uuid == event.entity_id
            )
        )

        self._set_labels(event=event, userinfo=userinfo)

    def _save_update(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = event.format_changes()
        filters = changes["filters"]
        updated_filters = {k: v for k, v in filters.items() if v is not None}

        try:
            self.session.execute(
                sql.update(schema.SavedSearch)
                .where(schema.SavedSearch.uuid == event.entity_id)
                .values(
                    name=changes["name"],
                    filters=updated_filters,
                    columns=changes["columns"],
                    sort_column=changes["sort_column"],
                    sort_order=changes["sort_order"],
                    date_updated=changes["date_updated"],
                    updated_by=sql.select(schema.Subject.id)
                    .where(schema.Subject.subject_type == "employee")
                    .where(
                        schema.Subject.uuid == changes["updated_by"]["uuid"]
                    )
                    .scalar_subquery(),
                )
            )
        except sqlalchemy_exc.IntegrityError as e:
            raise minty.exceptions.Conflict(
                "A saved search already exists with the specified name and owner",
                "saved_search/not_unique",
            ) from e

    def _save_update_permissions(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        old_permissions = _unpack_permissions(
            event.previous_value("permissions")
        )
        new_permissions = _unpack_permissions(event.new_value("permissions"))

        to_add = new_permissions - old_permissions
        to_remove = old_permissions - new_permissions

        saved_search_id = (
            sql.select(schema.SavedSearch.id)
            .where(schema.SavedSearch.uuid == event.entity_id)
            .scalar_subquery()
        )

        if to_add:
            self.session.execute(
                sql.insert(schema.SavedSearchPermission).values(
                    [
                        {
                            "saved_search_id": saved_search_id,
                            "group_id": sql.select(schema.Group.id)
                            .where(schema.Group.uuid == p[0])
                            .scalar_subquery(),
                            "role_id": sql.select(schema.Role.id)
                            .where(schema.Role.uuid == p[1])
                            .scalar_subquery(),
                            "permission": p[2],
                        }
                        for p in to_add
                    ]
                )
            )

        if to_remove:
            delete_conditions = [
                sql.and_(
                    schema.SavedSearchPermission.group_id
                    == sql.select(schema.Group.id)
                    .where(schema.Group.uuid == p[0])
                    .scalar_subquery(),
                    schema.SavedSearchPermission.role_id
                    == sql.select(schema.Role.id)
                    .where(schema.Role.uuid == p[1])
                    .scalar_subquery(),
                    schema.SavedSearchPermission.permission == p[2],
                )
                for p in to_remove
            ]

            self.session.execute(
                sql.delete(schema.SavedSearchPermission)
                .where(
                    sql.and_(
                        schema.SavedSearchPermission.saved_search_id
                        == saved_search_id,
                        sql.or_(*delete_conditions),
                    )
                )
                .execution_options(synchronize_session=False)
            )

        if to_add or to_remove:
            id = self.session.execute(
                sql.select(schema.SavedSearch.id).where(
                    schema.SavedSearch.uuid == event.entity_id
                )
            ).scalar()

            self.session.execute(
                sql.text(
                    "CALL saved_search_permission_sync(:saved_search_id)"
                ),
                {"saved_search_id": id},
            )

    def _set_labels(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        old_labels = event.previous_value("labels")
        new_labels = event.new_value("labels")

        (labels_to_add, labels_to_remove) = self.__diff_saved_search_labels(
            old_labels, new_labels
        )
        for label in labels_to_add:
            self._apply_label(saved_search_uuid=event.entity_id, label=label)

        for label in labels_to_remove:
            self._remove_label(saved_search_uuid=event.entity_id, label=label)

    def _apply_label(self, saved_search_uuid: UUID, label):
        label_id = (
            sql.select(schema.SavedSearchLabels.id)
            .where(schema.SavedSearchLabels.uuid == label["uuid"])
            .scalar_subquery()
        )

        saved_search_id = (
            sql.select(schema.SavedSearch.id)
            .where(schema.SavedSearch.uuid == saved_search_uuid)
            .scalar_subquery()
        )
        self.session.execute(
            sql.insert(schema.SavedSearchLabelsMapping).values(
                label_id=label_id, saved_search_id=saved_search_id
            )
        )

    def _remove_label(self, saved_search_uuid: UUID, label):
        label_id = (
            sql.select(schema.SavedSearchLabels.id)
            .where(schema.SavedSearchLabels.uuid == label["uuid"])
            .scalar_subquery()
        )
        saved_search_id = (
            sql.select(schema.SavedSearch.id)
            .where(schema.SavedSearch.uuid == saved_search_uuid)
            .scalar_subquery()
        )

        self.session.execute(
            sql.delete(schema.SavedSearchLabelsMapping)
            .where(
                sql.and_(
                    schema.SavedSearchLabelsMapping.label_id == label_id,
                    schema.SavedSearchLabelsMapping.saved_search_id
                    == saved_search_id,
                )
            )
            .execution_options(synchronize_session=False)
        )

    def __diff_saved_search_labels(
        self, old_labels: Sequence, new_labels: Sequence
    ):
        labels_to_add = []
        labels_to_remove = []

        for old_label in old_labels:
            if old_label not in new_labels:
                labels_to_remove.append(old_label)

        for new_label in new_labels:
            if new_label not in old_labels:
                labels_to_add.append(new_label)

        return (labels_to_add, labels_to_remove)
