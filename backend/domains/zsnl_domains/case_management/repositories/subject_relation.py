# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import os
import ssl
import urllib.request
from ... import ZaaksysteemRepositoryBase
from ..constants import BASE_RELATION_ROLES
from ..entities import Case, Employee, Organization, Person, SubjectRelation
from ..entities.subject_relation import (
    SubjectRelationRelatedCase,
    SubjectRelationRelatedSubject,
)
from . import case_acl
from datetime import datetime, timezone
from minty.cqrs import Event, UserInfo
from minty.exceptions import Conflict, NotFound
from minty.repository import Repository
from sqlalchemy import JSON, sql
from sqlalchemy import types as sqltypes
from urllib.error import HTTPError
from uuid import UUID, uuid4
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)

API_HOSTNAME = os.environ.get("API_HOSTNAME", None)
CA_BUNDLE = os.environ.get("REQUESTS_CA_BUNDLE", None)


def subject_relation_query(session, user_info: UserInfo):
    return sql.select(
        schema.ZaakBetrokkenen.uuid.label("uuid"),
        schema.ZaakBetrokkenen.betrokkene_type.label("subject_type"),
        sql.case(
            (
                schema.ZaakBetrokkenen.betrokkene_type == "medewerker",
                sql.cast(
                    schema.Subject.properties,
                    sqltypes.JSON,
                ).op(
                    "->>"
                )("displayname"),
            ),
            (
                schema.ZaakBetrokkenen.betrokkene_type == "natuurlijk_persoon",
                sql.func.concat_ws(
                    " ",
                    schema.NatuurlijkPersoon.voorletters,
                    schema.NatuurlijkPersoon.voorvoegsel,
                    schema.NatuurlijkPersoon.geslachtsnaam,
                ),
            ),
            (
                schema.ZaakBetrokkenen.betrokkene_type == "bedrijf",
                schema.Bedrijf.handelsnaam,
            ),
            else_=schema.ZaakBetrokkenen.naam,
        ).label("name"),
        sql.func.coalesce(
            schema.ZaakBetrokkenen.magic_string_prefix,
            sql.case(
                (
                    schema.ZaakBetrokkenen.id == schema.Case.aanvrager,
                    "aanvrager",
                ),
                (
                    schema.ZaakBetrokkenen.id == schema.Case.behandelaar,
                    "behandelaar",
                ),
                (
                    schema.ZaakBetrokkenen.id == schema.Case.coordinator,
                    "coordinator",
                ),
                else_=None,
            ),
        ).label("magic_string_prefix"),
        sql.case(
            (
                schema.ZaakBetrokkenen.rol.isnot(None),
                schema.ZaakBetrokkenen.pip_authorized,
            ),
            (schema.ZaakBetrokkenen.id == schema.Case.aanvrager, True),
            else_=False,
        ).label("pip_authorized"),
        schema.Case.uuid.label("case_uuid"),
        schema.ZaakBetrokkenen.subject_id.label("subject_uuid"),
        schema.ZaakBetrokkenen.authorisation.label("permission"),
        sql.func.coalesce(
            schema.ZaakBetrokkenen.rol,
            sql.case(
                (
                    schema.ZaakBetrokkenen.id == schema.Case.aanvrager,
                    "Aanvrager",
                ),
                (
                    schema.ZaakBetrokkenen.id == schema.Case.behandelaar,
                    "Behandelaar",
                ),
                (
                    schema.ZaakBetrokkenen.id == schema.Case.coordinator,
                    "Coordinator",
                ),
                else_=None,
            ),
        ).label("role"),
        schema.Case.preset_client.label("is_preset_client"),
        schema.BibliotheekKenmerk.uuid.label("source_custom_field_type_id"),
    ).select_from(
        sql.join(
            schema.Case,
            schema.ZaakBetrokkenen,
            sql.and_(
                schema.ZaakBetrokkenen.zaak_id == schema.Case.id,
                schema.ZaakBetrokkenen.deleted.is_(None),
                user_allowed_cases_subquery(
                    user_info=user_info,
                    permission="read",
                    skip_acl_check=user_info is None,
                ),
            ),
        )
        .join(
            schema.BibliotheekKenmerk,
            schema.ZaakBetrokkenen.bibliotheek_kenmerken_id
            == schema.BibliotheekKenmerk.id,
            isouter=True,
        )
        .join(
            schema.Subject,
            sql.and_(
                schema.ZaakBetrokkenen.betrokkene_type == "medewerker",
                schema.Subject.subject_type == "employee",
                schema.Subject.uuid == schema.ZaakBetrokkenen.subject_id,
            ),
            isouter=True,
        )
        .join(
            schema.NatuurlijkPersoon,
            sql.and_(
                schema.ZaakBetrokkenen.betrokkene_type == "natuurlijk_persoon",
                schema.NatuurlijkPersoon.uuid
                == schema.ZaakBetrokkenen.subject_id,
            ),
            isouter=True,
        )
        .join(
            schema.Bedrijf,
            sql.and_(
                schema.ZaakBetrokkenen.betrokkene_type == "bedrijf",
                schema.Bedrijf.uuid == schema.ZaakBetrokkenen.subject_id,
            ),
            isouter=True,
        )
    )


class SubjectRelationRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "SubjectRelation"

    _events_to_calls = {
        "SubjectRelationCreated": "_insert_subject_relation",
        "SubjectRelationEmailEnqueued": "_enqueue_email",
        "SubjectRelationEmailSend": "_send_email",
        "SubjectRelationUpdated": "_update_subject_relation",
        "SubjectRelationDeleted": "_delete_subject_relation_by_uuid",
    }

    def find_subject_relations_for_case(
        self, case_uuid: UUID, user_info: UserInfo
    ):
        """Find related subjects for a case by case_uuid.

        :param uuid: UUID of the case.
        :type uuid: UUID
        :raises NotFound: If person not present
        :return: List of  SubjectRelation entity
        :rtype: Related Subject entity
        test
        """

        qry_stmt = subject_relation_query(self.session, user_info).where(
            schema.Case.uuid == case_uuid
        )
        subject_relations = self.session.execute(qry_stmt).fetchall()
        return [
            self._sqla_to_entity(subject_relation)
            for subject_relation in subject_relations
        ]

    def _sqla_to_entity(self, query_result) -> SubjectRelation:
        """Initialize SubjectRelation Entity from sqla object.

        :param query_result: sqla query results
        :type query_result: object
        :return: case_type entity
        :rtype: entities.Organization
        """
        subject_type_mapping = {
            "medewerker": "employee",
            "natuurlijk_persoon": "person",
            "bedrijf": "organization",
        }

        subject_relation = SubjectRelation(
            entity_id=query_result.uuid,
            uuid=query_result.uuid,
            role=query_result.role,
            magic_string_prefix=query_result.magic_string_prefix,
            authorized=query_result.pip_authorized,
            permission=query_result.permission,
            case=SubjectRelationRelatedCase(
                id=query_result.case_uuid, type="case"
            ),
            subject=SubjectRelationRelatedSubject(
                id=query_result.subject_uuid,
                type=subject_type_mapping[query_result.subject_type],
                name=query_result.name,
            ),
            is_preset_client=query_result.is_preset_client,
            source_custom_field_type_id=query_result.source_custom_field_type_id,
            type="subject_relation",
            _event_service=self.event_service,
        )

        return subject_relation

    def create_subject_relation(
        self,
        case: Case,
        subject: Person | Employee | Organization,
        role: str,
        magic_string_prefix: str,
        authorized: bool | None,
        send_confirmation_email: bool | None,
        permission: str | None,
        source_custom_field_type_id: UUID | None,
    ):
        """Create subject relation for a case.

        :param case_uuid: UUID of the case.
        :type case_uuid: UUID
        :param subject: subject
        :type subject: dict
        :param role: role of the related subject
        :type role: str
        :param magic_string_prefix: magic string prefix for the related subject.
        :type magic_string_prefix: str
        :param authorized: flag to set pip authorization for the subject.
        :type authorized: Optional[bool]
        :param send_confirmation_email: flag to send confirmation email to the subject
        :type send_confirmation_email: Optional[bool]
        :param permission: Right for subject_relation of type  employee
        :type permission: Optional[str]
        """
        uuid = uuid4()
        subject_relation = SubjectRelation(
            entity_id=uuid,
            uuid=uuid,
            role=None,
            magic_string_prefix=None,
            authorized=None,
            case=None,
            subject=None,
            send_confirmation_email=None,
            permission=None,
            source_custom_field_type_id=None,
            _event_service=self.event_service,
        )
        subject_relation.create(
            role=role,
            magic_string_prefix=magic_string_prefix,
            authorized=authorized,
            case={"id": case.uuid, "type": "case"},
            subject={
                "id": subject.uuid,
                "type": subject.entity_type,
                "name": subject.name,
            },
            send_confirmation_email=send_confirmation_email,
            permission=permission,
            source_custom_field_type_id=source_custom_field_type_id,
        )
        return subject_relation

    def _insert_subject_relation(
        self, event: Event, userinfo: UserInfo, dry_run: bool = False
    ):
        """Create subject relation for a case.

        :param changes: CreateSubjectRelation event changes.
        :type changes: dict
        :raises Conflict: When subject has related subject with same role.
        :raises Conflict: When magic_string_prefix is taken.
        """
        changes = event.format_changes()
        values = self._generate_database_values(event.entity_id, changes)

        subject_name = changes["subject"]["name"]
        if role := values.get("rol"):
            self._assert_valid_role(
                uuid=event.entity_id,
                role=role,
                subject_id=values["gegevens_magazijn_id"],
                subject_name=subject_name,
                case_id=values["zaak_id"],
            )
        self._assert_valid_magic_string_prefix(
            uuid=event.entity_id,
            magic_string_prefix=values["magic_string_prefix"],
            case_id=values["zaak_id"],
        )

        stmt = sql.insert(schema.ZaakBetrokkenen).values(values)
        self.session.execute(stmt)

        self._sync_acl(
            case_id=values["zaak_id"],
            subject=changes["subject"],
            authorisation=values.get("authorisation", "none"),
        )

    def _sync_acl(self, case_id, subject, authorisation):
        """(Re-)write case authorization rules for the related subject."""

        if subject["type"] != "employee":
            return

        capability_map = {
            "none": [],
            "search": ["search"],
            "read": ["read", "search"],
            "write": ["write", "read", "search"],
        }
        capabilities = capability_map[authorisation]

        subject_row = self.session.execute(
            sql.select(schema.Subject.username).where(
                sql.and_(
                    schema.Subject.uuid == subject["id"],
                    schema.Subject.subject_type == "employee",
                )
            )
        ).fetchone()

        if not subject_row:
            return

        username = subject_row.username

        self.session.execute(
            sql.delete(schema.CaseAuthorisation)
            .where(
                sql.and_(
                    schema.CaseAuthorisation.zaak_id == case_id,
                    schema.CaseAuthorisation.entity_type == "user",
                    schema.CaseAuthorisation.entity_id == username,
                )
            )
            .execution_options(synchronize_session=False)
        )

        db_authorisations = []
        for capability in capabilities:
            db_authorisations.append(
                {
                    "zaak_id": case_id,
                    "capability": capability,
                    "entity_id": username,
                    "entity_type": "user",
                    "scope": "instance",
                }
            )

        if len(db_authorisations):
            self.session.execute(
                sql.insert(schema.CaseAuthorisation).values(db_authorisations)
            )

    def _generate_database_values(self, uuid, changes):
        """Generate database values from event changes.

        :param event: event
        :type event: event
        :return: values for database update or insert
        :rtype: dict
        """

        subject_relation_mapping = {
            "id": "id",
            "case": "zaak_id",
            "subject": "subject_id",
            "role": "rol",
            "magic_string_prefix": "magic_string_prefix",
            "authorized": "pip_authorized",
            "send_confirmation_email": None,
            "permission": "authorisation",
            "source_custom_field_type_id": "source_custom_field",
        }

        subject_type_mapping = {
            "organization": "bedrijf",
            "person": "natuurlijk_persoon",
            "employee": "medewerker",
        }

        values = {"uuid": uuid}

        for key in changes:
            db_fieldname = subject_relation_mapping[key]

            if db_fieldname == "subject_id":
                subject = changes["subject"]

                values["betrokkene_type"] = subject_type_mapping[
                    subject["type"]
                ]

                values["gegevens_magazijn_id"] = self._get_subject_id(
                    subject["type"], subject["id"]
                )
                values["betrokkene_id"] = self._get_subject_snapshot_id(
                    subject["type"], subject["id"]
                )
                values["subject_id"] = subject["id"]
                values["naam"] = subject["name"]

            elif db_fieldname == "zaak_id":
                case = changes["case"]
                values["zaak_id"] = (
                    sql.select(schema.Case.id)
                    .where(schema.Case.uuid == case["id"])
                    .scalar_subquery()
                )
            elif db_fieldname == "source_custom_field":
                values["bibliotheek_kenmerken_id"] = (
                    sql.select(schema.BibliotheekKenmerk.id)
                    .where(
                        schema.BibliotheekKenmerk.uuid
                        == changes["source_custom_field_type_id"]
                    )
                    .scalar_subquery()
                )
            elif db_fieldname is None:
                pass
            else:
                values[db_fieldname] = changes[key]
        return values

    def _get_subject_id(self, subject_type: str, subject_uuid: UUID):
        """Get subject by uuid.

        :param subject_uuid: UUID of the related subject
        :type subject_uuid: UUID
        :param subject_type: subject type
        :type subject_type: str
        """
        if subject_type == "person":
            return (
                sql.select(schema.NatuurlijkPersoon.id)
                .where(schema.NatuurlijkPersoon.uuid == subject_uuid)
                .scalar_subquery()
            )

        elif subject_type == "organization":
            return (
                sql.select(schema.Bedrijf.id)
                .where(schema.Bedrijf.uuid == subject_uuid)
                .scalar_subquery()
            )

        elif subject_type == "employee":
            return (
                sql.select(schema.Subject.id)
                .where(
                    sql.and_(
                        schema.Subject.uuid == subject_uuid,
                        schema.Subject.subject_type == "employee",
                    )
                )
                .scalar_subquery()
            )

    def _get_subject_snapshot_id(self, subject_type: str, subject_uuid: UUID):
        """Get subject snapshot id. Create snapshot for subject(person/organization) and return snapshot id.
        For employee, snapshot id is same as that of the id of employee.

        :param subject_type: [description]
        :type subject_type: str
        :param subject_uuid: [description]
        :type subject_uuid: UUID
        :return: [description]
        :rtype: [type]
        """
        if subject_type == "person":
            return self._create_and_return_snapshot_for_person(
                person_uuid=subject_uuid
            )

        elif subject_type == "organization":
            return self._create_and_return_snapshot_for_organization(
                organization_uuid=subject_uuid
            ).scalar_subquery()

        elif subject_type == "employee":
            return (
                sql.select(schema.Subject.id)
                .where(
                    sql.and_(
                        schema.Subject.uuid == subject_uuid,
                        schema.Subject.subject_type == "employee",
                    )
                )
                .scalar_subquery()
            )

    def _create_and_return_snapshot_for_person(self, person_uuid: UUID):
        """Create snapshot for person and returns the snapshot id.

        :param person_uuid: UUID of the person.
        :type person_uuid: UUID
        :return: snapshot id for person
        :rtype: int
        """
        person_select_qry = sql.select(
            schema.NatuurlijkPersoon.id,
            schema.NatuurlijkPersoon.burgerservicenummer,
            schema.NatuurlijkPersoon.a_nummer,
            schema.NatuurlijkPersoon.voorletters,
            schema.NatuurlijkPersoon.voorvoegsel,
            schema.NatuurlijkPersoon.voornamen,
            schema.NatuurlijkPersoon.geslachtsnaam,
            schema.NatuurlijkPersoon.geslachtsaanduiding,
            schema.NatuurlijkPersoon.nationaliteitscode1,
            schema.NatuurlijkPersoon.nationaliteitscode2,
            schema.NatuurlijkPersoon.nationaliteitscode3,
            schema.NatuurlijkPersoon.geboorteplaats,
            schema.NatuurlijkPersoon.geboorteland,
            schema.NatuurlijkPersoon.geboortedatum,
            schema.NatuurlijkPersoon.aanhef_aanschrijving,
            schema.NatuurlijkPersoon.voorletters_aanschrijving,
            schema.NatuurlijkPersoon.voornamen_aanschrijving,
            schema.NatuurlijkPersoon.naam_aanschrijving,
            schema.NatuurlijkPersoon.voorvoegsel_aanschrijving,
            schema.NatuurlijkPersoon.burgerlijke_staat,
            schema.NatuurlijkPersoon.indicatie_geheim,
            schema.NatuurlijkPersoon.import_datum,
            schema.NatuurlijkPersoon.authenticatedby,
            schema.NatuurlijkPersoon.verblijfsobject_id,
            schema.NatuurlijkPersoon.datum_overlijden,
            schema.NatuurlijkPersoon.aanduiding_naamgebruik,
            schema.NatuurlijkPersoon.onderzoek_persoon,
            schema.NatuurlijkPersoon.onderzoek_huwelijk,
            schema.NatuurlijkPersoon.onderzoek_overlijden,
            schema.NatuurlijkPersoon.onderzoek_verblijfplaats,
            schema.NatuurlijkPersoon.partner_a_nummer,
            schema.NatuurlijkPersoon.partner_burgerservicenummer,
            schema.NatuurlijkPersoon.partner_voorvoegsel,
            schema.NatuurlijkPersoon.partner_geslachtsnaam,
            schema.NatuurlijkPersoon.datum_huwelijk,
            schema.NatuurlijkPersoon.datum_huwelijk_ontbinding,
            schema.NatuurlijkPersoon.landcode,
            schema.NatuurlijkPersoon.naamgebruik,
            schema.NatuurlijkPersoon.adellijke_titel,
        ).where(schema.NatuurlijkPersoon.uuid == person_uuid)

        person_snapshot_insert_stmt = (
            sql.insert(schema.GmNatuurlijkPersoon)
            .from_select(
                names=[
                    "gegevens_magazijn_id",
                    "burgerservicenummer",
                    "a_nummer",
                    "voorletters",
                    "voorvoegsel",
                    "voornamen",
                    "geslachtsnaam",
                    "geslachtsaanduiding",
                    "nationaliteitscode1",
                    "nationaliteitscode2",
                    "nationaliteitscode3",
                    "geboorteplaats",
                    "geboorteland",
                    "geboortedatum",
                    "aanhef_aanschrijving",
                    "voorletters_aanschrijving",
                    "voornamen_aanschrijving",
                    "naam_aanschrijving",
                    "voorvoegsel_aanschrijving",
                    "burgerlijke_staat",
                    "indicatie_geheim",
                    "import_datum",
                    "authenticatedby",
                    "verblijfsobject_id",
                    "datum_overlijden",
                    "aanduiding_naamgebruik",
                    "onderzoek_persoon",
                    "onderzoek_huwelijk",
                    "onderzoek_overlijden",
                    "onderzoek_verblijfplaats",
                    "partner_a_nummer",
                    "partner_burgerservicenummer",
                    "partner_voorvoegsel",
                    "partner_geslachtsnaam",
                    "datum_huwelijk",
                    "datum_huwelijk_ontbinding",
                    "landcode",
                    "naamgebruik",
                    "adellijke_titel",
                ],
                select=person_select_qry,
            )
            .returning(schema.GmNatuurlijkPersoon.id)
        )
        person_snapshot_id = self.session.execute(
            person_snapshot_insert_stmt
        ).fetchone()[0]

        residence_address_select_query = sql.select(
            schema.Adres.straatnaam,
            schema.Adres.huisnummer,
            schema.Adres.huisnummertoevoeging,
            schema.Adres.nadere_aanduiding,
            schema.Adres.postcode,
            schema.Adres.woonplaats,
            schema.Adres.functie_adres,
            schema.Adres.landcode,
            schema.Adres.gemeentedeel,
            schema.Adres.datum_aanvang_bewoning,
            schema.Adres.woonplaats_id,
            schema.Adres.gemeente_code,
            schema.Adres.hash,
            schema.Adres.import_datum,
            schema.Adres.adres_buitenland1,
            schema.Adres.adres_buitenland2,
            schema.Adres.adres_buitenland3,
            person_snapshot_id,
        ).where(
            sql.and_(
                schema.NatuurlijkPersoon.uuid == person_uuid,
                schema.Adres.natuurlijk_persoon_id
                == schema.NatuurlijkPersoon.id,
                schema.Adres.functie_adres == "W",
            )
        )

        residence_address_snapshot_insert_stmt = (
            sql.insert(schema.GmAdres)
            .from_select(
                names=[
                    "straatnaam",
                    "huisnummer",
                    "huisnummertoevoeging",
                    "nadere_aanduiding",
                    "postcode",
                    "woonplaats",
                    "functie_adres",
                    "landcode",
                    "gemeentedeel",
                    "datum_aanvang_bewoning",
                    "woonplaats_id",
                    "gemeente_code",
                    "hash",
                    "import_datum",
                    "adres_buitenland1",
                    "adres_buitenland2",
                    "adres_buitenland3",
                    "natuurlijk_persoon_id",
                ],
                select=residence_address_select_query,
            )
            .returning(schema.GmAdres.id)
        )
        residence_address_snapshot_id = self.session.execute(
            residence_address_snapshot_insert_stmt
        ).fetchone()

        correspondence_address_select_query = sql.select(
            schema.Adres.straatnaam,
            schema.Adres.huisnummer,
            schema.Adres.huisnummertoevoeging,
            schema.Adres.nadere_aanduiding,
            schema.Adres.postcode,
            schema.Adres.woonplaats,
            schema.Adres.functie_adres,
            schema.Adres.landcode,
            schema.Adres.gemeentedeel,
            schema.Adres.datum_aanvang_bewoning,
            schema.Adres.woonplaats_id,
            schema.Adres.gemeente_code,
            schema.Adres.hash,
            schema.Adres.import_datum,
            schema.Adres.adres_buitenland1,
            schema.Adres.adres_buitenland2,
            schema.Adres.adres_buitenland3,
            person_snapshot_id,
        ).where(
            sql.and_(
                schema.NatuurlijkPersoon.uuid == person_uuid,
                schema.Adres.natuurlijk_persoon_id
                == schema.NatuurlijkPersoon.id,
                schema.Adres.functie_adres == "B",
            )
        )

        correspondence_address_snapshot_insert_stmt = (
            sql.insert(schema.GmAdres)
            .from_select(
                names=[
                    "straatnaam",
                    "huisnummer",
                    "huisnummertoevoeging",
                    "nadere_aanduiding",
                    "postcode",
                    "woonplaats",
                    "functie_adres",
                    "landcode",
                    "gemeentedeel",
                    "datum_aanvang_bewoning",
                    "woonplaats_id",
                    "gemeente_code",
                    "hash",
                    "import_datum",
                    "adres_buitenland1",
                    "adres_buitenland2",
                    "adres_buitenland3",
                    "natuurlijk_persoon_id",
                ],
                select=correspondence_address_select_query,
            )
            .returning(schema.GmAdres.id)
        )

        correspondence_address_snapshot_id = self.session.execute(
            correspondence_address_snapshot_insert_stmt
        ).fetchone()

        person_address_id = None
        if residence_address_snapshot_id:
            person_address_id = residence_address_snapshot_id[0]

        person_address_update_stmt = (
            sql.update(schema.GmNatuurlijkPersoon)
            .where(schema.GmNatuurlijkPersoon.id == person_snapshot_id)
            .values(adres_id=person_address_id)
            .execution_options(synchronize_session=False)
        )
        if correspondence_address_snapshot_id:
            person_address_update_stmt = (
                sql.update(schema.GmNatuurlijkPersoon)
                .where(schema.GmNatuurlijkPersoon.id == person_snapshot_id)
                .values(adres_id=correspondence_address_snapshot_id[0])
                .execution_options(synchronize_session=False)
            )

        self.session.execute(person_address_update_stmt)
        return person_snapshot_id

    def _create_and_return_snapshot_for_organization(
        self, organization_uuid: UUID
    ):
        """Create snapshot for organization and returns the snapshot id.

        :param organization_uuid: UUID of the organization.
        :type organization_uuid: UUID
        :return: snapshot id for organization
        :rtype: int
        """
        select_qry = sql.select(
            schema.Bedrijf.id,
            schema.Bedrijf.dossiernummer,
            schema.Bedrijf.subdossiernummer,
            schema.Bedrijf.hoofdvestiging_dossiernummer,
            schema.Bedrijf.hoofdvestiging_subdossiernummer,
            schema.Bedrijf.vorig_dossiernummer,
            schema.Bedrijf.vorig_subdossiernummer,
            schema.Bedrijf.handelsnaam,
            schema.Bedrijf.rechtsvorm,
            schema.Bedrijf.kamernummer,
            schema.Bedrijf.faillisement,
            schema.Bedrijf.surseance,
            schema.Bedrijf.telefoonnummer,
            schema.Bedrijf.email,
            schema.Bedrijf.vestiging_adres,
            schema.Bedrijf.vestiging_straatnaam,
            schema.Bedrijf.vestiging_huisnummer,
            schema.Bedrijf.vestiging_huisnummertoevoeging,
            schema.Bedrijf.vestiging_postcodewoonplaats,
            schema.Bedrijf.vestiging_postcode,
            schema.Bedrijf.vestiging_woonplaats,
            schema.Bedrijf.correspondentie_adres,
            schema.Bedrijf.correspondentie_straatnaam,
            schema.Bedrijf.correspondentie_huisnummer,
            schema.Bedrijf.correspondentie_huisnummertoevoeging,
            schema.Bedrijf.correspondentie_postcodewoonplaats,
            schema.Bedrijf.correspondentie_postcode,
            schema.Bedrijf.correspondentie_woonplaats,
            schema.Bedrijf.hoofdactiviteitencode,
            schema.Bedrijf.nevenactiviteitencode1,
            schema.Bedrijf.nevenactiviteitencode2,
            schema.Bedrijf.werkzamepersonen,
            schema.Bedrijf.contact_naam,
            schema.Bedrijf.contact_aanspreektitel,
            schema.Bedrijf.contact_voorletters,
            schema.Bedrijf.contact_voorvoegsel,
            schema.Bedrijf.contact_geslachtsnaam,
            schema.Bedrijf.contact_geslachtsaanduiding,
            schema.Bedrijf.authenticated,
            schema.Bedrijf.authenticatedby,
            schema.Bedrijf.import_datum,
            schema.Bedrijf.verblijfsobject_id,
            schema.Bedrijf.vestigingsnummer,
            schema.Bedrijf.vestiging_huisletter,
            schema.Bedrijf.correspondentie_huisletter,
            schema.Bedrijf.vestiging_adres_buitenland1,
            schema.Bedrijf.vestiging_adres_buitenland2,
            schema.Bedrijf.vestiging_adres_buitenland3,
            schema.Bedrijf.vestiging_landcode,
            schema.Bedrijf.correspondentie_adres_buitenland1,
            schema.Bedrijf.correspondentie_adres_buitenland2,
            schema.Bedrijf.correspondentie_adres_buitenland3,
            schema.Bedrijf.correspondentie_landcode,
        ).where(schema.Bedrijf.uuid == organization_uuid)

        organization_insert_cte = (
            sql.insert(schema.GmBedrijf)
            .from_select(
                names=[
                    "gegevens_magazijn_id",
                    "dossiernummer",
                    "subdossiernummer",
                    "hoofdvestiging_dossiernummer",
                    "hoofdvestiging_subdossiernummer",
                    "vorig_dossiernummer",
                    "vorig_subdossiernummer",
                    "handelsnaam",
                    "rechtsvorm",
                    "kamernummer",
                    "faillisement",
                    "surseance",
                    "telefoonnummer",
                    "email",
                    "vestiging_adres",
                    "vestiging_straatnaam",
                    "vestiging_huisnummer",
                    "vestiging_huisnummertoevoeging",
                    "vestiging_postcodewoonplaats",
                    "vestiging_postcode",
                    "vestiging_woonplaats",
                    "correspondentie_adres",
                    "correspondentie_straatnaam",
                    "correspondentie_huisnummer",
                    "correspondentie_huisnummertoevoeging",
                    "correspondentie_postcodewoonplaats",
                    "correspondentie_postcode",
                    "correspondentie_woonplaats",
                    "hoofdactiviteitencode",
                    "nevenactiviteitencode1",
                    "nevenactiviteitencode2",
                    "werkzamepersonen",
                    "contact_naam",
                    "contact_aanspreektitel",
                    "contact_voorletters",
                    "contact_voorvoegsel",
                    "contact_geslachtsnaam",
                    "contact_geslachtsaanduiding",
                    "authenticated",
                    "authenticatedby",
                    "import_datum",
                    "verblijfsobject_id",
                    "vestigingsnummer",
                    "vestiging_huisletter",
                    "correspondentie_huisletter",
                    "vestiging_adres_buitenland1",
                    "vestiging_adres_buitenland2",
                    "vestiging_adres_buitenland3",
                    "vestiging_landcode",
                    "correspondentie_adres_buitenland1",
                    "correspondentie_adres_buitenland2",
                    "correspondentie_adres_buitenland3",
                    "correspondentie_landcode",
                ],
                select=select_qry,
            )
            .returning(schema.GmBedrijf.id)
            .cte("organization_insert_cte")
        )
        return sql.select(organization_insert_cte.c.id)

    def _assert_valid_role(
        self,
        uuid: UUID,
        role: str,
        subject_id,
        subject_name: str,
        case_id: int,
    ):
        """Assert valid role for the related subject.

        :param role: role for the related subject.
        :type role: str
        :param subject_id: id of the subejct.
        :type subject_id: int
        :param subject_name: name of the subject
        :type subject_name: str
        :raises Conflict: When role is not in list of allowed relation roles.
        :raises Conflict: When related subject with role already exists for a subject.
        """

        allowed_relation_roles = BASE_RELATION_ROLES

        custom_relation_roles_query = sql.select(
            sql.cast(schema.Config.value, JSON)
        ).where(schema.Config.parameter == "custom_relation_roles")

        custom_relation_roles = self.session.execute(
            custom_relation_roles_query
        ).fetchone()

        if custom_relation_roles and custom_relation_roles[0]:
            allowed_relation_roles = (
                allowed_relation_roles + custom_relation_roles[0]
            )

        if role not in allowed_relation_roles:
            raise Conflict(
                f"Role '{role}' is not a valid role",
                "subject_relation/invalid_role",
            )

        role_exists_query = sql.select(1).where(
            sql.and_(
                schema.ZaakBetrokkenen.gegevens_magazijn_id == subject_id,
                schema.ZaakBetrokkenen.rol == role,
                schema.ZaakBetrokkenen.zaak_id == case_id,
                schema.ZaakBetrokkenen.uuid != uuid,
                schema.ZaakBetrokkenen.deleted.is_(None),
            )
        )
        query_result = self.session.execute(role_exists_query).fetchone()

        if query_result:
            raise Conflict(
                f"Related subject with role '{role}' already exists for '{subject_name}'",
                "subject_relation/already_exists",
            )

    def _assert_valid_magic_string_prefix(
        self, uuid: UUID, magic_string_prefix: str, case_id: int
    ):
        """Assert valid magic string for the subject relation.

        :param magic_string_prefix: magic_string_prefix for the subject relation.
        :type magic_string_prefix: str
        :raises Conflict: When subject relation with magic_string_prefix already exists.
        """

        magic_string_exists_query = sql.select(1).where(
            sql.and_(
                schema.ZaakBetrokkenen.magic_string_prefix
                == magic_string_prefix,
                schema.ZaakBetrokkenen.zaak_id == case_id,
                schema.ZaakBetrokkenen.uuid != uuid,
                schema.ZaakBetrokkenen.deleted.is_(None),
            )
        )
        magic_string = self.session.execute(
            magic_string_exists_query
        ).fetchone()

        if magic_string:
            raise Conflict(
                f"Related subject with magic_string_prefix '{magic_string_prefix}' already exists",
                "subject_relation/invalid_magic_string",
            )

    def find_subject_relation_by_uuid(self, uuid: UUID, user_info):
        """Find subject_relation by uuid.

        :param uuid: UUID of the subject relation.
        :type uuid: UUID
        :raises NotFound: When subject relation with UUID not found.
        :return: subject relation as SubjectRelation Entity
        :rtype: SubjectRelation
        """
        qry_stmt = subject_relation_query(self.session, user_info).where(
            sql.and_(
                schema.ZaakBetrokkenen.uuid == uuid,
                schema.ZaakBetrokkenen.deleted.is_(None),
            )
        )

        query_result = self.session.execute(qry_stmt).fetchone()

        if not query_result:
            raise NotFound(
                f"Subject relation with uuid '{uuid}' not found.",
                "subject_relation/not_found",
            )

        return self._sqla_to_entity(query_result=query_result)

    def _get_metadata_for_queue_item(self, user_uuid):
        # This code will disappear once the creation of email
        # has been moved to Python [20191122]
        metadata_ = {
            "require_object_model": 1,
            "subject_id": case_acl.get_subject_by_uuid(
                user_uuid, self.session
            ).Subject.id,
            "target": "backend",
        }
        return metadata_

    def _enqueue_email(
        self, event: Event, userinfo: UserInfo, dry_run: bool = False
    ):
        # This code will disappear once the creation of email
        # has been moved to Python [20191122]

        changes = event.format_changes()
        data = changes["enqueued_email_data"]

        values = {
            "id": event.entity_id,
            "object_id": data["case_uuid"],
            "status": "pending",
            "type": "send_subject_relation_email",
            "label": "Create subject relation email",
            "data": data,
            "date_created": datetime.now(timezone.utc),
            "date_started": None,
            "date_finished": None,
            "parent_id": None,
            "priority": 1000,
            "metadata": self._get_metadata_for_queue_item(event.user_uuid),
        }

        insert_stmt = sql.insert(schema.Queue).values(values)
        self.session.execute(insert_stmt)

    def _send_email(
        self, event: Event, userinfo: UserInfo, dry_run: bool = False
    ):
        # This code will disappear once the creation of email
        # has been moved to Python [20191122]
        queue_id = event.entity_id

        if API_HOSTNAME:
            run_queue_url = f"http://{API_HOSTNAME}/api/queue/{queue_id}/run"
        else:
            run_queue_url = f"https://{event.context}/api/queue/{queue_id}/run"

        if CA_BUNDLE:
            ctx = ssl.create_default_context(cafile=CA_BUNDLE)
        else:
            ctx = ssl.create_default_context()

        try:
            self.logger.debug("Calling API URL: " + run_queue_url)

            req = urllib.request.Request(run_queue_url)
            req.add_header("Host", event.context)
            response = urllib.request.urlopen(req, context=ctx)

            response_read = response.read()
            response_dict = json.loads(response_read)
            self.logger.info(
                str(datetime.now())
                + " Completed queue_id "
                + str(queue_id)
                + ": "
                + json.dumps(response_dict)
            )
        except HTTPError as error:
            self.logger.error(f"Error during API call: {error}")

    def _update_subject_relation(
        self, event: Event, userinfo: UserInfo, dry_run: bool = False
    ):
        """Update subject_relation by uuid.

        :param changes: SubjectRelationUpdated Event changes.
        :type changes: dict
        """

        changes = event.format_changes()
        case = event.entity_data["case"]
        subject = event.entity_data["subject"]

        values = self._generate_database_values(event.entity_id, changes)

        case_id = (
            sql.select(schema.Case.id)
            .where(schema.Case.uuid == case["id"])
            .scalar_subquery()
        )

        subject_id = self._get_subject_id(
            subject_type=subject["type"], subject_uuid=subject["id"]
        )
        subject_name = subject["name"]

        if role := values.get("rol"):
            self._assert_valid_role(
                uuid=event.entity_id,
                role=role,
                subject_id=subject_id,
                subject_name=subject_name,
                case_id=case_id,
            )
        if values.get("magic_string_prefix"):
            self._assert_valid_magic_string_prefix(
                uuid=event.entity_id,
                magic_string_prefix=values["magic_string_prefix"],
                case_id=case_id,
            )

        stmt = (
            sql.update(schema.ZaakBetrokkenen)
            .where(schema.ZaakBetrokkenen.uuid == event.entity_id)
            .values(values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(stmt)

        self._sync_acl(
            case_id=case_id,
            subject=subject,
            authorisation=values.get("authorisation", "none"),
        )

    def _delete_subject_relation_by_uuid(
        self, event: Event, userinfo: UserInfo, dry_run: bool = False
    ):
        """Delete subject_releation, given its uuid.

        :param changes: SubjectRelationUpdated Event.
        :type changes: dict
        """

        values = {"deleted": sql.functions.now()}

        stmt = (
            sql.update(schema.ZaakBetrokkenen)
            .where(schema.ZaakBetrokkenen.uuid == event.entity_id)
            .values(values)
            .execution_options(synchronize_session=False)
        )
        self.session.execute(stmt)

        self._sync_acl(
            case_id=sql.select(schema.Case.id)
            .where(schema.Case.uuid == event.entity_data["case"]["id"])
            .scalar_subquery(),
            subject=event.entity_data["subject"],
            authorisation="none",  # Deleted, so rights go away
        )

    def get_case_for_subject_relation(
        self, case_uuid, user_info, permission=None
    ):
        query = (
            sql.select(
                schema.Case.uuid,
                sql.literal("case").label("type"),
            )
            .select_from(schema.Case)
            .where(
                schema.Case.uuid == case_uuid,
            )
        )

        if permission is not None:
            query = query.where(
                user_allowed_cases_subquery(
                    user_info=user_info,
                    permission=permission,
                    skip_acl_check=user_info is None,
                ),
            )

        case = self.session.execute(query).fetchone()
        return case
