# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ... import ZaaksysteemRepositoryBase
from ..entities import CaseBasic, _shared
from minty.exceptions import NotFound
from minty.repository import Repository
from pydantic import validate_arguments
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from uuid import UUID
from zsnl_domains import case_management
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)
from zsnl_domains.shared.repositories.date_utilities import format_date


def get_case_basic_query(
    user_info: minty.cqrs.UserInfo, permission: _shared.CaseAuthorizationLevel
):
    user_id_query = (
        sql.select(schema.Subject.id)
        .where(schema.Subject.uuid == user_info.user_uuid)
        .scalar_subquery()
    )
    count_phases = (
        sql.select(sql.func.count(schema.ZaaktypeStatus.id))
        .where(
            schema.ZaaktypeStatus.zaaktype_node_id == schema.ZaaktypeNode.id
        )
        .label("count_phases")
    )
    return (
        sql.select(
            schema.CaseV2.id,
            schema.CaseV2.uuid,
            schema.CaseV2.custom_fields,
            schema.CaseV2.file_custom_fields,
            schema.CaseV2.registratiedatum.label("registration_date"),
            schema.CaseV2.streefafhandeldatum.label("target_completion_date"),
            schema.CaseV2.afhandeldatum.label("completion_date"),
            schema.CaseV2.vernietigingsdatum.label("destruction_date"),
            # For the "stalled since" date
            schema.CaseV2.case_meta,
            schema.CaseV2.stalled_until,
            schema.CaseV2.confidentiality,
            schema.CaseV2.contactkanaal.label("contact_channel"),
            schema.CaseV2.milestone,
            schema.CaseV2.progress_days.label("progress_percentage"),
            schema.CaseV2.result,
            schema.CaseV2.result_uuid,
            schema.CaseV2.result_description,
            # For CaseResult.archival_attributes.state
            schema.CaseV2.archival_state,
            schema.CaseV2.active_selection_list,
            schema.CaseV2.preset_client,
            schema.CaseV2.payment_amount,
            schema.CaseV2.payment_status,
            schema.CaseV2.status,
            schema.CaseV2.onderwerp.label("summary"),
            schema.CaseV2.onderwerp_extern.label("public_summary"),
            schema.CaseV2.requestor_obj,
            schema.CaseV2.assignee_obj,
            schema.CaseV2.coordinator_obj,
            schema.CaseV2.case_department,
            schema.CaseV2.case_role,
            schema.CaseV2.html_email_template,
            schema.CaseV2.case_type,
            schema.CaseV2.case_type_version,
            schema.CaseV2.case_subjects,
            sql.func.array(
                sql.select(schema.CaseAcl.permission)
                .select_from(schema.CaseAcl)
                .where(schema.CaseAcl.case_id == schema.CaseV2.id)
                .where(schema.CaseAcl.subject_uuid == user_info.user_uuid)
                .group_by(schema.CaseAcl.permission)
                .scalar_subquery()
            ).label("authorizations"),
            sql.case(
                (
                    sql.and_(
                        schema.CaseV2.aanvrager_type == "medewerker",
                        schema.CaseV2.aanvrager_gm_id == user_id_query,
                    ),
                    True,
                ),
                else_=False,
            ).label("user_is_requestor"),
            sql.case(
                (
                    schema.CaseV2.behandelaar_gm_id == user_id_query,
                    True,
                ),
                else_=False,
            ).label("user_is_assignee"),
            sql.case(
                (
                    schema.CaseV2.coordinator_gm_id == user_id_query,
                    True,
                ),
                else_=False,
            ).label("user_is_coordinator"),
            sql.literal(
                True
                if (user_info and user_info.permissions.get("admin"))
                else False
            ).label("user_is_admin"),
            (
                sql.cast(schema.CaseV2.milestone, sqltypes.Float)
                / count_phases
            ).label("progress_status"),
        )
        .select_from(
            sql.join(
                schema.CaseV2,
                schema.ZaaktypeNode,
                schema.ZaaktypeNode.id == schema.CaseV2.zaaktype_node_id,
                isouter=False,
            )
        )
        .where(
            user_allowed_cases_subquery(
                user_info=user_info,
                permission=permission,
                case_alias=schema.CaseV2,
            ),
        )
    )


class CaseBasicRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "CaseBasic"

    def _get_repo(self, domain, repo_name):
        repo = domain.REQUIRED_REPOSITORIES[repo_name]
        return repo(
            infrastructure_factory=self.infrastructure_factory,
            context=self.context,
            event_service=self.event_service,
        )

    def find_case_basic_by_uuid(
        self,
        case_uuid: UUID,
        user_info: minty.cqrs.UserInfo,
        permission: _shared.CaseAuthorizationLevel,
    ) -> CaseBasic:
        """Return case_basic entity for given id by fetching case from database.

        :param case_uuid: UUID of the case to retrieve
        :param user_info: UserInfo object for the user performing the action
        :param permission: permission needed for the action ("search", "read",
            "write" or "manage")
        :return: case_basic entity
        """
        query_result = self._get_basic_case(case_uuid, user_info, permission)
        case_entity = self._transform_to_entity(query_result=query_result)

        return case_entity

    def _transform_to_entity(self, query_result) -> CaseBasic:
        progress_status = (
            None
            if query_result.progress_status == ""
            else query_result.progress_status
        )
        custom_fields_repo = self._get_repo(case_management, "custom_fields")

        return CaseBasic.parse_obj(
            {
                "entity_id": query_result.uuid,
                "uuid": query_result.uuid,
                "number": query_result.id,
                "confidentiality": query_result.confidentiality,
                "contact_channel": query_result.contact_channel,
                "milestone": query_result.milestone,
                "payment": {
                    "amount": query_result.payment_amount,
                    "status": query_result.payment_status,
                }
                if query_result.payment_status
                else None,
                "progress_status": progress_status,
                "status": query_result.status,
                "registration_date": format_date(
                    query_result.registration_date
                ),
                "stalled_since_date": format_date(
                    query_result.case_meta["stalled_since"]
                ),
                "stalled_until_date": format_date(query_result.stalled_until),
                "target_completion_date": format_date(
                    query_result.target_completion_date
                ),
                "completion_date": format_date(query_result.completion_date),
                "destruction_date": format_date(query_result.destruction_date),
                "summary": str(query_result.summary or ""),
                "public_summary": str(query_result.public_summary or ""),
                "html_email_template": query_result.html_email_template,
                "custom_fields": custom_fields_repo.format_custom_fields_for_case_basic(
                    query_result.custom_fields, query_result.file_custom_fields
                ).custom_fields,
                "num_unaccepted_files": query_result.case_meta[
                    "unaccepted_files_count"
                ],
                "num_unaccepted_updates": query_result.case_meta[
                    "unaccepted_attribute_update_count"
                ],
                "num_unread_communication": query_result.case_meta[
                    "unread_communication_count"
                ],
                "result": {
                    "result": query_result.result,
                    "result_name": query_result.result_description,
                    "result_uuid": query_result.result_uuid,
                    "archival_attributes": {
                        "state": query_result.archival_state,
                        "selection_list": query_result.active_selection_list
                        or "",
                    },
                }
                if query_result.result_uuid
                else None,
                "case_type": {
                    "entity_id": query_result.case_type["uuid"],
                    "uuid": query_result.case_type["uuid"],
                },
                "case_type_version": {
                    "uuid": query_result.case_type_version["uuid"],
                    "entity_id": query_result.case_type_version["uuid"],
                    "entity_meta_summary": query_result.case_type_version[
                        "name"
                    ],
                },
                "requestor": {
                    "entity_type": query_result.requestor_obj["type"],
                    "entity_id": query_result.requestor_obj["uuid"],
                    "uuid": query_result.requestor_obj["uuid"],
                    "entity_meta_summary": query_result.requestor_obj[
                        "display_name"
                    ],
                },
                "requestor_is_preset_client": query_result.preset_client,
                "assignee": {
                    "entity_type": "employee",
                    "entity_id": query_result.assignee_obj["uuid"],
                    "uuid": query_result.assignee_obj["uuid"],
                    "entity_meta_summary": query_result.assignee_obj[
                        "display_name"
                    ],
                }
                if query_result.assignee_obj
                else None,
                "recipient": self._transform_recipient(
                    query_result.case_subjects
                ),
                "coordinator": {
                    "entity_type": "employee",
                    "entity_id": query_result.coordinator_obj["uuid"],
                    "uuid": query_result.coordinator_obj["uuid"],
                    "entity_meta_summary": query_result.coordinator_obj[
                        "display_name"
                    ],
                }
                if query_result.coordinator_obj
                else None,
                "department": {
                    "entity_id": query_result.case_department["uuid"],
                    "uuid": query_result.case_department["uuid"],
                    "entity_meta_summary": query_result.case_department[
                        "name"
                    ],
                }
                if query_result.case_department
                else None,
                "role": {
                    "entity_id": query_result.case_role["uuid"],
                    "uuid": query_result.case_role["uuid"],
                    "entity_meta_summary": query_result.case_role["name"],
                }
                if query_result.case_role and query_result.case_role["uuid"]
                else None,
                "entity_meta_authorizations": self._get_authorizations(
                    query_result.user_is_admin,
                    query_result.user_is_requestor,
                    query_result.user_is_assignee,
                    query_result.user_is_coordinator,
                    query_result.authorizations,
                ),
                "_event_service": self.event_service,
            }
        )

    def _transform_recipient(self, case_contacts):
        for case_contact in case_contacts:
            if case_contact["magic_string_prefix"] == "ontvanger":
                return {
                    "entity_type": case_contact["type"],
                    "entity_id": case_contact["subject_id"],
                    "uuid": case_contact["subject_id"],
                    "entity_meta_summary": case_contact["display_name"],
                }

        return None

    def _get_basic_case(
        self,
        case_uuid: UUID,
        user_info: minty.cqrs.UserInfo,
        permission: _shared.CaseAuthorizationLevel,
    ) -> dict:
        """
        Retrieve a specific case from the database with permission check.

        :param case_uuid: Case to retrieve
        :param user_info: UserInfo for the user requesting the case
        :param permission: Purpose the case is retrieved for. Can be one of
            "search", "read", "write" or "manage".
        :return: The case_basic database result; if the case can't be retrieved
            an exception is raised. This can happen when the case doesn't exist,
            or the user doesn't have the specified permissions.
        """

        query = get_case_basic_query(user_info, permission).where(
            schema.CaseV2.uuid == case_uuid,
        )

        basic_case = self.session.execute(query).fetchone()

        if not basic_case:
            raise NotFound(f"Case with uuid '{case_uuid}' not found.")

        return basic_case

    @validate_arguments
    def _get_authorizations(
        self,
        user_is_admin: bool,
        user_is_requestor: bool,
        user_is_assignee: bool,
        user_is_coordinator: bool,
        authorizations: list[_shared.CaseAuthorizationLevel],
    ) -> set[_shared.CaseAuthorizationLevel]:
        if user_is_admin:
            return {
                _shared.CaseAuthorizationLevel.search,
                _shared.CaseAuthorizationLevel.read,
                _shared.CaseAuthorizationLevel.write,
                _shared.CaseAuthorizationLevel.manage,
            }

        all_authorizations = set(authorizations)

        if user_is_requestor:
            all_authorizations |= {
                _shared.CaseAuthorizationLevel.search,
                _shared.CaseAuthorizationLevel.read,
            }

        if user_is_coordinator or user_is_assignee:
            all_authorizations |= {
                _shared.CaseAuthorizationLevel.search,
                _shared.CaseAuthorizationLevel.read,
                _shared.CaseAuthorizationLevel.write,
            }

        return all_authorizations
