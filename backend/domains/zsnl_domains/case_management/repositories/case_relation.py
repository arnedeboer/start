# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ... import ZaaksysteemRepositoryBase
from ..entities import _shared
from ..entities import case_relation as entity
from minty.exceptions import Conflict, NotFound
from minty.repository import Repository
from sqlalchemy import sql
from uuid import UUID, uuid4
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)


class CaseRelationRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "CaseRelation"

    _events_to_calls = {
        "CaseRelationCreated": "_create_relation",
        "CaseRelationReordered": "_reorder_relation",
        "CaseRelationDeleted": "_delete_relation",
    }

    case_a = sql.alias(schema.Case)
    case_b = sql.alias(schema.Case)

    relationship_query = sql.select(
        sql.literal("related_case").label("relation_type"),
        case_a.c.uuid.label("case_a_uuid"),
        case_b.c.uuid.label("case_b_uuid"),
        case_a.c.onderwerp.label("case_a_onderwerp"),
        case_b.c.onderwerp.label("case_b_onderwerp"),
        schema.CaseRelation.uuid.label("relation_uuid"),
        schema.CaseRelation.case_id_a,
        schema.CaseRelation.case_id_b,
        schema.CaseRelation.type_a,
        schema.CaseRelation.type_b,
        schema.CaseRelation.order_seq_a,
        schema.CaseRelation.order_seq_b,
    ).select_from(
        sql.join(
            case_a,
            schema.CaseRelation,
            case_a.c.id == schema.CaseRelation.case_id_a,
        ).join(case_b, case_b.c.id == schema.CaseRelation.case_id_b)
    )

    def get_case_for_case_relation(
        self, case_uuid, user_info, permission=None
    ):
        query = (
            sql.select(schema.Case.uuid, schema.Case.id)
            .select_from(schema.Case)
            .where(
                schema.Case.uuid == case_uuid,
            )
        )

        if permission is not None:
            query = query.where(
                user_allowed_cases_subquery(
                    user_info=user_info, permission=permission
                ),
            )

        case = self.session.execute(query).fetchone()
        return case

    def get_case_relations(
        self,
        case_uuid: UUID,
        authorization: _shared.CaseAuthorizationLevel,
        user_info,
    ) -> list[entity.CaseRelation]:
        """Retrieve all case relationships for the given case, that the user
        has at least authorization level `authorization` for the case"""

        case = self.get_case_for_case_relation(
            case_uuid=case_uuid, user_info=user_info, permission="read"
        )
        if case is None:
            raise NotFound("Case not found.", "case_relation/case/not_found")

        related_cases = self._get_related_cases_for_case(
            case_id=case.id, authorization=authorization, user_info=user_info
        )
        parent_children = self._get_parent_children_for_case(
            case_id=case.id, authorization=authorization, user_info=user_info
        )

        relationships = []
        relationships = related_cases + parent_children

        return [
            self._transform_to_entity(relationship, case_uuid)
            for relationship in relationships
        ]

    def _exists_relationship(self, object1_id, object2_id):
        relationship_row = self.session.execute(
            sql.select(schema.CaseRelationship.relation_uuid)
            .select_from(schema.CaseRelationship)
            .where(
                sql.or_(
                    sql.and_(
                        schema.CaseRelationship.case_id == object1_id,
                        schema.CaseRelationship.relation_id == object2_id,
                    ),
                    sql.and_(
                        schema.CaseRelationship.case_id == object2_id,
                        schema.CaseRelationship.relation_id == object1_id,
                    ),
                )
            )
        ).fetchone()

        return relationship_row is not None

    def _get_next_sequence_id_for_case_relation(self, case_id: int):
        """Get the next sequence order id for a case based on the current case relationships.
        If no sequence is present, then 1 will be returned"""
        return sql.select(
            sql.func.coalesce(
                sql.func.max(
                    sql.union_all(
                        sql.select(
                            schema.CaseRelation.order_seq_a.label("count")
                        )
                        .select_from(schema.CaseRelation)
                        .where(schema.CaseRelation.case_id_b == case_id),
                        sql.select(
                            schema.CaseRelation.order_seq_b.label("count")
                        )
                        .select_from(schema.CaseRelation)
                        .where(schema.CaseRelation.case_id_a == case_id),
                    )
                    .subquery()
                    .columns.count
                )
                + 1,
                1,
            )
        ).scalar_subquery()

    def _create_relation(
        self,
        event: minty.cqrs.Event,
        user_info: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = event.format_changes()

        cases = [
            self.get_case_for_case_relation(
                case_uuid=changes["object1_uuid"],
                user_info=user_info,
                permission="write",
            ),
            self.get_case_for_case_relation(
                case_uuid=changes["object2_uuid"],
                user_info=user_info,
                permission="write",
            ),
        ]

        if None in cases:
            raise NotFound("Case not found.", "case_relation/case/not_found")

        if changes["object1_uuid"] == changes["object2_uuid"]:
            raise Conflict(
                "Can not relate case to itself",
                "case_relation/self_relation/not_allowed",
            )
        if self._exists_relationship(cases[0].id, cases[1].id):
            raise Conflict(
                "Relationship already exists", "case_relation/already_exists"
            )
        self.session.execute(
            sql.insert(schema.CaseRelation).values(
                {
                    "case_id_a": cases[0].id,
                    "case_id_b": cases[1].id,
                    "order_seq_a": self._get_next_sequence_id_for_case_relation(
                        cases[1].id
                    ),
                    "order_seq_b": self._get_next_sequence_id_for_case_relation(
                        cases[0].id
                    ),
                    "type_a": "plain",
                    "type_b": "plain",
                }
            )
        )

        self._update_case_relation_properties(
            case_uuid=changes["object1_uuid"], user_info=user_info
        )
        self._update_case_relation_properties(
            case_uuid=changes["object2_uuid"], user_info=user_info
        )

    def create_case_relation(self, object1_uuid: UUID, object2_uuid: UUID):
        uuid = uuid4()
        case_relation = entity.CaseRelation.parse_obj(
            {
                "entity_id": uuid,
                "uuid": uuid,
                "relation_type": "related_case",
                "_event_service": self.event_service,
            }
        )
        return case_relation.create(object1_uuid, object2_uuid)

    def _get_related_cases_for_case(
        self,
        case_id: int,
        user_info,
        authorization: _shared.CaseAuthorizationLevel = _shared.CaseAuthorizationLevel.search,
        for_update: bool = False,
    ):
        case_a = sql.alias(schema.Case)
        case_b = sql.alias(schema.Case)

        related_cases_query = (
            sql.select(
                sql.literal("related_case").label("relation_type"),
                case_a.c.uuid.label("case_a_uuid"),
                case_b.c.uuid.label("case_b_uuid"),
                case_a.c.onderwerp.label("case_a_onderwerp"),
                case_b.c.onderwerp.label("case_b_onderwerp"),
                schema.CaseRelation.uuid.label("relation_uuid"),
                schema.CaseRelation.case_id_a,
                schema.CaseRelation.case_id_b,
                schema.CaseRelation.type_a,
                schema.CaseRelation.type_b,
                schema.CaseRelation.order_seq_a,
                schema.CaseRelation.order_seq_b,
            )
            .select_from(
                sql.join(
                    case_a,
                    schema.CaseRelation,
                    case_a.c.id == schema.CaseRelation.case_id_a,
                ).join(case_b, case_b.c.id == schema.CaseRelation.case_id_b)
            )
            .where(
                sql.and_(
                    sql.or_(
                        schema.CaseRelation.case_id_a == case_id,
                        schema.CaseRelation.case_id_b == case_id,
                    ),
                    user_allowed_cases_subquery(
                        user_info=user_info,
                        permission=authorization,
                        case_alias=case_a.c,
                    ),
                    user_allowed_cases_subquery(
                        user_info=user_info,
                        permission=authorization,
                        case_alias=case_b.c,
                    ),
                )
            )
        )

        if for_update:
            related_cases_query = related_cases_query.with_for_update(
                key_share=True
            )

        return self.session.execute(related_cases_query).fetchall()

    def _update_row(self, relation_uuid, values):
        self.session.execute(
            sql.update(schema.CaseRelation)
            .where(schema.CaseRelation.uuid == relation_uuid)
            .values(values)
            .execution_options(synchronize_session=False)
        )

    def _reorder_relation(
        self,
        event: minty.cqrs.Event,
        user_info: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = event.format_changes()

        current_case_uuid = changes["current_case_uuid"]

        # Only one of these fields will be present in this specific event
        if "order_seq_a" in changes:
            order_field_name = "order_seq_a"
        else:
            order_field_name = "order_seq_b"

        new_index = int(changes[order_field_name])
        old_index = int(event.previous_value(order_field_name))

        self._assert_valid_index_for_reorder(new_index, current_case_uuid)

        case = self.get_case_for_case_relation(
            case_uuid=current_case_uuid,
            user_info=user_info,
            permission="write",
        )
        if case is None:
            raise NotFound("Case not found.", "case_relation/case/not_found")

        going_up = new_index < old_index

        for row in self._get_related_cases_for_case(
            case.id,
            user_info=user_info,
            authorization=_shared.CaseAuthorizationLevel.read,
            for_update=True,
        ):
            if row.case_id_a != case.id:
                field_name = "order_seq_a"
            else:
                field_name = "order_seq_b"

            current_sequence_nr = getattr(row, field_name)
            row_uuid = str(row.relation_uuid)

            if (
                going_up
                and current_sequence_nr >= new_index
                and current_sequence_nr < old_index
            ):
                new_sequence_nr = current_sequence_nr + 1
            elif (
                (not going_up)
                and current_sequence_nr <= new_index
                and current_sequence_nr > old_index
            ):
                new_sequence_nr = current_sequence_nr - 1
            else:
                continue

            self._update_row(str(row_uuid), {field_name: new_sequence_nr})

        self._update_row(str(event.entity_id), {order_field_name: new_index})

    def _transform_to_entity(
        self, relation_row, case_uuid=None
    ) -> entity.CaseRelation:
        """Transform a 'case relation' database row into a `CaseRelation` entity"""

        relation = entity.CaseRelation.parse_obj(
            {
                "entity_id": relation_row.relation_uuid,
                "relation_type": relation_row.relation_type,
                "uuid": relation_row.relation_uuid,
                "object1_uuid": relation_row.case_a_uuid,
                "relationship_type1": relation_row.type_a,
                "summary1": relation_row.case_a_onderwerp,
                "order_seq_a": relation_row.order_seq_a,
                "case_id_a": relation_row.case_id_a,
                "object2_uuid": relation_row.case_b_uuid,
                "relationship_type2": relation_row.type_b,
                "summary2": relation_row.case_b_onderwerp,
                "order_seq_b": relation_row.order_seq_b,
                "case_id_b": relation_row.case_id_b,
                "current_case_uuid": case_uuid,
                "_event_service": self.event_service,
            }
        )

        return relation

    def find_case_relation_by_uuid(self, relation_uuid, user_info, permission):
        relationship = self.session.execute(
            self.relationship_query.where(
                schema.CaseRelation.uuid == relation_uuid
            )
        ).fetchone()

        if relationship is None:
            raise NotFound(
                "Case relation not found.", "case_relation/not_found"
            )

        case_a = self.get_case_for_case_relation(
            case_uuid=relationship.case_a_uuid,
            user_info=user_info,
            permission=permission,
        )
        case_b = self.get_case_for_case_relation(
            case_uuid=relationship.case_b_uuid,
            user_info=user_info,
            permission=permission,
        )
        if not (case_a and case_b):
            raise NotFound("Case not found.", "case_relation/case/not_found")

        return self._transform_to_entity(relationship)

    def _delete_relation(
        self,
        event: minty.cqrs.Event,
        user_info: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        """Delete a case_relation.

        :param event: CaseRelationDeleted event.
        :type event: event
        """
        relation_uuid = event.entity_id
        delete_stmt = sql.delete(schema.CaseRelation).where(
            schema.CaseRelation.uuid == relation_uuid
        )
        self.session.execute(delete_stmt)

        changes = event.format_changes()
        current_case_uuid = changes["current_case_uuid"]
        case_id = self.session.execute(
            sql.select(schema.Case.id).where(
                schema.Case.uuid == current_case_uuid
            )
        ).fetchone()[0]

        # Adjusting the index of case_relations after delete.
        sequence = 1
        for row in self._get_related_cases_for_case(
            case_id,
            user_info=user_info,
            authorization=_shared.CaseAuthorizationLevel.read,
            for_update=True,
        ):
            field_name = "order_seq_a"
            if row.case_id_a == case_id:
                field_name = "order_seq_b"
            current_sequence_nr = getattr(row, field_name)
            row_uuid = str(row.relation_uuid)
            if current_sequence_nr > sequence:
                self._update_row(
                    row_uuid, {field_name: current_sequence_nr - 1}
                )
            sequence = sequence + 1

        self._update_case_relation_properties(
            case_uuid=event.entity_data["object1_uuid"], user_info=user_info
        )
        self._update_case_relation_properties(
            case_uuid=event.entity_data["object2_uuid"], user_info=user_info
        )

    def _assert_valid_index_for_reorder(
        self, index: int, current_case_uuid: UUID
    ):
        """Assert valid reorder index.

        :param index: new_index for relation.
        :type index: int
        :param current_case_uuid: UUID of the case to be affected.
        :type current_case_uuid: UUID
        :raises Conflict: When index is less than 1.
        :raises Conflict: When index is greater than number of relations.
        """
        if index < 1:
            raise Conflict(
                "Minimum value for index is 1",
                "case_relation/reorder_index_less_than_one",
            )

        count_relations = self.session.execute(
            sql.select(sql.func.count(schema.CaseRelation.id))
            .select_from(
                sql.join(
                    schema.Case,
                    schema.CaseRelation,
                    sql.or_(
                        schema.CaseRelation.case_id_a == schema.Case.id,
                        schema.CaseRelation.case_id_b == schema.Case.id,
                    ),
                )
            )
            .where(schema.Case.uuid == current_case_uuid)
        ).fetchone()[0]

        if index > count_relations:
            raise Conflict(
                f"Maximum value for index is {count_relations}",
                "case_relation/reorder_index_greater_than_max",
            )

    def _update_case_relation_properties(
        self, case_uuid: UUID, user_info: minty.cqrs.UserInfo
    ):
        """Update the value of relation_uuids, linked_case_numbers and related_case_numbers properties.

        :param case_uuid: UUID of the case.
        :type case_uuid: UUID
        """
        case_id = self.session.execute(
            sql.select(schema.Case.id).where(schema.Case.uuid == case_uuid)
        ).fetchone()[0]

        uuids_lst = []
        case_numbers_lst = []

        relations = self._get_related_cases_for_case(
            case_id,
            user_info=user_info,
            authorization=_shared.CaseAuthorizationLevel.read,
            for_update=True,
        )

        for row in relations:
            if str(row.case_a_uuid) == str(case_uuid):
                uuids_lst.append(str(row.case_b_uuid))
                case_numbers_lst.append(str(row.case_id_b))
            else:
                uuids_lst.append(str(row.case_a_uuid))
                case_numbers_lst.append(str(row.case_id_a))

    def _get_parent_children_for_case(
        self,
        case_id: int,
        user_info: minty.cqrs.UserInfo,
        authorization: _shared.CaseAuthorizationLevel = _shared.CaseAuthorizationLevel.search,
    ):
        """Get parent and children for a case."""

        parent = sql.alias(schema.Case)
        parent_query = sql.select(
            sql.literal("parent_case").label("relation_type"),
            parent.c.uuid.label("case_a_uuid"),
            schema.Case.uuid.label("case_b_uuid"),
            parent.c.onderwerp.label("case_a_onderwerp"),
            schema.Case.onderwerp.label("case_b_onderwerp"),
            parent.c.uuid.label("relation_uuid"),
            parent.c.id.label("case_id_a"),
            schema.Case.id.label("case_id_b"),
            sql.literal("parent").label("type_a"),
            sql.literal("child").label("type_b"),
            sql.literal(None).label("order_seq_a"),
            sql.literal(None).label("order_seq_b"),
        ).select_from(
            sql.join(parent, schema.Case, parent.c.id == schema.Case.pid)
        )

        children = sql.alias(schema.Case)
        children_query = sql.select(
            sql.literal("child_case").label("relation_type"),
            children.c.uuid.label("case_a_uuid"),
            schema.Case.uuid.label("case_b_uuid"),
            children.c.onderwerp.label("case_a_onderwerp"),
            schema.Case.onderwerp.label("case_b_onderwerp"),
            children.c.uuid.label("relation_uuid"),
            children.c.id.label("case_id_a"),
            schema.Case.id.label("case_id_b"),
            sql.literal("child").label("type_a"),
            sql.literal("parent").label("type_b"),
            sql.literal(None).label("order_seq_a"),
            sql.literal(None).label("order_seq_b"),
        ).select_from(
            sql.join(children, schema.Case, children.c.pid == schema.Case.id)
        )

        return self.session.execute(
            sql.union(
                parent_query.where(
                    sql.and_(
                        schema.Case.id == case_id,
                        user_allowed_cases_subquery(
                            user_info=user_info,
                            permission=authorization,
                            case_alias=parent.c,
                        ),
                    )
                ),
                children_query.where(
                    sql.and_(
                        schema.Case.id == case_id,
                        user_allowed_cases_subquery(
                            user_info=user_info,
                            permission=authorization,
                            case_alias=children.c,
                        ),
                    )
                ),
            )
        ).fetchall()
