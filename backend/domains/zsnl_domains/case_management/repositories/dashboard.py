# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
import minty.exceptions
from ... import ZaaksysteemRepositoryBase
from ..entities import dashboard as entity
from collections import namedtuple
from minty import repository
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql as sql_pg
from uuid import UUID
from zsnl_domains.database import schema


def get_dashboard_query():
    return sql.select(
        schema.Dashboard.uuid,
        schema.Dashboard.widgets,
    )


class DashboardRepository(repository.Repository, ZaaksysteemRepositoryBase):
    _for_entity = "Dashboard"
    _events_to_calls = {
        "WidgetsUpdated": "_save_update_widgets",
    }

    def _save_update_widgets(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = event.format_changes()
        uuid = str(event.entity_id)
        widgets = changes["widgets"]
        if widgets is None:
            delete_stmt = (
                sql.delete(schema.Dashboard)
                .where(schema.Dashboard.uuid == uuid)
                .execution_options(synchronize_session=False)
            )
            self.session.execute(delete_stmt)
        else:
            self.session.execute(
                sql_pg.insert(schema.Dashboard)
                .values(uuid=uuid, widgets=widgets)
                .on_conflict_do_update(
                    index_elements=[schema.Dashboard.uuid],
                    set_={"widgets": widgets},
                )
            )

    def get(
        self, uuid: UUID, user_info: minty.cqrs.UserInfo
    ) -> entity.Dashboard:
        if str(user_info.user_uuid) != str(uuid):
            raise minty.exceptions.Forbidden(
                "Current user is not allowed to view or update this dashboard",
                "dashboard/action/not_allowed",
            )
        dashboard_query = get_dashboard_query()

        result = self.session.execute(
            dashboard_query.where(schema.Dashboard.uuid == uuid)
        ).fetchone()

        # Intially when dashboard doesnt exists then return dashboard with empty widgets
        if not result:
            result = namedtuple("result", ["uuid", "widgets"])(
                uuid=uuid, widgets=None
            )

        return self._inflate(result)

    def _inflate(self, db_row) -> entity.Dashboard:
        return entity.Dashboard.parse_obj(
            {
                "entity_id": db_row.uuid,
                "uuid": db_row.uuid,
                "widgets": db_row.widgets,
                "_event_service": self.event_service,
            }
        )
