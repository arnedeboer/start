# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import secrets
import tempfile
from ... import ZaaksysteemRepositoryBase
from .. import entities
from ..entities import TimelineEntry
from collections.abc import Iterable
from datetime import datetime, timedelta, timezone
from io import SEEK_SET
from minty.repository import Repository
from minty_infra_storage.s3 import S3Infrastructure
from sqlalchemy import sql
from uuid import UUID, uuid4
from zsnl_domains.database import schema


class TimelineExportRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "TimelineExport"
    _events_to_calls = {"TimelineExportCreated": "_create_export"}

    REQUIRED_INFRASTRUCTURE = {
        **ZaaksysteemRepositoryBase.REQUIRED_INFRASTRUCTURE,
        "s3": S3Infrastructure(),
    }

    def request_export(
        self,
        type: str,
        uuid: UUID,
        period_start: str | None = None,
        period_end: str | None = None,
    ):
        entities.TimelineExport.request(
            type=type,
            uuid=uuid,
            period_start=period_start,
            period_end=period_end,
            _event_service=self.event_service,
        )

    def create_export(
        self,
        type: str,
        uuid: UUID,
        output_user: UUID,
        entries: Iterable[TimelineEntry],
    ):
        # Convert entries to json and transform it into bytes, and
        # upload json entries to s3.
        file_handle = tempfile.SpooledTemporaryFile(max_size=5_000_000)

        file_handle.write(b"[")

        # Can't use `','.join()` because then the whole output file contents
        # end up in memory in a single string.
        first = True
        for entry in entries:
            if not first:
                file_handle.write(b",\n")
            file_handle.write(entry.to_json().encode("utf-8"))
            first = False

        file_handle.write(b"]")

        file_handle.seek(0, SEEK_SET)

        filestore_uuid = uuid4()

        upload_result = self._get_infrastructure("s3").upload(
            file_handle=file_handle, uuid=filestore_uuid
        )

        entities.TimelineExport.create(
            type=type,
            uuid=uuid,
            output_user=output_user,
            upload_result=upload_result,
            filestore_uuid=filestore_uuid,
            _event_service=self.event_service,
        )

    def _create_export(
        self,
        event,
        user_info: minty.cqrs.UserInfo | None = None,
        dry_run=False,
    ):
        """Create entry in export_queue table"""
        changes = event.format_changes()

        output_user_uuid = user_info.user_uuid
        event_type = changes["type"]
        upload_result = changes["upload_result"]
        filestore_uuid = changes["filestore_uuid"]

        # Insert details of uploaded file into filestore table.
        self.logger.info("Inserting data to filestore table")
        today = datetime.now(timezone.utc)
        stringified_date = today.strftime("%Y%m%dT%H%M%S")
        filestore_id = self.session.execute(
            sql.insert(schema.Filestore).values(
                {
                    "uuid": filestore_uuid,
                    "original_name": f"zs-export-timeline-{event_type}-{stringified_date}.json",
                    "is_archivable": True,
                    "virus_scan_status": "ok",
                    "mimetype": upload_result["mime_type"],
                    "md5": upload_result["md5"],
                    "size": upload_result["size"],
                    "storage_location": [upload_result["storage_location"]],
                },
            )
        ).inserted_primary_key[0]

        # Insert new export in export_queue table.
        self.session.execute(
            sql.insert(schema.ExportQueue).values(
                {
                    "uuid": uuid4(),
                    "subject_id": sql.select(schema.Subject.id)
                    .where(
                        sql.and_(
                            schema.Subject.uuid == output_user_uuid,
                            schema.Subject.subject_type == "employee",
                        )
                    )
                    .scalar_subquery(),
                    "subject_uuid": output_user_uuid,
                    "expires": today
                    + timedelta(
                        days=3
                    ),  # Will expire in 3 days from the date of creation
                    "token": secrets.token_hex(nbytes=32),
                    "filestore_id": filestore_id,
                    "filestore_uuid": filestore_uuid,
                    "downloaded": 0,
                },
            )
        )
