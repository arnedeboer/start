# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from .. import entities
from minty.repository import Repository
from sqlalchemy import cast, sql
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql
from uuid import UUID
from zsnl_domains.database import schema


class RelatedSubjectRepository(Repository, ZaaksysteemRepositoryBase):
    def find_for_subject(self, subject_uuid: UUID):
        """
        Find related subjects for subjects.

        :param subject_uuid: UUID of the subject to find relations for
        :return: subjects related to a specified subject
        """
        where_condition = (
            schema.ContactRelationship.contact_uuid == subject_uuid
        )
        inverse_where_condition = (
            schema.ContactRelationship.relation_uuid == subject_uuid
        )

        employee_select_stmt = self._select_stmt_employee().where(
            schema.Subject.id.in_(
                sql.union(
                    sql.select(schema.ContactRelationship.relation).where(
                        sql.and_(
                            where_condition,
                            schema.ContactRelationship.relation_type
                            == "employee",
                        )
                    ),
                    sql.select(schema.ContactRelationship.contact).where(
                        sql.and_(
                            inverse_where_condition,
                            schema.ContactRelationship.contact_type
                            == "employee",
                        )
                    ),
                )
            )
        )

        organization_select_stmt = self._select_stmt_organization().where(
            schema.Bedrijf.id.in_(
                sql.union(
                    sql.select(schema.ContactRelationship.relation).where(
                        sql.and_(
                            where_condition,
                            schema.ContactRelationship.relation_type
                            == "company",
                        )
                    ),
                    sql.select(schema.ContactRelationship.contact).where(
                        sql.and_(
                            inverse_where_condition,
                            schema.ContactRelationship.contact_type
                            == "company",
                        )
                    ),
                )
            )
        )

        person_select_stmt = self._select_stmt_person().where(
            schema.NatuurlijkPersoon.id.in_(
                sql.union(
                    sql.select(schema.ContactRelationship.relation).where(
                        sql.and_(
                            where_condition,
                            schema.ContactRelationship.relation_type
                            == "person",
                        )
                    ),
                    sql.select(schema.ContactRelationship.contact).where(
                        sql.and_(
                            inverse_where_condition,
                            schema.ContactRelationship.contact_type
                            == "person",
                        )
                    ),
                )
            )
        )
        complete_query = sql.union_all(
            employee_select_stmt,
            organization_select_stmt,
            person_select_stmt,
        )

        result = self.session.execute(complete_query).fetchall()
        return [self._inflate_row_to_entity(row) for row in result]

    def find_for_custom_object(self, object_uuid: UUID):
        """
        Find related subjects for custom objects.

        :param object_uuid: UUID of the custom object
        :return: subjects related to a custom object
        """

        where_condition = sql.and_(
            schema.CustomObject.uuid == object_uuid,
            schema.CustomObjectRelationship.custom_object_id
            == schema.CustomObject.id,
            schema.CustomObjectRelationship.relationship_type == "subject",
        )

        employee_select_stmt = self._select_stmt_employee().where(
            sql.and_(
                where_condition,
                schema.CustomObjectRelationship.related_employee_id
                == schema.Subject.id,
            )
        )

        organization_select_stmt = self._select_stmt_organization().where(
            sql.and_(
                where_condition,
                schema.CustomObjectRelationship.related_organization_id
                == schema.Bedrijf.id,
            )
        )

        person_select_stmt = self._select_stmt_person().where(
            sql.and_(
                where_condition,
                schema.CustomObjectRelationship.related_person_id
                == schema.NatuurlijkPersoon.id,
            )
        )

        complete_query = sql.union_all(
            employee_select_stmt, person_select_stmt, organization_select_stmt
        )

        result = self.session.execute(complete_query).fetchall()
        return [self._inflate_row_to_entity(row) for row in result]

    def _inflate_row_to_entity(self, row):
        """Inflate database row to RelatedSubject Entity.

        :param row: Database Row
        :return: RelatedSubject
        """
        mapping = {
            "uuid": "uuid",
            "entity_id": "uuid",
            "entity_meta_summary": "name",
            "type": "type",
            "roles": "roles",
        }

        entity_data = {}
        for key, objkey in mapping.items():
            entity_data[key] = getattr(row, objkey)

        # Filling the roles of subject as RelatedSubjectRole Entity in RelatedSubject Entity
        entity_data["roles"] = None
        if row.roles:
            entity_data["roles"] = []
            for role in row.roles:
                entity_data["roles"].append(
                    {
                        "uuid": role["uuid"],
                        "entity_id": role["uuid"],
                        "entity_meta_summary": role["name"],
                    }
                )

        return entities.RelatedSubject.parse_obj(
            {**entity_data, "_event_service": self.event_service}
        )

    def _select_stmt_employee(self):
        """Sql select statement for employee.

        Or:

        SELECT subject.uuid,
        :param_1 AS type,
        CAST(subject.properties AS JSON) ->> :param_2 AS name,
        array((SELECT json_build_object(:json_build_object_2, roles.uuid, :json_build_object_3, roles.name) AS json_build_object_1 FROM subject AS subject_1
        JOIN LATERAL json_array_elements_text(array_to_json(subject_1.role_ids)) AS role_id ON subject_1.uuid = subject.uuid
        JOIN roles ON roles.id = CAST(role_id AS INTEGER))) AS roles
        """
        subject = sql.alias(schema.Subject)

        role_ids = sql.func.json_array_elements_text(
            sql.func.array_to_json(subject.c.role_ids)
        ).lateral("role_id")

        roles_sub_query = sql.func.array(
            sql.select(
                sql.func.json_build_object(
                    "uuid", schema.Role.uuid, "name", schema.Role.name
                )
            )
            .select_from(
                sql.join(
                    subject, role_ids, subject.c.uuid == schema.Subject.uuid
                ).join(
                    schema.Role,
                    schema.Role.id
                    == sql.cast(sql.column(role_ids.name), sqltypes.Integer),
                )
            )
            .label("roles")
        )
        return sql.select(
            schema.Subject.uuid,
            sql.literal("employee").label("type"),
            cast(schema.Subject.properties, sqltypes.JSON)
            .op("->>")("displayname")
            .label("name"),
            roles_sub_query.label("roles"),
        )

    def _select_stmt_person(self):
        """Sql select statement for person

        Or:

        SELECT natuurlijk_persoon.uuid,
        :param_3 AS type,
        concat_ws(:concat_ws_1, natuurlijk_persoon.voorletters, natuurlijk_persoon.voorvoegsel, natuurlijk_persoon.geslachtsnaam) AS name,
        :param_4 AS roles
        """

        return sql.select(
            schema.NatuurlijkPersoon.uuid,
            sql.literal("person").label("type"),
            sql.func.get_display_name_for_person(
                postgresql.hstore(
                    postgresql.array(
                        ["voorletters", "naamgebruik", "geslachtsnaam"]
                    ),
                    postgresql.array(
                        [
                            schema.NatuurlijkPersoon.voorletters,
                            schema.NatuurlijkPersoon.naamgebruik,
                            schema.NatuurlijkPersoon.geslachtsnaam,
                        ]
                    ),
                )
            ).label("name"),
            sql.literal(None).label("roles"),
        )

    def _select_stmt_organization(self):
        """Sql select statement for organization

        Or:
        SELECT bedrijf.uuid,
        :param_5 AS type,
        bedrijf.handelsnaam AS name,
        :param_6 AS roles
        """
        return sql.select(
            schema.Bedrijf.uuid,
            sql.literal("organization").label("type"),
            schema.Bedrijf.handelsnaam.label("name"),
            sql.literal(None).label("roles"),
        )
