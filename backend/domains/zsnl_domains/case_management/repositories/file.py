# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from .. import entities
from minty.exceptions import NotFound
from minty.repository import Repository
from minty_infra_storage import s3
from sqlalchemy import sql
from typing import cast
from uuid import UUID
from zsnl_domains.database import schema


class FileRepository(Repository, ZaaksysteemRepositoryBase):
    def find_file_by_uuid(self, uuid: UUID) -> entities.File:
        db_row = self.session.execute(self.get_file_query(uuid)).fetchone()
        if db_row is None:
            raise NotFound(f"Could not find 'file' with uuid {uuid}")
        file = self._row_to_entity(db_row)
        return file

    def _row_to_entity(self, db_row) -> entities.File:
        store = cast(s3.S3Wrapper, self._get_infrastructure("s3"))
        download_url = store.get_download_url(
            uuid=db_row.uuid,
            storage_location=db_row.storage_location[0],
            mime_type=db_row.mimetype,
            filename=db_row.original_name,
            download=False,
        )
        return entities.File.parse_obj(
            {
                "entity_id": db_row.uuid,
                "uuid": db_row.uuid,
                "mimetype": db_row.mimetype,
                "filename": db_row.original_name,
                "storage_location": db_row.storage_location,
                "download_url": download_url,
                "_event_service": self.event_service,
            }
        )

    def get_file_query(self, uuid: UUID):
        query = sql.select(
            schema.Filestore.uuid,
            schema.Filestore.mimetype,
            schema.Filestore.original_name,
            schema.Filestore.storage_location,
        ).where(schema.Filestore.uuid == uuid)
        return query
