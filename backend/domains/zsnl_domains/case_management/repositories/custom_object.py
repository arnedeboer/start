# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import logging
from ... import ZaaksysteemRepositoryBase
from .. import entities
from minty import exceptions
from minty.cqrs import Event, UserInfo
from minty.entity import EntityCollection, _reflect
from minty.exceptions import ValidationError
from minty.repository import Repository
from sqlalchemy import sql
from uuid import UUID, uuid4
from zsnl_domains import case_management
from zsnl_domains.database import schema
from zsnl_domains.shared import custom_field
from zsnl_domains.shared.repositories import object_acl

logger = logging.getLogger(__name__)


def _get_custom_field_id(
    session,
    custom_field_uuid: UUID | None,
) -> int | None:
    if custom_field_uuid is None:
        return None

    attribute = session.execute(
        sql.select(schema.BibliotheekKenmerk.id).where(
            schema.BibliotheekKenmerk.uuid == custom_field_uuid
        )
    ).fetchone()

    if not attribute:
        return None

    return attribute.id


def _object_authorizations_subquery(
    user_uuid: UUID, custom_object_version_alias
):
    cota = sql.alias(schema.CustomObjectTypeAcl)
    cot = sql.alias(schema.CustomObjectType)
    cotv = sql.alias(schema.CustomObjectTypeVersion)
    spm = sql.alias(schema.SubjectPositionMatrix)
    cov = custom_object_version_alias

    return (
        sql.select(cota.c.authorization)
        .select_from(
            sql.join(
                cota,
                sql.join(
                    cot,
                    cotv,
                    cotv.c.custom_object_type_id == cot.c.id,
                ),
                cota.c.custom_object_type_id == cot.c.id,
            ).join(
                spm,
                sql.and_(
                    spm.c.group_id == cota.c.group_id,
                    spm.c.role_id == cota.c.role_id,
                    spm.c.subject_id
                    == (
                        sql.select(schema.Subject.id)
                        .where(schema.Subject.uuid == user_uuid)
                        .scalar_subquery()
                    ),
                ),
            )
        )
        .where(
            cotv.c.id == cov.c.custom_object_type_version_id,
        )
        .group_by(cota.c.authorization)
    ).scalar_subquery()


class CustomObjectRepository(Repository, ZaaksysteemRepositoryBase):
    def _get_repo(self, domain, repo_name):
        repo = domain.REQUIRED_REPOSITORIES[repo_name]
        return repo(
            infrastructure_factory=self.infrastructure_factory,
            context=self.context,
            event_service=self.event_service,
        )

    _for_entity = "CustomObject"
    _events_to_calls = {
        "CustomObjectCreated": "_update_or_create_custom_object",
        "CustomObjectUpdated": "_update_or_create_custom_object",
        "CustomObjectRelatedTo": "_update_or_create_custom_object",
        "CustomObjectUnrelatedFrom": "_update_or_create_custom_object",
        "CustomObjectDeleted": "_delete_custom_object",
        "CustomObjectGeoSynced": "_update_or_create_custom_object",
        "CustomObjectUpgraded": "_update_or_create_custom_object",
    }

    def create(self, **params) -> entities.CustomObject:
        """Create a new object type for supplied case & phase.

        :return: CustomObjectType
        :rtype: entities.CustomObjectType
        """
        current_datetime = datetime.datetime.now(datetime.UTC)

        ot_repo = self._get_repo(case_management, "custom_object_type")

        # Check if the user have right authorizations for creating the custom_object
        # done by checking acl on custom_object_type
        ot = ot_repo.find_by_uuid(
            params["custom_object_type_uuid"],
            user_info=params["user_info"],
            authorization=entities.AuthorizationLevel.readwrite,
        )

        if ot is None:
            raise exceptions.NotFound(
                "Could not find or rights are insufficient for object type by uuid {}".format(
                    params["custom_object_type_uuid"]
                ),
                "case_management/custom_object/object_create/unauthorised",
            )

        self._validate_custom_fields(ot, params["custom_fields"])

        # Transform relationships
        relationships = params.pop("related_to", {})
        for relationshipname, relationshipdata in relationships.items():
            if relationshipname == "cases":
                params["cases"] = [
                    {"uuid": case_uuid, "entity_id": case_uuid}
                    for case_uuid in relationshipdata
                ]

            if relationshipname == "custom_objects":
                params["custom_objects"] = [
                    {
                        "uuid": custom_object_uuid,
                        "entity_id": custom_object_uuid,
                    }
                    for custom_object_uuid in relationshipdata
                ]

        archive_metadata = (
            params["archive_metadata"]
            if "archive_metadata" in params
            else entities.custom_object.CustomObjectArchiveMetadata(
                status=entities.custom_object.ValidArchiveStatus.to_preserve,
                ground="",
                retention=0,
            )
        )

        entity = entities.CustomObject.create(
            **{
                # Create defaults
                "date_created": current_datetime,
                "last_modified": current_datetime,
                # Override with given parameters
                **params,
                "custom_object_type": ot,
                "name": ot.name,
                "title": "",  # will be filled by the template service,
                "subtitle": "",  # will be filled by the template service,
                "archive_metadata": archive_metadata,
                "external_reference": "",  # will be filled by the template service
                "version": 1,
                "is_active_version": True,
                "entity_id": params["uuid"],
                # Services
                "_event_service": self.event_service,
                "_template_service": params["template_service"],
            }
        )

        return entity

    def _validate_custom_fields(self, object_type, custom_fields):
        cf_model = custom_field.get_custom_fields_model(
            definition=object_type.custom_field_definition
        )

        cf_model.parse_obj(custom_fields)

        new_custom_fields = {
            key
            for key in custom_fields.keys()
            if custom_fields[key]
            and (
                bool(custom_fields[key]["value"]) is True
                or custom_fields[key]["value"] != ""
            )
        }
        required_custom_fields = {
            c_f.magic_string
            for c_f in object_type.custom_field_definition.custom_fields
            if c_f.is_required
        }

        if not required_custom_fields.issubset(new_custom_fields):
            missing_custom_fields = required_custom_fields.difference(
                new_custom_fields
            )

            raise ValidationError(
                f"Custom fields '{','.join(missing_custom_fields)}' is/are required",
                "custom_object/required_custom_fields_missing",
            )

    def _update_or_create_custom_object(
        self, event: Event, user_info: UserInfo = None, dry_run: bool = False
    ):
        changes = event.format_changes()

        # This is an update call, in which the entity_id / uuid might have
        # because of a new version
        previous_uuid = None
        mark_create = None
        mark_new_version = None
        if event.event_name in [
            "CustomObjectUpdated",
            "CustomObjectUpgraded",
        ]:
            previous_uuid = event.previous_value("uuid")
            mark_new_version = True
        elif event.event_name == "CustomObjectCreated":
            mark_create = True
        else:
            previous_uuid = event.entity_id

        # Find object in database
        custom_object: entities.CustomObject | None = None
        if previous_uuid is not None:
            custom_object = self.find_by_uuid(
                uuid=str(previous_uuid), user_info=None, authorization=None
            )

            if "custom_fields" in changes:
                self._validate_custom_fields(
                    custom_object.custom_object_type, changes["custom_fields"]
                )

        # This is a fresh create, get object from scratch
        if mark_create:
            custom_object = entities.CustomObject(**changes)
        else:
            # Apply changes on an existing Custom Object:
            self._apply_changes(custom_object, changes)

        self._save_to_database(
            custom_object,
            event=event,
            create=mark_create,
            new_version=mark_new_version,
            previous_uuid=previous_uuid,
        )

    def _collect_relationships_from_custom_fields(self, custom_object):
        cf_model = custom_field.get_custom_fields_model(
            definition=custom_object.custom_object_type.custom_field_definition
        )

        fields = cf_model.parse_obj(custom_object.custom_fields)

        relationship_custom_fields = {
            fieldname: fieldvalue
            for fieldname, fieldvalue in fields
            if fieldvalue and fieldvalue.type == "relationship"
        }

        return relationship_custom_fields

    def _process_relationships(
        self,
        custom_object,
        current_relationships,
        new_relationships,
        custom_object_version_id,
        custom_object_id,
        uuid_to_insert_map,
        # UUID-to-id mappings for all related things (cases, subjects,
        # custom objects, etc.)
        db_entries,
    ):
        for relationship_type in db_entries.keys():
            for db_entry in db_entries[relationship_type]:
                if (
                    uuid_to_insert_map[str(db_entry.uuid)]["type"]
                    != relationship_type
                ):
                    continue

                uuid_to_insert_map[str(db_entry.uuid)]["id"].append(
                    db_entry.id
                )

                db_insert = {
                    schema.CustomObjectRelationship.custom_object_id: custom_object_id,
                    schema.CustomObjectRelationship.custom_object_version_id: custom_object_version_id,
                    schema.CustomObjectRelationship.relationship_type: relationship_type,
                    schema.CustomObjectRelationship.related_uuid: db_entry.uuid,
                }

                # Special case for subject
                if relationship_type == "subject":
                    db_insert[
                        getattr(
                            schema.CustomObjectRelationship,
                            f"related_{db_entry.type}_id",
                        )
                    ] = db_entry.id
                else:
                    db_insert[
                        getattr(
                            schema.CustomObjectRelationship,
                            f"related_{relationship_type}_id",
                        )
                    ] = db_entry.id

                # Solid relationship, cannot be removed by an unrelate/relate call
                if uuid_to_insert_map[str(db_entry.uuid)]["magic_string"]:
                    db_insert[
                        schema.CustomObjectRelationship.relationship_magic_string_prefix
                    ] = uuid_to_insert_map[str(db_entry.uuid)]["magic_string"]

                # Relationships can show up multiple times, for instance if
                # multiple fields in a single case link to it.
                for custom_field_uuid in uuid_to_insert_map[
                    str(db_entry.uuid)
                ]["source_custom_field_uuid"]:
                    to_insert = db_insert.copy()

                    to_insert[
                        schema.CustomObjectRelationship.source_custom_field_type_id
                    ] = _get_custom_field_id(self.session, custom_field_uuid)

                    new_relationships["insert"].append(to_insert)

            # Is there any mapping without an entry?
            for uuid, insert_data in uuid_to_insert_map.items():
                if str(insert_data["type"]) != relationship_type:
                    continue

                if not insert_data["id"]:
                    raise exceptions.Conflict(
                        f"Given {relationship_type} with uuid {uuid} does not exist"
                    )

        return new_relationships

    def _process_case_relationships(
        self,
        custom_object,
        current_relationships,
        new_relationships,
        custom_object_version_id,
        custom_object_id,
    ):
        uuid_to_insert_map = {}

        for related_case in custom_object.cases:
            if str(related_case.uuid) not in uuid_to_insert_map:
                uuid_to_insert_map[str(related_case.uuid)] = {
                    "id": [],
                    "magic_string": None,
                    "type": "case",
                    "source_custom_field_uuid": [],
                }

            uuid_to_insert_map[str(related_case.uuid)][
                "source_custom_field_uuid"
            ].append(related_case.source_custom_field_uuid)

        db_entries = (
            self.session.query(schema.Case)
            .filter(
                sql.and_(
                    schema.Case.uuid.in_(uuid_to_insert_map.keys()),
                    schema.Case.status.notin_({"deleted"}),
                    schema.Case.deleted.is_(None),
                )
            )
            .all()
        )

        self._process_relationships(
            custom_object=custom_object,
            current_relationships=current_relationships,
            new_relationships=new_relationships,
            custom_object_version_id=None,
            custom_object_id=custom_object_id,
            uuid_to_insert_map=uuid_to_insert_map,
            db_entries={"case": db_entries},
        )

    def _process_custom_field_relationships(
        self,
        custom_object,
        current_relationships,
        new_relationships,
        custom_object_version_id,
        custom_object_id,
    ):
        list_of_relationship_custom_fields = (
            self._collect_relationships_from_custom_fields(custom_object)
        )

        uuid_to_insert_map = {}
        uuid_to_id_map = db_entries = {
            "document": {},
            "custom_object": {},
            "case": {},
            "subject": {},
        }

        for (
            cf_name,
            relationship_cf,
        ) in list_of_relationship_custom_fields.items():
            if not relationship_cf.value:
                continue

            relationship_values = []
            # value can contain a related UUID, or a list of relations
            if isinstance(relationship_cf.value, list):
                relationship_values.extend(relationship_cf.value)
            else:
                relationship_values.append(relationship_cf)

            for relationship in relationship_values:
                uuid_to_insert_map[str(relationship.value)] = {
                    "id": [],
                    "type": relationship.specifics.relationship_type.name,
                    "magic_string": cf_name,
                    "source_custom_field_uuid": [None],
                }

                uuid_to_id_map[relationship.specifics.relationship_type.name][
                    str(relationship.value)
                ] = None

        # Documents
        db_entries["document"] = (
            self.session.query(schema.File)
            .filter(
                sql.and_(
                    schema.File.uuid.in_(uuid_to_id_map["document"].keys()),
                )
            )
            .all()
        )

        # Custom Objects
        db_entries["custom_object"] = (
            self.session.query(schema.CustomObject)
            .join(
                schema.CustomObjectVersion,
                schema.CustomObject.custom_object_version_id
                == schema.CustomObjectVersion.id,
            )
            .filter(
                sql.and_(
                    schema.CustomObject.uuid.in_(
                        uuid_to_id_map["custom_object"].keys()
                    ),
                    schema.CustomObjectVersion.date_deleted.is_(None),
                )
            )
            .all()
        )

        # Case
        db_entries["case"] = (
            self.session.query(schema.Case)
            .filter(
                sql.and_(
                    schema.Case.uuid.in_(uuid_to_id_map["case"].keys()),
                    schema.Case.status.notin_({"deleted"}),
                    schema.Case.deleted.is_(None),
                )
            )
            .all()
        )

        db_entries["subject"] = self._get_subject_union_query(
            uuid_to_id_map["subject"]
        )

        self._process_relationships(
            custom_object=custom_object,
            current_relationships=current_relationships,
            new_relationships=new_relationships,
            custom_object_version_id=custom_object_version_id,
            custom_object_id=custom_object_id,
            uuid_to_insert_map=uuid_to_insert_map,
            db_entries=db_entries,
        )

    def _get_subject_union_query(self, subject_uuid_to_id_map):
        natural_person = sql.select(
            sql.literal_column("'person'").label("type"),
            schema.NatuurlijkPersoon.uuid,
            schema.NatuurlijkPersoon.id,
        ).where(
            sql.and_(
                schema.NatuurlijkPersoon.uuid.in_(
                    subject_uuid_to_id_map.keys()
                )
            )
        )

        organization = sql.select(
            sql.literal_column("'organization'").label("type"),
            schema.Bedrijf.uuid,
            schema.Bedrijf.id,
        ).where(
            sql.and_(schema.Bedrijf.uuid.in_(subject_uuid_to_id_map.keys()))
        )

        employee = sql.select(
            sql.literal_column("'employee'").label("type"),
            schema.Subject.uuid,
            schema.Subject.id,
        ).where(
            sql.and_(
                schema.Subject.uuid.in_(subject_uuid_to_id_map.keys()),
                schema.Subject.subject_type == "employee",
            )
        )

        sql.union_all(natural_person, organization, employee)
        rproxy = self.session.execute(
            sql.union_all(natural_person, organization, employee)
        )
        return rproxy.fetchall()

    def _process_custom_object_relationships(
        self,
        custom_object,
        current_relationships,
        new_relationships,
        custom_object_version_id,
        custom_object_id,
    ):
        uuid_to_insert_map = {}

        for (
            field_definition
        ) in (
            custom_object.custom_object_type.custom_field_definition.custom_fields
        ):
            if (
                field_definition.custom_field_specification is not None
                and field_definition.custom_field_type.value == "relationship"
                and field_definition.custom_field_specification.type
                == "custom_object"
                and field_definition.magic_string
                in custom_object.custom_fields.keys()
                and custom_object.custom_fields[field_definition.magic_string]
                is not None
            ):
                value = custom_object.custom_fields[
                    field_definition.magic_string
                ]["value"]
                if isinstance(value, list):
                    # multi-value relation
                    for relationship in value:
                        uuid_to_insert_map[relationship["value"]] = {
                            "id": [],
                            "magic_string": None,
                            "type": "custom_object",
                            "source_custom_field_uuid": [None],
                        }
                elif isinstance(value, str):
                    # single-value relation
                    uuid_to_insert_map[value] = {
                        "id": [],
                        "magic_string": None,
                        "type": "custom_object",
                        "source_custom_field_uuid": [None],
                    }
                else:
                    raise ValueError(f"Invalid value {value}")

        db_entries = (
            self.session.query(schema.CustomObject)
            .join(
                schema.CustomObjectVersion,
                schema.CustomObject.custom_object_version_id
                == schema.CustomObjectVersion.id,
            )
            .filter(
                sql.and_(
                    schema.CustomObject.uuid.in_(uuid_to_insert_map.keys()),
                    schema.CustomObjectVersion.date_deleted.is_(None),
                )
            )
            .all()
        )

        self._process_relationships(
            custom_object=custom_object,
            current_relationships=current_relationships,
            new_relationships=new_relationships,
            custom_object_version_id=custom_object_version_id,
            custom_object_id=custom_object_id,
            uuid_to_insert_map=uuid_to_insert_map,
            db_entries={"custom_object": db_entries},
        )

    def _apply_relationships(
        self,
        custom_object,
        custom_object_version_id,
        custom_object_id,
    ):
        # Retrieve current relationships
        current_relationships = (
            self.session.query(schema.CustomObjectRelationship)
            .filter(
                sql.and_(
                    schema.CustomObjectRelationship.custom_object_id
                    == custom_object_id,
                    sql.or_(
                        schema.CustomObjectRelationship.custom_object_version_id.is_(
                            None
                        ),
                        schema.CustomObjectRelationship.custom_object_version_id
                        == custom_object_version_id,
                    ),
                ),
            )
            .all()
        )

        new_relationships = {
            "insert": [],
        }

        # Order is important. When duplicates come around the corner, keep
        # the custom_fields.
        self._process_custom_field_relationships(
            custom_object,
            current_relationships,
            new_relationships,
            custom_object_version_id,
            custom_object_id,
        )

        self._process_case_relationships(
            custom_object,
            current_relationships,
            new_relationships,
            custom_object_version_id,
            custom_object_id,
        )

        self._process_custom_object_relationships(
            custom_object,
            current_relationships,
            new_relationships,
            custom_object_version_id,
            custom_object_id,
        )

        # We now have the db_entries we actually want to have as a relationship
        # between this object version and a document. Now apply them to the db

        # Insert relationships into database

        # Delete existing relationships
        self.session.query(schema.CustomObjectRelationship).filter(
            sql.and_(
                schema.CustomObjectRelationship.custom_object_id
                == custom_object_id
            )
        ).delete(synchronize_session=False)

        rows_inserted = []
        for db_entry in new_relationships["insert"]:
            self.logger.info(f"New db row to insert: {db_entry.values()}")
            if (
                (
                    db_entry[schema.CustomObjectRelationship.related_uuid],
                    db_entry[
                        schema.CustomObjectRelationship.source_custom_field_type_id
                    ],
                )
            ) in rows_inserted:
                self.logger.debug("Skipping already-added relation")
                continue

            logger.info(
                "Adding relationship of type "
                + f"{db_entry[schema.CustomObjectRelationship.relationship_type]} "
                + f"to custom_object with uuid {custom_object.uuid}"
            )
            self.session.execute(
                sql.insert(schema.CustomObjectRelationship).values(db_entry)
            )

            rows_inserted.append(
                (
                    db_entry[schema.CustomObjectRelationship.related_uuid],
                    db_entry[
                        schema.CustomObjectRelationship.source_custom_field_type_id
                    ],
                )
            )

    def _get_table_values_custom_object_version(
        self, custom_object, event, create, new_version, previous_uuid
    ):
        entity_data = _reflect(custom_object)

        cov_table = {
            k: v
            for k, v in entity_data.items()
            if k
            in [
                "status",
                "title",
                "subtitle",
                "external_reference",
                "date_created",
                "last_modified",
                "date_deleted",
                "uuid",
            ]
        }

        if create is True:
            cov_table["version"] = 1

            # Set timestamps
            cov_table["date_created"] = cov_table[
                "last_modified"
            ] = datetime.datetime.now(datetime.UTC)
        elif new_version:
            # Get latest version of this object
            stmt = (
                sql.select(
                    sql.expression.func.max(
                        schema.CustomObjectVersion.version
                    ).label("max_version")
                )
                .select_from(schema.CustomObjectVersion)
                .where(schema.CustomObjectVersion.uuid == previous_uuid)
            )
            rproxy = self.session.execute(stmt)
            maxrow = rproxy.fetchone()
            cov_table["version"] = maxrow.max_version + 1

            # Set timestamps and reset uuid
            cov_table["last_modified"] = datetime.datetime.now(datetime.UTC)

        # First, get the objecttype itself
        ot_table_id = (
            self.session.execute(
                sql.select(schema.CustomObjectTypeVersion.id).where(
                    schema.CustomObjectTypeVersion.uuid
                    == entity_data["custom_object_type"]["uuid"]
                )
            )
            .fetchone()
            .id
        )

        cov_table["custom_object_type_version_id"] = ot_table_id

        return cov_table

    def _get_table_values_custom_object_version_content(
        self, custom_object, event, create, new_version, previous_uuid
    ):
        entity_data = _reflect(custom_object)

        # Check and format custom_fields properly:
        cf_model = custom_field.get_custom_fields_model(
            definition=custom_object.custom_object_type.custom_field_definition
        )

        custom_fields = cf_model.parse_obj(entity_data["custom_fields"]).dict()

        return {
            "archive_ground": entity_data["archive_metadata"]["ground"],
            "archive_status": entity_data["archive_metadata"]["status"],
            "archive_retention": entity_data["archive_metadata"]["retention"],
            "custom_fields": custom_fields,
        }

    def _save_to_database(
        self,
        custom_object,
        event,
        create=None,
        new_version=None,
        previous_uuid=None,
    ):
        logger.info(
            f"Saving custom object with uuid {custom_object.entity_id} to the database"
        )

        cov_table = self._get_table_values_custom_object_version(
            custom_object, event, create, new_version, previous_uuid
        )

        covc_table = self._get_table_values_custom_object_version_content(
            custom_object, event, create, new_version, previous_uuid
        )

        custom_object_version_id = None
        custom_object_id = None

        # Create new version:
        if new_version or create:
            covc_table_id = (
                self.session.execute(
                    sql.insert(schema.CustomObjectVersionContent)
                    .values(covc_table)
                    .returning(schema.CustomObjectVersionContent.id)
                )
                .fetchone()
                .id
            )

            cov_table["custom_object_version_content_id"] = covc_table_id

            custom_object_version_id = (
                self.session.execute(
                    sql.insert(schema.CustomObjectVersion)
                    .values(cov_table)
                    .returning(schema.CustomObjectVersion.id)
                )
                .fetchone()
                .id
            )
        else:
            custom_object_version_id = (
                self.session.execute(
                    sql.select(schema.CustomObjectVersion.id).where(
                        schema.CustomObjectVersion.uuid == previous_uuid
                    )
                )
                .fetchone()
                .id
            )

        if create:
            custom_object_id = (
                self.session.execute(
                    sql.insert(schema.CustomObject)
                    .values(
                        {
                            "uuid": uuid4(),
                            "custom_object_version_id": custom_object_version_id,
                        }
                    )
                    .returning(schema.CustomObject.id)
                )
                .fetchone()
                .id
            )
        else:
            custom_object_id = (
                self.session.execute(
                    sql.select(schema.CustomObject.id).where(
                        schema.CustomObject.uuid
                        == custom_object.version_independent_uuid
                    )
                )
                .fetchone()
                .id
            )

        if new_version:
            self.session.execute(
                sql.update(schema.CustomObject)
                .values(
                    {
                        schema.CustomObject.custom_object_version_id: custom_object_version_id
                    }
                )
                .where(schema.CustomObject.id == custom_object_id)
                .execution_options(synchronize_session=False)
            )

        if new_version or create:
            self.session.execute(
                sql.update(schema.CustomObjectVersion)
                .values(
                    {
                        schema.CustomObjectVersion.custom_object_id: custom_object_id
                    }
                )
                .where(
                    schema.CustomObjectVersion.id == custom_object_version_id
                )
                .execution_options(synchronize_session=False)
            )

        self._apply_relationships(
            custom_object,
            custom_object_version_id=custom_object_version_id,
            custom_object_id=custom_object_id,
        )

    def _apply_changes(self, entity, changes):
        for k, v in changes.items():
            setattr(entity, k, v)

    def find_by_uuid(
        self,
        uuid: UUID,
        user_info: UserInfo | None = None,
        authorization: entities.AuthorizationLevel | None = None,
    ) -> entities.CustomObject | None:
        return self._load_custom_object_from_orm(
            any_uuid=uuid,
            user_info=user_info,
            authorization=authorization,
        )

    def filter_on_attribute(self, qry_components, key, value):
        if key.startswith("attributes."):
            key = key.replace("attributes.", "")

        column = getattr(qry_components["aliases"]["cov"].c, key)
        return column.like(f"%{value}%")

    def filter_on_relationship(self, qry_components, key, value):
        if (
            key
            == "relationships.custom_object_type.attributes.version_independent_uuid"
        ):
            return qry_components["aliases"]["ot"].c.uuid == value

    def filter_on_case_uuid(self, qry_components, key, value):
        relationship_query = sql.select(
            schema.CustomObjectRelationship.custom_object_id
        ).where(schema.CustomObjectRelationship.related_uuid == value)

        return qry_components["aliases"]["co"].c.id.in_(relationship_query)

    def _get_custom_filters(self):
        return {
            "relationships.custom_object_type.attributes.version_independent_uuid": "filter_on_relationship",
            "relationships.cases.id": "filter_on_case_uuid",
        }

    def filter(
        self, user_info: UserInfo, params
    ) -> EntityCollection[entities.CustomObject]:
        custom_filters = self._get_custom_filters()

        qry_components = self._get_db_query_components(user_info=user_info)
        stmt = self._select_stmt_custom_object(qry_components=qry_components)

        filters = []
        for filterkey, filtervalue in params.items():
            if filterkey in custom_filters:
                filters.append(
                    getattr(self, custom_filters[filterkey])(
                        qry_components, filterkey, filtervalue
                    )
                )
            else:
                filters.append(
                    self.filter_on_attribute(
                        qry_components, filterkey, filtervalue
                    )
                )

        # Only retrieve active versions from database
        stmt = stmt.where(
            sql.and_(
                *filters,
                qry_components["aliases"]["cov"].c.date_deleted.is_(None),
                qry_components["aliases"]["co"].c.custom_object_version_id
                == qry_components["aliases"]["cov"].c.id,
            )
        )

        rows = []
        for row in self.session.execute(stmt).fetchall():
            rows.append(self._inflate_row_to_entity(row))

        return EntityCollection[entities.CustomObject](rows)

    def _load_custom_object_from_orm(
        self,
        any_uuid: UUID,
        user_info: UserInfo | None = None,
        authorization: entities.AuthorizationLevel | None = None,
    ) -> entities.CustomObject | None:
        stmt = self._select_stmt_custom_object(
            any_uuid=any_uuid,
            user_info=user_info,
            authorization=authorization,
        ).limit(1)

        row = self.session.execute(stmt).fetchone()
        return self._inflate_row_to_entity(row)

    def _inflate_row_to_entity(self, row) -> entities.CustomObject | None:
        if not row:
            return None

        mapping = {
            "version_independent_uuid": "version_independent_uuid",
            "uuid": "uuid",
            "name": "object_type_name",
            "title": "title",
            "subtitle": "subtitle",
            "external_reference": "external_reference",
            "status": "status",
            "version": "version",
            "custom_fields": "custom_fields",
            "date_created": "date_created",
            "last_modified": "last_modified",
            "date_deleted": "date_deleted",
            "is_active_version": "is_active_version",
            "entity_id": "uuid",
            "entity_meta_summary": "title",
            "entity_meta_authorizations": "authorizations",
        }

        archive_mapping = {
            "status": "archive_status",
            "ground": "archive_ground",
            "retention": "archive_retention",
        }

        object_type_mapping = {
            "uuid": "ot_uuid",
            "name": "ot_name",
            "title": "ot_title",
            "subtitle": "ot_subtitle",
            "external_reference": "ot_external_reference",
            "status": "ot_status",
            "version": "ot_version",
            "custom_field_definition": "ot_custom_field_definition",
            "date_created": "ot_date_created",
            "last_modified": "ot_last_modified",
            "date_deleted": "ot_date_deleted",
            "is_active_version": "ot_is_active_version",
            "version_independent_uuid": "ot_version_independent_uuid",
            "audit_log": "ot_audit_log",
            "entity_id": "ot_uuid",
        }

        entity_obj = {}
        for key, objkey in mapping.items():
            entity_obj[key] = getattr(row, objkey)

        entity_obj["archive_metadata"] = {}
        for key, objkey in archive_mapping.items():
            entity_obj["archive_metadata"][key] = getattr(row, objkey)

        entity_obj["custom_object_type"] = {}
        for key, objkey in object_type_mapping.items():
            entity_obj["custom_object_type"][key] = getattr(row, objkey)

        self._inflate_row_relationships_to_entity(row, entity_obj)

        return entities.CustomObject.parse_obj(
            {**entity_obj, "_event_service": self.event_service}
        )

    def _inflate_row_relationships_to_entity(self, row, entity_obj):
        if row.cases:
            entity_obj["cases"] = []
            for r in row.cases:
                entity_obj["cases"].append(
                    {
                        "uuid": r["uuid"],
                        "entity_id": r["uuid"],
                        "source_custom_field_uuid": r[
                            "source_custom_field_uuid"
                        ],
                    }
                )

        if row.custom_objects:
            entity_obj["custom_objects"] = []
            for uuid in row.custom_objects:
                entity_obj["custom_objects"].append(
                    {"uuid": uuid, "entity_id": uuid}
                )

        if row.documents:
            entity_obj["documents"] = []
            for uuid in row.documents:
                entity_obj["documents"].append(
                    {"uuid": uuid, "entity_id": uuid}
                )

        if row.subjects:
            entity_obj["subjects"] = []
            for uuid in row.subjects:
                entity_obj["subjects"].append(
                    {"uuid": uuid, "entity_id": uuid}
                )

    def _get_db_query_components(
        self,
        user_info: UserInfo,
    ):
        co = sql.alias(schema.CustomObject, name="co")
        ot = sql.alias(schema.CustomObjectType, name="ot")
        otv = sql.alias(schema.CustomObjectTypeVersion, name="otv")
        cov = sql.alias(schema.CustomObjectVersion, name="cov")
        covc = sql.alias(schema.CustomObjectVersionContent, name="covc")
        cocr = sql.alias(schema.CustomObjectRelationship, name="cocr")
        bk = sql.alias(schema.BibliotheekKenmerk, name="bk")

        columns = [
            co.c.uuid.label("version_independent_uuid"),
            sql.case(
                (
                    co.c.custom_object_version_id == cov.c.id,
                    sql.literal_column("1"),
                ),
                else_=sql.literal_column("0"),
            ).label("is_active_version"),
            otv.c.uuid.label("object_type_uuid"),
            otv.c.name.label("object_type_name"),
            cov.c.uuid,
            cov.c.title,
            cov.c.subtitle,
            cov.c.external_reference,
            cov.c.status,
            cov.c.version,
            cov.c.date_created,
            cov.c.last_modified,
            cov.c.date_deleted,
            sql.func.array(
                _object_authorizations_subquery(
                    user_uuid=user_info.user_uuid,
                    custom_object_version_alias=cov,
                )
            ).label("authorizations")
            if user_info
            else sql.literal(None).label("authorizations"),
            sql.func.array(
                sql.select(
                    sql.func.json_build_object(
                        "uuid",
                        cocr.c.related_uuid,
                        "source_custom_field_uuid",
                        bk.c.uuid,
                    )
                )
                .select_from(
                    sql.join(
                        cocr,
                        bk,
                        bk.c.id == cocr.c.source_custom_field_type_id,
                        isouter=True,
                    )
                )
                .where(
                    sql.and_(
                        cov.c.custom_object_id == cocr.c.custom_object_id,
                        cocr.c.relationship_type == "case",
                    )
                )
                .scalar_subquery()
            ).label("cases"),
            sql.func.array(
                sql.select(sql.distinct(cocr.c.related_uuid))
                .select_from(cocr)
                .where(
                    sql.and_(
                        cov.c.custom_object_id == cocr.c.custom_object_id,
                        cocr.c.relationship_type == "custom_object",
                    )
                )
                .label("custom_objects_subq")
            ).label("custom_objects"),
            sql.func.array(
                sql.select(sql.distinct(cocr.c.related_uuid))
                .select_from(cocr)
                .where(
                    sql.and_(
                        cov.c.custom_object_id == cocr.c.custom_object_id,
                        cocr.c.relationship_type == "document",
                    )
                )
                .label("documents_subq")
            ).label("documents"),
            sql.func.array(
                sql.select(sql.distinct(cocr.c.related_uuid))
                .select_from(cocr)
                .where(
                    sql.and_(
                        cov.c.custom_object_id == cocr.c.custom_object_id,
                        cocr.c.relationship_type == "subject",
                    )
                )
                .label("subjects_subq")
            ).label("subjects"),
            covc.c.custom_fields,
            covc.c.archive_status,
            covc.c.archive_ground,
            covc.c.archive_retention,
            sql.case(
                (
                    ot.c.custom_object_type_version_id == otv.c.id,
                    sql.literal_column("1"),
                ),
                else_=sql.literal_column("0"),
            ).label("ot_is_active_version"),
            ot.c.uuid.label("ot_version_independent_uuid"),
            otv.c.uuid.label("ot_uuid"),
            otv.c.name.label("ot_name"),
            otv.c.title.label("ot_title"),
            otv.c.subtitle.label("ot_subtitle"),
            otv.c.external_reference.label("ot_external_reference"),
            otv.c.status.label("ot_status"),
            otv.c.version.label("ot_version"),
            otv.c.custom_field_definition.label("ot_custom_field_definition"),
            otv.c.relationship_definition.label("ot_relationship_definition"),
            otv.c.audit_log.label("ot_audit_log"),
            otv.c.date_created.label("ot_date_created"),
            otv.c.last_modified.label("ot_last_modified"),
            otv.c.date_deleted.label("ot_date_deleted"),
        ]

        join = sql.join(
            co,
            sql.join(
                sql.join(
                    cov,
                    sql.join(otv, ot, otv.c.custom_object_type_id == ot.c.id),
                    cov.c.custom_object_type_version_id == otv.c.id,
                ),
                covc,
                cov.c.custom_object_version_content_id == covc.c.id,
            ),
            co.c.id == cov.c.custom_object_id,
        )

        return {
            "columns": columns,
            "join": join,
            "aliases": {
                "co": co,
                "ot": ot,
                "otv": otv,
                "cov": cov,
                "covc": covc,
            },
        }

    def _select_stmt_custom_object(
        self,
        any_uuid=None,
        version_uuid=None,
        version_independent_uuid=None,
        qry_components=None,
        user_info=None,
        authorization=None,
    ):
        """Generate query to PGSQL to return not deleted rows related to
        the given UUID"""

        if qry_components:
            co_query = qry_components
        else:
            co_query = self._get_db_query_components(user_info=user_info)

        stmt = sql.select(*co_query["columns"]).select_from(co_query["join"])

        # ### Filter on uuid and date
        uuid_select_clause = []
        if version_uuid is not None:
            uuid_select_clause.append(
                co_query["aliases"]["cov"].c.uuid == version_uuid
            )
        elif (
            version_independent_uuid is not None
        ):  # pragma: no cover : TODO, implement version_independent
            uuid_select_clause.append(
                co_query["aliases"]["co"].c.uuid == version_independent_uuid
            )
        elif any_uuid is not None:
            uuid_select_clause.append(
                sql.or_(
                    co_query["aliases"]["co"].c.uuid == any_uuid,
                    co_query["aliases"]["cov"].c.uuid == any_uuid,
                )
            )

        if authorization:
            uuid_select_clause.append(
                object_acl.allowed_object_v2_subquery(
                    user_info=user_info,
                    authorization=authorization,
                    object_alias=co_query["aliases"]["co"],
                )
            )

        where_clause = sql.and_(
            *uuid_select_clause,
            sql.or_(
                co_query["aliases"]["cov"].c.date_deleted
                > datetime.datetime.now(datetime.UTC),
                co_query["aliases"]["cov"].c.date_deleted.is_(None),
            ),
        )

        return stmt.where(where_clause).order_by(
            sql.desc(sql.column("is_active_version"))
        )

    def _delete_custom_object(self, event, user_info=None, dry_run=False):
        """Delete All version and content references for given custom object"""

        stmt = sql.select(schema.CustomObjectVersion.custom_object_id)

        rproxy = self.session.execute(
            stmt.where(schema.CustomObjectVersion.uuid == str(event.entity_id))
        )
        row = rproxy.fetchone()
        object_id = row.custom_object_id

        if not object_id:
            raise exceptions.NotFound(
                f'Could not find "custom_object_version" with uuid {event.entity_id!s}',
                "case_management/custom_object/not_found",
            )

        stmt = sql.select(
            schema.CustomObjectVersion.custom_object_version_content_id
        )

        rows = self.session.execute(
            stmt.where(
                schema.CustomObjectVersion.custom_object_id == object_id
            )
        ).fetchall()
        version_content_id_list = [
            row.custom_object_version_content_id for row in rows
        ]

        self.session.execute(
            sql.delete(schema.CustomObject)
            .where(schema.CustomObject.id == object_id)
            .execution_options(synchronize_session=False)
        )

        self.session.execute(
            sql.delete(schema.CustomObjectVersionContent)
            .where(
                schema.CustomObjectVersionContent.id.in_(
                    version_content_id_list
                )
            )
            .execution_options(synchronize_session=False)
        )
