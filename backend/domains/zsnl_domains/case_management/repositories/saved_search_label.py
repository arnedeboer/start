# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
import minty.exceptions
from ... import ZaaksysteemRepositoryBase
from ..entities import saved_search_label as entity
from minty import repository
from sqlalchemy import sql
from uuid import UUID, uuid4
from zsnl_domains.database import schema


class SavedSearchLabelRepository(
    repository.Repository, ZaaksysteemRepositoryBase
):
    _for_entity = "SavedSearchLabel"
    _events_to_calls = {
        "SavedSearchLabelCreated": "_save_create",
        "SavedSearchLabelDeleted": "_save_delete",
    }

    def get_all_labels(self):
        query = sql.select(
            schema.SavedSearchLabels.uuid,
            schema.SavedSearchLabels.label,
        )
        result = self.session.execute(query).fetchall()

        return [self._sqla_to_entity_labels(row=row) for row in result]

    def get_labels_by_name(self, labels):
        query = sql.select(
            schema.SavedSearchLabels.uuid,
            schema.SavedSearchLabels.label,
        )
        result = self.session.execute(
            query.where(schema.SavedSearchLabels.label.in_(labels))
        ).fetchall()
        if result is not None:
            return [self._sqla_to_entity_labels(row=row) for row in result]

    def _sqla_to_entity_labels(self, row):
        return entity.SavedSearchLabel(
            entity_id=row.uuid,
            uuid=row.uuid,
            name=row.label,
            _event_service=self.event_service,
        )

    def create(
        self,
        name: str,
        uuid: UUID | None = None,
    ):
        if uuid is None:
            uuid = uuid4()
        label = entity.SavedSearchLabel.create(
            uuid=uuid,
            name=name,
            event_service=self.event_service,
        )

        return label

    def _save_create(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        changes = event.format_changes()
        self.session.execute(
            sql.insert(schema.SavedSearchLabels).values(
                label=changes["name"], uuid=changes["uuid"]
            )
        )

    # get list of unused labels
    def get_unused_lables(self):
        query = sql.select(
            schema.SavedSearchLabels.uuid, schema.SavedSearchLabels.label
        ).where(
            schema.SavedSearchLabels.id.not_in(
                sql.select(schema.SavedSearchLabelsMapping.label_id)
            )
        )
        result = self.session.execute(query).fetchall()
        if result is not None:
            return [self._sqla_to_entity_labels(row=row) for row in result]

    def _save_delete(
        self,
        event: minty.cqrs.Event,
        userinfo: minty.cqrs.UserInfo,
        dry_run: bool = False,
    ):
        self.session.execute(
            sql.delete(schema.SavedSearchLabels).where(
                schema.SavedSearchLabels.uuid == event.entity_id
            )
        )
