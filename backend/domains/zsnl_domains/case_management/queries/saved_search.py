# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
from ...shared.validators import split_comma_separated_field
from .. import repositories
from ..entities import saved_search as entity
from pydantic import BaseModel, Field, validate_arguments, validator
from typing import cast
from uuid import UUID


class ListSavedSearchFilter(BaseModel):
    kind: entity.SavedSearchKind | None = Field(
        None, title="Kind of saved search to retrieve a list of"
    )
    labels: list[str] | None = Field(
        None, title="Labels for saved search to retrieve a list of"
    )

    class Config:
        validate_all = True

    _split_comma_separated_fields = validator(
        "labels",
        allow_reuse=True,
        pre=True,
    )(split_comma_separated_field)


class GetSavedSearch(minty.cqrs.SplitQueryBase):
    name = "get_saved_search"

    @validate_arguments
    def __call__(self, uuid: UUID) -> entity.SavedSearch:
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        return repo.get(
            uuid=uuid,
            user_info=self.qry.user_info,
        )


class ListSavedSearch(minty.cqrs.SplitQueryBase):
    name = "list_saved_search"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
        filter: ListSavedSearchFilter | None = None,
    ) -> minty.entity.EntityCollection[entity.SavedSearch]:
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        result = repo.get_multiple(
            kind=filter.kind if filter else None,
            labels=filter.labels if filter else None,
            page=page,
            page_size=page_size,
            user_info=self.qry.user_info,
        )

        return result


class GetSavedSearchLabels(minty.cqrs.SplitQueryBase):
    name = "get_saved_search_labels"

    @validate_arguments
    def __call__(
        self,
    ) -> minty.entity.EntityCollection[entity.SavedSearchLabel]:
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search_label"),
        )

        result = repo.get_all_labels()

        return result
