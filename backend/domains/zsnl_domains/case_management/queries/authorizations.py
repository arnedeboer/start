# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import DepartmentRepository, RoleRepository
from typing import cast
from uuid import UUID


class GetDepartments(minty.cqrs.SplitQueryBase):
    name = "get_departments"

    def __call__(self):
        """Get a list of all departments configured in the system"""

        department_repo = cast(
            DepartmentRepository, self.get_repository("department")
        )
        return department_repo.find_all()


class GetRoles(minty.cqrs.SplitQueryBase):
    name = "get_roles"

    def __call__(self, filter_parent_uuid: str | None = None):
        """Get a list of all departments configured in the system"""

        role_repo = cast(RoleRepository, self.get_repository("role"))
        return role_repo.find_all(
            filter_parent_uuid=UUID(filter_parent_uuid)
            if filter_parent_uuid
            else None
        )
