# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ...shared.types import (
    ComparisonFilterConditionStr,
    DepartmentRoleTypedDict,
    FilterMultipleValuesWithOperator,
)
from .._shared import get_subjects
from ..entities.case_search_result import CaseSearchOrder, CaseSearchResult
from ..repositories import CaseSearchResultRepository
from minty.entity import EntityCollection
from pydantic import BaseModel, Field, validate_arguments
from typing import cast
from uuid import UUID
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidCaseUrgency,
    ValidContactChannel,
)


class CaseSearchResultFilter(BaseModel):
    filter_status: set[ValidCaseStatus] | None = Field(
        None, alias="attributes.status"
    )

    case_type_uuids: set[UUID] | None = Field(
        None, alias="relationship.case_type.id"
    )

    assignee_uuids: set[UUID] | None = Field(
        None, alias="relationship.assignee.id"
    )
    coordinator_uuids: set[UUID] | None = Field(
        None, alias="relationship.coordinator.id"
    )
    requestor_uuids: set[UUID] | None = Field(
        None, alias="relationship.requestor.id"
    )

    filter_registration_date: None | (
        list[ComparisonFilterConditionStr]
    ) = Field(None, alias="attributes.registration_date")

    filter_completion_date: None | (
        list[ComparisonFilterConditionStr]
    ) = Field(None, alias="attributes.completion_date")

    filter_payment_status: set[ValidCasePaymentStatus] | None = Field(
        None, alias="attributes.payment_status"
    )
    filter_channel_of_contact: set[ValidContactChannel] | None = Field(
        None, alias="attributes.channel_of_contact"
    )
    filter_confidentiality: set[ValidCaseConfidentiality] | None = Field(
        None, alias="attributes.confidentiality"
    )
    filter_archival_state: set[ValidCaseArchivalState] | None = Field(
        None, alias="attributes.archival_state"
    )
    filter_retention_period_source_date: None | (
        set[ValidCaseRetentionPeriodSourceDate]
    ) = Field(None, alias="attributes.retention_period_source_date")

    filter_result: set[ValidCaseResult] | None = Field(
        None, alias="attributes.result"
    )
    filter_case_location: FilterMultipleValuesWithOperator[str] | None = Field(
        None, alias="attributes.case_location"
    )
    filter_num_unread_messages: None | (
        list[ComparisonFilterConditionStr]
    ) = Field(None, alias="attributes.num_unread_messages")

    filter_num_unaccepted_files: None | (
        list[ComparisonFilterConditionStr]
    ) = Field(None, alias="attributes.num_unaccepted_files")

    filter_num_unaccepted_updates: None | (
        list[ComparisonFilterConditionStr]
    ) = Field(None, alias="attributes.num_unaccepted_updates")

    filter_keyword: FilterMultipleValuesWithOperator[str] | None = Field(
        None, alias="keyword"
    )
    filter_period_of_preservation_active: bool | None = Field(
        None, alias="attributes.period_of_preservation_active"
    )
    filter_subject: str | None = Field(None, alias="attributes.subject")
    filter_urgency: list[ValidCaseUrgency] | None = Field(
        None, alias="attributes.urgency"
    )
    operator: str | None = Field(None, alias="operator")
    filter_department_role: FilterMultipleValuesWithOperator[
        DepartmentRoleTypedDict
    ] | None = Field(None, alias="attributes.department_role")

    class Config:
        validate_all = True


class CaseSearch(minty.cqrs.SplitQueryBase):
    name = "search_case"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
        filters: CaseSearchResultFilter | None = None,
        sort: CaseSearchOrder = CaseSearchOrder.number_desc,
        includes: list[str] | None = None,
    ) -> EntityCollection[CaseSearchResult]:
        """
        Search for Cases based on the filters
        """
        if not filters:
            filters = CaseSearchResultFilter()
        repo = cast(
            CaseSearchResultRepository,
            self.get_repository("case_search_result"),
        )
        cases = repo.search(
            user_info=self.qry.user_info,
            permission="search",
            page=page,
            page_size=page_size,
            filters=filters.dict(),
            sort=sort,
        )
        related_contacts = None
        if includes is not None:
            related_contacts = self._get_contacts(includes, cases)
            return EntityCollection(cases, included_entities=related_contacts)
        return cases

    def _get_contacts(self, includes_contact_types, cases):
        contact_list = []
        if "requestor" in includes_contact_types:
            contact_list = contact_list + [
                {
                    "uuid": entity.requestor.entity_id,
                    "type": entity.requestor.entity_type,
                }
                for entity in cases.entities
            ]
        if "assignee" in includes_contact_types:
            contact_list = contact_list + [
                {"uuid": entity.assignee.entity_id, "type": "employee"}
                for entity in cases.entities
                if entity.assignee
            ]
        if "coordinator" in includes_contact_types:
            contact_list = contact_list + [
                {"uuid": entity.coordinator.entity_id, "type": "employee"}
                for entity in cases.entities
                if entity.coordinator
            ]
        contact_list = {v["uuid"]: v for v in contact_list}.values()
        person_list = [
            v["uuid"] for v in contact_list if v["type"] == "person"
        ]

        employee_list = [
            v["uuid"] for v in contact_list if v["type"] == "employee"
        ]
        organization_list = [
            v["uuid"] for v in contact_list if v["type"] == "organization"
        ]

        related_contacts = []
        if len(person_list) >= 1:
            subjects = get_subjects(
                get_repository=self.get_repository,
                subject_type="person",
                subject_uuids=person_list,
            )
            related_contacts.extend(subjects)
        if len(employee_list) >= 1:
            subjects = get_subjects(
                get_repository=self.get_repository,
                subject_type="employee",
                subject_uuids=employee_list,
            )
            related_contacts.extend(subjects)
        if len(organization_list) >= 1:
            subjects = get_subjects(
                get_repository=self.get_repository,
                subject_type="organization",
                subject_uuids=organization_list,
            )
            related_contacts.extend(subjects)
        return related_contacts


class CaseSearchTotalResults(minty.cqrs.SplitQueryBase):
    name = "search_case_total_results"

    @validate_arguments
    def __call__(self, filters: CaseSearchResultFilter | None = None):
        if not filters:
            filters = CaseSearchResultFilter()
        repo = cast(
            CaseSearchResultRepository,
            self.get_repository("case_search_result"),
        )
        total_result = repo.search_total_results(
            user_info=self.qry.user_info,
            permission="search",
            filters=filters.dict(),
        )

        return total_result
