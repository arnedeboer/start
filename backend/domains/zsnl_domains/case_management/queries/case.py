# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import CaseRepository, CaseSummaryRepository
from collections.abc import Iterable
from minty.exceptions import ValidationError
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import validate_arguments
from typing import cast
from uuid import UUID
from zsnl_domains.case_management.entities.check_result import CheckResult


class Get(minty.cqrs.SplitQueryBase):
    name = "get_case_by_uuid"

    def __call__(self, case_uuid: str) -> dict:
        """
        Get case entity as python dictionary by given `case id`.

        :param case_uuid: identifier of case
        :return: case dictionary
        """

        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.qry.user_info,
            permission="read",
        )
        return case


class GetSummaries(minty.cqrs.SplitQueryBase):
    name = "get_case_summaries_by_uuid"

    @validate_with(get_data(__name__, "validation/get_case_summaries.json"))
    def __call__(self, case_uuids: Iterable[str]):
        """
        Get list of case_summaries by uuid.

        :param case_uuids: List of UUIDs
        :return: List of case_summary entities
        """

        case_summary_repo = cast(
            CaseSummaryRepository, self.get_repository("case_summary")
        )
        case_summaries = case_summary_repo.get_case_summaries_by_uuid(
            uuids=map(lambda u: UUID(u), case_uuids), user_uuid=self.user_uuid
        )
        return {"result": case_summaries}


class CaseAllocationCheck(minty.cqrs.SplitQueryBase):
    name = "case_allocation_check"

    @validate_arguments
    def __call__(
        self,
        department_uuid: UUID,
        role_uuid: UUID,
        case_uuid: UUID | None = None,
        casetype_uuid: UUID | None = None,
    ) -> CheckResult:
        case_repo = cast(CaseRepository, self.get_repository("case"))

        if (case_uuid is None and casetype_uuid is None) or (
            case_uuid and casetype_uuid
        ):
            raise ValidationError(
                "One of <case_uuid> or <casetype_uuid> should be specified."
            )

        if case_uuid:
            # Output discarded, used to only check the case access
            case_repo.find_case_by_uuid(
                case_uuid=case_uuid,
                user_info=self.qry.user_info,
                permission="write",
            )

        check_result = case_repo.check_allocation_rights(
            department_uuid=department_uuid,
            role_uuid=role_uuid,
            case_uuid=case_uuid,
            casetype_uuid=casetype_uuid,
        )

        return check_result
