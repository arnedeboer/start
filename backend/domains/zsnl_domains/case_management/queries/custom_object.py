# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ...shared.custom_field import CustomFieldTypes
from ..entities import AuthorizationLevel, RelatedObject, RelatedSubject
from ..entities.custom_object import CustomObject
from ..repositories import (
    CustomObjectRepository,
    FileRepository,
    RelatedCaseRepository,
    RelatedObjectRepository,
    RelatedSubjectRepository,
)
from minty.entity import RedirectResponse
from minty.exceptions import NotFound
from pydantic import validate_arguments
from typing import cast
from uuid import UUID


class GetCustomObject(minty.cqrs.SplitQueryBase):
    name = "get_custom_object_by_uuid"

    @validate_arguments
    def __call__(self, uuid: UUID):
        """
        Get a custom object by its uuid.

        :param uuid: identifier of the custom object
        :return: CustomObject
        """

        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_object = repo.find_by_uuid(
            uuid=uuid,
            user_info=self.qry.user_info,
            authorization=AuthorizationLevel.read,
        )

        if not custom_object:
            raise NotFound(
                "Could not find or rights are insufficient for 'custom_object' with uuid {}".format(
                    uuid
                ),
                "case_management/custom_object/object/not_found",
            )

        return custom_object


class GetCustomObjects(minty.cqrs.SplitQueryBase):
    name = "get_custom_objects"

    def __call__(self, filter_params=None):
        """
        Get a list of custom objects

        Returns a list of all custom objects

        :param filter_params: filter of custom objects
        :return: CustomObject
        """

        if filter_params is None:
            filter_params = {}

        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_objects = repo.filter(
            user_info=self.qry.user_info, params=filter_params
        )

        return custom_objects


class GetRelatedCases(minty.cqrs.SplitQueryBase):
    name = "get_related_cases_for_custom_object"

    def __call__(self, object_uuid: str):
        """
        Get a list of cases related to the custom_object

        Returns a list of all cases related to object

        :param object_uuid: identifier of the custom_object_type
        :return: RelatedCases
        """

        repo = cast(RelatedCaseRepository, self.get_repository("related_case"))
        related_cases = repo.find_for_custom_object(
            object_uuid=UUID(object_uuid), user_uuid=self.user_uuid
        )
        return related_cases


class GetRelatedObjects(minty.cqrs.SplitQueryBase):
    name = "get_related_objects_for_custom_object"

    def __call__(self, object_uuid: str):
        """
        Get a list of objects related to the custom_object

        Returns a list of all custom_objects related to object

        :param object_uuid: identifier of the custom_object
        :type object_uuid: UUID
        :return: custom_objects related to custom_object
        :rtype: List[RelatedObject]
        """

        repo = cast(
            RelatedObjectRepository, self.get_repository("related_object")
        )
        related_objects = repo.find_for_custom_object(
            object_uuid=UUID(object_uuid)
        )
        return related_objects


class GetRelatedCustomObjectsForSubject(minty.cqrs.SplitQueryBase):
    name = "get_related_custom_objects_for_subject"

    def __call__(self, subject_uuid: str) -> list[RelatedObject]:
        """
        Get a list of custom objects related to a subject

        :param subject_uuid: identifier of the subject to search in
        :return: related custom objects
        """

        repo = cast(
            RelatedObjectRepository, self.get_repository("related_object")
        )
        related_objects = repo.find_for_subject(
            subject_uuid=UUID(subject_uuid)
        )
        return related_objects


class GetRelatedSubjects(minty.cqrs.SplitQueryBase):
    name = "get_related_subjects_for_custom_object"

    def __call__(self, object_uuid: str) -> list[RelatedSubject]:
        """
        Get a list of subjects related to the custom_object

        Returns a list of all subjects related to object

        :param object_uuid: identifier of the custom_object
        :return: subjects related to custom_object
        """

        repo = cast(
            RelatedSubjectRepository, self.get_repository("related_subject")
        )
        related_contacts = repo.find_for_custom_object(
            object_uuid=UUID(object_uuid)
        )
        return related_contacts


class DownloadFile(minty.cqrs.SplitQueryBase):
    name = "download_file_for_custom_object"

    def _assert_file_in_object(
        self, custom_object: CustomObject, file_uuid: UUID
    ):
        file_custom_fields = custom_object.get_custom_fields_by_type(
            type=CustomFieldTypes.file
        )
        file_items_in_custom_fields: list = []
        for cfd in file_custom_fields.values():
            if cfd is not None:
                file_items_in_custom_fields.extend(cfd["value"])
        file_uuid_present = any(
            item["value"] == file_uuid for item in file_items_in_custom_fields
        )
        if not file_uuid_present:
            raise NotFound(
                "Could not find file with uuid {} for custom_object with uuid {}".format(
                    file_uuid, custom_object.uuid
                )
            )

    def __call__(self, object_uuid: UUID, file_uuid: UUID):
        # First try to load the object to check if the user had the correct permissions to load the object.
        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_object = repo.find_by_uuid(
            uuid=object_uuid,
            user_info=self.qry.user_info,
            authorization=AuthorizationLevel.read,
        )
        if not custom_object:
            raise NotFound(
                "Could not find or rights are insufficient for 'custom_object' with uuid {}".format(
                    object_uuid
                ),
                "case_management/custom_object/object/not_found",
            )

        # Assert the requested file_uuid is present in one of the custom_fields of the object
        self._assert_file_in_object(custom_object, file_uuid)

        # user is allowed to read the object. Now start downloading the file
        repo = cast(FileRepository, self.get_repository("file"))
        file = repo.find_file_by_uuid(uuid=file_uuid)

        return RedirectResponse(location=file.download_url)


class GetRelatedSubjectsForSubject(minty.cqrs.SplitQueryBase):
    name = "get_related_subjects_for_subject"

    def __call__(self, subject_uuid: str) -> list[RelatedSubject]:
        """
        Get a list of subjects related to another subject

        :param subject_uuid: UUID of the subject to retrieve relationships of
        :return: subjects related to subject with specified UUID
        """

        repo = cast(
            RelatedSubjectRepository, self.get_repository("related_subject")
        )
        related_contacts = repo.find_for_subject(
            subject_uuid=UUID(subject_uuid)
        )
        return related_contacts
