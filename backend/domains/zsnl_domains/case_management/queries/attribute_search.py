# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..entities import attribute_search as search_entities
from ..repositories import AttributeSearchRepository
from minty.entity import EntityCollection
from pydantic import BaseModel, Field, validate_arguments
from typing import cast


class AttributeSearchFilter(BaseModel):
    filter_keyword: str | None = Field(None, alias="keyword")

    class Config:
        validate_all = True


class AttributeSearch(minty.cqrs.SplitQueryBase):
    name = "search_attribute"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
        filters: AttributeSearchFilter | None = None,
    ) -> EntityCollection[search_entities.AttributeSearch]:
        """
        Get a list of filtered attribute search results.
        """
        if not filters:
            filters = AttributeSearchFilter()

        attribute_search_repo = cast(
            AttributeSearchRepository,
            self.get_repository("attribute_search"),
        )

        result = attribute_search_repo.search(
            page=page,
            page_size=page_size,
            user_info=self.qry.user_info,
            filters=filters.dict(),
        )
        return result
