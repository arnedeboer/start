# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from .. import entities
from ..repositories import ExportFileRepository
from minty.entity import RedirectResponse
from pydantic import validate_arguments
from typing import cast


class GetExportFileList(minty.cqrs.SplitQueryBase):
    name = "get_export_file_list"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
    ) -> list[entities.ExportFile]:
        """
        Get list of export files available to the current user doing the query
        """
        repo = cast(ExportFileRepository, self.get_repository("export_file"))

        export_file_list = repo.get_export_file_list(
            user_uuid=self.qry.user_info.user_uuid,
            page=page,
            page_size=page_size,
        )
        return export_file_list


class DownloadExportFile(minty.cqrs.SplitQueryBase):
    name = "download_export_file"

    @validate_arguments
    def __call__(self, token: str) -> RedirectResponse:
        """
        Return the download URL of the export file with the specified token
        """
        repo = cast(ExportFileRepository, self.get_repository("export_file"))

        export_file = repo.get_export_file_by_token(
            token=token, user_info=self.qry.user_info
        )

        download_url = repo.get_export_download_url(
            export_file=export_file, user_info=self.qry.user_info
        )
        return RedirectResponse(location=download_url)
