# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ...shared.types import (
    ComparisonFilterConditionStr,
    CustomObjectAttributesValueFilter,
    FilterMultipleValuesWithOperator,
)
from ..entities import custom_object as object_entities
from ..entities import custom_object_search_result as search_entities
from ..repositories import CustomObjectSearchResultRepository
from minty.entity import EntityCollection
from pydantic import BaseModel, Field, validate_arguments
from typing import cast
from uuid import UUID


class CustomObjectSearchResultFilter(BaseModel):
    custom_object_type_uuid: UUID = Field(
        ..., alias="relationship.custom_object_type.id"
    )

    filter_status: set[object_entities.ValidObjectStatus] | None = Field(
        None, alias="attributes.status"
    )

    filter_archive_status: None | (
        set[object_entities.ValidArchiveStatus]
    ) = Field(None, alias="attributes.archive_status")

    filter_last_modified: list[ComparisonFilterConditionStr] | None = Field(
        None, alias="attributes.last_modified"
    )

    filter_keyword: FilterMultipleValuesWithOperator[str] | None = Field(
        None, alias="keyword"
    )

    filter_title: str | None = Field(None, alias="attributes.title")

    filter_subtitle: str | None = Field(None, alias="attributes.subtitle")

    filter_external_reference: str | None = Field(
        None, alias="attributes.external_reference"
    )

    filter_attributes_value: list[
        CustomObjectAttributesValueFilter
    ] | None = Field(None, alias="attributes.value")

    operator: str | None = Field(None, alias="operator")

    class Config:
        validate_all = True


class CustomObjectSearchResult(minty.cqrs.SplitQueryBase):
    name = "search_custom_objects"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
        filters: CustomObjectSearchResultFilter,
        sort: None
        | (
            search_entities.CustomObjectSearchOrder
        ) = search_entities.CustomObjectSearchOrder.title_asc,
    ) -> EntityCollection[search_entities.CustomObjectSearchResult]:
        """
        Get a list of filtered custom object search results.
        """

        custom_object_search_repo = cast(
            CustomObjectSearchResultRepository,
            self.get_repository("custom_object_search"),
        )

        result = custom_object_search_repo.search(
            page=page,
            page_size=page_size,
            sort=sort,
            user_info=self.qry.user_info,
            filters=filters.dict(),
        )
        return result


class CustomObjectSearchTotalResults(minty.cqrs.SplitQueryBase):
    name = "search_custom_object_total_results"

    @validate_arguments
    def __call__(self, filters: CustomObjectSearchResultFilter):
        custom_object_search_repo = cast(
            CustomObjectSearchResultRepository,
            self.get_repository("custom_object_search"),
        )

        total_result = custom_object_search_repo.search_total_results_count(
            user_info=self.qry.user_info,
            filters=filters.dict(),
        )

        return total_result
