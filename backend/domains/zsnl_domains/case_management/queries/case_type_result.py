# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..entities import CaseTypeResult
from ..repositories import CaseTypeResultRepository
from minty.entity import EntityCollection
from pydantic import validate_arguments
from typing import cast
from uuid import UUID


class GetCaseAvailableResultTypes(minty.cqrs.SplitQueryBase):
    name = "get_case_available_result_types"

    @validate_arguments
    def __call__(self, case_uuid: str) -> EntityCollection[CaseTypeResult]:
        """
        Get case type results entity as EntityCollection given `case uuid`.
        """
        repo = cast(
            CaseTypeResultRepository, self.get_repository("case_type_result")
        )
        collection = repo.get_multiple(
            case_uuid=UUID(case_uuid),
            user_info=self.qry.user_info,
            permission="read",
        )
        return collection
