# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .entities import Employee, Organization, Person
from .entities.person import PersonLimited
from .repositories import (
    EmployeeRepository,
    OrganizationRepository,
    PersonRepository,
    SavedSearchLabelRepository,
)
from collections.abc import Callable
from typing import cast
from uuid import UUID


def get_subject(
    get_repository: Callable, subject_type: str, subject_uuid: UUID
) -> Person | Organization | Employee:
    if subject_type == "person":
        subject_repo = cast(PersonRepository, get_repository("person"))
        subject = subject_repo.find_person_by_uuid(subject_uuid)
    elif subject_type == "organization":
        subject_repo = cast(
            OrganizationRepository, get_repository("organization")
        )
        subject = subject_repo.find_organization_by_uuid(subject_uuid)
    elif subject_type == "employee":
        subject_repo = cast(EmployeeRepository, get_repository("employee"))
        subject = subject_repo.find_employee_by_uuid(subject_uuid)
    else:  # pragma: no cover
        raise KeyError(subject_type)

    return subject


def get_subject_limited(
    get_repository: Callable, subject_type: str, subject_uuid: UUID
) -> PersonLimited | Organization | Employee:
    if subject_type == "person":
        subject_repo = cast(PersonRepository, get_repository("person"))
        subject = subject_repo.find_person_by_uuid_limited(subject_uuid)
    elif subject_type == "organization":
        subject_repo = cast(
            OrganizationRepository, get_repository("organization")
        )
        subject = subject_repo.find_organization_by_uuid_limited(subject_uuid)
    elif subject_type == "employee":
        subject_repo = cast(EmployeeRepository, get_repository("employee"))
        subject = subject_repo.find_employee_by_uuid_limited(subject_uuid)
    else:  # pragma: no cover
        raise KeyError(subject_type)

    return subject


def get_subjects(
    get_repository: Callable, subject_type: str, subject_uuids: list[UUID]
) -> list[Person] | list[Organization] | list[Employee]:
    if subject_type == "person":
        subject_repo = cast(PersonRepository, get_repository("person"))
        subjects = subject_repo.get_persons_by_uuid(subject_uuids)
    elif subject_type == "organization":
        subject_repo = cast(
            OrganizationRepository, get_repository("organization")
        )
        subjects = subject_repo.get_organizations_by_uuid(subject_uuids)
    elif subject_type == "employee":
        subject_repo = cast(EmployeeRepository, get_repository("employee"))
        subjects = subject_repo.get_employees_by_uuid(subject_uuids)
    else:  # pragma: no cover
        raise KeyError(subject_type)

    return subjects


def delete_unused_saved_search_labels(get_repository: Callable):
    saved_search_label_repo = cast(
        SavedSearchLabelRepository,
        get_repository("saved_search_label"),
    )
    unused_labels = saved_search_label_repo.get_unused_lables()
    if unused_labels:
        for label in unused_labels:
            label.delete()
        saved_search_label_repo.save()
