# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import CustomObjectTypeRepository
from minty.exceptions import NotFound, ValidationError
from minty.validation import validate_with
from pkgutil import get_data
from typing import cast


class CreateCustomObjectType(minty.cqrs.SplitCommandBase):
    name = "create_custom_object_type"

    @validate_with(
        get_data(__name__, "validation/create_custom_object_type.json")
    )
    def __call__(self, **kwargs):
        """
        Create an object type from the given json parameters.

        See validations json for more information about the possible parameters
        """

        # An optional catalog_folder_uuid can be given, to specify in which catalog
        # folder the new CustomObjectType will be stored. If not given or unknown it
        # will be stored in the root (catalog_folder_id will be None)

        repo = cast(
            CustomObjectTypeRepository,
            self.get_repository("custom_object_type"),
        )
        params = kwargs

        if "custom_field_definition" in params:
            repo.load_catalog_attribute_into_input(
                params["custom_field_definition"]
            )

        if "relationship_definition" in params:
            repo.load_catalog_relationship_into_input(
                params["relationship_definition"]
            )

        if "authorization_definition" in params:
            repo.load_authorization_into_input(
                params["authorization_definition"]
            )

        repo.create(**params)

        repo.save()


class UpdateCustomObjectType(minty.cqrs.SplitCommandBase):
    name = "update_custom_object_type"

    @validate_with(
        get_data(__name__, "validation/update_custom_object_type.json")
    )
    def __call__(self, **kwargs):
        """Updates an objecttype from the given json parameters

        See validations json for more information about the possible parameters
        """

        repo = cast(
            CustomObjectTypeRepository,
            self.get_repository("custom_object_type"),
        )
        objecttype = repo.find_by_uuid(uuid=kwargs["existing_uuid"])

        if not objecttype:
            raise NotFound(
                "Could not find 'custom_object_type' with uuid {}".format(
                    kwargs["existing_uuid"]
                )
            )

        params = kwargs
        del params["existing_uuid"]

        if "custom_field_definition" in params:
            repo.load_catalog_attribute_into_input(
                params["custom_field_definition"]
            )

        if "relationship_definition" in params:
            repo.load_catalog_relationship_into_input(
                params["relationship_definition"]
            )

        if "authorization_definition" in params:
            repo.load_authorization_into_input(
                params["authorization_definition"]
            )

        objecttype = objecttype.update(**kwargs)

        repo.save()


class DeleteCustomObjectType(minty.cqrs.SplitCommandBase):
    name = "delete_custom_object_type"

    @validate_with(
        get_data(__name__, "validation/delete_custom_object_type.json")
    )
    def __call__(self, **kwargs):
        """Updates an objecttype using the given uuid

        See validations json for more information about the possible parameters
        """

        repo = cast(
            CustomObjectTypeRepository,
            self.get_repository("custom_object_type"),
        )
        if "uuid" in kwargs:
            objecttype = repo.find_by_uuid(uuid=kwargs["uuid"])

            if not objecttype:
                raise NotFound(
                    f"Could not find 'custom_object_type_version' with uuid {kwargs['uuid']}"
                )
        elif "version_independent_uuid" in kwargs:
            objecttype = repo.find_by_version_independent_uuid(
                uuid=kwargs["version_independent_uuid"]
            )

            if not objecttype:
                raise NotFound(
                    f"Could not find 'custom_object_type' with uuid {kwargs['version_independent_uuid']}"
                )
        else:
            # pragma: no cover
            # This can't be covered because `validate_with` checks this; but
            # Python's type checker does not know this.

            raise ValidationError(
                "Neither <uuid> nor <version_independent_uuid> were specified."
            )

        objecttype.delete()

        repo.save()
