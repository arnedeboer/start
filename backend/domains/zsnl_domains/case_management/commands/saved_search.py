# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
from .. import repositories
from .._shared import delete_unused_saved_search_labels
from ..entities import saved_search as entity
from pydantic import Field, validate_arguments
from typing import Annotated, Optional, cast
from uuid import UUID


def _has_authorization(
    saved_search: entity.SavedSearch, authorization: entity.AuthorizationLevel
):
    if saved_search.entity_meta_authorizations and (
        authorization in saved_search.entity_meta_authorizations
    ):
        return True

    return False


class CreateSavedSearch(minty.cqrs.SplitCommandBase):
    name = "create_saved_search"

    @validate_arguments
    def __call__(
        self,
        uuid: UUID,
        name: str,
        kind: entity.SavedSearchKind,
        filters: Annotated[
            (
                entity.CustomObjectSearchFilterDefinition
                | entity.CaseSearchFilterDefinition
            ),
            Field(discriminator="kind_type"),
        ],
        permissions: list[entity.SavedSearchPermissionDefinition],
        columns: list[entity.SavedSearchColumnDefinition],
        sort_column: Optional[str] = None,
        sort_order: Optional[entity.SortOrder] = entity.SortOrder.desc,
    ):
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        repo.create(
            uuid=uuid,
            name=name,
            kind=kind,
            owner=self.cmd.user_info.user_uuid,
            filters=filters,
            permissions=permissions,
            columns=columns,
            sort_column=sort_column,
            sort_order=sort_order,
        )
        repo.save()


class DeleteSavedSearch(minty.cqrs.SplitCommandBase):
    name = "delete_saved_search"

    @validate_arguments
    def __call__(self, uuid: UUID):
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        saved_search = repo.get(
            uuid=uuid,
            user_info=self.cmd.user_info,
        )

        if not _has_authorization(
            saved_search, entity.AuthorizationLevel.admin
        ):
            raise minty.exceptions.Forbidden(
                "Current user is not authorised to delete this saved search",
                "saved_search/delete/not_authorized",
            )

        saved_search.delete()
        repo.save()

        delete_unused_saved_search_labels(get_repository=self.get_repository)


class UpdateSavedSearch(minty.cqrs.SplitCommandBase):
    name = "update_saved_search"

    @validate_arguments
    def __call__(
        self,
        uuid: UUID,
        name: str,
        filters: (
            entity.CustomObjectSearchFilterDefinition
            | entity.CaseSearchFilterDefinition
        ),
        columns: list[entity.SavedSearchColumnDefinition],
        sort_column: Optional[str] = None,
        sort_order: Optional[entity.SortOrder] = entity.SortOrder.desc,
        permissions: None
        | (list[entity.SavedSearchPermissionDefinition]) = None,
    ):
        repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )

        saved_search = repo.get(
            uuid=uuid,
            user_info=self.cmd.user_info,
        )

        if not _has_authorization(
            saved_search, entity.AuthorizationLevel.readwrite
        ):
            raise minty.exceptions.Forbidden(
                "Current user does not have write permission for this saved search",
                "saved_search/update/not_authorized",
            )

        saved_search.update(
            name=name,
            filters=filters,
            columns=columns,
            sort_column=sort_column,
            sort_order=sort_order,
            updated_by=self.cmd.user_info.user_uuid,
        )

        if permissions is not None:
            if not _has_authorization(
                saved_search, entity.AuthorizationLevel.admin
            ):
                raise minty.exceptions.Forbidden(
                    "Current user is not authorised to update permissions of this saved search",
                    "saved_search/update/permissions/not_authorized",
                )

            saved_search.update_permissions(permissions=permissions)

        repo.save()


class SetLabelsForSavedSearch(minty.cqrs.SplitCommandBase):
    name = "set_labels_for_saved_search"

    @validate_arguments
    def __call__(
        self, saved_search_uuids: list[UUID], saved_search_labels: list[str]
    ):
        saved_search_repo = cast(
            repositories.SavedSearchRepository,
            self.get_repository("saved_search"),
        )
        saved_search_label_repo = cast(
            repositories.SavedSearchLabelRepository,
            self.get_repository("saved_search_label"),
        )
        labels = []
        existing_labels = saved_search_label_repo.get_labels_by_name(
            saved_search_labels
        )
        labels = existing_labels
        existing_label_names = [label.name for label in existing_labels]
        for label in saved_search_labels:
            if label not in existing_label_names:
                labels.append(saved_search_label_repo.create(name=label))
        saved_search_label_repo.save()

        for uuid in saved_search_uuids:
            saved_search = saved_search_repo.get(
                uuid=uuid,
                user_info=self.cmd.user_info,
            )

            saved_search.set_labels(labels=labels)
        saved_search_repo.save()

        delete_unused_saved_search_labels(get_repository=self.get_repository)
