# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import minty.cqrs
import uuid
from ..entities import ContactType, Task, TaskAssigneeInputData
from ..repositories import CaseRepository, TaskRepository
from minty.exceptions import Conflict, Forbidden, NotFound
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import validate_arguments
from typing import cast
from uuid import UUID


class CreateTask(minty.cqrs.SplitCommandBase):
    name = "create_task"

    @validate_with(get_data(__name__, "validation/tasks/create_task.json"))
    def __call__(self, case_uuid: str, task_uuid: str, title: str, phase: int):
        """
        Create task related to case.

        :param case_uuid: uuid of case
        :param task_uuid: uuid of task
        :param title: task title
        :param phase: phase of which task falls under
        :raises Forbidden: No permission to edit case
        :raises Conflict: Task is not editable
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))
        case_repo = cast(CaseRepository, self.get_repository("case"))

        try:
            case = case_repo.find_case_by_uuid(
                case_uuid=UUID(case_uuid),
                user_info=self.cmd.user_info,
                permission="write",
            )
        except NotFound as e:
            raise Forbidden(
                "Not allowed to perform action on task for this case.",
                "case/task/not_allowed",
            ) from e
        task = task_repo.create_task(
            uuid=UUID(task_uuid), case=case, title=title, phase=phase
        )
        if task.is_editable and not task.completed:
            task_repo.save()
        else:
            raise Conflict(
                "Task not editable, can't complete action.",
                "task/action/not_editable",
            )


class DeleteTask(minty.cqrs.SplitCommandBase):
    name = "delete_task"

    @validate_with(get_data(__name__, "validation/tasks/delete_task.json"))
    def __call__(self, task_uuid: str):
        """
        Delete task.

        :param task_uuid: task uuid
        :raises Conflict: when not allowed to edit / delete.
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))

        task = task_repo.get_task(
            task_uuid=UUID(task_uuid),
            permission="write",
            user_info=self.cmd.user_info,
        )
        if task.is_editable and not task.completed:
            task.delete()
            task_repo.save()
        else:
            raise Conflict(
                "Task not editable, can't complete action.",
                "task/action/not_editable",
            )


class SetCompletionOnTask(minty.cqrs.SplitCommandBase):
    name = "set_completion_on_task"

    @validate_with(
        get_data(__name__, "validation/tasks/set_completion_on_task.json")
    )
    def __call__(self, task_uuid: str, completed: bool):
        """Set completion of task to True/False.

        :param task_uuid: uuid of task
        :param completed: completion of task
        :raises Conflict: when not allowed to edit
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))

        task = task_repo.get_task(
            task_uuid=UUID(task_uuid),
            permission="write",
            user_info=self.cmd.user_info,
        )
        if task.can_set_completion:
            task.set_completion(completed=completed)
            task_repo.save()
        else:
            raise Conflict(
                "Task not editable, can't complete action.",
                "task/action/not_editable",
            )
        task_repo.create_task_status_notification_for_case_assignee(
            task=task, user_uuid=self.cmd.user_info.user_uuid
        )


TITLE = "title"
DESCRIPTION = "description"
DUE_DATE = "due_date"
ASSIGNEE = "assignee"
PRODUCT_CODE = "product_code"
DSO_ACTION_REQUEST = "dso_action_request"


class UpdateTask(minty.cqrs.SplitCommandBase):
    name = "update_task"

    @validate_arguments
    def __call__(self, task_uuid: UUID, **kwargs):
        """
        Update/edit a task.

        :param task_uuid: task uuid
        :raises Conflict: when not allowed to edit
        """
        task_repo = cast(TaskRepository, self.get_repository("task"))

        task = task_repo.get_task(
            task_uuid=task_uuid,
            permission="write",
            user_info=self.cmd.user_info,
        )
        title: str = kwargs.get(TITLE) if TITLE in kwargs else task.title
        description: str | None = (
            kwargs.get(DESCRIPTION)
            if DESCRIPTION in kwargs
            else task.description
        )
        due_date: datetime.date | None = (
            kwargs.get(DUE_DATE) if DUE_DATE in kwargs else task.due_date
        )

        assignee: uuid.UUID | TaskAssigneeInputData | None = (
            kwargs.get(ASSIGNEE) if ASSIGNEE in kwargs else task.assignee
        )
        if isinstance(assignee, UUID):
            # Backwards compatibility
            assignee = TaskAssigneeInputData(
                type=ContactType.employee, id=assignee
            )
        elif isinstance(assignee, dict):
            assignee = TaskAssigneeInputData(
                type=assignee["type"], id=assignee["id"]
            )

        product_code: str | None = (
            kwargs.get(PRODUCT_CODE)
            if PRODUCT_CODE in kwargs
            else task.product_code
        )
        dso_action_request: bool = bool(
            kwargs.get(DSO_ACTION_REQUEST)
            if DSO_ACTION_REQUEST in kwargs
            else task.dso_action_request or False
        )

        if assignee and assignee.type == "employee":
            if not task.assignee or (assignee.id != task.assignee.id):
                task_repo.create_assigned_notification_for_task_assignee(
                    task=task,
                    user_uuid=self.cmd.user_info.user_uuid,
                    assignee_uuid=assignee.id,
                )

        if (
            assignee
            and assignee.type != "employee"
            and not task.can_assign_externally
        ):
            raise Conflict(
                "Tasks in cases of this case type can only be assigned to employees",
                "task/assignee/type_disallowed",
            )
        self._update_task(
            task=task,
            title=title,
            description=description,
            due_date=due_date,
            assignee=assignee,
            product_code=product_code,
            dso_action_request=dso_action_request,
        )
        task_repo.save()

    def _update_task(
        self,
        task: Task,
        title: str | None,
        description: str | None,
        due_date: datetime.date | None,
        assignee: TaskAssigneeInputData | None,
        product_code: str | None,
        dso_action_request: bool,
    ):
        if task.is_editable and not task.completed:
            task.update(
                title=title,
                description=description,
                due_date=due_date,
                assignee=assignee,
                product_code=product_code,
                dso_action_request=dso_action_request,
            )
        else:
            raise Conflict(
                "Task not editable, can't complete action.",
                "task/action/not_editable",
            )
