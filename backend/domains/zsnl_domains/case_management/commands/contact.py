# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import pydantic
from ...shared.repositories import ConfigurationRepository, SignatureUploadRole
from ..repositories import (
    EmployeeRepository,
    EmployeeSettingsRepository,
    OrganizationRepository,
    PersonRepository,
)
from minty.exceptions import Forbidden, ValidationError
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import Field, validate_arguments
from typing import Union, cast
from uuid import UUID
from zsnl_domains.case_management.entities._shared import (
    Address,
    ContactType,
    ValidGender,
)


class SetSubjectRelatedCustomObject(minty.cqrs.SplitCommandBase):
    name = "set_subject_related_custom_object"

    @validate_with(
        get_data(
            __name__,
            "validation/contact/set_subject_related_custom_object.json",
        )
    )
    def __call__(
        self,
        custom_object_uuid: str | None,
        person_uuid: str = None,
        organization_uuid: str = None,
        employee_uuid: str = None,
    ):
        if person_uuid:
            subject_repo = cast(
                PersonRepository, self.get_repository("person")
            )
            subject = subject_repo.find_person_by_uuid(uuid=UUID(person_uuid))
        elif organization_uuid:
            subject_repo = cast(
                OrganizationRepository, self.get_repository("organization")
            )
            subject = subject_repo.find_organization_by_uuid(
                uuid=UUID(organization_uuid)
            )
        elif employee_uuid:
            subject_repo = cast(
                EmployeeRepository, self.get_repository("employee")
            )
            subject = subject_repo.find_employee_by_uuid(
                uuid=UUID(employee_uuid)
            )
        else:  # pragma: no cover
            raise ValidationError()

        subject.set_related_custom_object(
            custom_object_uuid=UUID(custom_object_uuid)
            if custom_object_uuid
            else None
        )
        subject_repo.save()


class SetNotificationSettings(minty.cqrs.SplitCommandBase):
    name = "set_notification_settings"

    def __call__(self, uuid: UUID, notification_settings: dict):
        """Update notification_preference for an employee"""

        if not (
            self.cmd.user_info.permissions.get("admin", False)
            or self.cmd.user_info.user_uuid == uuid
        ):
            raise Forbidden(
                f"You don't have enough rights to set notification_settings for employee with uuid '{uuid}'",
                "contact/set_notification_settings/permission_denied",
            )

        repo = cast(
            EmployeeSettingsRepository,
            self.get_repository("employee_settings"),
        )
        contact_settings = repo.find_by_uuid(
            uuid=uuid, user_info=self.cmd.user_info
        )
        contact_settings.set_notification_settings(
            notification_settings=notification_settings
        )
        return repo.save()


class SignatureBase(minty.cqrs.SplitCommandBase):
    def assert_permission(self, employee_uuid: UUID):
        """Raise an exception unless the current user has rights to change
        their signature.
        """

        signature_upload_role = cast(
            ConfigurationRepository,
            self.get_repository("config"),
        ).get_signature_upload_role()

        if (
            signature_upload_role == SignatureUploadRole.user
            and employee_uuid == self.cmd.user_info.user_uuid
        ):
            return

        if self.cmd.user_info.permissions.get("owner_signatures", False):
            return

        raise Forbidden(
            f"You don't have enough rights to save signature for employee with uuid '{employee_uuid}'",
            "contact/save_signature/permission_denied",
        )


class SaveSignature(SignatureBase):
    name = "save_signature"

    @validate_with(
        get_data(__name__, "validation/contact/save_signature.json")
    )
    def __call__(self, uuid: str, file_uuid: str):
        """Save signature for an employee"""

        self.assert_permission(UUID(uuid))

        repo = cast(
            EmployeeSettingsRepository,
            self.get_repository("employee_settings"),
        )
        contact_settings = repo.find_by_uuid(
            uuid=UUID(uuid), user_info=self.cmd.user_info
        )

        contact_settings.save_signature(file_uuid=UUID(file_uuid))
        return repo.save(user_info=self.cmd.user_info)


class DeleteSignature(SignatureBase):
    name = "delete_signature"

    @validate_with(
        get_data(__name__, "validation/contact/delete_signature.json")
    )
    def __call__(self, uuid: str):
        """Delete signature for an employee"""

        self.assert_permission(UUID(uuid))
        repo = cast(
            EmployeeSettingsRepository,
            self.get_repository("employee_settings"),
        )
        contact_settings = repo.find_by_uuid(
            uuid=UUID(uuid), user_info=self.cmd.user_info
        )

        contact_settings.delete_signature()
        repo.save(user_info=self.cmd.user_info)


class ChangePhoneExtension(minty.cqrs.SplitCommandBase):
    name = "change_phone_extension"

    @validate_with(
        get_data(__name__, "validation/contact/change_phone_extension.json")
    )
    def __call__(self, uuid: UUID, extension: str | None):
        """Set a new phone extension for an employee"""

        if not (
            self.cmd.user_info.permissions.get("admin", False)
            or self.cmd.user_info.user_uuid == uuid
        ):
            raise Forbidden(
                f"You don't have enough rights to set phone extension for employee with uuid '{uuid}'",
                "contact/change_phone_extension/permission_denied",
            )

        repo = cast(
            EmployeeSettingsRepository,
            self.get_repository("employee_settings"),
        )
        contact_settings = repo.find_by_uuid(
            uuid=uuid, user_info=self.cmd.user_info
        )

        contact_settings.set_phone_extension(extension=extension)
        return repo.save(user_info=self.cmd.user_info)


class CreateLogforBSNRetrievedEvent(minty.cqrs.SplitCommandBase):
    name = "create_log_for_bsn_retrieved"

    @validate_with(
        get_data(
            __name__, "validation/contact/create_log_for_bsn_retrieved.json"
        )
    )
    def __call__(self, subject_uuid: str):
        """Create a log entry when BSN is retrieved"""

        subject_repo = cast(PersonRepository, self.get_repository("person"))
        if not self.cmd.user_info.permissions.get(
            "view_sensitive_contact_data", True
        ):
            raise Forbidden(
                "You don't have enough rights to log bsn retrieved entry",
                "contact/log_entry_for_bsn_retrieved/permission_denied",
            )
        subject = subject_repo.find_person_by_uuid(uuid=UUID(subject_uuid))
        subject.create_log_for_bsn_retrieved()
        subject_repo.save()


class SaveContactInformation(minty.cqrs.SplitCommandBase):
    name = "save_contact_information"

    @validate_with(
        get_data(
            __name__,
            "validation/contact/save_contact_information.json",
        )
    )
    def __call__(self, contact_information: dict, type: str, uuid: str):
        if type == "person":
            subject_repo = cast(
                PersonRepository, self.get_repository("person")
            )
            subject = subject_repo.find_person_by_uuid(uuid=UUID(uuid))
            user_info = self.cmd.user_info
            subject.save_contact_information(
                contact_information=contact_information, user_info=user_info
            )
        elif type == "organization":
            subject_repo = cast(
                OrganizationRepository, self.get_repository("organization")
            )
            subject = subject_repo.find_organization_by_uuid(uuid=UUID(uuid))
            subject.save_contact_information(
                contact_information=contact_information
            )
        else:  # pragma: no cover
            raise ValidationError()

        subject_repo.save()


class ContactInformationDict(dict):
    phone_number: str | None = Field(
        None, title="Phone number of the contact."
    )
    mobile_number: str | None = Field(
        None, title="Mobile number of the contact."
    )
    email: str | None = Field(None, title="Email id of the contact.")


class NonAuthenticatedPersonUpdateData(pydantic.BaseModel):
    first_name: str = Field(..., title="First name")
    family_name: str = Field(..., title="Family name")
    surname_prefix: str | None = Field(None, title="Surname Profex")
    noble_title: str | None = Field(None, title="Noble Title")
    gender: ValidGender | None = Field(None, title="gender")
    inside_municipality: bool | None = Field(
        False, title="Inside Municipality"
    )
    mailing_address: bool | None = Field(
        False, title="Is address mailing address too"
    )
    bsn: str | None = Field(None, title="BSN")
    sedula_number: str | None = Field(None, title="Sedula Number")


class NonAuthenticatedOrganizationData(pydantic.BaseModel):
    name: str | None = Field(None, title="Name")
    coc_number: str | None = Field(None, title="COC Number")
    coc_location_number: int | None = Field(None, title="COC location number")
    organization_type: str | None = Field("", title="Organization Type")
    contact_first_name: str | None = Field(None, title="Contact name")
    contact_last_name: str | None = Field(None, title="Contact last name")
    contact_title: str | None = Field(None, title="Contact title")
    contact_insertions: str | None = Field(None, title="Contact insertions")


NonAuthenticatedUpdateData = Union[
    NonAuthenticatedPersonUpdateData, NonAuthenticatedOrganizationData
]


class UpdateNonAuthenticContact(minty.cqrs.SplitCommandBase):
    name = "update_non_authentic_contact"

    @validate_arguments
    def __call__(
        self,
        uuid: UUID,
        type: ContactType,
        data: NonAuthenticatedUpdateData,
        address: Address | None = None,
        correspondence_address: Address | None = None,
        contact_information: ContactInformationDict | None = None,
    ):
        if not (
            self.cmd.user_info.permissions.get("contact_nieuw", False)
            or self.cmd.user_info.permissions.get("contact_search", False)
        ):
            raise Forbidden(
                f"You don't have enough rights to update contact with uuid '{uuid}'",
                "contact/update_non_authentic/permission_denied",
            )

        if type == "person":
            if (data.bsn or data.sedula_number) and not (
                self.cmd.user_info.permissions.get(
                    "view_sensitive_contact_data", False
                )
                or self.cmd.user_info.permissions.get("admin", False)
            ):
                raise Forbidden(
                    "User does not have enough rights to update Sensitive Data",
                    "contact/update/no_sensitive_data_permission",
                )

            subject_repo = cast(
                PersonRepository, self.get_repository("person")
            )
            subject = subject_repo.find_person_by_uuid(uuid=uuid)
            if contact_information:
                subject.save_contact_information(
                    contact_information=contact_information,
                    user_info=self.cmd.user_info,
                )
            if not address:
                address = subject.residence_address
            subject.update_non_authentic(
                first_name=data.first_name,
                family_name=data.family_name,
                surname_prefix=data.surname_prefix,
                noble_title=data.noble_title,
                gender=data.gender,
                inside_municipality=data.inside_municipality,
                address=address,
                correspondence_address=correspondence_address,
            )

            if data.bsn:
                subject.update_bsn_non_authentic(bsn=data.bsn)

            if data.sedula_number:
                subject.update_sedula_number_non_authentic(
                    sedula_number=data.sedula_number
                )

        elif type == "organization":
            subject_repo = cast(
                OrganizationRepository, self.get_repository("organization")
            )
            subject = subject_repo.find_organization_by_uuid(uuid=uuid)
            if contact_information:
                subject.save_contact_information(
                    contact_information=contact_information
                )
            subject.update_non_authentic(
                update_data=data,
                address=address,
                correspondence_address=correspondence_address,
            )
        else:  # pragma: no cover : method argument validation does not allow other values
            raise ValidationError()

        subject_repo.save()
