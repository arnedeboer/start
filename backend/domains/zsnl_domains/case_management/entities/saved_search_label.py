# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from minty import entity
from pydantic import Field
from uuid import UUID


class SavedSearchLabel(entity.Entity):
    entity_type = "saved_seach_label"
    entity_id__fields = ["uuid"]

    uuid: UUID | None = Field(..., title="UUID of this saved search label")
    name: str = Field(..., title="Name of this saved search label")

    @classmethod
    @entity.Entity.event("SavedSearchLabelCreated", fire_always=True)
    def create(
        cls,
        name,
        uuid: UUID,
        event_service: minty.cqrs.EventService,
    ):
        return cls.parse_obj(
            {
                "entity_id": uuid,
                "name": name,
                "uuid": uuid,
                "_event_service": event_service,
            }
        )

    @entity.Entity.event(name="SavedSearchLabelDeleted", fire_always=True)
    def delete(self):
        pass
