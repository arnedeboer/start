# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from ._shared import ContactInformation, RelatedCustomObject
from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class ValidEmployeeStatus(enum.StrEnum):
    active = "active"
    inactive = "inactive"


class RelatedDepartment(Entity):
    entity_type = "department"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the department")


class RelatedRole(Entity):
    entity_type = "role"
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the role")
    entity_relationships = ["parent"]
    parent: RelatedDepartment = Field(..., title="Parent department")


class Employee(Entity):
    """Content of a user defined Custom Object"""

    entity_type = "employee"
    entity_id__fields = ["uuid"]

    status: ValidEmployeeStatus = Field(..., title="Status of the employee.")
    source: str | None = Field(None, title="Source of the employee")
    uuid: UUID = Field(..., title="Internal identifier of the employee")

    first_name: str | None = Field(None, title="First names of employee")

    surname: str = Field("", title="Surname of employee")
    name: str = Field(..., title="Name of the person")
    title: str | None = Field(None, title="Title of person")

    department: RelatedDepartment = Field(..., title="Department of employee")
    roles: list[RelatedRole] = Field(..., title="Roles of employee")

    contact_information: ContactInformation | None = Field(
        None, title="Contact info of the person"
    )

    entity_relationships = ["related_custom_object", "roles", "department"]
    related_custom_object: RelatedCustomObject | None = Field(
        None, title="Custom object related to this person"
    )

    has_valid_address: bool = Field(
        False,
        title="True if the contact has a valid address. Always false for employees.",
    )
    is_active: bool = Field(True, title="User is active/inactive")

    @Entity.event(name="RelatedCustomObjectSet", fire_always=True)
    def set_related_custom_object(self, custom_object_uuid: UUID | None):
        self.related_custom_object = (
            None
            if custom_object_uuid is None
            else RelatedCustomObject(
                entity_id=custom_object_uuid,
                uuid=custom_object_uuid,
            )
        )


class EmployeeLimited(Entity):
    entity_type = "employee"
    entity_id__fields = ["uuid"]
    uuid: UUID = Field(..., title="Internal identifier of the employee")
