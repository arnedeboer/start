# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import TimelineEntry
from minty.entity import Entity, ValueObject
from pydantic import Field
from uuid import UUID


class OutputUser(ValueObject):
    user_uuid: UUID = Field(..., title="UUID of output user")
    permissions: dict = Field(..., title="Permssions of output user")


class TimelineExport(Entity):
    """Entity represents timeline export"""

    entity_type = "timeline_export"
    entity_id__fields = ["uuid"]

    uuid: UUID | None = Field(
        None, title="UUID of the object which you want to export timeline"
    )
    type: str | None = Field(
        None, title="Type of the the object which you want to export timeline"
    )
    entries: list[TimelineEntry] | None = Field(
        None, title="List of timeline entries you want to import"
    )
    upload_result: dict | None = Field(
        None, title="Dictionary containing details of upload result"
    )
    filestore_uuid: UUID | None = Field(None, title="UUID of stored file")

    period_start: str | None = Field(
        None, title="Start time filter for the timeline"
    )
    period_end: str | None = Field(
        None, title="End time filter for the timeline"
    )

    upload_result: dict | None = Field(
        None, title="Dictionary containing details of upload result"
    )
    filestore_uuid: UUID | None = Field(None, title="UUID of stored file")

    @classmethod
    @Entity.event(name="TimelineExportRequested", fire_always=True)
    def request(cls, **kwargs):
        return cls(**kwargs)

    @classmethod
    @Entity.event(name="TimelineExportCreated", fire_always=True)
    def create(cls, **kwargs):
        return cls(**kwargs)
