# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field, conint


class TotalResultCount(Entity):
    """Count of results of query"""

    entity_type = "result_count"

    total_results: conint(ge=0) = Field(..., title="Total number of results")
