# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field


class AttributeSearch(Entity):
    """Content of a attribute"""

    entity_type = "attribute"

    label: str | None = Field(..., title="Label for the attribute")
    magic_string: str | None = Field(
        ..., title="Magic string for the attribute"
    )
    description: str | None = Field(..., title="Description for the attribute")
    type: str | None = Field(..., title="Type for the attribute")
