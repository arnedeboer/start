# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from minty.entity import Entity
from pydantic import Field, validator
from uuid import UUID


class PersonSensitiveData(Entity):
    """Contains sensitive information about person"""

    entity_type = "person_sensitive_data"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Internal identifier of the person")
    personal_number: str | None = Field(
        None,
        title="Burger service number of the person",
    )
    sedula_number: str | None = Field(
        None,
        title="Sedula number of the person",
    )

    @validator("personal_number")
    def validate_personal_number(cls, v):
        if v is not None:
            v = v.zfill(9)
        return v


class PersonSensitiveDataType(enum.StrEnum):
    personal_number = "personal_number"
    sedula_number = "sedula_number"
