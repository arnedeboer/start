# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from . import _shared
from datetime import date
from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class CaseSearchResultPerson(_shared.RelatedPerson):
    entity_id__fields = ["uuid"]

    type: str = Field(..., title="Type of the requestor of the case")
    name: str = Field(..., title="Name of the requestor of the case")


class CaseSearchResultOrganization(_shared.RelatedOrganization):
    entity_id__fields = ["uuid"]

    type: str = Field(..., title="Type of the requestor of the case")
    name: str = Field(..., title="Name of the requestor of the case")


class CaseSearchResultEmployee(_shared.RelatedEmployee):
    entity_id__fields = ["uuid"]
    entity_type: str = Field("employee", const=True)

    type: str = Field(..., title="Type of the requestor of the case")
    name: str = Field(..., title="Name of the requestor of the case")


class CaseSearchOrder(enum.StrEnum):
    number_asc = "number"
    unread_message_count_asc = "unread_message_count"
    unaccepted_files_count_asc = "unaccepted_files_count"
    unaccepted_attribute_update_count_asc = "unaccepted_attribute_update_count"
    registration_date_asc = "registration_date"
    completion_date_asc = "completion_date"

    number_desc = "-number"
    unread_message_count_desc = "-unread_message_count"
    unaccepted_files_count_desc = "-unaccepted_files_count"
    unaccepted_attribute_update_count_desc = (
        "-unaccepted_attribute_update_count"
    )
    registration_date_desc = "-registration_date"
    completion_date_desc = "-completion_date"


class CaseSearchResult(Entity):
    entity_type = "case_search_result"
    entity_id__fields = ["uuid"]
    entity_relationships = ["assignee", "requestor", "coordinator"]

    uuid: UUID = Field(..., title="Uuid of the case")

    number: int = Field(..., title="Id of case")
    status: str = Field(..., title="Status of case")
    destruction_date: date | None = Field(
        None, title="Destruction date of the case"
    )
    archival_state: str | None = Field(
        None, title="Archival state of the case"
    )
    subject: str | None = Field(None, title="Subject of te case")

    case_type_title: str | None = Field(None, title="Title of the case_type")

    requestor: (
        CaseSearchResultPerson
        | CaseSearchResultOrganization
        | CaseSearchResultEmployee
    ) = Field(..., title="Requestor of the case")
    coordinator: CaseSearchResultEmployee | None = Field(
        ..., title="Coordinator of the case"
    )
    assignee: CaseSearchResultEmployee | None = Field(
        ..., title="Assignee of the case"
    )

    percentage_days_left: int | None = Field(
        None,
        title="Percentage of remaining days until completion.",
        description="Indicates with a percentage how much time is left to "
        + "complete the case. The percentage will be above 100 if the "
        + "remaining days are exeeded. When a case has the status stalled, "
        + "then the value is null",
    )

    progress: int = Field(..., title="Progress status of the case")

    unread_message_count: int = Field(..., title="Number of unread messages")
    unaccepted_files_count: int = Field(..., title="Count of unaccepted files")
    unaccepted_attribute_update_count: int = Field(
        ..., title="Count of unaccepted updates"
    )

    registration_date: date | None = Field(
        None, title="Registration date of the case"
    )

    completion_date: date | None = Field(
        None, title="Completion date of the case"
    )
    custom_fields: dict = Field(..., title="custom fields for the case")
    target_completion_date: date | None = Field(
        None, title="Target completion date for the case"
    )
