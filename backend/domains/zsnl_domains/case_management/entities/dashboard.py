# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .saved_search import SavedSearchKind
from minty import entity
from pydantic import Field, conint, conlist
from typing import Annotated, Literal
from uuid import UUID


class WidgetDimensions(entity.ValueObject):
    x: conint(ge=0) = Field(..., title="x coordinate for widget")
    y: conint(ge=0) = Field(..., title="y coordinate for widget")
    width: conint(ge=1) = Field(..., title="width of the widget")
    height: conint(ge=1) = Field(..., title="height of the widget")


class SavedSearchWidgetParameters(entity.ValueObject):
    "Widget parameters for saved searches"
    type: Literal["saved_search"]
    saved_search_id: UUID = Field(..., title="Identifier for the saved search")
    page_size: conint(ge=1) = Field(
        ..., title="Page size for the saved search"
    )
    kind: SavedSearchKind = Field(
        ..., title="The kind of entity this saved search searches for"
    )


class FavoriteCaseTypeWidgetParameters(entity.ValueObject):
    "Widget parameters for favorite case type"
    type: Literal["favorite_case_type"]


class TaskColumn(entity.ValueObject):
    id: str = Field(..., title="display number for task column")
    value: str = Field(..., title="case number for task column")
    active: bool = Field(
        ..., title="Identification of active status for task column"
    )
    is_new: bool = Field(..., title="case number for task column")


class TaskFilterData(entity.ValueObject):
    type: str = Field(..., title="Type for task filter data")


class TaskFilter(entity.ValueObject):
    label: str = Field(..., title="Label for task filter")
    value: UUID = Field(..., title="Value for task filter")
    data: TaskFilterData = Field(..., title="Data for task filter")


class TasksWidgetParameters(entity.ValueObject):
    "Widget parameters for tasks"
    type: Literal["tasks"]
    columns: list[TaskColumn] = Field(..., title="Columns for task widget")
    filters: list[TaskFilter] = Field(..., title="Filters for task widget")


class ExternalUrlParameters(entity.ValueObject):
    "Widget parameters for external url"
    type: Literal["external_url"]
    url: str = Field(..., title="url parameter")
    title: str = Field(..., title="title for external url parameter")


class Widget(entity.ValueObject):
    "Widget definition for the dashboard"

    dimensions: WidgetDimensions = Field(
        ..., title="Dimensions for the widget"
    )
    parameters: Annotated[
        (
            SavedSearchWidgetParameters
            | FavoriteCaseTypeWidgetParameters
            | FavoriteCaseTypeWidgetParameters
            | TasksWidgetParameters
            | ExternalUrlParameters
        ),
        Field(discriminator="type"),
    ]

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class Dashboard(entity.Entity):
    """
    Entity represents contents of dashboard screen.
    """

    entity_type = "dashboard"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier for the dashboard")
    widgets: conlist(Widget, max_items=10) | None = Field(
        ..., title="List of widgets for the dashboard"
    )

    @entity.Entity.event(name="WidgetsUpdated", fire_always=True)
    def set_widgets(
        self,
        uuid: UUID,
        widgets: conlist(Widget) | None = None,
    ):
        self.uuid = uuid
        self.widgets = widgets
