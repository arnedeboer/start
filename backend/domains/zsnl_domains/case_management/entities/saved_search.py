# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import enum
import minty.cqrs
from ...shared.custom_field import CustomFieldTypes
from ...shared.types import FilterOperator
from ._shared import AuthorizationLevel, ContactType
from datetime import date, datetime, timezone
from minty import entity
from pydantic import Field, conint, constr
from pydantic.generics import GenericModel
from typing import Annotated, Generic, Literal, Optional, TypeVar
from uuid import UUID
from zsnl_domains.case_management.entities.custom_object import (
    ValidArchiveStatus,
    ValidObjectStatus,
)
from zsnl_domains.case_management.entities.saved_search_label import (
    SavedSearchLabel,
)
from zsnl_domains.shared import types
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidCaseUrgency,
    ValidContactChannel,
)

ISO8601_PERIOD_REGEX = (
    r"^([?+-])?"
    r"P(?!\b)"
    r"([?0-9]+([,.][0-9]+)?Y)s?"
    r"([?0-9]+([,.][0-9]+)?M)?"
    r"([?0-9]+([,.][0-9]+)?W)?"
    r"([?0-9]+([,.][0-9]+)?D)?"
    r"((T)([?0-9]+([,.][0-9]+)?H)?"
    r"([?0-9]+([,.][0-9]+)?M)?"
    r"([?0-9]+([,.][0-9]+)?S)?)?$"
)

SimpleFilterType = TypeVar("SimpleFilterType")
MinZeroInteger = conint(ge=0)


class SimpleFilterObject(GenericModel, Generic[SimpleFilterType]):
    label: str | None = Field(
        None, title="Human-readable preview value of the filter value"
    )
    value: SimpleFilterType = Field(..., title="Value to filter for")

    class Config:
        @staticmethod
        def schema_extra(schema):
            for prop in schema.get("properties", {}).values():
                if (
                    "type" in prop
                    and prop["type"] == "array"
                    and "items" in schema["properties"]["value"]["items"]
                ):
                    updated_array = {
                        "oneOf": schema["properties"]["value"]["items"][
                            "items"
                        ]
                    }
                    schema["properties"]["value"]["items"][
                        "items"
                    ] = updated_array


CustomISODuration = constr(regex=ISO8601_PERIOD_REGEX)


class SimpleFilterObjectValuesMultiple(
    GenericModel, Generic[SimpleFilterType]
):
    label: str | None = Field(
        None, title="Human-readable preview value of the filter value"
    )
    value: list[SimpleFilterType] = Field(..., title="Value to filter for")


class AbsoluteDate(entity.ValueObject):
    type: Literal["absolute"]
    value: datetime | None
    operator: types.ComparisonFilterOperator


class RelativeDate(entity.ValueObject):
    type: Literal["relative"]
    value: CustomISODuration
    operator: types.ComparisonFilterOperator


class RangeDate(entity.ValueObject):
    type: Literal["range"]
    start_value: datetime
    end_value: datetime
    time_set_by_user: bool


class AttributeValue(entity.ValueObject):
    type: CustomFieldTypes | None
    value: str
    operator: types.ComparisonFilterOperator | None
    magic_string: str


class CustomObjectTypeFilter(entity.ValueObject):
    type: Literal["relationship.custom_object_type"]
    parameters: SimpleFilterObject[UUID]


class CustomObjectStatusFilter(entity.ValueObject):
    type: Literal["attributes.status"]
    parameters: SimpleFilterObject[ValidObjectStatus]


class CustomObjectArchiveStatusFilter(entity.ValueObject):
    type: Literal["attributes.archive_status"]
    parameters: SimpleFilterObject[ValidArchiveStatus]


class customFieldParameters(entity.ValueObject):
    operator: str | None
    magic_string: str
    type: str
    value: str


class ContactParameters(entity.ValueObject):
    label: str
    value: UUID
    type: ContactType


class CustomFieldFilter(entity.ValueObject):
    type: Literal["custom_field"]
    parameters: customFieldParameters


class CustomObjectLastModifiedFilter(entity.ValueObject):
    type: Literal["attributes.last_modified"]
    parameters: SimpleFilterObject[
        list[
            Annotated[
                (AbsoluteDate | RangeDate | RelativeDate),
                Field(discriminator="type"),
            ],
        ]
    ]


class CustomObjectAttributeValueFilter(entity.ValueObject):
    type: Literal["attributes.value"]
    parameters: SimpleFilterObject[AttributeValue]


class CaseTypeUuidFilter(entity.ValueObject):
    type: Literal["relationship.case_type.id"]
    parameters: list[SimpleFilterObject[UUID]]
    operator = types.FilterOperator.or_operator


class CaseStatusFilter(entity.ValueObject):
    type: Literal["attributes.status"]
    parameters: SimpleFilterObject[ValidCaseStatus]


class CaseAssigneeUuidsFilter(entity.ValueObject):
    type: Literal["relationship.assignee.id"]
    parameters: list[ContactParameters]


class CaseCoordinatorUuidsFilter(entity.ValueObject):
    type: Literal["relationship.coordinator.id"]
    parameters: list[ContactParameters]


class CaseRequestorUuidsFilter(entity.ValueObject):
    type: Literal["relationship.requestor.id"]
    parameters: list[ContactParameters]


class CaseRegistrationDateFilter(entity.ValueObject):
    type: Literal["attributes.registration_date"]
    parameters: SimpleFilterObject[
        list[
            Annotated[
                (AbsoluteDate | RangeDate | RelativeDate),
                Field(discriminator="type"),
            ],
        ]
    ]


class CaseCompletionDateFilter(entity.ValueObject):
    type: Literal["attributes.completion_date"]
    parameters: SimpleFilterObject[
        list[
            Annotated[
                (AbsoluteDate | RangeDate | RelativeDate),
                Field(discriminator="type"),
            ],
        ]
    ]


class CasePaymentStatusFilter(entity.ValueObject):
    type: Literal["attributes.payment_status"]
    parameters: list[ValidCasePaymentStatus]
    operator = types.FilterOperator.or_operator


class CaseChannelOfContactFilter(entity.ValueObject):
    type: Literal["attributes.channel_of_contact"]
    parameters: SimpleFilterObject[list[ValidContactChannel]]
    operator = types.FilterOperator.or_operator


class CaseConfidentialityFilter(entity.ValueObject):
    type: Literal["attributes.confidentiality"]
    parameters: SimpleFilterObject[list[ValidCaseConfidentiality]]
    operator = types.FilterOperator.or_operator


class CaseArchivalStateFilter(entity.ValueObject):
    type: Literal["attributes.archival_state"]
    parameters: SimpleFilterObject[ValidCaseArchivalState]


class CaseRetentionPeriodSourceDateFilter(entity.ValueObject):
    type: Literal["attributes.retention_period_source_date"]
    parameters: SimpleFilterObject[ValidCaseRetentionPeriodSourceDate]


class CaseResultFilter(entity.ValueObject):
    type: Literal["attributes.result"]
    parameters: SimpleFilterObject[list[ValidCaseResult]]
    operator = types.FilterOperator.or_operator


class CaseNumUnreadMessagesFilter(entity.ValueObject):
    type: Literal["attributes.num_unread_messages"]
    value: SimpleFilterObject[
        list[tuple[types.ComparisonFilterOperator, MinZeroInteger]]
    ]


class CaseNumUnacceptedFilesFilter(entity.ValueObject):
    type: Literal["attributes.num_unaccepted_files"]
    value: SimpleFilterObject[
        list[tuple[types.ComparisonFilterOperator, MinZeroInteger]]
    ]


class CaseNumUnacceptedUpdatesFilter(entity.ValueObject):
    type: Literal["attributes.num_unaccepted_updates"]
    value: SimpleFilterObject[
        list[tuple[types.ComparisonFilterOperator, MinZeroInteger]]
    ]


class CasePeriodOfPreservationActiveFilter(entity.ValueObject):
    type: Literal["attributes.period_of_preservation_active"]
    value: SimpleFilterObject[str]


class CaseSubjectFilter(entity.ValueObject):
    type: Literal["attributes.subject"]
    value: SimpleFilterObject[str]


class KeywordFilter(entity.ValueObject):
    type: Literal["keyword"]
    parameters: list[SimpleFilterObject[str | None]]
    operator: types.FilterOperator | None


class TitleFilter(entity.ValueObject):
    type: Literal["attributes.title"]
    parameters: SimpleFilterObject[str | None]


class SubTitleFilter(entity.ValueObject):
    type: Literal["attributes.subtitle"]
    parameters: SimpleFilterObject[str | None]


class ExternalReferenceFilter(entity.ValueObject):
    type: Literal["attributes.external_reference"]
    parameters: SimpleFilterObject[str | None]


class CaseUrgencyFilter(entity.ValueObject):
    type: Literal["attributes.urgency"]
    parameters: SimpleFilterObject[ValidCaseUrgency]


class CaseDepartmentRoleFilter(entity.ValueObject):
    class DepartmentRoleValues(entity.ValueObject):
        department: UUID | None
        role: UUID | None

    type: Literal["attributes.department_role"]
    parameters: SimpleFilterObjectValuesMultiple[DepartmentRoleValues]
    operator = types.FilterOperator.or_operator


class CaseAddressFilter(entity.ValueObject):
    class AddressFilterValues(entity.ValueObject):
        label: str | None
        value: str
        type: str
        id: str

    type: Literal["attributes.case_location"]
    operator: types.FilterOperator | None
    parameters: SimpleFilterObjectValuesMultiple[AddressFilterValues]


class CustomObjectSearchFilterDefinition(entity.ValueObject):
    kind_type: Literal["custom_object"]
    operator: FilterOperator | None
    filters: list[
        Annotated[
            (
                KeywordFilter
                | CustomFieldFilter
                | CustomObjectTypeFilter
                | CustomObjectStatusFilter
                | None
                | CustomObjectArchiveStatusFilter
                | None
                | CustomObjectLastModifiedFilter
                | None
                | TitleFilter
                | None
                | SubTitleFilter
                | None
                | ExternalReferenceFilter
                | None
                | CustomObjectAttributeValueFilter
                | None
            ),
            Field(discriminator="type"),
        ]
    ]

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class CaseSearchFilterDefinition(entity.ValueObject):
    kind_type: Literal["case"]
    custom_fields_operator: Literal["and"] | Literal["or"] | None
    operator: FilterOperator | None
    filters: list[
        Annotated[
            (
                KeywordFilter
                | CustomFieldFilter
                | CaseTypeUuidFilter
                | CaseStatusFilter
                | CaseAssigneeUuidsFilter
                | CaseCoordinatorUuidsFilter
                | CaseRequestorUuidsFilter
                | CaseRegistrationDateFilter
                | CaseCompletionDateFilter
                | CasePaymentStatusFilter
                | CaseChannelOfContactFilter
                | CaseConfidentialityFilter
                | CaseArchivalStateFilter
                | CaseRetentionPeriodSourceDateFilter
                | CaseResultFilter
                | CaseNumUnreadMessagesFilter
                | CaseNumUnacceptedFilesFilter
                | CaseNumUnacceptedUpdatesFilter
                | CasePeriodOfPreservationActiveFilter
                | CaseSubjectFilter
                | CustomObjectAttributeValueFilter
                | None
                | CaseUrgencyFilter
                | CaseDepartmentRoleFilter
                | CaseAddressFilter
            ),
            Field(discriminator="type"),
        ]
    ]

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class SavedSearchPermission(enum.StrEnum):
    """Permissions a user can give to a group/role on a saved search."""

    read = "read"
    write = "write"


class SortOrder(enum.StrEnum):
    asc = "asc"
    desc = "desc"


class SavedSearchKind(enum.StrEnum):
    case = "case"
    custom_object = "custom_object"


class SavedSearchPermissionDefinition(entity.ValueObject):
    group_id: UUID = Field(
        ..., title="Identifier of the group this permission applies to"
    )
    role_id: UUID = Field(
        ..., title="Identifier of the role this permission applies to"
    )
    permission: set[SavedSearchPermission] = Field(
        ...,
        title="Permissions the specified group + role has for this saved search",
    )


class SavedSearchColumnDefinition(entity.ValueObject):
    source: list[str] = Field(
        ...,
        title="Path to retrieve the field from the case or custom_object entity",
    )
    label: str = Field(..., title="Human-readable name for this column")
    type: str = Field(
        ...,
        title="Type of the field; can be used to determine how to show the 'raw' value",
    )
    magic_string: str | None = Field(
        None, title="Magic string for this column"
    )
    visible: bool = Field(True, title="Decide visibility of column")

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class SavedSearchOwner(entity.Entity):
    entity_type = "employee"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class SavedSearchUpdatedBy(entity.Entity):
    entity_type = "employee"
    entity_id__fields = ["uuid"]

    uuid: UUID | None = Field(None, title="Identifier of the last updater")
    summary: str | None = Field(None, title="Summary of last updater")


class SavedSearch(entity.Entity):
    """
    Represents a saved state of the "advanced search" screen.

    Users can create (and save) multiple search queries, for quick access
    while working with the system.
    """

    entity_type = "saved_search"
    entity_id__fields = ["uuid"]
    entity_relationships = ["owner"]
    entity_meta__fields = ["entity_meta_summary", "entity_meta_authorizations"]

    entity_meta_authorizations: set[AuthorizationLevel] | None = Field(
        None,
        title="The set of rights/authorizations the current user has for the saved search",
    )

    uuid: UUID = Field(..., title="Identifier for this saved search")
    name: str = Field(..., title="Unique name of this search")

    owner: SavedSearchOwner = Field(..., title="Owner of this saved search")

    kind: SavedSearchKind = Field(
        ..., title="The kind of entity this saved search searches for"
    )

    filters: Annotated[
        (CustomObjectSearchFilterDefinition | CaseSearchFilterDefinition),
        Field(discriminator="kind_type"),
    ]
    permissions: list[SavedSearchPermissionDefinition] = Field(
        ..., title="Group/role/permission"
    )

    columns: list[SavedSearchColumnDefinition] = Field(
        ..., title="Columns visible when using this search"
    )
    sort_column: Optional[str] = Field(
        None, title="Column to sort the result on, when using this search"
    )
    sort_order: Optional[SortOrder] = Field(
        SortOrder.desc,
        title="Order for sorting results (ascending or descending)",
    )
    date_deleted: date | None = Field(
        None, title="Date the saved search is deleted"
    )
    labels: list[SavedSearchLabel] = Field(
        ..., title="Labels for saved search"
    )

    date_created: datetime | None = Field(
        None, title="Date the saved search is created"
    )

    date_updated: datetime | None = Field(
        None, title="Date the saved search is last updated"
    )

    updated_by: SavedSearchUpdatedBy | None = Field(
        None, title="Updated last of this saved search"
    )

    @classmethod
    @entity.Entity.event(name="SavedSearchCreated", fire_always=True)
    def create(
        cls,
        uuid: UUID,
        name: str,
        kind: SavedSearchKind,
        owner: SavedSearchOwner,
        filters: (
            CustomObjectSearchFilterDefinition | CaseSearchFilterDefinition
        ),
        permissions: list[SavedSearchPermissionDefinition],
        columns: list[SavedSearchColumnDefinition],
        sort_column: str | None,
        sort_order: SortOrder,
        event_service: minty.cqrs.EventService,
    ):
        return cls.parse_obj(
            {
                "entity_id": uuid,
                "uuid": uuid,
                "name": name,
                "kind": kind,
                "owner": owner,
                "filters": filters,
                "permissions": permissions,
                "columns": columns,
                "labels": [],
                "sort_column": sort_column,
                "sort_order": sort_order,
                "_event_service": event_service,
                "date_created": datetime.now(timezone.utc),
            }
        )

    @entity.Entity.event(name="SavedSearchDeleted", fire_always=True)
    def delete(self):
        self.date_deleted = date.today()
        self.labels = []

    @entity.Entity.event(name="SavedSearchUpdated", fire_always=True)
    def update(
        self,
        name: str,
        filters: (
            CustomObjectSearchFilterDefinition | CaseSearchFilterDefinition
        ),
        columns: list[SavedSearchColumnDefinition],
        sort_column: str | None,
        sort_order: SortOrder,
        updated_by: UUID,
    ):
        self.name = name
        self.filters = filters
        self.columns = columns
        self.sort_column = sort_column
        self.sort_order = sort_order
        self.date_updated = datetime.now(timezone.utc)
        self.updated_by = {"uuid": updated_by}

    @entity.Entity.event(
        name="SavedSearchPermissionsUpdated", fire_always=True
    )
    def update_permissions(
        self, permissions: list[SavedSearchPermissionDefinition]
    ):
        self.permissions = permissions

    @entity.Entity.event("SavedSearchLabelsApplied", fire_always=True)
    def set_labels(self, labels: list[SavedSearchLabel]):
        self.labels = labels
