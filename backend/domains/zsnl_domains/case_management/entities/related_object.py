# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class RelatedObjectObjectType(Entity):
    """custom_object_type for the related_custom_object"""

    entity_type = "persistent_object_type"
    entity_id__fields = ["uuid"]

    uuid: UUID | None = Field(
        None, title="Internal identifier of custom_object_type"
    )


class RelatedObject(Entity):
    """Content of related_custom_object"""

    entity_type = "related_object"
    entity_relationships = ["object_type"]
    entity_id__fields = ["uuid"]

    # Properties
    uuid: UUID = Field(..., title="Internal identifier of the custom_object")

    # Relationship
    object_type: RelatedObjectObjectType | None = Field(
        ..., title="object_type of related_custom_object"
    )
