# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class Requestor(Entity):
    """Requestor Entity"""

    entity_type = "requestor"
    entity_id__fields = ["uuid"]

    id: str | None = Field(None, title="Id of the requestor")
    uuid: UUID | None = Field(None, title="Internal identifier of requestor")
    requestor_type: str | None = Field(None, title="Type of the requestor")
    name: str | None = Field(None, title="Name of the requestor")
    surname: str | None = Field(None, title="Surname of the requestor")
    surname_prefix: str | None = Field(
        None, title="Surname prefix of the requestor"
    )
    family_name: str | None = Field(None, title="Family name of the requestor")
    full_name: str | None = Field(None, title="Full name of the requestor")
    first_names: str | None = Field(
        None, title="First names for the requestor"
    )
    initials: str | None = Field(None, title="Initials for the requestor")
    used_name: str | None = Field(None, title="Identifier of case type")
    burgerservicenummer: str | None = Field(
        None, title="BSN number for the reqestor"
    )
    noble_title: str | None = Field(None, title="Title of the requestor")
    gender: str | None = Field(None, title="Gender of the requestor")
    date_of_birth: datetime | None = Field(
        None, title="Requestor's date of birth"
    )
    date_of_divorce: datetime | None = Field(
        None, title="Date of divorce for requestor"
    )
    place_of_birth: str | None = Field(
        None, title="Requestor's place of birth"
    )
    country_of_birth: str | None = Field(
        None, title="Requestor's country of birth"
    )
    salutation: str | None = Field(None, title="Salutation used for requestor")
    salutation1: str | None = Field(
        None, title="Another Salutation used for requestor"
    )
    salutation2: str | None = Field(
        None, title="Another Salutation used for requestor"
    )
    unique_name: str | None = Field(
        None, title="Unique name for the requestor"
    )
    subject_type: str | None = Field(None, title="type of the subject")
    status: str | None = Field(None, title="Status of requestor")
    place_of_residence: str | None = Field(
        None, title="place of residence for requestor"
    )
    country_of_residence: str | None = Field(
        None, title="country of residence for requestor"
    )
    street: str | None = Field(None, title="Address street name")
    residence_street: str | None = Field(
        None, title="Residence address street name"
    )
    zipcode: str | None = Field(None, title="Zipcode value")
    residence_zipcode: str | None = Field(
        None, title="Residence address zipcode"
    )
    house_number: str | None = Field(
        None, title="House number for the requestor"
    )
    residence_house_number: str | None = Field(
        None, title="Residence house number for requestor"
    )
    mobile_number: str | None = Field(
        None, title="Contact mobile number of requestor"
    )
    phone_number: str | None = Field(
        None, title="contact phone number of requestor"
    )
    email: str | None = Field(None, title="Email address of requestor")
    department: str | None = Field(
        None, title="Name of the department for requestor"
    )
    is_secret: bool | None = Field(None, title="If its secret value")
    a_number: str | None = Field(
        None, title="Another identifier in the dutch systems, called a_nummer"
    )
    foreign_residence_address_line1: str | None = Field(
        None, title="foreign residence address first line"
    )
    foreign_residence_address_line2: str | None = Field(
        None, title="foreign residence address second line"
    )
    foreign_residence_address_line3: str | None = Field(
        None, title="foreign residence address third line"
    )
    has_correspondence_address: bool | None = Field(
        None, title="Indicate if requestor has correspondence address"
    )
    correspondence_house_number: str | None = Field(
        None, title="correspondence house number"
    )
    correspondence_zipcode: str | None = Field(
        None, title="correspondence zipcode value"
    )
    correspondence_place_of_residence: str | None = Field(
        None, title="correspondence place of residence"
    )
    correspondence_street: str | None = Field(
        None, title="correspondence address street value"
    )
    password: str | None = Field(None, title="Password details for requestor")
    investigation: str | None = Field(
        None, title="investigation value for reqestor"
    )
    date_of_marriage: datetime | None = Field(
        None, title="Marriage date for requestor"
    )
    coc: str | None = Field(
        None, title="Chamber of Commerce number of the organization"
    )
    type_of_business_entity: str | None = Field(
        None, title="Type of the business"
    )
    establishment_number: str | None = Field(
        None, title="Establishment number of requestor organization"
    )
    trade_name: str | None = Field(
        None, title="Trade name for requestor organization"
    )
    login: str | None = Field(None, title="login details for requestor")
    title: str | None = Field(None, title="Title for the requestor")
