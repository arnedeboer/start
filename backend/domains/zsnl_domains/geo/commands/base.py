# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import minty.cqrs
from ..repositories import (
    GeoFeatureRelationshipRepository,
    GeoFeatureRepository,
)
from minty.exceptions import NotFound
from pydantic import validate_arguments
from typing import cast
from uuid import UUID, uuid4


class CreateGeoFeature(minty.cqrs.SplitCommandBase):
    name = "create_geo_feature"

    @validate_arguments
    def __call__(
        self,
        uuid: UUID,
        geojson: dict,
        source_event_timestamp: datetime.datetime,
    ):
        """
        Creates a new GeoFeature entry for an entity with the specified UUID
        """

        repo = cast(GeoFeatureRepository, self.get_repository("geo_feature"))
        repo.create_geo_feature(
            uuid=uuid,
            geo_json=geojson,
            last_modified=source_event_timestamp,
        )
        repo.save()


class DeleteGeoFeature(minty.cqrs.SplitCommandBase):
    name = "delete_geo_feature"

    @validate_arguments
    def __call__(self, object_uuid: UUID):
        """
        Delete geo features.
        """

        repo = cast(GeoFeatureRepository, self.get_repository("geo_feature"))
        try:
            geo_feature = repo.get_geo_feature(uuid=object_uuid)
        except NotFound:
            self.logger.debug(
                f"Geo not found with {object_uuid=}. Skipping delete."
            )
            return

        geo_feature.delete()

        repo.save()


class SetGeoFeatureRelationships(minty.cqrs.SplitCommandBase):
    name = "set_geo_feature_relationships"

    @validate_arguments
    def __call__(
        self,
        origin_uuid: UUID,
        related_uuids: list[UUID],
        source_event_timestamp: datetime.datetime,
    ):
        "Set the relationships for a geo feature"

        repo = cast(
            GeoFeatureRelationshipRepository,
            self.get_repository("geo_feature_relationship"),
        )
        current_relationships = repo.get_relationships(origin_uuid=origin_uuid)

        for relationship in current_relationships:
            related_uuid = relationship.related_uuid
            if related_uuid in related_uuids:
                # Relationship already exists: remove from "to add" list
                related_uuids.remove(related_uuid)

            elif (
                relationship.last_modified
                and relationship.last_modified < source_event_timestamp
            ):
                # Relationship no longer exists: delete
                relationship.delete()

        for related_uuid in related_uuids:
            repo.create_geo_feature_relationship(
                relationship_uuid=uuid4(),
                origin_uuid=origin_uuid,
                related_uuid=related_uuid,
                last_modified=source_event_timestamp,
            )

        repo.save()


class UpdateGeoFeatureRelationships(minty.cqrs.SplitCommandBase):
    name = "update_geo_feature_relationships"

    @validate_arguments
    def __call__(
        self,
        origin_uuid: UUID,
        added: list[UUID],
        removed: list[UUID],
        source_event_timestamp: datetime.datetime,
    ):
        """
        Update the list of relationships for a geo feature by adding and
        removing relations as specified.
        """

        repo = cast(
            GeoFeatureRelationshipRepository,
            self.get_repository("geo_feature_relationship"),
        )
        current_relationships = {
            relationship.related_uuid: relationship
            for relationship in repo.get_relationships(origin_uuid=origin_uuid)
        }

        for relationship in current_relationships:
            if relationship in removed:
                # Relationship found in "current relationships", and it's on
                # the remove list.
                current_relationships[relationship].delete()

        for relation_to_add in added:
            if relation_to_add in current_relationships:
                # Already there, no need to add again.
                continue

            # Create the newly added relations
            repo.create_geo_feature_relationship(
                relationship_uuid=uuid4(),
                origin_uuid=origin_uuid,
                related_uuid=relation_to_add,
                last_modified=source_event_timestamp,
            )

        repo.save()
