# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._common import MapMagicString
from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class Case(Entity):
    """
    Entity representing a case as seen by the geo domain.

    It is very minimal on purpose, as the "actual" case is part of the
    case-management domain; this only has the UUID as and a list of magic
    strings that should be represented on the map.
    """

    uuid: UUID = Field(..., title="Unique Identifier for a case")
    map_magic_strings: list[MapMagicString] = Field(
        ...,
        title="List of magic strings of custom fields that should be shown on the map",
    )
    family: str = Field(..., title="Family for this entity is case")
    status: str = Field(..., title="Status of the case")
    assignee: str | None = Field(..., title="Assignee of the case")
    title: str = Field(..., title="Id of the case")
    subtitle: str | None = Field(
        ..., title="Subtitle is the extra information for the case"
    )
    type: str | None = Field(..., title="Type of the case")
    requestor: str = Field(..., title="Requestor of the case")
    requestor_uuid: UUID | None = Field(
        None, title="UUID of requestor of the case"
    )

    use_geojson_address: bool = Field(
        ...,
        title="Flag that indicates whether the geo-data of the requestor needs to be linked",
    )

    entity_type = "case"
    entity_id__fields = ["uuid"]
