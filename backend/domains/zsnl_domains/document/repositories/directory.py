# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import Directory
from .database_queries import (
    directory_query,
    parent_directories_query,
    sub_directories_query,
)
from minty.exceptions import Conflict
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from sqlalchemy.exc import IntegrityError
from typing import List
from uuid import UUID
from zsnl_domains.database import schema


class DirectoryRepository(ZaaksysteemRepositoryBase):
    def get_parent_directories_for_directory(self, directory_uuid: UUID):
        parent_directories_qry_stmt = parent_directories_query().where(
            sql.and_(schema.Directory.uuid == directory_uuid)
        )

        directory_qry_stmt = directory_query().where(
            schema.Directory.uuid == directory_uuid
        )
        qry = sql.union_all(directory_qry_stmt, parent_directories_qry_stmt)
        directories = self.session.execute(qry).fetchall()
        return [
            self._transform_to_entity(directory_row)
            for directory_row in directories
        ]

    def _transform_to_entity(self, directory) -> Directory:
        directory = Directory(
            uuid=directory.uuid,
            name=directory.name,
            parent=directory.parent,
            case_id=directory.case_id,
            path=directory.path,
        )

        directory.event_service = self.event_service
        return directory

    def get_directory_by_uuid(self, uuid: UUID) -> Directory:
        directory = self.session.execute(
            sql.select(
                schema.Directory.name.label("name"),
                schema.Directory.case_id.label("case_id"),
                schema.Directory.path.label("path"),
                schema.Directory.uuid.label("uuid"),
                sql.expression.literal(None).label("parent"),
            ).where(schema.Directory.uuid == uuid)
        ).fetchone()

        if directory is None:
            raise Conflict(
                f"Could not find directory with uuid '{uuid}'",
                "domains/document/repositories/get_directory_by_uuid",
            )

        return self._transform_to_entity(directory)

    def get_directory_id_by_uuid(self, uuid: UUID) -> int:
        """
        Get the directory.id from the database for an
        given directiry.uuid
        :param uuid: UUID The directory UUID
        :return int: directory.id
        :raises Conflict if no directory found with given uuid.
        """
        directory = self.session.execute(
            sql.select(schema.Directory.id).where(
                schema.Directory.uuid == uuid
            )
        ).fetchone()

        if directory is None:
            raise Conflict(
                f"Could not find directory with uuid '{uuid}'",
                "domains/document/repositories/get_directory_id_by_uuid",
            )

        return directory.id

    def create_directory(
        self, directory_uuid: UUID, name: str, case_id: int, path: list
    ) -> Directory:
        """
        Create a new Directory Entity
        :param name: str: The name of the directory
        :param case_id: id: The case_id
        :param path: list[int] The new path of the directory
        :return: Directory Entity
        """
        directory = Directory(directory_uuid)
        directory.event_service = self.event_service
        directory.create(case_id=case_id, name=name, path=path)

        return directory

    def save_directory(self, event):
        """
        Save the new directory entity the database.
        :param event:
        :return:
        """
        current_changes = {}
        for change in event.changes:
            current_changes[change["key"]] = change["new_value"]

        # For the conversion of list[int] to PostgreSQL ARRAY[]::Integer[]
        # an conversion is needed. Because the type of an empty list
        # cannot be determined in PostgreSQL, and SQLAlchemy's
        # cast postgresql.array([], type_=INTEGER) will not work.
        # But we change the empty list to an empty dict, which in
        # PostgreSQL represents an empty array it will determine
        # the underlying ARRAY[]::Integer[] automagically.
        if current_changes["path"] == []:
            current_changes["path"] = {}
        else:
            current_changes["path"] = postgresql.array(current_changes["path"])

        try:
            statement = sql.insert(schema.Directory).values(
                {
                    "name": current_changes["name"],
                    "case_id": current_changes["case_id"],
                    "path": current_changes["path"],
                    "original_name": current_changes["name"],
                    "uuid": event.entity_id,
                },
            )

            self.session.execute(statement)

        except IntegrityError as e:
            raise Conflict(
                f"Database error inserting new directory with name '{current_changes['name']}' for case '{current_changes['case_id']}' and uuid '{event.entity_id}'",
                "domains/document/create_directory/integrity_error",
            ) from e

    def save(self):
        for event in self.event_service.event_list:
            if event.event_name == "DirectoryCreated":
                self.save_directory(event)
            if event.event_name == "DirectoryMoved":
                self.move_directory(event)

    def move_directory(self, event):
        current_changes = {}

        for change in event.changes:
            current_changes[change["key"]] = change["new_value"]

        if current_changes["path"] == []:
            current_changes["path"] = {}
        else:
            current_changes["path"] = postgresql.array(current_changes["path"])

        self.session.execute(
            sql.update(schema.Directory)
            .values({schema.Directory.path: current_changes["path"]})
            .where(schema.Directory.uuid == event.entity_id)
            .execution_options(synchronize_session=False)
        )
        self.session.commit()
        self._move_sub_direcotries(
            directory_uuid=str(event.entity_id),
            new_path=change["new_value"],
            old_path=change["old_value"],
        )

    def _get_sub_directories(self, directory_uuid) -> List[Directory]:
        sub_directories_qry_stmt = sub_directories_query(directory_uuid)
        directories = self.session.execute(sub_directories_qry_stmt).fetchall()

        return [
            self._transform_to_entity(directory_row)
            for directory_row in directories
        ]

    def _move_sub_direcotries(self, directory_uuid, new_path, old_path):
        sub_directories = self._get_sub_directories(directory_uuid)

        for directory in sub_directories:
            updated_path = []
            updated_path.extend(new_path)
            filtered_path = [i for i in directory.path if i not in old_path]
            updated_path.extend(filtered_path)
            self.session.execute(
                sql.update(schema.Directory)
                .values(
                    {schema.Directory.path: postgresql.array(updated_path)}
                )
                .where(schema.Directory.uuid == directory.uuid)
                .execution_options(synchronize_session=False)
            )

        self.session.commit()
