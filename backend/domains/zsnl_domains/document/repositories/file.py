# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import mimetypes
import os
from ... import ZaaksysteemRepositoryBase
from ..entities import File
from .shared import clean_filename_part
from minty.exceptions import NotFound
from minty_infra_storage import S3Infrastructure
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


def _generate_filename(file_uuid: UUID, mimetype: str):
    extension = mimetypes.guess_extension(mimetype)
    if extension is None:
        if mimetype.startswith("message/") or mimetype.startswith("text/"):
            extension = ".txt"
        else:
            extension = ".bin"

    return str(file_uuid) + extension


class FileRepository(ZaaksysteemRepositoryBase):
    ZaaksysteemRepositoryBase.REQUIRED_INFRASTRUCTURE.update(
        {"s3": S3Infrastructure()}
    )

    def create_file(
        self, file_uuid, filename, mimetype, size, storage_location, md5
    ) -> File:
        f = File(file_uuid)
        f.event_service = self.event_service

        if filename is None:
            filename = _generate_filename(file_uuid, mimetype)

        (basename, extension) = os.path.splitext(filename)
        basename = clean_filename_part(basename)
        extension = clean_filename_part(extension)
        if mimetype == "message/rfc822" and extension == "":
            extension = ".eml"
        f.create(basename, extension, mimetype, size, storage_location, md5)
        return f

    def _save_file_created(self, event):
        current_changes = {}
        for change in event.changes:
            current_changes[change["key"]] = change["new_value"]

        self.session.execute(
            sql.insert(schema.Filestore).values(
                {
                    "uuid": event.entity_id,
                    "original_name": current_changes["basename"]
                    + current_changes["extension"],
                    "size": current_changes["size"],
                    "mimetype": current_changes["mimetype"],
                    "storage_location": [current_changes["storage_location"]],
                    "is_archivable": current_changes["is_archivable"],
                    "md5": current_changes["md5"],
                },
            )
        )

    def save(self):
        for event in self.event_service.event_list:
            if event.event_name == "FileCreated":
                self._save_file_created(event)

    def get_file_by_uuid(self, file_uuid):
        """Get file by uuid.

        :param file_uuid: UUID of the filestore.
        :type file_uuid: UUID
        :raises NotFound: When file with uuid not found.
        :return: File
        :rtype: File
        """
        file_query = sql.select(
            schema.Filestore.uuid,
            schema.Filestore.original_name.label("filename"),
            schema.Filestore.mimetype,
            schema.Filestore.size,
            schema.Filestore.storage_location,
            schema.Filestore.md5,
            schema.Filestore.is_archivable,
            schema.Filestore.virus_scan_status,
        ).where(schema.Filestore.uuid == file_uuid)

        file_row = self.session.execute(file_query).fetchone()

        if not file_row:
            raise NotFound(
                f"No file found with uuid={file_uuid}", "file/not_found"
            )

        return self._transform_to_entity(file_row)

    def _transform_to_entity(self, file_row):
        """Transform a file_row to entity File.

        :param file_row: file_row from databse.
        :type file_row: database record
        :return: File Entity
        :rtype: File
        """
        (basename, extension) = os.path.splitext(file_row.filename)
        basename = clean_filename_part(basename)
        extension = clean_filename_part(extension)

        return File(
            file_uuid=file_row.uuid,
            basename=basename,
            extension=extension,
            mimetype=file_row.mimetype,
            size=file_row.size,
            storage_location=file_row.storage_location,
            md5=file_row.md5,
            is_archivable=file_row.is_archivable,
            virus_scan_status=file_row.virus_scan_status,
        )
