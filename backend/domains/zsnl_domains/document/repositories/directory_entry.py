# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import zsnl_domains.document.repositories.constants as constants
from ... import ZaaksysteemRepositoryBase
from ..entities import DirectoryEntry
from .database_queries import (
    directory_entry_directory_query,
    directory_entry_document_query,
)
from collections import namedtuple
from minty import exceptions
from minty.exceptions import ValidationError
from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema

UserInfo = namedtuple("UserInfo", "user_uuid permissions")

DEFAULT_SORT_ORDER = [sql.asc(schema.File.id)]

FILE_SORT_ORDERS = {
    "attributes.name": [sql.asc(schema.File.name), *DEFAULT_SORT_ORDER],
    "attributes.extension": [
        sql.asc(schema.File.extension),
        *DEFAULT_SORT_ORDER,
    ],
    "attributes.description": [
        sql.asc(schema.FileMetaData.description),
        *DEFAULT_SORT_ORDER,
    ],
    "attributes.last_modified_date_time": [
        sql.asc(schema.File.date_modified),
        *DEFAULT_SORT_ORDER,
    ],
    "-attributes.name": [sql.desc(schema.File.name), *DEFAULT_SORT_ORDER],
    "-attributes.extension": [
        sql.desc(schema.File.extension),
        *DEFAULT_SORT_ORDER,
    ],
    "-attributes.description": [
        sql.desc(schema.FileMetaData.description),
        *DEFAULT_SORT_ORDER,
    ],
    "-attributes.last_modified_date_time": [
        sql.desc(schema.File.date_modified),
        *DEFAULT_SORT_ORDER,
    ],
}


class DirectoryEntryRepository(ZaaksysteemRepositoryBase):
    def get_directory_entries_for_case(
        self,
        user_info,
        case_uuid: UUID,
        directory_uuid: UUID = None,
        search_term: str = None,
        no_empty_folders: bool = True,
    ):
        """Get directory entires for case to show in document tab

        :param user_info: User info dict.
        :type user_info: dict
        :param case_uuid: UUID of the case.
        :type case_uuid: UUID
        :param directory_uuid: UUID of the parent_directory/folder, defaults to None
        :type directory_uuid: UUID, optional
        :param search_term: Search term
        :type search_term: str
        :param no_empty_folders: Boolean to filter out empty folders.
        :type no_empty_folders: bool, optional
        :return: List of directory entries.
        :rtype: typing.List[DirectoryEntry]
        """
        is_pip_user = user_info.permissions.get("pip_user", False)
        user_uuid = user_info.user_uuid

        documents_qry_stmt = directory_entry_document_query(
            db=self.session,
            user_uuid=user_uuid,
            directory_uuid=directory_uuid,
            search_term=search_term,
            is_pip_user=is_pip_user,
        ).where(sql.and_(schema.Case.uuid == case_uuid))

        directories_qry_stmt = directory_entry_directory_query(
            db=self.session,
            user_uuid=user_uuid,
            directory_uuid=directory_uuid,
            search_term=search_term,
            is_pip_user=is_pip_user,
            no_empty_folders=no_empty_folders,
        ).where(schema.Case.uuid == case_uuid)

        qry_stmt = sql.union_all(documents_qry_stmt, directories_qry_stmt)
        document_entries = self.session.execute(qry_stmt).fetchall()

        return [self._transform_to_entity(entry) for entry in document_entries]

    def _transform_to_entity(self, entry) -> DirectoryEntry:
        """Transform directory_entry row to DirectoryEntry entity.

        :param directory_row: Database row of the directory_entry.
        :type directory_row: ResultProxy
        """

        preview_available = (
            entry.preview_uuid is not None
            or entry.mimetype == constants.PDF_MIMETYPE
        )

        document = (
            {
                "uuid": entry.uuid,
                "preview_available": preview_available,
                "intake_owner": entry.intake_owner,
                "intake_role_id": entry.intake_role_id,
                "role": entry.intake_role_name,
                "department": entry.intake_group_name,
                "intake_group_id": entry.intake_group_id,
                "intake_owner_display_name": entry.intake_owner_display_name,
                "intake_role_name": entry.intake_role_name,
                "intake_group_name": entry.intake_group_name,
                "thumbnail_uuid": entry.thumbnail_uuid,
                "thumbnail_mimetype": entry.thumbnail_mimetype,
            }
            if entry.type == "document"
            else None
        )

        directory = {"uuid": entry.uuid} if entry.type == "directory" else None

        assignment = (
            {
                "employee": {
                    "name": document.get("intake_owner_display_name", "")
                },
                "role": {"name": document.get("intake_role_name", "")},
                "department": {"name": document.get("intake_group_name", "")},
            }
            if document
            else {
                "employee": {"name": None},
                "role": {"name": None},
                "department": {"name": None},
            }
        )

        return DirectoryEntry(
            uuid=entry.uuid,
            name=entry.name,
            entry_type=entry.type,
            description=entry.description,
            extension=entry.extension,
            mimetype=entry.mimetype,
            accepted=entry.accepted,
            document_number=entry.document_number,
            last_modified_date_time=entry.last_modified_date_time,
            parent={
                "uuid": entry.parent_directory_uuid,
                "display_name": entry.parent_directory_display_name,
            },
            case={
                "uuid": entry.case_uuid,
                "display_number": entry.case_display_number,
            },
            modified_by={
                "uuid": entry.modified_by_uuid,
                "display_name": entry.modified_by_display_name,
            },
            document=document,
            directory=directory,
            rejection_reason=entry.rejection_reason,
            rejected_by_display_name=entry.rejected_by_display_name,
            assignment=assignment,
        )

    def get_directory_entries_for_intake(
        self,
        user_info: UserInfo,
        page: int,
        page_size: int,
        sort: str | None = None,
        search_term: str | None = None,
        assigned: bool | None = None,
        assigned_to_self: bool | None = None,
    ) -> list[DirectoryEntry]:
        user_uuid = user_info.user_uuid
        offset = self._calculate_offset(page, page_size)

        intake_conditions = [
            schema.File.case_id.is_(None),
            schema.File.skip_intake.is_(False),
            schema.File.queue.is_(True),
        ]

        if assigned and assigned_to_self:
            raise ValidationError(
                "Cannot request both assigned filtes at the same time"
            )

        if assigned_to_self is True:
            intake_owner = self._get_intake_owner_from_user_uuid(
                user_uuid=user_uuid
            )
            intake_conditions.append(schema.File.intake_owner == intake_owner)

        elif assigned is True:
            intake_conditions.append(
                sql.or_(
                    schema.File.intake_owner.isnot(None),
                    sql.and_(
                        schema.File.intake_group_id.isnot(None),
                        schema.File.intake_role_id.isnot(None),
                    ),
                )
            )
        elif assigned is False:
            intake_conditions.extend(
                [
                    schema.File.intake_owner.is_(None),
                    schema.File.intake_group_id.is_(None),
                    schema.File.intake_role_id.is_(None),
                ]
            )

        if sort:
            sort_order = FILE_SORT_ORDERS[sort]
        else:
            sort_order = DEFAULT_SORT_ORDER

        if user_info.permissions.get("documenten_intake_all"):
            self.logger.info("User has: documenten_intake_all == True")
            qry_stmt = (
                directory_entry_document_query(
                    db=self.session,
                    user_uuid=user_uuid,
                    search_term=search_term,
                    is_intake=True,
                )
                .where(sql.and_(*intake_conditions))
                .order_by(*sort_order)
                .offset(offset)
                .limit(page_size)
            )
        elif user_info.permissions.get("documenten_intake_subject"):
            self.logger.info("User has: documenten_intake_subject == True")
            qry_stmt = (
                directory_entry_document_query(
                    db=self.session,
                    user_uuid=user_uuid,
                    search_term=search_term,
                    is_intake=True,
                )
                .where(
                    sql.and_(
                        *intake_conditions,
                        sql.or_(
                            sql.exists(
                                sql.select(sql.literal(1))
                                .select_from(
                                    sql.join(
                                        schema.SubjectPositionMatrix,
                                        schema.Subject,
                                        schema.Subject.id
                                        == schema.SubjectPositionMatrix.subject_id,
                                    )
                                )
                                .where(
                                    sql.and_(
                                        schema.SubjectPositionMatrix.role_id
                                        == schema.File.intake_role_id
                                    ),
                                    schema.SubjectPositionMatrix.group_id
                                    == schema.File.intake_group_id,
                                    schema.Subject.uuid == user_uuid,
                                )
                            ),
                            sql.func.get_subject_by_legacy_id(
                                schema.File.intake_owner
                            )
                            == user_uuid,
                        ),
                    ),
                )
                .order_by(*sort_order)
                .offset(offset)
                .limit(page_size)
            )
        else:
            return []

        document_entries = self.session.execute(qry_stmt).fetchall()
        return [self._transform_to_entity(entry) for entry in document_entries]

    def _get_intake_owner_from_user_uuid(self, user_uuid):
        subject = self.session.execute(
            sql.select(schema.Subject.id).where(
                schema.Subject.uuid == user_uuid
            )
        ).fetchone()
        if subject is None:
            raise exceptions.NotFound(
                f"Subject with uuid '{user_uuid}' not found",
                "subject/not_found",
            )
        intake_owner = "betrokkene-medewerker-" + str(subject.id)
        return intake_owner
