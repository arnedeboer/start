# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from . import shared
from .document_label import DocumentLabel
from collections.abc import Sequence
from datetime import datetime
from dateutil.tz import gettz
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict, Forbidden
from uuid import UUID

logger = logging.getLogger(__name__)


class Document(EntityBase):
    @property
    def entity_id(self):
        return self.document_uuid

    def __init__(
        self,
        document_uuid,
        basename=None,
        extension=None,
        store_uuid=None,
        directory_uuid=None,
        case_uuid=None,
        case_display_number=None,
        mimetype=None,
        size=None,
        storage_location=None,
        md5=None,
        is_archivable=None,
        virus_scan_status=None,
        accepted=None,
        date_modified=None,
        thumbnail=None,
        creator_uuid=None,
        creator_displayname=None,
        attachment_uuid=None,
        message_uuid=None,
        creator_type=None,
        output_format="pdf",
        preview_uuid=None,
        preview_storage_location=None,
        preview_mimetype=None,
        labels=None,
        origin=None,
        origin_date=None,
        description=None,
        confidentiality=None,
        document_category=None,
        document_number=None,
        current_version=None,
        integrity_check_successful=None,
        destroy_reason=None,
        rejection_reason=None,
        rejected_by_display_name=None,
        intake_owner_uuid=None,
        intake_group_uuid=None,
        intake_role_uuid=None,
        skip_intake=False,
        thumbnail_uuid=None,
        thumbnail_storage_location=None,
        thumbnail_mimetype=None,
        auto_accept=False,
        explicit_accpet=False,
    ):
        self.document_uuid = document_uuid
        self.basename = basename
        self.extension = extension
        self.store_uuid = store_uuid

        self.directory_uuid = directory_uuid
        self.case_uuid = case_uuid
        self.case_display_number = case_display_number
        self.mimetype = mimetype
        self.size = size
        self.storage_location = storage_location
        self.md5 = md5
        self.is_archivable = is_archivable
        self.virus_scan_status = virus_scan_status
        self.accepted = accepted
        self.date_modified = date_modified
        self.thumbnail = thumbnail
        self.creator_uuid = creator_uuid
        self.creator_displayname = creator_displayname
        self.attachment_uuid = attachment_uuid
        self.message_uuid = message_uuid
        self.creator_type = creator_type
        self.output_format = output_format

        self.preview_uuid = preview_uuid
        self.preview_storage_location = preview_storage_location
        self.preview_mimetype = preview_mimetype

        self.labels = labels
        self.origin = origin
        self.origin_date = origin_date
        self.description = description
        self.confidentiality = confidentiality
        self.document_category = document_category

        self.document_number = document_number
        self.current_version = current_version

        self.integrity_check_successful = integrity_check_successful
        self.rejection_reason = rejection_reason
        self.rejected_by_display_name = rejected_by_display_name
        self.intake_owner_uuid = intake_owner_uuid
        self.intake_group_uuid = intake_group_uuid
        self.intake_role_uuid = intake_role_uuid
        self.skip_intake = skip_intake

        self.thumbnail_uuid = thumbnail_uuid
        self.thumbnail_storage_location = thumbnail_storage_location
        self.thumbnail_mimetype = thumbnail_mimetype
        self.auto_accept = auto_accept
        self.explicit_accpet = explicit_accpet

    @event("DocumentCreated")
    def create(
        self,
        basename,
        extension,
        accepted,
        store_uuid,
        directory_uuid,
        case_uuid,
        mimetype,
        size,
        storage_location,
        md5,
        creator_type="employee",
        skip_intake=False,
        auto_accept=False,
        virus_scan_status="pending",
    ):
        if self.store_uuid is not None:
            raise Conflict(
                "Can't create an already existing document",
                "document/already_exists",
            )

        if basename == "" or basename.startswith("."):
            raise Forbidden(
                "File name may not start with '.'", "file/starts_with_dot"
            )

        self.basename = basename
        self.extension = extension
        self.accepted = accepted
        self.store_uuid = store_uuid
        self.directory_uuid = directory_uuid
        self.case_uuid = case_uuid
        self.mimetype = mimetype
        self.size = size
        self.storage_location = storage_location
        self.md5 = md5
        self.is_archivable = shared.is_archivable(extension, mimetype)
        self.virus_scan_status = virus_scan_status
        self.creator_type = creator_type
        self.skip_intake = skip_intake
        self.auto_accept = auto_accept
        self.labels = []

    @event("DocumentFromAttachmentCreated")
    def create_document_from_attachment(self, attachment_uuid):
        self.attachment_uuid = attachment_uuid

    @event(
        "DocumentAddedToCase",
        extra_fields=[
            "md5",
            "size",
            "basename",
            "extension",
            "mimetype",
            "is_archivable",
            "description",
            "origin",
            "origin_date",
        ],
    )
    def add_to_case(self, case_uuid: UUID):
        if self.case_uuid:
            raise Conflict(
                f"Document with uuid '{self.document_uuid}' is already linked to a case with uuid '{self.case_uuid}'.",
                "document/already_linked_to_a_case",
            )
        self.case_uuid = case_uuid

    @event("DocumentUpdated")
    def update(
        self,
        basename,
        description,
        document_category,
        origin,
        origin_date,
        confidentiality,
    ):
        today = datetime.now(tz=gettz("Europe/Amsterdam")).date()
        self.basename = basename if basename is not None else self.basename
        self.description = (
            description if description is not None else self.description
        )
        self.document_category = (
            document_category
            if document_category is not None
            else self.document_category
        )
        self.origin = origin if origin is not None else self.origin
        self.origin_date = origin_date if origin_date else today
        self.confidentiality = (
            confidentiality
            if confidentiality is not None
            else self.confidentiality
        )

    @event("DocumentDeleted", extra_fields=["destroy_reason"])
    def delete(self, reason=""):
        """
        Permanently deletes a document. Works for documents not assigned to a case (document-intake).
        ToDo: When deleting from Document Tab (so assigned to a case) allow deletion of documents in Trash Bin.
        ToDo: So also make a trash_document method that puts it in the Trash Bin.
        :return:
        """
        if self.case_uuid is not None:
            raise Conflict(
                f"Document '{self.document_uuid}' is assign to case '{self.case_uuid}'",
                "document/delete_document/assigned_to_case",
            )

        self.destroy_reason = reason

    @event("DocumentAssignedToUser")
    def assign_document_to_user(self, intake_owner_uuid):
        self.intake_owner_uuid = intake_owner_uuid
        self.rejection_reason = None

    @event("DocumentAssignedToRole")
    def assign_document_to_role(self, intake_group_uuid, intake_role_uuid):
        self.intake_group_uuid = intake_group_uuid
        self.intake_role_uuid = intake_role_uuid
        self.rejection_reason = None

    @event("DocumentAssignmentRejected")
    def reject_from_intake(self, rejection_reason: str):
        if [
            self.intake_owner_uuid,
            self.intake_group_uuid,
            self.intake_role_uuid,
        ].count(None) == 3:
            raise Conflict(
                "Cannot reject a document that is not assigned",
                "document/reject_unassigned",
            )

        if self.case_uuid is not None:
            raise Conflict(
                "Cannot reject a document that is linked to a case",
                "document/reject_case_document",
            )

        self.intake_owner_uuid = None
        self.intake_group_uuid = None
        self.intake_role_uuid = None
        self.rejection_reason = rejection_reason

    @event("LabelsApplied", extra_fields=["case_uuid"])
    def apply_labels(self, labels: Sequence[DocumentLabel], case_uuid: UUID):
        existing_label_uuids = [str(label["uuid"]) for label in self.labels]
        new_labels = [*self.labels]

        for label in labels:
            if str(label.uuid) not in existing_label_uuids:
                new_labels.append(label)

        self.labels = new_labels
        self.case_uuid = case_uuid

    @event("LabelsRemoved", extra_fields=["case_uuid"])
    def remove_labels(self, labels: Sequence[DocumentLabel], case_uuid: UUID):
        new_labels = []

        labels_to_remove = [str(label.uuid) for label in labels]

        for label in self.labels:
            if str(label["uuid"]) not in labels_to_remove:
                new_labels.append(label)

        self.labels = new_labels
        self.case_uuid = case_uuid

    @event("DocumentMoved")
    def move_to_directory(self, directory_uuid):
        self.directory_uuid = directory_uuid

    @event(
        "DocumentAccepted",
        extra_fields=[
            "md5",
            "size",
            "basename",
            "extension",
            "mimetype",
            "is_archivable",
            "description",
            "origin",
            "origin_date",
            "case_uuid",
            "labels",
            "document_number",
        ],
    )
    def accept_document(self, accepted, explicit_accpet=False):
        self.accepted = accepted
        self.explicit_accpet = explicit_accpet


class File(EntityBase):
    @property
    def entity_id(self):
        return self.file_uuid

    def __init__(
        self,
        file_uuid,
        basename=None,
        extension=None,
        mimetype=None,
        size=None,
        storage_location=None,
        md5=None,
        is_archivable=None,
        virus_scan_status=None,
    ):
        self.file_uuid = file_uuid
        self.basename = basename
        self.extension = extension
        self.mimetype = mimetype
        self.size = size
        self.storage_location = storage_location
        self.md5 = md5
        self.is_archivable = is_archivable
        self.virus_scan_status = virus_scan_status

    @property
    def filename(self):
        return self.basename + self.extension

    @event("FileCreated")
    def create(
        self, basename, extension, mimetype, size, storage_location, md5
    ):
        if basename == "" or basename.startswith("."):
            raise Forbidden(
                "File name may not start with '.'", "file/starts_with_dot"
            )

        self.basename = basename
        self.extension = extension
        self.mimetype = mimetype
        self.size = size
        self.storage_location = storage_location
        self.md5 = md5
        self.is_archivable = shared.is_archivable(extension, mimetype)
        self.virus_scan_status = "pending"
