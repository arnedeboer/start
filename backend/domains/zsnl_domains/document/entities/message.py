# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from .message_external import MessageExternal
from datetime import datetime
from minty.entity import Entity, ValueObject
from pydantic import Field
from uuid import UUID

logger = logging.getLogger(__name__)


class Contact(ValueObject):
    name: str = Field(..., title="Contact name")
    contact_type: str = Field(..., title="Contact Type")


class Message(Entity):
    """
    Message Entity
    """

    entity_type = "message"
    entity_id__fields = ["uuid"]
    uuid: UUID = Field(..., title="The message uuid")
    case_uuid: UUID | None = Field(None, title="The related case uuid")

    message_type: str = Field(
        ..., title="The message type eg. external, contact_moment, etc"
    )
    created_by_displayname: str | None = Field(
        None, title="Display name of the message creator"
    )
    created_by_uuid: UUID | None = Field(
        None, title="The UUID of the message creator"
    )

    created: datetime = Field(
        ..., title="The datetime when the message was created in the database"
    )
    last_modified: datetime | None = Field(
        None, title="The datetime when the messages was last modified"
    )
    message_date: datetime | None = Field(
        None,
        title="The datetime when the corresponding message_external was created",
    )
    message_external: MessageExternal | None = Field(
        None, title="The related message_external"
    )
