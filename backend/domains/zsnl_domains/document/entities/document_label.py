# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class DocumentLabel(Entity):
    """Document Label entity for a case."""

    entity_type = "document_label"

    # Properties
    uuid: UUID = Field(..., title="Internal identifier")
    name: str = Field(..., title="Internal name")
    public_name: str = Field(..., title="Public name")
    magic_string: str = Field(..., title="Magic string")

    entity_id__fields = ["uuid"]
