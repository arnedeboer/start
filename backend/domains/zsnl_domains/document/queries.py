# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty import exceptions
from minty.cqrs import QueryBase
from minty.validation import validate_with
from pkgutil import get_data
from typing import cast
from uuid import UUID
from zsnl_domains.document.repositories import DocumentRepository, constants


class Queries(QueryBase):
    """Queries class for document."""

    @property
    def repository(self):
        return self.repository_factory.get_repository(
            "document", context=self.context
        )

    @validate_with(get_data(__name__, "validation/get_document.json"))
    def get_document_by_uuid(
        self, document_uuid: UUID, integrity_check: bool = False
    ):
        """Get document entity as python dictionary by given `document uuid`.
        :param documen_uuid: identifier of document
        :type document_uuid: UUID
        """

        document = self.repository.get_document_by_uuid(
            document_uuid, self.user_info, integrity_check
        )
        return document

    @validate_with(get_data(__name__, "validation/search_document.json"))
    def search_document(
        self,
        case_uuid: str | None,
        keyword: str | None,
        filter: str | None = None,
    ):
        """Search document by case_uuid or keyword.

        :param case_uuid: uuid of case.
        :type case_uuid: Optional[str]
        :param keyword: keyword for searching.
        :type keyword: Optional[str]
        :param filter: filter used for searching.
        :type filter: Optional[str]
        """

        document = self.repository.search_document(
            case_uuid=case_uuid,
            user_uuid=self.user_uuid,
            keyword=keyword,
            filter=filter,
        )
        return document

    @validate_with(
        get_data(__name__, "validation/get_directory_entries_for_case.json")
    )
    def get_directory_entries_for_case(
        self,
        case_uuid: UUID,
        directory_uuid: UUID = None,
        search_term: str = None,
        no_empty_folders: bool = True,
    ):
        """Get the list of directory_entries for a case to show in document_tab.

        To retrive the list of documents and directories related to a case.

        :param case_uuid: UUID of the case.
        :type case_uuid: UUID
        :param directory_uuid: UUID of the parent_directory, defaults to None
        :type directory_uuid: UUID, optional
        :param search_term: Filter the results with search_term, defaults to None
        :type search_term: str, optional
        :param no_empty_folders: Boolean to filter out empty folders.
        :type no_empty_folders: bool, optional
        :return: List of directory entries.
        :rtype: typing.List[DirectoryEntry]
        """
        directory_entry_repo = self.repository_factory.get_repository(
            "directory_entry", context=self.context
        )

        entries = directory_entry_repo.get_directory_entries_for_case(
            user_info=self.user_info,
            case_uuid=case_uuid,
            directory_uuid=directory_uuid,
            search_term=search_term,
            no_empty_folders=no_empty_folders,
        )

        return entries

    def get_parent_directories_for_directory(self, directory_uuid: UUID):
        """Get parent directories for directory.
        :param directory_uuid: directory_uuid
        :type directory_uuid: UUID
        """
        directory_repo = self.repository_factory.get_repository(
            "directory", context=self.context
        )

        directories = directory_repo.get_parent_directories_for_directory(
            directory_uuid=directory_uuid
        )

        return directories

    @validate_with(get_data(__name__, "validation/get_download_link.json"))
    def get_document_download_link(self, document_uuid: UUID, user_info):
        """
        Get the download link for a document given by document UUID
        :param document_uuid: The document UUID
        :param user_uuid: The user UUID
        :return:
        """
        document_repo = self.repository

        url = document_repo.generate_download_url(
            document_uuid=document_uuid, user_info=user_info
        )
        return url

    @validate_with(
        get_data(__name__, "validation/get_directory_entries_for_intake.json")
    )
    def get_directory_entries_for_intake(
        self,
        page: int,
        page_size: int,
        sort: str | None = None,
        search_term: str | None = None,
        assigned: bool | None = None,
        assigned_to_self: bool | None = None,
    ):
        """Get the list of documents as directory_entries for a document intake.

        To retrive the list of documents not related to a case.

        :param search_term: Filter the results with search_term, defaults to None
        :type search_term: str, optional
        :return: List of directory entries.
        :rtype: typing.List[DirectoryEntry]
        """
        directory_entry_repo = self.repository_factory.get_repository(
            "directory_entry", context=self.context
        )

        entries = directory_entry_repo.get_directory_entries_for_intake(
            user_info=self.user_info,
            page=page,
            page_size=page_size,
            sort=sort,
            search_term=search_term,
            assigned=assigned,
            assigned_to_self=assigned_to_self,
        )
        return entries

    @validate_with(get_data(__name__, "validation/get_download_link.json"))
    def get_document_preview_link(self, document_uuid: UUID, user_info):
        """
        Get the preview link for a document given by document UUID
        :param document_uuid: The document UUID
        :param user_uuid: The user UUID
        :return:
        """
        document_repo = self.repository
        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=user_info
        )
        if (
            document.mimetype in constants.IMAGE_MIMETYPES
            or document.mimetype == constants.PDF_MIMETYPE
        ):
            preview_uuid = document.store_uuid
            preview_mimetype = document.mimetype
            preview_storage_location = document.storage_location
        else:
            preview_uuid = document.preview_uuid
            preview_mimetype = document.preview_mimetype
            preview_storage_location = document.preview_storage_location

        if preview_uuid is None:
            raise exceptions.NotFound(
                f"No preview found for document with uuid {document_uuid}",
                "document/preview_not_found",
            )
        url = document_repo.generate_preview_url(
            preview_uuid=preview_uuid,
            preview_storage_location=preview_storage_location[0],
            preview_mime_type=preview_mimetype,
            preview_filename=document.basename + document.extension,
        )
        return url

    @validate_with(
        get_data(__name__, "validation/get_document_labels_for_case.json")
    )
    def get_document_labels_for_case(self, case_uuid: UUID):
        """Get document_labels for a case by case_uuid.

        :param case_uuid: UUID of the case
        :type case_uuid: UUID
        """

        repo = self.get_repository("document_label")
        document_labels = repo.get_document_labels_for_case(
            case_uuid=case_uuid, user_uuid=self.user_uuid
        )
        return document_labels

    @validate_with(get_data(__name__, "validation/get_thumbnail_link.json"))
    def get_document_thumbnail_link(self, document_uuid: UUID):
        """
        Get the thumbnail link for a document given by document UUID
        :param document_uuid: The document UUID
        :type document_uuid: UUID
        :return: Redirect link to the preview of document.
        """

        if not self.user_info:
            raise exceptions.Forbidden(
                "No user information", "document/user_required"
            )

        document_repo = cast(
            DocumentRepository, self.get_repository("document")
        )
        document = document_repo.get_document_by_uuid(
            document_uuid=document_uuid, user_info=self.user_info
        )

        if (
            document.thumbnail_uuid is None
            or document.thumbnail_storage_location is None
            or document.thumbnail_mimetype is None
        ):
            raise exceptions.NotFound(
                f"No thumbnail found for document with uuid {document_uuid}",
                "document/thumbnail_not_found",
            )

        url = document_repo.generate_thumbnail_url(
            thumbnail_uuid=document.thumbnail_uuid,
            thumbnail_storage_location=document.thumbnail_storage_location[0],
            thumbnail_mimetype=document.thumbnail_mimetype,
        )
        return url

    def get_edit_document_url(
        self,
        document_uuid: UUID,
        user_info,
        save_url,
    ):
        """Get URL to edit document online.

        :param document_uuid: UUID of the document.
        :type document_uuid: UUID
        :param user_info: user_info
        :type user_info: object
        :param save_url: url to save document
        :type save_url: str
        :rtype: URL to open document for editing
        """

        document_repo = self.get_repository("document")

        url = document_repo.generate_document_edit_url(
            document_uuid=document_uuid, user_info=user_info, save_url=save_url
        )

        return url

    def get_ms_wopi_configuration(
        self,
        document_uuid: UUID,
        user_info: object,
        save_url: str,
        close_url: str,
        context: str,
        host_page_url: str,
    ):
        """Get WOPI configuration to edit document with ms online"""

        document_repo = self.get_repository("document")
        return document_repo.get_ms_wopi_configuration(
            document_uuid=document_uuid,
            user_info=user_info,
            save_url=save_url,
            close_url=close_url,
            host_page_url=host_page_url,
            context=context,
        )
