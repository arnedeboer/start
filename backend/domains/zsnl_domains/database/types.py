# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import uuid
from datetime import datetime, timezone
from dateutil.tz import gettz
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.types import CHAR, DATETIME, TEXT, TypeDecorator


class UTCDate(TypeDecorator):
    """Database DateTime UTC field that should be Date fields but are not.

    This class handles the conversion from datetime(UTC) to Date fields
    because of legacy reasons we can't change the database fields yet so we're
    solving that in this class.
    """

    impl = DATETIME
    cache_ok = False

    def process_bind_param(self, value, dialect):
        if value is not None:
            if isinstance(value, str):
                value = datetime.fromisoformat(value)
            else:
                value = datetime(
                    year=value.year, month=value.month, day=value.day
                )
        return value

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            val = value.replace(tzinfo=timezone.utc)
            date_value = val.astimezone(tz=gettz("Europe/Amsterdam")).date()
        return date_value


class UTCDateTime(TypeDecorator):
    """Database DateTime field that's always UTC"""

    impl = DATETIME
    cache_ok = False

    def process_bind_param(self, value, dialect):
        if value is not None:
            if isinstance(value, str):
                value = datetime.fromisoformat(value).replace(
                    tzinfo=timezone.utc
                )
            if not isinstance(value, datetime):
                raise TypeError(
                    "Expected a datetime.datetime, not " + repr(value)
                )
            elif value.tzinfo is None:
                raise ValueError(
                    "datetime without time zone cannot be saved "
                    + "because it can't be converted to UTC reliably"
                )
        return value

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            val = value.replace(tzinfo=timezone.utc)
        return val


class GUID(TypeDecorator):
    """Platform-independent GUID type.

    Uses PostgreSQL's UUID type, otherwise uses
    CHAR(32), storing as stringified hex values.

    """

    impl = CHAR
    cache_ok = False

    def load_dialect_impl(self, dialect):
        if dialect.name == "postgresql":
            return dialect.type_descriptor(UUID())
        else:
            return dialect.type_descriptor(CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == "postgresql":
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            if not isinstance(value, uuid.UUID):
                value = uuid.UUID(value)
            return value


class JSONEncodedDict(TypeDecorator):
    """Represents an immutable structure as a json-encoded string.

    Usage:

        column_name = Column(JSONEncodedDict)
    """

    impl = TEXT
    cache_ok = False

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)

        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value
