# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs import event
from minty.entity import EntityBase
from uuid import UUID


class MessageAttachment(EntityBase):
    @property
    def entity_id(self):
        return self.file_uuid

    def __init__(
        self,
        filename: str,
        file_uuid: UUID,
        message_uuid: UUID,
        attachment_uuid: UUID,
    ):
        self.filename = filename
        self.file_uuid = file_uuid
        self.message_uuid = message_uuid
        self.attachment_uuid = attachment_uuid

    @event("AttachedToMessage")
    def attach_file(self, filename, file_uuid, message_uuid, attachment_uuid):
        self.filename = filename
        self.file_uuid = file_uuid
        self.message_uuid = message_uuid
        self.attachment_uuid = attachment_uuid


class AttachedFile(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        filename: str,
        md5: str,
        size: int,
        mimetype: str,
        date_created,
        storage_location,
        attachment_uuid=None,
        file_uuid=None,
        preview_uuid=None,
        preview_mimetype=None,
        preview_storage_location=None,
        preview_filename=None,
    ):
        self.uuid = uuid
        self.filename = filename
        self.md5 = md5
        self.mimetype = mimetype
        self.size = size
        self.date_created = date_created
        self.storage_location = storage_location
        self.attachment_uuid = attachment_uuid
        self.file_uuid = file_uuid if file_uuid is not None else uuid
        self.preview_uuid = preview_uuid
        self.preview_mimetype = preview_mimetype
        self.preview_storage_location = preview_storage_location
        self.preview_filename = preview_filename

    @event(
        "PdfDerivativeGenerated",
        extra_fields=["storage_location", "filename", "attachment_uuid"],
    )
    def generate_pdf_derivative(self):
        pass
