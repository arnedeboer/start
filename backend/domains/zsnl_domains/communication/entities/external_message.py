# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from .attachments import AttachedFile
from .contact import Contact
from .message import Message
from collections.abc import Iterable, Mapping
from datetime import datetime, timezone
from minty.cqrs import event
from minty.exceptions import Conflict
from uuid import UUID

logger = logging.getLogger(__name__)


class ExternalMessage(Message):
    def __init__(
        self,
        uuid: UUID,
        thread_uuid: UUID,
        case_uuid: UUID | None,
        created_by: Contact,
        created_by_displayname: str,
        message_slug: str,
        last_modified: datetime,
        created_date: datetime,
        content: str,
        subject: str,
        external_message_type: str,
        attachments: list[AttachedFile],
        participants: Iterable[Mapping[str, str]],
        direction: str,
        original_message_file: UUID,
        is_imported: bool = False,
        read_employee: datetime = None,
        read_pip: datetime = None,
        attachment_count: int = None,
        message_date: datetime = None,
        creator_type=None,
        failure_reason: str | None = None,
        case_html_email_template: str | None = None,
    ):
        super().__init__(
            message_type="external",
            uuid=uuid,
            thread_uuid=thread_uuid,
            case_uuid=case_uuid,
            message_slug=message_slug,
            created_date=created_date,
            last_modified=last_modified,
            created_by=created_by,
            created_by_displayname=created_by_displayname,
            message_date=message_date,
        )
        self.content = content
        self.subject = subject
        self.external_message_type = external_message_type
        self.attachments = attachments
        self.participants = participants
        self.direction = direction
        self.original_message_file = original_message_file
        self.is_imported = is_imported
        self.read_employee = read_employee
        self.read_pip = read_pip
        self.attachment_count = attachment_count
        self.creator_type = creator_type
        self.failure_reason = failure_reason
        self.case_html_email_template = case_html_email_template

    @event("ExternalMessageCreated", extra_fields=["thread_uuid"])
    def create(
        self,
        thread_uuid: UUID,
        created_by: Contact,
        content,
        subject,
        external_message_type,
        attachments,
        participants,
        direction,
        original_message_file,
        message_date: datetime = None,
        is_imported=False,
        creator_type="employee",
    ):
        now = datetime.now(timezone.utc).isoformat()

        if message_date is None:
            message_date = now

        self.thread_uuid = thread_uuid
        self.message_slug = content[:180]
        self.message_type = self.message_type
        self.created_by = created_by
        self.created_by_displayname = created_by.name if created_by else None
        self.content = content
        self.subject = subject
        self.external_message_type = external_message_type
        self.created_date = now
        self.last_modified = now
        self.message_date = message_date
        self.attachments = attachments
        self.participants = participants
        self.direction = direction
        self.original_message_file = original_message_file
        self.attachment_count = 0
        self.is_imported = is_imported
        self.creator_type = creator_type

    @event(
        "MessageDeleted",
        extra_fields=[
            "thread_uuid",
            "case_uuid",
            "message_type",
            "external_message_type",
        ],
    )
    def delete(self):
        """Deletes the current message and attachments."""
        pass

    @event("ExternalMessageSent")
    def send(self):
        pass

    @event("ExternalMessageMarkedUnread")
    def mark_unread(self, context):
        if context == "pip":
            if not self.read_pip:
                raise Conflict(
                    f"External message with uuid '{self.uuid}' is already unread",
                    "external_message/already_unread",
                )
            self.read_pip = None

        elif context == "employee":
            if self.read_employee is None:
                raise Conflict(
                    f"External message with uuid '{self.uuid}' is already unread",
                    "external_message/already_unread",
                )
            self.read_employee = None

    @event("ExternalMessageRead")
    def mark_read(self, context: str, timestamp: datetime):
        """Mark external message as read."""
        if context == "pip":
            if self.read_pip:
                raise Conflict(
                    f"External message with uuid:'{self.uuid}' is already read",
                    "external_message/already_read",
                )
            self.read_pip = timestamp
        elif context == "employee":
            if self.read_employee:
                raise Conflict(
                    f"External message with uuid:'{self.uuid}' is already read",
                    "external_message/already_read",
                )
            self.read_employee = timestamp

    @event("ExternalMessageAttachmentCountIncremented")
    def increment_attachment_count(self):
        self.attachment_count = self.attachment_count + 1
