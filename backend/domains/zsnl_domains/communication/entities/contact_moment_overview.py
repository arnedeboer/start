# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity
from pydantic import Field
from uuid import UUID


class ContactMomentOverview(Entity):
    """Entity represents contact moment properties.
    This will be used to get overview of contact moments in the system"""

    entity_type = "contact_moment_overview"
    entity_id__fields = ["uuid"]

    contact_uuid: UUID = Field(..., title="UUID of the contact")
    contact: str = Field(..., title="Display name of the contact")
    case_id: int | None = Field(..., title="Id of related case")
    direction: str | None = Field(..., title="Direction of the contact moment")
    uuid: UUID = Field(..., title="UUID of the contact moment")
    created: datetime = Field(..., title="Creation time of the contact moment")
    summary: str | None = Field(..., title="Summary of the contact moment")
    channel: str = Field(..., title="Channel of the contact moment")
    thread_uuid: UUID = Field(..., title="UUID of the thread")
    contact_type: str | None = Field(..., title="Type of the contact")
