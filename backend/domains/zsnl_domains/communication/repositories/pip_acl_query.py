# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from sqlalchemy import sql
from uuid import UUID
from zsnl_domains.database import schema


def pip_acl_query(user_uuid: UUID):
    """ACL query to get Case UUIDs a pip user is allowed to access.

    A pip user can either be an 'aanvrager'(assignee) or 'gemachtigde'(authorized)
    on the case.

    `usage: query.where(
        schema.Case.uuid.in_(
            pip_acl_query(uuid)
            )
        )
    )`

    :param user_uuid: pip_user uuid
    :type user_uuid: UUID
    :return: list of allowed case uuid's
    :rtype: query
    """
    case_zaakbetrokkenen = sql.join(
        schema.ZaakBetrokkenen,
        schema.Case,
        schema.Case.id == schema.ZaakBetrokkenen.zaak_id,
        isouter=False,
    )
    joined_schema = case_zaakbetrokkenen.join(
        schema.ZaaktypeNode,
        schema.ZaaktypeNode.id == schema.Case.zaaktype_node_id,
        isouter=False,
    )

    query = (
        sql.select(schema.Case.uuid)
        .select_from(joined_schema)
        .where(
            sql.and_(
                schema.ZaakBetrokkenen.subject_id == user_uuid,
                schema.ZaakBetrokkenen.deleted.is_(None),
                schema.Case.deleted.is_(None),
                schema.Case.status.in_(["new", "open", "stalled", "resolved"]),
                schema.ZaaktypeNode.prevent_pip.isnot(True),
                sql.or_(
                    schema.ZaakBetrokkenen.pip_authorized.is_(True),
                    schema.Case.aanvrager == schema.ZaakBetrokkenen.id,
                ),
            )
        )
    )
    return query
