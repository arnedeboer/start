# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import ZaaksysteemRepositoryBase
from ..entities import Contact, ContactMomentOverview
from . import database_queries
from minty.exceptions import NotFound
from minty.repository import Repository
from uuid import UUID


class ContactMomentOverviewRepository(Repository, ZaaksysteemRepositoryBase):
    _for_entity = "ContactMomentOverview"
    _events_to_calls: dict[str, str] = {}

    def get_contact_moment_list(self) -> list[ContactMomentOverview]:
        """Get list of contact moments.
        :rtype List[ContactMomentOverview]
        """
        contact_moment_rows = self.session.execute(
            database_queries.contact_moment_list_query()
        ).fetchall()

        return [
            self._entity_from_row(row=contact_moment_row)
            for contact_moment_row in contact_moment_rows
        ]

    def _entity_from_row(self, row) -> ContactMomentOverview:
        result = ContactMomentOverview(
            contact_uuid=row.contact_uuid,
            contact=row.contact,
            case_id=row.case_id,
            direction=row.direction,
            uuid=row.uuid,
            created=row.created,
            summary=row.summary,
            channel=row.channel,
            thread_uuid=row.thread_uuid,
            contact_type=self._get_contact_type(
                row.contact_uuid,
                row.contact,
            ),
        )
        return result

    def _get_contact_type(self, contact_uuid: UUID, contact_name: str) -> str:
        """Get contact type for given Contact

        :param contact_uuid: Contact uuid
        :type contact_uuid: UUID
        :param contact_name: Contact display name
        :type contact_name: str
        """
        if contact_uuid is None:
            return None

        contact_row = self.session.execute(
            database_queries.contact_type_query(contact_uuid)
        ).fetchone()

        if not contact_row:
            raise NotFound(
                f"Contact with uuid '{contact_uuid}' not found.",
                "communication/contact/not_found",
            )
        contact = Contact(
            uuid=contact_uuid, type=contact_row.type, name=contact_name
        )
        return contact.type
