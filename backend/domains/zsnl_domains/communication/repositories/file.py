# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import email
import os
import tempfile
from ... import ZaaksysteemRepositoryBase
from ..entities import File
from email.message import Message
from email.policy import default as DefaultMailPolicy
from minty.exceptions import NotFound
from minty_infra_storage import S3Infrastructure
from outlookmsgfile import load as load_outlook_msg
from sqlalchemy.sql import select
from uuid import UUID
from zsnl_domains.database.schema import Filestore

GetFileQuery = select(
    Filestore.uuid,
    Filestore.original_name,
    Filestore.size,
    Filestore.mimetype,
    Filestore.md5,
    Filestore.storage_location,
    Filestore.date_created,
)


class FileRepository(ZaaksysteemRepositoryBase):
    REQUIRED_INFRASTRUCTURE = {
        **ZaaksysteemRepositoryBase.REQUIRED_INFRASTRUCTURE,
        "s3": S3Infrastructure(),
    }

    def get_file_by_uuid(self, file_uuid: UUID) -> File:
        result = self.session.execute(
            GetFileQuery.where(Filestore.uuid == file_uuid)
        ).fetchone()

        if not result:
            raise NotFound("File not found")
        return self._transform_to_entity(result=result)

    def _transform_to_entity(self, result) -> File:
        return File(
            uuid=result.uuid,
            filename=result.original_name,
            md5=result.md5,
            size=result.size,
            mimetype=result.mimetype,
            date_created=result.date_created,
            storage_location=result.storage_location,
        )

    def get_file_handle(self, file_uuid):
        """ "Get a file handle to read the file with the given UUID

        :param file_uuid: UUID of the file to retrieve
        :type file_uuid: UUID
        :return: A handle to a temporary file that can be used to
            read the file's contents.
        :rtype: TemporaryFile
        """
        file_row = self.session.execute(
            GetFileQuery.where(Filestore.uuid == str(file_uuid))
        ).fetchone()

        storage_infra = self._get_infrastructure(name="s3")

        tmp_handle = tempfile.NamedTemporaryFile(mode="w+b")

        storage_infra.download_file(
            destination=tmp_handle,
            file_uuid=file_row.uuid,
            storage_location=file_row.storage_location[0],
        )

        tmp_handle.seek(0, os.SEEK_SET)

        return tmp_handle

    def upload(self, file_content, file_uuid: UUID):
        storage_infra = self._get_infrastructure(name="s3")

        upload_result = storage_infra.upload(
            file_handle=file_content, uuid=file_uuid
        )
        return upload_result

    def parse_as_message(self, file_uuid) -> Message:
        """Parse the message stored in `file_uuid` using
        `email.message_from_binary_file`

        :param file_uuid: UUID of the store file to read from
        :type file_uuid: str
        :return: The parsed email message, ready for processing
        :rtype: EmailMessage
        """
        file_handle = self.get_file_handle(file_uuid)
        return email.message_from_binary_file(
            file_handle, policy=DefaultMailPolicy
        )

    def __decode_error(self, error_message: bytes) -> str:
        encodings = ("utf-8", "cp1252")

        for encoding in encodings:
            try:
                return error_message.decode(encoding)
            except UnicodeDecodeError:
                pass

        return error_message.decode("utf-8", "replace")

    def parse_as_message_outlook(self, file_uuid) -> Message:
        """Parse a message of filetype `.msg`."""
        file_handle = self.get_file_handle(file_uuid)

        message = load_outlook_msg(file_handle)

        return message
