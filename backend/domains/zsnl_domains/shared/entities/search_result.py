# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty import entity
from pydantic import Field
from uuid import UUID


class SearchResultRelatedCustomObjectType(entity.Entity):
    entity_type = "custom_object_type"

    # Properties
    uuid: UUID = Field(..., title="Identifier of the custom_object_type")

    entity_id__fields = ["uuid"]


class SearchResultRelatedCase(entity.Entity):
    entity_type = "case"

    # Properties
    uuid: UUID = Field(..., title="Identifier of the case")

    entity_id__fields = ["uuid"]


class SearchResultRelatedCaseType(entity.Entity):
    entity_type = "case_type"

    # Properties
    uuid: UUID = Field(..., title="Identifier of the case_type")

    entity_id__fields = ["uuid"]


class SearchResult(entity.Entity):
    entity_type = "search_result"

    # Properties
    result_type: str = Field(
        ..., title="Type of the object represented by this search result"
    )
    uuid: UUID = Field(
        ..., title="UUID of the object represented by this search result"
    )
    description: str = Field(
        "", title="Description of the object represented by this search result"
    )

    entity_id__fields = ["uuid"]

    # only valid for type custom_object
    custom_object_type: SearchResultRelatedCustomObjectType | None = Field(
        None, title="Object type related to the custom_object"
    )
    case: SearchResultRelatedCase | None = Field(
        None, title="Case related to the search result, if applicable"
    )
    case_type: SearchResultRelatedCaseType | None = Field(
        None, title="Casetype related to the search_result, if applicable"
    )

    entity_relationships = ["custom_object_type", "case", "case_type"]
