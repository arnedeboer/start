# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import langdetect
import logging
import nltk.stem.snowball
import re
import time
from .types import FilterOperator
from pkgutil import get_data

stemmer_language_map = {
    "nl": "dutch",
    "en": "english",
    "fr": "french",
    "de": "german",
}

country_codes_json = get_data(__name__, "country_codes.json").decode("utf-8")
country_codes_list = json.loads(country_codes_json)
country_codes = {}
country_names = {}
for each in country_codes_list:
    country_codes[each["dutch_code"]] = each["label"]
    country_names[each["label"]] = each["dutch_code"]

legal_entity_codes_json = get_data(__name__, "legal_entity_codes.json").decode(
    "utf-8"
)
legal_entity_codes_list = json.loads(legal_entity_codes_json)
legal_entity_codes = {}
legal_entity_labels = {}
for each in legal_entity_codes_list:
    legal_entity_codes[each["code"]] = each["label"]
    legal_entity_labels[each["label"]] = each["code"]


def prepare_search_term(search_term: str):
    """
    Prepare a search term ("as entered") for use with TSVECTOR database fields.

    The prepared keywords for the search term will look like this:
    `case1:* & case2:*`.
    """

    try:
        language = langdetect.detect(search_term)
    except langdetect.lang_detect_exception.LangDetectException:
        # Langdetect couldn't detect a language; assume Dutch
        language = "nl"

    stemmer = nltk.stem.snowball.SnowballStemmer(
        stemmer_language_map.get(language, "dutch")
    )

    keywords = re.split(r"\W+", search_term)
    cleaned_keywords = {
        re.sub(r"[^A-Za-z0-9.@]+", "", keyword) for keyword in keywords
    }
    stemmed_keywords = {
        stemmer.stem(word) for word in cleaned_keywords if word
    }

    # The search_terms for file are stored as stemmed words in database.
    formatted_keywords = {f"{keyword}:*" for keyword in stemmed_keywords}

    return " & ".join(sorted(formatted_keywords))


def escape_term_for_like(term: str) -> str:
    return term.replace("~", "~~").replace("%", "~%").replace("_", "~_")


def get_type_of_business_from_entity_code(
    legal_entity_code: int,
) -> str | None:
    return legal_entity_codes.get(legal_entity_code, None)


def get_entity_code_from_type_of_business(
    type_of_business: str,
) -> int | None:
    return legal_entity_labels.get(type_of_business, None)


def get_country_name_from_landcode(landcode: int):
    return country_codes.get(landcode, None)


def get_landcode_from_country_name(country_name: str):
    return country_names.get(country_name, None)


def get_operator_values_from_filter(filter_dict):
    operator = filter_dict.get("operator", FilterOperator.or_operator)
    values = filter_dict.get("values")
    return operator, values


class TimedInMilliseconds:
    """Use this timer to determine if a block of code is executed within time.
    The threshold in milliseconds must be passed as an argument to the constructor.
    Usage example:
    with TimedInMilliseconds(50) as executed_in_time:
        ... (do potentionally long thing)

     if executed_in_time():
         ... (code that must be excecuted when above code is executed within 50ms.)

    """

    def __init__(self, name: str, threshold_ms: int):
        self.name = name
        self.threshold_ms = threshold_ms
        self.logger = logging.getLogger(__name__)

    def __enter__(self):
        self.start = time.perf_counter() * 1000
        self.end = 0.0

        def _within_threshold():
            duration = self.end - self.start
            within_threshold = self.threshold_ms >= duration

            if not within_threshold:
                self.logger.debug(
                    f"Timed code '{self.name}' took longer than "
                    f"{self.threshold_ms}ms: {duration:.3f}ms"
                )

            return within_threshold

        return _within_threshold

    def __exit__(self, *args):
        self.end = time.perf_counter() * 1000
