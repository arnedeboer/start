# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from sqlalchemy import sql
from zsnl_domains.database import schema


def user_allowed_cases_subquery(
    user_info: minty.cqrs.UserInfo,
    permission: str,
    case_alias=None,
    skip_acl_check: bool = False,
):
    """
    Build a partial query that limits case queries to ones the user has
    access to.

    :param user_info: UserInfo representing the user to check the ACL for
    :param permission: Permission needed to execute the action
    :param case_alias: Alias to the case table to join on. Should be
        used if the main query uses an alias for `schema.Case`.
    :param skip_acl_check: Flag to skip acl_checks.
    :return: Subquery that can be used to restrict case queries to allowed
        cases.
    """
    if case_alias is None:
        case_alias = schema.Case

    acl_checks = []

    if user_info is None and not skip_acl_check:
        raise AttributeError("Required attribute user_info is None")
    elif skip_acl_check or user_info.permissions.get("admin", False):
        # Admins skip ACL checks
        pass
    else:
        user_id = (
            sql.select(schema.Subject.id)
            .where(schema.Subject.uuid == user_info.user_uuid)
            .scalar_subquery()
        )

        acl_checks.append(
            sql.or_(
                case_alias.behandelaar_gm_id == user_id,
                case_alias.coordinator_gm_id == user_id,
                sql.and_(
                    case_alias.aanvrager_gm_id == user_id,
                    case_alias.aanvrager_type == "medewerker",
                ),
                sql.exists(
                    sql.select(1)
                    .select_from(schema.CaseAcl)
                    .where(
                        sql.and_(
                            schema.CaseAcl.case_id == case_alias.id,
                            schema.CaseAcl.casetype_id
                            == case_alias.zaaktype_id,
                            schema.CaseAcl.permission == permission,
                            schema.CaseAcl.subject_uuid == user_info.user_uuid,
                        )
                    )
                ),
            )
        )

    all_checks = [case_alias.deleted.is_(None), case_alias.status != "deleted"]

    if len(acl_checks):
        all_checks.append(sql.or_(*acl_checks))

    return sql.and_(*all_checks)


def is_user_admin_query(user_uuid):
    return sql.select(schema.Subject.uuid).where(
        sql.and_(
            schema.Subject.role_ids.overlap(
                sql.func.array(
                    sql.select(schema.Role.id)
                    .where(
                        schema.Role.name.in_(
                            ["Administrator", "Zaaksysteembeheerder"]
                        )
                    )
                    .scalar_subquery()
                )
            ),
            schema.Subject.uuid == user_uuid,
        )
    )


def _user_is_admin(db, user_uuid):
    is_admin_query = is_user_admin_query(user_uuid)

    result = db.execute(is_admin_query).fetchall()

    if len(result) == 0:
        return False
    else:
        return True


def allowed_cases_subquery(db, user_uuid, permission: str, case_alias=None):
    """***DEPRECATED***
    Build a partial query that limits case queries to ones the user has
    access to.

    :param db: SQLAlchemy session to use for the auxilliary queries
    :type db: session
    :param user_uuid: UUID of the user performing the action
    :type user_uuid: UUID
    :param permission: Permission needed to execute the action
    :type permission: str
    :param case_alias: Alias to the case table to join on. Should be
        used if the main query uses an alias for `schema.Case`.
    :return: Subquery that can be used to restrict case queries to allowed
        cases.
    :rtype: sqlalchemy.sql.expression.Select
    """
    if case_alias is None:
        case_alias = schema.Case

    if _user_is_admin(db, user_uuid):
        return True

    user_id = (
        sql.select(schema.Subject.id)
        .where(schema.Subject.uuid == user_uuid)
        .scalar_subquery()
    )
    return sql.and_(
        case_alias.deleted.is_(None),
        case_alias.status != "deleted",
        sql.or_(
            case_alias.behandelaar_gm_id == user_id,
            case_alias.coordinator_gm_id == user_id,
            sql.and_(
                case_alias.aanvrager_gm_id == user_id,
                case_alias.aanvrager_type == "medewerker",
            ),
            sql.exists(
                sql.select(1)
                .select_from(schema.CaseAcl)
                .where(
                    sql.and_(
                        schema.CaseAcl.case_id == case_alias.id,
                        schema.CaseAcl.casetype_id == case_alias.zaaktype_id,
                        schema.CaseAcl.permission == permission,
                        schema.CaseAcl.subject_uuid == user_uuid,
                    )
                )
            ),
        ),
    )
