# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs import UserInfo
from sqlalchemy import sql
from zsnl_domains.case_management.entities import AuthorizationLevel
from zsnl_domains.database import schema


def allowed_object_v1_subquery(
    object_alias, class_alias, user_info: UserInfo, capabilities=None
):
    """Return a subquery (that can be plugged into a `WHERE`) that filters v1
    objects based on permissions.

    :param object_alias: `schema.ObjectData` alias to use for the object itself
    :param class_alias: `schema .ObjectData` alias to use for the "object type"
    """
    if user_info.permissions.get("admin", False):
        return sql.literal(True)

    if capabilities is None:
        capabilities = ["search", "read"]

    selects = []
    if class_alias is not None:
        # Type-level ACLs
        selects.append(
            sql.select(1)
            .select_from(
                sql.join(
                    schema.ObjectAclEntry,
                    schema.SubjectPositionMatrix,
                    sql.and_(
                        schema.ObjectAclEntry.entity_id
                        == schema.SubjectPositionMatrix.position,
                        schema.ObjectAclEntry.entity_type == "position",
                        schema.ObjectAclEntry.scope == "type",
                        schema.ObjectAclEntry.capability.in_(capabilities),
                        schema.ObjectAclEntry.object_uuid
                        == class_alias.c.uuid,
                    ),
                ).join(
                    schema.Subject,
                    schema.Subject.id
                    == schema.SubjectPositionMatrix.subject_id,
                )
            )
            .where(schema.Subject.uuid == user_info.user_uuid)
        )

    # Instance-level ACLs
    selects.append(
        sql.select(1)
        .select_from(
            sql.join(
                schema.ObjectAclEntry,
                schema.SubjectPositionMatrix,
                sql.and_(
                    schema.ObjectAclEntry.entity_id
                    == schema.SubjectPositionMatrix.position,
                    schema.ObjectAclEntry.entity_type == "position",
                    schema.ObjectAclEntry.scope == "instance",
                    schema.ObjectAclEntry.capability.in_(capabilities),
                    schema.ObjectAclEntry.object_uuid == object_alias.c.uuid,
                ),
            ).join(
                schema.Subject,
                schema.Subject.id == schema.SubjectPositionMatrix.subject_id,
            )
        )
        .where(schema.Subject.uuid == user_info.user_uuid)
    )

    # ACL for entity_type = user and entity_id = subject.username
    selects.append(
        sql.select(1)
        .select_from(
            sql.join(
                schema.ObjectAclEntry,
                schema.Subject,
                sql.and_(
                    schema.ObjectAclEntry.entity_id == schema.Subject.username,
                    schema.ObjectAclEntry.entity_type == "user",
                    schema.ObjectAclEntry.scope == "instance",
                    schema.ObjectAclEntry.capability.in_(capabilities),
                    schema.ObjectAclEntry.object_uuid == object_alias.c.uuid,
                ),
            )
        )
        .where(schema.Subject.uuid == user_info.user_uuid)
    )

    return sql.exists(sql.union_all(*selects))


def allowed_object_v2_subquery(
    user_info: UserInfo,
    authorization: AuthorizationLevel,
    object_alias: sql.expression.Alias,
):
    """Return a subquery (that can be plugged into a `WHERE`) that filters v2
    custom_objects based on authorizations.

    :param user_uuid: UUID of the user
    :param authorization: custom_object authorization level
    :param object_alias: `schema.CustomObject` alias to use for the object itself
    """

    read_authorizations = [
        AuthorizationLevel.read,
        AuthorizationLevel.readwrite,
        AuthorizationLevel.admin,
    ]

    write_authorizations = [
        AuthorizationLevel.readwrite,
        AuthorizationLevel.admin,
    ]

    authorizations = read_authorizations
    if authorization in write_authorizations:
        authorizations = write_authorizations

    return sql.exists(
        sql.select(1)
        .select_from(
            sql.join(
                schema.CustomObjectVersion,
                schema.CustomObject,
                schema.CustomObject.custom_object_version_id
                == schema.CustomObjectVersion.id,
            )
            .join(
                schema.CustomObjectTypeVersion,
                schema.CustomObjectVersion.custom_object_type_version_id
                == schema.CustomObjectTypeVersion.id,
            )
            .join(
                schema.CustomObjectType,
                schema.CustomObjectTypeVersion.custom_object_type_id
                == schema.CustomObjectType.id,
            )
            .join(
                schema.CustomObjectTypeAcl,
                sql.and_(
                    schema.CustomObjectTypeAcl.custom_object_type_id
                    == schema.CustomObjectType.id,
                    schema.CustomObjectTypeAcl.authorization.in_(
                        authorizations
                    ),
                ),
            )
            .join(schema.Subject, schema.Subject.uuid == user_info.user_uuid)
            .join(
                schema.SubjectPositionMatrix,
                sql.and_(
                    schema.SubjectPositionMatrix.role_id
                    == schema.CustomObjectTypeAcl.role_id,
                    schema.SubjectPositionMatrix.group_id
                    == schema.CustomObjectTypeAcl.group_id,
                    schema.SubjectPositionMatrix.subject_id
                    == schema.Subject.id,
                ),
            )
        )
        .where(
            schema.CustomObject.id == object_alias.c.id,
        )
    )
