# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
from pydantic import BaseModel, Field
from pydantic.generics import GenericModel
from pydantic.types import constr
from typing import Generic, Literal, TypedDict, TypeVar, Union
from uuid import UUID

FilterUUIDExplicitEmptyValue = Union[UUID, Literal["\x00"], None]


class ComparisonFilterOperator(enum.StrEnum):
    lt = "lt"
    le = "le"
    gt = "gt"
    ge = "ge"
    eq = "eq"
    ne = "ne"


_comparison_filter_re = r"(" + r"|".join(list(ComparisonFilterOperator)) + r")"

ComparisonFilterConditionStr = constr(
    strip_whitespace=True, regex=rf"^{_comparison_filter_re} "
)

ComparisonT = TypeVar("ComparisonT")


class ComparisonFilterCondition(GenericModel, Generic[ComparisonT]):
    operator: ComparisonFilterOperator
    operand: ComparisonT

    @classmethod
    def from_str(
        cls, s: ComparisonFilterConditionStr
    ) -> "ComparisonFilterCondition[ComparisonT]":
        (operator, operand) = s.split(" ", maxsplit=1)
        return cls(operator=operator, operand=operand)


class CustomObjectAttributesValueFilter(BaseModel):
    type: str = Field(None, alias="type")
    value: str = Field(None, alias="value")
    operator: str = Field(None, alias="operator")
    magic_string: str = Field(None, alias="magic_string")


class FilterOperator(enum.StrEnum):
    or_operator = "or"
    and_operator = "and"


SimpleFilterType = TypeVar("SimpleFilterType")


class FilterMultipleValuesWithOperator(
    GenericModel, Generic[SimpleFilterType]
):
    operator: FilterOperator | None
    values: list[SimpleFilterType]


class DepartmentRoleTypedDict(TypedDict):
    department_uuid: UUID
    role_uuid: UUID
