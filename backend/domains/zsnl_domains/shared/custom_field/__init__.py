# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

"""Zaaksysteem.nl Custom Field handling

This package defines the different custom field types like textarea, date,
currency and handles them correctly. It provides type checking using pydantic
and supports validation (e.g.: is this date in the past or future).

The most import function to use from this package is:

    * get_custom_fields_model

"""
import enum
from .types.address import CustomFieldTypeAddressV2
from .types.checkbox import CustomFieldTypeCheckbox
from .types.currency import CustomFieldCurrency
from .types.date import CustomFieldTypeDate
from .types.document import CustomFieldDocument
from .types.email import CustomFieldEmail
from .types.geojson import CustomFieldTypeGeoJSON
from .types.iban import CustomFieldIBAN
from .types.relationship import CustomFieldTypeRelationship
from .types.select import CustomFieldTypeSelect
from .types.single import (
    CustomFieldNumeric,
    CustomFieldTypeOption,
    CustomFieldTypeRichText,
    CustomFieldTypeText,
    CustomFieldTypeTextarea,
)
from .types.url import CustomFieldURL
from minty import entity
from pydantic import BaseModel, Field, create_model, validator
from typing import Optional
from uuid import UUID

custom_field_types = {
    "address_v2": CustomFieldTypeAddressV2,
    "bankaccount": CustomFieldIBAN,
    "checkbox": CustomFieldTypeCheckbox,
    "date": CustomFieldTypeDate,
    "email": CustomFieldEmail,
    "geojson": CustomFieldTypeGeoJSON,
    "numeric": CustomFieldNumeric,
    "option": CustomFieldTypeOption,
    "relationship": CustomFieldTypeRelationship,
    "richtext": CustomFieldTypeRichText,
    "select": CustomFieldTypeSelect,
    "text": CustomFieldTypeText,
    "textarea": CustomFieldTypeTextarea,
    "url": CustomFieldURL,
    "valuta": CustomFieldCurrency,
    "file": CustomFieldDocument,
}

custom_field_types_with_options = ["option", "select", "checkbox"]


class CustomFieldRelationshipTypes(enum.StrEnum):
    case = "case"
    custom_object = "custom_object"
    document = "document"
    subject = "subject"


class CustomFieldMapSpecification(entity.ValueObject):
    # Valid only for custom_fields of type geojson and address_v2
    # If use_on_map is true, show the geojson value of custom_field in the map of custom_object

    use_on_map: bool | None = Field(
        False,
        title="Use the address_v2 or geolocation on the map of custom_object.",
    )


class CustomFieldRelationshipSpecification(entity.ValueObject):
    type: CustomFieldRelationshipTypes = Field(
        ..., title="Type of this relationship"
    )
    uuid: UUID | None = Field(None, title="UUID of the relationship type")
    name: str | None = Field(None, title="Name of the relationship")

    # Valid only for type 'custom_object'
    use_on_map: bool | None = Field(
        False,
        title="Use the GeoLocation(s) and Addresse(s) of relationship(custom_object) on the map of custom_object.",
    )


class CustomFieldTypes(enum.StrEnum):
    relationship = "relationship"
    text = "text"
    option = "option"
    select = "select"
    textarea = "textarea"
    richtext = "richtext"
    checkbox = "checkbox"
    date = "date"
    geojson = "geojson"
    valuta = "valuta"
    email = "email"
    url = "url"
    bankaccount = "bankaccount"
    address_v2 = "address_v2"
    numeric = "numeric"
    file = "file"


class CustomField(entity.ValueObject):
    """Definition of a object type related custom field"""

    # Free form descriptions
    label: str = Field(..., title="Label of this field")
    name: str = Field(..., title="Name of this field in the catalog")

    description: str = Field(
        None, title="Description of this field for internal purposes"
    )
    external_description: str = Field(
        None, title="Description of this field for public purposes"
    )

    # Flags
    is_required: bool = Field(
        False, title="Indicates whether this field is a required field"
    )
    is_hidden_field: bool = Field(
        False,
        title="Indicates whether this field is a hidden field, or 'system field'",
    )
    multiple_values: bool | None = Field(
        False,
        title="Indicates if multiple values are allowed.",
    )

    # Reference to attribute
    attribute_uuid: UUID | None = Field(
        None, title="UUID referencing the field in our attribute catalog"
    )

    # Multivalue custom fields
    options: list[str] | None = Field(
        None, title="List of possible options for this custom field"
    )

    # Details
    custom_field_type: CustomFieldTypes = Field(
        None, title="Type of this custom field"
    )
    magic_string: str = Field(None, title="The magic string of this field")

    # Specific custom field metadata
    custom_field_specification: None | (
        CustomFieldRelationshipSpecification | CustomFieldMapSpecification
    ) = Field(None, title="Specification around a certain custom field type")

    @validator("custom_field_type")
    def validate_options_for_option_attributes(cls, v, values):
        if (
            not values["options"] or not len(values["options"])
        ) and v in custom_field_types_with_options:
            raise ValueError(
                f"should have at least one option in options for type {v}"
            )

        return v


class CustomFieldDefinition(entity.ValueObject):
    """Custom field definition pointing to configured custom fields"""

    custom_fields: list[CustomField] = Field(
        default_factory=list, title="List of custom fields"
    )


class CustomFieldsModel(BaseModel):
    """Key-Value object of Custom Field values"""

    pass


def get_custom_fields_model(
    definition: CustomFieldDefinition,
) -> CustomFieldsModel:
    """Gets a pydantic CustomFields model to validate values against

    With the given CustomFieldDefinition in the definition parameter, this
    function returns an object to validate your custom field values against.

    Example:
        model = get_custom_fields_model(
            CustomFieldDefinition(
                custom_fields=[
                    CustomField(custom_field_type="date", magic_string="ztc_date")
                ]
            )
        )

        model.parse_obj({
            "ztc_date": {
                "value": "2020-04-01T20:00"
            }
        })

    Args:
        definition (CustomFieldDefinition): Definition of the custom fields

    Returns:
        CustomFieldsModel: an instance of it
    """

    if not isinstance(definition, CustomFieldDefinition):
        raise KeyError(
            "Input parameter schema must be of type 'CustomFieldDefinition'"
        )

    params = {}
    for field in definition.custom_fields:
        fieldname = field.magic_string
        fieldtype = field.custom_field_type
        pytype = custom_field_types[fieldtype]

        validators = pytype.get_validators(field)

        pytype = create_model(
            "CustomFieldValue", __base__=pytype, __validators__=validators
        )

        if field.is_required:
            pytype = pytype
            pydefault = ...
        else:
            pytype = Optional[pytype]
            pydefault = None

        params[fieldname] = (pytype, Field(pydefault, title=field.label))

    return create_model(
        "CustomFieldsModel", __base__=CustomFieldsModel, **params
    )
