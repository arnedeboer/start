# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic import Field
from zsnl_domains.shared.custom_field.types import base as typebase


class CustomFieldIBAN(typebase.CustomFieldValueBase):
    type: str = Field(
        "iban", title="A value formatted as a ISO 13616 IBAN string"
    )
    value: str = Field(
        ...,
        title="The value for this field formatted as a ISO 13616 IBAN string",
    )
