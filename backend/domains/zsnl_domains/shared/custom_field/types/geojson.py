# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import geojson as geojsonmodule
import json
from pydantic import Field
from zsnl_domains.shared.custom_field.types import base as typebase


class GeoJSON(dict):
    """
    Partial GeoJSON validation.
    """

    @classmethod
    def __get_validators__(cls):
        # one or more validators may be yielded which will be called in the
        # order to validate the input, each validator will receive as an input
        # the value returned from the previous validator
        yield cls.validate

    @classmethod
    def __modify_schema__(cls, field_schema):
        # __modify_schema__ should mutate the dict it receives in place,
        # the returned value will be ignored
        field_schema.update(
            # simplified regex here for brevity, see the wikipedia link above
            format="geojson",
            pattern="^\\{.*\\}$",
            examples=[
                '{"type":"Feature","geometry": {"type":"Point","coordinates":[125.6, 10.1]} }'
            ],
        )

    @classmethod
    def validate(cls, v):
        as_json = None
        as_object = None
        if not (isinstance(v, str) or isinstance(v, dict)):
            raise TypeError("string or object required")

        if isinstance(v, str):
            as_json = v
            as_object = json.loads(v)

        if isinstance(v, dict):
            as_object = v
            as_json = json.dumps(v)

        geojson_value = geojsonmodule.loads(as_json)

        if (
            not isinstance(geojson_value, geojsonmodule.GeoJSON)
            or not geojson_value.is_valid
        ):
            raise ValueError("Invalid GeoJSON format")

        return cls(as_object)

    def __repr__(self):
        return f"GeoJSON({super().__repr__()})"


class CustomFieldTypeGeoJSON(typebase.CustomFieldValueBase):
    type: str = Field(
        "geojson",
        const=True,
        title="A value formatted as a GeoJSON formatted object",
    )
    value: GeoJSON | None = Field(
        ..., title="The value for this field formatted as a GeoJSON object"
    )
