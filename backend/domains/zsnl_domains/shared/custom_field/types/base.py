# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from datetime import date
from pydantic import BaseModel, Field, validator
from typing import Any


class CustomFieldValueBase(BaseModel):
    """CustomField data"""

    value: Any = Field(..., title="The value for this custom field")
    type: str = Field(..., title="The Zaaksysteem.nl custom field type name")

    @classmethod
    def get_validators(cls, custom_field):
        return {}

    @classmethod
    def _get_option_validator(cls, custom_field):
        options = custom_field.options

        def check_options(v):
            if v not in options:
                raise ValueError(
                    "must be one of the following options: "
                    + ", ".join(options)
                )

            return v

        return validator("value", allow_reuse=True, each_item=True)(
            check_options
        )

    @classmethod
    def _get_date_validator(cls):
        def check_date(v):
            # return value from fromisofomat() is not needed, we only need a
            # ValidationError if not a correct str value in ISO format.
            date.fromisoformat(v)
            return v

        return validator("value", allow_reuse=True)(check_date)


class CustomFieldValueMultiValueBase(CustomFieldValueBase):
    value: list[str] = Field(
        ..., title="A list of values for this custom field"
    )
