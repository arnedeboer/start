# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.exceptions
import pytest
from email.message import EmailMessage
from minty.cqrs.test import TestBase
from minty_infra_email.infrastructure import EmailConfiguration
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import communication


class Test_AsEmployee_Send_Email_With_MarkUpTemplate(TestBase):
    def setup_method(self):
        self.email_infra = mock.MagicMock()
        self.storage_infra = mock.MagicMock()
        self.load_command_instance(
            domain=communication,
            inframocks={"email": self.email_infra, "s3": self.storage_infra},
        )

        self.external_message_uuid = uuid4()
        self.mock_external_message = mock.MagicMock(
            uuid=self.external_message_uuid,
            type="external",
            thread_uuid=uuid4(),
            case_uuid=uuid4(),
            case_html_email_template="test_template_2",
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            external_message_content="This is the content",
            external_message_subject="Subjective",
            external_message_type="email",
            participants=[
                {
                    "role": "to",
                    "display_name": "Testing 123",
                    "address": "foo@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 234",
                    "address": "foo2@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 345",
                    "address": "foo3@example.com",
                },
                {
                    "role": "to",
                    "display_name": "Me, myself and I",
                    "address": "sender@example.com",
                },
                # This tests the "empty email address" exception
                {"role": "to", "display_name": "Something", "address": ""},
            ],
            attachments=[],
            direction="outgoing",
            original_message_file=None,
        )
        self.mock_get_external_message = mock.MagicMock()
        self.mock_get_external_message.fetchone.return_value = (
            self.mock_external_message
        )

        self.mock_contact = mock.MagicMock(
            id=23, uuid=uuid4(), type="employee", name="beherder"
        )
        self.mock_get_contact = mock.MagicMock()
        self.mock_get_contact.fetchone.return_value = self.mock_contact

        self.mock_email_config = mock.MagicMock(
            interface_config={
                "api_user": "example@example.nl",
                "uuid": None,
                "subject": "MG",
                "use_dkim": 0,
                "notes": None,
                "smarthost_hostname": None,
                "use_smarthost": 0,
                "smarthost_username": None,
                "smarthost_password": None,
                "max_size": "10",
                "sender_name": "Gemeente Midden-Groningen",
                "custom_dkim": None,
                "smarthost_port": "587",
                "dkim_domain": None,
                "rich_email": 1,
                "rich_email_templates": [
                    {
                        "image": None,
                        "template": "<html><body><div>{{message}}</div></body></html>",
                        "label": "test_template_1",
                    },
                    {
                        "image": None,
                        "label": "test_template_2",
                        "template": "<html>\n  <head>\n    <style>\n      h1 { color: red; }\n      div { color: green; }\n      p { color: blue; }\n    </style>\n  </head>\n  <body>\n    <h1>Koptekst2</h1>\n    <div>{{message}}</div>\n    <p>Voettekst</p>\n  </body>\n</html>",
                    },
                ],
            }
        )
        self.mock_get_email_config = mock.MagicMock()
        self.mock_get_email_config.fetchone.return_value = (
            self.mock_email_config
        )

        self.mock_email_config_oauth2 = mock.MagicMock(
            uuid=uuid4(),
            interface_config={
                "api_user": "example@example.nl",
                "uuid": None,
                "subject": "MG",
                "use_dkim": 0,
                "notes": None,
                "smarthost_hostname": None,
                "use_smarthost": 1,
                "kind": "microsoft",
                "smarthost_username": None,
                "smarthost_password": None,
                "max_size": "10",
                "sender_name": "Gemeente Midden-Groningen",
                "custom_dkim": None,
                "smarthost_port": "587",
                "dkim_domain": None,
                "rich_email": 0,
                "rich_email_templates": [],
                "ms_tenant_id": str(uuid4()),
                "ms_client_id": str(uuid4()),
                "ms_client_secret": "ssshhh",
                "ms_username": "user@example.com",
            },
            internal_config={
                "refresh_token": "refresh_token_content",
                "access_token": "access_token_content",
                "access_token_expiration": 1_000_000_090,
            },
        )
        self.mock_get_email_config_oauth2 = mock.MagicMock()
        self.mock_get_email_config_oauth2.fetchone.return_value = (
            self.mock_email_config_oauth2
        )

        self.mock_insert_file = mock.MagicMock()
        self.mock_update_thread = mock.MagicMock()

    @mock.patch("email_validator.validate_email")
    def test_send_email(self, mock_validate_email):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        mock_rows = [
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_get_email_config,
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_insert_file,
            self.mock_update_thread,
        ]

        self.executed_queries = []

        def mock_execute(query):
            self.executed_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.cmd.send_email(message_uuid=str(self.external_message_uuid))

        self.email_infra.send.assert_called_once()
        args, kwargs = self.email_infra.send.call_args
        message = args[0]

        assert isinstance(message, EmailMessage)
        assert (
            message["To"]
            == 'Testing 123 <foo@example.com>, "Me, myself and I" <sender@example.com>'
        )
        assert (
            message["From"] == "Gemeente Midden-Groningen <example@example.nl>"
        )
        assert (
            message["Cc"]
            == "Testing 234 <foo2@example.com>, Testing 345 <foo3@example.com>"
        )
        assert message["Subject"] == "Subjective"

        html = message.get_payload()[1].get_payload()
        assert (
            html.replace("\n", " ")
            == "<html>   <head>     <style>       h1 { color: red; }       div { color: green; }       p { color: blue; }     </style>   </head>   <body>     <h1>Koptekst2</h1>     <div>This is the content</div>     <p>Voettekst</p>   </body> </html> "
        )

    @mock.patch("email_validator.validate_email")
    def test_send_email_oauth2(self, mock_validate_email):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        mock_rows = [
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_get_email_config_oauth2,
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_insert_file,
            self.mock_update_thread,
        ]

        self.executed_queries = []

        def mock_execute(query):
            self.executed_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        with mock.patch("requests.post") as mock_post, mock.patch(
            "time.time"
        ) as mock_time:
            # The "stored" access token, as set up above, expires at
            # 1_000_000_090.
            # The code requires a minimum validity of 60 seconds before it
            # tries to refresh the token.
            # Using 1_000_000_000 + 60s should lead to the existing being used
            # not a refresh.
            mock_time.return_value = 1_000_000_000

            self.cmd.send_email(message_uuid=str(self.external_message_uuid))

        mock_post.assert_not_called()

        self.email_infra.send.assert_called_once()
        args, kwargs = self.email_infra.send.call_args
        message = args[0]
        infra_config = args[1]

        assert isinstance(message, EmailMessage)
        assert infra_config == EmailConfiguration(
            smarthost_mode="oauth2",
            smarthost_username="user@example.com",
            smarthost_password=None,
            smarthost_token="access_token_content",
            smarthost_hostname="smtp.office365.com",
            smarthost_port=587,
            smarthost_security="starttls",
        )

    @mock.patch("email_validator.validate_email")
    def test_send_email_oauth2_with_refresh(self, mock_validate_email):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        mock_update = mock.Mock()
        mock_rows = [
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_get_email_config_oauth2,
            mock_update,
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_insert_file,
            self.mock_update_thread,
        ]

        self.executed_queries = []

        def mock_execute(query):
            self.executed_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        with mock.patch("requests.post") as mock_post, mock.patch(
            "time.time"
        ) as mock_time:
            mock_time.return_value = 2000000000
            mock_response = mock.Mock()
            mock_response.json.return_value = {
                "access_token": "access_token_new",
                "expires_in": 123,
                "refresh_token": "refresh_new",
            }
            mock_post.return_value = mock_response

            self.cmd.send_email(message_uuid=str(self.external_message_uuid))

        tenant_id = self.mock_email_config_oauth2.interface_config[
            "ms_tenant_id"
        ]
        client_id = self.mock_email_config_oauth2.interface_config[
            "ms_client_id"
        ]
        client_secret = self.mock_email_config_oauth2.interface_config[
            "ms_client_secret"
        ]
        mock_post.assert_called_once_with(
            f"https://login.microsoft.com/{tenant_id}/oauth2/v2.0/token",
            {
                "client_id": client_id,
                "client_secret": client_secret,
                "grant_type": "refresh_token",
                "refresh_token": "refresh_token_content",
            },
        )

        self.email_infra.send.assert_called_once()
        args, kwargs = self.email_infra.send.call_args
        message = args[0]
        infra_config = args[1]

        assert isinstance(message, EmailMessage)
        assert infra_config == EmailConfiguration(
            smarthost_mode="oauth2",
            smarthost_username="user@example.com",
            smarthost_password=None,
            smarthost_token="access_token_new",
            smarthost_hostname="smtp.office365.com",
            smarthost_port=587,
            smarthost_security="starttls",
        )

        update_config = self.executed_queries[3]
        compiled = update_config.compile(dialect=postgresql.dialect())
        assert (
            str(compiled)
            == "UPDATE interface SET internal_config=(interface.internal_config || %(internal_config_1)s) WHERE interface.uuid = %(uuid_1)s"
        )
        assert compiled.params == {
            "internal_config_1": {
                "access_token": "access_token_new",
                "access_token_expiration": 2000000123,
                "access_token_failed": 0,
                "refresh_token": "refresh_new",
            },
            "uuid_1": self.mock_email_config_oauth2.uuid,
        }

    @mock.patch("email_validator.validate_email")
    def test_send_email_oauth2_with_refresh_error(self, mock_validate_email):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        mock_update = mock.Mock()
        mock_rows = [
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_get_email_config_oauth2,
            mock_update,
        ]

        self.executed_queries = []

        def mock_execute(query):
            self.executed_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        with mock.patch("requests.post") as mock_post, mock.patch(
            "time.time"
        ) as mock_time, pytest.raises(minty.exceptions.CQRSException):
            mock_time.return_value = 2000000000
            mock_response = mock.Mock()
            mock_response.json.return_value = {
                "error": "yes",
                "error_description": "it's really bad",
            }
            mock_post.return_value = mock_response

            self.cmd.send_email(message_uuid=str(self.external_message_uuid))

        tenant_id = self.mock_email_config_oauth2.interface_config[
            "ms_tenant_id"
        ]
        client_id = self.mock_email_config_oauth2.interface_config[
            "ms_client_id"
        ]
        client_secret = self.mock_email_config_oauth2.interface_config[
            "ms_client_secret"
        ]
        mock_post.assert_called_once_with(
            f"https://login.microsoft.com/{tenant_id}/oauth2/v2.0/token",
            {
                "client_id": client_id,
                "client_secret": client_secret,
                "grant_type": "refresh_token",
                "refresh_token": "refresh_token_content",
            },
        )

        self.email_infra.send.assert_not_called()

        update_config = self.executed_queries[3]
        compiled = update_config.compile(dialect=postgresql.dialect())
        assert (
            str(compiled)
            == "UPDATE interface SET internal_config=(interface.internal_config || %(internal_config_1)s) WHERE interface.uuid = %(uuid_1)s"
        )
        assert compiled.params == {
            "internal_config_1": {"access_token_failed": 1},
            "uuid_1": self.mock_email_config_oauth2.uuid,
        }

    @mock.patch("email_validator.validate_email")
    def test_send_email_when_no_html_template_is_configured_in_casetype(
        self, mock_validate_email
    ):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        self.mock_get_external_message.fetchone.return_value = mock.MagicMock(
            uuid=self.external_message_uuid,
            type="external",
            thread_uuid=uuid4(),
            case_uuid=uuid4(),
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            external_message_content="This is the content",
            external_message_subject="Subjective",
            external_message_type="email",
            participants=[
                {
                    "role": "to",
                    "display_name": "Testing 123",
                    "address": "foo@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 234",
                    "address": "foo2@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 345",
                    "address": "foo3@example.com",
                },
                {
                    "role": "to",
                    "display_name": "Me, myself and I",
                    "address": "sender@example.com",
                },
                # This tests the "empty email address" exception
                {"role": "to", "display_name": "Something", "address": ""},
            ],
            attachments=[],
            direction="outgoing",
            original_message_file=None,
        )

        mock_rows = [
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_get_email_config,
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_insert_file,
            self.mock_update_thread,
        ]

        self.executed_queries = []

        def mock_execute(query):
            self.executed_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.cmd.send_email(message_uuid=str(self.external_message_uuid))

        self.email_infra.send.assert_called_once()
        args, kwargs = self.email_infra.send.call_args
        message = args[0]

        assert isinstance(message, EmailMessage)
        assert (
            message["To"]
            == 'Testing 123 <foo@example.com>, "Me, myself and I" <sender@example.com>'
        )
        assert (
            message["From"] == "Gemeente Midden-Groningen <example@example.nl>"
        )
        assert (
            message["Cc"]
            == "Testing 234 <foo2@example.com>, Testing 345 <foo3@example.com>"
        )
        assert message["Subject"] == "Subjective"

        html = message.get_payload()[1].get_payload()
        assert (
            html.replace("\n", " ")
            == "<html><body><div>This is the content</div></body></html> "
        )

    @mock.patch("email_validator.validate_email")
    def test_send_email_with_image(self, mock_validate_email):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        self.mock_get_email_config.fetchone.return_value = mock.MagicMock(
            interface_config={
                "api_user": "example@example.nl",
                "uuid": None,
                "subject": "MG",
                "use_dkim": 0,
                "notes": None,
                "smarthost_hostname": None,
                "use_smarthost": 0,
                "smarthost_username": None,
                "smarthost_password": None,
                "max_size": "10",
                "sender_name": "Gemeente Midden-Groningen",
                "custom_dkim": None,
                "smarthost_port": "587",
                "dkim_domain": None,
                "rich_email": 1,
                "rich_email_templates": [
                    {
                        "image": None,
                        "template": "<html><body><div>{{message}}</div></body></html>",
                        "label": "test_template_1",
                    },
                    {
                        "image": [
                            {
                                "id": 123,
                                "mimetype": "image/png",
                                "is_archivable": 1,
                                "size": 500,
                                "uuid": "1f22e9d4-93a3-4f62-977f-804b5fec9686",
                                "date_created": "2021-01-06 11:04:57.481472",
                                "storage_location": ["minio"],
                                "md5": "b75459d54279b5b7a5c777d01d33a8",
                                "original_name": "test_image.png",
                                "virus_scan_status": "pending",
                                "thumbnail_uuid": None,
                            }
                        ],
                        "label": "test_template_2",
                        "template": '<html>\n  <head>\n    <style>\n      h1 { color: red; }\n      div { color: green; }\n      p { color: blue; }\n    </style>\n  </head>\n  <body>\n    <h1>Koptekst2</h1>\n    <div>{{message}}</div>\n    <p>Voettekst</p>\n  <img src="{{image_url}}" alt="logo">\n  </body>\n</html>',
                    },
                ],
            }
        )
        mock_rows = [
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_get_email_config,
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_insert_file,
            self.mock_update_thread,
        ]

        self.executed_queries = []

        def mock_execute(query):
            self.executed_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.cmd.send_email(message_uuid=str(self.external_message_uuid))

        self.email_infra.send.assert_called_once()
        args, kwargs = self.email_infra.send.call_args
        message = args[0]

        assert isinstance(message, EmailMessage)
        assert (
            message["To"]
            == 'Testing 123 <foo@example.com>, "Me, myself and I" <sender@example.com>'
        )
        assert (
            message["From"] == "Gemeente Midden-Groningen <example@example.nl>"
        )
        assert (
            message["Cc"]
            == "Testing 234 <foo2@example.com>, Testing 345 <foo3@example.com>"
        )
        assert message["Subject"] == "Subjective"

        html = message.get_payload()[1].get_payload()[0].get_payload()
        assert (
            html.replace("\n", " ")
            == '<html>   <head>     <style>       h1 { color: red; }       div { color: green; }       p { color: blue; }     </style>   </head>   <body>     <h1>Koptekst2</h1>     <div>This is the content</div>     <p>Voettekst</p>   <img src="cid:1f22e9d4-93a3-4f62-977f-804b5fec9686" alt="logo">   </body> </html> '
        )
        self.storage_infra.download_file.assert_called_once()
        args, kwargs = self.storage_infra.download_file.call_args
        assert kwargs["file_uuid"] == "1f22e9d4-93a3-4f62-977f-804b5fec9686"

    @mock.patch("email_validator.validate_email")
    def test_send_email_when_email_configuration_has_no_rich_templates(
        self, mock_validate_email
    ):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        self.mock_get_email_config.fetchone.return_value = mock.MagicMock(
            interface_config={
                "api_user": "example@example.nl",
                "uuid": None,
                "subject": "MG",
                "use_dkim": 0,
                "notes": None,
                "smarthost_hostname": None,
                "use_smarthost": 0,
                "smarthost_username": None,
                "smarthost_password": None,
                "max_size": "10",
                "sender_name": "Gemeente Midden-Groningen",
                "custom_dkim": None,
                "smarthost_port": "587",
                "dkim_domain": None,
                "rich_email": 1,
                "rich_email_templates": [],
            }
        )
        mock_rows = [
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_get_email_config,
            self.mock_get_external_message,
            self.mock_get_contact,
            self.mock_insert_file,
            self.mock_update_thread,
        ]

        self.executed_queries = []

        def mock_execute(query):
            self.executed_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.cmd.send_email(message_uuid=str(self.external_message_uuid))

        self.email_infra.send.assert_called_once()
        args, kwargs = self.email_infra.send.call_args
        message = args[0]

        assert isinstance(message, EmailMessage)
        assert (
            message["To"]
            == 'Testing 123 <foo@example.com>, "Me, myself and I" <sender@example.com>'
        )
        assert (
            message["From"] == "Gemeente Midden-Groningen <example@example.nl>"
        )
        assert (
            message["Cc"]
            == "Testing 234 <foo2@example.com>, Testing 345 <foo3@example.com>"
        )
        assert message["Subject"] == "Subjective"

        message_content = message.get_payload()
        assert message_content.replace("\n", "") == "This is the content"
