# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import datetime
from minty.exceptions import Conflict
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import Contact, ExternalMessage


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestExternalMessageEntity:
    message_id = 123
    uuid = uuid4()
    thread_uuid = uuid4()
    case_uuid = uuid4()
    contact_uuid = uuid4()
    created_by_uuid = uuid4()
    thread_type = "employee"
    contact_name = "admin"
    subject = "email"
    display_name = "admin"
    created = last_modified = datetime.now().isoformat()
    last_message = {"slug": "test test test"}
    type = "external"
    message_slug = "contact message slug"
    content = "content"
    external_message_type = "pip"
    contact = Contact(uuid=contact_uuid, name=contact_name)
    created_by = Contact(uuid=created_by_uuid, name=contact_name)
    attachments = [1, 2]
    participants = {}
    original_message_file = uuid4()
    attachment_count = 2

    def setup_method(self):
        self.external_message = ExternalMessage(
            uuid=self.uuid,
            thread_uuid=self.thread_uuid,
            case_uuid=self.case_uuid,
            message_slug=self.message_slug,
            created_date=self.created,
            last_modified=self.last_modified,
            content=self.content,
            subject=self.subject,
            external_message_type=self.external_message_type,
            created_by=self.created_by,
            created_by_displayname="test created by",
            attachments=self.attachments,
            participants=self.participants,
            direction="unspecified",
            original_message_file=self.original_message_file,
            read_pip=None,
            read_employee=None,
            attachment_count=self.attachment_count,
        )
        self.external_message.event_service = mock.MagicMock()

    def test_contact_initialisation(self):
        assert self.external_message.uuid == self.uuid
        assert self.external_message.thread_uuid == self.thread_uuid
        assert self.external_message.case_uuid == self.case_uuid
        assert self.external_message.entity_id == self.uuid
        assert self.external_message.message_type == self.type

        assert self.external_message.created_by.uuid == self.created_by_uuid
        assert self.external_message.created_by.name == self.contact_name

        assert self.external_message.created_date == self.created
        assert self.external_message.last_modified == self.last_modified

        assert self.external_message.message_slug == self.message_slug
        assert self.external_message.content == self.content
        assert self.external_message.attachments == self.attachments
        assert (
            self.external_message.original_message_file
            == self.original_message_file
        )

    def test_create(self):
        thread_uuid = uuid4()
        created = Contact(uuid=uuid4(), name="test created by")
        original_message_file = uuid4()
        self.external_message.create(
            thread_uuid=thread_uuid,
            created_by=created,
            content="new content",
            subject="subject",
            external_message_type="pip",
            attachments=[1, 2],
            participants=[],
            direction="unspecified",
            original_message_file=original_message_file,
        )

        assert self.external_message.thread_uuid == thread_uuid
        assert self.external_message.created_by_displayname == created.name

        assert self.external_message.message_slug == "new content"
        assert self.external_message.content == "new content"
        assert self.external_message.subject == "subject"
        assert self.external_message.external_message_type == "pip"
        assert self.external_message.attachments == [1, 2]
        assert self.external_message.participants == []
        assert self.external_message.direction == "unspecified"
        assert (
            self.external_message.original_message_file
            == original_message_file
        )

    def test_delete(self):
        self.external_message.delete()
        self.external_message.event_service.log_event.assert_called_with(
            entity_type=self.external_message.__class__.__name__,
            entity_id=self.external_message.entity_id,
            event_name="MessageDeleted",
            changes=[],
            entity_data={
                "thread_uuid": str(self.external_message.thread_uuid),
                "case_uuid": str(self.external_message.case_uuid),
                "message_type": "external",
                "external_message_type": self.external_message.external_message_type,
            },
        )

    def test_send(self):
        self.external_message.send()

        self.external_message.event_service.log_event.assert_called_once_with(
            changes=[],
            entity_data={},
            entity_id=self.uuid,
            entity_type="ExternalMessage",
            event_name="ExternalMessageSent",
        )

    def test_mark_read(self):
        timestamp = datetime.now()

        self.external_message.mark_read(context="pip", timestamp=timestamp)
        assert self.external_message.read_pip == timestamp

        with pytest.raises(Conflict) as excinfo:
            self.external_message.mark_read(context="pip", timestamp=timestamp)
        assert excinfo.value.args == (
            f"External message with uuid:'{self.external_message.uuid}' is already read",
            "external_message/already_read",
        )

        self.external_message.mark_read(
            context="employee", timestamp=timestamp
        )
        assert self.external_message.read_employee == timestamp

        with pytest.raises(Conflict) as excinfo:
            self.external_message.mark_read(
                context="employee", timestamp=timestamp
            )
        assert excinfo.value.args == (
            f"External message with uuid:'{self.external_message.uuid}' is already read",
            "external_message/already_read",
        )

    def test_mark_unread_as_employee_sets_read_employee_to_none(self):
        self.external_message.read_employee = datetime.now()
        self.external_message.mark_unread(context="employee")
        assert self.external_message.read_employee is None

    def test_mark_unread_as_pip_sets_read_pip_to_none(self):
        self.external_message.read_pip = datetime.now()
        self.external_message.mark_unread(context="pip")
        assert self.external_message.read_employee is None

    def test_mark_unread_twice_as_employee_raises_conflict(self):
        self.external_message.read_employee = None

        with pytest.raises(Conflict) as exception_info:
            self.external_message.mark_unread(context="employee")

        assert exception_info.value.args == (
            f"External message with uuid '{self.uuid}' is already unread",
            "external_message/already_unread",
        )

    def test_mark_unread_twice_as_pip_raises_conflict(self):
        self.external_message.read_pip = None

        with pytest.raises(Conflict) as exception_info:
            self.external_message.mark_unread(context="pip")

        assert exception_info.value.args == (
            f"External message with uuid '{self.uuid}' is already unread",
            "external_message/already_unread",
        )

    def test_increment_attachment_count(self):
        self.external_message.increment_attachment_count()
        assert self.external_message.attachment_count == 3
