# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.cqrs.test import TestBase
from unittest import mock
from unittest.mock import patch
from uuid import uuid4
from zsnl_domains import communication
from zsnl_domains.communication.entities import ContactMomentOverview


class Test_AsUser_GetListOfContactMoment(TestBase):
    def setup_method(self):
        self.load_query_instance(communication)

    @patch(
        "zsnl_domains.communication.repositories.contact_moment_overview.ContactMomentOverviewRepository._get_contact_type"
    )
    def test_get_contact_moment_list(self, mock_contact):
        uuid1 = uuid4()
        uuid2 = uuid4()
        contact_uuid1 = uuid4()
        thread_uuid1 = uuid4()
        contact_uuid2 = uuid4()
        thread_uuid2 = uuid4()
        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )
        mock_contact.return_value = "organization"

        mock_contact_moment_list = [mock.Mock(), mock.Mock()]

        mock_contact_moment_list[0].configure_mock(
            contact_uuid=contact_uuid1,
            contact="testOrg",
            case_id=20,
            direction="outgoing",
            uuid=uuid1,
            created=created1,
            summary="Test organization.",
            channel="mail",
            thread_uuid=thread_uuid1,
            contact_type="organization",
        )
        mock_contact_moment_list[1].configure_mock(
            contact_uuid=contact_uuid2,
            contact="testOrg1",
            case_id=21,
            direction="incoming",
            uuid=uuid2,
            created=created2,
            summary="Test organization1.",
            channel="post",
            thread_uuid=thread_uuid2,
            contact_type="organization",
        )

        self.session.execute().fetchall.return_value = mock_contact_moment_list
        mock_contact.return_value = "organization"
        contact_moment_list = self.qry.get_contact_moment_list()

        assert isinstance(contact_moment_list[0], ContactMomentOverview)

        contact_moments = list(contact_moment_list)
        assert len(contact_moments) == 2

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query)
            == "SELECT thread.contact_uuid, thread.contact_displayname AS contact, thread.case_id, thread_message_contact_moment.direction, thread_message.uuid, thread.created, thread_message_contact_moment.content AS summary, thread_message_contact_moment.contact_channel AS channel, thread.uuid AS thread_uuid \n"
            "FROM thread_message_contact_moment JOIN (thread JOIN thread_message ON thread.id = thread_message.thread_id) ON thread_message.thread_message_contact_moment_id = thread_message_contact_moment.id ORDER BY thread.created DESC\n"
            " LIMIT :param_1"
        )
