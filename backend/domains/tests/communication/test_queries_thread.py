# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import minty.cqrs
import minty.cqrs.test
import minty.exceptions
import pytest
import random
import uuid
from sqlalchemy.dialects import postgresql
from unittest import mock
from zsnl_domains import communication


@pytest.fixture
def mock_thread():
    thread_type = random.choice(["email", "contact_moment", "note"])
    message_type = "email" if thread_type == "email" else thread_type

    thread_row = mock.Mock(name="Mock Thread")
    thread_row.configure_mock(
        id=random.randint(1, 100),
        uuid=uuid.uuid4(),
        contact_uuid=uuid.uuid4(),
        contact_displayname="Piet Friet",
        case_id=random.randint(101, 200),
        thread_type=thread_type,
        created=datetime.datetime.now(tz=datetime.UTC),
        last_modified=datetime.datetime.now(tz=datetime.UTC),
        last_message_cache={"message_type": message_type},
        message_count=random.randint(1, 10),
        unread_pip_count=random.randint(1, 10),
        unread_employee_count=random.randint(1, 10),
        case_uuid=uuid.uuid4(),
        case_status=random.choice(["open", "new"]),
        case_description="Foobar",
        case_description_public="Foobarz",
        case_type_name="Blabla",
        attachment_count=random.randint(50, 100),
    )

    return thread_row


class Test_Get_Thread_List(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_query_instance(communication)

    def test_get_thread_list_global_intake(self, mock_thread):
        self.session.execute().fetchall.return_value = [mock_thread]
        self.session.reset_mock()

        self.qry.get_thread_list(
            contact_uuid="\0",
            case_uuid="\0",
            message_types="email,contact_moment",
            page=3,
            page_size=50,
            keyword=None,
        )

        # session.execute.call_args_list[0] is a query to retrieve the user's roles
        thread_query = self.session.execute.call_args_list[1][0][0]

        compiled_query = thread_query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_query) == (
            "SELECT thread.id, thread.uuid, thread.contact_uuid, thread.contact_displayname, thread.case_id, thread.thread_type, thread.created, thread.last_modified, thread.last_message_cache, thread.message_count, thread.unread_pip_count, thread.unread_employee_count, zaak.uuid AS case_uuid, zaak.status AS case_status, zaak.onderwerp AS case_description, zaak.onderwerp_extern AS case_description_public, zaaktype_node.titel AS case_type_name, thread.attachment_count \n"
            "FROM thread LEFT OUTER JOIN (zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id) ON thread.case_id = zaak.id \n"
            "WHERE thread.case_id IS NULL AND thread.contact_uuid IS NULL AND (CAST(thread.last_message_cache AS JSON) ->> %(param_1)s) IN (%(param_2_1)s, %(param_2_2)s) ORDER BY CAST(CAST(thread.last_message_cache AS JSON) ->> %(param_3)s AS TIMESTAMP WITH TIME ZONE) DESC \n"
            " LIMIT %(param_4)s OFFSET %(param_5)s"
        )

    def test_get_thread_list_global_intake_default_message_types(
        self, mock_thread
    ):
        self.session.execute().fetchall.return_value = [mock_thread]
        self.session.reset_mock()

        self.qry.get_thread_list(
            contact_uuid="\0",
            case_uuid="\0",
            message_types=None,
            page=3,
            page_size=50,
            keyword=None,
        )

        # session.execute.call_args_list[0] is a query to retrieve the user's roles
        thread_query = self.session.execute.call_args_list[1][0][0]

        compiled_query = thread_query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_query) == (
            "SELECT thread.id, thread.uuid, thread.contact_uuid, thread.contact_displayname, thread.case_id, thread.thread_type, thread.created, thread.last_modified, thread.last_message_cache, thread.message_count, thread.unread_pip_count, thread.unread_employee_count, zaak.uuid AS case_uuid, zaak.status AS case_status, zaak.onderwerp AS case_description, zaak.onderwerp_extern AS case_description_public, zaaktype_node.titel AS case_type_name, thread.attachment_count \n"
            "FROM thread LEFT OUTER JOIN (zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id) ON thread.case_id = zaak.id \n"
            "WHERE thread.case_id IS NULL AND thread.contact_uuid IS NULL ORDER BY CAST(CAST(thread.last_message_cache AS JSON) ->> %(param_1)s AS TIMESTAMP WITH TIME ZONE) DESC \n"
            " LIMIT %(param_2)s OFFSET %(param_3)s"
        )

    def test_get_thread_list_global_intake_invalid_message_type(
        self, mock_thread
    ):
        self.session.execute().fetchall.return_value = [mock_thread]
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.Conflict):
            self.qry.get_thread_list(
                contact_uuid="\0",
                case_uuid="\0",
                message_types="invalid",
                page=3,
                page_size=50,
                keyword=None,
            )

    def test_get_thread_list_global_intake_with_keyword(self, mock_thread):
        self.session.execute().fetchall.return_value = [mock_thread]
        self.session.reset_mock()

        self.qry.get_thread_list(
            contact_uuid="\0",
            case_uuid="\0",
            message_types="email,contact_moment",
            page=3,
            page_size=50,
            keyword="key",
        )

        # session.execute.call_args_list[0] is a query to retrieve the user's roles
        thread_query = self.session.execute.call_args_list[1][0][0]

        compiled_query = thread_query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_query) == (
            "SELECT thread.id, thread.uuid, thread.contact_uuid, thread.contact_displayname, thread.case_id, thread.thread_type, thread.created, thread.last_modified, thread.last_message_cache, thread.message_count, thread.unread_pip_count, thread.unread_employee_count, zaak.uuid AS case_uuid, zaak.status AS case_status, zaak.onderwerp AS case_description, zaak.onderwerp_extern AS case_description_public, zaaktype_node.titel AS case_type_name, thread.attachment_count \n"
            "FROM thread LEFT OUTER JOIN (zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id) ON thread.case_id = zaak.id \n"
            "WHERE thread.case_id IS NULL AND thread.contact_uuid IS NULL AND (CAST(thread.last_message_cache AS JSON) ->> %(param_1)s) IN (%(param_2_1)s, %(param_2_2)s) AND (lower(CAST(thread.last_message_cache AS JSON) ->> %(param_3)s) LIKE lower(%(lower_1)s) ESCAPE '~' OR lower(CAST(thread.last_message_cache AS JSON) ->> %(param_4)s) LIKE lower(%(lower_2)s) ESCAPE '~' OR lower(CAST(thread.last_message_cache AS JSON) ->> %(param_5)s) LIKE lower(%(lower_3)s) ESCAPE '~' OR lower(CAST(thread.last_message_cache AS JSON) ->> %(param_6)s) LIKE lower(%(lower_4)s) ESCAPE '~') ORDER BY CAST(CAST(thread.last_message_cache AS JSON) ->> %(param_7)s AS TIMESTAMP WITH TIME ZONE) DESC \n"
            " LIMIT %(param_8)s OFFSET %(param_9)s"
        )
