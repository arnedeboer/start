# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import pytest
from collections import namedtuple
from minty.cqrs.test import TestBase
from minty.exceptions import Forbidden, NotFound, ValidationError
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import communication
from zsnl_domains.communication import Queries


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name]


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestQueryClass:
    def setup_method(self):
        mock_repo_factory = mock.MagicMock()

        self.user_uuid = uuid4()
        self.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid, permissions={"admin": True}
        )

        self.repo_factory = mock_repo_factory
        self.query = Queries(
            repository_factory=self.repo_factory,
            context=None,
            user_uuid=self.user_uuid,
        )
        self.query.user_info = self.user_info

    def test_search_contact(self):
        user_uuid = uuid4()
        mock_repo_factory = mock.MagicMock()
        mock_repo_factory.get_repository().search_contact.return_value = [
            "test"
        ]
        mock_repo_factory.reset_mock(return_value=False)

        query = Queries(
            repository_factory=mock_repo_factory,
            context=None,
            user_uuid=user_uuid,
        )

        result = query.search_contact(
            keyword="search keyword", type_filter=["employee", "person"]
        )

        assert result == ["test"]

        mock_repo_factory.get_repository.assert_called_once_with(
            name="contact", context=None, event_service=None
        )
        mock_repo_factory.get_repository().search_contact.assert_called_once_with(
            keyword="search keyword", type_filter={"employee", "person"}
        )

    def test_get_message_list(self):
        user_uuid = uuid4()
        mock_repo_factory = mock.MagicMock()
        mock_repo_factory.get_repository(
            "message"
        ).get_messages_by_thread_uuid.return_value = ["test"]

        user_info = mock.MagicMock()
        user_info.permissions = {}

        mock_repo_factory.get_repository(
            "case"
        ).find_case_by_thread_uuid.return_value = None

        query = Queries(
            repository_factory=mock_repo_factory,
            context=None,
            user_uuid=user_uuid,
        )
        query.user_info = user_info

        uuid = str(uuid4())
        page = 1
        page_size = 10
        result = query.get_message_list(
            thread_uuid=uuid, page=page, page_size=page_size
        )

        assert result == {"messages": ["test"], "case": None}

        mock_repo_factory.get_repository.assert_called_with(
            name="message", context=None, event_service=None
        )
        mock_repo_factory.get_repository(
            "message"
        ).get_messages_by_thread_uuid.assert_called_once_with(
            uuid=uuid,
            user_info=user_info,
            case_id=None,
            page=page,
            page_size=page_size,
        )

        mock_repo_factory.get_repository(
            "case"
        ).find_case_by_thread_uuid.assert_called_once_with(
            thread_uuid=uuid, user_uuid=user_uuid, user_info=user_info
        )

    def test_get_message_list_pip_user(self):
        user_uuid = uuid4()
        case_uud = uuid4()
        case = namedtuple("case", "id uuid")

        case_row = case(id=3, uuid=case_uud)
        mock_repo_factory = mock.MagicMock()
        mock_repo_factory.get_repository(
            "message"
        ).get_messages_by_thread_uuid.return_value = ["test"]

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}

        mock_repo_factory.get_repository(
            "case"
        ).find_case_by_thread_uuid.return_value = case_row

        query = Queries(
            repository_factory=mock_repo_factory,
            context=None,
            user_uuid=user_uuid,
        )
        query.user_info = user_info

        uuid = str(uuid4())
        page = 1
        page_size = 10

        result = query.get_message_list(
            thread_uuid=uuid, page=page, page_size=page_size
        )

        assert result == {"messages": ["test"], "case": case_row}

        mock_repo_factory.get_repository.assert_called_with(
            name="message", context=None, event_service=None
        )
        mock_repo_factory.get_repository(
            "message"
        ).get_messages_by_thread_uuid.assert_called_once_with(
            uuid=uuid,
            user_info=user_info,
            case_id=case_row.id,
            page=page,
            page_size=page_size,
        )

        mock_repo_factory.get_repository(
            "case"
        ).find_case_by_thread_uuid.assert_called_once_with(
            thread_uuid=uuid, user_uuid=user_uuid, user_info=user_info
        )

    def test_search_case(self):
        user_uuid = uuid4()
        mock_repo_factory = mock.MagicMock()
        mock_repo_factory.get_repository().search_cases.return_value = ["test"]
        mock_repo_factory.reset_mock(return_value=False)

        query = Queries(
            repository_factory=mock_repo_factory,
            context=None,
            user_uuid=user_uuid,
        )

        result = query.search_cases(
            search_term="search keyword",
            permission="read",
            case_status_filter=["open", "new", "stalled"],
            limit=42,
        )

        assert result == ["test"]

        mock_repo_factory.get_repository.assert_called_once_with(
            name="case", context=None, event_service=None
        )
        mock_repo_factory.get_repository().search_cases.assert_called_once_with(
            search_term="search keyword",
            user_uuid=user_uuid,
            permission="read",
            case_status_filter=["open", "new", "stalled"],
            limit=42,
        )

    def test_search_case_conflict_wrong_statuses(self):
        user_uuid = uuid4()
        mock_repo_factory = mock.MagicMock()
        # mock_repo_factory.get_repository().search_cases.return_value = Conflict
        mock_repo_factory.reset_mock(return_value=False)

        query = Queries(
            repository_factory=mock_repo_factory,
            context=None,
            user_uuid=user_uuid,
        )
        with pytest.raises(ValidationError):
            query.search_cases(
                search_term="search keyword",
                permission="read",
                case_status_filter=["unknown"],
                limit=None,
            )

    def test_get_case_list_for_contact(self):
        user_uuid = uuid4()

        mock_repo_factory = mock.MagicMock()
        mock_repo_factory.get_repository().get_case_list_for_contact.return_value = [
            "test"
        ]
        mock_repo_factory.reset_mock(return_value=False)

        query = Queries(
            repository_factory=mock_repo_factory,
            context=None,
            user_uuid=user_uuid,
        )

        query.user_info = minty.cqrs.UserInfo(
            user_uuid=user_uuid, permissions={"pip_user": True}
        )

        contact_uuid = str(uuid4())
        with pytest.raises(Forbidden):
            query.get_case_list_for_contact(contact_uuid=contact_uuid)

        res = query.get_case_list_for_contact(contact_uuid=str(user_uuid))
        assert res == ["test"]

        mock_repo_factory.get_repository().get_case_list_for_contact.assert_called_once_with(
            is_pip_user=True,
            user_uuid=user_uuid,
            contact_uuid=user_uuid,
        )

    def test_get_case_list_for_contact_pip_false(self):
        user_uuid = uuid4()

        mock_repo_factory = mock.MagicMock()
        mock_repo_factory.get_repository().get_case_list_for_contact.return_value = [
            "test"
        ]
        mock_repo_factory.reset_mock(return_value=False)

        query = Queries(
            repository_factory=mock_repo_factory,
            context=None,
            user_uuid=user_uuid,
        )

        query.user_info = minty.cqrs.UserInfo(
            user_uuid=user_uuid, permissions={"pip_user": False}
        )

        contact_uuid = uuid4()
        res = query.get_case_list_for_contact(contact_uuid=str(contact_uuid))
        assert res == ["test"]

        mock_repo_factory.get_repository().get_case_list_for_contact.assert_called_once_with(
            is_pip_user=False,
            user_uuid=user_uuid,
            contact_uuid=contact_uuid,
        )

        mock_repo_factory.get_repository().get_case_list_for_contact.reset_mock(
            return_value=False
        )

        res = query.get_case_list_for_contact(contact_uuid=str(user_uuid))
        assert res == ["test"]

        mock_repo_factory.get_repository().get_case_list_for_contact.assert_called_once_with(
            is_pip_user=False,
            user_uuid=user_uuid,
            contact_uuid=user_uuid,
        )

    def test_get_download_link(self):
        self.repo_factory.reset()

        self.repo_factory.get_repository().generate_download_url.return_value = (
            "https://some_url_to_download_a_file.nl"
        )
        self.repo_factory.get_repository().get_file_by_uuid.return_value = (
            "file"
        )

        att_uuid = str(uuid4())
        result = self.query.get_download_link(attachment_uuid=att_uuid)

        assert result == "https://some_url_to_download_a_file.nl"

        self.repo_factory.get_repository().get_file_by_uuid.assert_called_once_with(
            attachment_uuid=att_uuid,
            user_uuid=self.user_uuid,
            is_pip_user=False,
        )
        self.repo_factory.get_repository().generate_download_url.assert_called_once_with(
            "file"
        )


class Test_as_user_I_want_to_see_preview_for_an_attachment(TestBase):
    """As a user user I want to see preview for an attachment"""

    def setup_method(self):
        inframocks = {"s3": mock.MagicMock()}
        self.load_query_instance(communication, inframocks)

        self.attachment_uuid = str(uuid4())
        # self.user_uuid = str(uuid4())

        self.attachment = namedtuple(
            "AttachedFile",
            [
                "uuid",
                "original_name",
                "size",
                "mimetype",
                "md5",
                "date_created",
                "storage_location",
                "attachment_uuid",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
            ],
        )(
            uuid=uuid4(),
            original_name="testdoc",
            size="123",
            mimetype="application/pdf",
            md5="123",
            date_created="2019-05-02",
            storage_location=["Minio"],
            attachment_uuid=uuid4(),
            preview_uuid=uuid4(),
            preview_mimetype="application/pdf",
            preview_storage_location=["Minio"],
        )
        # self.mock_infra = MockInfrastructureFactory(
        #     mock_infra=self.mock_infrastructures
        # )

        # repo_factory = RepositoryFactory(infra_factory=self.mock_infra)
        # repo_factory.register_repository("attachment", AttachmentRepository)

        # self.query_instance = Queries(
        #     repository_factory=repo_factory,
        #     context="testcontext",
        #     user_uuid=self.user_uuid,
        # )

        self.s3 = inframocks["s3"]
        user_info = mock.MagicMock()
        user_info.uuid = uuid4()

        user_info.permissions = {"pip_user": True}
        self.qry.user_info = user_info

    def test_get_preview_url_for_attachment_as_pip_user(self):
        self.session.execute().fetchone.return_value = self.attachment

        self.s3.get_download_url.return_value = (
            "https://example.com/example.txt"
        )

        self.session.reset_mock()

        result = self.qry.get_preview_link(
            attachment_uuid=str(self.attachment_uuid)
        )
        db_execute_calls = self.session.execute.call_args_list

        # From the first (only) execute(), get the 0th element of the "args"
        select_statement = db_execute_calls[0][0][0]

        compiled = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert len(db_execute_calls) == 1
        assert result == "https://example.com/example.txt"
        assert (
            str(compiled)
            == "SELECT filestore.uuid, thread_message_attachment.filename AS original_name, filestore.size, filestore.mimetype, filestore.md5, filestore.date_created, filestore.storage_location, thread_message_attachment.uuid AS attachment_uuid, filestore_1.uuid AS preview_uuid, filestore_1.mimetype AS preview_mimetype, filestore_1.storage_location AS preview_storage_location, filestore_1.original_name AS preview_filename \n"
            "FROM filestore JOIN thread_message_attachment ON filestore.id = thread_message_attachment.filestore_id LEFT OUTER JOIN thread_message_attachment_derivative ON thread_message_attachment_derivative.thread_message_attachment_id = thread_message_attachment.id AND thread_message_attachment_derivative.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s LEFT OUTER JOIN filestore AS filestore_1 ON filestore_1.id = thread_message_attachment_derivative.filestore_id JOIN thread_message ON thread_message.id = thread_message_attachment.thread_message_id JOIN thread ON thread_message.thread_id = thread.id LEFT OUTER JOIN zaak ON zaak.id = thread.case_id \n"
            "WHERE thread_message_attachment.uuid = %(uuid_1)s AND thread.thread_type = %(thread_type_1)s AND zaak.uuid IN (SELECT zaak.uuid \n"
            "FROM zaak_betrokkenen JOIN zaak ON zaak.id = zaak_betrokkenen.zaak_id JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id \n"
            "WHERE zaak_betrokkenen.subject_id = %(subject_id_1)s AND zaak_betrokkenen.deleted IS NULL AND zaak.deleted IS NULL AND zaak.status IN (%(status_1_1)s, %(status_1_2)s, %(status_1_3)s, %(status_1_4)s) AND zaaktype_node.prevent_pip IS NOT true AND (zaak_betrokkenen.pip_authorized IS true OR zaak.aanvrager = zaak_betrokkenen.id))"
        )

    def test_get_preview_url_for_attachment_as_an_employee(self):
        self.qry.user_info.permissions["pip_user"] = False
        self.session.execute().fetchone.return_value = self.attachment

        self.s3.get_download_url.return_value = (
            "https://example.com/example.txt"
        )

        self.session.reset_mock()

        result = self.qry.get_preview_link(
            attachment_uuid=str(self.attachment_uuid)
        )
        db_execute_calls = self.session.execute.call_args_list

        select_statement_1 = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_2 = db_execute_calls[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 2
        assert result == "https://example.com/example.txt"
        assert str(select_statement_1) == (
            "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert (
            str(select_statement_2)
            == "SELECT filestore.uuid, thread_message_attachment.filename AS original_name, filestore.size, filestore.mimetype, filestore.md5, filestore.date_created, filestore.storage_location, thread_message_attachment.uuid AS attachment_uuid, filestore_1.uuid AS preview_uuid, filestore_1.mimetype AS preview_mimetype, filestore_1.storage_location AS preview_storage_location, filestore_1.original_name AS preview_filename \n"
            "FROM filestore JOIN thread_message_attachment ON filestore.id = thread_message_attachment.filestore_id LEFT OUTER JOIN thread_message_attachment_derivative ON thread_message_attachment_derivative.thread_message_attachment_id = thread_message_attachment.id AND thread_message_attachment_derivative.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s LEFT OUTER JOIN filestore AS filestore_1 ON filestore_1.id = thread_message_attachment_derivative.filestore_id JOIN thread_message ON thread_message.id = thread_message_attachment.thread_message_id JOIN thread ON thread_message.thread_id = thread.id LEFT OUTER JOIN zaak ON zaak.id = thread.case_id \n"
            "WHERE thread_message_attachment.uuid = %(uuid_1)s AND (thread.case_id IS NULL OR zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))))"
        )

    def test_get_preview_url_for_an_attachment_which_is_not_of_type_pdf(self):
        self.session.execute().fetchone.return_value = namedtuple(
            "AttachedFile",
            [
                "uuid",
                "original_name",
                "size",
                "mimetype",
                "md5",
                "date_created",
                "storage_location",
                "attachment_uuid",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
            ],
        )(
            uuid=uuid4(),
            original_name="testdoc",
            size="123",
            mimetype="application/msword",
            md5="123",
            date_created="2019-05-02",
            storage_location=["Minio"],
            attachment_uuid=uuid4(),
            preview_uuid=uuid4(),
            preview_mimetype="application/pdf",
            preview_storage_location=["Minio"],
        )
        self.session.reset_mock()

        self.s3.get_download_url.return_value = (
            "https://example.com/example.txt"
        )

        result = self.qry.get_preview_link(
            attachment_uuid=str(self.attachment_uuid)
        )
        db_execute_calls = self.session.execute.call_args_list

        # From the first (only) execute(), get the 0th element of the "args"
        select_statement = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 1
        assert result == "https://example.com/example.txt"
        assert str(select_statement) == (
            "SELECT filestore.uuid, thread_message_attachment.filename AS original_name, filestore.size, filestore.mimetype, filestore.md5, filestore.date_created, filestore.storage_location, thread_message_attachment.uuid AS attachment_uuid, filestore_1.uuid AS preview_uuid, filestore_1.mimetype AS preview_mimetype, filestore_1.storage_location AS preview_storage_location, filestore_1.original_name AS preview_filename \n"
            "FROM filestore JOIN thread_message_attachment ON filestore.id = thread_message_attachment.filestore_id LEFT OUTER JOIN thread_message_attachment_derivative ON thread_message_attachment_derivative.thread_message_attachment_id = thread_message_attachment.id AND thread_message_attachment_derivative.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s LEFT OUTER JOIN filestore AS filestore_1 ON filestore_1.id = thread_message_attachment_derivative.filestore_id JOIN thread_message ON thread_message.id = thread_message_attachment.thread_message_id JOIN thread ON thread_message.thread_id = thread.id LEFT OUTER JOIN zaak ON zaak.id = thread.case_id \n"
            "WHERE thread_message_attachment.uuid = %(uuid_1)s AND thread.thread_type = %(thread_type_1)s AND zaak.uuid IN (SELECT zaak.uuid \n"
            "FROM zaak_betrokkenen JOIN zaak ON zaak.id = zaak_betrokkenen.zaak_id JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id \n"
            "WHERE zaak_betrokkenen.subject_id = %(subject_id_1)s AND zaak_betrokkenen.deleted IS NULL AND zaak.deleted IS NULL AND zaak.status IN (%(status_1_1)s, %(status_1_2)s, %(status_1_3)s, %(status_1_4)s) AND zaaktype_node.prevent_pip IS NOT true AND (zaak_betrokkenen.pip_authorized IS true OR zaak.aanvrager = zaak_betrokkenen.id))"
        )

    def test_get_preview_url_for_an_attachment_preview_not_found(self):
        self.session.execute().fetchone.return_value = namedtuple(
            "AttachedFile",
            [
                "uuid",
                "original_name",
                "size",
                "mimetype",
                "md5",
                "date_created",
                "storage_location",
                "attachment_uuid",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
            ],
        )(
            uuid=uuid4(),
            original_name="testdoc",
            size="123",
            mimetype="application/msword",
            md5="123",
            date_created="2019-05-02",
            storage_location=["Minio"],
            attachment_uuid=uuid4(),
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
        )

        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_preview_link(
                attachment_uuid=str(self.attachment_uuid)
            )
        assert excinfo.value.args == (
            f"No preview found for attachment with uuid {self.attachment_uuid}",
            "attachment/preview_not_found",
        )
