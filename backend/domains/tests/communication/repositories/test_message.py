# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import dkim
import email_validator
import logging
import pytest
import smtplib
from collections import namedtuple
from datetime import datetime
from email.message import EmailMessage
from minty.exceptions import ConfigurationConflict, Conflict, NotFound
from minty_infra_email.infrastructure import EmailConfiguration
from sqlalchemy import sql
from sqlalchemy.exc import IntegrityError
from tempfile import NamedTemporaryFile
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.communication.entities import (
    AttachedFile,
    Contact,
    ContactMoment,
    ExternalMessage,
    Note,
)
from zsnl_domains.communication.repositories import MessageRepository
from zsnl_domains.communication.repositories.message import (
    _dkim_sign_message,
    _encode_thread_uuid,
)
from zsnl_domains.database import schema


class InfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra
        self.config = {}

    def get_config(self, context):
        return self.config

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestMessageRepository:
    def setup_method(self):
        with mock.patch("sqlalchemy.orm.Session") as mock_session:
            self.mock_infra = mock.MagicMock()
            self.infra = InfraFactoryMock(infra={"database": mock_session})
            self.event_service = mock.MagicMock()
            self.message_repo = MessageRepository(
                infrastructure_factory=self.infra,
                context=None,
                event_service=self.event_service,
            )
            self.message_uuid = str(uuid4())
            self.mock_session = mock_session

            self.db = self.message_repo._get_infrastructure("database")

    def test_get_session(self):
        assert self.message_repo.session is self.infra.get_infrastructure(
            context=None, infrastructure_name="database"
        )

    def test_create_note(self):
        thread_uuid = uuid4()
        note_uuid = uuid4()

        created_by = Contact(uuid=uuid4(), name="test_name")
        content = "content"

        note = self.message_repo.create_note(
            note_uuid, thread_uuid, created_by, content
        )
        assert isinstance(note, Note)
        assert note.event_service is self.event_service

    def test__generate_message_database_values(self):
        event = mock.MagicMock
        event.entity_id = uuid4()
        event.changes = [
            {
                "key": "thread_uuid",
                "old_value": None,
                "new_value": "a5da1df2-7f50-4c4e-b46e-6fd7baabaed2",
            },
            {
                "key": "message_slug",
                "old_value": None,
                "new_value": "test content test content",
            },
            {
                "key": "message_type",
                "old_value": None,
                "new_value": "contactmoment",
            },
            {
                "key": "created_by",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "7f7e04a5-4382-40ff-b69a-b4ac81c7a46b",
                },
            },
            {
                "key": "content",
                "old_value": None,
                "new_value": "test content test content",
            },
            {"key": "channel", "old_value": None, "new_value": "email"},
            {"key": "direction", "old_value": None, "new_value": "incoming"},
            {
                "key": "recipient",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "f6985c75-8824-4594-930c-b68fa4228179",
                },
            },
            {
                "key": "created_date",
                "old_value": None,
                "new_value": "2019-08-14T19:33:14.754457",
            },
            {
                "key": "last_modified",
                "old_value": None,
                "new_value": "2019-08-19T15:45:41.423244",
            },
        ]

        message = self.message_repo._generate_message_database_values(
            event=event
        )

        assert message["message_slug"] == "test content test content"
        assert message["type"] == "contactmoment"
        assert message["created"] == "2019-08-14T19:33:14.754457"
        assert str(message["thread_id"]) == str(
            sql.select(schema.Thread.id)
            .where(
                schema.Thread.uuid == "a5da1df2-7f50-4c4e-b46e-6fd7baabaed2"
            )
            .scalar_subquery()
        )
        assert "created_by_uuid" in message

    def test__generate_message_database_values_no_creator(self):
        event = mock.MagicMock
        event.entity_id = uuid4()
        event.changes = [
            {
                "key": "thread_uuid",
                "old_value": None,
                "new_value": "a5da1df2-7f50-4c4e-b46e-6fd7baabaed2",
            },
            {
                "key": "message_slug",
                "old_value": None,
                "new_value": "test content test content",
            },
            {
                "key": "created_by",
                "new_value": None,
                "old_value": {
                    "type": "Contact",
                    "entity_id": "7f7e04a5-4382-40ff-b69a-b4ac81c7a46b",
                },
            },
            {
                "key": "message_type",
                "old_value": None,
                "new_value": "contactmoment",
            },
            {
                "key": "content",
                "old_value": None,
                "new_value": "test content test content",
            },
            {"key": "channel", "old_value": None, "new_value": "email"},
            {"key": "direction", "old_value": None, "new_value": "incoming"},
            {
                "key": "recipient",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "f6985c75-8824-4594-930c-b68fa4228179",
                },
            },
            {
                "key": "created_date",
                "old_value": None,
                "new_value": "2019-08-14T19:33:14.754457",
            },
            {
                "key": "last_modified",
                "old_value": None,
                "new_value": "2019-08-19T15:45:41.423244",
            },
        ]

        message = self.message_repo._generate_message_database_values(
            event=event
        )

        assert message["message_slug"] == "test content test content"
        assert message["type"] == "contactmoment"
        assert message["created"] == "2019-08-14T19:33:14.754457"
        assert str(message["thread_id"]) == str(
            sql.select(schema.Thread.id)
            .where(
                schema.Thread.uuid == "a5da1df2-7f50-4c4e-b46e-6fd7baabaed2"
            )
            .scalar_subquery()
        )
        assert message["created_by_uuid"] is None

    def test_save_contact_moment(self):
        now = datetime.utcnow()
        self.mock_infra.get_infrastructure.return_value = self.mock_session
        event_changes = [
            {
                "key": "thread_uuid",
                "old_value": None,
                "new_value": "a5da1df2-7f50-4c4e-b46e-6fd7baabaed2",
            },
            {
                "key": "message_slug",
                "old_value": None,
                "new_value": "test content test content",
            },
            {
                "key": "message_type",
                "old_value": None,
                "new_value": "contactmoment",
            },
            {
                "key": "created_by_displayname",
                "old_value": None,
                "new_value": "test name",
            },
            {
                "key": "recipient_displayname",
                "old_value": None,
                "new_value": "recipient name",
            },
            {
                "key": "created_by",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "7f7e04a5-4382-40ff-b69a-b4ac81c7a46b",
                },
            },
            {
                "key": "content",
                "old_value": None,
                "new_value": "test content test content",
            },
            {"key": "channel", "old_value": None, "new_value": "email"},
            {"key": "direction", "old_value": None, "new_value": "incoming"},
            {
                "key": "recipient",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "f6985c75-8824-4594-930c-b68fa4228179",
                },
            },
            {"key": "created_date", "old_value": None, "new_value": now},
            {"key": "last_modified", "old_value": None, "new_value": now},
        ]

        event = mock.MagicMock()

        event.entity_type = "ContactMoment"
        event.entity_id = self.message_uuid
        event.event_name = "ContactMomentCreated"
        event.changes = event_changes
        event.entity_data = {"thread_uuid": str(uuid4())}

        self.event_service.event_list = [event]

        exec_result = mock.MagicMock()
        self.mock_session.execute.return_value = exec_result

        self.message_repo.save()
        self.mock_session.execute.assert_called()

    def test_save_note_type_message(self):
        self.mock_infra.get_infrastructure.return_value = self.mock_session
        now = datetime.utcnow()
        event_changes = [
            {
                "key": "thread_uuid",
                "old_value": None,
                "new_value": str(uuid4()),
            },
            {
                "key": "created_by_displayname",
                "old_value": None,
                "new_value": "test name",
            },
            {
                "key": "message_slug",
                "old_value": None,
                "new_value": "test content test content",
            },
            {"key": "message_type", "old_value": None, "new_value": "note"},
            {
                "key": "created_by",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "7f7e04a5-4382-40ff-b69a-b4ac81c7a46b",
                },
            },
            {
                "key": "content",
                "old_value": None,
                "new_value": "test content test content",
            },
            {"key": "created_date", "old_value": None, "new_value": now},
            {"key": "last_modified", "old_value": None, "new_value": now},
        ]

        event = mock.MagicMock

        event.entity_type = "Note"
        event.entity_id = self.message_uuid
        event.event_name = "NoteCreated"
        event.changes = event_changes
        event.entity_data = {"thread_uuid": str(uuid4())}

        self.event_service.event_list = [event]

        exec_result = mock.MagicMock()
        exec_result.inserted_primary_key = [1, 2]
        self.mock_session.execute.return_value = exec_result

        self.message_repo.save()

    def test_save_external_message(self):
        now = datetime.utcnow()
        self.mock_infra.get_infrastructure.return_value = self.mock_session
        event_changes = [
            {
                "key": "thread_uuid",
                "old_value": None,
                "new_value": "70f7c47f-6575-4c08-8e94-0997514cc928",
            },
            {"key": "message_slug", "old_value": None, "new_value": "string"},
            {
                "key": "message_type",
                "old_value": "external",
                "new_value": "external",
            },
            {
                "key": "created_by",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "398375e4-ed8b-469c-8f27-1f8da190beb1",
                },
            },
            {
                "key": "created_by_displayname",
                "old_value": None,
                "new_value": "A. Admîn",
            },
            {"key": "content", "old_value": None, "new_value": "string"},
            {"key": "subject", "old_value": None, "new_value": "string"},
            {
                "key": "message_type",
                "old_value": "external",
                "new_value": "external",
            },
            {
                "key": "external_message_type",
                "old_value": None,
                "new_value": "pip",
            },
            {"key": "created_date", "old_value": None, "new_value": now},
            {"key": "last_modified", "old_value": None, "new_value": now},
            {
                "key": "original_message_file",
                "old_value": None,
                "new_value": uuid4(),
            },
        ]

        event = mock.MagicMock()

        event.entity_type = "ExternalMessage"
        event.entity_id = self.message_uuid
        event.event_name = "ExternalMessageCreated"
        event.changes = event_changes
        event.entity_data = {"thread_uuid": str(uuid4())}

        self.event_service.event_list = [event]

        exec_result = mock.MagicMock()
        self.mock_session.execute.return_value = exec_result

        self.message_repo.save()
        self.mock_session.execute.assert_called()

    def test_save_external_message_no_subject(self):
        now = datetime.utcnow()
        self.mock_infra.get_infrastructure.return_value = self.mock_session
        event_changes = [
            {
                "key": "thread_uuid",
                "old_value": None,
                "new_value": "70f7c47f-6575-4c08-8e94-0997514cc928",
            },
            {"key": "message_slug", "old_value": None, "new_value": "string"},
            {
                "key": "message_type",
                "old_value": "external",
                "new_value": "external",
            },
            {
                "key": "created_by",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "398375e4-ed8b-469c-8f27-1f8da190beb1",
                },
            },
            {
                "key": "created_by_displayname",
                "old_value": None,
                "new_value": "A. Admîn",
            },
            {"key": "content", "old_value": None, "new_value": "string"},
            {"key": "subject", "old_value": None, "new_value": None},
            {
                "key": "message_type",
                "old_value": "external",
                "new_value": "external",
            },
            {
                "key": "external_message_type",
                "old_value": None,
                "new_value": "pip",
            },
            {"key": "created_date", "old_value": None, "new_value": now},
            {"key": "last_modified", "old_value": None, "new_value": now},
            {
                "key": "original_message_file",
                "old_value": None,
                "new_value": uuid4(),
            },
        ]

        event = mock.MagicMock()

        event.entity_type = "ExternalMessage"
        event.entity_id = self.message_uuid
        event.event_name = "ExternalMessageCreated"
        event.changes = event_changes
        event.entity_data = {"thread_uuid": str(uuid4())}

        self.event_service.event_list = [event]

        exec_result = mock.MagicMock()
        self.mock_session.execute.return_value = exec_result

        self.message_repo.save()
        self.mock_session.execute.assert_called()

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._transform_to_entity"
    )
    def test_get_messages_by_thread_uuid(self, mock_transform):
        uuid = uuid4()

        self.message_repo.session.execute().fetchall.return_value = [1, 2]
        user_info = mock.MagicMock()
        user_info.permissions = {}
        page = 1
        page_size = 10

        mock_transform.return_value = "message"
        result = self.message_repo.get_messages_by_thread_uuid(
            uuid=uuid,
            user_info=user_info,
            case_id=1,
            page=page,
            page_size=page_size,
        )
        assert result == ["message", "message"]

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._transform_to_entity"
    )
    def test_get_messages_by_thread_uuid_for_pip_user(self, mock_transform):
        uuid = uuid4()
        page = 1
        page_size = 10

        self.message_repo.session.execute().fetchall.return_value = [1, 2]
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}

        mock_transform.return_value = "message"
        result = self.message_repo.get_messages_by_thread_uuid(
            uuid=uuid,
            user_info=user_info,
            case_id=1,
            page=page,
            page_size=page_size,
        )
        assert result == ["message", "message"]

    def test_transform_to_entity_contactmoment(self):
        qry_result = namedtuple(
            "qry_row",
            """type created_by_id created_by_uuid created_by_type created_by_displayname
             uuid thread_uuid case_uuid message_slug last_modified created message_date recipient_id recipient_uuid
             recipient_type contactmoment_content contactmoment_channel
             contactmoment_direction recipient_displayname""",
        )

        thread_uuid = uuid4()
        case_uuid = uuid4()
        contact_moment = qry_result(
            type="contact_moment",
            created_by_id=111,
            created_by_uuid=uuid4(),
            created_by_type="employee",
            created_by_displayname="A. Admin",
            thread_uuid=thread_uuid,
            case_uuid=case_uuid,
            uuid=uuid4(),
            message_slug="message_slug",
            last_modified="2019-02-18",
            created="2019-01-01",
            message_date="2020-03-24",
            recipient_type="person",
            recipient_id=1234,
            recipient_uuid=uuid4(),
            contactmoment_content="some content about a contactmoment",
            contactmoment_channel="phone",
            contactmoment_direction="incoming",
            recipient_displayname="test recipient",
        )
        result = self.message_repo._transform_to_entity(
            message_row=contact_moment
        )

        assert result.uuid == contact_moment.uuid
        assert result.thread_uuid == thread_uuid
        assert result.case_uuid == case_uuid
        assert result.message_type == "contact_moment"
        assert result.last_modified == contact_moment.last_modified
        assert result.created_date == contact_moment.created
        assert result.message_date == contact_moment.message_date
        assert result.content == contact_moment.contactmoment_content
        assert result.recipient.name == contact_moment.recipient_displayname
        assert result.channel == contact_moment.contactmoment_channel
        assert result.direction == contact_moment.contactmoment_direction

    def test_transform_to_entity_note(self):
        qry_result = namedtuple(
            "qry_row",
            """type created_by_id created_by_uuid created_by_type created_by_displayname
            uuid message_slug last_modified created message_date note_content thread_uuid case_uuid""",
        )

        thread_uuid = uuid4()
        case_uuid = uuid4()
        note = qry_result(
            type="note",
            created_by_id=111,
            created_by_uuid=uuid4(),
            created_by_type="employee",
            created_by_displayname="A. Admin",
            uuid=uuid4(),
            thread_uuid=thread_uuid,
            case_uuid=case_uuid,
            message_slug="message_slug",
            last_modified="2019-02-18",
            created="2019-01-01",
            message_date="2020-03-24",
            note_content="some note contents",
        )
        result = self.message_repo._transform_to_entity(message_row=note)
        assert result.uuid == note.uuid
        assert result.thread_uuid == thread_uuid
        assert result.case_uuid == case_uuid
        assert result.message_type == "note"
        assert result.last_modified == note.last_modified
        assert result.created_date == note.created
        assert result.message_date == note.message_date
        assert result.content == note.note_content

    def test_transform_to_entity_nonexistent(self):
        qry_result = namedtuple(
            "qry_row",
            """type created_by_id created_by_uuid created_by_type created_by_displayname
             uuid thread_uuid case_uuid message_slug last_modified created message_date
             external_message_content external_message_subject
             external_message_type attachments participants direction is_imported read_employee read_pip attachment_count external_message_failure_reason""",
        )
        fake_message = qry_result(
            type="fake",
            thread_uuid=uuid4(),
            case_uuid=uuid4(),
            created_by_id=111,
            created_by_uuid=uuid4(),
            created_by_type="employee",
            created_by_displayname="A. Admin",
            uuid=uuid4(),
            message_slug="message_slug",
            message_date="2001-01-01",
            last_modified="2019-02-18",
            created="2019-01-01",
            external_message_content="content",
            external_message_subject="subject",
            external_message_type="pip",
            attachments=[],
            participants={},
            direction="unspecified",
            is_imported=False,
            read_employee="2019-01-01",
            read_pip=None,
            attachment_count=0,
            external_message_failure_reason="Attachment size exceeded",
        )

        with pytest.raises(Conflict):
            self.message_repo._transform_to_entity(message_row=fake_message)

    def test_transform_to_entity_external_message(self):
        qry_result = namedtuple(
            "qry_row",
            """type created_by_id created_by_uuid created_by_type created_by_displayname
             uuid thread_uuid case_uuid message_slug last_modified created message_date
             external_message_content external_message_subject
             external_message_type attachments participants direction is_imported read_employee read_pip attachment_count external_message_failure_reason case_html_email_template""",
        )

        thread_uuid = uuid4()
        case_uuid = uuid4()
        external_message = qry_result(
            type="external",
            thread_uuid=thread_uuid,
            case_uuid=case_uuid,
            created_by_id=111,
            created_by_uuid=uuid4(),
            created_by_type="employee",
            created_by_displayname="A. Admin",
            uuid=uuid4(),
            message_slug="message_slug",
            message_date="2001-01-01",
            last_modified="2019-02-18",
            created="2019-01-01",
            external_message_content="content",
            external_message_subject="subject",
            external_message_type="pip",
            attachments=[],
            participants={},
            direction="unspecified",
            is_imported=False,
            read_employee="2019-01-01",
            read_pip=None,
            attachment_count=0,
            external_message_failure_reason="Attachment size exceeded",
            case_html_email_template=None,
        )
        result = self.message_repo._transform_to_entity(
            message_row=external_message
        )

        assert result.uuid == external_message.uuid
        assert result.thread_uuid == thread_uuid
        assert result.case_uuid == case_uuid
        assert result.message_type == "external"
        assert result.last_modified == external_message.last_modified
        assert result.created_date == external_message.created
        assert result.message_date == external_message.message_date
        assert result.content == external_message.external_message_content
        assert result.subject == external_message.external_message_subject
        assert (
            result.external_message_type
            == external_message.external_message_type
        )
        assert result.participants == {}
        assert result.direction == "unspecified"
        assert result.is_imported is False
        assert result.attachment_count == 0
        assert result.failure_reason == "Attachment size exceeded"

    def test_create_contact_moment(self):
        cm_uuid = uuid4()
        thread_uuid = uuid4()
        created_by = Contact(name="test created", uuid=uuid4())
        content = "comecontent"
        channel = "phone"
        direction = "incoming"
        contact = Contact(name="test contact", uuid=uuid4())

        cm = self.message_repo.create_contact_moment(
            thread_uuid=thread_uuid,
            created_by=created_by,
            content=content,
            channel=channel,
            direction=direction,
            contact=contact,
            contact_moment_uuid=cm_uuid,
        )
        assert isinstance(cm, ContactMoment)
        assert cm.event_service is not None

    def test_create_external_message(self):
        external_message_uuid = uuid4()
        thread_uuid = uuid4()
        created_by = Contact(name="test created", uuid=uuid4())
        content = "comecontent"
        subject = "phone"
        external_message_type = "pip"
        original_message_file = uuid4()

        em = self.message_repo.create_external_message(
            thread_uuid=thread_uuid,
            case_id=31337,
            created_by=created_by,
            content=content,
            subject=subject,
            external_message_type=external_message_type,
            external_message_uuid=external_message_uuid,
            participants=[],
            direction="incoming",
            original_message_file=original_message_file,
        )
        assert isinstance(em, ExternalMessage)
        assert em.event_service is not None

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_email_configuration"
    )
    def test_create_external_message_outgoing_email(
        self, mock_get_email_config
    ):
        external_message_uuid = uuid4()
        thread_uuid = str(uuid4())
        created_by = Contact(name="test created", uuid=uuid4())
        content = "comecontent"
        subject = "phone"

        external_message_type = "email"
        original_message_file = uuid4()

        mock_get_email_config.return_value = {
            "dkim": {"use_dkim": True},
            "infrastructure_config": "infra_config",
            "sender_address": "sender@example.com",
            "sender_name": "Sender Fullname",
            "subject_prefix": "PREFIX",
        }

        em = self.message_repo.create_external_message(
            thread_uuid=thread_uuid,
            case_id=42,
            created_by=created_by,
            content=content,
            subject=subject,
            external_message_type=external_message_type,
            external_message_uuid=external_message_uuid,
            participants=[],
            direction="outgoing",
            original_message_file=original_message_file,
        )
        assert isinstance(em, ExternalMessage)
        assert em.event_service is not None

        encoded_uuid = _encode_thread_uuid(UUID(thread_uuid))
        assert em.subject == f"[PREFIX 42 {encoded_uuid}] phone"

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_message_database_values"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_contact_moment_database_values"
    )
    def test_insert_contact_moment(self, mock_gen_cm, mock_generate_db_values):
        mock_gen_cm.return_value = {}
        mock_generate_db_values.return_value = {"type": "some_type"}
        self.message_repo.session.execute.side_effect = IntegrityError(
            statement="", params="", orig=None
        )
        event = namedtuple("event", "entity_id")
        ev = event(entity_id=123)
        with pytest.raises(Conflict):
            self.message_repo.insert_contact_moment(event=ev)

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_message_database_values"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_note_database_values"
    )
    def test_insert_note(self, mock_gen_n, mock_generate_db_values):
        mock_gen_n.return_value = {}
        mock_generate_db_values.return_value = {"type": "some_type"}
        self.message_repo.session.execute.side_effect = IntegrityError(
            statement="", params="", orig=None
        )
        event = namedtuple("event", "entity_id")
        ev = event(entity_id=123)
        with pytest.raises(Conflict):
            self.message_repo.insert_note(event=ev)

    def test__insert_message(self):
        self.message_repo.session.execute.side_effect = IntegrityError(
            statement="", params="", orig=None
        )
        with pytest.raises(Conflict):
            self.message_repo._insert_message(values={"uuid": uuid4()})

    def test__update_thread_last_message_cache(self):
        self.message_repo.session.execute.side_effect = IntegrityError(
            statement="", params="", orig=None
        )
        with pytest.raises(Conflict):
            self.message_repo._update_thread_attributes(
                last_message_cache={"uuid": uuid4()},
                thread_id=12345,
                message_count="decrement",
            )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_message_database_values"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_external_message_database_values"
    )
    def test_insert_external_message(
        self, mock_gen_em, mock_generate_db_values
    ):
        mock_gen_em.return_value = {}
        mock_generate_db_values.return_value = {"type": "some_type"}
        self.message_repo.session.execute.side_effect = IntegrityError(
            statement="", params="", orig=None
        )
        event = namedtuple("event", "entity_id")
        ev = event(entity_id=123)
        with pytest.raises(Conflict):
            self.message_repo.insert_external_message(event=ev)

    def test__get_contact_no_uuid(self):
        rv = self.message_repo._get_contact(None, None)
        assert rv is None

    def test__get_contact_no_result(self):
        self.message_repo.session.execute().fetchone.return_value = None

        contact_uuid = uuid4()

        with pytest.raises(NotFound) as excinfo:
            self.message_repo._get_contact(
                contact_uuid=contact_uuid, contact_name="contact name"
            )

        assert excinfo.value.args == (
            f"Contact with uuid '{contact_uuid}' not found.",
            "communication/contact/not_found",
        )

    def test__transform_attachments(self):
        one = {
            "uuid": str(uuid4()),
            "file_uuid": str(uuid4()),
            "filename": "test.jpg",
            "size": 1234,
            "mimetype": "image/jpg",
            "md5": "601297f43b56cbc8422d27ba00953742",
            "date_created": "2019-08-19T15:45:41.423244",
            "storage_location": ["nowhere"],
            "preview_uuid": None,
        }
        two = {
            "uuid": str(uuid4()),
            "file_uuid": str(uuid4()),
            "filename": "test.pdf",
            "size": 4321,
            "mimetype": "file/pdf",
            "date_created": "2019-09-19T15:45:41.423244",
            "md5": "5c7133a947bbd823719db12620fda5c9",
            "storage_location": ["nowhere"],
            "preview_uuid": str(uuid4()),
        }
        files = [one, two]
        out = self.message_repo._transform_attachments(files)
        for item in out:
            assert isinstance(item, AttachedFile)
            index = out.index(item)
            assert item.md5 == files[index]["md5"]
            assert item.filename == files[index]["filename"]
            assert item.size == files[index]["size"]
            assert item.date_created == files[index]["date_created"]
            assert item.mimetype == files[index]["mimetype"]

    def test__get_case_assignee_contact_id(self):
        result = namedtuple("result", "betrokkene_type betrokkene_id uuid")(
            betrokkene_type="medewerker", betrokkene_id=432, uuid=uuid4()
        )
        self.message_repo.session.execute().fetchone.return_value = result
        res = self.message_repo._get_case_assignee_contact_id(case_id=1234)
        assert res == {
            "legacy_id": "betrokkene-medewerker-432",
            "uuid": result.uuid,
        }

        result = None
        self.message_repo.session.execute().fetchone.return_value = result
        res = self.message_repo._get_case_assignee_contact_id(case_id=1234)
        assert res == {"legacy_id": None, "uuid": None}

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._transform_to_entity"
    )
    def test_get_message_by_uuid(self, transform):
        message_uuid = str(uuid4())
        result = namedtuple("Result", "thread_uuid")
        res = result(thread_uuid=uuid4())
        self.message_repo.session.execute().fetchone.return_value = res
        self.message_repo.get_message_by_uuid(message_uuid=message_uuid)

        transform.assert_called_with(message_row=res)

    def test_get_message_by_uuid_no_result(self):
        message_uuid = str(uuid4())
        self.message_repo.session.execute().fetchone.return_value = None
        with pytest.raises(NotFound):
            self.message_repo.get_message_by_uuid(message_uuid=message_uuid)

    def test_save(self):
        event = namedtuple("Event", "event_name entity_type")
        with mock.patch(
            "zsnl_domains.communication.repositories.message.MessageRepository._delete_message"
        ) as func:
            ev = event("MessageDeleted", "ExternalMessage")
            self.event_service.event_list = [ev]
            self.message_repo.save()
            func.assert_called_with(ev)

        with mock.patch(
            "zsnl_domains.communication.repositories.message.MessageRepository.insert_external_message"
        ) as func:
            ev = event("ExternalMessageCreated", "ExternalMessage")
            self.event_service.event_list = [ev]
            self.message_repo.save()
            func.assert_called_with(ev)

        with mock.patch(
            "zsnl_domains.communication.repositories.message.MessageRepository.insert_contact_moment"
        ) as func:
            ev = event("ContactMomentCreated", "ContactMoment")
            self.event_service.event_list = [ev]
            self.message_repo.save()
            func.assert_called_with(ev)

        with mock.patch(
            "zsnl_domains.communication.repositories.message.MessageRepository.insert_note"
        ) as func:
            ev = event("NoteCreated", "Note")
            self.event_service.event_list = [ev]
            self.message_repo.save()
            func.assert_called_with(ev)

        with mock.patch(
            "zsnl_domains.communication.repositories.message.MessageRepository._send_email"
        ) as func:
            ev = event("ExternalMessageSent", "ExternalMessage")
            self.event_service.event_list = [ev]
            self.message_repo.save()
            func.assert_called_with(ev)

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._create_cached_values"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._update_thread_attributes"
    )
    def test_update_cache(self, _update_cache, create_cached):
        thread_uuid = uuid4()
        message_count = 4

        res = namedtuple("Result", "thread_id")(437)
        self.db.execute().fetchone.return_value = res
        create_cached.return_value = "values"

        self.message_repo._update_cache(
            thread_uuid=thread_uuid, message_count=message_count
        )
        create_cached.assert_called_once_with(res)
        _update_cache.assert_called_once_with(
            last_message_cache="values",
            thread_id=res.thread_id,
            message_count=message_count,
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._create_cached_values"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._update_thread_attributes"
    )
    def test_update_cache_no_results(self, _update_cache, create_cached):
        thread_uuid = uuid4()
        message_count = 4

        self.db.execute().fetchone.return_value = None

        self.message_repo._update_cache(
            thread_uuid=thread_uuid, message_count=message_count
        )
        create_cached.assert_not_called()
        _update_cache.assert_not_called()

    def test__create_cached_values(self):
        Message = namedtuple(
            "Message",
            """type message_slug created_by_displayname created contactmoment_direction
            contactmoment_channel recipient_displayname external_message_type
            external_message_subject""",
        )
        created = datetime.now()

        msg = Message(
            type="note",
            message_slug="msg_slug",
            created=created,
            created_by_displayname="someone",
            contactmoment_direction=None,
            contactmoment_channel=None,
            recipient_displayname=None,
            external_message_type=None,
            external_message_subject=None,
        )
        msg_cache = self.message_repo._create_cached_values(msg)
        assert msg_cache == {
            "message_type": "note",
            "slug": "msg_slug",
            "created_name": "someone",
            "created": str(created.isoformat()),
            "failure_reason": None,
        }
        msg_contact = Message(
            type="contact_moment",
            message_slug="msg_slug",
            created=created,
            created_by_displayname="someone",
            contactmoment_direction="outgoing",
            contactmoment_channel="phone",
            recipient_displayname="someone else",
            external_message_type=None,
            external_message_subject=None,
        )
        msg_cache = self.message_repo._create_cached_values(msg_contact)
        assert msg_cache == {
            "message_type": "contact_moment",
            "slug": "msg_slug",
            "created_name": "someone",
            "created": str(created.isoformat()),
            "direction": "outgoing",
            "channel": "phone",
            "recipient_name": "someone else",
            "failure_reason": None,
        }

        msg_ext = Message(
            type="external",
            message_slug="msg_slug",
            created=created,
            created_by_displayname="someone",
            contactmoment_direction="outgoing",
            contactmoment_channel="phone",
            recipient_displayname="someone else",
            external_message_type="pip",
            external_message_subject="some_subject",
        )
        msg_ext = self.message_repo._create_cached_values(msg_ext)
        assert msg_ext == {
            "message_type": "pip_message",
            "slug": "msg_slug",
            "created_name": "someone",
            "created": str(created.isoformat()),
            "subject": "some_subject",
            "failure_reason": None,
        }

    def test__create_cached_values_uses_message_date_for_older_external_messages(
        self,
    ):
        Message = namedtuple(
            "Message",
            """type message_slug created_by_displayname created message_date contactmoment_direction
            contactmoment_channel recipient_displayname external_message_type
            external_message_subject""",
        )
        created = datetime.now()
        message_date = datetime.strptime(
            "2010-12-31 23:59:59", "%Y-%m-%d %H:%M:%S"
        )

        msg = Message(
            type="note",
            message_slug="msg_slug",
            created=created,
            created_by_displayname="someone",
            contactmoment_direction=None,
            message_date=message_date,
            contactmoment_channel=None,
            recipient_displayname=None,
            external_message_type=None,
            external_message_subject=None,
        )

        msg_cache = self.message_repo._create_cached_values(msg)
        assert msg_cache == {
            "message_type": "note",
            "slug": "msg_slug",
            "created_name": "someone",
            "created": str(message_date.isoformat()),
            "failure_reason": None,
        }

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_message_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._update_cache"
    )
    def test__delete_message(self, update_cache, mock_get_message):
        thread_uuid = uuid4()

        mock_message = ExternalMessage(
            uuid=uuid4(),
            thread_uuid=thread_uuid,
            case_uuid=uuid4(),
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            content="This is the content",
            subject="Subjective",
            external_message_type="email",
            participants=[
                {
                    "role": "to",
                    "display_name": "Testing 123",
                    "address": "foo@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 234",
                    "address": "foo2@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 345",
                    "address": "foo3@example.com",
                },
                {
                    "role": "to",
                    "display_name": "Me, myself and I",
                    "address": "sender@example.com",
                },
                # This tests the "empty email address" exception
                {"role": "to", "display_name": "Something", "address": ""},
            ],
            attachments=None,
            direction="outgoing",
            original_message_file=None,
        )
        mock_get_message.return_value = mock_message

        event = namedtuple("Event", "entity_id entity_type entity_data")
        thread_uuid = str(uuid4())
        ev = event(
            entity_id=str(uuid4()),
            entity_type="Note",
            entity_data={"thread_uuid": thread_uuid},
        )
        self.message_repo._delete_message(ev)
        update_cache.assert_called_once_with(
            thread_uuid=thread_uuid, message_count="decrement"
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_message_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._update_cache"
    )
    def test_delete_contact_moment(self, update_cache, mock_get_message):
        thread_uuid = uuid4()
        mock_contact_moment = ContactMoment(
            uuid=uuid4(),
            thread_uuid=thread_uuid,
            case_uuid=uuid4(),
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            content="This is the content",
            direction="outgoing",
            message_date=None,
            recipient=None,
            recipient_displayname=None,
            channel=None,
        )
        mock_get_message.return_value = mock_contact_moment

        event = namedtuple("Event", "entity_id entity_type entity_data")
        thread_uuid = str(uuid4())
        ev = event(
            entity_id=str(uuid4()),
            entity_type="ContactMoment",
            entity_data={"thread_uuid": thread_uuid},
        )
        self.message_repo._delete_message(ev)
        update_cache.assert_called_once_with(
            thread_uuid=thread_uuid, message_count="decrement"
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_message_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._update_cache"
    )
    def test_prevent_delete_message(self, update_cache, mock_get_message):
        thread_uuid = uuid4()

        mock_message = ExternalMessage(
            uuid=uuid4(),
            thread_uuid=thread_uuid,
            case_uuid=uuid4(),
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            content="This is the content",
            subject="Subjective",
            external_message_type="pip",
            participants=[
                {
                    "role": "to",
                    "display_name": "Testing 123",
                    "address": "foo@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 234",
                    "address": "foo2@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 345",
                    "address": "foo3@example.com",
                },
                {
                    "role": "to",
                    "display_name": "Me, myself and I",
                    "address": "sender@example.com",
                },
                # This tests the "empty email address" exception
                {"role": "to", "display_name": "Something", "address": ""},
            ],
            attachments=None,
            direction="outgoing",
            original_message_file=None,
        )
        mock_get_message.return_value = mock_message

        event = namedtuple("Event", "entity_id entity_type entity_data")
        thread_uuid = str(uuid4())
        ev = event(
            entity_id=str(uuid4()),
            entity_type="ExternalMessage",
            entity_data={"thread_uuid": thread_uuid},
        )

        with pytest.raises(Conflict) as exception_info:
            self.message_repo._delete_message(ev)

        assert exception_info.value.args == (
            "PIP messages can not be deleted",
            "communication/message/delete_message/pip_deletion_prevented",
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_email_configuration"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_message_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._get_infrastructure"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message._dkim_sign_message"
    )
    @mock.patch("email_validator.validate_email")
    def test__send_email(
        self,
        mock_validate_email,
        mock_sign,
        mock_get_infrastructure,
        mock_get_message,
        mock_get_email_config,
    ):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        thread_uuid = uuid4()

        mock_event = mock.MagicMock()
        mock_event.entity_id = str(uuid4())

        mock_attachment = AttachedFile(
            uuid=uuid4(),
            filename="some_file.pdf",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            size=31337,
            mimetype="application/pdf",
            date_created=None,
            storage_location="Somewhere",
        )

        mock_message = ExternalMessage(
            uuid=uuid4(),
            thread_uuid=thread_uuid,
            case_uuid=uuid4(),
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            content="This is the content",
            subject="Subjective",
            external_message_type="email",
            participants=[
                {
                    "role": "to",
                    "display_name": "Testing 123",
                    "address": "foo@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 234",
                    "address": "foo2@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 345",
                    "address": "foo3@example.com",
                },
                {
                    "role": "to",
                    "display_name": "Me, myself and I",
                    "address": "sender@example.com",
                },
                # This tests the "empty email address" exception
                {"role": "to", "display_name": "Something", "address": ""},
            ],
            attachments=[mock_attachment],
            direction="outgoing",
            original_message_file=None,
        )
        mock_get_message.return_value = mock_message

        def mock_download(destination, file_uuid, storage_location):
            destination.write(
                f"attachment: {file_uuid}@{storage_location}".encode()
            )

        mock_get_infrastructure().download_file = mock_download

        mock_get_email_config.return_value = {
            "dkim": {"use_dkim": True},
            "infrastructure_config": "infra_config",
            "sender_address": "sender@example.com",
            "sender_name": "Sender Fullname",
            "subject_prefix": "PREFIX",
            "rich_email": 0,
            "rich_email_templates": [],
        }

        # The actual test:
        self.message_repo._send_email(mock_event)

        call_args = mock_get_infrastructure().send.call_args_list
        (args, kwargs) = call_args[0]
        email_message = args[0]
        email_config = args[1]

        assert email_config == "infra_config"

        mock_sign.assert_called_once_with(
            message=email_message, dkim_config={"use_dkim": True}
        )

        assert isinstance(email_message, EmailMessage)
        assert email_message["To"] == "Testing 123 <foo@example.com>"
        assert (
            email_message["Cc"]
            == "Testing 234 <foo2@example.com>, Testing 345 <foo3@example.com>"
        )

        assert email_message["Subject"] == "Subjective"
        assert email_message.get_content_type() == "multipart/mixed"

        parts = list(email_message.iter_parts())
        assert len(parts) == 2

        assert parts[0].get_content_type() == "text/plain"
        assert parts[0].get_content() == "This is the content\n"
        assert parts[1].get_content_type() == "application/pdf"
        assert (
            parts[1].get_content()
            == f"attachment: {mock_attachment.file_uuid}@{mock_attachment.storage_location[0]}".encode()
        )

        assert str(self.message_repo.session.execute.call_args[0][0]) == str(
            sql.update(schema.ThreadMessageExternal)
            .values(
                source_file_id=self.mock_session.schema.execute().inserted_primary_key[
                    0
                ]
            )
            .where(
                sql.and_(
                    schema.ThreadMessageExternal.id
                    == schema.ThreadMessage.thread_message_external_id,
                    schema.ThreadMessage.uuid == mock_message.uuid,
                )
            )
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_email_configuration"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_message_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._get_infrastructure"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message._dkim_sign_message"
    )
    @mock.patch("email_validator.validate_email")
    def test__send_email_dkim_key_error(
        self,
        mock_validate_email,
        mock_sign,
        mock_get_infrastructure,
        mock_get_message,
        mock_get_email_config,
    ):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        thread_uuid = uuid4()

        mock_event = mock.MagicMock()
        mock_event.entity_id = str(uuid4())

        mock_attachment = AttachedFile(
            uuid=uuid4(),
            filename="some_file.pdf",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            size=31337,
            mimetype="application/pdf",
            date_created=None,
            storage_location="Somewhere",
        )

        mock_message = ExternalMessage(
            uuid=uuid4(),
            thread_uuid=thread_uuid,
            case_uuid=uuid4(),
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            content="This is the content",
            subject="Subjective",
            external_message_type="email",
            participants=[
                {
                    "role": "to",
                    "display_name": "Testing 123",
                    "address": "foo@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 234",
                    "address": "foo2@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 345",
                    "address": "foo3@example.com",
                },
            ],
            attachments=[mock_attachment],
            direction="outgoing",
            original_message_file=None,
        )
        mock_get_message.return_value = mock_message

        def mock_download(destination, file_uuid, storage_location):
            destination.write(
                f"attachment: {file_uuid}@{storage_location}".encode()
            )

        mock_get_infrastructure().download_file = mock_download

        mock_get_email_config.return_value = {
            "dkim": {"use_dkim": True},
            "infrastructure_config": "infra_config",
            "sender_address": "sender@example.com",
            "sender_name": "Sender Fullname",
            "subject_prefix": "PREFIX",
            "rich_email": 0,
            "rich_email_templates": [],
        }

        self.message_repo._logger = mock.MagicMock()

        mock_sign.side_effect = dkim.KeyFormatError

        # The actual test:
        self.message_repo._send_email(mock_event)

        call_args = mock_get_infrastructure().send.call_args_list
        (args, kwargs) = call_args[0]
        email_message = args[0]
        email_config = args[1]

        assert email_config == "infra_config"

        (
            log_args,
            log_kwargs,
        ) = self.message_repo._logger.error.call_args_list[0]
        assert log_args == (
            "Error generating DKIM signature: Key format error",
        )
        assert isinstance(log_kwargs["exc_info"], dkim.KeyFormatError)

        mock_sign.assert_called_once_with(
            message=email_message, dkim_config={"use_dkim": True}
        )

        assert isinstance(email_message, EmailMessage)
        assert email_message["To"] == "Testing 123 <foo@example.com>"
        assert (
            email_message["Cc"]
            == "Testing 234 <foo2@example.com>, Testing 345 <foo3@example.com>"
        )

        assert email_message["Subject"] == "Subjective"
        assert email_message.get_content_type() == "multipart/mixed"

        parts = list(email_message.iter_parts())
        assert len(parts) == 2

        assert parts[0].get_content_type() == "text/plain"
        assert parts[0].get_content() == "This is the content\n"
        assert parts[1].get_content_type() == "application/pdf"
        assert (
            parts[1].get_content()
            == f"attachment: {mock_attachment.file_uuid}@{mock_attachment.storage_location[0]}".encode()
        )

        assert str(self.message_repo.session.execute.call_args[0][0]) == str(
            sql.update(schema.ThreadMessageExternal)
            .values(
                source_file_id=self.mock_session.schema.execute().inserted_primary_key[
                    0
                ]
            )
            .where(
                sql.and_(
                    schema.ThreadMessageExternal.id
                    == schema.ThreadMessage.thread_message_external_id,
                    schema.ThreadMessage.uuid == mock_message.uuid,
                )
            )
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_email_configuration"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_message_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._get_infrastructure"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message._dkim_sign_message"
    )
    @mock.patch("email_validator.validate_email")
    def test__send_email_smtp_error(
        self,
        mock_validate_email,
        mock_sign,
        mock_get_infrastructure,
        mock_get_message,
        mock_get_email_config,
    ):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.return_value = True

        thread_uuid = uuid4()

        mock_event = mock.MagicMock()
        mock_event.entity_id = str(uuid4())

        mock_attachment = AttachedFile(
            uuid=uuid4(),
            filename="some_file.pdf",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            size=31337,
            mimetype="application/pdf",
            date_created=None,
            storage_location="Somewhere",
        )

        mock_message = ExternalMessage(
            uuid=uuid4(),
            thread_uuid=thread_uuid,
            case_uuid=uuid4(),
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            content="This is the content",
            subject="Subjective",
            external_message_type="email",
            participants=[
                {
                    "role": "to",
                    "display_name": "Testing 123",
                    "address": "foo@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 234",
                    "address": "foo2@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 345",
                    "address": "foo3@example.com",
                },
            ],
            attachments=[mock_attachment],
            direction="outgoing",
            original_message_file=None,
        )
        mock_get_message.return_value = mock_message

        def mock_download(destination, file_uuid, storage_location):
            destination.write(
                f"attachment: {file_uuid}@{storage_location}".encode()
            )

        mock_get_infrastructure().download_file = mock_download

        mock_get_email_config.return_value = {
            "dkim": {"use_dkim": True},
            "infrastructure_config": "infra_config",
            "sender_address": "sender@example.com",
            "sender_name": "Sender Fullname",
            "subject_prefix": "PREFIX",
            "rich_email": 0,
            "rich_email_templates": [],
        }

        self.message_repo._logger = mock.MagicMock()

        mock_get_infrastructure().send.side_effect = (
            smtplib.SMTPResponseException(566, "Test error")
        )

        # The actual test:
        self.message_repo._send_email(mock_event)

        call_args = mock_get_infrastructure().send.call_args_list
        (args, kwargs) = call_args[0]
        email_message = args[0]
        email_config = args[1]

        assert email_config == "infra_config"

        assert isinstance(email_message, EmailMessage)
        assert email_message["To"] == "Testing 123 <foo@example.com>"
        assert (
            email_message["Cc"]
            == "Testing 234 <foo2@example.com>, Testing 345 <foo3@example.com>"
        )

        assert email_message["Subject"] == "Subjective"
        assert email_message.get_content_type() == "multipart/mixed"

        parts = list(email_message.iter_parts())
        assert len(parts) == 2

        assert parts[0].get_content_type() == "text/plain"
        assert parts[0].get_content() == "This is the content\n"
        assert parts[1].get_content_type() == "application/pdf"
        assert (
            parts[1].get_content()
            == f"attachment: {mock_attachment.file_uuid}@{mock_attachment.storage_location[0]}".encode()
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_email_configuration"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository.get_message_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._get_infrastructure"
    )
    @mock.patch(
        "zsnl_domains.communication.repositories.message._dkim_sign_message"
    )
    def test__send_email_too_big(
        self,
        mock_sign,
        mock_get_infrastructure,
        mock_get_message,
        mock_get_email_config,
    ):
        thread_uuid = uuid4()

        mock_event = mock.MagicMock()
        mock_event.entity_id = str(uuid4())

        mock_attachment = AttachedFile(
            uuid=uuid4(),
            filename="some_file.pdf",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            size=900 * 1024 * 1024,
            mimetype="application/pdf",
            date_created=None,
            storage_location="Somewhere",
        )

        mock_message = ExternalMessage(
            uuid=uuid4(),
            thread_uuid=thread_uuid,
            case_uuid=uuid4(),
            created_by=None,
            created_by_displayname=None,
            message_slug="",
            last_modified=None,
            created_date=None,
            content="This is the content",
            subject="Subjective",
            external_message_type="email",
            participants=[
                {
                    "role": "to",
                    "display_name": "Testing 123",
                    "address": "foo@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 234",
                    "address": "foo2@example.com",
                },
                {
                    "role": "cc",
                    "display_name": "Testing 345",
                    "address": "foo3@example.com",
                },
            ],
            attachments=[mock_attachment],
            direction="outgoing",
            original_message_file=None,
        )
        mock_get_message.return_value = mock_message

        mock_get_email_config.return_value = {
            "dkim": {"use_dkim": True},
            "infrastructure_config": "infra_config",
            "sender_address": "sender@example.com",
            "sender_name": "Sender Fullname",
            "subject_prefix": "PREFIX",
            "rich_email": 0,
            "rich_email_templates": [],
        }

        self.message_repo._logger = mock.MagicMock()

        # The actual test:
        self.message_repo._send_email(mock_event)

        call_args = mock_get_infrastructure().send.call_args_list
        assert len(call_args) == 0

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._get_infrastructure"
    )
    def test__get_dkim_settings(self, mock_get_infrastructure):
        integration_config = {
            "dkim_algorithm": "rsa-sha256",
            "dkim_canonicalization": "relaxed",
            "dkim_domain": "ssdff",
            "dkim_key": [
                {
                    "storage_location": ["minio"],
                    "uuid": "a8434374-10ff-4250-bab8-28a839f97529",
                }
            ],
            "dkim_selector": "sdfsdf",
            "smarthost_hostname": "smtp.gmail.com",
            "smarthost_port": "587",
            "smarthost_password": None,
            "smarthost_username": None,
            "subject": "TEST",
            "custom_dkim": 1,
            "use_smarthost": 0,
            "api_user": "example@example.com",
            "sender_name": "Blarp",
            "use_dkim": 1,
        }

        def mock_download(destination, file_uuid, storage_location):
            assert file_uuid == "a8434374-10ff-4250-bab8-28a839f97529"
            assert storage_location == "minio"
            destination.write(b"DKIM KEY GOES HERE")

        mock_get_infrastructure().download_file = mock_download

        settings = self.message_repo._get_dkim_settings(integration_config)

        assert settings == {
            "use_dkim": True,
            "dkim_selector": b"sdfsdf",
            "dkim_domain": b"ssdff",
            "dkim_algorithm": b"rsa-sha256",
            "dkim_private_key": b"DKIM KEY GOES HERE",
        }

    def test__get_dkim_settings_default_dkim(self):
        integration_config = {
            "dkim_algorithm": "rsa-sha256",
            "dkim_canonicalization": "relaxed",
            "dkim_domain": "example.com",
            "use_dkim": 1,
        }

        with NamedTemporaryFile(mode="w+b", delete=True) as dkim_key_file:
            dkim_key_file.write(b"DEFAULT KEY")
            dkim_key_file.flush()

            self.infra.config = {
                "email": {
                    "dkim_selector": "default",
                    "dkim_private_key": dkim_key_file.name,
                }
            }

            settings = self.message_repo._get_dkim_settings(integration_config)

        assert settings == {
            "use_dkim": True,
            "dkim_domain": b"example.com",
            "dkim_selector": b"default",
            "dkim_algorithm": b"rsa-sha256",
            "dkim_private_key": b"DEFAULT KEY",
        }

    def test__get_dkim_settings_no_dkim(self):
        integration_config = {
            "dkim_algorithm": "rsa-sha256",
            "dkim_canonicalization": "relaxed",
            "use_dkim": 0,
        }

        settings = self.message_repo._get_dkim_settings(integration_config)

        assert settings == {"use_dkim": False}

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._get_dkim_settings"
    )
    def test_get_email_configuration(self, mock_get_dkim_settings):
        self.mock_session.execute().fetchone().interface_config = {
            "use_smarthost": False,
            "api_user": "sender@example.com",
            "sender_name": "Sender Fullname",
        }
        self.infra.config = {
            "email": {"smarthost_hostname": "smtp.default.example.com"}
        }

        email_config = self.message_repo.get_email_configuration()

        assert email_config["dkim"] == mock_get_dkim_settings()
        assert isinstance(
            email_config["infrastructure_config"], EmailConfiguration
        )
        assert email_config["infrastructure_config"].smarthost_hostname is None
        assert email_config["infrastructure_config"].smarthost_port == 25
        assert email_config["infrastructure_config"].smarthost_username is None
        assert email_config["infrastructure_config"].smarthost_password is None
        assert (
            email_config["infrastructure_config"].smarthost_security == "none"
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._get_dkim_settings"
    )
    def test__get_email_configuration_custom_smarthost(
        self, mock_get_dkim_settings
    ):
        self.mock_session.execute().fetchone().interface_config = {
            "use_smarthost": True,
            "smarthost_hostname": "smtp.example.com",
            "smarthost_port": 25,
            "smarthost_username": "piet",
            "smarthost_password": "friet",
            "api_user": "sender@example.com",
            "sender_name": "Sender Fullname",
        }

        email_config = self.message_repo.get_email_configuration()

        assert email_config["dkim"] == mock_get_dkim_settings()
        assert isinstance(
            email_config["infrastructure_config"], EmailConfiguration
        )
        assert (
            email_config["infrastructure_config"].smarthost_hostname
            == "smtp.example.com"
        )
        assert email_config["infrastructure_config"].smarthost_port == 25
        assert (
            email_config["infrastructure_config"].smarthost_username == "piet"
        )
        assert (
            email_config["infrastructure_config"].smarthost_password == "friet"
        )
        assert (
            email_config["infrastructure_config"].smarthost_security
            == "starttls"
        )
        assert email_config["sender_name"] == "Sender Fullname"

        assert email_config["sender_address"] == "sender@example.com"

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._get_dkim_settings"
    )
    def test__get_email_configuration_unconfigured(
        self, mock_get_dkim_settings
    ):
        self.mock_session.execute().fetchone.return_value = None

        with pytest.raises(ConfigurationConflict):
            self.message_repo.get_email_configuration()

    @mock.patch("dkim.dkim_sign")
    def test__dkim_sign_message(self, mock_sign):
        message = EmailMessage()
        message["to"] = "to@example.com"
        message["subject"] = "example"
        message.set_content("content goes here")

        message_bytes = message.as_bytes()

        dkim_config = {
            "dkim_selector": b"dkim",
            "dkim_domain": b"dkim.example.com",
            "dkim_algorithm": b"rsa-sha256",
            "dkim_private_key": b"-----BEGIN FAKE KEY-----",
        }

        mock_sign.return_value = (
            b"DKIM-Signature: This is not a DKIM signature"
        )

        _dkim_sign_message(message, dkim_config)

        mock_sign.assert_called_once_with(
            message=message_bytes,
            selector=b"dkim",
            domain=b"dkim.example.com",
            canonicalize=(b"relaxed", b"simple"),
            signature_algorithm=b"rsa-sha256",
            privkey=b"-----BEGIN FAKE KEY-----",
            logger=logging.getLogger(
                "zsnl_domains.communication.repositories.message"
            ),
        )

        assert message["DKIM-Signature"] == "This is not a DKIM signature"

    @mock.patch("zsnl_domains.communication.repositories.message.uuid4")
    def test__save_message_source(self, mock_uuid4):
        message_file_uuid = uuid4()
        message_uuid = uuid4()

        mock_uuid4.return_value = message_file_uuid
        mock_self = mock.MagicMock()
        message_content = b"abc"

        upload_result = {
            "mime_type": "text/plain",
            "md5": "123",
            "size": 42,
            "storage_location": "the moon",
        }

        mock_infra = mock_self._get_infrastructure()
        mock_infra.upload.return_value = upload_result

        result = MessageRepository._save_message_source(
            mock_self, message_uuid, message_content
        )

        mock_self._get_infrastructure.assert_called_with(name="s3")

        insert_call = mock_self.session.execute.call_args[0]

        assert str(insert_call[0]) == str(
            sql.insert(schema.Filestore).values(
                {
                    "uuid": message_file_uuid,
                    "original_name": f"message-{message_uuid}.eml",
                    "is_archivable": True,
                    "virus_scan_status": "ok",
                    "mimetype": upload_result["mime_type"],
                    "md5": upload_result["md5"],
                    "size": upload_result["size"],
                    "storage_location": [upload_result["storage_location"]],
                }
            )
        )

        assert result == mock_self.session.execute().inserted_primary_key[0]

    @mock.patch("zsnl_domains.communication.repositories.message.uuid4")
    def test__save_message_source_when_pip_message_and_filename_is_not_given(
        self, mock_uuid4
    ):
        message_file_uuid = uuid4()
        message_uuid = uuid4()

        mock_uuid4.return_value = message_file_uuid
        mock_self = mock.MagicMock()
        message_content = b"abc"

        upload_result = {
            "mime_type": "text/plain",
            "md5": "123",
            "size": 42,
            "storage_location": "the moon",
        }

        mock_infra = mock_self._get_infrastructure()
        mock_infra.upload.return_value = upload_result

        result = MessageRepository._save_message_source(
            mock_self, message_uuid, message_content, pip_message=True
        )

        mock_self._get_infrastructure.assert_called_with(name="s3")

        insert_call = mock_self.session.execute.call_args[0]

        assert str(insert_call[0]) == str(
            sql.insert(schema.Filestore).values(
                {
                    "uuid": message_file_uuid,
                    "original_name": f"pip-message-{message_uuid}.txt",
                    "is_archivable": True,
                    "virus_scan_status": "ok",
                    "mimetype": upload_result["mime_type"],
                    "md5": upload_result["md5"],
                    "size": upload_result["size"],
                    "storage_location": [upload_result["storage_location"]],
                },
            )
        )

        assert result == mock_self.session.execute().inserted_primary_key[0]

    @mock.patch("zsnl_domains.communication.repositories.message.uuid4")
    def test__save_message_source_when_pip_message_and_filename_given(
        self, mock_uuid4
    ):
        message_file_uuid = uuid4()
        message_uuid = uuid4()

        mock_uuid4.return_value = message_file_uuid
        mock_self = mock.MagicMock()
        message_content = b"abc"

        upload_result = {
            "mime_type": "text/plain",
            "md5": "123",
            "size": 42,
            "storage_location": "the moon",
        }

        mock_infra = mock_self._get_infrastructure()
        mock_infra.upload.return_value = upload_result

        result = MessageRepository._save_message_source(
            mock_self,
            message_uuid,
            message_content,
            pip_message=True,
            pip_message_filename="my-pip-filename.txt",
        )

        mock_self._get_infrastructure.assert_called_with(name="s3")

        insert_call = mock_self.session.execute.call_args[0]

        assert str(insert_call[0]) == str(
            sql.insert(schema.Filestore).values(
                {
                    "uuid": message_file_uuid,
                    "original_name": "my-pip-filename.txt",
                    "is_archivable": True,
                    "virus_scan_status": "ok",
                    "mimetype": upload_result["mime_type"],
                    "md5": upload_result["md5"],
                    "size": upload_result["size"],
                    "storage_location": [upload_result["storage_location"]],
                },
            )
        )

        assert result == mock_self.session.execute().inserted_primary_key[0]

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._transform_to_entity"
    )
    def test_get_messages_by_uuid(self, mock_transform):
        uuids = [uuid4()]

        self.message_repo.session.execute().fetchall.return_value = [1, 2]
        user_info = mock.MagicMock()
        user_info.permissions = {}

        mock_transform.return_value = "message"
        result = self.message_repo.get_messages_by_uuid(
            uuids=uuids, user_info=user_info
        )
        assert result == ["message", "message"]

        user_info.permissions = {"pip_user": True}
        result = self.message_repo.get_messages_by_uuid(
            uuids=uuids, user_info=user_info
        )

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_external_message_database_values"
    )
    def test_mark_message_read(self, generate_database_value):
        now = datetime.utcnow()
        event = mock.MagicMock()
        event.entity_id = uuid4()

        generate_database_value.return_value = {"read_emloyee": now}
        self.message_repo._mark_message_read(event)

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._mark_message_read"
    )
    def test_save__mark_message_unread(self, mark_message_read):
        now = datetime.utcnow()
        event_changes = [
            {"key": "read_employee", "old_value": None, "new_value": now}
        ]

        event = mock.MagicMock()

        event.entity_type = "ExternalMessage"
        event.entity_id = self.message_uuid
        event.event_name = "ExternalMessageRead"
        event.changes = event_changes
        event.entity_data = {"thread_uuid": str(uuid4())}

        self.event_service.event_list = [event]
        self.message_repo.save()
        mark_message_read.assert_called_with(event)

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_external_message_database_values"
    )
    def test_mark_message_unread(self, generate_database_value):
        event = mock.MagicMock()
        event.entity_id = uuid4()

        generate_database_value.return_value = {"read_emloyee": None}
        self.message_repo._mark_message_unread(event)

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._mark_message_unread"
    )
    def test_save__mark_message_read(self, mock_mark_message_unread):
        now = datetime.utcnow()
        event_changes = [
            {"key": "read_employee", "old_value": now, "new_value": None}
        ]

        event = mock.MagicMock()

        event.entity_type = "ExternalMessage"
        event.entity_id = self.message_uuid
        event.event_name = "ExternalMessageMarkedUnread"
        event.changes = event_changes
        event.entity_data = {"thread_uuid": str(uuid4())}

        self.event_service.event_list = [event]
        self.message_repo.save()
        mock_mark_message_unread.assert_called_with(event)

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._generate_external_message_database_values"
    )
    def test__message_attachment_count_updated(
        self, mock_generate_database_values
    ):
        event = mock.MagicMock
        event.entity_type = "ExternalMessage"
        event.entity_id = self.message_uuid
        event.event_name = "ExternalMessageAttachmentCountIncremented"
        event.entity_data = {}

        mock_generate_database_values.return_value = {"attachment_count": 3}

        self.message_repo._update_message_attachment_count(event=event)
        mock_generate_database_values.assert_called_once_with(event)

    @mock.patch(
        "zsnl_domains.communication.repositories.message.MessageRepository._update_message_attachment_count"
    )
    def test_save_message_attachment_count_incremeneted(
        self, update_attachment_count
    ):
        event = mock.MagicMock()
        event.entity_type = "ExternalMessage"
        event.event_name = "ExternalMessageAttachmentCountIncremented"
        self.event_service.event_list = [event]
        self.message_repo.save()
        update_attachment_count.assert_called_once_with(event)

    def test__update_message_thread_external_source_file_id(self):
        valid_uuid = uuid4()

        self.message_repo._update_message_thread_external_source_file_id(
            thread_message_uuid=valid_uuid, file_store_id=42
        )

        db_statement = str(self.db.execute.call_args_list[0][0][0]).replace(
            "\n", ""
        )
        assert (
            db_statement
            == "UPDATE thread_message_external SET source_file_id=:source_file_id WHERE thread_message_external.id = (SELECT thread_message_external.id FROM thread_message_external JOIN thread_message ON thread_message.thread_message_external_id = thread_message_external.id WHERE thread_message.uuid = :uuid_1)"
        )

    @mock.patch("email_validator.validate_email")
    def test__generate_external_message_database_values(
        self, mock_validate_email
    ):
        # Assume that the email addresses we supply in our test suite are
        # correct, and that the email validation library has its own test suite
        mock_validate_email.side_effect = [
            True,
            True,
            email_validator.EmailNotValidError,
        ]

        event = mock.Mock()
        event.new_value.return_value = False
        event.changes = [
            {
                "key": "thread_uuid",
                "old_value": None,
                "new_value": "a12fabb5-4dfc-41ca-87f0-3911871c8f3b",
            },
            {"key": "message_slug", "old_value": None, "new_value": "test"},
            {
                "key": "message_type",
                "old_value": "external",
                "new_value": "external",
            },
            {
                "key": "created_by",
                "old_value": None,
                "new_value": {
                    "type": "Contact",
                    "entity_id": "6e8dfce8-0062-4ec3-bbcd-656d96f5a4bf",
                },
            },
            {
                "key": "created_by_displayname",
                "old_value": None,
                "new_value": "Ad Mïn",
            },
            {"key": "content", "old_value": None, "new_value": "test"},
            {
                "key": "subject",
                "old_value": None,
                "new_value": "Test Subject",
            },
            {
                "key": "external_message_type",
                "old_value": None,
                "new_value": "email",
            },
            {
                "key": "created_date",
                "old_value": None,
                "new_value": "2020-12-07T09:35:48.710622+00:00",
            },
            {
                "key": "last_modified",
                "old_value": None,
                "new_value": "2020-12-07T09:35:48.710622+00:00",
            },
            {
                "key": "message_date",
                "old_value": None,
                "new_value": "2020-12-07T09:35:48.710622+00:00",
            },
            {"key": "attachments", "old_value": None, "new_value": None},
            {
                "key": "participants",
                "old_value": [],
                "new_value": [
                    {
                        "address": "foo@example.com",
                        "role": "to",
                        "display_name": "Ad Mïn (Aanvrager)",
                        "uuid": "6e8dfce8-0062-4ec3-bbcd-656d96f5a4bf",
                    },
                    {
                        "address": "foo1@example.com",
                        "role": "bcc",
                        "display_name": "foo1@example.com",
                    },
                    {
                        "address": "test123",
                        "role": "bcc",
                        "display_name": "test123",
                    },
                ],
            },
            {"key": "direction", "old_value": None, "new_value": "outgoing"},
            {
                "key": "original_message_file",
                "old_value": None,
                "new_value": None,
            },
            {"key": "attachment_count", "old_value": None, "new_value": 0},
            {"key": "is_imported", "old_value": None, "new_value": False},
            {
                "key": "creator_type",
                "old_value": None,
                "new_value": "employee",
            },
        ]

        message = self.message_repo._generate_external_message_database_values(
            event=event
        )

        # check invalid bcc address
        assert message["participants"] == [
            {
                "address": "foo@example.com",
                "role": "to",
                "display_name": "Ad Mïn (Aanvrager)",
                "uuid": "6e8dfce8-0062-4ec3-bbcd-656d96f5a4bf",
            },
            {
                "address": "foo1@example.com",
                "role": "bcc",
                "display_name": "foo1@example.com",
            },
        ]
        assert message["type"] == "email"
        assert message["direction"] == "outgoing"
        assert message["content"] == "test"
        assert message["content"] == "test"
        assert message["subject"] == "Test Subject"

        # check for imported messages
        event.new_value.return_value = True

        message = self.message_repo._generate_external_message_database_values(
            event=event
        )
        assert message["participants"] == [
            {
                "address": "foo@example.com",
                "role": "to",
                "display_name": "Ad Mïn (Aanvrager)",
                "uuid": "6e8dfce8-0062-4ec3-bbcd-656d96f5a4bf",
            },
            {
                "address": "foo1@example.com",
                "role": "bcc",
                "display_name": "foo1@example.com",
            },
            {"address": "test123", "role": "bcc", "display_name": "test123"},
        ]
