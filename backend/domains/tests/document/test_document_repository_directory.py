# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from dataclasses import dataclass
from minty.cqrs import EventService
from sqlalchemy import sql
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains.database import schema
from zsnl_domains.document.repositories.database_queries import (
    directory_query,
    parent_directories_query,
)
from zsnl_domains.document.repositories.directory import DirectoryRepository


@dataclass
class Directory:
    id: int
    uuid: UUID
    name: str
    parent: dict
    case_id: int
    path: list


class TestDirectoryRepository:
    def setup_method(self):
        self.mock_session = mock.create_autospec("sqlalchemy.orm.Session")
        self.mock_infra = mock.MagicMock()
        self.directory_repo = DirectoryRepository(
            infrastructure_factory=self.mock_infra,
            context=None,
            event_service=EventService(
                correlation_id=str(uuid4()),
                domain="domain",
                context="context",
                user_uuid=str(uuid4()),
            ),
        )

        self.directory = Directory(
            id=10,
            uuid=uuid4(),
            name="dir1",
            parent={"uuid": uuid4()},
            case_id=1,
            path=[],
        )

        self.directory_repo.session.reset_mock()

    def test_get_parent_directories_for_directory(self):
        self.directory_repo.session.execute().fetchall.return_value = [
            self.directory
        ]
        res = self.directory_repo.get_parent_directories_for_directory(
            directory_uuid=self.directory.uuid
        )

        assert str(
            self.directory_repo.session.execute.call_args_list[1][0][0]
        ) == str(
            sql.union_all(
                directory_query().where(
                    schema.Directory.uuid == self.directory.uuid
                ),
                parent_directories_query().where(
                    schema.Directory.uuid == self.directory.uuid
                ),
            )
        )

        assert res[0].uuid == self.directory.uuid
        assert res[0].entity_id == self.directory.uuid
        assert res[0].name == self.directory.name
        assert res[0].parent == self.directory.parent
        assert res[0].case_id == self.directory.case_id
        assert res[0].path == self.directory.path
