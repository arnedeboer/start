# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import document
from zsnl_domains.document import entities

mock_label_uuids = [uuid4(), uuid4()]


@pytest.fixture
def mock_select_documents():
    mock_doc1 = mock.Mock(name="mock_doc1")
    mock_doc1.configure_mock(
        document_label_uuid=mock_label_uuids[0],
        uuid=uuid4(),
        name="empty",
        extension=".txt",
        md5="d41d8cd98f00b204e9800998ecf8427e",
        size=0,
        mimetype="text/plain",
        is_archivable=True,
        original_name="nothing.txt",
    )

    mock_select = mock.Mock(name="mock_select_documents")
    mock_select.fetchall.return_value = [mock_doc1]

    return mock_select


class Test_as_user_I_want_to_get_document_labels_for_a_case(TestBase):
    """As a user user I want see the list of document labels for a case"""

    def setup_method(self):
        self.load_query_instance(document)

        self.document_label = namedtuple(
            "DocumentLabel", ["uuid", "name", "public_name", "magic_string"]
        )(
            uuid=uuid4(),
            name="doc_att",
            public_name="doc",
            magic_string="foobar",
        )

    def test_get_document_label_for_case(self):
        self.session.execute().fetchall.return_value = [self.document_label]

        self.session.reset_mock()
        case_uuid = str(uuid4())
        result = self.qry.get_document_labels_for_case(case_uuid=case_uuid)
        db_execute_calls = self.session.execute.call_args_list

        select_statement_1 = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_2 = db_execute_calls[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 2
        assert (
            str(select_statement_1) == "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert str(select_statement_2) == (
            "SELECT zaaktype_document_kenmerken_map.case_document_uuid AS uuid, zaaktype_document_kenmerken_map.name, zaaktype_document_kenmerken_map.public_name, zaaktype_document_kenmerken_map.magic_string \n"
            "FROM zaaktype_document_kenmerken_map JOIN zaak ON zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE zaak.uuid = %(uuid_1)s"
        )

        assert isinstance(result[0], entities.DocumentLabel)
        assert result[0].uuid == self.document_label.uuid
        assert result[0].name == self.document_label.name


class Test_as_user_I_want_to_apply_labels_for_document(TestBase):
    """As a user user I want to apply labels for document"""

    def setup_method(self):
        self.load_command_instance(document)
        user_info = mock.Mock(name="user_info")
        user_info.configure_mock(
            user_uuid=uuid4(), permissions={"admin": True}
        )
        self.cmd.user_info = user_info

        self.mock_doc_label_rows = [
            mock.Mock(name="label_row_1"),
            mock.Mock(name="label_row_2"),
        ]
        self.mock_doc_label_rows[0].configure_mock(
            uuid=uuid4(),
            name="doc_att_1",
            public_name="doc_att_1",
            magic_string="magic1",
        )
        self.mock_doc_label_rows[1].configure_mock(
            uuid=uuid4(),
            name="doc_att_2",
            public_name="doc_att_2",
            magic_string="magic2",
        )

        self.mock_document_rows = [
            mock.Mock(name="mock_document_1"),
            mock.Mock(name="mock_document_2"),
        ]
        self.mock_document_rows[0].configure_mock(
            document_uuid=uuid4(), filename="doc_1", labels=[]
        )
        self.mock_document_rows[1].configure_mock(
            document_uuid=uuid4(),
            filename="doc_2",
            labels=[
                {
                    "uuid": self.mock_doc_label_rows[0].uuid,
                    "name": self.mock_doc_label_rows[0].name,
                    "public_name": self.mock_doc_label_rows[0].public_name,
                    "magic_string": self.mock_doc_label_rows[0].magic_string,
                }
            ],
        )

    def test_apply_labels_for_document(self, mock_select_documents):
        mock_check_if_user_is_admin = mock.Mock(name="check_user_admin")
        mock_check_if_user_is_admin.fetchall.return_value = [True]

        mock_get_document_1 = mock.Mock(name="get_document_1")
        mock_get_document_2 = mock.Mock(name="get_document_2")

        mock_get_document_1.fetchone.return_value = self.mock_document_rows[0]
        mock_get_document_2.fetchone.return_value = self.mock_document_rows[1]

        mock_get_labels = mock.Mock(name="get_labels")
        mock_get_labels.fetchall.return_value = self.mock_doc_label_rows
        mock_insert_file_case_document = mock.Mock(
            name="insert_file_case_document"
        )

        mock_get_case_uuid = mock.Mock(name="get_case_uuid")

        subcase_result = namedtuple("SubcaseUUIDs", ["uuid"])
        mock_get_subcase_uuids = mock.Mock(name="get_subcase_uuids")
        mock_get_subcase_uuids.fetchall.return_value = [
            subcase_result(uuid=uuid4()),
            subcase_result(uuid=uuid4()),
        ]

        mock_rows = [
            mock_get_labels,
            mock_check_if_user_is_admin,
            mock_get_document_1,
            mock_check_if_user_is_admin,
            mock_get_document_2,
            mock_get_case_uuid,  # One for each document
            mock_get_subcase_uuids,  # One for each label added
            mock_insert_file_case_document,
            mock_insert_file_case_document,
            mock_get_subcase_uuids,
            mock_insert_file_case_document,
            mock_insert_file_case_document,  # One for each document
            mock_get_case_uuid,  # One for each _set_labels call (= each doc)
            mock_get_subcase_uuids,
            mock_insert_file_case_document,
            mock_insert_file_case_document,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.cmd.apply_labels_to_document(
            document_label_uuids=[
                self.mock_doc_label_rows[0].uuid,
                self.mock_doc_label_rows[1].uuid,
            ],
            document_uuids=[
                self.mock_document_rows[0].uuid,
                self.mock_document_rows[1].uuid,
            ],
        )

        get_document_labels_statement = self.execute_queries[0]
        compiled = get_document_labels_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled) == (
            "SELECT zaaktype_document_kenmerken_map.case_document_uuid AS uuid, zaaktype_document_kenmerken_map.name, zaaktype_document_kenmerken_map.public_name, zaaktype_document_kenmerken_map.magic_string \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid IN (%(case_document_uuid_1_1)s, %(case_document_uuid_1_2)s)"
        )

        insert_label_statement = self.execute_queries[7]
        compiled = insert_label_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert (
            str(insert_label_statement)
            == "INSERT INTO file_case_document (file_id, magic_string, bibliotheek_kenmerken_id, case_id) VALUES ((SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = :uuid_1 AND file.active_version IS true), (SELECT zaaktype_document_kenmerken_map.magic_string \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid = :case_document_uuid_1), (SELECT zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid = :case_document_uuid_2), (SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = :uuid_2))"
        )

    def test_apply_labels_for_document_single_label(
        self, mock_select_documents
    ):
        mock_check_if_user_is_admin = mock.Mock(name="check_user_admin")
        mock_check_if_user_is_admin.fetchall.return_value = [True]

        mock_get_document_1 = mock.Mock(name="get_document_1")
        mock_get_document_1.fetchone.return_value = self.mock_document_rows[0]

        mock_get_labels = mock.Mock(name="get_labels")
        mock_get_labels.fetchall.return_value = [self.mock_doc_label_rows[0]]
        mock_insert_file_case_document = mock.Mock(
            name="insert_file_case_document"
        )

        mock_get_case_uuid = mock.Mock(name="get_case_uuid")

        mock_get_subcase_uuids = mock.Mock(name="get_subcase_uuids")
        mock_get_subcase_uuids.fetchall.return_value = [mock_get_case_uuid]

        mock_select_label = mock.Mock(name="select_label")
        mock_select_label.fetchone.return_value = mock.Mock(
            show_on_pip=True, show_on_website=True
        )
        mock_select_metadata_id = mock.Mock(name="mock_select_metadata")
        mock_select_metadata_id.fetchone.return_value = mock.Mock(id=123)
        mock_update_metadata = mock.Mock()

        mock_rows = [
            mock_get_labels,
            mock_check_if_user_is_admin,
            mock_get_document_1,
            mock_check_if_user_is_admin,
            mock_get_subcase_uuids,
            mock_insert_file_case_document,
            mock_select_documents,
            mock_select_label,
            mock_select_metadata_id,
            mock_update_metadata,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.cmd.apply_labels_to_document(
            document_label_uuids=[self.mock_doc_label_rows[0].uuid],
            document_uuids=[self.mock_document_rows[0].uuid],
        )

        get_document_labels_statement = self.execute_queries[0]
        compiled = get_document_labels_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled) == (
            "SELECT zaaktype_document_kenmerken_map.case_document_uuid AS uuid, zaaktype_document_kenmerken_map.name, zaaktype_document_kenmerken_map.public_name, zaaktype_document_kenmerken_map.magic_string \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid IN (%(case_document_uuid_1_1)s)"
        )

        insert_label_statement = self.execute_queries[5]
        compiled = insert_label_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert (
            str(compiled)
            == "INSERT INTO file_case_document (file_id, magic_string, bibliotheek_kenmerken_id, case_id) VALUES ((SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_1)s AND file.active_version IS true), (SELECT zaaktype_document_kenmerken_map.magic_string \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid = %(case_document_uuid_1)s), (SELECT zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid = %(case_document_uuid_2)s), (SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_2)s)) RETURNING file_case_document.id"
        )

        update_metadata_statement = self.execute_queries[8].compile(
            dialect=postgresql.dialect()
        )
        assert (
            (str(update_metadata_statement))
            == "UPDATE file_metadata SET trust_level=%(trust_level)s, origin=%(origin)s, document_category=%(document_category)s WHERE file_metadata.id = %(id_1)s"
        )

        update_defaults_statement = self.execute_queries[9].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(update_defaults_statement)
            == "UPDATE file SET metadata_id=%(metadata_id)s, publish_pip=%(publish_pip)s, publish_website=%(publish_website)s, confidential=%(confidential)s WHERE file.uuid = %(uuid_1)s AND file.active_version IS true"
        )

        assert (
            update_defaults_statement.params["uuid_1"]
            == self.mock_document_rows[0].document_uuid
        )
        assert update_defaults_statement.params["confidential"] is False


class Test_as_user_I_want_to_remove_labels_for_document(TestBase):
    """As a user user I want to remove labels for document"""

    def setup_method(self):
        self.load_command_instance(document)
        user_info = mock.MagicMock()
        user_info.configure_mock(
            user_uuid=uuid4(), permissions={"admin": True}
        )
        self.cmd.user_info = user_info

        self.mock_doc_label_rows = [mock.Mock(), mock.Mock()]
        self.mock_doc_label_rows[0].configure_mock(
            uuid=uuid4(),
            name="doc_att_1",
            public_name="doc_att_1",
            magic_string="magic1",
        )
        self.mock_doc_label_rows[1].configure_mock(
            uuid=uuid4(),
            name="doc_att_2",
            public_name="doc_att_2",
            magic_string="magic2",
        )

        self.mock_document_rows = [mock.Mock(), mock.Mock()]
        self.mock_document_rows[0].configure_mock(
            document_uuid=uuid4(),
            filename="doc_1",
            labels=[
                {
                    "uuid": self.mock_doc_label_rows[0].uuid,
                    "name": self.mock_doc_label_rows[0].name,
                    "public_name": self.mock_doc_label_rows[0].public_name,
                    "magic_string": self.mock_doc_label_rows[0].magic_string,
                },
                {
                    "uuid": self.mock_doc_label_rows[1].uuid,
                    "name": self.mock_doc_label_rows[1].name,
                    "public_name": self.mock_doc_label_rows[1].public_name,
                    "magic_string": self.mock_doc_label_rows[1].magic_string,
                },
            ],
        )
        self.mock_document_rows[1].configure_mock(
            document_uuid=uuid4(),
            filename="doc_2",
            labels=[
                {
                    "uuid": self.mock_doc_label_rows[0].uuid,
                    "name": self.mock_doc_label_rows[0].name,
                    "public_name": self.mock_doc_label_rows[0].public_name,
                    "magic_string": self.mock_doc_label_rows[0].magic_string,
                },
                {
                    "uuid": self.mock_doc_label_rows[1].uuid,
                    "name": self.mock_doc_label_rows[1].name,
                    "public_name": self.mock_doc_label_rows[1].public_name,
                    "magic_string": self.mock_doc_label_rows[1].magic_string,
                },
            ],
        )

    def test_remove_labels_from_document(self, mock_select_documents):
        mock_check_if_user_is_admin = mock.Mock(name="check_user_admin")
        mock_check_if_user_is_admin.fetchall.return_value = [True]

        mock_get_document_1 = mock.Mock()
        mock_get_document_2 = mock.Mock()
        mock_get_document_1.fetchone.return_value = self.mock_document_rows[0]
        mock_get_document_2.fetchone.return_value = self.mock_document_rows[1]

        mock_get_labels = mock.Mock()
        mock_get_labels.fetchall.return_value = [self.mock_doc_label_rows[1]]
        mock_remove_file_case_documents = mock.Mock()

        mock_get_case_uuid = mock.Mock()

        subcase_result = namedtuple("SubcaseUUIDs", ["uuid"])
        mock_get_subcase_uuids = mock.Mock(name="get_subcase_uuids")
        mock_get_subcase_uuids.fetchall.return_value = [
            subcase_result(uuid=uuid4()),
            subcase_result(uuid=uuid4()),
        ]

        mock_rows = [
            mock_get_labels,
            mock_check_if_user_is_admin,
            mock_get_document_1,
            mock_check_if_user_is_admin,
            mock_get_document_2,
            mock_get_case_uuid,
            mock_get_subcase_uuids,  # One for each label removed
            mock_remove_file_case_documents,  # Once for each document
            mock_remove_file_case_documents,
            mock_get_case_uuid,
            mock_get_subcase_uuids,  # One for each label removed
            mock_remove_file_case_documents,
            mock_remove_file_case_documents,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.cmd.remove_labels_from_document(
            document_label_uuids=[self.mock_doc_label_rows[1].uuid],
            document_uuids=[
                self.mock_document_rows[0].uuid,
                self.mock_document_rows[1].uuid,
            ],
        )

        get_document_labels_statement = self.execute_queries[0]
        compiled = get_document_labels_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled) == (
            "SELECT zaaktype_document_kenmerken_map.case_document_uuid AS uuid, zaaktype_document_kenmerken_map.name, zaaktype_document_kenmerken_map.public_name, zaaktype_document_kenmerken_map.magic_string \n"
            "FROM zaaktype_document_kenmerken_map \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid IN (%(case_document_uuid_1_1)s)"
        )

        delete_labels_statement = self.execute_queries[7]
        delete_labels_compiled = delete_labels_statement.compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(delete_labels_compiled)
            == "DELETE FROM file_case_document USING file, zaak, zaaktype_document_kenmerken_map WHERE file_case_document.file_id = file.id AND file_case_document.case_id = zaak.id AND file_case_document.bibliotheek_kenmerken_id = zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id AND file.uuid = %(uuid_1)s AND zaak.uuid = %(uuid_2)s AND zaaktype_document_kenmerken_map.case_document_uuid = %(case_document_uuid_1)s"
        )
