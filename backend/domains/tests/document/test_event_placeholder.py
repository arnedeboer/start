# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from unittest import mock
from zsnl_domains.document.repositories import event_placeholder


class TestEventPlaceholder:
    @mock.patch("sqlalchemy.orm.Session")
    def test_get_user_display_name_and_id(self, mock_session):
        assert event_placeholder.get_user_display_name_and_id(
            mock_session, None
        ) == {"id": None, "displayname": None}

        PropertiesData = namedtuple("PropertiesData", "id displayname")
        properties = PropertiesData(id=1, displayname="fake_displayname")
        exec_result = mock.MagicMock()
        exec_result.fetchone = mock.MagicMock()
        exec_result.fetchone.return_value = properties

        mock_session.execute.return_value = exec_result

        assert event_placeholder.get_user_display_name_and_id(
            mock_session, "fake_uuid"
        ) == {"id": 1, "displayname": "fake_displayname"}

    @mock.patch("sqlalchemy.orm.Session")
    @mock.patch(
        "zsnl_domains.document.repositories.event_placeholder.get_user_display_name_and_id"
    )
    def test_insert_logging_and_message_no_case_assignee(
        self, mock_display_name_and_id, mock_session
    ):
        mock_display_name_and_id.return_value = {
            "id": 1,
            "displayname": "fake_displayname",
        }

        insert_params = {
            "user_uuid": "fake_uuid",
            "case_assignee_uuid": "fake_uuid",
            "case_id": 1,
            "file_id": 2,
            "name": "fake_name",
            "created_by": "fake_created_by",
        }

        event_placeholder.insert_logging_and_message(
            mock_session, insert_params
        )
        mock_display_name_and_id.assert_called()
        mock_session.execute.assert_called_once()

    @mock.patch("sqlalchemy.orm.Session")
    @mock.patch(
        "zsnl_domains.document.repositories.event_placeholder.get_user_display_name_and_id"
    )
    def test_insert_logging_and_message_with_case_assignee(
        self, mock_display_name_and_id, mock_session
    ):
        mock_display_name_and_id.return_value = {
            "id": 1,
            "displayname": "fake_displayname",
        }

        insert_params = {
            "user_uuid": "fake_uuid",
            "case_assignee_uuid": "fake_uuid",
            "case_id": 1,
            "file_id": 2,
            "name": "fake_name",
            "created_by": "fake_created_by",
        }

        event_placeholder.insert_logging_and_message(
            mock_session, insert_params
        )
        mock_display_name_and_id.assert_called()
        mock_session.execute.assert_called_once()

        insert_params["case_assignee_uuid"] = None

        event_placeholder.insert_logging_and_message(
            mock_session, insert_params
        )
        mock_display_name_and_id.assert_called()
        with pytest.raises(AssertionError):
            mock_session.execute.assert_called_once()
