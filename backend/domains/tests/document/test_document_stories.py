# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
import pytest
import random
from collections import namedtuple
from datetime import datetime, timedelta, timezone
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.exceptions import Conflict, Forbidden, NotFound
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import document


class Test_as_an_employee_I_want_to_add_document_to_a_case(TestBase):
    """As an employee I want to add document to a case."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = str(uuid4())
        self.case_uuid = str(uuid4())
        self.document_label_uuid = str(uuid4())

        document_file = namedtuple(
            "Document",
            [
                "document_uuid",
                "store_uuid",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "filename",
                "extension",
                "accepted",
                "creator_uuid",
                "creator_displayname",
                "date_modified",
                "id",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
                "intake_owner_uuid",
                "intake_group_uuid",
                "intake_role_uuid",
            ],
        )(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

        document_label = namedtuple(
            "DocumentLabel", ["uuid", "name", "public_name", "magic_string"]
        )(
            uuid=self.document_label_uuid,
            name="doc_att",
            public_name="doc",
            magic_string="magic",
        )

        file_metadata = namedtuple("FileMetaData", ["id"])(id=26)
        case = namedtuple("Case", ["id", "status"])(id=9, status="open")

        self.mock_is_admin = mock.MagicMock()
        self.mock_is_admin.fetchall.return_value = []

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = document_file

        self.mock_get_document_labels = mock.MagicMock()
        self.mock_get_document_labels.fetchall.return_value = [document_label]

        self.mock_get_case = mock.MagicMock()
        self.mock_get_case.fetchone.return_value = case

        self.mock_get_file_metadata = mock.MagicMock()
        self.mock_get_file_metadata.fetchone.return_value = file_metadata

        self.mock_get_case_unaccepted_files = mock.MagicMock()
        self.mock_get_case_unaccepted_files.first.return_value = namedtuple(
            "CountUnacceptedFiles", "count"
        )(count=1)

        self.mock_get_case_property_document_label = mock.Mock()
        self.mock_get_case_property_document_label.fetchone.return_value = (
            namedtuple("CaseProperty", "value_v0")(value_v0=[{"md5": 123}])
        )

        case_uuid = uuid4()
        self.mock_get_case_by_document = mock.Mock()
        self.mock_get_case_by_document.scalar.return_value = case_uuid

        self.mock_get_case_uuids = mock.Mock()
        self.mock_get_case_uuids.fetchall.return_value = []

        self.mock_get_label_defaults = mock.Mock()
        self.mock_get_label_defaults.fetchone.return_value = mock.Mock(
            show_on_pip=True,
            show_on_website=False,
            trust_level="Openbaar",
            document_category="Aangifte",
            origin="Inkomend",
        )

        self.mock_update_file_metadata = mock.Mock()
        self.mock_insert_file_metadata = mock.Mock(inserted_primary_key=[123])
        self.mock_update_file = mock.Mock()
        self.mock_update_case_property_case_documents = mock.Mock()
        self.mock_update_file_case_document = mock.Mock()
        self.mock_update_case_property_document_label = mock.Mock()
        self.mock_update_case_property_unaccepted_files = mock.Mock()
        self.mock_update_object_data = mock.Mock()
        self.mock_update_case_meta = mock.Mock()

    def test_add_document_to_case(self):
        """Test add document to a case without any error"""
        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_document_labels,
            self.mock_get_case,
            self.mock_update_file,
            self.mock_get_case_unaccepted_files,
            self.mock_update_case_meta,
            self.mock_get_case_by_document,
            self.mock_get_case_uuids,
            self.mock_get_label_defaults,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.add_document_to_case(
            document_uuid=self.document_uuid,
            case_uuid=self.case_uuid,
            document_label_uuids=[self.document_label_uuid],
        )

        self.assert_has_event_name("DocumentAddedToCase")

    def test_add_document_to_case_when_case_property_with_document_label_doesnot_exists(
        self,
    ):
        """Test add document to a case when the case_property for document_label doesnot exists"""
        self.mock_get_label_defaults.fetchone.return_value = None
        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_document_labels,
            self.mock_get_case,
            self.mock_update_file,
            self.mock_get_case_unaccepted_files,
            self.mock_update_case_meta,
            self.mock_get_case_by_document,
            self.mock_get_case_uuids,
            self.mock_get_label_defaults,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_update_file,
        ]
        queries = []

        def mock_execute(query):
            queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.add_document_to_case(
            document_uuid=self.document_uuid,
            case_uuid=self.case_uuid,
            document_label_uuids=[self.document_label_uuid],
        )

        update_file = queries[4].compile(dialect=postgresql.dialect())

        assert str(update_file) == (
            "UPDATE file SET case_id=(SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s) WHERE file.uuid = %(uuid_2)s"
        )

        get_label_defaults = queries[9].compile(dialect=postgresql.dialect())
        assert str(get_label_defaults) == (
            "SELECT zaaktype_document_kenmerken_map.show_on_pip, zaaktype_document_kenmerken_map.show_on_website, file_metadata.trust_level, file_metadata.document_category, file_metadata.origin \n"
            "FROM zaaktype_document_kenmerken_map JOIN bibliotheek_kenmerken ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id JOIN file_metadata ON file_metadata.id = bibliotheek_kenmerken.file_metadata_id \n"
            "WHERE zaaktype_document_kenmerken_map.case_document_uuid = %(case_document_uuid_1)s"
        )

    def test_add_document_to_case_when_document_has_no_metadata(self):
        """Test add document to a case when the document has no meta data"""
        self.mock_get_file_metadata.fetchone.return_value = None
        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_document_labels,
            self.mock_get_case,
            self.mock_update_file,
            self.mock_get_case_unaccepted_files,
            self.mock_update_case_meta,
            self.mock_get_case_by_document,
            self.mock_get_case_uuids,
            self.mock_get_label_defaults,
            self.mock_get_file_metadata,
            self.mock_insert_file_metadata,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.add_document_to_case(
            document_uuid=self.document_uuid,
            case_uuid=self.case_uuid,
            document_label_uuids=[self.document_label_uuid],
        )

    def test_add_document_to_resolved_case(self):
        """Test add document to resolved case"""
        self.mock_get_case.fetchone.return_value = namedtuple(
            "Case", ["id", "status"]
        )(id=10, status="resolved")

        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_document_labels,
            self.mock_get_case,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_update_file,
            self.mock_update_case_property_case_documents,
            self.mock_update_file_case_document,
            self.mock_get_case_property_document_label,
            self.mock_update_case_property_document_label,
            self.mock_get_case_unaccepted_files,
            self.mock_update_case_property_unaccepted_files,
            self.mock_update_object_data,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.add_document_to_case(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                document_label_uuids=[self.document_label_uuid],
            )

        assert excinfo.value.args == (
            f"Document cannot be added to resolved case with uuid '{self.case_uuid}'",
            "document/cannot_add_to_resolved_case",
        )

    def test_add_document_to_case_but_document_is_already_linked_to_a_case(
        self,
    ):
        """Test add document to a case when already a case was linked to the document"""
        case_uuid = str(uuid4())
        document = namedtuple(
            "Document",
            [
                "document_uuid",
                "store_uuid",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "filename",
                "extension",
                "accepted",
                "creator_uuid",
                "creator_displayname",
                "date_modified",
                "id",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
                "intake_owner_uuid",
                "intake_group_uuid",
                "intake_role_uuid",
            ],
        )(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=case_uuid,
            case_display_number=9,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )
        self.mock_get_document.fetchone.return_value = document

        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_document_labels,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.add_document_to_case(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                document_label_uuids=[self.document_label_uuid],
            )

        assert excinfo.value.args == (
            f"Document with uuid '{self.document_uuid}' is already linked to a case with uuid '{case_uuid}'.",
            "document/already_linked_to_a_case",
        )

    def test_add_document_to_case_but_document_cannot_be_found(self):
        """Test add document to a case but document with given uuid is not accessable to the user/document doesn't exists"""

        self.mock_get_document.fetchone.return_value = None

        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_document_labels,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.add_document_to_case(
                document_uuid=self.document_uuid,
                case_uuid=self.case_uuid,
                document_label_uuids=[self.document_label_uuid],
            )

        assert excinfo.value.args == (
            f"No document found with uuid={self.document_uuid}",
            "document/not_found",
        )


class Test_as_an_employee_I_want_to_update_a_document(TestBase):
    """As an employee I want to update a document."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = str(uuid4())

        document_file = namedtuple(
            "Document",
            [
                "document_uuid",
                "store_uuid",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "filename",
                "extension",
                "accepted",
                "creator_uuid",
                "creator_displayname",
                "date_modified",
                "id",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "confidentiality",
                "document_category",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
                "intake_owner_uuid",
                "intake_group_uuid",
                "intake_role_uuid",
            ],
        )(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

        file_metadata = namedtuple("FileMetaData", ["id"])(id=26)

        self.mock_is_admin = mock.MagicMock()
        self.mock_is_admin.fetchall.return_value = []

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = document_file

        self.mock_get_file_metadata = mock.MagicMock()
        self.mock_get_file_metadata.fetchone.return_value = file_metadata

        self.mock_update_file_metadata = mock.MagicMock()
        self.mock_update_file = mock.MagicMock()

        self.mock_get_contact = mock.Mock()
        mock_contact = mock.Mock()
        mock_contact.id = random.randint(1, 1000)
        mock_contact.subject_type = "medewerker"
        self.mock_get_contact.fetchone.return_value = mock_contact

    def test_update_document(self):
        """Test update document"""
        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_get_contact,
            self.mock_update_file,
        ]

        queries = []

        def mock_execute(query):
            queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.update_document(
            document_uuid=self.document_uuid,
            origin="Intern",
            origin_date="2020-05-13",
            description="description",
        )

        self.assert_has_event_name("DocumentUpdated")

        update_metadata_query = queries[3].compile(
            dialect=postgresql.dialect()
        )
        assert (
            (str(update_metadata_query))
            == "UPDATE file_metadata SET description=%(description)s, trust_level=%(trust_level)s, origin=%(origin)s, document_category=%(document_category)s, origin_date=%(origin_date)s WHERE file_metadata.id = %(id_1)s"
        )

        get_contact_query = queries[4].compile(dialect=postgresql.dialect())
        assert str(get_contact_query) == (
            "SELECT subject.id, %(param_1)s AS subject_type \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s AND subject.subject_type = %(subject_type_1)s UNION ALL SELECT natuurlijk_persoon.id, %(param_2)s AS subject_type \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_2)s UNION ALL SELECT bedrijf.id, %(param_3)s AS subject_type \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = %(uuid_3)s"
        )
        assert get_contact_query.params == {
            "param_1": "medewerker",
            "param_2": "natuurlijk_persoon",
            "param_3": "bedrijf",
            "subject_type_1": "employee",
            "uuid_1": self.cmd.user_uuid,
            "uuid_2": self.cmd.user_uuid,
            "uuid_3": self.cmd.user_uuid,
        }

        update_file_query = queries[5].compile(dialect=postgresql.dialect())
        assert (
            str(update_file_query)
            == "UPDATE file SET name=%(name)s, metadata_id=%(metadata_id)s, date_modified=(CURRENT_TIMESTAMP AT TIME ZONE %(current_timestamp_1)s), modified_by=%(modified_by)s WHERE file.uuid = %(uuid_1)s"
        )
        assert update_file_query.params == {
            "current_timestamp_1": "UTC",
            "metadata_id": 26,
            "name": "word_document",
            "uuid_1": self.document_uuid,
            "modified_by": "betrokkene-medewerker-"
            + str(self.mock_get_contact.fetchone().id),
        }

    def test_update_document_with_no_origin_date(self):
        """Test update document when origin date is null"""
        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_file_metadata,
            self.mock_update_file_metadata,
            self.mock_get_contact,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.update_document(
            document_uuid=self.document_uuid,
            origin="Intern",
            origin_date=None,
        )

        self.assert_has_event_name("DocumentUpdated")


class Test_as_an_employee_I_want_to_assign_document_to_user(TestBase):
    """As an employee I want to update a document."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = str(uuid4())

        documen_file = namedtuple(
            "Document",
            [
                "document_uuid",
                "store_uuid",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "filename",
                "extension",
                "accepted",
                "creator_uuid",
                "creator_displayname",
                "date_modified",
                "id",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "confidentiality",
                "document_category",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
                "intake_owner_uuid",
                "intake_group_uuid",
                "intake_role_uuid",
            ],
        )(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

        subject = namedtuple("Subject", ["id"])(id=3)

        self.mock_is_admin = mock.MagicMock()
        self.mock_is_admin.fetchall.return_value = []

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = documen_file

        self.mock_get_subject = mock.MagicMock()
        self.mock_get_subject.fetchone.return_value = subject

        self.mock_update_file = mock.MagicMock()

    def test_assign_document_to_user(self):
        """Test assign document to user"""
        user_uuid = str(uuid4())
        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_subject,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.assign_document_to_user(
            document_uuid=self.document_uuid, user_uuid=user_uuid
        )

        self.assert_has_event_name("DocumentAssignedToUser")

    def test_assign_document_to_user_subject_not_found(self):
        """Test assign document to user"""
        user_uuid = str(uuid4())
        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_get_subject,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.mock_get_subject.fetchone.return_value = None
            self.cmd.assign_document_to_user(
                document_uuid=self.document_uuid, user_uuid=user_uuid
            )

        assert excinfo.value.args == (
            f"Subject with uuid '{user_uuid}' not found",
            "subject/not_found",
        )

    def test_reject_assigned_document(self):
        "Test rejecting a document's intake assignment"

        document_file = mock.Mock()
        document_file.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=uuid4(),
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

        mock_get_document = mock.Mock()
        mock_get_document.fetchone.return_value = document_file

        mock_rows = [
            self.mock_is_admin,
            mock_get_document,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute.side_effect = mock_execute
        self.session.reset_mock()

        self.cmd.reject_assigned_document(
            document_uuid=self.document_uuid,
            rejection_reason="I don't like it.",
        )

        self.assert_has_event_name("DocumentAssignmentRejected")

    def test_reject_assigned_document_failure(self):
        "Test rejecting a document's intake assignment"

        document_file = mock.Mock()
        document_file.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

        mock_get_document = mock.Mock()
        mock_get_document.fetchone.return_value = document_file

        mock_rows = [
            self.mock_is_admin,
            mock_get_document,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute.side_effect = mock_execute
        self.session.reset_mock()

        with pytest.raises(Conflict):
            self.cmd.reject_assigned_document(
                document_uuid=self.document_uuid,
                rejection_reason="I don't like it.",
            )

        document_file.intake_role_uuid = uuid4()
        document_file.case_uuid = uuid4()

        mock_rows = [
            self.mock_is_admin,
            mock_get_document,
            self.mock_update_file,
        ]

        with pytest.raises(Conflict):
            self.cmd.reject_assigned_document(
                document_uuid=self.document_uuid,
                rejection_reason="I don't like it.",
            )


class Test_as_an_employee_I_want_to_assign_document_to_role(TestBase):
    """As an employee I want to update a document."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = str(uuid4())

        documen_file = namedtuple(
            "Document",
            [
                "document_uuid",
                "store_uuid",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "filename",
                "extension",
                "accepted",
                "creator_uuid",
                "creator_displayname",
                "date_modified",
                "id",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "confidentiality",
                "document_category",
                "intake_owner_uuid",
                "intake_role_uuid",
                "intake_group_uuid",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
            ],
        )(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            intake_role_uuid=None,
            intake_group_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
        )

        self.mock_is_admin = mock.MagicMock()
        self.mock_is_admin.fetchall.return_value = []

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = documen_file

        self.mock_update_file = mock.MagicMock()

    def test_assign_document_to_role(self):
        """Test assign document to role"""
        role_uuid = str(uuid4())
        group_uuid = str(uuid4())

        mock_rows = [
            self.mock_is_admin,
            self.mock_get_document,
            self.mock_update_file,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.cmd.assign_document_to_role(
            document_uuid=self.document_uuid,
            group_uuid=group_uuid,
            role_uuid=role_uuid,
        )

        self.assert_has_event_name("DocumentAssignedToRole")


class Test_as_an_employee_I_want_to_get_thumbnail_for_a_document(TestBase):
    """As an employee I want to get thumbnail for document."""

    def setup_method(self):
        self.load_query_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.qry.user_info = user_info

        self.document_uuid = str(uuid4())
        self.thumbnail_uuid = uuid4()

        self.document_file = mock.MagicMock()
        self.document_file.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size="123",
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=None,
            case_uuid=None,
            case_display_number=None,
            filename="word_document",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            intake_role_uuid=None,
            intake_group_uuid=None,
            thumbnail_uuid=self.thumbnail_uuid,
            thumbnail_mimetype="image/png",
            thumbnail_storage_location=["minio"],
        )

        self.mock_is_admin = mock.MagicMock()
        self.mock_is_admin.fetchall.return_value = []

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = self.document_file

    def test_get_document_thumbnail(self):
        mock_rows = [self.mock_is_admin, self.mock_get_document]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.qry.get_document_thumbnail_link(document_uuid=self.document_uuid)

        self.qry.get_repository("document")._get_infrastructure(
            "s3"
        ).get_download_url.assert_called_once_with(
            uuid=self.thumbnail_uuid,
            storage_location="minio",
            mime_type="image/png",
            filename=None,
            download=False,
        )

    def test_get_document_thumbnail_where_thumbnail_not_found(self):
        self.document_file.thumbnail_uuid = None
        self.mock_get_document.fetchone.return_value = self.document_file

        mock_rows = [self.mock_is_admin, self.mock_get_document]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_document_thumbnail_link(
                document_uuid=self.document_uuid
            )

        assert excinfo.value.args == (
            f"No thumbnail found for document with uuid {self.document_uuid}",
            "document/thumbnail_not_found",
        )


class Test_as_an_employee_I_want_to_edit_document_with_ZOHO(TestBase):
    """Test document edit with ZOHO"""

    def setup_method(self):
        self.load_query_instance(
            document, {"zoho": mock.MagicMock(), "redis": mock.MagicMock()}
        )

        self.document_uuid = str(uuid4())
        self.directory_uuid = uuid4()
        self.document = mock.MagicMock()
        self.document.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=self.directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )

        self.mock_get_user_name = mock.MagicMock()
        self.mock_get_user_name.fetchone.return_value = ["Admin"]

        self.mock_is_doc_edit_enabled = mock.MagicMock()
        self.mock_is_doc_edit_enabled.fetchone.return_value = [1]

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = self.document

        mock_rows = [
            self.mock_get_user_name,
            self.mock_is_doc_edit_enabled,
            self.mock_get_document,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.user_info = mock.MagicMock()
        self.user_info.configure_mock(user_uuid=uuid4())

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "s3"
        ).get_download_url.return_value = "document_download_url"

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "zoho"
        ).edit_document.return_value = "zoho-edit-url"

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_edit_document_url_for_zoho(self, mock_secrets):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        res = self.qry.get_edit_document_url(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
        )

        assert res == "zoho-edit-url"
        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "redis"
        ).set.assert_called_once_with(
            f"zoho_authentication_token:edit_document_online:test:{self.document_uuid}",
            json.dumps(
                {
                    "token": "urlsafe_token",
                    "user_uuid": str(self.user_info.user_uuid),
                    "case_uuid": str(self.document.case_uuid),
                }
            ),
            3600,
        )

        document_info = {
            "document_name": "test_doc.docx",
            "document_id": f"{self.document_uuid}-test",
        }

        callback_settings = {
            "save_format": "docx",
            "save_url": "save_url",
            "context_info": "Edit document",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "zoho",
            },
        }

        user_info = {
            "user_id": self.user_info.user_uuid,
            "display_name": "Admin",
        }

        options = {
            "editor_settings": '{ "unit": "in", "language": "nl", "view": "pageview" }',
            "permissions": '{ "document.export": true, "document.print": true, "document.edit": true, "review.changes.resolve": false, "review.comment": true, "collab.chat": true }',
            "callback_settings": str(callback_settings),
            "document_info": str(document_info),
            "user_info": str(user_info),
            "document_defaults": '{ "orientation": "portrait", "paper_size": "A4", "font_name": "Lato", "font_size": 12, "track_changes": "disabled" }',
            "url": "document_download_url",
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "zoho"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_edit_document_url_failed_when_document_has_no_case(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.case_uuid = None
        self.mock_get_document.fetchone.return_value = self.document

        with pytest.raises(Conflict) as excinfo:
            self.qry.get_edit_document_url(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
            )

        assert excinfo.value.args == (
            "Document is not linked to any case",
            "document/case/not_exists",
        )

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_edit_document_url_failed_with_payment_required_error(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.mock_is_doc_edit_enabled.fetchone.return_value = [0]
        with pytest.raises(Forbidden) as excinfo:
            self.qry.get_edit_document_url(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
            )

        assert excinfo.value.args == (
            "Payment required to use document editor",
            "document/edit/not_enabled",
        )


class Test_as_an_employee_I_want_to_get_wopi_configuration_for_document(
    TestBase
):
    """As an employee I want to get edit_document with Ms Online"""

    def setup_method(self):
        self.load_query_instance(
            document, {"wopi": mock.MagicMock(), "redis": mock.MagicMock()}
        )

        self.document_uuid = str(uuid4())
        self.directory_uuid = uuid4()
        self.document = mock.MagicMock()
        self.document.configure_mock(
            document_uuid=self.document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=self.directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2019-05-02",
            id=1,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2020-03-03",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )

        self.mock_is_user_admin = mock.MagicMock()
        self.mock_is_user_admin.fetchall.return_value = [1]

        self.mock_get_document = mock.MagicMock()
        self.mock_get_document.fetchone.return_value = self.document

        self.mock_get_user_name = mock.MagicMock()
        self.mock_get_user_name.fetchone.return_value = ["Admin"]

        mock_rows = [
            self.mock_is_user_admin,
            self.mock_get_document,
            self.mock_get_user_name,
        ]

        self.execute_queries = []

        def mock_execute(query):
            self.execute_queries.append(query)
            return mock_rows.pop(0)

        self.session.execute = mock_execute

        self.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"pip_user": False}
        )

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "s3"
        ).get_download_url.return_value = "document_download_url"

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.return_value = {
            "favIconUrl": "favIconUrl",
            "urlsrc": "urlsrc",
            "access_token": "access_token",
            "access_token_ttl": "access_token_ttl",
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "redis"
        ).get.return_value = json.dumps(
            {
                "token": "urlsafe_token",
                "user_uuid": str(self.user_info.user_uuid),
                "case_uuid": str(self.document.case_uuid),
                "user_permissions": json.dumps(self.user_info.permissions),
            }
        ).encode(
            "utf-8"
        )

    def test_get_wopi_configuration_for_document(self):
        res = self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
        )

        assert res == {
            "fav_icon_url": "favIconUrl",
            "action_url": "urlsrc",
            "access_token": "access_token",
            "access_token_ttl": "access_token_ttl",
        }

        document_info = {
            "extension": "docx",
            "filename": "test_doc.docx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "msonline",
                "directory_uuid": str(self.directory_uuid),
                "user_uuid": str(self.user_info.user_uuid),
            },
            "close_url": "close_url",
            "host_page_url": "host_page_url",
        }

        options = {
            "document_uuid": self.document_uuid,
            "context": "test.zaaksyteem.nl",
            "app": "Word",
            "action": "edit",
            "user_uuid": str(self.user_info.user_uuid),
            "user_display_name": "Admin",
            "owner_uuid": str(self.document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_for_document_with_no_token_data(
        self, mock_secrets
    ):
        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "redis"
        ).get.return_value = None

        mock_secrets.token_urlsafe.return_value = "urlsafe_token"
        self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
        )

        assert (
            len(
                self.qry.repository_factory.infrastructure_factory.get_infrastructure(
                    None, "redis"
                ).set.call_args_list
            )
            == 1
        )

        set_args = self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "redis"
        ).set.call_args[
            0
        ]

        assert (
            set_args[0]
            == f"msonline:edit_document_online:test:{self.document_uuid}:{self.user_info.user_uuid}"
        )
        assert json.loads(set_args[1]) == {
            "token": "urlsafe_token",
            "user_info": {
                "type": "UserInfo",
                "user_uuid": str(self.user_info.user_uuid),
                "permissions": self.user_info.permissions,
            },
            "case_uuid": str(self.document.case_uuid),
            "user_uuid": str(self.user_info.user_uuid),
            "user_permissions": self.user_info.permissions,
        }
        assert set_args[2] == 3600

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_for_wopitest_files(self, mock_secrets):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.extension = ".wopitestx"
        self.mock_get_document.fetchone.return_value = self.document

        self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
        )

        document_info = {
            "extension": "wopitestx",
            "filename": "test_doc.wopitestx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "msonline",
                "directory_uuid": str(self.directory_uuid),
                "user_uuid": str(self.user_info.user_uuid),
            },
            "close_url": "close_url",
            "host_page_url": "host_page_url",
        }

        options = {
            "document_uuid": self.document_uuid,
            "context": "test.zaaksyteem.nl",
            "app": "WopiTest",
            "action": "edit",
            "user_uuid": str(self.user_info.user_uuid),
            "user_display_name": "Admin",
            "owner_uuid": str(self.document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_for_ppt_files(self, mock_secrets):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.extension = ".pptx"
        self.mock_get_document.fetchone.return_value = self.document

        self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
        )

        document_info = {
            "extension": "pptx",
            "filename": "test_doc.pptx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "msonline",
                "directory_uuid": str(self.directory_uuid),
                "user_uuid": str(self.user_info.user_uuid),
            },
            "close_url": "close_url",
            "host_page_url": "host_page_url",
        }

        options = {
            "document_uuid": self.document_uuid,
            "context": "test.zaaksyteem.nl",
            "app": "PowerPoint",
            "action": "edit",
            "user_uuid": str(self.user_info.user_uuid),
            "user_display_name": "Admin",
            "owner_uuid": str(self.document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_for_excel_files(self, mock_secrets):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.extension = ".xlsx"
        self.mock_get_document.fetchone.return_value = self.document

        self.qry.get_ms_wopi_configuration(
            document_uuid=self.document_uuid,
            user_info=self.user_info,
            save_url="save_url",
            close_url="close_url",
            host_page_url="host_page_url",
            context="test.zaaksyteem.nl",
        )

        document_info = {
            "extension": "xlsx",
            "filename": "test_doc.xlsx",
            "version": 1,
            "size": 1056,
            "file_url": "document_download_url",
        }

        callback_settings = {
            "save_url": "save_url",
            "save_url_params": {
                "document_uuid": self.document_uuid,
                "authentication_token": "urlsafe_token",
                "editor_type": "msonline",
                "directory_uuid": str(self.directory_uuid),
                "user_uuid": str(self.user_info.user_uuid),
            },
            "close_url": "close_url",
            "host_page_url": "host_page_url",
        }

        options = {
            "document_uuid": self.document_uuid,
            "context": "test.zaaksyteem.nl",
            "app": "Excel",
            "action": "edit",
            "user_uuid": str(self.user_info.user_uuid),
            "user_display_name": "Admin",
            "owner_uuid": str(self.document.creator_uuid),
            "callback_settings": callback_settings,
            "file_info": document_info,
            "business_user": True,
        }

        self.qry.repository_factory.infrastructure_factory.get_infrastructure(
            None, "wopi"
        ).edit_document.assert_called_once_with(options)

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_failed_when_document_is_not_supported_by_wopi(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.document.extension = ".png"
        self.mock_get_document.fetchone.return_value = self.document

        with pytest.raises(Forbidden) as excinfo:
            self.qry.get_ms_wopi_configuration(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
                close_url="close_url",
                host_page_url="host_page_url",
                context="test.zaaksyteem.nl",
            )

        assert excinfo.value.args == (
            "Document with extension '.png' cannot be edited with Microsoft Online.",
            "document/cannot_be_edited",
        )

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_failed_when_user_cannot_be_found(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"

        self.mock_get_user_name.fetchone.return_value = []

        with pytest.raises(NotFound) as excinfo:
            self.qry.get_ms_wopi_configuration(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
                close_url="close_url",
                host_page_url="host_page_url",
                context="test.zaaksyteem.nl",
            )

        assert excinfo.value.args == (
            f"No user found with uuid '{self.user_info.user_uuid}'",
            "user/not_found",
        )

    @mock.patch("zsnl_domains.document.repositories.document.secrets")
    def test_get_wopi_configuration_failed_when_document_has_no_case(
        self, mock_secrets
    ):
        mock_secrets.token_urlsafe.return_value = "urlsafe_token"
        self.document.case_uuid = None
        self.mock_get_document.fetchone.return_value = self.document

        with pytest.raises(Conflict) as excinfo:
            self.qry.get_ms_wopi_configuration(
                document_uuid=self.document_uuid,
                user_info=self.user_info,
                save_url="save_url",
                close_url="close_url",
                host_page_url="host_page_url",
                context="test.zaaksyteem.nl",
            )

        assert excinfo.value.args == (
            "Document is not linked to any case",
            "document/case/not_exists",
        )


class Test_Bugfix_External_Document_Upload_Removes_Document_Label(TestBase):
    def setup_method(self):
        self.load_command_instance(document)

    def test_document_autoaccept(self):
        """ "
        Documents that are updated by an external editing service are updated
        using create_document with `auto_accept=True`
        """

        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )
        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )

        mock_directory = mock.Mock(id=123)
        mock_old_active_file = mock.Mock(
            id=122, uuid=document_uuid, version=6, root_file_id=None
        )
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            None,  # Get "root file" id
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            None,  # Get "root file" id, again
            mock_old_active_file,  # Old version to update
            mock_user_result,  # User to use for "created by"
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=True,
            editor_update=False,
        )

        update = str(self.session.execute.call_args_list[14][0][0])
        assert update == (
            "UPDATE file_case_document SET file_id=(SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = :uuid_1 AND file.active_version IS true) WHERE file_case_document.file_id IN (SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = :uuid_2)"
        )

    def test_create_document_second_version(self):
        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)

        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )

        mock_directory = mock.Mock(id=123)
        mock_old_active_file = mock.Mock(
            id=122, uuid=document_uuid, version=6, root_file_id=None
        )
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            mock.Mock(root_file_id=150),  # Get "root file" id
            None,  # get_filestore_by_uuid
            None,
            mock_directory,
            mock_case,  # _get_case_id_assignee_uuid
            mock.Mock(root_file_id=150),  # Get "root file" id, again
            mock_old_active_file,  # Old version to update
            mock_user_result,  # User to use for "created by"
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=False,
            editor_update=False,
        )

        insert = str(self.session.execute.call_args_list[14][0][0])
        assert insert == (
            "INSERT INTO file (filestore_id, name, extension, root_file_id, version, case_id, directory_id, creation_reason, accepted, reject_to_queue, is_duplicate_name, publish_pip, publish_website, date_created, created_by, date_modified, modified_by, destroyed, active_version, is_duplicate_of, queue, document_status, uuid, confidential, skip_intake) VALUES (:filestore_id, :name, :extension, :root_file_id, :version, :case_id, :directory_id, :creation_reason, :accepted, :reject_to_queue, :is_duplicate_name, :publish_pip, :publish_website, :date_created, :created_by, :date_modified, :modified_by, :destroyed, :active_version, :is_duplicate_of, :queue, :document_status, :uuid, :confidential, :skip_intake)"
        )

    def test_document_autoaccept_notfound(self):
        """ "
        Documents that are updated by an external editing service are updated
        using create_document with `auto_accept=True`
        """

        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )
        mock_directory = mock.Mock(id=123)

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            None,  # Get "root file" id
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            None,  # Get "root file" id, again
            None,  # Old version to update
        ]
        self.session.reset_mock()

        with pytest.raises(Conflict):
            self.cmd.create_document(
                document_uuid=str(document_uuid),
                filename="some_file.pdf",
                store_uuid=str(store_uuid),
                directory_uuid=str(directory_uuid),
                mimetype="application/pdf",
                size=1234,
                storage_location="earth",
                md5="d41d8cd98f00b204e9800998ecf8427e",
                magic_strings=[],
                case_uuid=str(case_uuid),
                skip_intake=False,
                auto_accept=True,
            )


class Test_as_an_employee_I_want_to_replace_a_document(TestBase):
    def setup_method(self):
        self.load_command_instance(document)

    def test_document_replace(self):
        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()
        replaces = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )
        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )
        mock_directory = mock.Mock(id=123)
        mock_old_active_file = mock.Mock(
            id=122, uuid=document_uuid, version=6, root_file_id=None
        )
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            None,  # Get "root file" id
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            None,  # Get "root file" id, again
            mock_old_active_file,  # Old version to update
            mock_user_result,  # User to use for "created by"
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=True,
            replaces=str(replaces),
            editor_update=False,
        )
        call_list = self.session.execute.call_args_list

        update_statement = call_list[14][0][0]

        update_stmt = update_statement.compile(dialect=postgresql.dialect())

        assert str(update_stmt) == (
            "UPDATE file_case_document SET file_id=(SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_1)s AND file.active_version IS true) WHERE file_case_document.file_id IN (SELECT file.id \n"
            "FROM file \n"
            "WHERE file.uuid = %(uuid_2)s)"
        )
        assert update_stmt.params["uuid_1"] == str(replaces)


class Test_as_an_employee_I_want_to_move_documents(TestBase):
    def setup_method(self):
        self.load_command_instance(document)
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

    def test_move_directory_to_another_directory(self):
        source_directory_uuid_1 = uuid4()
        source_directory_uuid_2 = uuid4()
        desstination_directory_uuid = uuid4()
        source_directories = [str(uuid4()), str(uuid4)]
        sub_directory_uuid = str(uuid4())
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=case_id,
            original_name="Another Name",
            path=[],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_directory_1 = mock.Mock()
        source_directory_1.configure_mock(
            id=1,
            name="Another Name1",
            case_id=case_id,
            original_name="Another Name1",
            path=[],
            uuid=source_directory_uuid_1,
            parent=None,
        )
        source_directory_2 = mock.Mock()
        source_directory_2.configure_mock(
            id=2,
            name="Another Name2",
            case_id=case_id,
            original_name="Another Name2",
            path=[],
            uuid=source_directory_uuid_2,
            parent=None,
        )
        sub_directory_1 = mock.Mock()
        sub_directory_1.configure_mock(
            id=3,
            name="Another Name3",
            case_id=case_id,
            original_name="Another Name3",
            path=[2],
            uuid=sub_directory_uuid,
            parent=None,
        )
        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=10),
            source_directory_1,
            namedtuple("directory", "id")(id=1),
            source_directory_2,
            namedtuple("directory", "id")(id=2),
        ]
        self.session.execute().fetchall.side_effect = [[], [sub_directory_1]]
        self.session.reset_mock()
        self.cmd.move_to_directory(
            destination_directory_uuid=desstination_directory_uuid,
            source_directories=source_directories,
        )

        call_list = self.session.execute.call_args_list

        update_statement = call_list[8][0][0]
        update_stmt = update_statement.compile(dialect=postgresql.dialect())

        assert str(update_stmt) == (
            "UPDATE directory SET path=ARRAY[%(param_1)s] WHERE directory.uuid = %(uuid_1)s"
        )

        select_statement = call_list[9][0][0]
        select_stmt = select_statement.compile(dialect=postgresql.dialect())

        assert str(select_stmt) == (
            "SELECT directory.name AS name, directory.case_id AS case_id, directory.path AS path, directory.uuid AS uuid, %(param_1)s AS parent \n"
            "FROM directory \n"
            "WHERE (SELECT directory.id \n"
            "FROM directory \n"
            "WHERE directory.uuid = %(uuid_1)s) = any(directory.path)"
        )
        update_statement = call_list[10][0][0]
        update_stmt = update_statement.compile(dialect=postgresql.dialect())

        assert str(update_stmt) == (
            "UPDATE directory SET path=ARRAY[%(param_1)s, %(param_2)s] WHERE directory.uuid = %(uuid_1)s"
        )

    def test_move_directory_to_root(self):
        source_directory_uuid_1 = uuid4()
        source_directories = [source_directory_uuid_1]
        case_id = 100

        source_directory_1 = mock.Mock()
        source_directory_1.configure_mock(
            id=1,
            name="Another Name1",
            case_id=case_id,
            original_name="Another Name1",
            path=[],
            uuid=source_directory_uuid_1,
            parent=None,
        )
        self.session.execute().fetchone.side_effect = [
            source_directory_1,
            namedtuple("directory", "id")(id=1),
        ]
        self.session.reset_mock()
        self.cmd.move_to_directory(source_directories=source_directories)

        call_list = self.session.execute.call_args_list

        select_statement = call_list[3][0][0]
        select_stmt = select_statement.compile(dialect=postgresql.dialect())

        assert str(select_stmt) == (
            "SELECT directory.name AS name, directory.case_id AS case_id, directory.path AS path, directory.uuid AS uuid, %(param_1)s AS parent \n"
            "FROM directory \n"
            "WHERE (SELECT directory.id \n"
            "FROM directory \n"
            "WHERE directory.uuid = %(uuid_1)s) = any(directory.path)"
        )

    def test_move_directory_to_different_case(self):
        source_directory_uuid_1 = uuid4()
        source_directory_uuid_2 = uuid4()
        desstination_directory_uuid = uuid4()
        source_directories = [source_directory_uuid_1]
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=200,
            original_name="Another Name",
            path=[],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_directory_1 = mock.Mock()
        source_directory_1.configure_mock(
            id=1,
            name="Another Name1",
            case_id=case_id,
            original_name="Another Name1",
            path=[],
            uuid=source_directory_uuid_1,
            parent=None,
        )
        source_directory_2 = mock.Mock()
        source_directory_2.configure_mock(
            id=2,
            name="Another Name2",
            case_id=case_id,
            original_name="Another Name2",
            path=[],
            uuid=source_directory_uuid_2,
            parent=None,
        )
        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=10),
            source_directory_1,
            namedtuple("directory", "id")(id=1),
        ]
        with pytest.raises(Conflict) as excinfo:
            self.cmd.move_to_directory(
                destination_directory_uuid=desstination_directory_uuid,
                source_directories=source_directories,
            )
        assert excinfo.value.args == (
            f"The destination directories case_id does not match the given directories case_id ({destination_directory.case_id} != {source_directory_1.case_id})",
            "domains/document/move_to_directory/directory_case_id_mismatch",
        )

    def test_move_directory_to_child_fails(self):
        source_directory_uuid_1 = uuid4()
        source_directory_uuid_2 = uuid4()
        desstination_directory_uuid = uuid4()
        source_directories = [source_directory_uuid_1]
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=case_id,
            original_name="Another Name",
            path=[1],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_directory_1 = mock.Mock()
        source_directory_1.configure_mock(
            id=1,
            name="Another Name1",
            case_id=case_id,
            original_name="Another Name1",
            path=[],
            uuid=source_directory_uuid_1,
            parent=None,
        )
        source_directory_2 = mock.Mock()
        source_directory_2.configure_mock(
            id=2,
            name="Another Name2",
            case_id=case_id,
            original_name="Another Name2",
            path=[],
            uuid=source_directory_uuid_2,
            parent=None,
        )
        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=10),
            source_directory_1,
            namedtuple("directory", "id")(id=1),
        ]
        with pytest.raises(Conflict) as excinfo:
            self.cmd.move_to_directory(
                destination_directory_uuid=desstination_directory_uuid,
                source_directories=source_directories,
            )
        assert excinfo.value.args == (
            "Can not move directory to itself or its child directories",
            "domains/document/move_to_directory/moving_to_self",
        )

    def test_move_document_to_directory(self):
        source_doc_uuid_1 = uuid4()
        desstination_directory_uuid = uuid4()
        source_documents = [source_doc_uuid_1]
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=case_id,
            original_name="Another Name",
            path=[],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_doc_1 = mock.Mock()
        source_doc_1.configure_mock(
            id=source_doc_uuid_1,
            document_uuid=source_doc_uuid_1,
            filename="fake_filename",
            extension="fake_extension",
            store_uuid="fake_store_uuid",
            directory_uuid=None,
            case_uuid="fake_case_uuid",
            case_display_number=case_id,
            mimetype="fake_mimetype",
            size=1,
            storage_location="fake_storage_location",
            md5="fake_md5",
            is_archivable="fake_is_archivable",
            virus_scan_status="fake_virus_scan_status",
            accepted="fake_accepted",
            date_modified="fake_date_modified",
            thumbnail="fake_thumbnail",
            creator_uuid="fake_creator_uuid",
            creator_displayname="fake_creator_displayname",
            type="fake_type",
            properties='{"displayname": "fake_displayname"}',
            preview_uuid=uuid4(),
            preview_storage_location=None,
            preview_mimetype=None,
            description="desc",
            origin="Intern",
            origin_date="2015-05-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
        )

        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=10),
            source_doc_1,
            namedtuple("directory_result", "id")(id=10),
        ]
        self.session.reset_mock()
        self.cmd.move_to_directory(
            destination_directory_uuid=desstination_directory_uuid,
            source_documents=source_documents,
        )

        call_list = self.session.execute.call_args_list

        update_statement = call_list[5][0][0]
        update_stmt = update_statement.compile(dialect=postgresql.dialect())

        assert str(update_stmt) == (
            "UPDATE file SET directory_id=%(directory_id)s WHERE file.date_deleted IS NULL AND file.uuid = %(uuid_1)s"
        )

    def test_move_document_to_different_case(self):
        source_doc_uuid_1 = uuid4()
        desstination_directory_uuid = uuid4()
        source_documents = [source_doc_uuid_1]
        case_id = 100

        destination_directory = mock.Mock()
        destination_directory.configure_mock(
            id=10,
            name="Another Name",
            case_id=20,
            original_name="Another Name",
            path=[],
            uuid=desstination_directory_uuid,
            parent=None,
        )
        source_doc_1 = mock.Mock()
        source_doc_1.configure_mock(
            id=source_doc_uuid_1,
            document_uuid=source_doc_uuid_1,
            filename="fake_filename",
            extension="fake_extension",
            store_uuid="fake_store_uuid",
            directory_uuid=None,
            case_uuid="fake_case_uuid",
            case_display_number=case_id,
            mimetype="fake_mimetype",
            size=1,
            storage_location="fake_storage_location",
            md5="fake_md5",
            is_archivable="fake_is_archivable",
            virus_scan_status="fake_virus_scan_status",
            accepted="fake_accepted",
            date_modified="fake_date_modified",
            thumbnail="fake_thumbnail",
            creator_uuid="fake_creator_uuid",
            creator_displayname="fake_creator_displayname",
            type="fake_type",
            properties='{"displayname": "fake_displayname"}',
            preview_uuid=uuid4(),
            preview_storage_location=None,
            preview_mimetype=None,
            description="desc",
            origin="Intern",
            origin_date="2015-05-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
        )

        self.session.execute().fetchone.side_effect = [
            destination_directory,
            namedtuple("directory", "id")(id=20),
            source_doc_1,
            namedtuple("directory_result", "id")(id=20),
        ]
        self.session.reset_mock()
        with pytest.raises(Conflict) as excinfo:
            self.cmd.move_to_directory(
                destination_directory_uuid=desstination_directory_uuid,
                source_documents=source_documents,
            )
        assert excinfo.value.args == (
            f"The destination directories case_id does not match the given documents case_id ({destination_directory.case_id} != {source_doc_1.case_display_number})",
            "domains/document/move_to_directory/document_case_id_mismatch",
        )


class Test_update_a_document_from_editor(TestBase):
    def setup_method(self):
        self.load_command_instance(document)
        self.mock_repo_factory = mock.MagicMock()
        store_uuid = uuid4()
        self.mock_filestore = mock.Mock(
            id=123,
            uuid=store_uuid,
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        )

    @mock.patch(
        "zsnl_domains.document.repositories.document.DocumentRepository._get_filestore_by_uuid",
        return_value=mock.Mock(
            id=123,
            uuid=uuid4(),
            filename="some_file.pdf",
            file_id=None,
            filestore_id=None,
            file_uuid=None,
            case_id=None,
            basename=None,
            accepted=None,
            created_by=None,
        ),
    )
    def test_document_from_editor(self, file_data):
        """ "
        Documents that are updated by an external editing service are updated
        using create_document with `auto_accept=True`
        """

        document_uuid = uuid4()
        store_uuid = uuid4()
        directory_uuid = uuid4()
        case_uuid = uuid4()

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_filestore = self.mock_filestore
        mock_file_metadata = mock.Mock(
            id=10,
            description="test metadata",
            trust_level="Openbaar",
            document_category="Aangifte",
            pronom_format=3,
            origin_date=datetime.now(timezone.utc) - timedelta(minutes=15),
            appearance=None,
            structure=None,
            origin="Inkomend",
        )
        mock_publishing_setting = mock.Mock(
            publish_pip="t", publish_website="t"
        )

        mock_directory = mock.Mock(id=123)
        mock_old_active_file = mock.Mock(
            id=122, uuid=document_uuid, version=6, root_file_id=None
        )
        mock_user_result = mock.Mock(subject_type="medewerker", id=123)

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
            [],  # Select a list of "affected" document labels
            [],  # Select a list of "affected" documents
        ]
        self.session.execute().fetchone.side_effect = [
            mock_case,  # Case to check assignee
            None,  # Get "root file" id
            mock_filestore,  # get_filestore_by_uuid
            mock_directory,  # get directory id
            mock_case,  # _get_case_id_assignee_uuid
            None,  # Get "root file" id, again
            mock_old_active_file,  # Old version to update
            mock_user_result,  # User to use for "created by"
            mock_file_metadata,
            mock_publishing_setting,
        ]
        self.session.reset_mock()

        self.cmd.create_document(
            document_uuid=str(document_uuid),
            filename="some_file.pdf",
            store_uuid=str(store_uuid),
            directory_uuid=str(directory_uuid),
            mimetype="application/pdf",
            size=1234,
            storage_location="earth",
            md5="d41d8cd98f00b204e9800998ecf8427e",
            magic_strings=[],
            case_uuid=str(case_uuid),
            skip_intake=False,
            auto_accept=True,
            editor_update=True,
        )


class Test_as_an_employee_I_want_to_accpet_document(TestBase):
    """As an employee I want to accept a document."""

    def setup_method(self):
        self.load_command_instance(document)

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.cmd.user_info = user_info

        self.document_uuid = uuid4()

    def test_accept_document(self):
        mock_is_admin = mock.MagicMock(name="check_user_admin")
        mock_is_admin.fetchall.return_value = [True]

        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_file_info = mock.Mock(
            root_file_id=100, metadata_id=None, accepted=False
        )
        prev_file_info = mock.Mock(
            id=100,
            version=1,
            uuid=uuid4(),
            root_file_id=None,
            metadata_id=None,
            name="test_doc",
        )

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [
            document,
            mock_case,
            mock_file_info,
            prev_file_info,
        ]
        self.session.reset_mock()

        self.cmd.accept_document(document_uuid=document_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 10

        update_query = call_list[6][0][0].compile(dialect=postgresql.dialect())

        assert str(update_query) == (
            "UPDATE file SET version=%(version)s, accepted=%(accepted)s, is_duplicate_name=%(is_duplicate_name)s, date_modified=%(date_modified)s, is_duplicate_of=%(is_duplicate_of)s, uuid=%(uuid)s WHERE file.id = %(id_1)s"
        )
        assert update_query.params["accepted"] is True
        update_query = call_list[7][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE file SET active_version=%(active_version)s WHERE file.id = %(id_1)s"
        )
        assert update_query.params["active_version"] is False
        update_query = call_list[9][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE zaak_meta SET unaccepted_files_count=%(unaccepted_files_count)s FROM zaak WHERE zaak_meta.zaak_id = zaak.id AND zaak.uuid = %(uuid_1)s"
        )

    def test_accept_document_from_intake(self):
        mock_is_admin = mock.MagicMock(name="check_user_admin")
        mock_is_admin.fetchall.return_value = [True]

        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_file_info = mock.Mock(
            root_file_id=None, metadata_id=2345, accepted=False
        )
        prev_file_info = mock.MagicMock()
        prev_file_info.configure_mock(
            id=100,
            version=1,
            uuid=uuid4(),
            root_file_id=None,
            metadata_id=2314,
            name="test_doc",
        )

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [
            document,
            mock_case,
            mock_file_info,
            prev_file_info,
        ]
        self.session.reset_mock()

        self.cmd.accept_document(document_uuid=document_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 9

        update_query = call_list[6][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE file SET name=%(name)s, accepted=%(accepted)s, date_modified=%(date_modified)s, active_version=%(active_version)s WHERE file.id = %(id_1)s"
        )
        assert update_query.params["accepted"] is True
        update_query = call_list[8][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE zaak_meta SET unaccepted_files_count=%(unaccepted_files_count)s FROM zaak WHERE zaak_meta.zaak_id = zaak.id AND zaak.uuid = %(uuid_1)s"
        )

    def test_accept_document_from_intake_with_second_version(self):
        mock_is_admin = mock.MagicMock(name="check_user_admin")
        mock_is_admin.fetchall.return_value = [True]

        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=102,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_file_info = mock.Mock(
            root_file_id=None, metadata_id=2345, accepted=False
        )
        prev_file_info = mock.MagicMock()
        prev_file_info.configure_mock(
            id=100,
            version=1,
            uuid=uuid4(),
            root_file_id=None,
            metadata_id=2314,
            name="test_doc (1)",
        )

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [
            document,
            mock_case,
            mock_file_info,
            prev_file_info,
        ]
        self.session.reset_mock()

        self.cmd.accept_document(document_uuid=document_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 9

        update_query = call_list[6][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE file SET name=%(name)s, accepted=%(accepted)s, date_modified=%(date_modified)s, active_version=%(active_version)s WHERE file.id = %(id_1)s"
        )

        update_query = call_list[8][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE zaak_meta SET unaccepted_files_count=%(unaccepted_files_count)s FROM zaak WHERE zaak_meta.zaak_id = zaak.id AND zaak.uuid = %(uuid_1)s"
        )

    def test_accept_document_from_other_user(self):
        mock_is_admin = mock.MagicMock(name="check_user_admin")
        mock_is_admin.fetchall.return_value = [True]

        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=102,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )

        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_file_info = mock.Mock(
            root_file_id=None, metadata_id=None, accepted=False
        )

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [
            document,
            mock_case,
            mock_file_info,
            None,
        ]
        self.session.reset_mock()

        self.cmd.accept_document(document_uuid=document_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 9

        update_query = call_list[6][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE file SET version=%(version)s, accepted=%(accepted)s, is_duplicate_name=%(is_duplicate_name)s, date_modified=%(date_modified)s, is_duplicate_of=%(is_duplicate_of)s, uuid=%(uuid)s WHERE file.id = %(id_1)s"
        )
        assert update_query.params["accepted"] is True
        update_query = call_list[8][0][0].compile(dialect=postgresql.dialect())
        assert str(update_query) == (
            "UPDATE zaak_meta SET unaccepted_files_count=%(unaccepted_files_count)s FROM zaak WHERE zaak_meta.zaak_id = zaak.id AND zaak.uuid = %(uuid_1)s"
        )

    def test_reaccept_document(self):
        mock_is_admin = mock.MagicMock(name="check_user_admin")
        mock_is_admin.fetchall.return_value = [True]

        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=True,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )
        mock_case = mock.Mock(id=123, status="open", behandelaar_gm_id=42)
        mock_file_info = mock.Mock(
            root_file_id=None, metadata_id=2345, accepted=True
        )

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [
            document,
            mock_case,
            mock_file_info,
        ]
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.accept_document(document_uuid=document_uuid)

        assert excinfo.value.args == (
            "can not re-accept accepted document",
            "domains/document/accept_document/docuemt_already_accepted",
        )

    def test_accept_document_without_case(self):
        mock_is_admin = mock.MagicMock(name="check_user_admin")
        mock_is_admin.fetchall.return_value = [True]

        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=None,
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )

        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [document]
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.accept_document(document_uuid=document_uuid)

        assert excinfo.value.args == (
            "can not accept document that's not in a case",
            "domains/document/accept_document/docuemt_without_case",
        )

    def test_accept_document_for_resolved_case(self):
        mock_is_admin = mock.MagicMock(name="check_user_admin")
        mock_is_admin.fetchall.return_value = [True]

        directory_uuid = uuid4()
        document = mock.MagicMock()
        document_uuid = str(uuid4())
        document.configure_mock(
            document_uuid=document_uuid,
            store_uuid=uuid4(),
            mimetype="application/msword",
            size=1056,
            storage_location=["Minio"],
            md5="123",
            is_archivable=False,
            virus_scan_status="ok",
            directory_uuid=directory_uuid,
            case_uuid=uuid4(),
            case_display_number=None,
            filename="test_doc",
            extension=".docx",
            accepted=False,
            creator_uuid=None,
            creator_displayname=None,
            date_modified="2022-01-03",
            id=101,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
            description="description",
            origin="Intern",
            origin_date="2022-01-01",
            confidentiality="Intern",
            document_category="Plan",
            intake_owner_uuid=None,
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            version=1,
        )
        mock_case = mock.Mock(id=123, status="resolved", behandelaar_gm_id=42)
        self.session.execute().fetchall.side_effect = [
            [True],  # user is admin
            [True],  # user is admin
        ]
        self.session.execute().fetchone.side_effect = [document, mock_case]
        self.session.reset_mock()

        with pytest.raises(Conflict) as excinfo:
            self.cmd.accept_document(document_uuid=document_uuid)

        assert excinfo.value.args == (
            "Not allowed to create document for 'resolved' case.",
            "document/case/case_resolved",
        )
