# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.exceptions import Conflict
from pytest import raises
from zsnl_domains.document.repositories.shared import (
    clean_filename_part,
    windows_reserved_names,
)


def test_windows_reserved_names():
    assert "con" in windows_reserved_names()
    assert "nul" in windows_reserved_names()
    assert "prn" in windows_reserved_names()
    assert "aux" in windows_reserved_names()

    for i in range(0, 10):
        assert f"lpt{i}" in windows_reserved_names()
        assert f"com{i}" in windows_reserved_names()


def test_clean_filename_part():
    assert clean_filename_part('<>&":|?*') == "________"
    assert clean_filename_part("prn") == "prn_"

    assert clean_filename_part(".prn") == ".prn_"

    assert clean_filename_part(r"C:\Users\Test") == "Test"
    assert clean_filename_part(r"/etc/motd") == "motd"

    with raises(Conflict):
        clean_filename_part("\x03")
