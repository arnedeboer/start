# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.events import EventService
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql as sqlalchemy_pg
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin import catalog
from zsnl_domains.admin.catalog import entities


class TestAs_A_User_I_Want_To_Get_Folder_Contents(TestBase):
    def setup_method(self):
        self.load_query_instance(catalog)
        self.mock_infra = mock.MagicMock()
        self.event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

    def test_get_folder_content(self):
        folder_uuid = uuid4()

        folder_content_rows = [mock.Mock()]

        folder_content_rows[0].configure_mock(
            entry_type=entities.EntryType.folder,
            uuid=folder_uuid,
            name="name",
            active=True,
        )
        self.session.execute().fetchall.return_value = folder_content_rows

        self.qry.get_folder_contents(
            folder_uuid=folder_uuid, page=1, page_size=10
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert (
            str(query)
            == "(SELECT %(param_1)s AS type, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, true AS active, bibliotheek_categorie.pid AS parent_folder_id, bibliotheek_categorie.id AS object_id \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.pid = %(pid_1)s ORDER BY name) UNION ALL (SELECT %(param_2)s AS type, zaaktype.uuid, zaaktype_node.titel AS name, zaaktype.active, zaaktype.bibliotheek_categorie_id AS parent_folder_id, zaaktype.id AS object_id \n"
            "FROM zaaktype, zaaktype_node \n"
            "WHERE zaaktype.deleted IS NULL AND zaaktype.zaaktype_node_id = zaaktype_node.id AND zaaktype.bibliotheek_categorie_id = %(bibliotheek_categorie_id_1)s ORDER BY name) UNION ALL (SELECT %(param_3)s AS type, bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.naam AS name, true AS active, bibliotheek_kenmerken.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_kenmerken.id AS object_id \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.deleted IS NULL AND bibliotheek_kenmerken.bibliotheek_categorie_id = %(bibliotheek_categorie_id_2)s ORDER BY name) UNION ALL (SELECT %(param_4)s AS type, bibliotheek_sjablonen.uuid, bibliotheek_sjablonen.naam AS name, true AS active, bibliotheek_sjablonen.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_sjablonen.id AS object_id \n"
            "FROM bibliotheek_sjablonen \n"
            "WHERE bibliotheek_sjablonen.deleted IS NULL AND bibliotheek_sjablonen.bibliotheek_categorie_id = %(bibliotheek_categorie_id_3)s ORDER BY name) UNION ALL (SELECT %(param_5)s AS type, bibliotheek_notificaties.uuid, bibliotheek_notificaties.label AS name, true AS active, bibliotheek_notificaties.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_notificaties.id AS object_id \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.deleted IS NULL AND bibliotheek_notificaties.bibliotheek_categorie_id = %(bibliotheek_categorie_id_4)s ORDER BY name) UNION ALL (SELECT %(param_6)s AS type, object_bibliotheek_entry.object_uuid AS uuid, object_bibliotheek_entry.name, true AS active, object_bibliotheek_entry.bibliotheek_categorie_id AS parent_folder_id, object_bibliotheek_entry.id AS object_id \n"
            "FROM object_bibliotheek_entry \n"
            "WHERE object_bibliotheek_entry.bibliotheek_categorie_id = %(bibliotheek_categorie_id_5)s ORDER BY object_bibliotheek_entry.name) UNION ALL (SELECT %(param_7)s AS type, custom_object_type.uuid, custom_object_type_version.name, true AS active, custom_object_type.catalog_folder_id AS parent_folder_id, custom_object_type_version.id AS object_id \n"
            "FROM custom_object_type, custom_object_type_version \n"
            "WHERE custom_object_type_version.date_deleted IS NULL AND custom_object_type.custom_object_type_version_id = custom_object_type_version.id AND custom_object_type.catalog_folder_id = %(catalog_folder_id_1)s ORDER BY custom_object_type_version.name) \n"
            " LIMIT %(param_8)s OFFSET %(param_9)s"
        )

    def test_get_folder_content_withour_paging(self):
        folder_uuid = uuid4()

        folder_content_rows = [mock.Mock()]

        folder_content_rows[0].configure_mock(
            entry_type=entities.EntryType.folder,
            uuid=folder_uuid,
            name="name",
            active=True,
        )
        self.session.execute().fetchall.return_value = folder_content_rows

        self.qry.get_folder_contents(
            folder_uuid=folder_uuid, page=1, page_size=10
        )
        self.qry.get_folder_contents(folder_uuid=folder_uuid)

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert (
            str(query)
            == "(SELECT %(param_1)s AS type, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, true AS active, bibliotheek_categorie.pid AS parent_folder_id, bibliotheek_categorie.id AS object_id \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.pid = %(pid_1)s ORDER BY name) UNION ALL (SELECT %(param_2)s AS type, zaaktype.uuid, zaaktype_node.titel AS name, zaaktype.active, zaaktype.bibliotheek_categorie_id AS parent_folder_id, zaaktype.id AS object_id \n"
            "FROM zaaktype, zaaktype_node \n"
            "WHERE zaaktype.deleted IS NULL AND zaaktype.zaaktype_node_id = zaaktype_node.id AND zaaktype.bibliotheek_categorie_id = %(bibliotheek_categorie_id_1)s ORDER BY name) UNION ALL (SELECT %(param_3)s AS type, bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.naam AS name, true AS active, bibliotheek_kenmerken.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_kenmerken.id AS object_id \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.deleted IS NULL AND bibliotheek_kenmerken.bibliotheek_categorie_id = %(bibliotheek_categorie_id_2)s ORDER BY name) UNION ALL (SELECT %(param_4)s AS type, bibliotheek_sjablonen.uuid, bibliotheek_sjablonen.naam AS name, true AS active, bibliotheek_sjablonen.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_sjablonen.id AS object_id \n"
            "FROM bibliotheek_sjablonen \n"
            "WHERE bibliotheek_sjablonen.deleted IS NULL AND bibliotheek_sjablonen.bibliotheek_categorie_id = %(bibliotheek_categorie_id_3)s ORDER BY name) UNION ALL (SELECT %(param_5)s AS type, bibliotheek_notificaties.uuid, bibliotheek_notificaties.label AS name, true AS active, bibliotheek_notificaties.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_notificaties.id AS object_id \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.deleted IS NULL AND bibliotheek_notificaties.bibliotheek_categorie_id = %(bibliotheek_categorie_id_4)s ORDER BY name) UNION ALL (SELECT %(param_6)s AS type, object_bibliotheek_entry.object_uuid AS uuid, object_bibliotheek_entry.name, true AS active, object_bibliotheek_entry.bibliotheek_categorie_id AS parent_folder_id, object_bibliotheek_entry.id AS object_id \n"
            "FROM object_bibliotheek_entry \n"
            "WHERE object_bibliotheek_entry.bibliotheek_categorie_id = %(bibliotheek_categorie_id_5)s ORDER BY object_bibliotheek_entry.name) UNION ALL (SELECT %(param_7)s AS type, custom_object_type.uuid, custom_object_type_version.name, true AS active, custom_object_type.catalog_folder_id AS parent_folder_id, custom_object_type_version.id AS object_id \n"
            "FROM custom_object_type, custom_object_type_version \n"
            "WHERE custom_object_type_version.date_deleted IS NULL AND custom_object_type.custom_object_type_version_id = custom_object_type_version.id AND custom_object_type.catalog_folder_id = %(catalog_folder_id_1)s ORDER BY custom_object_type_version.name)"
        )


class TestAs_A_User_I_Want_To_Get_Make_Catalog_Search(TestBase):
    def setup_method(self):
        self.load_query_instance(catalog)
        self.mock_infra = mock.MagicMock()
        self.event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

    def test_catalog_search_quey(self):
        folder_uuid = uuid4()

        folder_content_rows = [mock.Mock()]

        folder_content_rows[0].configure_mock(
            entry_type=entities.EntryType.folder,
            uuid=folder_uuid,
            name="name",
            active=True,
        )
        self.session.execute().fetchall.return_value = folder_content_rows

        self.qry.search_catalog(keyword="", type="")

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())
        assert (
            str(query)
            == "(SELECT %(param_1)s AS type, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, true AS active, bibliotheek_categorie.pid AS parent_folder_id, bibliotheek_categorie.id AS object_id \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.naam ILIKE %(naam_1)s ORDER BY bibliotheek_categorie.naam) UNION ALL (SELECT %(param_2)s AS type, zaaktype.uuid, zaaktype_node.titel AS name, zaaktype.active, zaaktype.bibliotheek_categorie_id AS parent_folder_id, zaaktype.id AS object_id \n"
            "FROM zaaktype, zaaktype_node \n"
            "WHERE zaaktype.deleted IS NULL AND zaaktype.zaaktype_node_id = zaaktype_node.id AND (zaaktype_node.titel ILIKE %(titel_1)s OR zaaktype_node.zaaktype_trefwoorden ILIKE %(zaaktype_trefwoorden_1)s) ORDER BY zaaktype_node.titel) UNION ALL (SELECT %(param_3)s AS type, bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.naam AS name, true AS active, bibliotheek_kenmerken.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_kenmerken.id AS object_id \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.deleted IS NULL AND bibliotheek_kenmerken.naam ILIKE %(naam_2)s AND bibliotheek_kenmerken.deleted IS NULL ORDER BY bibliotheek_kenmerken.naam) UNION ALL (SELECT %(param_4)s AS type, bibliotheek_sjablonen.uuid, bibliotheek_sjablonen.naam AS name, true AS active, bibliotheek_sjablonen.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_sjablonen.id AS object_id \n"
            "FROM bibliotheek_sjablonen \n"
            "WHERE bibliotheek_sjablonen.deleted IS NULL AND bibliotheek_sjablonen.naam ILIKE %(naam_3)s ORDER BY bibliotheek_sjablonen.naam) UNION ALL (SELECT %(param_5)s AS type, bibliotheek_notificaties.uuid, bibliotheek_notificaties.label AS name, true AS active, bibliotheek_notificaties.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_notificaties.id AS object_id \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.deleted IS NULL AND bibliotheek_notificaties.label ILIKE %(label_1)s ORDER BY bibliotheek_notificaties.label) UNION ALL (SELECT %(param_6)s AS type, object_bibliotheek_entry.object_uuid AS uuid, object_bibliotheek_entry.name, true AS active, object_bibliotheek_entry.bibliotheek_categorie_id AS parent_folder_id, object_bibliotheek_entry.id AS object_id \n"
            "FROM object_bibliotheek_entry \n"
            "WHERE object_bibliotheek_entry.name ILIKE %(name_1)s ORDER BY object_bibliotheek_entry.name) UNION ALL (SELECT %(param_7)s AS type, custom_object_type.uuid, custom_object_type_version.name, true AS active, custom_object_type.catalog_folder_id AS parent_folder_id, custom_object_type_version.id AS object_id \n"
            "FROM custom_object_type, custom_object_type_version \n"
            "WHERE custom_object_type_version.date_deleted IS NULL AND custom_object_type.custom_object_type_version_id = custom_object_type_version.id AND custom_object_type_version.name ILIKE %(name_2)s ORDER BY custom_object_type_version.name)"
        )

    def test_catalog_search_quey_with_paging(self):
        folder_uuid = uuid4()

        folder_content_rows = [mock.Mock()]

        folder_content_rows[0].configure_mock(
            entry_type=entities.EntryType.folder,
            uuid=folder_uuid,
            name="name",
            active=True,
        )
        self.session.execute().fetchall.return_value = folder_content_rows

        self.qry.search_catalog(keyword="", type="", page=1, page_size=10)

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())
        assert (
            str(query)
            == "(SELECT %(param_1)s AS type, bibliotheek_categorie.uuid, bibliotheek_categorie.naam AS name, true AS active, bibliotheek_categorie.pid AS parent_folder_id, bibliotheek_categorie.id AS object_id \n"
            "FROM bibliotheek_categorie \n"
            "WHERE bibliotheek_categorie.naam ILIKE %(naam_1)s ORDER BY bibliotheek_categorie.naam) UNION ALL (SELECT %(param_2)s AS type, zaaktype.uuid, zaaktype_node.titel AS name, zaaktype.active, zaaktype.bibliotheek_categorie_id AS parent_folder_id, zaaktype.id AS object_id \n"
            "FROM zaaktype, zaaktype_node \n"
            "WHERE zaaktype.deleted IS NULL AND zaaktype.zaaktype_node_id = zaaktype_node.id AND (zaaktype_node.titel ILIKE %(titel_1)s OR zaaktype_node.zaaktype_trefwoorden ILIKE %(zaaktype_trefwoorden_1)s) ORDER BY zaaktype_node.titel) UNION ALL (SELECT %(param_3)s AS type, bibliotheek_kenmerken.uuid, bibliotheek_kenmerken.naam AS name, true AS active, bibliotheek_kenmerken.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_kenmerken.id AS object_id \n"
            "FROM bibliotheek_kenmerken \n"
            "WHERE bibliotheek_kenmerken.deleted IS NULL AND bibliotheek_kenmerken.naam ILIKE %(naam_2)s AND bibliotheek_kenmerken.deleted IS NULL ORDER BY bibliotheek_kenmerken.naam) UNION ALL (SELECT %(param_4)s AS type, bibliotheek_sjablonen.uuid, bibliotheek_sjablonen.naam AS name, true AS active, bibliotheek_sjablonen.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_sjablonen.id AS object_id \n"
            "FROM bibliotheek_sjablonen \n"
            "WHERE bibliotheek_sjablonen.deleted IS NULL AND bibliotheek_sjablonen.naam ILIKE %(naam_3)s ORDER BY bibliotheek_sjablonen.naam) UNION ALL (SELECT %(param_5)s AS type, bibliotheek_notificaties.uuid, bibliotheek_notificaties.label AS name, true AS active, bibliotheek_notificaties.bibliotheek_categorie_id AS parent_folder_id, bibliotheek_notificaties.id AS object_id \n"
            "FROM bibliotheek_notificaties \n"
            "WHERE bibliotheek_notificaties.deleted IS NULL AND bibliotheek_notificaties.label ILIKE %(label_1)s ORDER BY bibliotheek_notificaties.label) UNION ALL (SELECT %(param_6)s AS type, object_bibliotheek_entry.object_uuid AS uuid, object_bibliotheek_entry.name, true AS active, object_bibliotheek_entry.bibliotheek_categorie_id AS parent_folder_id, object_bibliotheek_entry.id AS object_id \n"
            "FROM object_bibliotheek_entry \n"
            "WHERE object_bibliotheek_entry.name ILIKE %(name_1)s ORDER BY object_bibliotheek_entry.name) UNION ALL (SELECT %(param_7)s AS type, custom_object_type.uuid, custom_object_type_version.name, true AS active, custom_object_type.catalog_folder_id AS parent_folder_id, custom_object_type_version.id AS object_id \n"
            "FROM custom_object_type, custom_object_type_version \n"
            "WHERE custom_object_type_version.date_deleted IS NULL AND custom_object_type.custom_object_type_version_id = custom_object_type_version.id AND custom_object_type_version.name ILIKE %(name_2)s ORDER BY custom_object_type_version.name) \n"
            " LIMIT %(param_8)s OFFSET %(param_9)s"
        )
