# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.exceptions import Conflict
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.entities.case_type import CaseTypeEntity


class TestCaseTypeEntity:
    def setup_method(self):
        self.case_type_uuid = uuid4()
        self.version_uuid = uuid4()
        self.case_type = CaseTypeEntity(
            id=1,
            uuid=self.case_type_uuid,
            name="test_name",
            active=True,
            identification=123,
            current_version=10,
            last_modified="2019-12-21 23:50:43",
            current_version_uuid=self.version_uuid,
            initiator_source="internal",
        )
        self.case_type.event_service = mock.MagicMock()

    def test_set_status(self):
        with pytest.raises(Conflict) as excinfo:
            self.case_type.change_online_status(active=True, reason="reason")
        assert excinfo.value.args == (
            "Can't activate case_type, status is already: 'True''",
            "case_type/status_already_true",
        )

        self.case_type.change_online_status(active=False, reason="reason")

        assert self.case_type.active is False
        assert self.case_type.reason == "reason"
