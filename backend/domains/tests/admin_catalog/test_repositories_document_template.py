# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from datetime import datetime
from minty.cqrs.events import EventService
from minty.exceptions import Conflict, NotFound
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import select
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.entities.document_template import (
    DocumentTemplate,
)
from zsnl_domains.admin.catalog.repositories.document_template import (
    DocumentTemplateRepository,
)
from zsnl_domains.database import schema


class TestDocumentTemplateRepository:
    def setup_method(self):
        self.mock_infra = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

        self.document_template_repo = DocumentTemplateRepository(
            infrastructure_factory=self.mock_infra,
            context="context",
            event_service=event_service,
        )

    def test_create_new_document_templatee(self):
        uuid = uuid4()

        document_template = (
            self.document_template_repo.create_new_document_template(
                uuid=uuid, fields={}
            )
        )

        assert isinstance(document_template, DocumentTemplate)
        assert (
            document_template.event_service
            == self.document_template_repo.event_service
        )
        assert document_template.uuid == uuid

    @mock.patch.object(DocumentTemplateRepository, "_insert_document_template")
    def test_save(self, mock_insert):
        event_created = namedtuple(
            "event", "entity_type entity_id event_name changes"
        )(
            entity_type="DocumentTemplate",
            entity_id=uuid4(),
            event_name="DocumentTemplateCreated",
            changes=[
                {"key": "name", "old_value": None, "new_value": "document"},
                {
                    "key": "category_uuid",
                    "old_value": None,
                    "new_value": "d9c671e6-6885-4573-9c66-b7f2dbb78d7e",
                },
            ],
        )
        self.document_template_repo.event_service.event_list = [event_created]

        self.document_template_repo.save()

        mock_insert.assert_called_once_with(
            uuid=event_created.entity_id,
            formatted_changes={
                "name": "document",
                "category_uuid": "d9c671e6-6885-4573-9c66-b7f2dbb78d7e",
            },
        )

    @mock.patch.object(DocumentTemplateRepository, "_generate_database_values")
    def test__insert_document_template(self, mock_generate_values):
        formatted_changes = {
            "name": "document",
            "category_uuid": "d9c671e6-6885-4573-9c66-b7f2dbb78d7e",
        }

        self.document_template_repo._insert_document_template(
            uuid=uuid4(), formatted_changes=formatted_changes
        )

        self.mock_infra.get_infrastructure().execute.assert_called_once()
        mock_generate_values.assert_called_once_with(
            formatted_changes=formatted_changes
        )

        with pytest.raises(Conflict):
            self.mock_infra.get_infrastructure().execute.side_effect = (
                IntegrityError(None, None, None)
            )
            self.document_template_repo._insert_document_template(
                uuid=uuid4(), formatted_changes=formatted_changes
            )

    @mock.patch.object(
        DocumentTemplateRepository, "_check_integration_reference"
    )
    def test__generate_database_values(self, mock_check_integration_reference):
        formatted_changes = {
            "commit_message": "message on commit",
            "name": "document",
            "integration_uuid": "00c9b515-e51b-4d42-b8e5-7f37177c04l2",
            "integration_reference": "reference to external integration",
            "file_uuid": "00c9b515-e51b-4d42-b8e5-7f37177c04l2",
            "help": "extra info",
            "category_uuid": "ca1b0e3b-fe26-4b73-a8da-6f601b87be77",
        }

        values = self.document_template_repo._generate_database_values(
            formatted_changes=formatted_changes
        )

        assert values["naam"] == "document"
        assert values["help"] == "extra info"
        assert str(values["bibliotheek_categorie_id"]) == str(
            select(schema.BibliotheekCategorie.id)
            .where(
                schema.BibliotheekCategorie.uuid
                == "ca1b0e3b-fe26-4b73-a8da-6f601b87be77"
            )
            .scalar_subquery()
        )
        assert str(values["interface_id"]) == str(
            select(schema.Interface.id)
            .where(
                schema.Interface.uuid == "00c9b515-e51b-4d42-b8e5-7f37177c04l2"
            )
            .scalar_subquery()
        )
        assert (
            values["template_external_name"]
            == "reference to external integration"
        )

        mock_check_integration_reference.assert_called_once_with(
            integration_uuid="00c9b515-e51b-4d42-b8e5-7f37177c04l2",
            integration_reference="reference to external integration",
        )

        formatted_changes = {
            "commit_message": "message on commit",
            "name": "document",
            "integration_uuid": None,
            "integration_reference": None,
            "file_uuid": "00c9b515-e51b-4d42-b8e5-7f37177c04l2",
            "help": "extra info",
            "category_uuid": "ca1b0e3b-fe26-4b73-a8da-6f601b87be77",
        }

        values = self.document_template_repo._generate_database_values(
            formatted_changes=formatted_changes
        )

        assert str(values["filestore_id"]) == str(
            select(schema.Filestore.id)
            .where(
                schema.Filestore.uuid == "00c9b515-e51b-4d42-b8e5-7f37177c04l2"
            )
            .scalar_subquery()
        )

    def test__check_integration_refernce(self):
        with pytest.raises(Conflict) as excinfo:
            integration = namedtuple("integration", "module")(module="xential")
            integration_reference = "external reference"
            self.mock_infra.get_infrastructure().query().filter().one.return_value = (
                integration
            )

            self.document_template_repo._check_integration_reference(
                integration_uuid=uuid4,
                integration_reference=integration_reference,
            )
            assert excinfo.value.args == (
                f"Incorrect template_name '{integration_reference}'",
                "document_template/incorrect_template_name",
            )

        with pytest.raises(Conflict) as excinfo:
            integration = namedtuple("integration", "module")(
                module="stuf_dcr"
            )
            integration_reference = "  "
            self.mock_infra.get_infrastructure().query().filter().one.return_value = (
                integration
            )

            self.document_template_repo._check_integration_reference(
                integration_uuid=uuid4,
                integration_reference=integration_reference,
            )
            assert excinfo.value.args == (
                f"Incorrect template_name '{integration_reference}'",
                "document_template/incorrect_template_name",
            )

    def test_get_document_template_details_by_uuid(self):
        mock_ses = mock.MagicMock()
        self.document_template_repo.session = mock_ses

        document_template_uuid = str(uuid4())
        BibliotheekSjabloon = namedtuple(
            "BibliotheekSjabloon", "uuid id naam help template_external_name"
        )(
            uuid=document_template_uuid,
            id=1,
            naam="doc",
            help="extra info",
            template_external_name="external reference",
        )

        Interface = namedtuple("Interface", "uuid original_name")(
            uuid=str(uuid4()), original_name="ineterfcae.odt"
        )

        FileStore = namedtuple("FileStore", "uuid original_name")(
            uuid=str(uuid4()), original_name="txt.odt"
        )

        BibliotheekCategorie = namedtuple("BibliotheekCategorie", "uuid")(
            uuid=str(uuid4())
        )

        qry_result = namedtuple(
            "qry_result",
            "BibliotheekSjabloon BibliotheekCategorie Filestore Interface",
        )

        mock_ses.query().outerjoin().outerjoin().outerjoin().filter().one.return_value = qry_result(
            BibliotheekSjabloon, BibliotheekCategorie, FileStore, Interface
        )

        res = (
            self.document_template_repo.get_document_template_details_by_uuid(
                uuid=document_template_uuid
            )
        )
        assert isinstance(res, DocumentTemplate)

        assert res.entity_id == BibliotheekSjabloon.uuid
        assert res.id == BibliotheekSjabloon.id
        assert res.name == BibliotheekSjabloon.naam
        assert res.file_uuid == FileStore.uuid
        assert res.file_name == FileStore.original_name
        assert res.integration_uuid == Interface.uuid
        assert (
            res.integration_reference
            == BibliotheekSjabloon.template_external_name
        )
        assert res.category_uuid == BibliotheekCategorie.uuid
        assert res.help == BibliotheekSjabloon.help

    def test_get_document_template_details_by_uuid_not_found(self):
        uuid = str(uuid4())
        mock_ses = mock.MagicMock()
        self.document_template_repo.session = mock_ses
        mock_ses.query().outerjoin().outerjoin().outerjoin().filter().one.side_effect = (
            NoResultFound
        )

        with pytest.raises(NotFound) as excinfo:
            self.document_template_repo.get_document_template_details_by_uuid(
                uuid=uuid
            )

        assert excinfo.value.args == (
            f"Document template with uuid {uuid} not found",
            "document_template/not_found",
        )

    def test__edit_document_template(self):
        event_created = namedtuple(
            "event", "entity_type entity_id event_name changes"
        )(
            entity_type="DocumentTemplate",
            entity_id=uuid4(),
            event_name="DocumentTemplateEdited",
            changes=[
                {"key": "name", "old_value": "doc", "new_value": "document"},
                {
                    "key": "category_uuid",
                    "old_value": "e5c673e9-7985-4373-9c66-b7f2dbb78d7e",
                    "new_value": "d9c671e6-6885-4573-9c66-b7f2dbb78d7e",
                },
            ],
        )
        self.document_template_repo.event_service.event_list = [event_created]

        self.document_template_repo.save()

    def test_delete_document_template(self):
        uuid = str(uuid4())
        reason = "no more used"

        res = self.document_template_repo.delete_document_template(
            uuid=uuid, reason=reason
        )

        assert res.uuid == uuid
        assert res.commit_message == reason
        assert isinstance(res.deleted, datetime)

    def test__get_case_types_related_to_document_template(self):
        uuid = uuid4()
        mock_ses = mock.MagicMock()
        self.document_template_repo.session = mock_ses
        mock_ses.execute().fetchone.return_value = namedtuple(
            "RowProxy", "cnt"
        )(cnt=3)

        res = self.document_template_repo._get_case_types_related_to_document_template(
            uuid=uuid
        )

        assert res == 3

    @mock.patch.object(
        DocumentTemplateRepository,
        "_get_case_types_related_to_document_template",
    )
    def test__delete_email_template(self, mock_get_case_types):
        document_template_uuid = uuid4()
        mock_ses = mock.MagicMock()
        self.document_template_repo.session = mock_ses
        self.document_template_repo.event_service.log_event(
            entity_type="DocumentTemplate",
            entity_id=document_template_uuid,
            event_name="DocumentTemplateDeleted",
            changes=[
                {
                    "key": "deleted",
                    "old_value": None,
                    "new_value": "2019-07-08 11:09:02.885741",
                },
                {
                    "key": "commit_message",
                    "old_value": None,
                    "new_value": "Deleting an unsed email_template",
                },
            ],
            entity_data={},
        )
        mock_get_case_types.return_value = 0

        self.document_template_repo.save()
        mock_get_case_types.assert_called_once_with(
            uuid=document_template_uuid
        )

        with pytest.raises(Conflict) as excinfo:
            mock_get_case_types.return_value = 4

            self.document_template_repo.save()

        assert excinfo.value.args == (
            "Document template used in case types",
            "document_template/used_in_case_types",
        )
