# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from minty.entity import EntityCollection
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_GetListOfRoles(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_get_roles_unfiltered(self):
        mock_roles = [mock.Mock(), mock.Mock()]
        role_uuids = [uuid4(), uuid4()]
        department_uuid = uuid4()

        mock_roles[0].configure_mock(
            uuid=role_uuids[0],
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )
        mock_roles[1].configure_mock(
            uuid=role_uuids[1],
            name="Second role",
            description="Another role",
            parent_uuid=None,
            parent_name=None,
        )

        self.session.execute().fetchall.return_value = mock_roles

        roles = self.qry.get_roles()
        assert isinstance(roles, EntityCollection)

        roles = list(roles)
        assert len(roles) == 2

        assert roles[0].name == "Role name"
        assert roles[0].uuid == role_uuids[0]
        assert roles[0].entity_id == roles[0].uuid
        assert roles[0].entity_meta_summary == roles[0].name
        assert roles[0].parent.name == "Parent group"
        assert roles[0].parent.uuid == department_uuid

        assert roles[1].name == "Second role"
        assert roles[1].uuid == role_uuids[1]
        assert roles[1].entity_id == roles[1].uuid
        assert roles[1].entity_meta_summary == roles[1].name
        assert roles[1].parent is None

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query)
            == "SELECT roles.uuid, roles.name, roles.description, groups.uuid AS parent_uuid, groups.name AS parent_name \n"
            "FROM roles JOIN groups ON roles.parent_group_id = groups.id"
        )

    def test_get_roles_filtered(self):
        mock_roles = [mock.Mock(), mock.Mock()]
        role_uuids = [uuid4(), uuid4()]
        department_uuid = uuid4()

        mock_roles[0].configure_mock(
            uuid=role_uuids[0],
            name="Role name",
            description="A role",
            parent_uuid=department_uuid,
            parent_name="Parent group",
        )
        mock_roles[1].configure_mock(
            uuid=role_uuids[1],
            name="Second role",
            description="Another role",
            parent_uuid=None,
            parent_name=None,
        )

        self.session.execute().fetchall.return_value = mock_roles

        roles = self.qry.get_roles(filter_parent_uuid=str(department_uuid))
        assert isinstance(roles, EntityCollection)

        roles = list(roles)
        assert len(roles) == 2

        assert roles[0].name == "Role name"
        assert roles[0].uuid == role_uuids[0]
        assert roles[0].entity_id == roles[0].uuid
        assert roles[0].entity_meta_summary == roles[0].name
        assert roles[0].parent.name == "Parent group"
        assert roles[0].parent.uuid == department_uuid

        assert roles[1].name == "Second role"
        assert roles[1].uuid == role_uuids[1]
        assert roles[1].entity_id == roles[1].uuid
        assert roles[1].entity_meta_summary == roles[1].name
        assert roles[1].parent is None

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert (
            str(query)
            == "SELECT roles.uuid, roles.name, roles.description, groups.uuid AS parent_uuid, groups.name AS parent_name \n"
            "FROM roles JOIN groups ON roles.parent_group_id = groups.id \n"
            "WHERE groups.id IN (SELECT groups_1.id \n"
            "FROM groups AS groups_2 JOIN groups AS groups_1 ON groups_1.id = groups_2.path[:path_1] OR groups_1.id = groups_2.id \n"
            "WHERE groups_2.uuid = :uuid_1)"
        )
