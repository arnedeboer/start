# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_GetRelatedSubjectsForCustomObject(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_get_related_subjects_for_custom_subject(self):
        mock_subjects = [mock.Mock(), mock.Mock()]
        subject_uuids = [uuid4(), uuid4(), uuid4()]
        role_uuids = [uuid4(), uuid4()]

        mock_subjects[0].configure_mock(
            uuid=subject_uuids[0],
            name="beheeeder",
            type="employee",
            roles=[
                {"uuid": role_uuids[0], "name": "test_role_1"},
                {"uuid": role_uuids[1], "name": "test_role_2"},
            ],
        )
        mock_subjects[1].configure_mock(
            uuid=subject_uuids[1], name="person", type="person", roles=None
        )

        self.session.execute().fetchall.return_value = mock_subjects

        subjects = self.qry.get_related_subjects_for_custom_object(
            object_uuid=str(uuid4())
        )
        assert isinstance(subjects, list)
        assert len(subjects) == 2

        assert subjects[0].uuid == subject_uuids[0]
        assert subjects[0].entity_id == mock_subjects[0].uuid
        assert subjects[0].entity_meta_summary == mock_subjects[0].name

        assert len(subjects[0].roles) == 2
        assert subjects[0].roles[0].entity_id == role_uuids[0]
        assert (
            subjects[0].roles[0].entity_meta_summary
            == mock_subjects[0].roles[0]["name"]
        )

        assert subjects[0].roles[1].entity_id == role_uuids[1]
        assert (
            subjects[0].roles[1].entity_meta_summary
            == mock_subjects[0].roles[1]["name"]
        )

        assert subjects[1].uuid == subject_uuids[1]
        assert subjects[1].entity_id == mock_subjects[1].uuid
        assert subjects[1].entity_meta_summary == mock_subjects[1].name
        assert subjects[1].roles is None

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert str(query) == (
            "SELECT subject.uuid, :param_1 AS type, CAST(subject.properties AS JSON) ->> :param_2 AS name, array((SELECT json_build_object(:json_build_object_2, roles.uuid, :json_build_object_3, roles.name) AS json_build_object_1 \n"
            "FROM subject AS subject_1 JOIN LATERAL json_array_elements_text(array_to_json(subject_1.role_ids)) AS role_id ON subject_1.uuid = subject.uuid JOIN roles ON roles.id = CAST(role_id AS INTEGER))) AS roles \n"
            "FROM subject, custom_object, custom_object_relationship \n"
            "WHERE custom_object.uuid = :uuid_1 AND custom_object_relationship.custom_object_id = custom_object.id AND custom_object_relationship.relationship_type = :relationship_type_1 AND custom_object_relationship.related_employee_id = subject.id UNION ALL SELECT natuurlijk_persoon.uuid, :param_3 AS type, get_display_name_for_person(hstore(ARRAY[%(param_1)s, %(param_2)s, %(param_3)s], ARRAY[natuurlijk_persoon.voorletters, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.geslachtsnaam])) AS name, :param_4 AS roles \n"
            "FROM natuurlijk_persoon, custom_object, custom_object_relationship \n"
            "WHERE custom_object.uuid = :uuid_1 AND custom_object_relationship.custom_object_id = custom_object.id AND custom_object_relationship.relationship_type = :relationship_type_1 AND custom_object_relationship.related_person_id = natuurlijk_persoon.id UNION ALL SELECT bedrijf.uuid, :param_5 AS type, bedrijf.handelsnaam AS name, :param_6 AS roles \n"
            "FROM bedrijf, custom_object, custom_object_relationship \n"
            "WHERE custom_object.uuid = :uuid_1 AND custom_object_relationship.custom_object_id = custom_object.id AND custom_object_relationship.relationship_type = :relationship_type_1 AND custom_object_relationship.related_organization_id = bedrijf.id"
        )

    def test_get_related_subjects_for_subject(self):
        mock_subjects = [mock.Mock(), mock.Mock()]
        subject_uuids = [uuid4(), uuid4(), uuid4()]
        role_uuids = [uuid4(), uuid4()]

        mock_subjects[0].configure_mock(
            uuid=subject_uuids[0],
            name="beheeeder",
            type="employee",
            roles=[
                {"uuid": role_uuids[0], "name": "test_role_1"},
                {"uuid": role_uuids[1], "name": "test_role_2"},
            ],
        )
        mock_subjects[1].configure_mock(
            uuid=subject_uuids[1], name="person", type="person", roles=None
        )

        self.session.execute().fetchall.return_value = mock_subjects

        subjects = self.qry.get_related_subjects_for_subject(
            subject_uuid=str(uuid4())
        )
        assert isinstance(subjects, list)
        assert len(subjects) == 2

        assert subjects[0].uuid == subject_uuids[0]
        assert subjects[0].entity_id == mock_subjects[0].uuid
        assert subjects[0].entity_meta_summary == mock_subjects[0].name

        assert len(subjects[0].roles) == 2
        assert subjects[0].roles[0].entity_id == role_uuids[0]
        assert (
            subjects[0].roles[0].entity_meta_summary
            == mock_subjects[0].roles[0]["name"]
        )

        assert subjects[0].roles[1].entity_id == role_uuids[1]
        assert (
            subjects[0].roles[1].entity_meta_summary
            == mock_subjects[0].roles[1]["name"]
        )

        assert subjects[1].uuid == subject_uuids[1]
        assert subjects[1].entity_id == mock_subjects[1].uuid
        assert subjects[1].entity_meta_summary == mock_subjects[1].name
        assert subjects[1].roles is None

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert str(query) == (
            "SELECT subject.uuid, :param_1 AS type, CAST(subject.properties AS JSON) ->> :param_2 AS name, array((SELECT json_build_object(:json_build_object_2, roles.uuid, :json_build_object_3, roles.name) AS json_build_object_1 \n"
            "FROM subject AS subject_1 JOIN LATERAL json_array_elements_text(array_to_json(subject_1.role_ids)) AS role_id ON subject_1.uuid = subject.uuid JOIN roles ON roles.id = CAST(role_id AS INTEGER))) AS roles \n"
            "FROM subject \n"
            "WHERE subject.id IN (SELECT contact_relationship_view.relation \n"
            "FROM contact_relationship_view \n"
            "WHERE contact_relationship_view.contact_uuid = :contact_uuid_1 AND contact_relationship_view.relation_type = :relation_type_1 UNION SELECT contact_relationship_view.contact \n"
            "FROM contact_relationship_view \n"
            "WHERE contact_relationship_view.relation_uuid = :relation_uuid_1 AND contact_relationship_view.contact_type = :contact_type_1) UNION ALL SELECT bedrijf.uuid, :param_3 AS type, bedrijf.handelsnaam AS name, :param_4 AS roles \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.id IN (SELECT contact_relationship_view.relation \n"
            "FROM contact_relationship_view \n"
            "WHERE contact_relationship_view.contact_uuid = :contact_uuid_1 AND contact_relationship_view.relation_type = :relation_type_2 UNION SELECT contact_relationship_view.contact \n"
            "FROM contact_relationship_view \n"
            "WHERE contact_relationship_view.relation_uuid = :relation_uuid_1 AND contact_relationship_view.contact_type = :contact_type_2) UNION ALL SELECT natuurlijk_persoon.uuid, :param_5 AS type, get_display_name_for_person(hstore(ARRAY[%(param_1)s, %(param_2)s, %(param_3)s], ARRAY[natuurlijk_persoon.voorletters, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.geslachtsnaam])) AS name, :param_6 AS roles \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.id IN (SELECT contact_relationship_view.relation \n"
            "FROM contact_relationship_view \n"
            "WHERE contact_relationship_view.contact_uuid = :contact_uuid_1 AND contact_relationship_view.relation_type = :relation_type_3 UNION SELECT contact_relationship_view.contact \n"
            "FROM contact_relationship_view \n"
            "WHERE contact_relationship_view.relation_uuid = :relation_uuid_1 AND contact_relationship_view.contact_type = :contact_type_3)"
        )
