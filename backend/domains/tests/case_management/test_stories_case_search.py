# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from datetime import datetime
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.queries.case_search_result import (
    CaseSearchResultFilter,
)
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidCaseUrgency,
    ValidContactChannel,
)


class Test_Case_Search_Queries(TestBase):
    mock_db_rows = [mock.Mock(), mock.Mock()]
    mock_db_rows[0].configure_mock(
        id=2,
        uuid=uuid4(),
        status="new",
        vernietigingsdatum=None,
        archival_state=None,
        percentage_days_left=4,
        onderwerp="subject of case",
        progress=0.5,
        titel="case type title",
        unaccepted_files_count=1,
        unread_communication_count=2,
        unaccepted_attribute_update_count=3,
        requestor_name="Diederik-Jan",
        requestor_type="person",
        requestor_uuid=uuid4(),
        assignee_name="Sjaak",
        assignee_uuid=uuid4(),
        coordinator_uuid=uuid4(),
        coordinator_name="Sjaak",
        registratiedatum="2022-02-08",
        afhandeldatum="2022-02-08",
        target_completion_date="2025-02-08",
        custom_fields=[
            {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            },
            {
                "name": "custom_field_2",
                "magic_string": "object_relation_1",
                "type": "relationship",
                "value": [
                    '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                ],
                "is_multiple": False,
            },
            {
                "name": "custom_field_3",
                "magic_string": "object_relation_2",
                "type": "relationship",
                "value": [
                    '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                ],
                "is_multiple": False,
            },
        ],
        file_custom_fields=[
            {
                "name": "some name",
                "magic_string": "magicstring2",
                "type": "file",
                "is_multiple": False,
                "value": [
                    {
                        "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                        "size": 3,
                        "uuid": uuid4(),
                        "filename": "hello.txt",
                        "mimetype": "text/plain",
                        "is_archivable": True,
                        "original_name": "hello.txt",
                        "thumbnail_uuid": uuid4(),
                    },
                ],
            }
        ],
    )
    mock_db_rows[1].configure_mock(
        id=3,
        uuid=uuid4(),
        status="pending",
        vernietigingsdatum=datetime.now(),
        archival_state="vernietigen",
        percentage_days_left=152,
        onderwerp="subject of case",
        progress=1.0,
        titel="case type title2",
        unaccepted_files_count=4,
        unread_communication_count=5,
        unaccepted_attribute_update_count=6,
        requestor_name="Donald Duck",
        requestor_type="person",
        requestor_uuid=uuid4(),
        assignee_name="Dagobert",
        assignee_uuid=uuid4(),
        coordinator_uuid=uuid4(),
        coordinator_name="Sjaak",
        registratiedatum="2022-02-08",
        afhandeldatum="2022-02-08",
        target_completion_date="2025-02-08",
        custom_fields=[
            {
                "name": "custom_field_1",
                "magic_string": "test_subject",
                "type": "text",
                "value": ["Test Subject"],
                "is_multiple": False,
            },
            {
                "name": "custom_field_2",
                "magic_string": "object_relation_1",
                "type": "relationship",
                "value": [
                    '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                ],
                "is_multiple": False,
            },
            {
                "name": "custom_field_3",
                "magic_string": "object_relation_2",
                "type": "relationship",
                "value": [
                    '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                ],
                "is_multiple": False,
            },
        ],
        file_custom_fields=[
            {
                "name": "some name",
                "magic_string": "magicstring2",
                "type": "file",
                "is_multiple": False,
                "value": [
                    {
                        "md5": "4216455ceebbc3038bd0550c85b6a3bf",
                        "size": 3,
                        "uuid": uuid4(),
                        "filename": "hello.txt",
                        "mimetype": "text/plain",
                        "is_archivable": True,
                        "original_name": "hello.txt",
                        "thumbnail_uuid": uuid4(),
                    },
                ],
            }
        ],
    )

    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_info = self.user_info

    def test_search_case(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        case_search_result = self.qry.search_case(page=1, page_size=10)

        assert len(case_search_result.entities) == 2
        # verify 1st search result
        search_result1 = case_search_result.entities[0]
        assert search_result1.number == 2
        assert search_result1.status == "new"
        assert search_result1.destruction_date is None
        assert search_result1.archival_state is None
        assert search_result1.percentage_days_left == 4
        assert search_result1.subject == "subject of case"
        assert search_result1.case_type_title == "case type title"
        assert search_result1.requestor.type == "person"
        assert search_result1.assignee.name == "Sjaak"
        assert search_result1.progress == 50
        assert search_result1.unread_message_count == 2
        assert search_result1.unaccepted_files_count == 1
        assert search_result1.unaccepted_attribute_update_count == 3

        # verify 2nd search result
        search_result2 = case_search_result.entities[1]
        assert search_result2.number == 3
        assert search_result2.status == "pending"
        assert search_result2.destruction_date is not None
        assert search_result2.archival_state == "vernietigen"
        assert search_result2.percentage_days_left == 152
        assert search_result2.subject == "subject of case"
        assert search_result2.case_type_title == "case type title2"
        assert search_result2.requestor.type == "person"
        assert search_result2.assignee.name == "Dagobert"
        assert search_result2.progress == 100
        assert search_result2.unread_message_count == 5
        assert search_result2.unaccepted_files_count == 4
        assert search_result2.unaccepted_attribute_update_count == 6

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_status(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_status = {ValidCaseStatus.new, ValidCaseStatus.open}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.status IN (%(status_2_1)s, %(status_2_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_casetype_uuid(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.case_type_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaaktype.uuid IN (%(uuid_2_1)s, %(uuid_2_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_assignee(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.assignee_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_betrokkenen_1.subject_id IN (%(subject_id_1_1)s, %(subject_id_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_coordinator(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.coordinator_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_betrokkenen_2.subject_id IN (%(subject_id_1_1)s, %(subject_id_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_requestor(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.requestor_uuids = {uuid4(), uuid4()}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_betrokkenen_3.subject_id IN (%(subject_id_1_1)s, %(subject_id_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_registration_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_registration_date = ["gt 2022-02-08T11:13:16Z"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.registratiedatum > %(registratiedatum_1)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_completion_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_completion_date = ["gt 2022-01-04T14:02:11Z"]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.afhandeldatum > %(afhandeldatum_1)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_payment_status(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_payment_status = {
            ValidCasePaymentStatus.success,
            ValidCasePaymentStatus.pending,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.payment_status IN (%(payment_status_1_1)s, %(payment_status_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_channel_of_contact(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_channel_of_contact = {
            ValidContactChannel.behandelaar,
            ValidContactChannel.post,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.contactkanaal IN (%(contactkanaal_1_1)s, %(contactkanaal_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_confidentiality(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_confidentiality = {
            ValidCaseConfidentiality.public,
            ValidCaseConfidentiality.internal,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.confidentiality IN (%(confidentiality_1_1)s, %(confidentiality_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_archival_state(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_archival_state = {
            ValidCaseArchivalState.vernietigen,
            ValidCaseArchivalState.overdragen,
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.archival_state IN (%(archival_state_1_1)s, %(archival_state_1_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_retention_period_source_date(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_retention_period_source_date = {
            ValidCaseRetentionPeriodSourceDate.vervallen
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1
        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaaktype_resultaten_1.ingang IN (%(ingang_1_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_result(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_result = {ValidCaseResult.aangegaan}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.resultaat IN (%(resultaat_1_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_case_location(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_case_location = {
            "values": [
                "nummeraanduiding-0437200000001964",
                "openbareruimte-0437300000000021",
            ]
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak_bag_1.bag_nummeraanduiding_id IN (%(bag_nummeraanduiding_id_1_1)s, %(bag_nummeraanduiding_id_1_2)s) OR zaak_bag_1.bag_openbareruimte_id IN (%(bag_openbareruimte_id_1_1)s, %(bag_openbareruimte_id_1_2)s)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unread_messages_and_sort_asc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_num_unread_messages = ["gt 2"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, sort="unread_message_count"
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_meta.unread_communication_count > %(unread_communication_count_1)s ORDER BY zaak_meta.unread_communication_count ASC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unaccepted_files_sort_desc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_num_unaccepted_files = ["gt 1"]
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="-unaccepted_files_count",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_meta.unaccepted_files_count > %(unaccepted_files_count_1)s ORDER BY zaak_meta.unaccepted_files_count DESC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_num_unaccepted_updates_sort_asc(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_num_unaccepted_updates = ["gt 1"]
        self.qry.search_case(
            page=1,
            page_size=10,
            filters=filters,
            sort="unaccepted_attribute_update_count",
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_meta.unaccepted_attribute_update_count > %(unaccepted_attribute_update_count_1)s ORDER BY zaak_meta.unaccepted_attribute_update_count ASC, zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_keyword(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_keyword = {"operator": "or", "values": ["test"]}
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.id = zaak_meta.zaak_id AND zaak_meta.text_vector @@ to_tsquery(%(text_vector_1)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_keyword_and_operator(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_keyword = {
            "operator": "and",
            "values": ["test", "case"],
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.id = zaak_meta.zaak_id AND zaak_meta.text_vector @@ to_tsquery(%(text_vector_1)s) AND zaak_1.id = zaak_meta.zaak_id AND zaak_meta.text_vector @@ to_tsquery(%(text_vector_2)s) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_period_of_preservation_active(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_period_of_preservation_active = True
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaaktype_resultaten_1.trigger_archival IS true ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_subject(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_subject = "test"
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.onderwerp ILIKE %(onderwerp_1)s ESCAPE '~' ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_total_count(self):
        self.session.execute().fetchall.return_value = 2
        self.session.reset_mock()

        self.qry.search_case_total_results()

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        query = call_list[0][0][0]
        query = query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s)))"
        )

    def test_search_case_includes_contacts(self):
        employee_row = mock.MagicMock()
        employee_row.configure_mock(
            uuid=uuid4(),
            active=True,
            surname="",
            title=None,
            first_name="beheerder",
            name="testgebruiker",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="beheerder",
        )
        person_uuid = uuid4()
        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1918, 4, 26),
            date_of_death=datetime(2004, 1, 25),
            first_names="Fanny",
            family_name="Koen",
            initials=None,
            insertions="Blankers",
            surname="Koen",
            noble_title=None,
            name="Fanny Blankers-Koen",
            gender="V",
            inside_municipality=True,
            properties={"anonymous": "0"},
            address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
                "bag_id": "9876543210098765",
            },
            correspondence_address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Fanny Blankers-Koen",
            external_identifier=None,
        )

        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            [person_row],
            [employee_row],
        )
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_subject = "test"
        includes = ["assignee", "coordinator", "requestor"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, includes=includes
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 3

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.onderwerp ILIKE %(onderwerp_1)s ESCAPE '~' ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid IN (%(uuid_1_1)s, %(uuid_1_2)s)"
        )

        select_statement = call_list[2][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT subject.uuid, subject.username AS name, coalesce(CAST(subject.properties AS JSON) -> %(param_1)s, NULL) AS first_name, coalesce(CAST(subject.properties AS JSON) -> %(param_2)s, NULL) AS surname, coalesce(CAST(subject.properties AS JSON) -> %(param_3)s, NULL) AS title, json_build_object(%(json_build_object_1)s, coalesce(CAST(subject.properties AS JSON) -> %(param_4)s, NULL), %(json_build_object_2)s, coalesce(CAST(subject.properties AS JSON) -> %(param_5)s, NULL)) AS contact_information, (SELECT coalesce(user_entity.active, %(coalesce_2)s) AS coalesce_1 \n"
            "FROM user_entity \n"
            "WHERE user_entity.subject_id = subject.id \n"
            " LIMIT %(param_6)s) AS active, (SELECT json_build_object(%(json_build_object_4)s, groups.uuid, %(json_build_object_5)s, groups.name) AS json_build_object_3 \n"
            "FROM groups \n"
            "WHERE groups.id = subject.group_ids[%(group_ids_1)s]) AS department, array((SELECT json_build_object(%(json_build_object_7)s, roles.uuid, %(json_build_object_8)s, roles.name, %(json_build_object_9)s, json_build_object(%(json_build_object_10)s, groups_1.uuid, %(json_build_object_11)s, groups_1.name)) AS json_build_object_6 \n"
            "FROM subject AS subject_1 JOIN LATERAL json_array_elements_text(array_to_json(subject_1.role_ids)) AS role_id ON subject_1.uuid = subject.uuid JOIN roles ON roles.id = CAST(role_id AS INTEGER) JOIN groups AS groups_1 ON groups_1.id = roles.parent_group_id)) AS roles, custom_object.uuid AS related_custom_object_uuid, CAST(subject.properties AS JSON) ->> %(param_7)s AS summary \n"
            "FROM subject LEFT OUTER JOIN custom_object ON subject.related_custom_object_id = custom_object.id \n"
            "WHERE subject.uuid IN (%(uuid_1_1)s, %(uuid_1_2)s, %(uuid_1_3)s, %(uuid_1_4)s) AND subject.subject_type = %(subject_type_1)s"
        )

    def test_search_case_includes_organization(self):
        employee_row = mock.MagicMock()
        employee_row.configure_mock(
            uuid=uuid4(),
            active=True,
            surname="",
            title=None,
            first_name="beheerder",
            name="testgebruiker",
            gender="Female",
            inside_municipality=True,
            properties={"anonymous": "0"},
            department={"uuid": uuid4(), "name": "Back office"},
            roles=[
                {
                    "name": "admin",
                    "uuid": uuid4(),
                    "department": {"name": "Back office", "uuid": uuid4()},
                }
            ],
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            summary="beheerder",
        )

        person_uuid = uuid4()
        person_row = mock.MagicMock()
        person_row.configure_mock(
            uuid=person_uuid,
            authenticated=True,
            authenticatedby="source",
            date_of_birth=datetime(1918, 4, 26),
            date_of_death=datetime(2004, 1, 25),
            first_names="Fanny",
            family_name="Koen",
            initials=None,
            insertions="Blankers",
            surname="Koen",
            noble_title=None,
            name="Fanny Blankers-Koen",
            gender="V",
            inside_municipality=True,
            properties={"anonymous": "0"},
            address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
                "bag_id": "9876543210098765",
            },
            correspondence_address={
                "street": "Lage Vuursche ",
                "zipcode": "1234RT",
                "street_number": 3,
                "city": "Baarn",
                "country_code": 6030,
            },
            contact_information={"email": "test@test.com"},
            related_custom_object_uuid=uuid4(),
            preferred_contact_channel="pip",
            summary="Fanny Blankers-Koen",
            external_identifier=None,
        )
        org_uuid = uuid4()
        org_row = mock.MagicMock()
        org_row.configure_mock(
            uuid=org_uuid,
            authenticated=True,
            authenticatedby="Intern",
            name="Company Name 1",
            coc_number="12345678",
            coc_location_number="123456789012",
            date_founded=None,
            date_registered=None,
            date_ceased=None,
            rsin=None,
            oin=None,
            main_activity={"code": 123, "description": "Something"},
            secondary_activities=[{"code": 31337, "description": "Test"}],
            business_entity_code=1,  # Eenmanszaak
            location_address={
                "street": "Test street",
                "street_number": 123,
                "street_number_letter": "Z",
                "street_number_suffix": None,
                "zipcode": "1234AB",
                "city": "Test City",
                "country": 6030,
                "bag_id": None,
            },
            correspondence_address=None,
            contact_information={},
            related_custom_object_uuid=None,
            preferred_contact_channel="pip",
            contact_person_info={},
            summary="Company Name 1",
        )
        self.mock_db_rows[0].requestor_type = "company"

        self.session.execute().fetchall.side_effect = (
            self.mock_db_rows,
            [person_row],
            [employee_row],
            [org_row],
        )
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_subject = "test"
        includes = ["assignee", "requestor"]
        self.qry.search_case(
            page=1, page_size=10, filters=filters, includes=includes
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 4

        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak_1.onderwerp ILIKE %(onderwerp_1)s ESCAPE '~' ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

        select_statement = call_list[1][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT natuurlijk_persoon.uuid, natuurlijk_persoon.authenticated, natuurlijk_persoon.authenticatedby, natuurlijk_persoon.voornamen AS first_names, natuurlijk_persoon.voorletters AS initials, natuurlijk_persoon.voorvoegsel AS insertions, natuurlijk_persoon.geslachtsnaam AS family_name, natuurlijk_persoon.adellijke_titel AS noble_title, natuurlijk_persoon.surname AS surname, natuurlijk_persoon.geboortedatum AS date_of_birth, natuurlijk_persoon.datum_overlijden AS date_of_death, natuurlijk_persoon.geslachtsaanduiding AS gender, natuurlijk_persoon.in_gemeente AS inside_municipality, natuurlijk_persoon.landcode, natuurlijk_persoon.preferred_contact_channel, natuurlijk_persoon.active AS active, (SELECT json_build_object(%(json_build_object_2)s, contact_data.email, %(json_build_object_3)s, contact_data.telefoonnummer, %(json_build_object_4)s, contact_data.mobiel, %(json_build_object_5)s, contact_data.note) AS json_build_object_1 \n"
            "FROM contact_data \n"
            "WHERE contact_data.gegevens_magazijn_id = natuurlijk_persoon.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s) AS contact_information, (SELECT CASE WHEN (adres_1.straatnaam != %(straatnaam_1)s AND (adres_1.landcode = %(landcode_1)s OR adres_1.landcode = %(landcode_2)s)) THEN json_build_object(%(json_build_object_6)s, adres_1.straatnaam, %(json_build_object_7)s, adres_1.postcode, %(json_build_object_8)s, adres_1.huisnummer, %(json_build_object_9)s, adres_1.huisletter, %(json_build_object_10)s, adres_1.huisnummertoevoeging, %(json_build_object_11)s, adres_1.woonplaats, %(json_build_object_12)s, adres_1.landcode, %(json_build_object_13)s, adres_1.bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, adres_1.geo_lat_long) ELSE json_build_object(%(json_build_object_17)s, adres_1.landcode, %(json_build_object_18)s, adres_1.adres_buitenland1, %(json_build_object_19)s, adres_1.adres_buitenland2, %(json_build_object_20)s, adres_1.adres_buitenland3, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, adres_1.geo_lat_long) END AS anon_1 \n"
            "FROM adres AS adres_1 \n"
            "WHERE adres_1.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_1.functie_adres = %(functie_adres_1)s) AS address, (SELECT CASE WHEN (adres_2.straatnaam != %(straatnaam_2)s AND (adres_2.landcode = %(landcode_3)s OR adres_2.landcode = %(landcode_4)s)) THEN json_build_object(%(json_build_object_25)s, adres_2.straatnaam, %(json_build_object_26)s, adres_2.postcode, %(json_build_object_27)s, adres_2.huisnummer, %(json_build_object_28)s, adres_2.huisletter, %(json_build_object_29)s, adres_2.huisnummertoevoeging, %(json_build_object_30)s, adres_2.woonplaats, %(json_build_object_31)s, adres_2.landcode, %(json_build_object_32)s, adres_2.bag_id, %(json_build_object_33)s, %(json_build_object_34)s, %(json_build_object_35)s, adres_2.geo_lat_long) ELSE json_build_object(%(json_build_object_36)s, adres_2.landcode, %(json_build_object_37)s, adres_2.adres_buitenland1, %(json_build_object_38)s, adres_2.adres_buitenland2, %(json_build_object_39)s, adres_2.adres_buitenland3, %(json_build_object_40)s, NULL, %(json_build_object_41)s, %(json_build_object_42)s, %(json_build_object_43)s, adres_2.geo_lat_long) END AS anon_2 \n"
            "FROM adres AS adres_2 \n"
            "WHERE adres_2.natuurlijk_persoon_id = natuurlijk_persoon.id AND adres_2.functie_adres = %(functie_adres_2)s) AS correspondence_address, natuurlijk_persoon.indicatie_geheim AS indicatie_geheim, custom_object.uuid AS related_custom_object_uuid, subject.properties, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname) AS summary, object_subscription.external_id AS external_identifier \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN custom_object ON natuurlijk_persoon.related_custom_object_id = custom_object.id LEFT OUTER JOIN subject ON subject.uuid = natuurlijk_persoon.uuid LEFT OUTER JOIN object_subscription ON natuurlijk_persoon.id = CAST(object_subscription.local_id AS INTEGER) AND object_subscription.local_table = %(local_table_1)s AND object_subscription.date_deleted IS NULL \n"
            "WHERE natuurlijk_persoon.deleted_on IS NULL AND natuurlijk_persoon.uuid IN (%(uuid_1_1)s)"
        )

        select_statement = call_list[3][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(compiled_select) == (
            "SELECT bedrijf.uuid, bedrijf.authenticated, bedrijf.authenticatedby, bedrijf.handelsnaam AS name, bedrijf.date_founded, bedrijf.date_registration AS date_registered, bedrijf.date_ceased, bedrijf.rsin, bedrijf.oin, bedrijf.dossiernummer AS coc_number, bedrijf.vestigingsnummer AS coc_location_number, bedrijf.rechtsvorm AS business_entity_code, bedrijf.main_activity, bedrijf.secondairy_activities AS secondary_activities, bedrijf.preferred_contact_channel, json_build_object(%(json_build_object_1)s, contact_data.email, %(json_build_object_2)s, contact_data.telefoonnummer, %(json_build_object_3)s, contact_data.mobiel, %(json_build_object_4)s, contact_data.note, %(json_build_object_5)s, bedrijf.preferred_contact_channel) AS contact_information, CASE WHEN (coalesce(bedrijf.vestiging_adres_buitenland1, bedrijf.vestiging_adres_buitenland2, bedrijf.vestiging_adres_buitenland3, bedrijf.vestiging_straatnaam, %(coalesce_1)s) = %(coalesce_2)s) THEN NULL WHEN (bedrijf.vestiging_straatnaam != %(vestiging_straatnaam_1)s AND (bedrijf.vestiging_landcode = %(vestiging_landcode_1)s OR bedrijf.vestiging_landcode = %(vestiging_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_6)s, bedrijf.vestiging_straatnaam, %(json_build_object_7)s, bedrijf.vestiging_postcode, %(json_build_object_8)s, bedrijf.vestiging_huisnummer, %(json_build_object_9)s, bedrijf.vestiging_huisletter, %(json_build_object_10)s, bedrijf.vestiging_huisnummertoevoeging, %(json_build_object_11)s, bedrijf.vestiging_woonplaats, %(json_build_object_12)s, bedrijf.vestiging_landcode, %(json_build_object_13)s, bedrijf.vestiging_bag_id, %(json_build_object_14)s, %(json_build_object_15)s, %(json_build_object_16)s, bedrijf.vestiging_latlong) AS JSON) ELSE CAST(json_build_object(%(json_build_object_17)s, bedrijf.vestiging_adres_buitenland1, %(json_build_object_18)s, bedrijf.vestiging_adres_buitenland2, %(json_build_object_19)s, bedrijf.vestiging_adres_buitenland3, %(json_build_object_20)s, bedrijf.vestiging_landcode, %(json_build_object_21)s, NULL, %(json_build_object_22)s, %(json_build_object_23)s, %(json_build_object_24)s, bedrijf.vestiging_latlong) AS JSON) END AS location_address, CASE WHEN (coalesce(bedrijf.correspondentie_adres_buitenland1, bedrijf.correspondentie_adres_buitenland2, bedrijf.correspondentie_adres_buitenland3, bedrijf.correspondentie_straatnaam, %(coalesce_3)s) = %(coalesce_4)s) THEN NULL WHEN (bedrijf.correspondentie_straatnaam != %(correspondentie_straatnaam_1)s AND (bedrijf.correspondentie_landcode = %(correspondentie_landcode_1)s OR bedrijf.correspondentie_landcode = %(correspondentie_landcode_2)s)) THEN CAST(json_build_object(%(json_build_object_25)s, bedrijf.correspondentie_straatnaam, %(json_build_object_26)s, bedrijf.correspondentie_postcode, %(json_build_object_27)s, bedrijf.correspondentie_huisnummer, %(json_build_object_28)s, bedrijf.correspondentie_huisletter, %(json_build_object_29)s, bedrijf.correspondentie_huisnummertoevoeging, %(json_build_object_30)s, bedrijf.correspondentie_woonplaats, %(json_build_object_31)s, bedrijf.correspondentie_landcode, %(json_build_object_32)s, %(json_build_object_33)s) AS JSON) ELSE CAST(json_build_object(%(json_build_object_34)s, bedrijf.correspondentie_adres_buitenland1, %(json_build_object_35)s, bedrijf.correspondentie_adres_buitenland2, %(json_build_object_36)s, bedrijf.correspondentie_adres_buitenland3, %(json_build_object_37)s, bedrijf.correspondentie_landcode, %(json_build_object_38)s, %(json_build_object_39)s) AS JSON) END AS correspondence_address, custom_object.uuid AS related_custom_object_uuid, json_build_object(%(json_build_object_40)s, bedrijf.contact_voorletters, %(json_build_object_41)s, bedrijf.contact_voorvoegsel, %(json_build_object_42)s, bedrijf.contact_geslachtsnaam, %(json_build_object_43)s, bedrijf.contact_naam) AS contact_person_info, bedrijf.handelsnaam AS summary \n"
            "FROM bedrijf LEFT OUTER JOIN custom_object ON bedrijf.related_custom_object_id = custom_object.id LEFT OUTER JOIN contact_data ON contact_data.gegevens_magazijn_id = bedrijf.id AND contact_data.betrokkene_type = %(betrokkene_type_1)s \n"
            "WHERE bedrijf.deleted_on IS NULL AND bedrijf.uuid IN (%(uuid_1_1)s)"
        )

    def test_search_case_filter_urgency(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_urgency = [ValidCaseUrgency.late]
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND coalesce(zaak_1.afhandeldatum, now()) >= zaak_1.streefafhandeldatum AND zaak_1.status != %(status_2)s ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )

    def test_search_case_filter_department_role(self):
        self.session.execute().fetchall.return_value = self.mock_db_rows
        self.session.reset_mock()

        filters = CaseSearchResultFilter()
        filters.filter_department_role = {
            "operator": "or",
            "values": [
                {"department_uuid": str(uuid4()), "role_uuid": str(uuid4())},
                {"department_uuid": str(uuid4()), "role_uuid": str(uuid4())},
            ],
        }
        self.qry.search_case(page=1, page_size=10, filters=filters)

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 1

        # query
        select_statement = call_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled_select) == (
            "SELECT zaak_1.id, zaak_1.uuid, zaak_1.status, zaak_1.vernietigingsdatum, zaak_1.archival_state, zaak_1.registratiedatum, zaak_1.afhandeldatum, zaak_1.streefafhandeldatum AS target_completion_date, get_date_progress_from_case(zaak_1.afhandeldatum, zaak_1.streefafhandeldatum, zaak_1.registratiedatum, zaak_1.status) AS percentage_days_left, zaak_1.onderwerp, CAST(zaak_1.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.titel, zaak_meta.unaccepted_files_count, zaak_meta.unread_communication_count, zaak_meta.unaccepted_attribute_update_count, zaak_1.requestor_v1_json -> %(requestor_v1_json_1)s AS requestor_name, ((zaak_1.requestor_v1_json -> %(requestor_v1_json_2)s) -> %(param_1)s) -> %(param_2)s AS requestor_type, zaak_1.requestor_v1_json -> %(requestor_v1_json_3)s AS requestor_uuid, zaak_1.assignee_v1_json -> %(assignee_v1_json_1)s AS assignee_name, zaak_1.assignee_v1_json -> %(assignee_v1_json_2)s AS assignee_uuid, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_1)s AS coordinator_name, zaak_1.coordinator_v1_json -> %(coordinator_v1_json_2)s AS coordinator_uuid, array((SELECT json_build_object(%(json_build_object_2)s, bibliotheek_kenmerken.naam, %(json_build_object_3)s, bibliotheek_kenmerken.magic_string, %(json_build_object_4)s, zaak_kenmerk.value, %(json_build_object_5)s, bibliotheek_kenmerken.value_type, %(json_build_object_6)s, bibliotheek_kenmerken.type_multiple) AS json_build_object_1 \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id \n"
            "WHERE zaak_kenmerk.zaak_id = zaak_1.id)) AS custom_fields, array((SELECT json_build_object(%(json_build_object_8)s, bibliotheek_kenmerken.naam, %(json_build_object_9)s, bibliotheek_kenmerken.magic_string, %(json_build_object_10)s, bibliotheek_kenmerken.value_type, %(json_build_object_11)s, array((SELECT json_build_object(%(json_build_object_13)s, filestore.md5, %(json_build_object_14)s, filestore.size, %(json_build_object_15)s, filestore.uuid, %(json_build_object_16)s, file.name || file.extension, %(json_build_object_17)s, filestore.mimetype, %(json_build_object_18)s, filestore.is_archivable, %(json_build_object_19)s, filestore.original_name, %(json_build_object_20)s, filestore.thumbnail_uuid) AS json_build_object_12 \n"
            "FROM file_case_document JOIN file ON file_case_document.file_id = file.id AND file_case_document.case_id = zaak_1.id JOIN filestore ON file.filestore_id = filestore.id \n"
            "WHERE file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object_7 \n"
            "FROM zaaktype_kenmerken JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id AND bibliotheek_kenmerken.value_type = %(value_type_1)s JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id \n"
            "WHERE zaaktype_node.id = zaak_1.zaaktype_node_id)) AS file_custom_fields \n"
            "FROM zaak AS zaak_1 JOIN zaaktype_node ON zaak_1.zaaktype_node_id = zaaktype_node.id JOIN zaak_meta ON zaak_1.id = zaak_meta.zaak_id JOIN zaaktype ON zaak_1.zaaktype_id = zaaktype.id LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_1 ON zaak_betrokkenen_1.id = zaak_1.behandelaar LEFT OUTER JOIN zaak_betrokkenen AS zaak_betrokkenen_2 ON zaak_betrokkenen_2.id = zaak_1.coordinator JOIN zaak_betrokkenen AS zaak_betrokkenen_3 ON zaak_betrokkenen_3.id = zaak_1.aanvrager LEFT OUTER JOIN zaaktype_resultaten AS zaaktype_resultaten_1 ON zaaktype_resultaten_1.id = zaak_1.resultaat_id LEFT OUTER JOIN zaak_bag AS zaak_bag_1 ON zaak_bag_1.id = zaak_1.locatie_zaak \n"
            "WHERE zaak_1.deleted IS NULL AND zaak_1.status != %(status_1)s AND (zaak_1.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak_1.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak_1.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak_1.id AND case_acl.casetype_id = zaak_1.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak_1.route_ou = (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_2)s) AND zaak_1.route_role = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_3)s) OR zaak_1.route_ou = (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_4)s) AND zaak_1.route_role = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_5)s)) ORDER BY zaak_1.id DESC \n"
            " LIMIT %(param_3)s OFFSET %(param_4)s"
        )
