# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import json
import zoneinfo
from minty.cqrs.test import TestBase
from minty.entity import RedirectResponse
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import ExportFile

MAGIC_NOT_BEFORE_DATE = datetime.datetime(
    1955,
    10,
    12,
    22,
    4,
    11,
    tzinfo=zoneinfo.ZoneInfo("US/Pacific"),
)


class TestExportFileQueries(TestBase):
    def setup_method(self):
        self.mock_amqp_infra = mock.Mock("amqp_infra")
        self.mock_s3 = mock.Mock(name="s3_infra")
        self.load_query_instance(
            case_management,
            inframocks={"amqp": self.mock_amqp_infra, "s3": self.mock_s3},
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.export_file.datetime"
    )
    def test_get_export_file_list(self, mock_dt):
        mock_dt.now.return_value = MAGIC_NOT_BEFORE_DATE

        mock_export_files = [mock.Mock(name="export_file_1")]
        mock_export_files[0].configure_mock(
            uuid=uuid4(),
            subject_uuid=self.qry.user_info.user_uuid,
            expires=datetime.datetime.now(tz=datetime.timezone.utc),
            downloaded=4,
            token="abcdefg",
            original_name="export_file.csv",
            file_uuid=uuid4(),
            mime_type="text/csv",
            storage_location=["somewhere"],
        )

        self.session.execute().fetchall.return_value = mock_export_files
        self.session.reset_mock()

        res = self.qry.get_export_file_list(page=3, page_size=10)

        assert isinstance(res, list)
        assert isinstance(res[0], ExportFile)

        assert res[0].entity_id == mock_export_files[0].uuid
        assert res[0].expires_at == mock_export_files[0].expires
        assert res[0].token == mock_export_files[0].token
        assert res[0].downloads == mock_export_files[0].downloaded
        assert res[0].name == mock_export_files[0].original_name
        assert res[0].mime_type == mock_export_files[0].mime_type
        assert (
            res[0].storage_location == mock_export_files[0].storage_location[0]
        )

        assert len(self.session.execute.call_args_list) == 1

        select_statement = self.session.execute.call_args_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT export_queue.uuid AS uuid, export_queue.subject_uuid AS subject_uuid, export_queue.expires AS expires, export_queue.downloaded AS downloaded, export_queue.token AS token, filestore.original_name AS original_name, filestore.uuid AS file_uuid, filestore.mimetype AS mime_type, filestore.storage_location AS storage_location \n"
            "FROM export_queue JOIN filestore ON export_queue.filestore_id = filestore.id \n"
            "WHERE export_queue.subject_uuid = %(subject_uuid_1)s AND export_queue.expires > %(expires_1)s ORDER BY export_queue.expires ASC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )
        assert compiled_select.params == {
            "subject_uuid_1": self.qry.user_info.user_uuid,
            "expires_1": MAGIC_NOT_BEFORE_DATE,
            "param_1": 10,
            "param_2": 20,
        }

    @mock.patch("zsnl_domains.amqpstorm")
    @mock.patch(
        "zsnl_domains.case_management.repositories.export_file.datetime"
    )
    @mock.patch("minty.cqrs.events.datetime")
    @mock.patch("minty.cqrs.events.uuid4")
    def test_download_export_file(
        self, mock_minty_uuid4, mock_minty_dt, mock_dt, mock_amqpstorm
    ):
        event_id = uuid4()
        correlation_id = uuid4()
        mock_minty_uuid4.side_effect = [event_id, correlation_id]

        mock_minty_dt.datetime.now.return_value = MAGIC_NOT_BEFORE_DATE
        mock_dt.now.return_value = MAGIC_NOT_BEFORE_DATE

        self.qry.repository_factory.infrastructure_factory.get_config = (
            lambda context: {
                "instance_uuid": "test",
                "amqp": {"publish_settings": {"exchange": "dummy_exchange"}},
            }
        )

        mock_export_file = mock.Mock(name="export_file_1")
        mock_export_file.configure_mock(
            uuid=uuid4(),
            subject_uuid=self.qry.user_info.user_uuid,
            expires=datetime.datetime.now(tz=datetime.timezone.utc),
            downloaded=4,
            token="abcdefg",
            original_name="export_file.csv",
            file_uuid=uuid4(),
            mime_type="text/csv",
            storage_location=["somewhere"],
        )

        self.mock_s3.get_download_url.return_value = (
            "http://download.url.example.com"
        )
        # self.mock_amqp

        self.session.execute().fetchone.return_value = mock_export_file
        self.session.reset_mock()

        res = self.qry.download_export_file(token="abcdefg")

        assert isinstance(res, RedirectResponse)
        assert res.location == "http://download.url.example.com"

        assert len(self.session.execute.call_args_list) == 1

        select_statement = self.session.execute.call_args_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT export_queue.uuid AS uuid, export_queue.subject_uuid AS subject_uuid, export_queue.expires AS expires, export_queue.downloaded AS downloaded, export_queue.token AS token, filestore.original_name AS original_name, filestore.uuid AS file_uuid, filestore.mimetype AS mime_type, filestore.storage_location AS storage_location \n"
            "FROM export_queue JOIN filestore ON export_queue.filestore_id = filestore.id \n"
            "WHERE export_queue.subject_uuid = %(subject_uuid_1)s AND export_queue.expires > %(expires_1)s AND export_queue.token = %(token_1)s"
        )
        assert compiled_select.params == {
            "expires_1": MAGIC_NOT_BEFORE_DATE,
            "subject_uuid_1": self.qry.user_info.user_uuid,
            "token_1": "abcdefg",
        }

        mock_amqpstorm.Message.create.assert_called_once_with(
            channel=self.mock_amqp_infra,
            body=mock.ANY,
            properties={"content_type": "application/json"},
        )

        assert mock_amqpstorm.Message.create.call_args.kwargs[
            "body"
        ] == json.dumps(
            {
                "changes": [],
                "context": None,  # TestBase sets context in all repositories to None
                "correlation_id": str(correlation_id),
                "created_date": MAGIC_NOT_BEFORE_DATE.isoformat(),
                "domain": "zsnl_domains_case_management",
                "entity_data": {},
                "entity_id": str(mock_export_file.uuid),
                "entity_type": "ExportFile",
                "event_name": "ExportFileDownloaded",
                "id": str(event_id),
                "user_info": {
                    "permissions": {"admin": True},
                    "type": "UserInfo",
                    "user_uuid": str(self.qry.user_info.user_uuid),
                },
                "user_uuid": str(self.qry.user_info.user_uuid),
            },
            sort_keys=True,
        )

        mock_amqpstorm.Message.create().publish.assert_called_once_with(
            routing_key="zsnl.v2.zsnl_domains_case_management.ExportFile.ExportFileDownloaded",
            exchange="dummy_exchange",
        )


class TestExportFileCommands(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)

    @mock.patch(
        "zsnl_domains.case_management.repositories.export_file.datetime"
    )
    def test_increase_download_count(self, mock_dt):
        mock_dt.now.return_value = MAGIC_NOT_BEFORE_DATE

        export_file_uuid = uuid4()

        mock_export_file = mock.Mock(name="export_file_1")
        mock_export_file.configure_mock(
            uuid=export_file_uuid,
            subject_uuid=self.cmd.user_info.user_uuid,
            expires=datetime.datetime.now(tz=datetime.timezone.utc),
            downloaded=4,
            token="abcdefg",
            original_name="export_file.csv",
            file_uuid=uuid4(),
            mime_type="text/csv",
            storage_location=["somewhere"],
        )

        self.session.execute().fetchone.return_value = mock_export_file
        self.session.reset_mock()

        self.cmd.export_file_increase_download_counter(
            export_file_uuid=export_file_uuid
        )

        assert len(self.session.execute.call_args_list) == 2

        select_statement = self.session.execute.call_args_list[0][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_select) == (
            "SELECT export_queue.uuid AS uuid, export_queue.subject_uuid AS subject_uuid, export_queue.expires AS expires, export_queue.downloaded AS downloaded, export_queue.token AS token, filestore.original_name AS original_name, filestore.uuid AS file_uuid, filestore.mimetype AS mime_type, filestore.storage_location AS storage_location \n"
            "FROM export_queue JOIN filestore ON export_queue.filestore_id = filestore.id \n"
            "WHERE export_queue.subject_uuid = %(subject_uuid_1)s AND export_queue.expires > %(expires_1)s AND export_queue.uuid = %(uuid_1)s"
        )
        assert compiled_select.params == {
            "expires_1": MAGIC_NOT_BEFORE_DATE,
            "subject_uuid_1": self.cmd.user_info.user_uuid,
            "uuid_1": export_file_uuid,
        }

        update_statement = self.session.execute.call_args_list[1][0][0]
        compiled_update = update_statement.compile(
            dialect=postgresql.dialect()
        )

        assert str(compiled_update) == (
            "UPDATE export_queue SET downloaded=(export_queue.downloaded + %(downloaded_1)s) WHERE export_queue.uuid = %(uuid_1)s"
        )
        assert compiled_update.params == {
            "downloaded_1": 1,
            "uuid_1": str(export_file_uuid),
        }
