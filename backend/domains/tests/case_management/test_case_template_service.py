# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from zsnl_domains.case_management.services.case_template import (
    CaseTemplateService,
)


class Test_CaseTemplateService(TestBase):
    def test_case_loation(self, test_case):
        case = test_case()
        case_location = "Ellermanstraat 23, 1114AK Amsterdam-Duivendrecht"
        expected_results = {
            "[[zaaklocatie_ligplaats]]": case_location,
            "[[zaaklocatie_nummeraanduiding]]": case_location,
            "[[zaaklocatie_openbareruimte]]": case_location,
            "[[zaaklocatie_pand]]": case_location,
            "[[zaaklocatie_standplaats]]": case_location,
            "[[zaaklocatie_verblijfsobject]]": case_location,
            "[[zaaklocatie_woonplaats]]": case_location,
            "[[case.case_location.ligplaats]]": case_location,
            "[[case.case_location.nummeraanduiding]]": case_location,
            "[[case.case_location.openbareruimte]]": case_location,
            "[[case.case_location.pand]]": case_location,
            "[[case.case_location.standplaats]]": case_location,
            "[[case.case_location.verblijfsobject]]": case_location,
            "[[case.case_location.woonplaats]]": case_location,
        }
        for key, value in expected_results.items():
            self._assert_template_value(
                case=case,
                magic_string=key,
                expected_value=value,
            )

    def test_system_attributes(self, test_case):
        case = test_case()
        expected_results = {
            "[[case.number_relations]]": "",
            "[[relates_to]]": "",
            "[[case.department]]": "Backoffice",
            "[[afdeling]]": "Backoffice",
        }
        for key, value in expected_results.items():
            self._assert_template_value(
                case=case,
                magic_string=key,
                expected_value=value,
            )

    def _assert_template_value(self, case, magic_string, expected_value):
        caseTemplateService = CaseTemplateService(user_name_retriever=None)
        case.case_type_version.case_summary = magic_string
        result = caseTemplateService.render_value(
            case=case, template="case_summary"
        )
        assert result == expected_value
