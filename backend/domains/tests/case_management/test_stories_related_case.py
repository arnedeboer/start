# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from minty.entity import EntityCollection
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_GetRelatedCasesForCustomObject(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_get_related_cases_for_custom_object(self):
        mock_cases = [mock.Mock(), mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        assignee_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]

        mock_cases[0].configure_mock(
            uuid=case_uuids[0],
            id=5,
            status="open",
            progress=0.5,
            summary="summary 1",
            result="result 1",
            case_type_version_uuid=case_type_uuids[0],
            case_type_version_title="Casetype 1",
            assignee_uuid=assignee_uuids[0],
            assignee_name="Employee 1",
        )
        mock_cases[1].configure_mock(
            uuid=case_uuids[1],
            id=2,
            status="new",
            progress=0,
            summary="summary 2",
            result="result 2",
            case_type_version_uuid=case_type_uuids[1],
            case_type_version_title="Casetype 2",
            assignee_uuid=assignee_uuids[1],
            assignee_name="Employee 2",
        )

        self.session.execute().fetchall.return_value = mock_cases

        result = self.qry.get_related_cases_for_custom_object(
            object_uuid=str(uuid4())
        )
        assert isinstance(result, EntityCollection)

        cases = list(result)
        assert len(cases) == 2

        assert cases[0].uuid == case_uuids[0]
        assert cases[0].entity_id == mock_cases[0].uuid
        assert cases[0].number == mock_cases[0].id
        assert cases[0].status == mock_cases[0].status
        assert cases[0].progress == mock_cases[0].progress
        assert cases[0].assignee.uuid == assignee_uuids[0]
        assert cases[0].assignee.entity_id == assignee_uuids[0]
        assert cases[0].assignee.entity_meta_summary == "Employee 1"
        assert cases[0].case_type.uuid == case_type_uuids[0]
        assert cases[0].case_type.entity_id == case_type_uuids[0]
        assert cases[0].case_type.entity_meta_summary == "Casetype 1"

        assert cases[1].uuid == case_uuids[1]
        assert cases[1].entity_id == mock_cases[1].uuid
        assert cases[1].number == mock_cases[1].id
        assert cases[1].status == mock_cases[1].status
        assert cases[1].progress == mock_cases[1].progress
        assert cases[1].assignee.uuid == assignee_uuids[1]
        assert cases[1].assignee.entity_id == assignee_uuids[1]
        assert cases[1].assignee.entity_meta_summary == "Employee 2"
        assert cases[1].case_type.uuid == case_type_uuids[1]
        assert cases[1].case_type.entity_id == case_type_uuids[1]
        assert cases[1].case_type.entity_meta_summary == "Casetype 2"

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert str(query) == (
            "SELECT zaak.uuid, zaak.id, zaak.onderwerp AS summary, zaak.resultaat AS result, zaak.status, CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 \n"
            "FROM zaaktype_status \n"
            "WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress, zaaktype_node.uuid AS case_type_version_uuid, zaaktype_node.titel AS case_type_version_title, subject.uuid AS assignee_uuid, CAST(subject.properties AS JSON)[:param_1] AS assignee_name \n"
            "FROM custom_object, custom_object_relationship, zaak JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id LEFT OUTER JOIN subject ON subject.id = zaak.behandelaar_gm_id \n"
            "WHERE custom_object.uuid = :uuid_1 AND custom_object_relationship.custom_object_id = custom_object.id AND custom_object_relationship.relationship_type = :relationship_type_1 AND custom_object_relationship.related_uuid = zaak.uuid"
        )
