# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import json
from collections import defaultdict, namedtuple
from datetime import date, datetime, timezone
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from sqlalchemy import sql
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_Manage_ObjectRelation(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": True, "zaak_create_skip_required": True},
        )
        self.cmd.user_info = self.user_info

    def __mock_case_type(self, case_type_uuid, phases):
        mock_case_type = defaultdict(mock.MagicMock())
        mock_case_type["uuid"] = case_type_uuid
        mock_case_type["id"] = "1"
        mock_case_type["case_type_uuid"] = uuid4()
        mock_case_type["properties"] = {}
        mock_case_type["phases"] = phases

        mock_case_type["name"] = "mock_case_type"
        mock_case_type["description"] = ""
        mock_case_type["identification"] = ""
        mock_case_type["tags"] = ""
        mock_case_type["case_summary"] = "summary"
        mock_case_type["case_public_summary"] = "public_summary"

        mock_case_type["requestor"] = {
            "type_of_requestors": ["medewerker", "pip"],
            "use_for_correspondence": True,
        }
        mock_case_type["catalog_folder"] = None
        mock_case_type["preset_assignee"] = None
        mock_case_type["preset_requestor"] = None

        mock_case_type["active"] = True
        mock_case_type["allow_reuse_casedata"] = True
        mock_case_type["enable_webform"] = True
        mock_case_type["enable_online_payment"] = True
        mock_case_type["require_email_on_webform"] = False
        mock_case_type["require_phonenumber_on_webform"] = True
        mock_case_type["require_mobilenumber_on_webform"] = False
        mock_case_type["enable_subject_relations_on_form"] = False
        mock_case_type["open_case_on_create"] = True
        mock_case_type["enable_allocation_on_form"] = True
        mock_case_type["disable_pip_for_requestor"] = True
        mock_case_type["is_public"] = True

        mock_case_type["legal_basis"] = "legal_basis"

        mock_case_type["terms"] = {
            "lead_time_legal": {"type": "kalenderdagen", "value": "123"},
            "lead_time_service": {"type": "kalenderdagen", "value": "234"},
        }
        mock_case_type["process_description"] = ""
        mock_case_type["initiator_source"] = ""
        mock_case_type["initiator_type"] = ""
        mock_case_type["show_contact_info"] = True
        mock_case_type["webform_amount"] = "30"
        mock_case_type["results"] = [
            {
                "id": 156,
                "uuid": "a04d7629-ca9a-4246-8e9c-cc38b327e904",
                "name": "Bewaren",
                "result": "behaald",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
            {
                "id": 157,
                "uuid": "604138d9-e14f-4944-812c-12a098a68092",
                "name": "Nog een 2de resultaat",
                "result": "afgebroken",
                "trigger_archival": True,
                "preservation_term_label": "4 weken",
                "preservation_term_unit": "week",
                "preservation_term_unit_amount": 4,
            },
        ]

        return mock_case_type

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    def test_add_object_attribute_relation(
        self,
        mock_case,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        custom_object_uuid_1 = str(uuid4())
        custom_object_uuid_2 = str(uuid4())
        custom_fields = {
            "object_relation_1": [
                json.dumps(
                    {
                        "type": "relationship",
                        "value": custom_object_uuid_1,
                        "specifics": {
                            "metadata": {"summary": "custom_object_uuid_1"},
                            "relationship_type": "custom_object",
                        },
                    }
                )
            ],
            "object_relation_2": [
                json.dumps(
                    {
                        "type": "relationship",
                        "value": [
                            {
                                "type": "relationship",
                                "value": custom_object_uuid_2,
                                "specifics": {
                                    "metadata": {
                                        "summary": "Custom Object Type Title"
                                    },
                                    "relationship_type": "custom_object",
                                },
                            }
                        ],
                        "specifics": None,
                    }
                )
            ],
            "test_subject": ["test"],
        }

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": uuid4(),
                            "field_magic_string": "test_subject",
                            "field_type": "text",
                            "name": "custom_object_1",
                            "public_name": "custom_object_1",
                        },
                        {
                            "uuid": type_uuid_1,
                            "field_magic_string": "object_relation_1",
                            "field_type": "relationship",
                            "relationship_type": "custom_object",
                            "name": "custom_object_2",
                            "public_name": "custom_object_2",
                        },
                        {
                            "uuid": type_uuid_2,
                            "field_magic_string": "object_relation_2",
                            "field_type": "relationship",
                            "relationship_type": "custom_object",
                            "name": "custom_object_3",
                            "public_name": "custom_object_3",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        existing_object_relations = [
            mock.Mock(
                attribute_id="32",
                magic_string="object_relation_1",
                source_custom_field_type_id=type_uuid_1,
                uuid=str(uuid4()),
                custom_object_uuid=custom_object_uuid_1,
            )
        ]

        mock_case_values = mock.MagicMock()
        mock_case_values = {
            "id": 16,
            "uuid": case_uuid,
            "department": None,
            "role": "mock.Mock()",
            "vernietigingsdatum": None,
            "archival_state": None,
            "status": "new",
            "afhandeldatum": None,
            "stalled_until": None,
            "milestone": 1,
            "suspension_reason": None,
            "completion": None,
            "last_modified": date(2019, 3, 30),
            "coordinator": None,
            "assignee": None,
            "requestor": None,
            "subject": "Parking permit",
            "subject_extern": "Parking permit amsterdam",
            "case_type_uuid": "case_type_uuid",
            "case_type_version_uuid": uuid4(),
            "aanvraag_trigger": "extern",
            "contactkanaal": "post",
            "resume_reason": None,
            "summary": "Perking permit Amsterdam",
            "payment_amount": "100",
            "payment_status": "success",
            "confidentiality": "public",
            "attributes": "{}",
            "prefix": "1",
            "route_ou": "1",
            "route_role": "role_id",
            "behandelaar": 100,
            "behandelaar_gm_id": 1,
            "coordinator_gm_id": 1,
            "created": date(2020, 1, 1),
            "registratiedatum": date(2020, 1, 1),
            "streefafhandeldatum": None,
            "urgency": "No",
            "preset_client": "No",
            "onderwerp": "test",
            "onderwerp_extern": "test summary",
            "resultaat": "test",
            "resultaat_id": 1,
            "aanvrager": 200,
            "aanvrager_gm_id": 1,
            "selectielijst": None,
            "case_status_details": {
                "id": 348,
                "zaaktype_node_id": 147,
                "status": 1,
                "status_type": None,
                "naam": "Geregistreerd",
                "created": date(2020, 1, 1),
                "last_modified": date(2019, 3, 30),
                "ou_id": 1,
                "role_id": 12,
                "checklist": None,
                "fase": "Registreren",
                "role_set": None,
            },
            "case_meta": {
                "opschorten": None,
                "afhandeling": None,
                "stalled_since": None,
            },
            "case_actions": {},
            "custom_fields": [
                {
                    "name": "custom_field_1",
                    "magic_string": "test_subject",
                    "type": "text",
                    "value": ["Test Subject"],
                },
                {
                    "name": "custom_field_2",
                    "magic_string": "object_relation_1",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "479bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-1"}, "relationship_type": "custom_object"}}'
                    ],
                },
                {
                    "name": "custom_field_3",
                    "magic_string": "object_relation_2",
                    "type": "relationship",
                    "value": [
                        '{"type": "relationship", "value": "579bdb2e-8778-4dc3-9b42-688374536764", "specifics": {"metadata": {"summary": "custom-object-2"}, "relationship_type": "custom_object"}}'
                    ],
                },
            ],
            "file_custom_fields": None,
        }

        mock_case.return_value = mock_case_values
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            mock.Mock(
                version_independent_uuid=uuid4(),
                is_active_version=1,
                object_type_uuid=uuid4(),
                object_type_name="Foo",
                uuid=custom_object_uuid_1,
                title="Title",
                subtitle="Subtitle",
                external_reference="X",
                status="active",
                version=100,
                date_created=datetime.now(tz=timezone.utc),
                last_modified=datetime.now(tz=timezone.utc),
                date_deleted=None,
                authorizations=None,
                cases=[],
                custom_objects=[],
                documents=[],
                subjects=[],
                custom_fields={},
                archive_status="archived",
                archive_ground="lala",
                archive_retention=1000,
                ot_is_active_version=True,
                ot_version_independent_uuid=uuid4(),
                ot_uuid=uuid4(),
                ot_name="Testtype",
                ot_title="TypeTitle",
                ot_subtitle="TypeSubtitle",
                ot_external_reference="TypeExternalRef",
                ot_status="active",
                ot_version=42,
                ot_custom_field_definition={"custom_fields": []},
                ot_relationship_definition={"relationships": None},
                ot_audit_log={
                    "description": "f",
                    "updated_components": ["attributes"],
                },
                ot_date_created=datetime.now(tz=timezone.utc),
                ot_last_modified=datetime.now(tz=timezone.utc),
                ot_date_deleted=None,
            ),
            mock.Mock(
                version_independent_uuid=uuid4(),
                is_active_version=1,
                object_type_uuid=uuid4(),
                object_type_name="Foo2",
                uuid=custom_object_uuid_2,
                title="Title2",
                subtitle="Subtitle2",
                external_reference="X",
                status="active",
                version=100,
                date_created=datetime.now(tz=timezone.utc),
                last_modified=datetime.now(tz=timezone.utc),
                date_deleted=None,
                authorizations=None,
                cases=[],
                custom_objects=[],
                documents=[],
                subjects=[],
                custom_fields={},
                archive_status="archived",
                archive_ground="lala",
                archive_retention=1000,
                ot_is_active_version=True,
                ot_version_independent_uuid=uuid4(),
                ot_uuid=uuid4(),
                ot_name="Testtype",
                ot_title="TypeTitle",
                ot_subtitle="TypeSubtitle",
                ot_external_reference="TypeExternalRef",
                ot_status="active",
                ot_version=42,
                ot_custom_field_definition={"custom_fields": []},
                ot_relationship_definition={"relationships": None},
                ot_audit_log={
                    "description": "f",
                    "updated_components": ["attributes"],
                },
                ot_date_created=datetime.now(tz=timezone.utc),
                ot_last_modified=datetime.now(tz=timezone.utc),
                ot_date_deleted=None,
            ),
            mock.Mock(id=101),  # _get_table_values_custom_object_version
            mock.Mock(id=102),  # custom_object_version_id
            mock.Mock(id=103),  # custom_object_id
            mock.Mock(id=104),  # custom_field_id
        ]
        self.session.execute().fetchall.side_effect = [
            [],  # find_subject_relations_for_case
            existing_object_relations,  # find_object_relations_for_case
            [],  # _get_subject_union_query
        ]

        mock_case_details = mock.Mock(name="case_details")
        mock_case_details.configure_mock(id=16, uuid=case_uuid)
        self.session.query().filter().all.side_effect = [
            [],  # Current custom object relationships
            [],  # document
            [],  # case
            [mock_case_details],  # case
        ]
        self.session.query().join().filter().all.side_effect = [
            [],  # custom_object
            [],
        ]

        self.session.reset_mock()
        self.cmd.synchronize_relations_for_case(
            case_uuid=str(case_uuid),
            custom_fields=custom_fields,
        )

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 13
        select_statement = call_list[0][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        select_statement = call_list[1][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        select_statement = call_list[2][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        select_statement = call_list[4][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT bibliotheek_kenmerken.id AS attribute_id, bibliotheek_kenmerken.magic_string, bibliotheek_kenmerken.uuid AS source_custom_field_type_id, custom_object_relationship.uuid, custom_object_relationship.custom_object_id, custom_object.uuid AS custom_object_uuid \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id JOIN zaak ON zaak_kenmerk.zaak_id = zaak.id LEFT OUTER JOIN custom_object_relationship ON bibliotheek_kenmerken.id = custom_object_relationship.source_custom_field_type_id LEFT OUTER JOIN custom_object ON custom_object.id = custom_object_relationship.custom_object_id \n"
            "WHERE custom_object_relationship.related_uuid = %(related_uuid_1)s AND custom_object_relationship.relationship_type = %(relationship_type_1)s AND bibliotheek_kenmerken.value_type = %(value_type_1)s AND bibliotheek_kenmerken.relationship_type = %(relationship_type_2)s AND zaak_kenmerk.zaak_id = (SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s)"
        )

        insert_case_relation = call_list[12][0][0]
        compiled_insert = insert_case_relation.compile(
            dialect=postgresql.dialect()
        )

        assert (
            str(compiled_insert)
            == "INSERT INTO custom_object_relationship (custom_object_id, custom_object_version_id, relationship_type, related_case_id, related_uuid, source_custom_field_type_id) VALUES (%(custom_object_id)s, %(custom_object_version_id)s, %(relationship_type)s, %(related_case_id)s, %(related_uuid)s, %(source_custom_field_type_id)s) RETURNING custom_object_relationship.id"
        )
        assert compiled_insert.params == {
            "custom_object_id": 103,
            "custom_object_version_id": None,
            "related_case_id": 16,
            "related_uuid": str(case_uuid),
            "relationship_type": "case",
            "source_custom_field_type_id": 104,
        }

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    def test_delete_object_attribute_relation(
        self,
        mock_object,
        mock_case,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        custom_object_uuid_1 = uuid4()
        custom_object_uuid_2 = str(uuid4())
        custom_fields = {
            "object_relation_1": [],
            "object_relation_2": [
                json.dumps(
                    {
                        "type": "relationship",
                        "value": custom_object_uuid_2,
                        "specifics": {
                            "metadata": {"summary": custom_object_uuid_2},
                            "relationship_type": "custom_object",
                        },
                    }
                )
            ],
            "test_subject": ["test"],
        }

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": uuid4(),
                            "field_magic_string": "test_subject",
                            "field_type": "text",
                            "name": "custom_object_1",
                            "public_name": "custom_object_1",
                        },
                        {
                            "uuid": type_uuid_1,
                            "field_magic_string": "object_relation_1",
                            "field_type": "relationship",
                            "relationship_type": "custom_object",
                            "name": "custom_object_2",
                            "public_name": "custom_object_2",
                        },
                        {
                            "uuid": type_uuid_2,
                            "field_magic_string": "object_relation_2",
                            "field_type": "relationship",
                            "relationship_type": "custom_object",
                            "name": "custom_object_3",
                            "public_name": "custom_object_3",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        mock_object_entity = mock.Mock()
        mock_object.return_value = mock_object_entity

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            namedtuple("RowProxy", "uuid")(uuid=custom_object_uuid_1),
        ]
        self.session.execute().fetchall.side_effect = [
            [],
            [],
        ]

        mock_case = mock.Mock()
        mock_case.configure_mock(meta=3, prefix="", id=31337, uuid=case_uuid)

        self.session.reset_mock()
        self.cmd.synchronize_relations_for_case(
            case_uuid=str(case_uuid),
            custom_fields=custom_fields,
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 5

        select_statement = call_list[0][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        select_statement = call_list[1][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        select_statement = call_list[2][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )

        select_statement = call_list[4][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert isinstance(select_statement, sql.Select)
        assert str(compiled_select) == (
            "SELECT bibliotheek_kenmerken.id AS attribute_id, bibliotheek_kenmerken.magic_string, bibliotheek_kenmerken.uuid AS source_custom_field_type_id, custom_object_relationship.uuid, custom_object_relationship.custom_object_id, custom_object.uuid AS custom_object_uuid \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id JOIN zaak ON zaak_kenmerk.zaak_id = zaak.id LEFT OUTER JOIN custom_object_relationship ON bibliotheek_kenmerken.id = custom_object_relationship.source_custom_field_type_id LEFT OUTER JOIN custom_object ON custom_object.id = custom_object_relationship.custom_object_id \n"
            "WHERE custom_object_relationship.related_uuid = %(related_uuid_1)s AND custom_object_relationship.relationship_type = %(relationship_type_1)s AND bibliotheek_kenmerken.value_type = %(value_type_1)s AND bibliotheek_kenmerken.relationship_type = %(relationship_type_2)s AND zaak_kenmerk.zaak_id = (SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s)"
        )

    @mock.patch(
        "zsnl_domains.case_management.repositories.case.CaseRepository.find_case_by_uuid"
    )
    @mock.patch(
        "zsnl_domains.case_management.repositories.custom_object.CustomObjectRepository.find_by_uuid"
    )
    def test_delete_object_relation_for_wrong_object(
        self,
        mock_object,
        mock_case,
    ):
        case_uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        type_uuid_1 = uuid4()
        type_uuid_2 = uuid4()
        custom_object_uuid_1 = str(uuid4())
        custom_object_uuid_2 = str(uuid4())
        custom_object_uuid_3 = uuid4()
        custom_fields = {
            "object_relation_1": [
                json.dumps(
                    {
                        "type": "relationship",
                        "value": custom_object_uuid_1,
                        "specifics": {
                            "metadata": {"summary": custom_object_uuid_1},
                            "relationship_type": "custom_object",
                        },
                    }
                )
            ],
            "object_relation_2": [
                json.dumps(
                    {
                        "type": "relationship",
                        "value": custom_object_uuid_2,
                        "specifics": {
                            "metadata": {"summary": custom_object_uuid_2},
                            "relationship_type": "custom_object",
                        },
                    }
                )
            ],
            "test_subject": ["test"],
        }

        mock_case_type_node = mock.Mock()
        mock_case_type_node.configure_mock(id=42)

        mock_case_type = self.__mock_case_type(
            case_type_uuid,
            phases=[
                {
                    "milestone": 1,
                    "name": "Registreeren",
                    "phase": 1,
                    "cases": [],
                    "subjects": [],
                    "custom_fields": [
                        {
                            "uuid": uuid4(),
                            "field_magic_string": "test_subject",
                            "field_type": "text",
                            "name": "custom_object_1",
                            "public_name": "custom_object_1",
                        },
                        {
                            "uuid": type_uuid_1,
                            "field_magic_string": "object_relation_1",
                            "field_type": "relationship",
                            "relationship_type": "custom_object",
                            "name": "custom_object_2",
                            "public_name": "custom_object_2",
                        },
                        {
                            "uuid": type_uuid_2,
                            "field_magic_string": "object_relation_2",
                            "field_type": "relationship",
                            "relationship_type": "custom_object",
                            "name": "custom_object_3",
                            "public_name": "custom_object_3",
                        },
                    ],
                    "documents": [],
                }
            ],
        )

        existing_object_relations = [mock.Mock(), mock.Mock()]

        existing_object_relations[0].configure_mock(
            entity_type="object_relation",
            entity_id=None,
            entity_meta_summary=None,
            entity_relationships=[],
            entity_meta__fields=["entity_meta_summary"],
            entity_id__fields=["uuid"],
            entity_changelog=[],
            attribute_id="32",
            source_custom_field_type_id=type_uuid_1,
            magic_string="object_relation_1",
            uuid=str(uuid4()),
            custom_object_uuid=custom_object_uuid_1,
        )
        existing_object_relations[1].configure_mock(
            entity_type="object_relation",
            entity_id=None,
            entity_meta_summary=None,
            entity_relationships=[],
            entity_meta__fields=["entity_meta_summary"],
            entity_id__fields=["uuid"],
            entity_changelog=[],
            attribute_id="33",
            source_custom_field_type_id=type_uuid_2,
            magic_string="object_relation_2",
            uuid=str(uuid4()),
            custom_object_uuid=custom_object_uuid_3,
        )

        mock_object_entity = mock.Mock()
        mock_object.return_value = mock_object_entity

        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            namedtuple("RowProxy", "uuid")(uuid=custom_object_uuid_1),
        ]
        self.session.execute().fetchall.side_effect = [
            [],
            existing_object_relations,
            [],
        ]

        mock_case = mock.Mock()
        mock_case.configure_mock(meta=3, prefix="", id=31337, uuid=case_uuid)
        self.session.reset_mock()

        self.cmd.synchronize_relations_for_case(
            case_uuid=str(case_uuid),
            custom_fields=custom_fields,
        )
        call_list = self.session.execute.call_args_list
        assert len(call_list) == 5

        select_statement = call_list[0][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        select_statement = call_list[1][0][0]
        assert isinstance(select_statement, sql.Select)

        select_statement = call_list[2][0][0]
        assert isinstance(select_statement, sql.Select)

        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_select) == (
            "SELECT 1 \n"
            "FROM zaaktype_status JOIN zaaktype_node ON zaaktype_status.status = %(status_1)s AND zaaktype_status.zaaktype_node_id = zaaktype_node.id AND zaaktype_node.uuid = %(uuid_1)s AND coalesce(CAST(zaaktype_node.adres_andere_locatie AS BOOLEAN), %(coalesce_1)s) IS false AND coalesce(CAST(zaaktype_node.adres_aanvrager AS BOOLEAN), %(coalesce_2)s) IS false"
        )

        select_statement = call_list[4][0][0]
        compiled_select = select_statement.compile(
            dialect=postgresql.dialect()
        )
        assert isinstance(select_statement, sql.Select)
        assert str(compiled_select) == (
            "SELECT bibliotheek_kenmerken.id AS attribute_id, bibliotheek_kenmerken.magic_string, bibliotheek_kenmerken.uuid AS source_custom_field_type_id, custom_object_relationship.uuid, custom_object_relationship.custom_object_id, custom_object.uuid AS custom_object_uuid \n"
            "FROM zaak_kenmerk JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id JOIN zaak ON zaak_kenmerk.zaak_id = zaak.id LEFT OUTER JOIN custom_object_relationship ON bibliotheek_kenmerken.id = custom_object_relationship.source_custom_field_type_id LEFT OUTER JOIN custom_object ON custom_object.id = custom_object_relationship.custom_object_id \n"
            "WHERE custom_object_relationship.related_uuid = %(related_uuid_1)s AND custom_object_relationship.relationship_type = %(relationship_type_1)s AND bibliotheek_kenmerken.value_type = %(value_type_1)s AND bibliotheek_kenmerken.relationship_type = %(relationship_type_2)s AND zaak_kenmerk.zaak_id = (SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s)"
        )

        self.session.reset_mock()
        self.session.execute().fetchone.side_effect = [
            mock_case_type_node,
            [mock_case_type],
            None,  # "is_simple_case_type"
            None,
        ]
        self.session.execute().fetchall.side_effect = [
            [],
            existing_object_relations,
        ]
        mock_case = mock.Mock()
        mock_case.configure_mock(meta=3, prefix="", id=31337, uuid=case_uuid)
