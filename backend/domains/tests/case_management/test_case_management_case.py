# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import json
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains.case_management.repositories import case_acl
from zsnl_domains.shared.repositories import case_acl as shared_case_acl


class TestCaseAclQueries:
    def test__user_is_admin(self):
        mock_db = mock.MagicMock()
        user_uuid = str(uuid4())

        mock_db.execute().fetchall.return_value = []
        is_admin = shared_case_acl._user_is_admin(mock_db, user_uuid)

        (args, kwargs) = mock_db.execute.call_args
        assert kwargs == {}

        query = args[0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert (
            str(query) == "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )

        assert query.params == {
            "name_1_1": "Administrator",
            "name_1_2": "Zaaksysteembeheerder",
            "uuid_1": user_uuid,
        }
        assert is_admin is False

        mock_db.execute().fetchall.return_value = ["something"]
        is_admin = shared_case_acl._user_is_admin(mock_db, user_uuid)

        assert is_admin is True

    def test_get_subject_by_id(self):
        db = mock.MagicMock()
        db.query().get = mock.MagicMock(return_value="test")

        res = case_acl.get_subject_by_id(1, db)

        assert res == "test"

    def test_get_subject_by_uuid(self):
        db = mock.MagicMock()
        db.query().join().filter().one = mock.MagicMock(return_value="test")

        res = case_acl.get_subject_by_uuid(uuid4(), db)

        assert res == "test"

    def test_get_involved_entity_by_id(self):
        db = mock.MagicMock()
        db.query().filter().one = mock.MagicMock(return_value="test")

        res = case_acl.get_involved_entity_by_id(1, db)

        assert res == "test"

    def test_add_entity_from_subject(self):
        db = mock.MagicMock()
        db.execute().inserted_primary_key = [2]
        properties = json.dumps(
            {
                "default_dashboard": 1,
                "mail": "auser@mintlab.nl",
                "sn": "testname",
                "title": "Administrator",
                "displayname": "auser",
                "initials": "a",
                "givenname": "auser",
            }
        )
        subject = {
            "id": "1",
            "uuid": uuid4(),
            "type": "employee",
            "properties": properties,
            "settings": {},
            "username": "Subject",
            "last_modified": None,
            "role_ids": [],
            "group_ids": [1, 2],
            "nobody": False,
            "system": True,
            "department_uuid": uuid4(),
            "department": "depatment",
            "employee_address": None,
            "Group": [],
        }
        res = case_acl.add_entity_from_subject(subject, 1, "Behandelaar", db)

        assert res == 2

    def test_add_entity_from_natural_person(self):
        db = mock.MagicMock()
        db.execute().inserted_primary_key = [3]

        person = mock.MagicMock()
        person.configure_mock(
            uuid=str(uuid4()),
            id=1,
            voornamen="Edwin",
            naamgebruik="T Jaison",
        )
        res = case_acl.add_entity_from_natural_person(
            person=person,
            person_snapshot_id=2,
            case_id=200,
            role="Aanvrager",
            db=db,
        )

        assert res == 3

    def test_add_entity_from_organization(self):
        db = mock.MagicMock()
        db.execute().inserted_primary_key = [5]
        org = {
            "id": "1",
            "uuid": uuid4(),
            "type": "organization",
            "handelsnaam": "abc.limited",
        }
        res = case_acl.add_entity_from_organization(
            organization=org,
            organization_snapshot_id=3,
            case_id=200,
            role="Aanvrager",
            db=db,
        )
        assert res == 5

    def mock_filter(self, args):
        _return_value = mock.MagicMock()
        _return_value.all.return_value = ["test"]
        assert str(args) == "zaak.uuid = :uuid_1 AND zaak.status != :status_1"
        self.count_mock_filter_call += 1
        return _return_value
