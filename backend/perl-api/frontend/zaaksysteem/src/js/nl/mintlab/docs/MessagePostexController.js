// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.MessagePostexController', [
    '$scope',
    '$rootScope',
    '$q',
    '$http',
    'smartHttp',
    'translationService',
    'systemMessageService',
    'snackbarService',
    'messagePostexService',
    function (
      $scope,
      $rootScope,
      $q,
      $http,
      smartHttp,
      translationService,
      systemMessageService,
      snackbarService,
      messagePostexService
    ) {
      var loading = false;
      let postexRecipientTypes;
      let availableSubjectRoles;

      /* Copy/pasta from https://www.npmjs.com/package/@opndev/util
       * MIT license, Wesley Schwengle <wesley@opndev.io>
       * See https://gitlab.com/opndev/opndev-js-util/-/blob/master/lib/defined.js
       */
      function defined(what) {
        if (what === null) return false;

        if (typeof what === 'undefined') return false;

        /* This will probably never be hit:
         * https://stackoverflow.com/questions/4725603/variable-undefined-vs-typeof-variable-undefined
         */
        if (what === undefined) return false;

        return true;
      }
      /* End of MIT license stuff */

      function getSubjectInformationFromAPI(caseObject) {
        let caseUuid = caseObject.reference;
        return $http
          .get(`/api/v2/cm/case/get_subject_relations?case_uuid=${caseUuid}`)
          .then(function (response) {
            const data = response.data.data;

            let roles = [];
            let authorized = false;
            let namesInRole = {};
            let authorizedNames = [];

            const rolesSkip = ['Behandelaar', 'Coordinator', 'Aanvrager'];

            data.forEach(function (item) {
              if (item.relationships.subject.data.type === 'employee') {
                return;
              }

              const role = item.attributes.role;
              if (rolesSkip.some((x) => x === role)) {
                return;
              }

              const displayName =
                item.relationships.subject.data.meta.display_name;

              if (role in namesInRole) {
                namesInRole[role].push(displayName);
              } else {
                namesInRole[role] = [displayName];
              }

              roles.push(role);
              if (item.attributes.authorized === true) {
                authorized = item.attributes.authorized;
                authorizedNames.push(displayName);
              }
              return;
            });

            roles = roles.filter((item, i, ar) => ar.indexOf(item) === i);
            roles = roles.map(function (item) {
              return {
                label: item,
                value: item,
              };
            });

            return {
              roles,
              authorized,
              namesInRole,
              authorizedNames,
            };
          });
      }

      function hasOnlyAllowedFiles(files) {
        return !Boolean(
          _.find(files, function (file, i) {
            return file.extension_dotless != 'pdf';
          })
        );
      }

      $scope.init = function () {
        postexRecipientTypes = [];
        const requestor = $scope.case.instance.requestor.instance;

        if (requestor.subject_type !== 'medewerker') {
          postexRecipientTypes.push({
            value: 'requestor',
            label: 'Aanvrager',
          });
        }

        postexRecipientTypes.push({
          value: 'ocr',
          label: 'OCR (gegevens uit bestand)',
        });

        getSubjectInformationFromAPI($scope.case).then(function (data) {
          if (!data.roles.length) {
            return;
          }

          availableSubjectRoles = data.roles;
          namesInRole = data.namesInRole;
          authorizedNames = data.authorizedNames;

          // Index for maintaining order
          const index = postexRecipientTypes.length - 1;

          if (data.authorized === true) {
            postexRecipientTypes.splice(index, 0, {
              value: 'authorized',
              label: 'Gemachtigde',
            });
          }

          postexRecipientTypes.splice(index, 0, {
            value: 'betrokkene',
            label: 'Betrokkene',
          });

          if (!$scope.typeRecipient)
            $scope.typeRecipient = postexRecipientTypes[0].value;

          if (!$scope.roleRecipient)
            $scope.roleRecipient = availableSubjectRoles[0].value;
        });
      };

      $scope.maySubmit = function () {
        if ($scope.loading) return false;

        if (!hasOnlyAllowedFiles($scope.attachments)) return false;

        const recipientType = $scope.typeRecipient;
        const employeeType = 'medewerker';

        /* Easy check, employees are never authorized. Roles are a
         * different beast but we need more information from the case.
         * We cannot see who are authorized as well. Additional calls
         * maybe?
         *
         * First make it work, than make a propper check.
         */
        if (
          recipientType === 'requestor' &&
          $scope.requestor.subject_type === employeeType
        )
          return false;

        return true;
      };

      $scope.handleTypeRecipientChange = function () {
        $scope.recipientAddress = '';
      };

      $scope.getRoles = function () {
        return availableSubjectRoles;
      };

      $scope.getRecipients = function () {
        return postexRecipientTypes;
      };

      $scope.getSubjectNames = function () {
        const recipientType = $scope.typeRecipient;
        if (recipientType === 'authorized') {
          return authorizedNames.join(', ');
        }

        if (recipientType === 'betrokkene') {
          return namesInRole[$scope.roleRecipient].join(', ');
        }

        return;
      };

      $scope.submit = function () {
        $scope.loading = true;

        snackbarService
          .wait('Bezig met versturen...', {
            promise: messagePostexService.submit(
              $scope.postexInterfaceUUID,
              $scope.requestor.uuid,
              {
                sender_uuid: $scope.subjectUUID,

                recipient_type: $scope.typeRecipient,
                recipient_role:
                  $scope.typeRecipient === 'betrokkene'
                    ? $scope.roleRecipient
                    : null,

                case_id: $scope.case.instance.number,

                subject: $scope.subject,
                body: $scope.content,

                file_attachments: _.map($scope.attachments, function (file, i) {
                  return file.id;
                }),
              }
            ),
            then: function (response) {
              return 'Het bericht is afgeleverd bij Postex.';
            },
            catch: function (error) {
              if (_.get(error, 'data.result.instance.message')) {
                return _.get(error, 'data.result.instance.message');
              } else if (_.get(error, 'data.result.preview')) {
                return _.get(error, 'data.result.preview');
              } else if (typeof error === 'string') {
                return error;
              } else {
                return 'Er is iets fout gegaan bij het versturen van het bericht. Neem contact op met de beheerder';
              }
            },
            finally: function () {
              $scope.loading = false;
            },
          })
          .then(function () {
            $scope.closePopup();
          })
          .finally(function () {
            $scope.loading = false;
          });
      };
    },
  ]);
