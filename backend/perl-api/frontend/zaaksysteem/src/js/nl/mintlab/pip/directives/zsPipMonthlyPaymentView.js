// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.pip').directive('zsPipMonthlyPaymentView', [
    '$window',
    '$http',
    'translationService',
    'systemMessageService',
    'dateFilter',
    function (
      $window,
      $http,
      translationService,
      systemMessageService,
      dateFilter
    ) {
      var crudConfig = {
        options: {
          select: 'single',
          pagination: false,
        },
        actions: [
          {
            id: 'download',
            type: 'click',
            data: {
              click: 'pipMonthlyPaymentView.downloadPdf($selection[0])',
            },
            label: translationService.get('Download'),
          },
        ],
        columns: [
          {
            id: 'regeling',
            label: translationService.get('Regeling'),
            template: '<[item.Dossierhistorie[0].Regeling]>)',
            locked: true,
            sort: false,
          },
          {
            id: 'year',
            label: translationService.get('Jaar'),
            template:
              '<[pipMonthlyPaymentView.getYearLabel(pipMonthlyPaymentView.month.year)]>',
            locked: true,
            sort: false,
          },
          {
            id: 'month',
            label: translationService.get('Maand'),
            template:
              '<[pipMonthlyPaymentView.getMonthLabel(pipMonthlyPaymentView.month.month)]>',
            locked: true,
            sort: false,
          },
          {
            id: 'actions',
            label: 'Acties',
            locked: true,
            templateUrl: '/html/core/crud/crud-item-action-menu.html',
            dynamic: true,
            sort: false,
          },
        ],
      };

      return {
        scope: true,
        require: ['zsPipMonthlyPaymentView', '^zsPipPaymentView'],
        controller: [
          function () {
            var ctrl = this,
              zsPipPaymentView,
              years = [],
              months = [],
              items = [];

            function getMonthData() {
              $http({
                method: 'GET',
                url:
                  '/pip/gws4all/specificatie?period=' +
                  ctrl.month.year +
                  ctrl.month.month,
              })
                .success(function (response) {
                  items = response.result[0].Uitkeringsspecificatie;
                })
                .error(function () {
                  systemMessageService.emitLoadError('uw specificaties');
                });
            }

            ctrl.link = function (controllers) {
              zsPipPaymentView = controllers[0];

              years = zsPipPaymentView.getYears().concat().reverse();
              months = _.range(1, 13);

              ctrl.month = {
                year: _.last(years),
                month: _.last(months),
              };

              getMonthData();
            };

            ctrl.getYears = function () {
              return zsPipPaymentView.getYears();
            };

            ctrl.getMonths = function () {
              return months;
            };

            ctrl.getCrudConfig = function () {
              return crudConfig;
            };

            ctrl.getItems = function () {
              return items;
            };

            ctrl.handleMonthChange = function () {
              getMonthData();
            };

            ctrl.handleYearChange = function () {
              getMonthData();
            };

            ctrl.getYearLabel = function (year) {
              var date = new Date(year, 0, 1),
                label;

              label = dateFilter(date, 'yyyy');

              return label;
            };

            ctrl.getMonthLabel = function (month) {
              var date = new Date(new Date().getFullYear(), month - 1, 1),
                label;

              label = dateFilter(date, 'MMMM');

              return label;
            };

            ctrl.downloadPdf = function (item) {
              var month = ctrl.month.month;

              if (month < 10) {
                month = '0' + month;
              } else {
                month = month.toString();
              }

              $window.open(
                '/pip/gws4all/specificatie/pdf?period=' +
                  ctrl.month.year +
                  month +
                  '&index=' +
                  _.indexOf(items, item)
              );
            };

            return ctrl;
          },
        ],
        controllerAs: 'pipMonthlyPaymentView',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
