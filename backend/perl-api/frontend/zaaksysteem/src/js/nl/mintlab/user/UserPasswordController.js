// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.user')
    .controller('nl.mintlab.user.UserPasswordController', [
      '$scope',
      'smartHttp',
      'translationService',
      function ($scope, smartHttp, translationService) {
        var getPasswordStrength = window.zsFetch(
            'nl.mintlab.utils.getPasswordStrength'
          ),
          safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
          nwPass = '';

        $scope.getPassStrength = function () {
          var passStrength = getPasswordStrength('', nwPass || '');
          return passStrength;
        };

        $scope.hasNewPass = function () {
          return !!nwPass;
        };

        $scope.$on('form.submit.success', function (/*event, name, data*/) {
          $scope.$emit('systemMessage', {
            type: 'info',
            content: translationService.get(
              'Uw wachtwoord is succesvol gewijzigd.'
            ),
          });
        });

        $scope.$on('form.submit.error', function (event, name, data) {
          var error = data.result ? data.result[0] : null,
            type = error ? error.type : '',
            errorObj = {
              type: 'error',
            };

          switch (type) {
            case 'user/password/wrong_credentials':
              errorObj.content = translationService.get(
                'Incorrect huidig wachtwoord. Probeer opnieuw.'
              );
              break;

            default:
              errorObj.content = translationService.get(
                'Er ging iets fout bij het wijzigen van uw wachtwoord. Probeer het later opnieuw.'
              );
              break;
          }

          $scope.$emit('systemMessage', errorObj);
        });

        $scope.$on('form.change', function (event, field, nwVal /*, oldVal*/) {
          safeApply($scope, function () {
            if (field.name === 'new_password') {
              nwPass = nwVal;
            }
          });
        });
      },
    ]);
})();
