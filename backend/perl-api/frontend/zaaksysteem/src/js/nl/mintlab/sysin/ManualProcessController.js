// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.sysin')
    .controller('nl.mintlab.sysin.ManualProcessController', [
      '$scope',
      'translationService',
      'formService',
      function ($scope, translationService, formService) {
        function getFormValue(property) {
          var form = formService.get('manual-process'),
            val;

          if (form) {
            val = form.getValue(property);
          }

          return val;
        }

        $scope.getManualUploadOptions = function () {
          var options = [],
            interfaces = $scope.interfaces || [],
            item,
            i,
            l;

          for (i = 0, l = interfaces.length; i < l; ++i) {
            item = interfaces[i];

            if (item.active) {
              options.push({
                name: item.name,
                value: item.id,
                label: item.name + ' (' + item.id + ')',
              });
            }
          }

          options.sort(function (a, b) {
            return a.label.localeCompare(b.label);
          });

          return options;
        };

        $scope.allowUploadType = function (interfaceId, type) {
          var interfaces = $scope.interfaces,
            intFace,
            i,
            l,
            module,
            isAllowed = false;

          for (i = 0, l = interfaces.length; i < l; ++i) {
            if (interfaces[i].id === interfaceId) {
              intFace = interfaces[i];
              break;
            }
          }

          if (intFace) {
            module = $scope.getModuleByName(intFace.module);
            if (module) {
              isAllowed =
                module.manual_type &&
                _.indexOf(module.manual_type, type) !== -1;
            }
          }
          return isAllowed;
        };

        $scope.getSubmitUrl = function (interfaceId) {
          return '/sysin/interface/' + interfaceId + '/manual_process';
        };

        $scope.getDefaultInterfaceId = function () {
          return $scope.link ? $scope.link.id : null;
        };

        $scope.isFieldVisible = function (type) {
          var isVisible =
            getFormValue('interface') &&
            $scope.allowUploadType(getFormValue('interface'), type);
          return isVisible;
        };

        $scope.isSubmitDisabled = function () {
          return !_.some(['file', 'text', 'references'], function (type) {
            var isVisible = $scope.isFieldVisible(type),
              hasData;

            switch (type) {
              case 'text':
                hasData = !!getFormValue('input_data');
                break;

              case 'file':
                hasData =
                  getFormValue('input_filestore_uuid') &&
                  getFormValue('input_filestore_uuid').length;
                break;

              case 'references':
                hasData =
                  getFormValue('input_references') &&
                  getFormValue('input_references').length;
                break;

              default:
                hasData = false;
            }

            return isVisible && hasData;
          });
        };

        $scope.$on('form.submit.success', function (event, name, data) {
          var transactionId;
          if (event.targetScope.getFormName() === 'manual-process') {
            transactionId = data.result[0].id;

            $scope.$emit('systemMessage', {
              type: 'info',
              content:
                "<a target='_parent' href='/main/transactions/" +
                transactionId +
                "'>" +
                translationService.get('Transactie geaccepteerd') +
                '</a>',
            });
          }
        });

        $scope.$on('form.submit.error', function (event, name, data) {
          var errorMessage,
            error = data.result ? data.result[0] : null;

          if (error && error.type === 'sysin/modules/process/inactive_module') {
            errorMessage = 'Deze module is niet actief';
          } else {
            errorMessage = translationService.get(
              'Er ging iets fout bij het verwerken van de transactie'
            );
          }

          $scope.$emit('systemMessage', {
            type: 'error',
            content: errorMessage,
          });
        });
      },
    ]);
})();
