// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem').directive('ngMultiselect', [
    function () {
      var query = 'input[type="checkbox"]',
        indexOf = window.zsFetch('nl.mintlab.utils.shims.indexOf'),
        cancelEvent = window.zsFetch('nl.mintlab.utils.events.cancelEvent');

      return function (scope, element, attrs) {
        var listeningTo = [],
          lastClicked,
          isEnabled,
          unwatch;

        function evaluateDirective() {
          var shouldBeEnabled =
            attrs.ngMultiselect !== ''
              ? scope.$eval(attrs.ngMultiselect)
              : true;
          if (shouldBeEnabled !== isEnabled) {
            if (shouldBeEnabled) {
              enable();
            } else {
              disable();
            }
          }
        }

        function enable() {
          unwatch = scope.$watch(function () {
            setListeners();
          });
        }

        function disable() {
          var input;

          while (listeningTo.length) {
            input = listeningTo.shift();
            angular.element(input).unbind('click', onInputClick);
          }

          if (unwatch) {
            unwatch();
          }

          unwatch = null;
        }

        function getInputList() {
          return element[0].querySelectorAll(query);
        }

        function setListeners() {
          var list = getInputList(),
            i,
            l,
            input,
            queue = listeningTo.concat(),
            index;

          for (i = 0, l = list.length; i < l; i++) {
            input = list[i];
            index = indexOf(queue, input);
            if (index === -1) {
              listeningTo.push(input);
              angular.element(input).bind('click', onInputClick);
            } else {
              listeningTo.splice(index, 1);
              queue.splice(index, 1);
            }
          }

          for (i = 0, l = queue.length; i < l; ++i) {
            input = queue[i];
            angular.element(input).unbind('click', onInputClick);
          }
        }

        function onInputClick(event) {
          var list, input, i, l, start, end;

          if (!event) {
            return;
          }

          if (event.shiftKey && lastClicked) {
            cancelEvent(event);

            list = getInputList();

            start = indexOf(list, lastClicked);
            end = indexOf(list, event.target);

            if (end < start) {
              (i = end), (l = i + (start - end + 1));
            } else {
              i = start;
              l = i + (end - start + 1);
            }

            for (; i < l; ++i) {
              input = angular.element(list[i]);
              if (input.attr('checked') !== 'checked') {
                input[0].click();
              }
            }
          }
          lastClicked = event.target;
        }

        scope.$watch('ngMultiselect', function () {
          evaluateDirective();
        });
      };
    },
  ]);
})();
