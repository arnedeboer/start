// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.utils.events.cancelEvent', function () {
    var win = window,
      event = win.document.createEvent
        ? win.document.createEvent('Event')
        : win.document.createEventObject();

    if (event.stopPropagation) {
      return function (event, preventDefault) {
        if (preventDefault === undefined) {
          preventDefault = true;
        }
        event.stopPropagation();
        event.stopImmediatePropagation();
        if (preventDefault) {
          event.preventDefault();
        }
        return false;
      };
    } else {
      return function (event, preventDefault) {
        if (preventDefault === undefined) {
          preventDefault = true;
        }
        event.returnValue = !preventDefault;
        event.cancelBubble = true;
        return false;
      };
    }
  });
})();
