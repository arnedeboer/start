// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').filter('truncate', [
    function () {
      return function (str, max) {
        if (!str || max === undefined) {
          return str;
        }

        if (str.length > max) {
          str = str.substr(0, max) + '…';
        }

        return str;
      };
    },
  ]);
})();
