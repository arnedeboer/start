// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  const conversions = {
    '!=': '!',
    '==': '=',
    '<=': '[',
    '>=': ']',
  };

  const symbols = {
    '^': { infix: '_POW' },
    '*': { infix: '_MUL' },
    '/': { infix: '_DIV' },
    '%': { infix: '_MOD' },
    '+': { infix: '_ADD', prefix: '_POS' },
    '-': { infix: '_SUB', prefix: '_NEG' },
    ']': { infix: '_EGT' },
    '>': { infix: '_GT' },
    '<': { infix: '_LT' },
    '[': { infix: '_ELT' },
    '=': { infix: '_EQ' },
    '!': { infix: '_NEQ' },
  };

  const operators = {
    _POW: {
      name: 'Power',
      precedence: 4,
      associativity: 'right',
      method: (x, y) => Math.pow(x, y),
    },
    _POS: {
      name: 'Positive',
      precedence: 3,
      associativity: 'right',
      method: (x) => x,
    },
    _NEG: {
      name: 'Negative',
      precedence: 3,
      associativity: 'right',
      method: (x) => -x,
    },
    _MUL: {
      name: 'Multiply',
      precedence: 2,
      associativity: 'left',
      method: (x, y) => x * y,
    },
    _DIV: {
      name: 'Divide',
      precedence: 2,
      associativity: 'left',
      method: (x, y) => x / y,
    },
    _MOD: {
      name: 'Modulo',
      precedence: 2,
      associativity: 'left',
      method: (x, y) => x % y,
    },
    _ADD: {
      name: 'Add',
      precedence: 1,
      associativity: 'left',
      method: (x, y) => x + y,
    },
    _SUB: {
      name: 'Subtract',
      precedence: 1,
      associativity: 'left',
      method: (x, y) => x - y,
    },
    _GT: {
      name: 'Greater than',
      precedence: 0,
      associativity: 'left',
      method: (x, y) => x > y,
    },
    _LT: {
      name: 'Lesser than',
      precedence: 0,
      associativity: 'left',
      method: (x, y) => x < y,
    },
    _EGT: {
      name: 'Equal to or greater than',
      precedence: 0,
      associativity: 'left',
      method: (x, y) => x >= y,
    },
    _ELT: {
      name: 'Equal to or lesser than',
      precedence: 0,
      associativity: 'left',
      method: (x, y) => x <= y,
    },
    _EQ: {
      name: 'Equal',
      precedence: 0,
      associativity: 'left',
      method: (x, y) => x == y,
    },
    _NEQ: {
      name: 'Not equal',
      precedence: 0,
      associativity: 'left',
      method: (x, y) => x != y,
    },
  };

  const isSymbol = (token) => Object.keys(symbols).includes(token);
  const isOperator = (token) => Object.keys(operators).includes(token);
  const isNumber = (token) => /(\d+\.\d*)|(\d*\.\d+)|(\d+)/.test(token);
  const isOpenParenthesis = (token) => /\(/.test(token);
  const isCloseParenthesis = (token) => /\)/.test(token);
  const isComma = (token) => /,/.test(token);
  const isWhitespace = (token) => /\s/.test(token);

  const round = (number, precision) => {
    const modifier = Math.pow(10, precision);

    return !modifier
      ? Math.round(number)
      : Math.round(number * modifier) / modifier;
  };

  function topOperatorHasPrecedence(operatorStack, currentOperatorName) {
    if (!operatorStack.length) return false;

    const topToken = operatorStack[operatorStack.length - 1];

    if (!isOperator(topToken)) return false;

    const topOperator = operators[topToken];
    const currentOperator = operators[currentOperatorName];

    if (currentOperator.method.length === 1 && topOperator.method.length > 1)
      return false;

    if (topOperator.precedence > currentOperator.precedence) return true;

    return (
      topOperator.precedence === currentOperator.precedence &&
      topOperator.associativity === 'left'
    );
  }

  function determineOperator(token, previousToken) {
    if (
      previousToken === undefined ||
      isOpenParenthesis(previousToken) ||
      isSymbol(previousToken) ||
      isComma(previousToken)
    ) {
      return symbols[token].prefix;
    }

    if (isCloseParenthesis(previousToken) || isNumber(previousToken)) {
      return symbols[token].infix;
    }

    return undefined;
  }

  function parse(expression) {
    if (!expression.length) {
      throw Error('No input');
    }

    const pattern = /(\d+\.\d*)|(\d*\.\d+)|(\d+)|([a-zA-Z0-9_]+)|(.)/g;

    const infixExpression = (expression.match(pattern) || [])
      .filter((token) => !isWhitespace(token))
      .map((token) => token.toUpperCase());

    return infixExpression;
  }

  function convert(infixExpression) {
    if (!infixExpression.length) {
      throw Error('No valid tokens');
    }

    const operatorStack = [];
    const arityStack = [];
    const postfixExpression = [];
    let methodIsNewlyDeclared = false;

    infixExpression.forEach((token, index) => {
      if (methodIsNewlyDeclared && !isOpenParenthesis(token)) {
        throw Error(
          `Misused method: ${operatorStack[operatorStack.length - 1]}`
        );
      }

      if (isNumber(token)) {
        postfixExpression.push(parseFloat(token));
        return;
      }

      if (isSymbol(token)) {
        const operatorName = determineOperator(
          token,
          infixExpression[index - 1]
        );
        const operator = operators[operatorName];

        if (operator === undefined) {
          throw Error(`Misused operator: ${token}`);
        }

        while (topOperatorHasPrecedence(operatorStack, operatorName)) {
          postfixExpression.push(operatorStack.pop());
        }

        operatorStack.push(operatorName);
        return;
      }

      if (isOpenParenthesis(token)) {
        operatorStack.push(token);
        return;
      }

      if (isComma(token)) {
        arityStack[arityStack.length - 1] += 1;

        while (!isOpenParenthesis(operatorStack[operatorStack.length - 1])) {
          if (!operatorStack.length) {
            throw Error('Invalid token: ,');
          }

          postfixExpression.push(operatorStack.pop());
        }

        return;
      }

      if (isCloseParenthesis(token)) {
        while (!isOpenParenthesis(operatorStack[operatorStack.length - 1])) {
          if (!operatorStack.length) {
            throw Error('Mismatched parentheses');
          }

          postfixExpression.push(operatorStack.pop());
        }

        operatorStack.pop();

        return;
      }

      throw Error(`Invalid token: ${token}`);
    });

    while (operatorStack.length) {
      const operator = operatorStack[operatorStack.length - 1];

      if (isOpenParenthesis(operator) || isCloseParenthesis(operator)) {
        throw Error('Mismatched parentheses');
      }

      postfixExpression.push(operatorStack.pop());
    }

    return postfixExpression;
  }

  function resolve(postfixExpression) {
    if (!postfixExpression.length) {
      throw Error('No operations');
    }

    const evaluationStack = [];

    postfixExpression.forEach((token) => {
      if (isNumber(token)) {
        evaluationStack.push(token);
        return;
      }

      const operator = operators[token];

      if (evaluationStack.length < operator.method.length) {
        throw Error(`Insufficient operands for operator: ${operator.name}`);
      }

      if (token === '_DIV' && evaluationStack[1] === 0) {
        throw Error('Division by zero');
      }

      const result = operator.method(
        ...evaluationStack.splice(-operator.method.length)
      );
      evaluationStack.push(result);
    });

    if (evaluationStack.length > 1) {
      throw Error('Insufficient operators');
    }

    const reduction = evaluationStack[0];
    const result = round(reduction, 8);

    return result;
  }

  function evaluateMathString(rawExpression) {
    let expression = rawExpression;
    Object.keys(conversions).forEach((k) => {
      expression = expression.replace(k, conversions[k]);
    });
    expression = parse(expression.replace(/(:pi|constant\("pi"\))/g, Math.PI));
    const postfixExpression = convert(expression);
    const result = resolve(postfixExpression);
    return result;
  }

  angular.module('Zaaksysteem.case').directive('zsCaseWebformRuleManager', [
    'objectService',
    'zsCalculateDate',
    function (objectService, zsCalculateDate) {
      return {
        require: ['zsCaseWebformRuleManager', '^?zsCaseView'],
        controller: [
          '$scope',
          function ($scope) {
            var ctrl = this,
              zsCaseView,
              visibility = {},
              disabled = {},
              controls = [],
              values = {},
              paused = {},
              fixedValues = {},
              revalidateAttributes = [];

            function isValid(rule, values) {
              var relation = rule.conditions.type,
                filter = relation === 'and' ? _.every : _.some;

              return (
                !rule.conditions.conditions.length ||
                filter(rule.conditions.conditions, function (condition) {
                  return matchCondition(condition, values);
                })
              );
            }

            function isFile(key) {
              var value = ctrl.getValue(key),
                file = _.isArray(value) ? value[0] : value,
                isF;

              if (file) {
                isF = file.filename !== undefined;
              }

              return isF;
            }

            function matchCondition(condition, values) {
              var attributeName = condition.attribute_name,
                attrValue =
                  values[attributeName] !== undefined
                    ? values[attributeName]
                    : ctrl.getValue(attributeName),
                matches,
                isArray = _.isArray(attrValue);

              if (attrValue && attrValue.original !== undefined) {
                attrValue = attrValue.original;
              }

              if (condition.validation_type === 'revalidate') {
                revalidateAttributes.push(attributeName);
              }

              if (condition.validates_true !== undefined) {
                matches = !!condition.validates_true;
              } else {
                if (visibility[attributeName] === false) {
                  attrValue = null;
                }

                matches = _.some(condition.values, function (val) {
                  var m;
                  if (isArray) {
                    m = _.indexOf(attrValue, val) !== -1;
                  } else {
                    m = objectService.isEqualValue(attrValue, val);
                  }
                  return m;
                });
              }

              return matches;
            }

            function getValue(key) {
              var value;

              if (zsCaseView) {
                value = zsCaseView.getCaseValue(key);
              } else {
                value = values[key];
              }

              return value;
            }

            function unsetExec(actions) {
              _.each(actions, function (action) {
                delete action.executed;
              });
            }

            function calculate(expression, values) {
              var val;

              expression = expression.replace(
                /([a-zA-Z]{1,}(?:[a-zA-Z0-9_\.]+))/g,
                function () {
                  var key = arguments[1],
                    value;

                  if (key.indexOf('.') === -1) {
                    key = 'attribute.' + key;
                  }

                  value = values[key];

                  if (_.isArray(value)) {
                    value = value[0];
                  }

                  // ZS-13747: We want to allow calculating values that use a comma for a decimal separator.
                  if (typeof value === 'string' && value.indexOf(',') !== -1) {
                    value = String(value).replace(',', '.');
                  }

                  return isNaN(value) ||
                    value === undefined ||
                    value === null ||
                    value === ''
                    ? 0
                    : Number(value);
                }
              );

              try {
                val = evaluateMathString(expression);
                if (
                  isNaN(val) ||
                  val === Number.POSITIVE_INFINITY ||
                  val === Number.NEGATIVE_INFINITY
                ) {
                  val = null;
                } else {
                  val = parseFloat(Math.round(val * 100) / 100).toFixed(2);
                }
              } catch (e) {
                val = null;
              }

              return val;
            }

            function applyRules() {
              var vals = {},
                caseValues = _.mapValues(ctrl.getValues(), function (
                  value,
                  key
                ) {
                  return zsCaseView ? zsCaseView.getCaseValue(key) : value;
                }),
                invalidated,
                children;

              visibility = {};
              disabled = {};
              paused = {};
              fixedValues = {};

              revalidateAttributes = [];

              _.each(controls, function (child) {
                _.each(child.getRules(), function (rule) {
                  var valid = isValid(rule, vals),
                    actions;

                  rule.valid = valid;

                  if (valid) {
                    actions = rule.then;
                    unsetExec(rule['else']);
                  } else {
                    actions = rule['else'];
                    unsetExec(rule.then);
                  }

                  _.each(actions, function (action) {
                    switch (action.type) {
                      default:
                        break;

                      case 'hide_attribute':
                      case 'hide_group':
                        visibility[action.data.attribute_name] = false;
                        break;

                      case 'show_attribute':
                      case 'show_group':
                        delete visibility[action.data.attribute_name];
                        break;

                      case 'hide_text_block':
                        var textBlockId =
                          'text_block_' + action.data.attribute_name;
                        visibility[textBlockId] = false;
                        break;

                      case 'show_text_block':
                        var textBlockId =
                          'text_block_' + action.data.attribute_name;
                        delete visibility[textBlockId];
                        break;

                      case 'set_value':
                        if (!action.data.can_change || !action.executed) {
                          vals[action.data.attribute_name] = action.data.value;
                        }
                        if (!action.data.can_change) {
                          fixedValues[action.data.attribute_name] =
                            action.data.value;
                        }
                        break;

                      case 'pause_application':
                        paused[child.getPhaseId()] = rule;
                        break;

                      case 'set_value_formula':
                        fixedValues[action.data.attribute_name] = vals[
                          action.data.attribute_name
                        ] = calculate(
                          action.data.formula,
                          _.assign({}, vals, caseValues, fixedValues)
                        );
                        break;

                      case 'date_calculations': {
                        var sourceAttr = action.data.source_attribute;
                        var targetAttr = action.data.target_attribute;
                        var baseValue = caseValues[sourceAttr];

                        if (!baseValue) break;

                        var country = 'nl';
                        var institutionType = 'government';

                        var date = zsCalculateDate(
                          baseValue,
                          action.data.math,
                          country,
                          institutionType
                        );

                        var newDate = new Date(date);

                        var day = newDate.getDate();
                        var month = newDate.getMonth() + 1;
                        var year = newDate.getFullYear();
                        var newDateString = `${day}-${month}-${year}`;

                        fixedValues[targetAttr] = newDateString;

                        if (!action.executed) {
                          vals[targetAttr] = newDateString;
                        }
                        fixedValues[targetAttr] = newDateString;

                        break;
                      }
                    }
                    action.executed = true;
                  });
                });
              });

              // reset all values of hidden attributes/groups

              children = _(controls)
                .map(function (control) {
                  return control.getControls();
                })
                .flatten()
                .groupBy(function (control) {
                  return control.getGroupId();
                })
                .value();

              _.each(visibility, function (value, key) {
                if (!value && !isFile(key)) {
                  if (!isNaN(Number(key)) && children[Number(key)]) {
                    _.each(children[Number(key)], function (child) {
                      vals[child.getAttributeName()] = null;
                    });
                  } else {
                    vals[key] = null;
                  }
                }
              });

              _.each(vals, function (value, key) {
                disabled[key] = true;
                if (!objectService.isEqualValue(value, getValue(key))) {
                  ctrl.setValue(key, value);
                  invalidated = true;
                }
              });

              if (invalidated) {
                _.each(controls, function (child) {
                  child.invalidateForm();
                });
              }
            }

            function onUpdate() {
              ctrl.invalidateRules();
            }

            function reloadRulesOf(attributeName, value) {
              _.each(controls, function (child) {
                if (
                  _.indexOf(child.getAttributeNames(), attributeName) !== -1
                ) {
                  child.reloadRules(attributeName, value);
                }
              });
            }

            ctrl.link = function (controllers) {
              zsCaseView = controllers[0];
              if (zsCaseView) {
                zsCaseView.updateListeners.push(onUpdate);
                $scope.$on('$destroy', function () {
                  _.pull(zsCaseView.updateListeners, onUpdate);
                });
              }
            };

            ctrl.addControl = function (child) {
              controls.push(child);
            };

            ctrl.removeControl = function (child) {
              _.pull(controls, child);
            };

            ctrl.setValue = function (key, value, resolved) {
              if (zsCaseView) {
                zsCaseView.setCaseValue(key, value, resolved);
              } else {
                values[key] = value;
              }

              if (_.indexOf(revalidateAttributes, key) !== -1) {
                reloadRulesOf(key, resolved);
              }
            };

            ctrl.getValue = function (key) {
              var value;

              value = getValue(key);

              return value;
            };

            ctrl.getValues = function () {
              var vals = angular.copy(values),
                caseObj = zsCaseView ? zsCaseView.getCase() : null;

              if (caseObj) {
                angular.extend(vals, angular.copy(caseObj.values));
              }

              return vals;
            };

            ctrl.isFieldVisible = function (attributeName) {
              return visibility[attributeName] !== false;
            };

            ctrl.isFixedValue = function (attributeName) {
              return fixedValues[attributeName] !== undefined;
            };

            ctrl.getFixedValue = function (attributeName) {
              var value = fixedValues[attributeName],
                label;

              if (
                value === null ||
                value === undefined ||
                (typeof value === 'number' && isNaN(value)) ||
                value === Number.POSITIVE_INFINITY ||
                value === Number.NEGATIVE_INFINITY
              ) {
                label = '-';
              } else {
                if (typeof value === 'number') {
                  label = parseFloat(value).toFixed(2);
                } else {
                  label = String(value);
                }
              }
              return label;
            };

            ctrl.invalidateRules = function () {
              applyRules();
            };

            ctrl.getApplicationPauseRule = function (phaseId) {
              return paused[phaseId];
            };

            ctrl.matchCondition = function (condition, values) {
              return matchCondition(condition, values);
            };

            ctrl.getAttributeNames = function () {
              return _.uniq(
                _.flatten(
                  _.map(controls, function (child) {
                    return child.getAttributeNames();
                  })
                )
              );
            };

            return ctrl;
          },
        ],
        controllerAs: 'caseWebformRuleManager',
        link: function (scope, element, attrs, controllers) {
          var zsCaseWebformRuleManager = controllers[0];
          zsCaseWebformRuleManager.link(controllers.slice(1));
        },
      };
    },
  ]);
})();
