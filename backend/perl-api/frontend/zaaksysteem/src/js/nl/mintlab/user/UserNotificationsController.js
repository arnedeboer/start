// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.user')
    .controller('nl.mintlab.user.UserNotificationsController', [
      '$scope',
      'smartHttp',
      'translationService',
      'systemMessageService',
      function ($scope, smartHttp, translationService, systemMessageService) {
        $scope.$on('form.submit.success', function () {
          $scope.$emit('systemMessage', {
            type: 'info',
            content: translationService.get(
              'Notificatie instellingen opgeslagen.'
            ),
          });
        });
      },
    ]);
})();
