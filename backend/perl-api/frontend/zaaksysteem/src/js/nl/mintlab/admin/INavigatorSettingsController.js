// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  'use strict';
  angular
    .module('Zaaksysteem.admin')
    .controller('nl.mintlab.admin.INavigatorSettingsController', [
      '$scope',
      '$window',
      'smartHttp',
      'translationService',
      function ($scope, $window, smartHttp, translationService) {
        $scope.restoreDefaultSettings = function () {
          $scope.loading = true;
          smartHttp
            .connect({
              method: 'POST',
              url: '/api/casetype/inavigator/restore_default_settings',
              data: {
                code: $scope.$parent.code,
              },
            })
            .success(function (response) {
              var result = response.result[0];
              $scope.$emit('systemMessage', {
                type: 'info',
                content: result.message,
              });
              $scope.$parent.settings = result.settings;
              $scope.$parent.prefillDocumentPhases();
              if ($scope.matches.length === 0) {
                $scope.$parent.templateCasetype = null;
              }
            })
            .error(function (response) {
              var messages = response.result[0].messages.join(',');
              $scope.$emit('systemMessage', {
                type: 'error',
                content: messages,
              });
            })
            ['finally'](function () {
              $scope.loading = false;
            });
        };
      },
    ]);
})();
