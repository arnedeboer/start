// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformAddress2Field', [
    function () {
      return {
        scope: true,
        require: [
          'zsCaseWebformAddress2Field',
          '^zsCaseWebformField',
          '^zsCaseWebformRuleManager',
        ],
        controller: [
          'smartHttp',
          '$element',
          '$compile',
          '$scope',
          'sessionService',
          'composedReducer',
          function (
            smartHttp,
            $element,
            $compile,
            $scope,
            sessionService,
            composedReducer
          ) {
            var ctrl = this,
              parentController,
              config,
              inputs = Array.from($element[0].querySelectorAll('input')),
              placeholder = $element[0].querySelector('.address-placeholder'),
              veldoptieId = placeholder.getAttribute('veldoptie-id'),
              iframe = document.createElement('iframe');
            currentValue = null;
            currentValueFlat = null;
            const input = inputs[0];

            if (input && input.value) {
              try {
                currentValue = JSON.parse(input.value);
                currentValueFlat = { label: currentValue.address.full };
              } catch (err) {}
            }

            try {
              angular.forEach(
                $element.parent().parent().parent().children(),
                function (el) {
                  el.className ===
                    'kenmerk-veld ezra_field_wrapper row ng-scope' &&
                    $(el).insertAfter(el.parentElement);
                }
              );
            } catch (err) {
              console.log(err);
            }

            var sessionResource = sessionService.createResource($scope);

            var sessionReducer = composedReducer(
              { scope: $scope },
              sessionResource
            ).reduce(function (session) {
              return session ? session.instance : {};
            });

            function config() {
              return sessionReducer.data().configurable;
            }

            Object.defineProperty($scope, 'config', { value: config });

            Object.defineProperty($scope, 'spotEnlighterAddressValue', {
              get: function () {
                return currentValueFlat;
              },
              set: function (enlighterVal) {
                if (!enlighterVal) {
                  input.value = '';
                  currentValue = null;
                  currentValueFlat = null;
                  sendMessage({
                    type: 'setMarker',
                    version: 5,
                    name: input.name,
                    value: null,
                  });
                } else {
                  fetch(
                    `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?${new URLSearchParams(
                      { id: enlighterVal.id }
                    )}`
                  )
                    .then((r) => r.json())
                    .then((innerBody) => {
                      const val = pdokAdressToZsAddress(
                        innerBody.response.docs[0]
                      );
                      input.value = JSON.stringify(val);
                      currentValue = val;
                      currentValueFlat = {
                        label: currentValue.address.full,
                      };
                      sendMessage({
                        type: 'setMarker',
                        version: 5,
                        name: input.name,
                        value: val.geojson.features[0].geometry,
                        xpathQuery: true,
                      });
                    });
                }
              },
            });

            var sendMessage = function (message) {
              iframe.contentWindow.postMessage(message, '*');
            };

            const pdokAdressToZsAddress = (doc) => {
              const [, lat, lon] = /POINT\((.+)\s(.+)\)/.exec(
                doc.centroide_ll
              ) || [null, '', ''];

              return {
                geojson: {
                  type: 'FeatureCollection',
                  features: [
                    {
                      type: 'Feature',
                      properties: {},
                      geometry: {
                        type: 'Point',
                        coordinates: [Number(lat), Number(lon)],
                      },
                    },
                  ],
                },
                address: { full: doc.weergavenaam },
                bag: {
                  type: 'nummeraanduiding',
                  id: `nummeraanduiding-${doc.nummeraanduiding_id}`,
                },
              };
            };

            var setClosestAddress = function (lat, lon) {
              fetch(
                `https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest?${new URLSearchParams(
                  { lon, lat, q: 'type:adres' }
                )}`
              )
                .then((r) => r.json())
                .then((resp) => {
                  fetch(
                    `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?${new URLSearchParams(
                      { id: resp.response.docs[0].id }
                    )}`
                  )
                    .then((r) => r.json())
                    .then((innerBody) => {
                      const val = pdokAdressToZsAddress(
                        innerBody.response.docs[0]
                      );
                      input.value = JSON.stringify(val);
                      currentValue = val;
                      currentValueFlat = {
                        label: currentValue.address.full,
                      };
                      sendMessage({
                        type: 'setMarker',
                        version: 5,
                        name: input.name,
                        value: val.geojson.features[0].geometry,
                      });
                    });
                });
            };

            smartHttp
              .connect({
                method: 'GET',
                url: '/api/v1/map/ol_settings',
              })
              .success(function (response) {
                var map_center = response.result.instance.map_center;
                var wms_layers = response.result.instance.wms_layers;
                var map_application_url =
                  response.result.instance.map_application_url;
                var map_application = response.result.instance.map_application;

                config = {
                  center: map_center.split(',').map(Number),
                  appUrl:
                    map_application === 'external'
                      ? map_application_url
                      : window.location.origin +
                        '/external-components/index.html?component=map',
                  wmsLayers: wms_layers
                    .filter(function (layer) {
                      return layer.instance.active;
                    })
                    .map(function (layer) {
                      return {
                        url: layer.instance.url,
                        layers: layer.instance.layer_name,
                        label: layer.instance.label,
                        xpath: layer.instance.feature_info_xpath,
                      };
                    }),
                };
                ctrl.initMap();
              });

            ctrl.initMap = function () {
              iframe.src = config.appUrl;
              iframe.style.width = '100%';
              iframe.style.height = '450px';
              iframe.title = input.name;
              iframe.frameBorder = '0';
              iframe.allow = 'fullscreen; geolocation';
              iframe.allowFullscreen = true;
              iframe.addEventListener('load', function () {
                sendMessage({
                  type: 'init',
                  name: input.name,
                  version: 5,
                  value: {
                    initialFeature: null,
                    center: config.center,
                    wmsLayers: config.wmsLayers,
                    canDrawFeatures: false,
                    context: { type: 'WebformFormField', data: null },
                    featureRequestTargetLayer: inputs[1].value,
                  },
                });
                if (currentValue) {
                  sendMessage({
                    type: 'setMarker',
                    version: 5,
                    name: input.name,
                    value: currentValue.geojson.features[0].geometry,
                  });
                }
                window.top.addEventListener('message', ctrl.handleMessage);
              });
              placeholder.innerHtml = '';

              var addressSelect = $compile(
                `<div style="margin: 10px 0 15px 0;" class="spot-enlighter-wrapper"><input name="Adres zoeken" type="text" id="${veldoptieId}" data-ng-model="spotEnlighterAddressValue" data-zs-placeholder="Vul een adres" data-zs-spot-enlighter data-zs-spot-enlighter-restrict="address_v2" data-zs-spot-enlighter-label="label" /></div>`
              )($scope)[0];
              placeholder.appendChild(addressSelect);
              placeholder.appendChild(iframe);
            };

            ctrl.onRemove = function () {
              currentValue = null;
              currentValueFlat = null;
              sendMessage({
                type: 'setMarker',
                version: 5,
                name: input.name,
                value: null,
              });
            };

            ctrl.handleMessage = function (event) {
              if (event.data.type === 'click') {
                setClosestAddress(
                  event.data.value.coordinates[1],
                  event.data.value.coordinates[0]
                );
              } else if (event.data.type === 'getFeatureInfoResolved') {
                const xpathQueryResult = event.data.value;
                const radioInput = document.querySelector(
                  `input[value="${xpathQueryResult}"]`
                );

                if (radioInput && radioInput.offsetParent) {
                  // when the target field is visible
                  radioInput.click();
                } else {
                  // when the target field is invisible (system_attribute)

                  // set the zone_value in the target attribute
                  const fieldName = `kenmerk_id_${inputs[2].value}`;
                  const fieldEl = document.getElementsByName(fieldName)[0];
                  fieldEl.value = xpathQueryResult;

                  zsCaseWebformRuleManager.setValue(
                    fieldEl.getAttribute('data-magic-string'),
                    [xpathQueryResult],
                    [xpathQueryResult]
                  );

                  // set the map_value in the source/addressV2 attribute

                  // this is a fix for some weird behaviour:
                  //   when you set the addressV2 value using the input
                  //   instead of clicking on the map, the value of the
                  //   addressV2 attribute _is_ set, but setting the value
                  //   in the target attribute then causes the value of the
                  //   addressV2 attribute to be lost when navigating to
                  //   the next step.
                  //   resetting the value of the addressV2 attribute
                  //   with the rule engines 'fixes' this loss
                  const fieldElMap = document.querySelector(
                    '[data-zs-case-webform-field-id]'
                  );

                  zsCaseWebformRuleManager.setValue(
                    fieldElMap.getAttribute('data-zs-case-webform-field-name'),
                    [input.value],
                    [input.value]
                  );

                  zsCaseWebformRuleManager.invalidateRules();
                }
              }
            };

            ctrl.link = function (controllers) {
              parentController = controllers[0];
              zsCaseWebformRuleManager = controllers[1];
              parentController.setGetter(function () {
                return input.value;
              });
            };

            ctrl.handleAddressSelect = function (addressObject) {
              addressObject &&
                sendMessage({
                  type: 'setMarker',
                  version: 5,
                  name: input.name,
                  xpathQuery: true,
                  value: {
                    type: 'Point',
                    coordinates: [
                      addressObject.geo_punt.coordinates[0],
                      addressObject.geo_punt.coordinates[1],
                    ],
                  },
                });
            };

            return ctrl;
          },
        ],
        controllerAs: 'zsCaseWebformAddress2Field',
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
