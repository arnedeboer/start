// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.auth').directive('zsPositionPicker', [
    '$parse',
    'organisationalUnitService',
    function ($parse, organisationalUnitService) {
      return {
        templateUrl: '/html/directives/auth/position-picker.html',
        controller: [
          '$scope',
          '$attrs',
          function ($scope, $attrs) {
            var ctrl = this,
              units = [],
              rolesByUnitId = {},
              ready = false;

            function callback() {
              var cb = $attrs.zsPositionPickerChange;
              if (cb) {
                $parse(cb)($scope, {
                  $orgUnitId: ctrl.org_unit_id,
                  $roleId: ctrl.role_id,
                });
              }
            }

            function getOrgUnitId() {
              return $scope.$eval($attrs.zsPositionPickerOrgUnit) || 0;
            }

            function getRoleId() {
              return $scope.$eval($attrs.zsPositionPickerRole) || null;
            }

            function setRoleId() {
              var roleId = ctrl.role_id,
                roles,
                roleIsAvailable = false;

              roles = rolesByUnitId[ctrl.org_unit_id];
              roleIsAvailable = !!_.find(roles, { role_id: roleId });

              if (!roleIsAvailable) {
                if (roles && roles.length) {
                  ctrl.role_id = roles[0].role_id;
                } else {
                  ctrl.role_id = null;
                }
              }
            }

            organisationalUnitService.getUnits().then(function (result) {
              units = result;

              _.each(units, function (unit) {
                rolesByUnitId[unit.org_unit_id] = unit.roles;
              });

              ctrl.org_unit_id = getOrgUnitId();
              ctrl.role_id = getRoleId();

              if (ctrl.role_id === undefined) {
                setRoleId();
              }

              ready = true;
            });

            ctrl.handleOrgUnitChange = function () {
              setRoleId();
              callback();
            };

            ctrl.handleRoleChange = function () {
              callback();
            };

            ctrl.getUnits = function () {
              return units;
            };

            ctrl.getRolesForUnit = function (orgUnitId) {
              return rolesByUnitId[orgUnitId] || [];
            };

            ctrl.getRoleName = function (role) {
              return (!role.system ? '* ' : '') + role.name;
            };

            ctrl.isReady = function () {
              return ready;
            };

            $scope.$watch(function () {
              ctrl.org_unit_id = getOrgUnitId();
              ctrl.role_id = getRoleId();
            });

            return ctrl;
          },
        ],
        controllerAs: 'positionPicker',
      };
    },
  ]);
})();
