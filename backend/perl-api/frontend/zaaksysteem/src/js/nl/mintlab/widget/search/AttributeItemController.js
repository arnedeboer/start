// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.widget.search')
    .controller('nl.mintlab.widget.search.AttributeItemController', [
      '$scope',
      function ($scope) {
        $scope.isOpen = function () {
          return $scope.getSelectedAttribute() === $scope.object;
        };

        $scope.toggleOpen = function () {
          $scope.isOpen()
            ? $scope.setSelectedAttribute(null)
            : $scope.setSelectedAttribute($scope.object);
        };
      },
    ]);
})();
