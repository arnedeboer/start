[% USE Dumper %]


[% # preprocess kenmerk to create a lookup for kenmerk id => { kenmerk data } %]
[% kenmerk_info = {} %]
[% first_kenmerk_id = 0 %]
[% available_kenmerken = milestone.elementen.kenmerken %] 
[% FOREACH mkenmerki IN available_kenmerken.keys.nsort %]
    [% kenmerk_item = available_kenmerken.$mkenmerki %]
    [% IF !kenmerk_item.is_group %]
        [% kenmerk_info.${kenmerk_item.bibliotheek_kenmerken_id} = kenmerk_item %]
        [% IF !first_kenmerk_id %]
            [% first_kenmerk_id = kenmerk_item.bibliotheek_kenmerken_id %]
        [% END %]
    [% END %]
[% END %]

[%
  available_acties_order = [
    'toon_kenmerk',
    'verberg_kenmerk',
    'show_text_block',
    'hide_text_block',
    'show_group',
    'hide_group',
    'pauzeer_aanvraag',
    'vul_waarde_in',
    'vul_waarde_in_met_formule',
    'date_calculations'
    ];
%]

[%
  IF milestone_number != 1;
    available_acties_order.push('trigger_object_action');
    available_acties_order.push('set_value_magic_string');
  END;

  # no_show is so that the return value isn't shown in the template,
  # which for some reason import does.
  # Ugly, but showing ARRAY(XXXX) is even uglier
  no_show = available_acties_order.import(
    [
      'wijzig_registratiedatum',
      'wijzig_afhandeltermijn',
      'sjabloon_genereren',
      'start_case',
      'toewijzing',
      'schedule_mail',
      'add_subject',
      'change_confidentiality'
      'change_html_mail_template'
    ]
  );
%]

[% IF milestone_number == 1 %]
    [% available_acties_order.push('set_online_payment') %]
[% END %]



[% available_acties_order.push('send_external_system_message') %]

[% available_acties = {
        'pauzeer_aanvraag' => 'Pauzeer aanvraag',
        'toon_kenmerk' => 'Toon kenmerk',
        'verberg_kenmerk' => 'Verberg kenmerk',
        'show_text_block' => 'Toon tekstblok',
        'hide_text_block' => 'Verberg tekstblok',
        'vul_waarde_in' => 'Vul waarde in',
        'vul_waarde_in_met_formule' => 'Vul waarde in met formule',
        'set_value_magic_string' => 'Vul waarde in met magic string verwerking',
        'wijzig_registratiedatum' => 'Wijzig registratiedatum',
        'date_calculations' => 'Rekenen met datumkenmerken',
        'wijzig_afhandeltermijn' => 'Wijzig afhandeltermijn',
        'sjabloon_genereren' => 'Genereer sjabloon',
        'start_case' => 'Start zaak',
        'toewijzing' => 'Toewijzing',
        'schedule_mail' => 'E-mail versturen',
        'send_external_system_message' => 'Verstuur extern systeembericht',
        'set_online_payment' => 'Internetkassa aan/uit',
        'hide_group' => 'Verberg kenmerkgroep',
        'show_group' => 'Toon kenmerkgroep',
        'add_subject' => 'Voeg betrokkene toe',
        'change_confidentiality' => 'Wijzig vertrouwelijkheid van de zaak',
        'change_html_mail_template' => 'Wijzig HTML e-mail template',
        'trigger_object_action' => 'Objectactie',
    } 
%]

[% BLOCK action_type;
  type = fieldname _ '_kenmerk';
  IF is_regel_type || params.$type == 'case_result';
    available_acties_order = [ 'sjabloon_genereren', 'start_case', 'schedule_mail', 'send_external_system_message'];
  END;
END %]

[% # contains kenmerken that have been referenced in the voorwaarden, to make sure a kenmerk does not alter itself. %]
[% avoid_kenmerken = {} %] 
[% global.kenmerk_selection = '' %]
[% wijk_kenmerken = {} %]


[% #do it only once, performance %]
[% distinct_parkeergebieden = c.model('DB::Parkeergebied').parkeergebieden %]


[% BLOCK selecteer_milestone_kenmerk %]
[% # show_all_kenmerken: display the full list, otherwise only show kenmerken that can be used as
   # filters.
   # show_value_kenmerken: display the list minus a few types that are impractical for this purpose
%]

    [% allowed_kenmerk_types = { 
        option              => 1, 
        checkbox            => 1, 
        select              => 1, 
        bag_adres           => 1,
        bag_straat_adres    => 1,
       }
    %]

    [% kenmerk_types_for_wijk_display = {
            googlemaps => 1,
            bag_adres => 1,
            bag_openbareruimte => 1,
            bag_straat_adres => 1,
        }
    %]

    Kenmerk:
    [% PROCESS beheer/zaaktypen/milestones/rules/field_select.tt %]
[% END %]

[% BLOCK selecteer_milestone_group %]
[% # show_all_kenmerken: display the full list, otherwise only show kenmerken that can be used as
   # filters.
   # show_value_kenmerken: display the list minus a few types that are impractical for this purpose
%]
    Kenmerkgroep:
    [% PROCESS beheer/zaaktypen/milestones/rules/field_select.tt show_group_fields=1 %]
[% END %]

[% BLOCK selecteer_milestone_text_block %]
    Tekstblok:
    [% PROCESS beheer/zaaktypen/milestones/rules/field_select.tt show_text_blocks=1 %]
[% END %]

<div id="regel_definitie">
<form>
<input type="hidden" name="milestone_number" value="[% milestone_number %]" />
<input type="hidden" name="zaaktype_id" value="[% zaaktype_id %]" />
<input type="hidden" name="zaaktype_update" value="" />
<div class="regel_definitie_wrapper">
<div class="regel_definitie_inner">
<div class="spinner-groot"><div></div></div>
<input type="hidden" name="uniqueidr" value="[% c.req.params.uniqueidr %]"/>
<input type="hidden" name="update" value="1"/>
<input type="hidden" name="rownumber" value="[% rownumber %]"/>
<table class="table_zaakinformatie" border="0" id="regel_template">
    <tbody>
        <tr class="naam_regel">
            <td class="td200">
                <input type="text"
                    class="infinity"
                    value="[% params.naam %]"
                    name="regels_naam"
                    placeholder="Vul hier de naam van de regel in"
                />
            </td>
        </tr>
        
        <tr>
            <td>
                <div class="voeg-regel-toe"><strong>Voorwaarden</strong>
                    <a class="add_voorwaarde button button-secondary button-smaller right" href="#">
                       <i class="icon-font-awesome icon-plus"></i>
                    </a>
                    <span class="right regel-voorwaarden-dropdown">
                    <label for="select_en_of"> Welke voorwaarden moeten voldoen:
                        <select id="select_en_of" name="regels_condition_type">
                            <option value="and"[% params.condition_type == 'and' ? ' selected="selected"' : '' %] >Alle (AND)</option>
                            <option value="or"[% params.condition_type == 'or' ? ' selected="selected"' : '' %] >Enkelen (OR)</option>
                        </select>
                    </label>
                    </span>
                </div>
            </td>
        </tr>
        
        <tr>
            <td>
                <table border="0" width="100%" class="rules_inner_table">
                    [% voorwaarden = params.voorwaarden %]
                    [% IF !voorwaarden %][% voorwaarden = [ '1' ] %][% END %]
                    [% FOREACH voorwaarde_index = voorwaarden %]
                        [% PROCESS beheer/zaaktypen/milestones/rules/condition.tt  
                            fieldname = 'voorwaarde_' _ voorwaarde_index
                        %]
                    [% END %]
        
                    [% IF new_voorwaarde %]
                        [% PROCESS beheer/zaaktypen/milestones/rules/condition.tt  
                            fieldname = 'voorwaarde_' _ new_voorwaarde
                            voorwaarde_index = new_voorwaarde
                        %]
                    [% END %]
        
                </table>
                
            </td>
        </tr>
        <tr>
            <td>
                [% PROCESS action_type %]
                <div class="voeg-regel-toe"><strong>Acties</strong>
                    <a class="add_actie button button-secondary button-smaller right" href="#">
                        <i class="icon-font-awesome icon-plus"></i>
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table class="rules_inner_table">
                    [% acties = params.acties %]
                    [% IF !acties %][% acties = [ '1' ] %][% END %]
                    [% FOREACH actie_index = acties %]
                        [% PROCESS beheer/zaaktypen/milestones/rules/action.tt 
                            fieldname = 'actie_' _ actie_index
                            fieldgroup = 'acties'
                            can_remove_always = new_actie
                        %]
                    [% END %]
                    [% IF new_actie %]
                        [% PROCESS beheer/zaaktypen/milestones/rules/action.tt 
                            fieldname = 'actie_' _ new_actie 
                            actie_index = new_actie
                            fieldgroup = 'acties'
                            can_remove_always = 1
                        %]
                    [% END %]
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <div class="voeg-regel-toe last"><strong>Anders</strong>
                    <a class="add_anders button button-secondary button-smaller right" href="#">
                        <i class="icon-font-awesome icon-plus"></i>
                    </a>
                </div>
            </td>
        </tr>
        
        
        <tr>
            <td>
                <table class="rules_inner_table last">
                    [% anders = params.anders %]
                    [% IF !anders %]
                        [% anders = [] %]
                    [% END %]
                    [% FOREACH actie_index = anders %]
                        [% PROCESS beheer/zaaktypen/milestones/rules/action.tt 
                            fieldname = 'ander_' _ actie_index
                            fieldgroup = 'anders'
                            can_remove_always = 1
                        %]
                    [% END %]
                    [% IF new_anders %]
                        [% PROCESS beheer/zaaktypen/milestones/rules/action.tt 
                            fieldname = 'ander_' _ new_anders 
                            actie_index = new_anders
                            fieldgroup = 'anders'
                            can_remove_always = 1
                        %]
                    [% END %]
                </table>
            </td>
        </tr>
        
    </tbody>
</table>
<div class="form-actions form-actions-bottom">
    <input type="button" value="Opslaan" class="button button-primary save_regel"/>
</div>
</div>
</div>
</form>
</div>
