$(document).ready(function() {
    if(!$('#import_validation').length) return;

    // Capture clicks on the item header, unfold
    // possible options underneath it.
    $('div.import_object_item .import_object_item_header').on('click', function() {
        var me = $(this);

        if(me.hasClass('open')) {
            me.parent('form.save_item_state').trigger('submit');
        }

        me.next('.import_object_options').toggle();
        me.find('input[name="save"]').toggle();
        me.toggleClass('open');


        // Prevent 'old' zaaktype import js from hooking into this event
        return false;
    });

    // Transmorgrify plain input button into input submit button
    $('form.save_item_state input[name="save"]').on('click', function() {
        $(this).closest('form').trigger('submit');

        return false;
    });

    // Capture all submit events for item state forms
    // XHR that stuff to the backend, pretend nothing happened
    $('form.save_item_state').on('submit', function() {
        var me = $(this);

        // If no option is selected, don't try saving this option
        if(me.find('input[name=option]').val().length == 0) {
            return false;
        }

        me.find('.import_object_item_header .spinner-klein').toggle();
        me.find('.import_object_item_header .toggle').toggle();

        $.ajax({
            type: me.attr('method'),
            url: me.attr('action'),
            data: me.serialize(),
            global: false,
            complete: function() { me.find('.import_object_item_header .spinner-klein').toggle(); },
            success: function(data) {
                me.find('div.rfa span').html(data.label);
                me.find('input[name="option"]').prop('value', data.option);
                me.find('.import_object_item_header .toggle').toggle();
                //me.find('.import_object_options').toggle();
            }
        });

        // Prevent non-XHR form post
        return false;
    });

    // Automatically trigger form submit on changes of input fields
    $('form.save_item_state input').on('change', function() {
        var form = $(this).parents('form');

        form.trigger('submit');
    });

    // Handler that sets a hidden input field to be saved to the backend
    // on collapse of the optionslist, sets the header state as well
    $('div.import_object_item_option').on('click', function() {
        var me = $(this);

        var form = me.parents('form.save_item_state');
        var field = form.find('input[name=option]');
        var header = field.next('div.import_object_item_header');

        field.val(me.attr('option-name'));
        form.trigger('submit');

        // Toggle the error_or_not state depending on the content of
        // the selected option
        header.toggleClass(
            'import_item_error',
            me.attr('option-name').length == 0
        );
    });

    $('.ezra_import_library_id select').on('change', function () {
        var $select = $(this);

        $.post('/beheer/object/import/set_library_id', { library_id: $select.val() });
    });

    $('form.save_item_state input[type="checkbox"]').on('click', function(e) {
        /* Oh IE, how I loathe thee */
        if(e.stopPropagation) {
            e.stopPropagation();
        } else {
            e.cancelBubble = true;
        }
    });
});
