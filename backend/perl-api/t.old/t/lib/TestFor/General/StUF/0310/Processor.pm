package TestFor::General::StUF::0310::Processor;
use base 'ZSTest';

use DateTime;
use TestSetup;
use Zaaksysteem::StUF::0310::Processor;

sub test_format_stuf_boolean : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    is($processor->_format_stuf_boolean(1), 'J', 'StUF boolean value for a true value is "J"');
    is($processor->_format_stuf_boolean(0), 'N', 'StUF boolean value for a false value is "N"');
}

sub test_parse_stuf_boolean : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    ok( $processor->_parse_stuf_boolean('J'), 'StUF boolean value "J" is true');
    ok(!$processor->_parse_stuf_boolean('N'), 'StUF boolean value "N" is false');
    throws_ok(
        sub { $processor->_parse_stuf_boolean('X') },
        qr#stufzkn/unknown_boolean#,
        'StUF boolean value "X" does not exist and leads to a exception',
    );
}

sub test_parse_stuf_date : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    my $test_date = DateTime->new(
        year  => 1955,
        month => 11,
        day   => 12,
    );

    is(
        $processor->_parse_stuf_date('19551112'),
        $test_date,
        'Parsing a StUF format date (yyyymmdd) works',
    );

    my $nothing = $processor->_parse_stuf_date();
    is(
        $nothing,
        undef,
        'Parsing an empty StUF date leads to undef',
    );

    throws_ok(
        sub { $processor->_parse_stuf_date('not_a_date') },
        qr#stufzkn/malformed_date#,
        'Malformed date leads to exception',
    );

    throws_ok(
        sub { $processor->_parse_stuf_date('20159999') },
        qr#an integer between 1 and 12#,
        'Differently malformed date leads to exception',
    );
}

sub test_format_stuf_date : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    my $test_date = DateTime->new(
        year  => 1955,
        month => 11,
        day   => 12,
    );

    cmp_deeply(
        $processor->_format_stuf_date($test_date),
        { _ => '19551112' },
        'Date is correctly formatted for StUF reponses',
    );

    cmp_deeply(
        $processor->_format_stuf_date(undef),
        {
            noValue => 'geenWaarde',
            _       => 'NIL',
        },
        'Empty date is correctly formatted for StUF reponses',
    );
}

sub test_parse_stuurgegevens : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    my $stuurgegevens = Test::MockObject->new();
    $stuurgegevens->mock(
        'findvalue' => sub {
            my $self = shift;

            return "findvalue - $_[0]";
        }
    );

    cmp_deeply(
        { $processor->_parse_stuurgegevens($stuurgegevens) },
        {
            sender    => 'findvalue - StUF:zender/StUF:applicatie',
            receiver  => 'findvalue - StUF:ontvanger/StUF:applicatie',
            reference => 'findvalue - StUF:referentienummer',
        },
        'StUF stuurgegevens are parsed correctly',
    );
}

sub _build_mock_medewerker {
    my $self = shift;
    my ($with_email, $with_phonenumber) = @_;

    my $mock = Test::MockObject->new();

    $mock->set_always(btype => 'medewerker');
    for my $key (qw(username geslachtsnaam voornamen voorletters)) {
        $mock->set_always($key => "betrokkene-$key");
    }

    if ($with_email) {
        $mock->set_always(email => 'email@example.com');
    }
    else {
        $mock->set_always(email => undef);
    }

    if ($with_phonenumber) {
        $mock->set_always(telefoonnummer => '0600031337');
    }
    else {
        $mock->set_always(telefoonnummer => undef);
    }

    return $mock;
}

sub _build_mock_natuurlijk_persoon {
    my $self = shift;

    my $mock = Test::MockObject->new();

    $mock->set_always(btype               => 'natuurlijk_persoon');
    $mock->set_always(burgerservicenummer => '987654321');
    $mock->set_always(authenticated       => 0);

    return $mock;
}

sub _build_mock_bedrijf {
    my $self = shift;

    my $mock = Test::MockObject->new();

    $mock->set_always(btype            => 'bedrijf');
    $mock->set_always(vestigingsnummer => '123123123123');
    $mock->set_always(authenticated    => 1);

    return $mock;
}

sub test_format_person : Tests {
    my $self = shift;

    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    my $nothing = $processor->_format_person('TEST_ENTITEIT', undef);
    is(
        $nothing,
        undef,
        'Not specifying leads to an empty return'
    );

    for my $permutation (([0, 0], [1, 0], [0, 1], [1, 1])) {
        my $person = $self->_build_mock_medewerker(@$permutation);

        my $rv = $processor->_format_person('TEST_ENTITEIT', $person);

        cmp_deeply(
            $rv,
            {
                entiteittype => 'TEST_ENTITEIT',
                gerelateerde => {
                    medewerker => {
                        entiteittype  => 'MDW',
                        identificatie => 'betrokkene-username',
                        achternaam    => 'betrokkene-geslachtsnaam',
                        roepnaam      => 'betrokkene-voornamen',
                        voorletters   => 'betrokkene-voorletters',

                        $permutation->[0]
                            ? (emailadres     => 'email@example.com')
                            : (),
                        $permutation->[1]
                            ? (telefoonnummer => '0600031337')
                            : (),
                    },
                },
            },
            sprintf(
                'Medewerker StUF serialization (XML::Compile style) is correct (email: %s; phone: %s)',
                $permutation->[0] ? 'Yes' : 'No',
                $permutation->[1] ? 'Yes' : 'No',
            ),
        );
    }

    my $natuurlijk_persoon = $self->_build_mock_natuurlijk_persoon();
    my $np_rv = $processor->_format_person('TEST_NP', $natuurlijk_persoon);
    cmp_deeply(
        $np_rv,
        {
            entiteittype => 'TEST_NP',
            gerelateerde => {
                natuurlijkPersoon => {
                    entiteittype => 'NPS',
                    'inp.bsn'    => '987654321',
                    authentiek   => 'N',
                },
            },
        },
        'Natuurlijk Persoon StUF serialization (XML::Compile style) is correct'
    );

    my $bedrijf = $self->_build_mock_bedrijf();
    my $bedrijf_rv = $processor->_format_person('TEST_BEDRIJF', $bedrijf);
    cmp_deeply(
        $bedrijf_rv,
        {
            entiteittype => 'TEST_BEDRIJF',
            gerelateerde => {
                vestiging => {
                    entiteittype     => 'VES',
                    vestigingsNummer => '123123123123',
                    authentiek       => 'J',
                },
            },
        },
        'Bedrijf (Vestiging) StUF serialization (XML::Compile style) is correct'
    );
}

sub test_format_document_full : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    my $mock_filestore = Test::MockObject->new();
    $mock_filestore->set_always(content => 'file contents go here');

    my $mock_file = Test::MockObject->new();
    $mock_file->set_always(id => 31337);
    $mock_file->mock(
        'get_column' => sub {
            my $self = shift;
            my $column = shift;

            return 42 if $column eq 'case_id';
            return;
        }
    );
    $mock_file->set_always(date_created => DateTime->new(year => 1955, month => 11, day => 12));
    $mock_file->set_always(name => 'Mock File');
    $mock_file->set_always(extension => '.odt');
    $mock_file->set_always(version => 16777216);
    $mock_file->set_always(created_by => 'Gekke Henkie');
    $mock_file->set_always(filename => 'Geen Idee.odt');
    $mock_file->set_always(filestore_id => $mock_filestore);

    my $mock_metadata;
    $mock_file->set_bound(metadata_id => \$mock_metadata);

    my $doc = $processor->_format_document_full($mock_file);
    cmp_deeply(
        $doc,
        {
            identificatie => { _ => 31337 },
            isRelevantVoor => {
                gerelateerde => {
                    identificatie => "000042",
                },
            },
            creatiedatum => { _ => '19551112' },
            titel => { _ => 'Mock File' },
            formaat => { _ => '.odt' },
            versie => { _ => 16777216 },
            auteur => 'Gekke Henkie',
            taal => { _ => 'NIL', noValue => 'waardeOnbekend' },
            vertrouwelijkAanduiding => { _ => 'NIL', noValue => 'geenWaarde' },

            inhoud => {
                contentType => 'application/vnd.oasis.opendocument.text',
                bestandsnaam => 'Geen Idee.odt',
                _ => 'file contents go here',
            }
        },
        'File object is properly serialized to StUF EDC format (XML::Compile style)',
    );

    my $metadata_origin = 'Inkomend';
    $mock_metadata = Test::MockObject->new();
    $mock_metadata->set_always(trust_level => 'Vertrouwelijk');
    $mock_metadata->set_bound(origin => \$metadata_origin);
    $mock_metadata->set_always(origin_date => DateTime->new(year => 2015, month => 10, day => 21));

    my $doc_meta1 = $processor->_format_document_full($mock_file);
    cmp_deeply(
        $doc_meta1,
        {
            identificatie => { _ => 31337 },
            isRelevantVoor => {
                gerelateerde => {
                    identificatie => "000042",
                },
            },
            creatiedatum => { _ => '19551112' },
            titel => { _ => 'Mock File' },
            formaat => { _ => '.odt' },
            versie => { _ => 16777216 },
            auteur => 'Gekke Henkie',
            taal => { _ => 'NIL', noValue => 'waardeOnbekend' },
            vertrouwelijkAanduiding => { _ => 'VERTROUWELIJK' },

            inhoud => {
                contentType => 'application/vnd.oasis.opendocument.text',
                bestandsnaam => 'Geen Idee.odt',
                _ => 'file contents go here',
            },

            ontvangstdatum => { _ => '20151021' },
        },
        'File object is properly serialized to StUF EDC format (XML::Compile style)',
    );

    $metadata_origin = 'Uitgaand';
    my $doc_meta2 = $processor->_format_document_full($mock_file);
    cmp_deeply(
        $doc_meta2,
        {
            identificatie => { _ => 31337 },
            isRelevantVoor => {
                gerelateerde => {
                    identificatie => "000042",
                },
            },
            creatiedatum => { _ => '19551112' },
            titel => { _ => 'Mock File' },
            formaat => { _ => '.odt' },
            versie => { _ => 16777216 },
            auteur => 'Gekke Henkie',
            taal => { _ => 'NIL', noValue => 'waardeOnbekend' },
            vertrouwelijkAanduiding => { _ => 'VERTROUWELIJK' },

            inhoud => {
                contentType => 'application/vnd.oasis.opendocument.text',
                bestandsnaam => 'Geen Idee.odt',
                _ => 'file contents go here',
            },

            verzenddatum => { _ => '20151021' },
        },
        'File object is properly serialized to StUF EDC format (XML::Compile style)',
    );
}

sub test_format_betrokkenen : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    my $medewerker = $zs->create_medewerker_ok()->subject_id;
    my $aanvrager = {
        'betrokkene' => 'betrokkene-medewerker-' . $medewerker->id,
        'verificatie' => 'medewerker'
    };
    my $case = $zs->create_case_ok(aanvragers => [$aanvrager]);

    my $mw_betrokkene = $zs->betrokkene_model->get_by_string('betrokkene-medewerker-' . $medewerker->id);

    {
        my %rv = $processor->_format_betrokkenen($case);

        cmp_deeply(
            \%rv,
            { heeftAlsInitiator => $processor->_format_person('ZAKBTRINI', $mw_betrokkene) },
            'format_betrokkenen works for case with only an initiator',
        );
    }

    $case->set_behandelaar('betrokkene-medewerker-' . $medewerker->id);
    $case->discard_changes();

    {
        my %rv = $processor->_format_betrokkenen($case);

        cmp_deeply(
            \%rv,
            {
                heeftAlsInitiator => $processor->_format_person('ZAKBTRINI', $mw_betrokkene),
                heeftAlsUitvoerende => $processor->_format_person('ZAKBTRUTV', $mw_betrokkene),
            },
            'format_betrokkenen works for case with initiator + uitvoerende',
        );
    }

    $case->set_coordinator('betrokkene-medewerker-' . $medewerker->id);
    $case->discard_changes();

    {
        my %rv = $processor->_format_betrokkenen($case);

        cmp_deeply(
            \%rv,
            {
                heeftAlsInitiator => $processor->_format_person('ZAKBTRINI', $mw_betrokkene),
                heeftAlsUitvoerende => $processor->_format_person('ZAKBTRUTV', $mw_betrokkene),
                heeftAlsVerantwoordelijke => $processor->_format_person('ZAKBTRVRA', $mw_betrokkene),
            },
            'format_betrokkenen works for case with initiator + uitvoerende + coordinator',
        );
    }

    $case->betrokkene_relateren(
        {
            betrokkene_identifier => 'betrokkene-medewerker-' . $medewerker->id,
            rol                   => 'Gemachtigde',
            magic_string_prefix   => 'gemachtigde',
        },
    );

    $case->betrokkene_relateren(
        {
            betrokkene_identifier => 'betrokkene-medewerker-' . $medewerker->id,
            rol                   => 'Belanghebbende',
            magic_string_prefix   => 'belanghebbende',
        },
    );

    $case->betrokkene_relateren(
        {
            betrokkene_identifier => 'betrokkene-medewerker-' . $medewerker->id,
            rol                   => 'Overig',
            magic_string_prefix   => 'overig',
        },
    );

    {
        my %rv = $processor->_format_betrokkenen($case);

        cmp_deeply(
            \%rv,
            {
                heeftAlsInitiator         => $processor->_format_person('ZAKBTRINI', $mw_betrokkene),
                heeftAlsUitvoerende       => $processor->_format_person('ZAKBTRUTV', $mw_betrokkene),
                heeftAlsVerantwoordelijke => $processor->_format_person('ZAKBTRVRA', $mw_betrokkene),
                heeftAlsGemachtigde       => [$processor->_format_person('ZAKBTRGMC', $mw_betrokkene)],
                heeftAlsBelanghebbende    => [$processor->_format_person('ZAKBTRBLH', $mw_betrokkene)],
                heeftAlsOverigBetrokkene  => [$processor->_format_person('ZAKBTROVR', $mw_betrokkene)],
            },
            'format_betrokkenen works for case with initiator + uitvoerende + coordinator + betrokkenen',
        );
    }

}

sub test_format_documents : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    my $mock_files1 = Test::MockObject->new();
    $mock_files1->set_series( next => (qw()) );

    my $mock_files2 = Test::MockObject->new();
    $mock_files2->set_series( next => (qw(aap noot mies)) );

    no warnings 'redefine';
    local *Zaaksysteem::StUF::0310::Processor::_format_document_minimal = sub {
        my $self = shift;
        return "got $_[0]";
    };

    my %rv = $processor->_format_documents($mock_files1);
    cmp_deeply(
        \%rv,
        {},
        'Calling _format_documents with no files leads to empty return',
    );

    %rv = $processor->_format_documents($mock_files2);
    cmp_deeply(
        \%rv,
        {
            heeftRelevant => [
                {
                    entiteittype => 'ZAKEDC',
                    gerelateerde => 'got aap',
                },
                {
                    entiteittype => 'ZAKEDC',
                    gerelateerde => 'got noot',
                },
                {
                    entiteittype => 'ZAKEDC',
                    gerelateerde => 'got mies',
                },
            ]
        },
        'Calling _format_documents with files leads to proper return.',
    );
}

sub test_format_case : Tests {
    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');

    my $medewerker = $zs->create_medewerker_ok()->subject_id;
    my $aanvrager = {
        'betrokkene' => 'betrokkene-medewerker-' . $medewerker->id,
        'verificatie' => 'medewerker'
    };
    my $case = $zs->create_case_ok(aanvragers => [$aanvrager]);
    my $mw_betrokkene = $zs->betrokkene_model->get_by_string('betrokkene-medewerker-' . $medewerker->id);

    $case->registratiedatum(DateTime->new(year => 1885, month => 9, day => 2));
    $case->streefafhandeldatum(DateTime->new(year => 1985, month => 10, day => 25));
    $case->afhandeldatum(DateTime->new(year => 2015, month => 10, day => 21));
    $case->vernietigingsdatum(undef);
    $case->onderwerp('blablabla');
    $case->update();

    no warnings 'redefine';
    local *Zaaksysteem::StUF::0310::Processor::_format_status = sub {
        my $self = shift;
        return (heeft => "zaakstatus here");
    };

    my $rv = $processor->_format_case($case);
    cmp_deeply(
        $rv,
        {
            identificatie => { _ => '0000' . $case->id },
            einddatum => { _ => '20151021' },
            einddatumGepland => { _ => '19851025' },
            uiterlijkeEinddatum => { _ => '19851025' },
            startdatum => { _ => '18850902' },
            registratiedatum => { _ => '18850902' },
            datumVernietigingDossier => { _ => 'NIL', noValue => 'geenWaarde' },

            omschrijving => { _ => 'blablabla' },
            toelichting  => { _ => $case->zaaktype_node_id->toelichting },

            zaakniveau => { _ => undef, noValue => 'nietOndersteund' },
            deelzakenIndicatie => { _ => 'N' },

            isVan => {
                gerelateerde => {
                    code         => $case->zaaktype_node_id->code,
                    omschrijving => $case->zaaktype_node_id->titel,
                },
            },

            heeft => "zaakstatus here",
            heeftAlsInitiator => $processor->_format_person('ZAKBTRINI', $mw_betrokkene),
        },
        'Case rows are properly serialized into StUF ZKN format',
    );

    my $resultaat
        = $case->zaaktype_node_id->zaaktype_resultaten->search_rs()
        ->first;
    $case->resultaat($resultaat->resultaat);

    $rv = $processor->_format_case($case);
    cmp_deeply(
        $rv,
        {
            identificatie => { _ => '0000' . $case->id },
            einddatum => { _ => '20151021' },
            einddatumGepland => { _ => '19851025' },
            uiterlijkeEinddatum => { _ => '19851025' },
            startdatum => { _ => '18850902' },
            registratiedatum => { _ => '18850902' },
            datumVernietigingDossier => { _ => 'NIL', noValue => 'geenWaarde' },

            omschrijving => { _ => 'blablabla' },
            toelichting  => { _ => $case->zaaktype_node_id->toelichting },

            zaakniveau => { _ => undef, noValue => 'nietOndersteund' },
            deelzakenIndicatie => { _ => 'N' },

            isVan => {
                gerelateerde => {
                    code         => $case->zaaktype_node_id->code,
                    omschrijving => $case->zaaktype_node_id->titel,
                },
            },

            heeft => "zaakstatus here",
            heeftAlsInitiator => $processor->_format_person('ZAKBTRINI', $mw_betrokkene),
            resultaat => {
                omschrijving => { _ => $resultaat->resultaat },
                toelichting  => { _ => $resultaat->label },
            },
        },
        'Case rows are properly serialized into StUF ZKN format with result',
    );

}

sub test_find_case : Tests {
    my $processor = Zaaksysteem::StUF::0310::Processor->new(
        xml_backend      => Test::MockObject->new(),
        record           => Test::MockObject->new(),
        schema           => $zs->schema,
        object_model     => $zs->object_model,
        betrokkene_model => $zs->betrokkene_model,
    );

    my $case = $zs->create_case_ok();

    my $mock_object = Test::MockObject->new();
    $mock_object->set_always(findvalue => '0000' . $case->id);

    no warnings 'redefine';
    local *Zaaksysteem::StUF::0310::Processor::assert_casetype = sub { return; };
    my $found = $processor->_find_case($mock_object);

    isa_ok($found, 'Zaaksysteem::Schema::Zaak', 'Found a case row');
    is($found->id, $case->id, 'Found the correct case');

    throws_ok(
        sub {
            my $mock_object2 = Test::MockObject->new();
            $mock_object2->set_always(findvalue => '1234' . $case->id);
            $processor->_find_case($mock_object2);
        },
        qr#stufzkn/case_not_found#,
        'Unknown case id leads to proper exception',
    );
}

sub test_assert_casetype : Tests {
    no warnings 'redefine';
    local *Zaaksysteem::StUF::0310::Processor::_allowed_casetype_id = sub { 42 };

    my $processor = bless({}, 'Zaaksysteem::StUF::0310::Processor');
    $processor->{schema} = $zs->schema;

    lives_ok(
        sub { $processor->assert_casetype(42) },
        "assert_casetype() doesn't die when given the correct case type id"
    );

    throws_ok(
        sub { $processor->assert_casetype(666) },
        qr#stufzkn/casetype_mismatch#,
        "assert_casetype() dies when given the wrong case type id"
    );
}

sub test_generate_case_id : Tests {
    my %mock_store;

    my $CASE_ID = int(rand(1000));
    my $mock_resultset = Test::MockObject->new();
    $mock_resultset->set_always(generate_case_id => $CASE_ID);

    my $mock_schema = Test::MockObject->new();
    $mock_schema->set_always(resultset => $mock_resultset);

    my $mock_instance = Test::MockObject->new();
    $mock_instance->mock(
        'generate_case_id_return' => sub {
            my $self = shift;
            $mock_store{generate_case_id_return} = { @_ };
            return 'mock-xml';
        }
    );
    my $mock_backend = Test::MockObject->new();
    $mock_backend->set_always(stuf0310 => $mock_instance);

    my $mock_transaction = Test::MockObject->new();
    $mock_transaction->mock(
        update => sub {
            my $self = shift;

            $mock_store{transaction_update} = $_[0];

            return;
        }
    );

    my $mock_record = Test::MockObject->new();
    $mock_record->set_always(transaction_id => $mock_transaction);
    $mock_record->set_always(id => 42);

    my $mock_xpc = Test::MockObject->new();
    $mock_xpc->set_isa('XML::LibXML::XPathContext');

    my $mock_stuurgegevens = Test::MockObject->new();
    $mock_stuurgegevens->mock(
        findvalue => sub {
            my $self = shift;
            my $what = shift;

            if ($what eq 'StUF:referentienummer') {
                return '31337';
            }
            elsif($what eq 'StUF:zender/StUF:applicatie') {
                return "zender"
            }
            elsif($what eq 'StUF:ontvanger/StUF:applicatie') {
                return "ontvanger"
            }
            else {
                die "Unknown argument to findvalue: $what";
            }
        }
    );
    $mock_xpc->mock(
        findnodes => sub {
            my $self = shift;
            my $what = shift;

            if ($what eq 'ZKN:stuurgegevens') {
                return $mock_stuurgegevens;
            }
            else {
                die "Unknown argument to findnodes: $what";
            }
        }
    );

    my $processor = Zaaksysteem::StUF::0310::Processor->new(
        xml_backend      => $mock_backend,
        schema           => $mock_schema,
        record           => $mock_record,
        object_model     => $zs->object_model,
        betrokkene_model => $zs->betrokkene_model,
    );

    no warnings 'redefine';
    local *Zaaksysteem::StUF::0310::Processor::_log_case_id = sub {
        my $self = shift;
        $mock_store{log_case_id} = { generated_id => shift, xpc => shift };
    };

    with_stopped_clock {
        my $xml = $processor->generate_case_id($mock_xpc);
        is($xml, 'mock-xml', 'generate_case_id returns the generated XML');

        is_deeply(
            \%mock_store,
            {
                generate_case_id_return => {
                    writer => {
                        stuurgegevens => {
                            berichtcode => 'Du02',
                            functie     => 'genereerZaakidentificatie',
                            ontvanger   => { applicatie => 'zender' },
                            zender      => { applicatie => 'ontvanger' },
                            crossRefnummer   => 31337,
                            referentienummer => 42,
                            tijdstipBericht  => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),
                        },
                        zaak => {
                            entiteittype => 'ZAK',
                            identificatie => { _ => '0000' . $CASE_ID },
                        },
                    }
                },
                log_case_id => {
                    generated_id => '0000' . $CASE_ID,
                    xpc          => {
                        receiver  => 'ontvanger',
                        sender    => 'zender',
                        reference => 31337,
                    },
                },
                transaction_update => {
                    external_transaction_id => 'case-0000' . $CASE_ID,
                },
            }
        );
    };
}

sub test_generate_bv03 : Tests {
    my %mock_store;
    my $mock_instance = Test::MockObject->new();
    $mock_instance->mock(
        'bv03' => sub {
            my $self = shift;
            $mock_store{bv03} = { @_ };
            return 'mock-xml';
        }
    );

    my $mock_backend = Test::MockObject->new();
    $mock_backend->set_always(stuf0310 => $mock_instance);

    my $processor = Zaaksysteem::StUF::0310::Processor->new(
        xml_backend      => $mock_backend,
        schema           => Test::MockObject->new(),
        record           => Test::MockObject->new(),
        object_model     => Test::MockObject->new(),
        betrokkene_model => Test::MockObject->new(),
    );

    with_stopped_clock {
        my $bv03 = $processor->generate_bv03(
            '31337',
            {
                sender    => 'sender',
                receiver  => 'receiver',
                reference => '42',
            }
        );

        is($bv03, 'mock-xml', 'bv03 returns the correct');
        cmp_deeply(
            $mock_store{bv03},
            {
                writer => {
                    stuurgegevens => {
                        berichtcode      => 'Bv03',
                        zender           => { applicatie => 'receiver' },
                        ontvanger        => { applicatie => 'sender' },
                        referentienummer => 31337,
                        crossRefnummer   => 42,
                        tijdstipBericht  => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),
                    },
                }
            },
            'Bv03 reply looks right',
        );
    };
}

sub test_write_case {
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
