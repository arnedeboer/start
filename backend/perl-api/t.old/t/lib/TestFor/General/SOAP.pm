package TestFor::General::SOAP;
use base qw(ZSTest);

use TestSetup;
use File::Spec::Functions;
use IO::All;
use Data::UUID;

sub zaaksysteem_soap_client : Tests {
    $zs->txn_ok(
        sub {

            my $file = catfile(qw(xential buildservice.wsdl));

            my $client = Zaaksysteem::SOAP->new(
                wsdl_file     => $file,
                home          => catfile(qw(/ vagrant)),
                soap_endpoint => 'https://10.44.0.11',
            );

            isa_ok($client, 'Zaaksysteem::SOAP');

            my $wsdl = $client->wsdl;
            isa_ok($wsdl, "XML::Compile::WSDL11");

            {    # Test if arrayref works like intended.
                my @files = ($file, catfile(qw(xential infoservice.wsdl)));
                my $client = Zaaksysteem::SOAP->new(
                    wsdl_file => \@files,
                    home      => catfile(qw(/ vagrant)),
                );
                isa_ok($client, 'Zaaksysteem::SOAP');

                my $wsdl = $client->wsdl;
                isa_ok($wsdl, "XML::Compile::WSDL11");
            }

            my $transport = $client->transport();
            isa_ok($transport, 'XML::Compile::Transport::SOAPHTTP');

        }
    );
}


sub _slurp {
    my $c = eval { scalar io->catfile(@_)->slurp };
    return $c ? $c : '';
}

sub zaaksysteem_soap_xential : Tests {

    my $baseurl = 'https://zaaksysteem.demo.interactionnext.nl/Factory/';

    $zs->txn_ok(
        sub {

            my $hostname      = "$baseurl/InfoService";
            my %test_response = ();

            if ($ENV{ZS_DEV_XENTIAL}) {
                $hostname = $ENV{ZS_DEV_XENTIAL};
                diag("Running live tests");
            }
            else {
                diag("To run live test, set ZS_DEV_XENTIAL to a hostname");
                my @calls = qw(getVersion getVersionInfo);
                %test_response = map {
                    $_ => _slurp(
                        qw(/ vagrant t data xml xential infoservice),
                        "${_}Response.xml"
                        ),
                } @calls;
            }

            my @files  = (catfile(qw(xential infoservice.wsdl)));
            my $client = Zaaksysteem::SOAP->new(
                wsdl_file       => \@files,
                xml_definitions => [
                    catfile(qw(/ vagrant share xsd xential infoservice.xsd)),
                    catfile(
                        qw(/ vagrant share xsd xential jaxb.dev.java.net.array.xsd)
                    ),
                ],
                home           => catfile(qw(/ vagrant)),
                soap_endpoint  => $hostname,
                test_responses => \%test_response,
            );
            isa_ok($client, 'Zaaksysteem::SOAP');

            my $wsdl = $client->wsdl;
            isa_ok($wsdl, "XML::Compile::WSDL11");

            _test_response(
                $client,
                call   => 'getVersion',
                params => {},
                expect => { return => 'trunk' },
            );

            _test_response(
                $client,
                call   => 'getVersionInfo',
                params => {},
                expect => {
                    versionInfo => {
                        codeVersion => 1111,
                        major       => 1,
                        minor       => 0,
                        revision    => 0
                    },
                },
            );

        },
        "InfoService"
    );

    $zs->txn_ok(
        sub {
            my $hostname      = "$baseurl/BuildService";
            my %test_response = ();

            if ($ENV{ZS_DEV_XENTIAL}) {
                $hostname = $ENV{ZS_DEV_XENTIAL};
                note("Running live tests");
            }
            else {
                note("To run live test, set ZS_DEV_XENTIAL to a hostname");
                my @calls
                    = qw(preparePreregisteredWithOptions startDocument buildDocument);
                %test_response = map {
                    $_ => _slurp(
                        qw(/ vagrant t data xml xential buildservice),
                        "${_}Response.xml"
                        ),
                } @calls;
            }

            my @files  = (catfile(qw(xential buildservice.wsdl)));
            my $client = Zaaksysteem::SOAP->new(
                wsdl_file       => \@files,
                xml_definitions => [
                    catfile(qw(/ vagrant share xsd xential buildservice.xsd)),
                    catfile(
                        qw(/ vagrant share xsd xential jaxb.dev.java.net.array.xsd)
                    ),
                ],
                home           => catfile(qw(/ vagrant)),
                soap_endpoint  => $hostname,
                test_responses => \%test_response,
            );
            isa_ok($client, 'Zaaksysteem::SOAP');

            my $wsdl = $client->wsdl;
            isa_ok($wsdl, "XML::Compile::WSDL11");

            # Flow:
            # prepare_preregistered_with_options
            # start_document
            # build_document

            _test_response(
                $client,
                call   => 'preparePreregisteredWithOptions',
                params => {

                    # Our XML
                    xml => '<foo></foo>',

                    # Template by UUID
                    selectionOption =>
                        { selectionByUuid => $zs->generate_uuid, },

                    # Storage option, NONE is not stored on disk (probably
                    # testing), on other platforms, use Zaaksysteem
                    storageOption => {
                        fixed      => 1,
                        target     => 'NONE',
                        properties => { entry => [] },
                    },

                    # This is where we want to get our answer
                    statusUpdateOption => {
                        events => {
                            data    => 'foo',
                            eventId => 'AFTERBUILD',
                            headers => { entry => [], },
                            method  => 'GET',
                            url     => 'foo',
                        },
                    },
                },
                expect => {
                    preparePreregisteredResult => {
                        startDocumentUrl =>
                            'https://start.document.url.xential.nl',
                        ticketUuid => '8fe02991-f524-4ab4-8578-7d2eecda91b2',
                        xentialRegistrationUuid =>
                            '8fe02991-f524-4ab4-8578-7d2eecda91b2',
                    },
                },
            );

            _test_response(
                $client,
                call   => 'startDocument',
                params => {
                    ticketUuid => $zs->generate_uuid,
                    documentTitle =>
                        Zaaksysteem::TestUtils::generate_random_string(),
                },
                expect => {
                    startDocumentResult => {
                        documentId => 42,
                        resumeDocumentUrl =>
                            'https://resume.document.url.xential.nl',
                        status => 'VALID',
                    },
                },
            );

            _test_response(
                $client,
                call   => 'buildDocument',
                params => {
                    documentId => $zs->generate_uuid,
                    priority   => 'NORMAL',
                },
                expect => {
                    buildResult => { id => 42 },
                },
            );

        },
        "BuildService"
    );

}

sub _test_response {
    my ($client, %options) = @_;

    my ($answer, $trace) = $client->dispatch($options{call}, $options{params});
    my $expect = { parameters => $options{expect} };

    my $ok = is_deeply($answer, $expect,
        "$options{call} returns correct response");

    if (!$ok) {
        local $Data::Dumper::Sortkeys = 1;
        diag explain { got => $answer, want => $expect };
    }

    return $ok;
}

1;

__END__

=head1 NAME

TestFor::General::SOAP - Test ZS::SOAP module

=head1 SEE ALSO

L<Zaaksysteem::SOAP>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
