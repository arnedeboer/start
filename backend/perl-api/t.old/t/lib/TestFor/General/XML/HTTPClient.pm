package TestFor::General::XML::HTTPClient;
use base qw(Test::Class);

use TestSetup;

use Zaaksysteem::XML::HTTPClient;

sub test_xmlhttp_client : Tests {
    my %result;

    my $fake_ua = Test::MockObject->new();
    $fake_ua->set_isa('LWP::UserAgent');
    $fake_ua->mock(
        post => sub {
            my $self = shift;
            my ($url, %headers) = @_;

            push @{ $result{ua_post} }, { url => $url, headers => \%headers };

            my $res = Test::MockObject->new();
            $res->mock('is_success' => sub { 1 });
            $res->mock('decoded_content' => sub { 'decoded' });
            return $res;
        },
    );

    my $fake_instance = Test::MockObject->new();
    $fake_instance->mock(
        'dummy_req' => sub {
            my $self = shift;
            my ($type, @args) = @_;

            push @{ $result{dummy_req} }, { type => $type, args => \@args };

            return "xml";
        },
    );
    $fake_instance->mock(
        'dummy_res' => sub {
            my $self = shift;
            my ($type, @args) = @_;

            push @{ $result{dummy_res} }, { type => $type, args => \@args };

            return "data";
        },
    );

    my $client = Zaaksysteem::XML::HTTPClient->new(
        ua           => $fake_ua,
        xml_instance => $fake_instance,
        call_mapping => { # Bypass the builder
            'dummy' => {
                request  => 'dummy_req',
                response => 'dummy_res',
                url      => 'dummy_url',
            },
        },
    );

    my @rv = $client->call('dummy', "foobar");
    is_deeply(
        \@rv,
        [ 'data', 'decoded' ],
        'Return value of the call is correct',
    );

    is_deeply(
        \%result,
        {
            'dummy_req' => [
                {
                    'args' => ['foobar'],
                    'type' => 'WRITER'
                }
            ],
            'dummy_res' => [
                {
                    'args' => ['decoded'],
                    'type' => 'READER'
                }
            ],
            'ua_post' => [
                {
                    'headers' => {
                        'Content'      => 'xml',
                        'Content_Type' => 'text/xml'
                    },
                    'url' => 'dummy_url'
                }
            ]
        },
        'All intermediate calls were correct'
    );
}

sub test_xmlhttp_client_throw1 : Tests {
    my %result;

    my $fake_ua = Test::MockObject->new();
    $fake_ua->set_isa('LWP::UserAgent');
    $fake_ua->mock(
        post => sub {
            my $self = shift;
            my ($url, %headers) = @_;

            push @{ $result{ua_post} }, { url => $url, headers => \%headers };

            my $res = Test::MockObject->new();
            $res->mock('is_success' => sub { 0 });
            $res->mock('code' => sub { 1337 });
            return $res;
        },
    );

    my $fake_instance = Test::MockObject->new();
    $fake_instance->mock(
        'dummy_req' => sub {
            my $self = shift;
            my ($type, @args) = @_;

            push @{ $result{dummy_req} }, { type => $type, args => \@args };

            return "xml";
        },
    );

    my $client = Zaaksysteem::XML::HTTPClient->new(
        ua           => $fake_ua,
        xml_instance => $fake_instance,
        call_mapping => { # Bypass the builder
            'dummy' => {
                request  => 'dummy_req',
                response => 'dummy_res',
                url      => 'dummy_url',
            },
        },
    );

    throws_ok(
        sub {
            my @rv = $client->call('dummy', "foobar");
        },
        qr/Got a non-success response/,
        'Unsuccessful HTTP response leads to exception',
    );
}

sub test_xmlhttp_client_throw2 : Tests {
    my %result;

    my $fake_ua = Test::MockObject->new();
    $fake_ua->set_isa('LWP::UserAgent');
    $fake_ua->mock(
        post => sub {
            my $self = shift;
            my ($url, %headers) = @_;

            push @{ $result{ua_post} }, { url => $url, headers => \%headers };

            my $res = Test::MockObject->new();
            $res->mock('is_success' => sub { 1 });
            $res->mock('decoded_content' => sub { 'decoded' });
            return $res;
        },
    );

    my $fake_instance = Test::MockObject->new();
    $fake_instance->mock(
        'dummy_req' => sub {
            my $self = shift;
            my ($type, @args) = @_;

            push @{ $result{dummy_req} }, { type => $type, args => \@args };

            return "xml";
        },
    );
    $fake_instance->mock(
        'dummy_res' => sub {
            die "died in fake_instance dummy_res";
        },
    );

    my $client = Zaaksysteem::XML::HTTPClient->new(
        ua           => $fake_ua,
        xml_instance => $fake_instance,
        call_mapping => { # Bypass the builder
            'dummy' => {
                request  => 'dummy_req',
                response => 'dummy_res',
                url      => 'dummy_url',
            },
        },
    );

    throws_ok(
        sub { my @rv = $client->call('dummy', "foobar"); },
        qr/Got invalid XML response from HTTP request.*dummy_res/,
        'Invalid XML responses trigger an exception'
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
