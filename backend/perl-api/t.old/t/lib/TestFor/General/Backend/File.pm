package TestFor::General::Backend::File;
use base qw(ZSTest);

use TestSetup;
use DateTime;

use Encode qw(encode_utf8 decode_utf8);
use File::Basename;
use File::Copy qw(cp);
use File::Spec::Functions;
use File::Temp;

use constant OUTLOOK_DIR => catfile('t', 'inc', 'OutlookTestMails');

sub zs_backend_file_reject : Tests {
    $zs->txn_ok(
        sub {
            my $case = $zs->create_case_ok();
            my $file = $zs->create_file_ok( case => $case );
            my %args = ( rejected => 1 );

            $file->update_properties(\%args);

            ok(!$file->get_column('case_id'), "No case ID found");
            ok(!$file->get_column('date_deleted'), "Not deleted");
            ok(!$file->get_column('accepted'), "Not accepted");


        },
        'ZS-10054: allow rejections',
    );
}

sub zs_backend_file_origin_date : Tests {
    $zs->txn_ok(
        sub {
            my $file = $zs->create_file_ok();

            my $md = $file->metadata;
            is($md, undef, "No metadata is stored");

            # We don't care about timezones.
            my $now = DateTime->now();
            my $dt = DateTime->new(
                time_zone => "America/Aruba",
                hour      => 22,
                minute    => 15,
                month     => $now->month,
                day       => $now->day,
                year      => $now->year,
            )->subtract(years => 7);

            $file->update_properties(
                {
                    subject   => "testsuite",
                    metadata => {
                        origin      => 'Intern',
                        origin_date => $dt,
                    },
                }
            );

            $dt->set_time_zone("Europe/Amsterdam");

            $md = $file->metadata;
            isa_ok($md, "Zaaksysteem::Model::DB::FileMetadata");
            is($md->origin, 'Intern', 'Correct origin');
            is($md->origin_date, $dt->truncate(to => 'day'), "Correct origin date: " . $dt->truncate(to => 'day'));

            $file->update_properties(
                {
                    subject   => "testsuite",
                    metadata => {
                        origin      => 'Intern',
                        origin_date => '2006-12-31',
                    },
                }
            );
            is($file->metadata->origin_date, '2006-12-31T00:00:00', 'Correct origin date: manual');

        },
        "ZS-6365: Verzend- en ontvangstdatum bij document"
    );
}

sub zs_backend_file_document_status : Tests {
    $zs->txn_ok(
        sub {

            my $file = $zs->create_file_ok();
            is($file->document_status, 'original', 'documentstatus is set correctly to the default');

            $file = $zs->create_file_ok(
                db_params => {
                    document_status => 'copy',
                }
            );
            is($file->document_status, 'copy', 'documentstatus is set correctly via file_create');

            foreach my $status (qw(converted copy replaced original)) {
                $file->update_properties(
                    {
                        document_status => $status,
                        subject         => "testsuite"
                    }
                );
                is($file->document_status, $status, "documentstatus is set correctly to: $status");
            }

            throws_ok(
                sub {
                    $zs->create_file_ok(
                        db_params => { document_status => 'meuk', });
                },
                qr/Invalid options given: document_status/,
                "meuk is not a document_status"
            );

        },
        "Document status"
    );
}

sub zs_backend_file_outlook_attachments_duplicate_mails : Tests {
    $zs->zs_transaction_ok(
        sub {

            my $r = outlook_files_ok(
                mail   => 'Tekst met bijlage - PDF.msg',
                expect => 4,
            );

            my $case  = $r->{case};
            my $files = $r->{files};

            my $mail_one = catfile(OUTLOOK_DIR, 'Tekst met bijlage - PDF.msg');
            my $fh = File::Temp->new(UNLINK => 1, SUFFIX => '.msg');
            my $mail_two = $fh->filename;
            close($fh);

            cp($mail_one, $mail_two);
            my $file_two = _create_file($case, $mail_two);
            $files = $zs->schema->resultset('File')->search_rs({ case_id => $case->id });

            # Because we now apply more logic a mail which is send twice
            # results in several files which are again dupes of eachother, if
            # the subject is the same and the case is the same.
            # Perhaps we should do something with a message id as well.
            my $accepted = 0;
            my $not_accepted = 0;
            while (my $f = $files->next) {
                if ($f->accepted) {
                    $accepted++;
                }
                else {
                    $not_accepted++;
                }
            }

            is_deeply(
                { accepted => $accepted, not_accepted => $not_accepted },
                { accepted => 5,         not_accepted => 3 },
                "All files are accepted",
            );

            unlink($mail_two);
        },
        "All files accepted from two different mails with similar files"
    );
}

sub zs_backend_file_outlook_attachments_evil_file_names : Tests {
    $zs->zs_transaction_ok(
        sub {
            outlook_files_ok(
                mail   => 'Test met diakriet.msg'),
                expect => 3,
            );
        },
        "ZS-5925 - Gemailde bestanden met accenten etc werken naar behoren",
    );
    $zs->zs_transaction_ok(
        sub {
            outlook_files_ok(
                mail   => 'ZS-1210.msg',
                expect => 7,
            );
        },
        "ZS-1210 - Gemailde bestanden met haakjes (and more?) komen niet aan in documentintake",
    );
}

sub zs_backend_file_outlook_attachments_html_mail : Tests {
    $zs->zs_transaction_ok(
        sub {
            outlook_files_ok(
                mail   => 'HTML_mail.msg',
                expect => 4,
                todo   => "Bug in Outlook::Email",
            );
        },
        "ZS-4605 Outlook mails met HTML worden niet goed geconverteerd",
    );
}

sub zs_backend_file_outlook_attachments_eml_mail : Tests {
    my $self = shift;
    if (!$ENV{ZS_DEVELOPER_MAIL}) {
        $self->builder->skip(
            "We don't have dummy mails with this scenario yet"
        );
    }
    return;

    $zs->zs_transaction_ok(
        sub {
                outlook_files_ok(
                    mail   => 'nested_eml.msg',
                    expect => 6,
                );
        },
        "ZS-5498: Nested eml in Outlook mails"
    );
}

sub zs_backend_file_outlook_attachments_strange_from : Tests {
    $zs->zs_transaction_ok(
        sub {
            outlook_files_ok(
                mail   => 'Zonder Afzender.msg',
                expect => 3,
            );
        },
        "ZS-7854 - Email zonder afzender geeft foutmelding",
    );
    $zs->zs_transaction_ok(
        sub {
            outlook_files_ok(
                mail   => 'Lange Afzender.msg',
                expect => 3,
            );
        },
        "ZS-7861 - Error bij toevoegen .msg wanneer afzender langer is dan 100 tekens.",
    );
}

sub zs_backend_file_resultset_search_active : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $r = outlook_files_ok(
                mail   => 'Tekst met bijlage - PDF.msg',
                expect => 4,
            );
            my $found = $r->{case}->search_active_files;
            is($found->count, $r->{files}->count, "Active equals files count");
        },
        "Search active files",
    );
}

sub zs_backend_file_to_json : Tests {
    $zs->txn_ok(
        sub {

            with_stopped_clock {
                my $now  = DateTime->now();
                my $subject = $zs->get_subject_ok();
                my $file = $zs->create_file_ok(betrokkene => $subject);

                my $json_data = $file->TO_JSON;

                my $filestore = delete $json_data->{filestore_id};
                isa_ok($filestore, "Zaaksysteem::Model::DB::Filestore");
                my $search_index = delete $json_data->{search_index};
                ok($search_index, "Has a search_index");

                cmp_deeply(
                    $json_data,
                    {
                        aggregation_scope => 'Archiefstuk',
                        search_order      => undef,
                        generator         => undef,
                        accepted          => 0,
                        active_version    => 1,
                        annotation_count  => 0,
                        case_documents    => [],
                        case_id           => undef,
                        created_by        => $subject,
                        creation_reason   => 'FilestoreTest.txt toegevoegd',
                        date_created      => $now . "Z",
                        date_deleted      => undef,
                        date_modified     => $now . "Z",
                        deleted_by        => undef,
                        destroyed         => 0,
                        directory_id      => undef,
                        document_status   => 'original',
                        extension         => '.txt',
                        extension_dotless => 'txt',
                        id                => $file->id,
                        intake_owner      => undef,
                        is_duplicate_name => 0,
                        is_duplicate_of   => undef,
                        metadata_id       => undef,
                        modified_by       => $subject,
                        name              => 'FilestoreTest',
                        object_type       => 'file',
                        pip_thumbnail_url => '/pip/file/thumbnail/file_id/' . $file->id,
                        publish_pip       => 0,
                        publish_website   => 0,
                        queue             => 1,
                        reject_to_queue   => 1,
                        rejection_reason  => undef,
                        root_file_id      => undef,
                        scheduled_jobs_id => undef,
                        search_term       => 'FilestoreTest',
                        subject_id        => undef,
                        searchable_id     => $file->get_column('searchable_id'),
                        thumbnail_url     => '/file/thumbnail/file_id/' . $file->id,
                        version           => 1
                    },
                    "Got the correct JSON"
                );
            }

        },
    );
}

sub _create_file {
    my ($case, $file) = @_;
    my ($filename, undef, $suffix) = fileparse($file, '\.[^\.]*');
    my $create_opts = {
        db_params => {
            case_id    => $case->id,
            created_by => $zs->get_subject_ok,
            accepted   => 1,
        },
        file_path => $file,
        name      => $filename . $suffix,
    };

    return $zs->schema->resultset('File')->file_create($create_opts);
}

sub outlook_files_ok {
    my %args = @_;

    my $mail = catfile(OUTLOOK_DIR, $args{mail});
    my $case = $zs->create_case_ok();

    my $file = _create_file($case, $mail);
    my $files = $zs->schema->resultset('File')->search_rs({ case_id => $case->id });

    my $msg = encode_utf8("Found $args{expect} file in $args{mail}");

    if (defined $args{todo}) {
        TODO : {
            local $TODO = $args{todo};
            is($files->count, $args{expect}, $msg);
        }
    }
    else {
        is($files->count, $args{expect}, $msg);
    }
    return { case => $case, files => $files };

}

sub zs_t_203_file_update_file_t : Tests {

    my $now = DateTime->now;

    $zs->zs_transaction_ok(
        sub {
            my $file     = $zs->create_file_ok;
            my $case     = $zs->create_case_ok;
            my $subject  = $zs->get_subject_ok;
            my $new_name = 'I am Batman.doc.docx';

            # Add some extra properties
            $file->update_properties(
                {
                    subject      => $subject,
                    directory_id => $zs->create_directory_ok,
                    metadata     => { description => 'Yayayayaya' },
                }
            );
            ok $file->metadata->id,  'File has metadata set';
            ok $file->directory->id, 'File has directory set';

            my $directory_id = $file->directory->id;

            my $result = $file->update_file(
                {
                    subject       => $subject,
                    original_name => $new_name,
                    new_file_path => $zs->config->{filestore_test_file_path}
                }
            );

            ok $result, 'Updated file';
            is $result->name . $result->extension, $new_name,
                'Name + extension matches new name';
            is $result->metadata->description, $file->metadata->description,
                'Description copied';
            is $result->directory->id, $directory_id, 'Directory copied';
            ok !$file->directory, 'Cleared directory on old file';
            is $result->filestore->original_name, $new_name,
                'original_name set to new file name';
            isnt $result->filestore->id, $file->filestore->id,
                'Filestore row is a new entry';
            ok($result->date_created >= $now,
                'Date created is equal or greater then now');
            is $result->created_by, $subject, 'Given subject is set in created_by';
            ok(($result->date_modified >= $now),
                'Date modified is equal or greater than now');
            is $result->modified_by, $subject,
                'Given subject is set in modified_by';
            is $result->root_file->id, $file->id, 'File points to the root file';
            is $result->version, 2, 'Version was raised';
            like $result->creation_reason,
                qr/Document.*FilestoreTest.txt.*versie 1.*vervangen met.*I am Batman.doc.docx.*versie 2/,
                'Correct creation reason';
        },
        'update_file'
    );


    $zs->zs_transaction_ok(
        sub {
            my $file = $zs->create_file_ok;
            my $case = $zs->create_case_ok;

            my $result = $file->update_file(
                {
                    subject       => $zs->get_subject_ok,
                    original_name => 'Original Name.jpg',
                    new_file_path => $zs->config->{filestore_test_file_path}
                }
            );
            ok $result, 'Updated file';
            is $result->root_file_id->id, $file->id, 'File points to the root file';
            is $result->version, 2, 'Version was raised';
            isnt $result->filestore->id, $file->filestore->id,
                'Has new filestore entry';

            throws_ok sub {
                $file->update_file(
                    {
                        subject       => $zs->get_subject_ok,
                        original_name => 'Original Name.jpg',
                        new_file_path => $zs->config->{filestore_test_file},
                    }
                );
            }, qr/is at version 2, can't modify/, 'Updating an old file fails';
        },
        'update_file on old version'
    );


    $zs->zs_transaction_ok(
        sub {
            my $file    = $zs->create_file_ok;
            my $case    = $zs->create_case_ok;
            my $subject = $zs->get_subject_ok;

            # Add some extra properties
            $file->update_properties(
                {
                    subject      => $subject,
                    directory_id => $zs->create_directory_ok,
                    metadata     => { description => 'Yayayayaya' },
                }
            );
            ok $file->metadata->id,  'File has metadata set';
            ok $file->directory->id, 'File has directory set';

            my $directory_id = $file->directory->id;

            my $first_update = $file->update_file(
                {
                    subject       => $subject,
                    original_name => 'Clever Girl.docx',
                    new_file_path => $zs->config->{filestore_test_file_path}
                }
            );
            ok $first_update, 'First update done';
            my $second_update = $first_update->update_file(
                {
                    subject       => $subject,
                    original_name => 'Gordon Freeman.jpg',
                    new_file_path => $zs->config->{filestore_test_file_path}
                }
            );
            ok $second_update, 'Second update done';

            $second_update->update({ directory_id => $zs->create_directory_ok });
            ok $second_update->directory, 'Set new directory';

            my $result = $file->make_leading($subject);
            ok $result, 'Made the initial file leading';
            is $result->name, $file->name, 'Name copied';
            is $result->metadata->description, $file->metadata->description,
                'Description copied';
            is $result->directory->id, $second_update->directory->id,
                'Last used directory copied';
            ok !$second_update->discard_changes->directory,
                'Cleared directory on second update (used to be last)';
            is $result->filestore->original_name, $file->name . $file->extension,
                'original_name set to file name';
            isnt $result->filestore->id, $file->filestore->id,
                'Filestore row is a new entry';
            ok($result->date_created >= $now,
                'Date created is equal or greater then now');
            is $result->created_by, $subject, 'Given subject is set in created_by';
            ok($result->date_modified >= $now,
                'Date modified is equal or greater than now');
            is $result->modified_by, $subject,
                'Given subject is set in modified_by';
            is $result->root_file->id, $file->id, 'File points to the root file';
            is $result->version, 4, 'Version was raised';

            TODO: {
                local $TODO = "AUTUMN2015BREAK: Log message incorrect, it is gordon, but message says filestoretest in both cases";

                like($result->creation_reason,
                    qr/Document.*Gordon Freeman.jpg.*versie 4.*Hersteld.*FilestoreTest.txt.*versie 1/, "Creation reason ok");
            };

        },
        'update_file with is_restore (make_leading)'
    );


    $zs->zs_transaction_ok(
        sub {
            my $subject = $zs->get_subject_ok;
            my $case    = $zs->create_case_ok();
            my $file    = $zs->create_file_ok(db_params => { case => $case });
            $file->update_properties(
                {
                    accepted          => 1,
                    subject           => $subject,
                    case_document_ids => [$zs->create_case_document_ok->id],
                }
            );
            my $second_file = $zs->create_file_ok(db_params => { case => $case });
            ok !$second_file->accepted, 'Second file is not accepted';
            ok !$second_file->case_documents->count,
                'Second file has no case documents';

            ok $file->update_existing(
                {
                    subject          => $subject,
                    existing_file_id => $second_file->id,
                }
                ),
                'Replaced file';

            $second_file->discard_changes;
            is $second_file->root_file->id, $file->id, 'Files are now related';
            is $file->name, $second_file->name, 'Names match';
            ok $second_file->accepted, 'File got accepted';
            ok $second_file->case_documents->count, 'Case document copied';
        },
        'update_existing should not rename'
    );


    $zs->zs_transaction_ok(
        sub {
            my $subject   = $zs->get_subject_ok;
            my $case      = $zs->create_case_ok;
            my $file      = $zs->create_file_ok(db_params => { case => $case });
            my $directory = $zs->create_directory_ok;
            $file->update_properties(
                {
                    accepted          => 1,
                    subject           => $subject,
                    directory_id      => $directory->id,
                    case_document_ids => [$zs->create_case_document_ok->id],
                }
            );
            my $second_file = $zs->create_file_ok(db_params => { case => $case });
            ok !$second_file->accepted, 'Second file is not accepted';

            ok(
                $file->update_existing(
                    { subject => $subject, existing_file_id => $second_file->id, }
                ),
                'Replaced file'
            );
            $second_file->discard_changes;
            ok !$file->directory_id, 'Old file directory removed';
            is $second_file->directory->id, $directory->id,
                'Second file has correct directory set';
        },
        'update_existing should set deleted on old'
    );


    $zs->zs_transaction_ok(
        sub {
            my $subject = $zs->get_subject_ok;
            my $case    = $zs->create_case_ok;
            my $file    = $zs->create_file_ok(db_params => { case => $case });

            ok $file->update_properties(
                {
                    subject         => $subject,
                    publish_pip     => 1,
                    publish_website => 1,
                }
                ),
                'Set PIP publish true, website publish true';

            my $second_file = $zs->create_file_ok(db_params => { case => $case });
            ok !$second_file->publish_pip,     'PIP publish is false';
            ok !$second_file->publish_website, 'Website publish is false';

            ok $file->update_existing(
                {
                    subject          => $subject,
                    existing_file_id => $second_file->id,
                }
                ),
                'Replaced file';
            $second_file->discard_changes;
            ok $second_file->publish_pip,     'PIP publish is true';
            ok $second_file->publish_website, 'Website publish is true';

            # ZS-5992
            $second_file->update({publish_pip => 0, publish_website => 0});
            my $third_file = $zs->create_file_ok(db_params => { case => $case });
            $second_file->update({publish_pip => 1, publish_website => 1});
            $third_file = $second_file->update_existing({
                subject          => $subject,
                existing_file_id => $third_file->id,
            });
            ok($third_file->publish_pip, "Publish PIP is true on update_existing");
            ok($third_file->publish_website, "Publish website is true on update_existing");

        },
        'update_existing should copy publish values'
    );

    $zs->zs_transaction_ok(
        sub {
            my $subject = $zs->get_subject_ok;

            my $bk   = $zs->create_bibliotheek_kenmerk_ok(value_type => 'file');
            my $node = $zs->create_zaaktype_node_ok();
            my $zt   = $zs->create_zaaktype_ok(node => $node);

            my $max = 3;
            for (1 .. $max) {
                $zs->create_zaaktype_kenmerk_ok(
                    bibliotheek_kenmerk => $bk,
                    node                => $node,
                );
            }
            my $case = $zs->create_case_ok(zaaktype => $zt,);
            my $file = $zs->create_file_ok(db_params => { case => $case });

            my $case_documents
                = $zs->schema->resultset('ZaaktypeKenmerken')->search(
                {
                    value_type       => 'file',
                    zaaktype_node_id => $file->case->zaaktype_node_id->id,
                },
                { join => 'bibliotheek_kenmerken_id' }
                );
            my @case_document_ids = map { $_->id } $case_documents->all;

            $file->update_properties(
                {
                    accepted          => 1,
                    subject           => $subject,
                    case_document_ids => \@case_document_ids,
                }
            );
            is $file->case_documents, 3, 'Got three case documents';
        },
        'update with multiple case documents'
    );

    $zs->zs_transaction_ok(
        sub {
            my $subject = $zs->get_subject_ok;

            my $bk   = $zs->create_bibliotheek_kenmerk_ok(value_type => 'file');
            my $node = $zs->create_zaaktype_node_ok();
            my $zt   = $zs->create_zaaktype_ok(node => $node);

            my $max = 3;
            for (1 .. $max) {
                $zs->create_zaaktype_kenmerk_ok(
                    bibliotheek_kenmerk => $bk,
                    node                => $node,
                );
            }
            my $case = $zs->create_case_ok(zaaktype => $zt,);
            my $file = $zs->create_file_ok(db_params => { case => $case });

            my $case_documents
                = $zs->schema->resultset('ZaaktypeKenmerken')->search(
                {
                    value_type       => 'file',
                    zaaktype_node_id => $file->case->zaaktype_node_id->id,
                },
                { join => 'bibliotheek_kenmerken_id' }
                );
            my @case_document_ids = map { $_->id } $case_documents->all;

            $file->update_properties(
                {
                    subject           => $subject,
                    case_document_ids => \@case_document_ids,
                }
            );
            is $file->case_documents, 3, 'Got three case documents';

            my $second_file = $zs->create_file_ok(db_params => { case => $case });

            ok $file->update_existing(
                {
                    subject          => $subject,
                    existing_file_id => $second_file->id,
                }
                ),
                'Replaced file';

            # Refresh results
            $file->discard_changes;
            $second_file->discard_changes;

            ok !$file->case_documents->count,
                'No case documents found on original document';
            is $second_file->case_documents->count, 3,
                'Got three case documents on second file';

        },
        'update_existing with multiple case documents'
    );

    $zs->zs_transaction_ok(
        sub {
            my $subject = $zs->get_subject_ok;
            my $case    = $zs->create_case_ok;
            my $file    = $zs->create_file_ok(db_params => { case => $case });

            my $case_docs = $zs->create_case_document_ok(case => $case)->id;
            my $result = $file->update_properties(
                {
                    subject           => $subject,
                    case_document_ids => [ $case_docs, $case_docs ],
                }
            );

            is $result->case_documents->count, 1, 'Found one case document';

        },
        'update_existing with multiple case documents with duplicate ids yields 1 result'
    );

    $zs->zs_transaction_ok(
        sub {
            my $subject   = $zs->get_subject_ok;
            my $case      = $zs->create_case_ok;
            my $file      = $zs->create_file_ok(db_params => { case => $case });
            my $directory = $zs->create_directory_ok(case => $case);
            $file->update_properties(
                {
                    accepted     => 1,
                    subject      => $subject,
                    directory_id => $directory->id,
                    case_document_ids =>
                        [$zs->create_case_document_ok(case => $case)->id],
                }
            );
            my $second_file = $zs->create_file_ok(db_params => { case => $case });
            ok !$second_file->accepted, 'Second file is not accepted....';

            ok $file->update_existing(
                {
                    subject          => $subject,
                    existing_file_id => $second_file->id,
                }
                ),
                'Replaced file';
            $second_file->discard_changes;

            throws_ok(
                sub {
                    $second_file->update_properties(
                        {
                            'accepted'         => 0,
                            'rejection_reason' => 'foo',
                            'subject'          => $subject,
                        }
                    );
                },
                qr/File is already accepted/,
                'Reject after file is accepted'
            );

            $second_file->discard_changes;

            is($second_file->root_file_id->id,
                $file->id, 'Has root file id of first file');

            ### Bug symptons:
            ok(!$second_file->date_deleted, 'Second file is alive');
            ok($second_file->accepted,      'Second file is accepted');

        },
        'ZS-1863: Prevent leaving inconsistent state after accepting and unaccepting same file'
    );
}

sub _set_new_behandelaar {
    my ($case) = @_;

    my $subject = $zs->create_medewerker_ok()->subject_id;
    my $bid = 'betrokkene-medewerker-'. $subject->id;

    $case->set_behandelaar($bid);
    return $bid;
}

sub zs_11285 : Tests {
    $zs->txn_ok(
        sub {

            my $case    = $zs->create_case_ok();
            my $kenmerk = $zs->create_case_document_ok(case => $case);

            my $bid = _set_new_behandelaar($case);

            my $sjabloon = $zs->create_sjabloon_ok();

            my %file_create_opts = (
                name          => 'foo',
                case          => $case,
                subject       => $bid,
                target_format => 'odt',
                case_document_ids => $kenmerk,
            );

            my $files_on_db = $zs->schema->resultset('File')->search_rs->all;

            my $file = $sjabloon->file_create(\%file_create_opts);

            my $new_files = $zs->schema->resultset('File')->search_rs->all;

            #diag explain { old => $files_on_db, new => $new_files };
            #my $file = $zs->create_file_ok(
            #    db_params         => { case => $case, accepted => 1 },
            #    case_document_ids => [$kenmerk->id],
            #    subject => $bid,
            #);

            my $fh = File::Temp->new();
            print $fh 'foo', $/;
            close($fh);

            $bid = _set_new_behandelaar($case);
            my $updated_file = $file->update_file({
                subject       => $bid,
                original_name => $file->filename,
                new_file_path => "" . $fh,
            });

            is(
                $updated_file->get_column('root_file_id'), $file->id,
                "Has the correct root file id: " . $file->id
            );

            my @files = $case->files->document_list->all;
            is(@files, 1, "Only one case document");

        },
        "ZS-11285: update_file with new content should up version and do correct things"
    );
}

sub copy_file_as_preview : Tests {

    $zs->txn_ok(
        sub {

            my $file = $zs->create_file_ok();
            my $copy = $file->create_copy(
                type => 'pdf',
            );

            isa_ok($copy, 'Zaaksysteem::Model::DB::Filestore');
            $file->add_preview($copy, 'pdf');

            my $pdf_preview = $file->get_preview_pdf;

            isa_ok($pdf_preview, 'Zaaksysteem::Model::DB::Filestore');
            is($pdf_preview->id, $copy->id, "Generated correct preview file");

            is($pdf_preview->name, $copy->name, "preview has same name");
            like($pdf_preview->name, qr/^preview_/, "Preview name");

            is($pdf_preview->extension, '.pdf', "Is PDF copy");

            {
                my $copy = $file->create_copy(
                    type    => 'pdf',
                    preview => 0,
                );

                is($copy->name, $file->filestore_id->name, "Copy name is the same");
            }

            my $employee = $zs->create_medewerker_ok()->subject_id;
            {
                my $copy = $file->save_copy(subject => $employee);
                isa_ok($copy, 'Zaaksysteem::Model::DB::File');
                isnt($copy->id, $file->id, "Different ID");

                my @logs = $zs->schema->resultset('Logging')->search({component_id => $copy->id})->all;
                is(@logs, 1, "One log entry found for copy to pdf action");

                my ($log) = @logs;

                is($log->event_type, 'case/document/copy', 'log event type');
                isa_ok($log->data, "HASH", "has event_data in JSON");

                is($log->onderwerp, "Document 'FilestoreTest.txt' (versie 1) gekopieerd naar document 'FilestoreTest.pdf' (versie 1)", "Onderwerp is correct");

            }

            {
                no warnings qw(redefine once);
                local *Zaaksysteem::Backend::File::Component::may_preview_as_pdf = sub { return 0 };
                local *Zaaksysteem::Backend::File::Component::may_copy_to_pdf    = sub { return 0 };
                throws_ok(
                    sub {
                        $file->save_copy(subject => $employee);
                    },
                    qr#documents/copy/pdf: Not allowed to copy documents to PDF#,
                    "Not allowed to copy files to PDF"
                );
            }

        },
        "PDF epic"
    );
}

sub set_search_terms : Tests {
    $zs->txn_ok(
        sub {
            my $file = $zs->create_file_ok();

            my @items = $zs->schema->resultset('Queue')->search_rs({type => 'set_search_terms'})->all;
            is(@items, 1, "One queue item for search terms");

            $file->update();
            @items = $zs->schema->resultset('Queue')->search_rs({type => 'set_search_terms'})->all;
            is(@items, 2, "Two queue items for search terms");

            is($file->search_index, undef, "No search_index");
            $file->set_search_terms();
            isnt($file->search_index, undef, "Search_index");

        },
        'Set search terms for file'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
