#!/bin/sh

if [ -x /tmp/generate_dev_certs.sh ]; then
    /tmp/generate_dev_certs.sh
fi

#
# USAGE: ZAAKSYSTEEM_API_HOST=zaysteem-api.default.svc.cluster.local ./entrypoint.sh
#

export DNS_RESOLVER="$(grep nameserver /etc/resolv.conf| head -n 1| awk '{ print $2; }')";

if [ -n "${DEVELOPMENT_MODE}" ]; then
    cp /etc/nginx/servers.conf.dev /etc/nginx/servers.conf
else
    # nginx include wants something.. empty file for prod
    touch /etc/nginx/servers.conf
fi

if [ -z "${USE_HTTP}" ]; then
    [ -n "${DEVELOPMENT_MODE}" ] && echo "Running in fastcgi mode"
    SOURCE_FILE=/etc/nginx/conf.d/zaaksysteem.conf.fastcgi.tpl
else
    [ -n "${DEVELOPMENT_MODE}" ] && echo "Running in http mode"
    SOURCE_FILE=/etc/nginx/conf.d/zaaksysteem.conf.http.tpl
fi

sed \
    -e "s/ZAAKSYSTEEM_HOST_API/${ZAAKSYSTEEM_HOST_API:-backend}/g" \
    -e "s/ZAAKSYSTEEM_HOST_CSV/${ZAAKSYSTEEM_HOST_CSV:-api2csv}/g" \
    -e "s/ZAAKSYSTEEM_HOST_SWAGGER/${ZAAKSYSTEEM_HOST_SWAGGER:-swaggerui}/g" \
    -e "s/ZAAKSYSTEEM_HOST_REDOC/${ZAAKSYSTEEM_HOST_REDOC:-redoc}/g" \
    -e "s/ZAAKSYSTEEM_HOST_BBV/${ZAAKSYSTEEM_HOST_BBV:-bbvproxy}/g" \
    -e "s/ZAAKSYSTEEM_HOST_FRONTEND_MONO/${ZAAKSYSTEEM_HOST_FRONTEND_MONO:-frontend-mono}/g" \
    -e "s/ZAAKSYSTEEM_HOST_V2_ADMIN/${ZAAKSYSTEEM_HOST_V2_ADMIN:-httpd-admin}/g" \
    -e "s/ZAAKSYSTEEM_HOST_V2_CASE_MANAGEMENT/${ZAAKSYSTEEM_HOST_V2_CASE_MANAGEMENT:-httpd-case-management}/g" \
    -e "s/ZAAKSYSTEEM_HOST_V2_DOCUMENT/${ZAAKSYSTEEM_HOST_V2_DOCUMENT:-httpd-document}/g" \
    -e "s/ZAAKSYSTEEM_HOST_V2_COMMUNICATION/${ZAAKSYSTEEM_HOST_V2_COMMUNICATION:-httpd-communication}/g" \
    -e "s/ZAAKSYSTEEM_HOST_V2_GEO/${ZAAKSYSTEEM_HOST_V2_GEO:-http-geo}/g" \
    -e "s/DNS_RESOLVER/${DNS_RESOLVER}/g" \
    -e "s/CONNECTION_PER_INSTANCE_LIMIT/${CONNECTION_PER_INSTANCE_LIMIT:-10}/g" \
        < "$SOURCE_FILE" > /etc/nginx/conf.d/zaaksysteem.conf

exec "$@"
