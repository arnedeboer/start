rewrite ^/tpl/zaak_v1/nl_NL/css/.*?common.css$ /page/minified/common/css/common.css last;
rewrite ^/html/(.*)$                           /html/nl/$1                          last;

### Backend
proxy_set_header X-Forwarded-For $remote_addr;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;

# Allow client bodies ("file uploads") to be slightly over 2GB (so a 2GB file
# and some metadata can be uploaded at once)
client_max_body_size        2050M;

# Some generous buffers for request headers
large_client_header_buffers 4 512k;

resolver DNS_RESOLVER;

# { START Security-related headers
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy

    add_header X-Frame-Options        sameorigin  always;
    add_header X-Content-Type-Options nosniff     always;
    add_header Referrer-Policy        same-origin always;
# } STOP Security-related headers

# { START backend

    location / {
        include fastcgi_params;
        fastcgi_param SCRIPT_NAME /;
        fastcgi_param HTTPS $fe_https;

        fastcgi_connect_timeout 60;
        fastcgi_send_timeout 3600;
        fastcgi_read_timeout 3600;
        fastcgi_buffer_size 128k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;
        fastcgi_intercept_errors on;

        ### Limit connections
        limit_conn_log_level notice;
        limit_conn_status 429;
        limit_conn perserver CONNECTION_PER_INSTANCE_LIMIT;

        set $proxyhost ZAAKSYSTEEM_HOST_API;
        fastcgi_pass $proxyhost:9083;
    }

# } END backend

# { START apps

    # API2CSV NodeJS App
    location /app/api2csv {
        set $proxyhost ZAAKSYSTEEM_HOST_CSV;
        proxy_pass http://$proxyhost:1030;
    }

    rewrite ^/apidocs$ /apidocs/ permanent;
    location /apidocs/ {
        set $proxyhost ZAAKSYSTEEM_HOST_SWAGGER;
        proxy_pass http://$proxyhost:8080;
    }

    rewrite ^/redoc$ /redoc/ permanent;
    location ~ /redoc/?(.*) {
        set $proxyhost ZAAKSYSTEEM_HOST_REDOC;
        proxy_pass http://$proxyhost:80/$1;
    }

    location /api/v2/admin {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_ADMIN;
        proxy_pass http://$proxyhost:9084;
    }

    location /api/v2/cm {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_CASE_MANAGEMENT;
        proxy_pass http://$proxyhost:9085;
    }

    location /api/v2/document {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_DOCUMENT;
        proxy_request_buffering off;
        client_body_temp_path      /tmp/;
        client_body_in_file_only   on;
        client_body_buffer_size    1M;
        client_max_body_size       7G;
        proxy_pass http://$proxyhost:9086;
    }

    location /api/v2/file {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_DOCUMENT;
        proxy_request_buffering off;
        client_body_temp_path      /tmp/;
        client_body_in_file_only   on;
        client_body_buffer_size    1M;
        client_max_body_size       7G;
        proxy_pass http://$proxyhost:9086;
    }

    location /api/v2/communication {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_COMMUNICATION;
        proxy_pass http://$proxyhost:9087;
    }

    location /api/v2/geo {
        set $proxyhost ZAAKSYSTEEM_HOST_V2_GEO;
        proxy_pass http://$proxyhost:9084;
    }

    location /doc/api {
        alias /opt/zaaksysteem/apidocs/;
    }

    location ~ ^/(main|admin|my-pip|external-components|meeting|intern|mor|pdc|vergadering) {
        set $proxyhost ZAAKSYSTEEM_HOST_FRONTEND_MONO;

        ## Make sure when the apps-nginx sends a redirect, we translate
        ## http://host:80 to https://host
        proxy_redirect http://$host:82 $scheme://$host;

        # Allow WebSocket connections
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";

        proxy_pass http://$proxyhost:82;
    }

# } END apps

# { START internal locations

    # Exclude static files from running through the backend.
    location ~ ^/(assets|css|data\/|error_pages|examples|favicon\.ico|flexpaper|fonts|html|images|js|offline|partials|pdf\.js-with-viewer|robots.txt|tpl|webodf) {
        root /opt/zaaksysteem/root;
    }

# } STOP internal locations
