// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import definitions from './holiday.definitions';
import { generateHolidays } from './holiday.library';

const KILL_SWITCH = 10000;

export const getDateString = (date) =>
  [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-');

const calculateWorkDays = (
  baseDate,
  workDays,
  workDaysAction,
  country,
  institutionType
) => {
  const definition = definitions[country];
  const holidays = generateHolidays(
    baseDate,
    workDays,
    workDaysAction,
    definition,
    institutionType
  );
  const weekend = definition.weekend;

  let i = 0;
  let daysToAdd = 0;

  while (daysToAdd < KILL_SWITCH && i < workDays) {
    if (workDaysAction === 'add') {
      daysToAdd++;
    } else {
      daysToAdd--;
    }

    let newDate = new Date(baseDate);
    newDate.setDate(baseDate.getDate() + daysToAdd);

    const weekDay = newDate.getDay();
    const humanWeekDay = weekDay === 0 ? 7 : weekDay;
    const isWeekendDay = weekend.includes(humanWeekDay);

    if (isWeekendDay) {
      continue;
    } else {
      const isHoliday = holidays.includes(getDateString(newDate));

      if (isHoliday) {
        continue;
      } else {
        i++;
      }
    }
  }

  const date = new Date(baseDate);

  date.setDate(baseDate.getDate() + daysToAdd);

  return date;
};

const calculateDays = (baseDate, math) => {
  const [days, weeks, months, years] = ['days', 'weeks', 'months', 'years'].map(
    (type) => {
      const operationType = math[`${type}_action`];
      const amount = math[`${type}_amount`];

      return operationType === 'add' ? Number(amount) : Number(amount) * -1;
    }
  );

  const date = new Date(baseDate);

  date.setYear(baseDate.getFullYear() + years);
  date.setMonth(baseDate.getMonth() + months);
  date.setDate(baseDate.getDate() + days + weeks * 7);

  return date;
};

export default (baseValue, math, country, institutionType) => {
  let baseDate;

  if (
    typeof baseValue === 'string' &&
    baseValue.length >= 8 &&
    baseValue.length <= 10
  ) {
    // external registration form (1-1-2001 / 31-12-2001)
    const [day, month, year] = baseValue.split('-');

    baseDate = new Date(year, month - 1, day);
  } else {
    // internal registration form
    // case view
    baseDate = new Date(baseValue);
  }

  const workDays = math.working_days_amount;
  const workDaysAction = math.working_days_action;

  let newDate;

  if (!workDays || workDays === '0') {
    newDate = calculateDays(baseDate, math);
  } else {
    newDate = calculateWorkDays(
      baseDate,
      workDays,
      workDaysAction,
      country,
      institutionType
    );
  }

  return newDate.toISOString();
};
