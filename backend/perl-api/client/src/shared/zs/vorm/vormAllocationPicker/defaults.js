// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default {
  me: {
    data: {
      changeDept: true,
    },
  },
  coworker: {
    data: {
      changeDept: true,
      informAssignee: true,
    },
  },
};
