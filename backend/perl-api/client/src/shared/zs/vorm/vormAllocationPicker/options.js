// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default [
  {
    name: 'me',
    label: 'Zelf in behandeling nemen',
  },
  {
    name: 'coworker',
    label: 'Specifieke behandelaar',
  },
  {
    name: 'org-unit',
    label: 'Rol of afdeling',
  },
];
