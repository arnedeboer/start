// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import once from 'lodash/once';

export default angular.module('zsDatePicker', []).directive('zsDatePicker', [
  '$q',
  'dateFilter',
  ($q, dateFilter) => {
    let load = once(() => {
      return $q((resolve) => {
        require(['pikaday', './getDatepickerOptions', './styles.scss'], (
          ...rest
        ) => {
          resolve(rest);
        });
      });
    });

    return {
      restrict: 'A',
      require: 'ngModel',
      link: (scope, element, attrs, ngModel) => {
        const dateExpression = /^\d{1,2}-\d{1,2}-\d{4}$/;
        const modelDateFormat = 'yyyy-MM-dd';
        const viewDateFormat = 'dd-MM-yyyy';
        let oldValue = '';
        let picker;

        const setDatePicker = (value) => {
          let val = value || null;

          if (
            picker &&
            dateFilter(picker.getDate(), viewDateFormat) !==
              dateFilter(val, viewDateFormat)
          ) {
            picker.setDate(val, true);
          }
        };

        const parser = (val) => {
          let value = val;

          if (val && dateExpression.test(val)) {
            const [day, month, year] = val.split('-');

            value = new Date(year, month - 1, day);
            value = dateFilter(value, modelDateFormat);

            // * only call setDatePicker on succesful dateExpression test
            //   to keep backspace working om the input field in IE
            // * guard against infinite loop
            if (value !== oldValue) {
              oldValue = value;
              setDatePicker(value);
            }
          }

          return value;
        };

        const formatter = (val) => {
          return val ? dateFilter(val, viewDateFormat) : val;
        };

        ngModel.$parsers.unshift(parser);
        ngModel.$formatters.unshift(formatter);

        load().then((libs) => {
          const [Pikaday, getDatePickerOptions] = libs;
          const options = getDatePickerOptions(element[0]);

          picker = new Pikaday(options);

          if (ngModel.$modelValue) {
            setDatePicker(ngModel.$modelValue);
          }
        });
      },
    };
  },
]).name;
