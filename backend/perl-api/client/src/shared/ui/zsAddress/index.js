// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import resourceModule from './../../api/resource';
import zsSuggestionListModule from './../zsSuggestionList';
import zsClickOutsideModule from './../zsClickOutside';
import sessionServiceModule from './../../user/sessionService';
import composedReducerModule from './../../api/resource/composedReducer';
import template from './template.html';
import get from 'lodash/get';
import './styles.scss';

const tryFunction = (x = '') => (typeof x === 'function' ? x() : x);

const pdokAddressToZsAddress = (doc) => {
  const [, lat, lon] = /POINT\((.+)\s(.+)\)/.exec(doc.centroide_ll) || [
    null,
    '',
    '',
  ];

  return {
    geojson: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Point',
            coordinates: [Number(lat), Number(lon)],
          },
        },
      ],
    },
    address: { full: doc.weergavenaam },
    bag: {
      type: 'nummeraanduiding',
      id: `nummeraanduiding-${doc.nummeraanduiding_id}`,
    },
  };
};

const pdokSuggestionToZsSuggestion = (doc) => {
  return {
    value: doc.id,
    label: doc.weergavenaam,
    id: doc.id,
  };
};

export default angular
  .module('zsAddress', [
    resourceModule,
    zsSuggestionListModule,
    zsClickOutsideModule,
    sessionServiceModule,
    composedReducerModule,
  ])
  .directive('zsAddress', [
    'resource',
    'composedReducer',
    'sessionService',
    (resource, composedReducer, sessionService) => {
      return {
        restrict: 'E',
        template,
        scope: {
          mapName: '&',
          mapSettings: '&',
          mapHeight: '&',
          addressValue: '&',
          handleChange: '&',
          context: '&',
          targetLayer: '&',
          onFeaturesSelect: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            let ctrl = this;
            let inputDelegate = {
              input: $element.find('input').eq(0),
            };
            let isOpen = false;
            let iframe = document.createElement('iframe');
            let shouldOpen = true;
            let sessionResource = sessionService.createResource($scope);

            const placeholderReducer = composedReducer(
              { scope: $scope },
              sessionResource
            ).reduce((session) => {
              return session
                ? `Zoek op adres (bv. ${get(
                    session,
                    'instance.account.instance.place'
                  )}, ${get(session, 'instance.account.instance.address')})`
                : '';
            });

            const mapName = ctrl.mapName();
            const mapSettings = ctrl.mapSettings();
            const addressValue = ctrl.addressValue();
            const handleChange = ctrl.handleChange();
            const appUrl =
              get(mapSettings, 'map_application') === 'external'
                ? get(mapSettings, 'map_application_url')
                : `${window.location.origin}/external-components/index.html?component=map`;
            const mapCenter = get(mapSettings, 'map_center', '')
              .split(',')
              .map(Number);
            const wmsLayers = get(mapSettings, 'wms_layers', [])
              .filter(({ instance: { active } }) => active)
              .map(
                ({
                  instance: { url, layer_name, label, feature_info_xpath },
                }) => ({
                  xpath: feature_info_xpath,
                  layers: layer_name,
                  label,
                  url,
                })
              );

            iframe.src = appUrl;
            iframe.style.width = '100%';
            iframe.style.height = ctrl.mapHeight() || '100%';
            iframe.style.border = '1px solid #CCC';
            iframe.title = 'address';
            iframe.allow = 'fullscreen; geolocation';
            iframe.addEventListener('load', () => {
              sendMessage({
                type: 'init',
                name: tryFunction(mapName),
                version: 5,
                value: {
                  center: mapCenter,
                  canDrawFeatures: false,
                  wmsLayers,
                  context: ctrl.context(),
                  featureRequestTargetLayer: ctrl.targetLayer(),
                },
              });

              if (addressValue()) {
                const full = addressValue().address.full;
                const pointGeometry = addressValue().geojson.features[0]
                  .geometry;

                sendMessage({
                  type: 'setMarker',
                  version: 5,
                  name: tryFunction(mapName),
                  value: pointGeometry,
                });

                ctrl.query = full;
                inputDelegate.input[0].value = full;
              }

              window.top.addEventListener('message', handleMessage);
            });

            const sendMessage = (message) => {
              iframe.contentWindow.postMessage(message, '*');
            };

            const pinToClosestAddress = (currentPoint) =>
              fetch(
                `https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest?${new URLSearchParams(
                  {
                    lon: currentPoint.coordinates[0],
                    lat: currentPoint.coordinates[1],
                    q: 'type:adres',
                  }
                )}`
              )
                .then((r) => r.json())
                .then((resp) => {
                  fetch(
                    `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?${new URLSearchParams(
                      { id: resp.response.docs[0].id }
                    )}`
                  )
                    .then((r) => r.json())
                    .then((innerBody) => {
                      const top = innerBody.response.docs[0];

                      return top ? pdokAddressToZsAddress(top) : null;
                    })
                    .then((newState) => {
                      if (newState) {
                        sendMessage({
                          type: 'setMarker',
                          version: 5,
                          name: tryFunction(mapName),
                          value: newState.geojson.features[0].geometry,
                        });

                        ctrl.query = newState.address.full;

                        handleChange(newState);
                      }
                    });
                });

            const handleMessage = (event) => {
              const evName = get(event, 'data.name');
              if (evName !== tryFunction(mapName) || !evName) {
                return;
              }

              if (handleChange && event.data.type === 'click') {
                pinToClosestAddress(get(event, 'data.value'));
              } else if (event.data.type === 'getFeatureInfoResolved') {
                const xpathQueryResult = event.data.value;
                const onFeaturesSelect = ctrl.onFeaturesSelect();

                onFeaturesSelect(xpathQueryResult);
              }

              shouldOpen = false;

              inputDelegate.input[0].focus();
              iframe.focus();

              setTimeout(() => {
                inputDelegate.input[0].blur();
                isOpen = false;
                setTimeout(() => (shouldOpen = true), 1500);
              }, 0);
            };

            ctrl.query = '';

            ctrl.getInputDelegate = () => inputDelegate;

            ctrl.handleFocus = () => {
              if (shouldOpen) {
                isOpen = true;
              }
            };

            ctrl.handleSelect = (suggestion) => {
              const id = suggestion.data || suggestion.value;
              ctrl.query = suggestion.label;

              fetch(
                `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?${new URLSearchParams(
                  { id }
                )}`
              )
                .then((r) => r.json())
                .then((resp) => {
                  const val = pdokAddressToZsAddress(resp.response.docs[0]);

                  sendMessage({
                    type: 'setMarker',
                    version: 5,
                    name: tryFunction(mapName),
                    value: val ? val.geojson.features[0].geometry : null,
                    xpathQuery: true,
                  });
                  handleChange(val);
                });

              isOpen = false;
            };

            ctrl.handleClickOutside = () => {
              if (ctrl.getSuggestions()) {
                $scope.$evalAsync(() => {
                  isOpen = false;
                });
              }

              return false;
            };

            ctrl.getPlaceholder = placeholderReducer.data;

            ctrl.hasQuery = () => {
              return ctrl.query && ctrl.query !== '';
            };

            ctrl.clearAddress = () => {
              ctrl.query = '';
              sendMessage({
                type: 'setMarker',
                version: 5,
                name: tryFunction(mapName),
                value: null,
              });
              handleChange(null);
            };

            const suggestionResource = resource(
              () => {
                return isOpen && ctrl.query
                  ? {
                      url:
                        'https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest',
                      params: {
                        q: ctrl.query,
                        fq: 'type:adres',
                      },
                    }
                  : null;
              },
              { scope: $scope, cache: { disabled: true } }
            ).reduce((requestOptions, data) => {
              return data
                ? data.response.docs.map(pdokSuggestionToZsSuggestion)
                : [];
            });

            ctrl.isOpen = () => isOpen;

            ctrl.getSuggestions = suggestionResource.data;

            $element.find('address-content').replaceWith(iframe);
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
