// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import resourceModule from './../../../api/resource';
import zsSuggestionListModule from './../../zsSuggestionList';
import zsClickOutsideModule from './../../zsClickOutside';
import sessionServiceModule from './../../../user/sessionService';
import composedReducerModule from './../../../api/resource/composedReducer';
import get from 'lodash/get';
import template from './template.html';
import './styles.scss';

const pdokAddressToZsAddress = (doc) => {
  const [, lat, lon] = /POINT\((.+)\s(.+)\)/.exec(doc.centroide_ll) || [
    null,
    '',
    '',
  ];

  return {
    geojson: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Point',
            coordinates: [Number(lat), Number(lon)],
          },
        },
      ],
    },
    address: { full: doc.weergavenaam },
    bag: {
      type: 'nummeraanduiding',
      id: `nummeraanduiding-${doc.nummeraanduiding_id}`,
    },
  };
};

const pdokSuggestionToZsSuggestion = (doc) => {
  return {
    value: doc.id,
    label: doc.weergavenaam,
    id: doc.id,
  };
};

export default angular
  .module('zsMapSearch', [
    resourceModule,
    zsSuggestionListModule,
    zsClickOutsideModule,
    sessionServiceModule,
    composedReducerModule,
  ])
  .directive('zsMapSearch', [
    '$document',
    'resource',
    'composedReducer',
    'sessionService',
    ($document, resource, composedReducer, sessionService) => {
      return {
        restrict: 'E',
        template,
        scope: {
          onChange: '&',
          addressType: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function (scope, element) {
            let ctrl = this,
              suggestionResource,
              placeholderReducer,
              inputDelegate = { input: element.find('input').eq(0) },
              isOpen = false;

            placeholderReducer = composedReducer(
              { scope },
              sessionService.createResource(scope)
            ).reduce((session) => {
              return session
                ? `Zoek op adres (bv. ${get(
                    session,
                    'instance.account.instance.place'
                  )}, ${get(session, 'instance.account.instance.address')})`
                : '';
            });

            ctrl.query = '';

            ctrl.getInputDelegate = () => inputDelegate;

            ctrl.handleFocus = () => {
              isOpen = true;
            };

            ctrl.handleSelect = (suggestion) => {
              fetch(
                `https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup?${new URLSearchParams(
                  { id: suggestion.value }
                )}`
              )
                .then((r) => r.json())
                .then((resp) => {
                  const val = pdokAddressToZsAddress(resp.response.docs[0]);
                  let $location =
                    ctrl.addressType() === 'coordinate'
                      ? [
                          val.geojson.features[0].geometry.coordinates[1],
                          val.geojson.features[0].geometry.coordinates[0],
                        ].join(',')
                      : val.address.full;

                  ctrl.onChange({
                    $location,
                  });
                });

              isOpen = false;
            };

            ctrl.handleClickOutside = () => {
              if (ctrl.getSuggestions()) {
                scope.$evalAsync(() => {
                  isOpen = false;
                });
              }

              return false;
            };

            ctrl.getPlaceholder = placeholderReducer.data;

            suggestionResource = resource(
              () => {
                return isOpen && ctrl.query
                  ? {
                      url:
                        'https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest',
                      params: {
                        q: ctrl.query,
                        fq: 'type:adres',
                      },
                    }
                  : null;
              },
              { scope, cache: { disabled: true } }
            ).reduce((requestOptions, data) => {
              return get(data, 'response.docs', []).map(
                pdokSuggestionToZsSuggestion
              );
            });

            ctrl.isOpen = () => isOpen;

            ctrl.getSuggestions = suggestionResource.data;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
