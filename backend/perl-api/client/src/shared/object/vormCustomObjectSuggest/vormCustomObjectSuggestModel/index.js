// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import zsModalModule from './../../../../shared/ui/zsModal';
import snackbarServiceModule from './../../../../shared/ui/zsSnackbar/snackbarService';
import zsObjectSuggestModule from '../../zsObjectSuggest';
import vormCustomObjectSuggestDisplayModule from '../vormCustomObjectSuggestDisplay';
import composedReducerModule from '../../../api/resource/composedReducer';
import get from 'lodash/get';

export default angular
  .module('vormCustomObjectSuggestModel', [
    zsModalModule,
    zsObjectSuggestModule,
    vormCustomObjectSuggestDisplayModule,
    composedReducerModule,
    snackbarServiceModule,
  ])
  .directive('vormCustomObjectSuggestModel', [
    '$compile',
    ($compile) => {
      return {
        restrict: 'E',
        template,
        scope: {
          delegate: '&',
          inputId: '&',
          placeholder: '@',
          caseData: '&',
          values: '&',
        },
        require: ['vormCustomObjectSuggestModel', '^vormField', 'ngModel'],
        bindToController: true,
        controller: [
          '$scope',
          '$http',
          'sessionService',
          'snackbarService',
          'composedReducer',
          'zsModal',
          function (
            $scope,
            $http,
            sessionService,
            snackbarService,
            composedReducer,
            zsModal
          ) {
            let ctrl = this,
              modal,
              modalScope,
              vormField,
              ngModel;

            ctrl.link = (controllers) => {
              [vormField, ngModel] = controllers;
            };

            let sessionResource = sessionService.createResource($scope);

            let sessionReducer = composedReducer(
              { scope: $scope },
              sessionResource
            ).reduce((session) => (session ? session.instance : {}));

            ctrl.getSession = () => sessionReducer.data();

            ctrl.getObjectType = () => {
              return vormField ? vormField.invokeData('objectType') : null;
            };

            ctrl.getQueryOptions = () => {
              return vormField ? vormField.invokeData('queryOptions') : null;
            };

            ctrl.getAllowCreate = () => {
              return vormField ? vormField.invokeData('allowCreate') : null;
            };

            ctrl.getObjectActionLabel = () => {
              const objectTypeName = vormField
                ? vormField.invokeData('objectTypeName')
                : '';
              const configuredLabel = vormField
                ? vormField.invokeData('createLabel')
                : null;

              return configuredLabel || `${objectTypeName} aanmaken`;
            };

            ctrl.handleSuggest = ($object) => {
              let values = ctrl.values();
              let uuids = values.map((value) => value.id);
              let formatter = get(vormField.templateData(), 'format');
              let object = $object;

              if (formatter) {
                object = formatter(object);
              }

              if (uuids.includes(object.id)) {
                snackbarService.error('Dit object is al gerelateerd.');

                return;
              }

              ngModel.$setViewValue(object, 'click');
            };

            ctrl.clearObject = () => {
              vormField.clearDelegate(ctrl.delegate());
            };

            ctrl.getObject = () => ngModel.$modelValue;

            //modal

            let closeModal = () => {
              modal.close();
              modalScope.$destroy();
              modal = modalScope = null;
            };

            ctrl.openModal = () => {
              const caseUuid = vormField.invokeData('caseUuid');
              const attributeId = vormField.invokeData('attributeId');
              const objectTypeUuid = vormField.invokeData('queryOptions')
                .objectTypeUuid;
              const iframeUrl = `/external-components/exposed/case/${caseUuid}/object/create/?objectTypeUuid=${objectTypeUuid}&attributeId=${attributeId}`;

              modalScope = $scope.$new();

              modalScope.route = () => iframeUrl;

              modalScope.caseData = ctrl.caseData();

              modalScope.onMessage = (message) => {
                if (message && message.type === 'handleObjectSuccess') {
                  $http({
                    url: `/api/v2/cm/custom_object/get_custom_object?uuid=${message.data.uuid}`,
                    method: 'GET',
                  }).then((response) => {
                    // use the version_independent_uuid to always link to the most recent version
                    const id =
                      response.data.data.attributes.version_independent_uuid;

                    const object = {
                      data: undefined,
                      description: response.data.data.attributes.subtitle,
                      id,
                      label: response.data.data.meta.summary,
                      link: `/object/${id}`,
                      name: id,
                      type: 'custom_object',
                    };

                    ngModel.$setViewValue(object, 'click');
                    closeModal();

                    const snackbarText = {
                      created: 'Het object is aangemaakt.',
                      updated: 'Het object is bewerkt.',
                      deactivated: 'Het object is uitgeschakeld.',
                    }[message.data.result];

                    snackbarService.info(snackbarText, {
                      actions: [
                        {
                          type: 'link',
                          label: 'Object openen',
                          link: `/main/object/${id}`,
                        },
                      ],
                    });
                  });
                }
              };

              modal = zsModal({
                title: ctrl.getObjectActionLabel(),
                fullWidth: true,
                el: $compile(
                  `<zs-react-iframe
                  iframe-src="route()"
                  on-message="onMessage(data)"
                  case-data="caseData"
                  data-height="600"
                ></zs-react-iframe>`
                )(modalScope),
              });

              modal.open();
            };
          },
        ],
        controllerAs: 'vormCustomObjectSuggestModel',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(controllers);
        },
      };
    },
  ]).name;
