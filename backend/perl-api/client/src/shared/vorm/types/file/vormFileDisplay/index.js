// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import get from 'lodash/get';
import zsIconModule from './../../../../ui/zsIcon';

export default angular
  .module('vormFileDisplay', [zsIconModule])
  .directive('vormFileDisplay', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          formatter: '&',
          file: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.getFilename = () => {
              let file = ctrl.file(),
                formatter = ctrl.formatter();

              return formatter
                ? formatter(file) || ''
                : get(file, 'filename') || '';
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
