// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import isArray from 'lodash/isArray';
import first from 'lodash/first';
import mapValues from 'lodash/mapValues';

export default (fields, values, type) => {
  return mapValues(values, (value, key) => {
    let field = fields[key],
      val = value,
      hasTransformers = field && get(field, `${type}.length`);

    if (!isArray(val)) {
      val = [val];
    }

    val = hasTransformers
      ? val.map((v) => {
          return field[type].reduce((prev, current) => current(prev), v);
        })
      : val;

    return isArray(value) ? val : first(val);
  });
};
