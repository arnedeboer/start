// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import transformAttributeValues from './transformAttributeValues';

export default (fields, values) =>
  transformAttributeValues(fields, values, 'formatters');
