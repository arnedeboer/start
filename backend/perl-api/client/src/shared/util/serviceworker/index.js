// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import { Promise } from 'es6-promise';
import snackbarServiceModule from './../../ui/zsSnackbar/snackbarService';
import once from 'lodash/once';

if (!localStorage.getItem('swRegistration2') && 'serviceWorker' in navigator) {
  navigator.serviceWorker.getRegistrations().then((registrations) => {
    for (let registration of registrations) {
      registration.unregister();
    }
  });

  localStorage.setItem('swRegistration2', '1');
}

export default angular
  .module('shared.util.serviceworker', [snackbarServiceModule])
  .factory('serviceWorker', [
    '$window',
    '$rootScope',
    'snackbarService',
    ($window, $rootScope, snackbarService) => {
      return (nativeScope) => {
        const scope = `/assets${nativeScope || '/'}`;
        const filePath = `${scope}service-worker.js`;
        let serviceWorker = {},
          supported =
            'serviceWorker' in navigator &&
            (window.location.protocol === 'https:' ||
              window.location.hostname === 'localhost' ||
              window.location.hostname.indexOf('127.') === 0),
          registrationPromise,
          enabled;

        serviceWorker.isSupported = () => supported;

        serviceWorker.isEnabled = () => {
          return navigator.serviceWorker
            .getRegistration(filePath)
            .then((registration) => {
              return Boolean(registration);
            });
        };

        let register = () => {
          registrationPromise = navigator.serviceWorker.register(filePath, {
            scope,
          });

          return registrationPromise;
        };

        let emitNewVersion = () => {
          snackbarService.info(
            'Er is een nieuwe versie van Zaaksysteem beschikbaar.',
            {
              timeout: 0,
              actions: [
                {
                  id: 'refresh',
                  label: 'Herladen',
                  click: () => {
                    $window.location.reload();
                  },
                },
              ],
            }
          );
        };

        serviceWorker.enable = () => {
          return register().catch((err) => {
            console.error('Error installing ServiceWorker', err);
          });
        };

        serviceWorker.disable = () => {
          let promises = [];

          if (enabled === false) {
            return serviceWorker.clear();
          }

          promises.push(serviceWorker.clear());

          promises.push(
            navigator.serviceWorker.getRegistration().then((registration) => {
              if (registration) {
                return registration.unregister();
              }
            })
          );

          return Promise.all(promises);
        };

        serviceWorker.clear = () => {
          return $window.caches.keys().then((cacheNames) => {
            return Promise.all(
              cacheNames.map((cacheName) => {
                return $window.caches.delete(cacheName);
              })
            );
          });
        };

        if (supported) {
          navigator.serviceWorker
            .getRegistration(scope)
            .then((registration) => {
              if (!registration) {
                return;
              }

              registration.onupdatefound = once(() => {
                $rootScope.$apply(() => {
                  emitNewVersion();
                });
              });

              if (typeof registration.update === 'function') {
                registration.update();
              }
            });
        }

        return serviceWorker;
      };
    },
  ]).name;
