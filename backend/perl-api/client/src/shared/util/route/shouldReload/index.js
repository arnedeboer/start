// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import stateRegistrarModule from './../stateRegistrar';
import observableStateParamsModule from './../observableStateParams';
import assign from 'lodash/assign';
import defaults from 'lodash/defaults';

export default angular
  .module('preventReload', [
    angularUiRouter,
    observableStateParamsModule,
    stateRegistrarModule,
  ])

  .run([
    '$rootScope',
    '$q',
    '$state',
    '$stateParams',
    '$urlRouter',
    'observableStateParams',
    'stateRegistrar',
    (
      $rootScope,
      $q,
      $state,
      $stateParams,
      $urlRouter,
      observableStateParams,
      stateRegistrar
    ) => {
      let transitionTo = $state.transitionTo;

      $state.transitionTo = (to, toParams, options, ...rest) => {
        let opts = defaults({}, options, {
            location: true,
            inherit: false,
            relative: null,
            notify: true,
            reload: false,
            $retry: false,
          }),
          reload = true,
          toState = stateRegistrar.getState(to),
          fromParams = $stateParams,
          mergedParams = options.inherit
            ? assign({}, fromParams, toParams)
            : toParams;

        if (
          toState &&
          typeof toState.self.shouldReload === 'function' &&
          !opts.reload
        ) {
          reload = toState.self.shouldReload(
            $state.current,
            fromParams,
            toState.self,
            mergedParams
          );
        }

        // copied from angular-ui-router
        if (!reload) {
          $state.params = mergedParams;
          angular.copy($state.params, $stateParams);

          if (opts.location && toState.navigable && toState.navigable.url) {
            $urlRouter.push(toState.navigable.url, mergedParams, {
              $$avoidResync: true,
              replace: opts.location === 'replace',
            });
            $urlRouter.update(true);
          }
          $state.transition = null;
          return $q.when($state.current);
        }

        return transitionTo(to, toParams, opts, ...rest);
      };
    },
  ]).name;
