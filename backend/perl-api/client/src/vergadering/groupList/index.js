// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from '../../shared/api/resource/composedReducer';
import groupListViewModule from './groupListView';

export default {
  moduleName: angular
    .module('Zaaksysteem.meeting.groupList', [
      resourceModule,
      groupListViewModule,
      composedReducerModule,
    ])
    .controller('groupListController', [
      '$scope',
      'appConfig',
      ($scope, appConfig) => {
        $scope.appConfig = appConfig.data;
      },
    ]).name,
  config: [
    {
      route: {
        url: '',
        scope: {
          appConfig: '&',
        },
        template,
        controller: 'groupListController',
      },
      state: 'groupList',
    },
  ],
};
