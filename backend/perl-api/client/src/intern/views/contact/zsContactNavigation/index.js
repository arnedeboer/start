// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../../shared/api/resource/composedReducer';
import auxiliaryRouteModule from '../../../../shared/util/route/auxiliaryRoute';
import './styles.scss';

export default angular
  .module('zsContactNavigation', [composedReducerModule, auxiliaryRouteModule])
  .component('zsContactNavigation', {
    bindings: {
      subject: '&',
    },
    controller: [
      '$scope',
      '$state',
      '$stateParams',
      'composedReducer',
      'auxiliaryRouteService',
      function (
        scope,
        $state,
        $stateParams,
        composedReducer,
        auxiliaryRouteService
      ) {
        let ctrl = this,
          itemReducer;

        let getStyle = (name) => {
          if ($state.current.name === name) {
            return { selected: true };
          }
          return null;
        };

        itemReducer = composedReducer({ scope }, ctrl.user, ctrl.subject, () =>
          auxiliaryRouteService.getCurrentBase()
        ).reduce((user, subject /*, base*/) => {
          return [
            {
              name: 'summary',
              icon: 'human',
              label: 'Overzicht',
              link: $state.href(
                'contact.summary',
                { subjectUuid: $stateParams.subjectUuid },
                { inherit: true }
              ),
              style: getStyle('contact.summary'),
            },
            {
              name: 'cases',
              icon: 'folder-multiple',
              label: 'Zaken',
              link: $state.href(
                'contact.cases',
                { subjectUuid: $stateParams.subjectUuid },
                { inherit: true }
              ),
              style: getStyle('contact.cases'),
            },
            {
              name: 'timeline',
              icon: 'clock',
              label: 'Tijdlijn',
              link: $state.href(
                'contact.timeline',
                { subjectUuid: $stateParams.subjectUuid },
                { inherit: true }
              ),
              style: getStyle('contact.timeline'),
            },
            {
              name: 'information',
              icon: 'account-box',
              label: 'Gegevens',
              link: $state.href(
                'contact.information',
                { subjectUuid: $stateParams.subjectUuid },
                { inherit: true }
              ),
              style: getStyle('contact.information'),
            },
            {
              name: 'domains',
              icon: 'cloud-outline',
              label: 'Omgevingen',
              link: $state.href(
                'contact.domains',
                { subjectUuid: $stateParams.subjectUuid },
                { inherit: true }
              ),
              hide: !!(subject.instance.subject_type === 'person'),
              style: getStyle('contact.domains'),
            },
            {
              name: 'settings',
              icon: 'settings',
              label: 'Instellingen',
              link: $state.href(
                'contact.settings',
                { subjectUuid: $stateParams.subjectUuid },
                { inherit: true }
              ),
              style: getStyle('contact.settings'),
            },
          ].filter((link) => {
            return !link.hide;
          });
        });

        ctrl.getNavigationItems = itemReducer.data;
      },
    ],
    template,
  }).name;
