// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import get from 'lodash/get';
import reactIframeModule from './../../../../../../../shared/ui/zsReactIframe';
import './styles.scss';

export default angular
  .module(
    'Zaaksysteem.intern.home.zsDashboard.zsDashboardWidget.types.zsDashboardWidgetTasksIframe',
    [reactIframeModule]
  )
  .directive('zsDashboardWidgetTasksIframe', [
    () => {
      return {
        restrict: 'E',
        scope: {
          widgetUuid: '&',
          handleRemoveClick: '&',
          onDataChange: '&',
        },
        template,
        bindToController: true,
        controller: [
          '$element',
          function () {
            let ctrl = this;

            ctrl.getStartUrl = () => {
              return `/external-components/exposed/tasks/${ctrl.widgetUuid()}`;
            };

            ctrl.onMessage = (message) => {
              const widgetUuid = get(message, 'data.widgetUuid');

              if (widgetUuid === ctrl.widgetUuid()) {
                const messageType = get(message, 'type');

                if (messageType === 'deleteWidget') {
                  ctrl.handleRemoveClick();
                } else if (messageType === 'updateWidget') {
                  ctrl.onDataChange({
                    $newData: get(message, 'data.settings'),
                  });
                }
              }
            };

            return ctrl;
          },
        ],
        controllerAs: 'zsDashboardWidgetTasksIframe',
      };
    },
  ]).name;
