// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import './../../../../../shared/styles/_contextual-settings.scss';

export default angular
  .module('zsDashboardSettings', [])
  .directive('zsDashboardSettings', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          onClear: '&',
          onReset: '&',
          isEmpty: '&',
          onClose: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.handleDefaultClick = ($event) => {
              ctrl.onReset({ $event });
              ctrl.onClose({ $event });
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
