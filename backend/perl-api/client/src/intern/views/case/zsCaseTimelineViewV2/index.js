// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import reactIframeModule from '../../../../shared/ui/zsReactIframe';
import template from './template.html';
import './styles.scss';

export default angular
  .module('zsCaseTimelineViewV2', [angularUiRouterModule, reactIframeModule])
  .directive('zsCaseTimelineViewV2', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          caseUuid: '&',
          caseData: '&',
          reloadCase: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.getStartUrl = () => {
              return `/external-components/exposed/case/${ctrl.caseUuid()}/timeline`;
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
