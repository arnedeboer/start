// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './index.html';
import uiRouter from 'angular-ui-router';
import zsIcon from '../../../shared/ui/zsIcon';
import configServiceModule from '../configService';
import rwdServiceModule from '../../../shared/util/rwdService';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import zsUiViewProgressModule from './../../../shared/ui/zsNProgress/zsUiViewProgress';
import appServiceModule from './../appService';
import filterServiceModule from '../filterService';
import zsSuggestionListModule from './../../../shared/ui/zsSuggestionList';
import get from 'lodash/get';
import last from 'lodash/last';
import pick from 'lodash/pick';
import sessionServiceModule from '../../../shared/user/sessionService';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';

import './styles.scss';

export default angular
  .module('Zaaksysteem.mor.morNav', [
    uiRouter,
    zsIcon,
    configServiceModule,
    rwdServiceModule,
    composedReducerModule,
    zsUiViewProgressModule,
    filterServiceModule,
    appServiceModule,
    zsSuggestionListModule,
    sessionServiceModule,
    snackbarServiceModule,
  ])
  .directive('morNav', [
    '$window',
    '$http',
    '$document',
    '$rootScope',
    '$state',
    '$timeout',
    'configService',
    'rwdService',
    'composedReducer',
    'filterService',
    'appService',
    'sessionService',
    'snackbarService',
    'zsStorage',
    (
      $window,
      $http,
      $document,
      $rootScope,
      $state,
      $timeout,
      configService,
      rwdService,
      composedReducer,
      filterService,
      appService,
      sessionService,
      snackbarService
    ) => {
      return {
        restrict: 'E',
        template,
        scope: {
          titleBind: '&',
          onButtonClick: '&',
          views: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            let ctrl = this,
              styleReducer,
              input = $element.find('input'),
              userResource = sessionService.createResource($scope);

            styleReducer = composedReducer(
              { scope: $scope },
              configService.getConfig
            ).reduce((config) => {
              let style = {
                container: {
                  'background-image': `url(${config.header_bgimage})`,
                },
                bg: {
                  'background-color': config.header_bgcolor,
                },
              };

              return style;
            });

            ctrl.getState = () => $state.current.name;

            ctrl.getActiveFilters = () => appService.state().filters;

            ctrl.addFilter = (query) => {
              if (appService.state().filters.length < 3) {
                appService.dispatch('filter_add', query);

                ctrl.searchQuery = '';

                $timeout(
                  () => {
                    input[0].focus();
                  },
                  0,
                  false
                );
              } else {
                snackbarService.info(
                  'U kunt maximaal 3 filters toevoegen. Verwijder een filter en probeer het opnieuw.'
                );
              }
            };

            ctrl.removeFilter = (filter) => {
              appService.dispatch('filter_remove', filter);
            };

            ctrl.handleSuggestionSelect = (suggestion) => {
              if (appService.state().filters.length < 3) {
                ctrl.searchQuery = '';
                appService.dispatch('filter_add', suggestion.id);
              }
            };

            ctrl.handleSearchToggle = () => {
              ctrl.searchActive = !ctrl.searchActive;

              if (!ctrl.searchActive) {
                appService.dispatch('filter_clear');
              }
            };

            ctrl.isSearchVisible = () => {
              return ctrl.searchActive;
            };

            ctrl.getSuggestions = composedReducer(
              { scope: $scope },
              () => ctrl.searchQuery
            ).reduce((query = '') => {
              return filterService.getFromIndex(query).map((suggestion) => {
                let rex = /(<([^>]+)>)|(&lt;([^>]+)&gt;)/gi;

                return {
                  id: suggestion,
                  label: suggestion.replace(rex, ' '),
                };
              });
            }).data;

            ctrl.handleSearchKeyUp = (event) => {
              switch (event.keyCode) {
                case 27:
                  ctrl.handleSearchToggle();
                  break;

                case 8:
                  if (!ctrl.searchQuery && ctrl.getActiveFilters().length) {
                    ctrl.removeFilter(last(ctrl.getActiveFilters()));
                  }
                  break;

                case 13:
                  if (ctrl.searchQuery.length) {
                    ctrl.addFilter(ctrl.searchQuery);
                  }
                  break;
              }
            };

            ctrl.getKeyInputDelegate = () => ({ input });

            // Get header nav styling
            ctrl.getContainerStyle = () =>
              get(styleReducer.data(), 'container');
            ctrl.getBgContainerStyle = () => get(styleReducer.data(), 'bg');

            ctrl.handleLogout = () => {
              $http({
                url: '/auth/logout',
                method: 'POST',
              }).then(() => {
                $window.location = `/auth/login?referer=${$window.location.pathname}`;
              });
            };

            userResource.subscribe((data) => {
              if (get(data, 'type') === 'exception') {
                $window.location = `/auth/login?referer=${$window.location.pathname}`;
              }
            });

            // Get title
            ctrl.getViewportTitle = () => configService.getConfig().title;

            $rootScope.$on('mor.view.scroll', (event, options) => {
              let container = $element[0].querySelector(
                  '.mor-nav__tabs-slidecontainer'
                ),
                slider = container.querySelector('.mor-nav__tabs-slide'),
                percentage = options.percentage,
                scaleX = 1 / ctrl.views().length,
                translateX = percentage * scaleX - scaleX / 2;

              slider.style.transform = slider.style.webkitTransform = `translateX(${Math.round(
                translateX * 100
              )}%) scaleX(${scaleX})`;

              if (
                !angular.equals(
                  pick(
                    slider.style,
                    'transitionTimingFunction',
                    'transitionDuration'
                  ),
                  pick(
                    options,
                    'transitionTimingFunction',
                    'transitionDuration'
                  )
                )
              ) {
                slider.style.transitionTimingFunction =
                  options.transitionTimingFunction;
                slider.style.transitionDuration = options.transitionDuration;
              }
            });
          },
        ],
        controllerAs: 'morNav',
      };
    },
  ])
  .directive('focusOnCondition', [
    '$timeout',
    ($timeout) => {
      return {
        restrict: 'A',
        link: (scope, element, attrs) => {
          $timeout(() => {
            scope.$watch(attrs.focusOnCondition, (value) => {
              if (value) {
                element[0].focus();
              }
            });
          });
        },
      };
    },
  ])
  .directive('zsScroll', [
    '$window',
    ($window) => {
      return {
        restrict: 'A',
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            let el = $element[0],
              tabsEl = el.querySelector('.mor-nav__tabs'),
              containerEl = el.querySelector('.mor-nav__container'),
              buttonEl = el.querySelector('.mor-nav__button-bar'),
              containerHeight = containerEl.clientHeight,
              tabsHeight = tabsEl.clientHeight,
              maxOpacity = 0.25,
              lastSet = NaN;

            let getMultiplier = () =>
              Math.max(
                0,
                Math.min(
                  1,
                  $window.pageYOffset / (containerHeight - tabsHeight)
                )
              );

            let setDimensions = () => {
              let multiplier = getMultiplier(),
                maxMargin =
                  buttonEl.clientWidth +
                  parseFloat(buttonEl.style.paddingLeft || 0) +
                  parseFloat(buttonEl.style.paddingRight || 0);

              tabsEl.style.marginRight = `${multiplier * maxMargin}px`;
            };

            let setStyle = () => {
              let multiplier = getMultiplier(),
                opacity = (1 - multiplier) * maxOpacity;

              if (lastSet === multiplier) {
                return;
              }

              lastSet = multiplier;

              if (multiplier >= 1) {
                $element.addClass('small');
              } else {
                $element.removeClass('small');
              }

              el.querySelector('.mor-nav__bg').style.opacity = opacity;

              setDimensions();
            };

            angular.element($window).bind('scroll', setStyle);
            angular.element($window).bind('resize', setDimensions);

            $scope.$$postDigest(setStyle);
            $scope.$$postDigest(setDimensions);
          },
        ],
      };
    },
  ]).name;
