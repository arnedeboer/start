// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import uiViewTransitionDirectionModule from './../../../shared/util/route/uiViewTransitionDirection';
import morNavModule from './../morNav';
import zsSnackbarModule from './../../../shared/ui/zsSnackbar';
import template from './template.html';
import FTScroller from 'ftscroller';
import indexOf from 'lodash/indexOf';
import './styles.scss';

export default angular
  .module('Zaaksysteem.mor.morApp', [
    angularUiRouterModule,
    uiViewTransitionDirectionModule,
    morNavModule,
    zsSnackbarModule,
  ])
  .directive('morApp', [
    '$window',
    '$timeout',
    '$rootScope',
    '$state',
    '$stateParams',
    '$compile',
    ($window, $timeout, $rootScope, $state, $stateParams, $compile) => {
      const X_BOUNDS = 30;

      return {
        restrict: 'E',
        template,
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            let ctrl = this,
              viewOrder = ['open', 'afgehandeld'],
              indexBy = 'caseType',
              scroller,
              viewPlaceholder = angular.element(
                '<div class="view-placeholder view-placeholder-loader"><zs-spinner is-loading="true"></zs-spinner></div>'
              ),
              parent = $element[0].querySelector('.scroll-container'),
              render = false;

            $compile(viewPlaceholder)($scope);

            let isRelevantStateChange = (
              toState,
              toParams,
              fromState /*, fromParams*/
            ) => {
              return (
                toState.name === 'caseList' && fromState.name === 'caseList'
              );
            };

            let getStateValue = (params = $stateParams) => {
              return params[indexBy];
            };

            let dispatchProgress = () => {
              let style = parent.querySelector('.ftscroller_x').style,
                transform = style.transform || style.webkitTransform,
                translateX = transform
                  ? transform.match(/(\d+(\.\d+)?)px/)[1]
                  : 0,
                percentage = Math.max(
                  0,
                  Math.min(
                    1,
                    Number(translateX) /
                      (parent.childNodes.length * parent.offsetWidth)
                  )
                ),
                transitionTimingFunction = style.transitionTimingFunction,
                transitionDuration = style.transitionDuration;

              $rootScope.$emit('mor.view.scroll', {
                percentage,
                transitionTimingFunction,
                transitionDuration,
              });

              if (render) {
                $window.requestAnimationFrame(dispatchProgress);
              }
            };

            let startRender = () => {
              render = true;
              $window.requestAnimationFrame(dispatchProgress);
            };

            let stopRender = () => {
              render = false;
            };

            let setScroller = (prevStateValue) => {
              let isCorrectSegment = false,
                currentSegment = scroller.currentSegment.x,
                expectedSegment = viewOrder.indexOf(getStateValue()),
                viewToRemove = parent.querySelector('ui-view');

              isCorrectSegment = currentSegment === expectedSegment;

              Array.prototype.forEach.call(
                $element[0].querySelectorAll('.view-placeholder'),
                (el) => {
                  angular.element(el).remove();
                }
              );

              if (ctrl.hasViewsBefore()) {
                viewToRemove.parentNode.insertBefore(
                  viewPlaceholder.clone()[0],
                  viewToRemove
                );
              }

              if (ctrl.hasViewsAfter()) {
                angular.element(viewToRemove).after(viewPlaceholder.clone());
              }

              $timeout(
                () => {
                  if (!isCorrectSegment) {
                    let scroll = expectedSegment * parent.offsetWidth;

                    scroller.scrollTo(scroll, 0, prevStateValue ? 250 : 0);
                  }
                },
                0,
                false
              );
            };

            ctrl.hasViewsBefore = () => {
              return indexOf(viewOrder, getStateValue()) > 0;
            };

            ctrl.hasViewsAfter = () => {
              return indexOf(viewOrder, getStateValue()) < viewOrder.length - 1;
            };

            ctrl.getViewOrder = () => viewOrder;
            ctrl.getIndexby = () => indexBy;

            $rootScope.$on('$stateChangeStart', (event, ...rest) => {
              if (isRelevantStateChange(...rest)) {
                viewPlaceholder[0].innerHTML = parent.querySelector(
                  'ui-view'
                ).innerHTML;
                angular
                  .element(viewPlaceholder)
                  .removeClass('view-placeholder-loader');
              }
            });

            $rootScope.$on('$stateChangeSuccess', (event, ...rest) => {
              if (isRelevantStateChange(...rest)) {
                $scope.$$postDigest(() => {
                  setScroller(getStateValue(rest[3]));
                  if (rest[1].caseType !== rest[3].caseType) {
                    $window.scrollTo(0, 0);
                  }
                });
              }
            });

            $rootScope.$on('$viewContentAnimationEnded', () => {
              $window.requestAnimationFrame(dispatchProgress);
            });

            $window.addEventListener('resize', () => {
              let scrollTo =
                viewOrder.indexOf(getStateValue()) * parent.offsetWidth;

              scroller.scrollTo(scrollTo, 0, 0);
              dispatchProgress();
            });

            $scope.$$postDigest(() => {
              let lastX;

              scroller = new FTScroller.FTScroller(parent, {
                alwaysScroll: true,
                bouncing: false,
                flinging: true,
                scrollbars: false,
                singlePageScrolls: true,
                snapping: true,
                updateOnWindowResize: true,
                updateOnChanges: false,
                scrollingY: false,
                scrollBoundary: X_BOUNDS,
                scrollResponseBoundary: X_BOUNDS,
                disabledInputMethods: { scroll: true },
                maxFlingDuration: 350,
              });

              scroller.addEventListener('scrollstart', startRender);

              scroller.addEventListener('scrollend', stopRender);

              scroller.addEventListener('segmentdidchange', (segment) => {
                let stateVal = viewOrder[segment.segmentX];

                if (stateVal !== getStateValue()) {
                  $scope.$evalAsync(() => {
                    $state.go('caseList', { [indexBy]: stateVal });
                  });
                }
              });

              // FTScroller cancels the event if x < X_BOUNDS,
              // which results in vertical scrolls being canceled,
              // so we just stop propagation if the difference

              parent.addEventListener(
                'touchstart',
                (event) => {
                  lastX = event.touches[0].clientX;
                },
                true
              );

              parent.addEventListener(
                'touchmove',
                (event) => {
                  if (Math.abs(lastX - event.touches[0].clientX) < X_BOUNDS) {
                    event.stopPropagation();
                  }
                },
                true
              );

              parent.addEventListener('touchend', () => {
                lastX = NaN;
              });

              let unwatcher = $scope.$watch(
                () => $stateParams[indexBy],
                (val) => {
                  if (val) {
                    unwatcher();
                    dispatchProgress();
                    setScroller();
                  }
                }
              );
            });
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
