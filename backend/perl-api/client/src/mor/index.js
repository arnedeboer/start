// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import routing from './routing';
import resourceModule from './../shared/api/resource';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import configServiceModule from './shared/configService';
import appServiceModule from './shared/appService';
import serviceworkerModule from './../shared/util/serviceworker';
import get from 'lodash/get';
import without from 'lodash/without';
import uniq from 'lodash/uniq';
import createManifest from './shared/manifest';
import sessionServiceModule from '../shared/user/sessionService';
import loadIconFont from '../shared/util/loadIconFont';
import '../../node_modules/angular/angular-csp.css';

export default angular
  .module('Zaaksysteem.mor', [
    routing,
    resourceModule,
    ngAnimate,
    configServiceModule,
    appServiceModule,
    serviceworkerModule,
    sessionServiceModule,
  ])
  .config([
    '$provide',
    ($provide) => {
      // make sure sourcemaps work
      // from https://github.com/angular/angular.js/issues/5217

      $provide.decorator('$exceptionHandler', [
        '$delegate',
        ($delegate) => {
          return (exception, cause) => {
            $delegate(exception, cause);
            setTimeout(() => {
              console.error(exception.stack);
            });
          };
        },
      ]);
    },
  ])
  .config([
    'appServiceProvider',
    (appServiceProvider) => {
      appServiceProvider
        .setDefaultState({ filters: [] })
        .reduce('filter_add', 'filters', (filters, filterToAdd) =>
          uniq(filters.concat(filterToAdd))
        )
        .reduce('filter_remove', 'filters', (filters, filterToRemove) =>
          without(filters, filterToRemove)
        )
        .reduce('filter_clear', 'filters', () => []);
    },
  ])
  .config([
    '$httpProvider',
    ($httpProvider) => {
      $httpProvider.defaults.withCredentials = true;
      $httpProvider.defaults.headers.common['X-Client-Type'] = 'web';
      $httpProvider.useApplyAsync(true);
      $httpProvider.interceptors.push([
        'zsStorage',
        '$window',
        '$q',
        (zsStorage, $window, $q) => ({
          response: (response) => {
            if (response.config.url === '/api/v1/session/current') {
              if (!response.data.result.instance.logged_in_user) {
                zsStorage.clear();
                $window.location.href = `/auth/login?referer=${$window.location.pathname}`;
                return $q.reject(response);
              }
            }

            return $q.resolve(response);
          },
          responseError: (rejection) => {
            if (
              rejection.status === 401 ||
              // configuration_incomplete implies the API is not configured for public accces,
              // which means we don't have a logged in user
              get(rejection, 'data.result.instance.type') ===
                'api/v1/configuration_incomplete'
            ) {
              zsStorage.clear();
              $window.location.href = `/auth/login?referer=${$window.location.pathname}`;
            }

            return $q.reject(rejection);
          },
        }),
      ]);
    },
  ])
  .config([
    'resourceProvider',
    (resourceProvider) => {
      zsResourceConfiguration(resourceProvider.configure);
    },
  ])
  .run(loadIconFont)
  .run([
    '$document',
    'configService',
    ($document, configService) => {
      let linkEl;

      configService.getResource().onUpdate((data) => {
        createManifest(get(data, 'instance.interface_config'), $document);
        let color = encodeURIComponent(
          get(data, 'instance.interface_config.accent_color', '#FFF')
        );

        if (linkEl) {
          linkEl.remove();
        }

        linkEl = $document[0].createElement('link');
        linkEl.rel = 'stylesheet';
        linkEl.type = 'text/css';
        linkEl.href = `/api/style/mor?accent_color=${color}`;

        $document[0].head.appendChild(linkEl);
      });
    },
  ])
  .run([
    '$animate',
    ($animate) => {
      $animate.enabled(true);
    },
  ])
  .run([
    '$window',
    'serviceWorker',
    ($window, serviceWorker) => {
      let enable = ENV.USE_SERVICE_WORKERS || true;
      let worker = serviceWorker();

      if (worker.isSupported()) {
        worker.isEnabled().then((enabled) => {
          if (enabled !== enable) {
            return enable
              ? worker.enable()
              : worker.disable().then(() => {
                  if (enabled) {
                    $window.location.reload();
                  }
                });
          }
        });
      }
    },
  ]).name;
