// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import caseList from './caseList';
import caseDetail from './caseDetail';
import caseComplete from './caseComplete';
import flatten from 'lodash/flatten';
import configServiceModule from './shared/configService';
import snackbarServiceModule from './../shared/ui/zsSnackbar/snackbarService';
import sessionServiceModule from './../shared/user/sessionService';
import morAppModule from './shared/morApp';
import zsUiViewProgressModule from './../shared/ui/zsNProgress/zsUiViewProgress';
import merge from 'lodash/merge';

export default angular
  .module('Zaaksysteem.mor.routing', [
    uiRouter,
    configServiceModule,
    caseList.moduleName,
    caseDetail.moduleName,
    caseComplete.moduleName,
    snackbarServiceModule,
    sessionServiceModule,
    morAppModule,
    zsUiViewProgressModule,
  ])
  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    ($stateProvider, $urlRouterProvider, $locationProvider) => {
      $locationProvider.html5Mode(true);
      $urlRouterProvider.otherwise(($injector) => {
        let $state = $injector.get('$state');

        $state.go('caseList', { caseType: 'open' });
      });

      $stateProvider.state({
        name: 'root',
        template: '<mor-app></mor-app>',
        resolve: {
          user: [
            '$rootScope',
            '$q',
            '$window',
            'snackbarService',
            'sessionService',
            ($rootScope, $q, $window, snackbarService, sessionService) => {
              let resource = sessionService.createResource($rootScope);

              return resource
                .asPromise()
                .then(() => resource)
                .catch((err) => {
                  $window.location = `/auth/login?referer=${$window.location.pathname}`;

                  snackbarService.error(
                    'U wordt doorverwezen naar het loginscherm.'
                  );

                  return $q.reject(err);
                });
            },
          ],
          config: [
            '$q',
            'configService',
            'snackbarService',
            ($q, configService, snackbarService) => {
              return configService
                .getResource()
                .asPromise()
                .catch((error) => {
                  snackbarService.error(
                    'De configuratie van de app kon niet geladen worden. Neem contact op met uw beheerder voor meer informatie.'
                  );

                  return $q.reject(error);
                });
            },
          ],
        },
      });

      flatten(
        [caseList, caseDetail, caseComplete].map(
          (routeConfig) => routeConfig.config
        )
      ).forEach((route) => {
        let mergedState = merge(route.route, {
          parent: 'root',
        });

        $stateProvider.state(route.state, mergedState);
      });
    },
  ])
  .run([
    '$rootScope',
    ($rootScope) => {
      $rootScope.$on('$stateChangeError', (...rest) => {
        console.log(...rest);
      });
    },
  ]).name;
