package Zaaksysteem::Tools::TextVector;
use warnings;
use strict;
use autodie;

use Exporter qw(import);

our @EXPORT_OK = qw(
    vectorize
    to_text_vector
);

use List::Util qw(uniq);

=head1 NAME

Zaaksysteem::Tools::TextVector - Play with text vectors

=head1 DESCRIPTION

Namespace for dealing with text vectors so we are keeping it DRY

=head1 METHODS

=head2 vectorize

Make a value ready to be added to a text vector. References aren't vectorized
unless they are references to arrays and/or hashes. Goes only one level deep.

=cut

sub vectorize {
    my $val = shift;

    return unless defined $val;

    my $ref = ref $val;

    if($ref eq 'ARRAY') {
        $val = join ' ', @{ $val }
    }
    elsif ($ref eq 'HASH') {
        $val = join ' ', %{ $val }
    }
    elsif ($ref) {
        return;
    }

    $val =~ s/[^\w\s\.\@]//g;

    return lc($val);
}

=head2 to_text_vector

A list of vectors are stringified so we can save them in the database as a text
vector.

=cut

sub to_text_vector {
    my @vector = @_;
    return join(' ', uniq grep { defined && length > 2 } @vector);
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
