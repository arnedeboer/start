package Zaaksysteem::StatsD;

use Moose;
use Scalar::Util qw/blessed/;
use Log::Log4perl;

=head1 NAME

Zaaksysteem::StatsD - StatsD Module for Zaaksysteem

=head1 SYNOPSIS

    ### Increment by one
    $c->statsd->increment('employee.login.failed', 1);

=head1 DESCRIPTION

Interface to statsd daemon

=head1 METHODS

=head2 $c->statsd

Return value: $STATSD_OBJECT

    my $statsd_object = $c->statsd;

Returns the L<Zaaksysteem::StatsD::Backend> object, which is documented below

=cut

our $_statsd_per_request;
our $_statsd;

sub _init_statsd {
    my ($c) = @_;

    ### Initialize an empty and default statsd config (active = false)
    $_statsd = undef;
    $_statsd_per_request = {
        config  => {
            host        => '127.0.0.1',
            port        => 8125
        },
        active          => 0,
    };

    ### When $c is blessed (we are in a request, outside testsuite), load statsd and activate
    if (blessed($c)) {
        $_statsd_per_request->{calledhost} = $c->get_server_hostname;

        ### IP hosts not allowed
        $_statsd_per_request->{calledhost} = 'unknown' if ($_statsd_per_request->{calledhost} =~ /^[\d\.]+$/);

        if ($c->config->{statsd} && $c->config->{statsd}->{host} && $c->config->{statsd}->{port}) {
            $_statsd_per_request->{config} = {
                host        => $c->config->{statsd}->{host},
                port        => $c->config->{statsd}->{port},
            };
        }

        $_statsd_per_request->{instance_uuid}  = $c->config->{logging_id};
        $_statsd_per_request->{active}         = 1;
    }

    return 1;
}

sub statsd {
    my $c       = shift;

    return $_statsd if $_statsd;

    $c->_init_statsd unless $_statsd_per_request;

    return $_statsd = Zaaksysteem::StatsD::Backend->new(
        %$_statsd_per_request
    );
}


package Zaaksysteem::StatsD::Backend;

use Net::Statsd;
use Time::HiRes qw/gettimeofday tv_interval/;

use Moose;

has 'config'    => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub { {}; }
);

has [qw/active calledhost instance_uuid/] => (
    'is'        => 'rw',
);

sub BUILD {
    my $self            = shift;

    return unless $self->active;
}

sub _prepare {
    my $self            = shift;

    return unless $self->active;

    $Net::Statsd::HOST = $self->config->{host};

    if ($self->config->{port}) {
        $Net::Statsd::PORT = $self->config->{port};
    }

    return 1;
}

sub prefix {
    my $self = shift;

    my $SILO_ID = $ENV{ZS_SILO_ID} // 'unknown';
    (my $HOST = $self->calledhost) =~ s/\./_/g;

    my $COMPONENT = "zaaksysteem_api";
    $COMPONENT .= '_internal' if $ENV{ZAAKSYSTEEM_INTERNAL};

    return "$COMPONENT.silo.$SILO_ID.$HOST.";
}

=head1 BACKEND METHODS

=head2 $c->statsd->increment($STRING_PREFIX, $INTEGER)

Return value: $BOOL_SUCCESS

    $c->statsd->increment('employee.login.failed', 1);

Increments a statsd counter with one.

=cut

sub increment {
    my $self            = shift;
    my $metric          = shift || 1;
    my $count           = shift || 1;

    return unless $self->_prepare;

    if ($ENV{ZS_LOG_STATSD}) {
        my $logger = Log::Log4perl->get_logger();
        $logger->trace(sprintf("StatsD increment %s%s: %s", $self->prefix, $metric, $count));
    }

    return Net::Statsd::increment($self->prefix . $metric, $count);
}

=head2 $c->statsd->gauge($STRING_PREFIX, $INTEGER)

Return value: $BOOL_SUCCESS

    $c->statsd->gauge('employee.login.failed', 4);

Set the gauge of a metric

=cut

sub gauge {
    my $self            = shift;
    my $metric          = shift || 1;
    my $gauge           = shift || 1;

    return unless $self->_prepare;

    if ($ENV{ZS_LOG_STATSD}) {
        my $logger = Log::Log4perl->get_logger();
        $logger->trace(sprintf("StatsD gauge %s%s: %s", $self->prefix, $metric, $gauge));
    }

    return Net::Statsd::gauge($self->prefix . $metric, $gauge);
}



=head2 $c->statsd->timing($STRING_PREFIX, $TIME_MICROSECONDS)

Return value: $BOOL_SUCCESS

    $c->statsd->timing('request.time', '2500');

Increments a statsd counter with one.

=cut

sub timing {
    my $self            = shift;
    my $metric          = shift;

    if ($ENV{ZS_LOG_STATSD}) {
        my $logger = Log::Log4perl->get_logger();
        $logger->trace(sprintf("StatsD timing: %s%s: %sms", $self->prefix, $metric, join("ms, ", @_)));
    }

    return unless $self->_prepare;
    return Net::Statsd::timing($self->prefix . $metric, @_);
}

=head2 $c->statsd->start()

Return value: [gettimeofday]

    $t0 = $c->statsd->start()

    $c->statsd->end('request.timing', $t0);
    ### Calculates difference between t0 and 'end'


Will return a t0 for timing later on. Convenience method.

=cut

sub start {
    my $self            = shift;

    return [gettimeofday];
}

=head2 $c->statsd->end()

Return value: $BOOLEAN_SUCCESS

    $t0 = $c->statsd->start()

    $c->statsd->end('request.timing', $t0);
    ### Calculates difference between t0 and 'end'


When given a C<t0>, it will calculate the time between it and sends it to this metric

=cut

sub end {
    my $self            = shift;
    my $metric          = shift;
    my $t0              = shift;

    die('Expected an array of "gettimeofday"') unless (ref $t0 eq 'ARRAY');

    my $time = int(tv_interval ( $t0, [gettimeofday])*1000);
    $self->timing($metric, $time);
    return $time;
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
