package Zaaksysteem::Object::Types::Person;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw (
    Zaaksysteem::Object::Roles::Relation
    Zaaksysteem::BR::Subject::Types::Person
    Zaaksysteem::Object::Types::Roles::Address
);

use BTTW::Tools;
use Zaaksysteem::Types qw(
    ObjectSubjectType
    JSONBoolean
    Datestamp
    PersonPersonalNumber
    PersonPersonalNumberA
);

use Zaaksysteem::Object::Types::Address;
use Zaaksysteem::Object::Types::MunicipalityCode;
use Zaaksysteem::Backend::Subject::Naamgebruik qw/naamgebruik/;


=head1 NAME

Zaaksysteem::Object::Types::Person - This object describes a subject of the type: Person

=head1 DESCRIPTION

This object contains the primary data of a person, like the "personal_number" (BSN) and their
name. It has references to their correspondence and residential address.

=head1 PERSON ATTRIBUTES

=head2 personal_number

The personal number of a person, called "BSN" or "Burgerservicenummer" in the Netherlands

=cut

has personal_number => (
    is       => 'rw',
    isa      => PersonPersonalNumber,
    traits   => [qw(OA)],
    label    => 'Personal Number (BSN)',
    required => 0,
);

=head2 personal_number_a

The "hidden" personal number of a person, called "A nummer" in the Netherlands

=cut

has personal_number_a => (
    is       => 'rw',
    isa      => PersonPersonalNumberA,
    traits   => [qw(OA)],
    label    => 'Personal Number A (A Nummer)',
    required => 0,
);


has persoonsnummer => (
    is       => 'rw',
    isa      => PersonPersonalNumberA,
    traits   => [qw(OA)],
    label    => 'Sedula/cedula',
    required => 0,
);

=head2 initials

The initials of a person

=cut

has initials => (
    is       => 'rw',
    isa      => 'Maybe[Str]',
    traits   => [qw(OA)],
    label    => 'Initials',
    required => 0,
);

=head2 first_names

The first names of a person

=cut

has first_names => (
    is       => 'rw',
    isa      => 'Maybe[Str]',
    traits   => [qw(OA)],
    label    => 'First names',
    required => 0,
);

=head2 family_name

The given family name, geslachtsnaam, of a person.

=cut

has family_name => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Family name',
    required => 0,
);

=head2 noble_title

The noble title of this person, or "Adellijke titel" in Dutch.

=cut

has noble_title => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Noble title',
    required => 0,
);

=head2 prefix

The prefix of a person, "tussenvoegsel" in Dutch

=cut

has prefix => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Prefix (tussenvoegsel)',
    required => 0,
);

=head2 gender

The gender of a person

=cut

has gender => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Gender',
    required => 0,
);

=head2 place_of_birth

The place of birth for a person

=cut

has place_of_birth => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Place of birth',
    required => 0,
);

# =head2 country_of_birth

# The country of birth for a person

# =cut

# has country_of_birth => (
#     is       => 'rw',
#     isa      => 'Str',
#     traits   => [qw(OA)],
#     label    => 'Country of birth',
#     required => 0,
# );

=head2 date_of_birth

The date of birth for a person

=cut

has date_of_birth => (
    is       => 'rw',
    isa      => Datestamp,
    traits   => [qw(OA)],
    label    => 'Date of birth',
    required => 0,
    coerce   => 1,
);

=head2 date_of_death

The date of birth for a person

=cut

has date_of_death => (
    is       => 'rw',
    isa      => Datestamp,
    traits   => [qw(OA)],
    label    => 'Decease date',
    required => 0,
    coerce   => 1,
);

=head2 is_secret

Whether this person is marked secret

=cut

has is_secret => (
    is       => 'rw',
    isa      => JSONBoolean,
    traits   => [qw(OA)],
    label    => 'Secret?',
    coerce   => 1,
    required => 0,
);

=head2 is_local_resident

Whether this person lives in the local area (gemeente)

=cut

has is_local_resident => (
    is       => 'rw',
    isa      => JSONBoolean,
    traits   => [qw(OA)],
    coerce   => 1,
    label    => 'Local resident?',
    required => 0,
);

=head2 use_of_name

The use of the name indicator (aanduiding_naamgebruik)

=cut

has use_of_name => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Use of name',
    required => 0,
);

=head2 surname

The result of use of the name. Real achternaam

=cut

has surname => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Result of use of namee',
    required => 0,
    lazy     => 1,
    builder  => '_build_surname',
    writer   => '_set_surname',     ### We disable changing the attribute
);

=head2 mobile_phone_number

This subject's mobile phone number.

=cut

has mobile_phone_number => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Mobile phone number',
    required => 0,
);

=head2 phone_number

This subject's preferred phone number.

=cut

has phone_number => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Phone number',
    required => 0,
);

=head2 email_address

This subject's email address

=cut

has email_address => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Email address',
    required => 0,
);

=head2 partner

B<Type>: L<Zaaksysteem::Object::Types::Person>

The related person object which defines the partner for this person. A subset of the complete
L<Person|Zaaksysteem::Object::Types::Person> object, because we only store the prefix and family_name
of the partner.

=cut

has partner => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Object::Types::Person',
    traits   => [qw(OR)],
    embed    => 1,
    label    => 'Partner data',
    required => 0,
);

=head1 PRIVATE METHODS

=head2 _build_surname

Builds the surname using data from C<< $self->partner >> and the current information from
family_name,prefix etc.

Uses L<Zaaksysteem::Backend::Subject::Naamgebruik> for this logic

=cut

sub _build_surname {
    my $self        = shift;

    return naamgebruik(
        {
            aanduiding              => $self->use_of_name,
            voorvoegsel             => $self->prefix,
            geslachtsnaam           => $self->family_name,
            partner_voorvoegsel     => ($self->partner ? $self->partner->prefix : ''),
            partner_geslachtsnaam   => ($self->partner ? $self->partner->family_name : ''),
        }
    );
}



__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
