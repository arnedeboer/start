package Zaaksysteem::Object::Roles::Security;

use Moose::Role;
use namespace::autoclean;

requires 'capabilities';

use BTTW::Tools;
use Zaaksysteem::Object::SecurityRule;

use Moose::Util::TypeConstraints qw[duck_type enum];

=head1 NAME

Zaaksysteem::Object::Roles::Security - Augment an object class with security
extensions

=head1 DESCRIPTION

This role implements the security aspect of L<object|Zaaksysteem::Object>s.

An B<object> has zero or more B<ACL entries>, which encode B<security_rules>
that are used by the L<model|Zaaksysteem::Object::Model> to determine which
B<security identity> has the required B<permissions>.

=head1 ATTRIBUTES

=head2 _rules

This attribute holds an array of rules that apply to the security of the
object instance.

=head3 Sub-interfaces

This attribute uses the L<Array|Moose::Meta::Attribute::Native::Trait::Array>
trait, and exposes a select group of interfaces to that trait.

=over 4

=item C<all_rules> returns the list of all rules

=item C<add_rule> add a rule to the end of the list

=back

=cut

has security_rules => (
    is => 'rw',
    isa => 'ArrayRef[Zaaksysteem::Object::SecurityRule]',
    traits => [qw[Array]],
    default => sub { [] },

    handles => {
        all_rules => 'elements',
        _add_rule  => 'push',
        find_rule => 'first'
    }
);

sub add_rule {
    my $self = shift;
    my $rule = shift;
    if ($self->find_rule(sub { $_->compare($rule) })) {
        return;
    }
    $self->_add_rule($rule);
    return;
};

=head2 update_security_rules

This domain-crossing attribute is an indication to the model that the security
rules contained in the object should or should not be saved to the database.

Defaults to 1, but when we inflate this object from JSON, we want to ignore
the POSTed rules.

=cut

has update_security_rules => (
    is => 'rw',
    isa => 'Bool',
    default => 1
);

=head1 METHODS

=head2 permit

This is a convenience method for a clean internal API, it calls L</ratify>,
passing on all arguments provided plus setting the verdict to C<permit>.

=cut

sub permit {
    my $self = shift;
    my $entity = shift;

    for my $capability (@_) {
        $self->ratify(
            entity     => $entity,
            capability => $capability,
        );
    }

    return $self;
}

=head2 proscribe

This is a convenience method for a clean internal API, it calls L</ratify>,
passing on all arguments provided plus setting the verdict to C<proscribe>.

=cut

sub proscribe {
    my $self = shift;
    my $entity = shift;

    for my $capability (@_) {
        $self->ratify(
            entity     => $entity,
            capability => $capability,
        );
    }

    return $self;
}

=head2 ratify

This is the main method for adding rules that modify the security behavior
of instances of objects that C<does> this role.

=head3 Arguments

=over 4

=item entity

This argument must hold an object that implements the C<security_identity>
method. The object passed will identify itself through that interface.

=item capability

This argument must hold a textual capability to be permitted or proscribed.
It is checked against the known capabilities of the object that this role is
applied to through the C<capabilities> interface of that object. If a
non-existant capability is requested, a C<object/security/capability>
exception will be thrown.

=item groupname

TODO XXX NEW DOCS

=back

=cut

define_profile ratify => (
    required => {
        entity => duck_type([qw[security_identity]]),
        capability => 'Str',
    },
    optional => {
        groupname => 'Str'
    }
);

sub ratify {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    unless(grep { $_ eq $opts->{ capability } } $self->capabilities) {
        throw('object/security/capability', sprintf(
            'Unable to ratify permission on object %s, the type does not implement "%s"',
            $self->TO_STRING,
            $opts->{ capability }
        ));
    }

    my $new_rule = Zaaksysteem::Object::SecurityRule->new($opts);
    $self->add_rule($new_rule);

    return $self;
}

=head2 revoke

This is the main method for removing rules that modify the security behavior
of instances of objects that C<does> this role.

=head3 Arguments

=over 4

=item entity

This argument must hold an object that implements the C<security_identity>
method. The object passed will identify itself through that interface.

=item capability

This optional argument must hold a textual capability to be revoked.

When not supplied as key/value pair, it is assumed to be a 'wildcard'
capability, and all rules matching the supplied arguments will be deleted.

=item groupname

TODO XXX NEW DOCS

=back

=cut

define_profile revoke => (
    required => {
        entity => duck_type([qw[security_identity]]),
    },
    optional => {
        capability => 'Str',
        groupname => 'Str'
    }
);

sub revoke {
    my ($self, %params) = @_;

    my $opts = assert_profile(\%params)->valid;

    my $rule = Zaaksysteem::Object::SecurityRule->new($opts);

    $self->security_rules([
        grep { !$_->compare($rule) } $self->all_rules
    ]);

    return $self;
}

=head2 TO_JSON

This method extends the existing L<Zaaksysteem::Object/TO_JSON> method by
hydrating the security rules.

=cut

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $retval = $self->$orig(@_);

    $retval->{ security_rules } = $self->security_rules;

    return $retval;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

