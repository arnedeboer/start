package Zaaksysteem::Event::P5::TaskCreated;
use Moose;

with 'Zaaksysteem::Event::EventInterface', 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Event::P5::TaskCreated - "Task created" event handler

=cut

sub event_type { 'TaskCreated' }

sub emit_event {
    my $self = shift;

    $self->queue->emit_case_event(
        {
            case        => $self->case,
            event_name  => 'TaskCreated',
            description => 'Task created',
            changes     => $self->event->{changes},
            user_uuid   => $self->event->{user_uuid},
            entity_id   => $self->event->{entity_id},
            entity_type => 'task',
        }
    );

    return $self->new_ack_message('Event processed: dispatched as case event');
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
