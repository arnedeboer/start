=head1 Zaaksysteem Configuration

Every instance of Zaaksysteem requires a slew of configuration before use of
the system makes sense. The most basic configuration is done in a file called
C<zaaksysteem.conf>.

It will contain configuration for core components of the system, such as
authentication classes, LDAP connection info, Session config, and much more.

The use of this file for new configuration items is deprecated, because
maintenance is laborious and prone to errors.

=head2 Configuration file location

The configuration file can be placed as in C</etc/zaaksysteem/>, in case there
is a generic or one-off instance running on the machine. More configureably,
the filepath can be specified in the C<$ENV{ZAAKSYSTEEM_CONF}> variable.

If the above mentioned configuration files do not exist, L<Zaaksysteem>
will attempt to load a file C<etc/zaaksysteem.conf> relative to the directory
the instance was started in (so your C<pwd> at the time of instance start).

=head2 Structure

=head3 Main settings

    name Zaaksysteem

    saas_range             = 10.44.0.1\d+
    otap_ip                = 10.44.0.11
    invoke_assets_minified = 0
    libreoffice_version    = 3
    default_customer       = 10.44.0.11
    dev                    = 0

    google_api_key = "somekey"

=over 4

=item saas_range

This item should be set to a regex that matches all the IPs of machines in the
current SaaS platform range.

Example value: C<10.44.0.1\d+>

B<TODO>: explain fully how this is used etc.

=item otap_ip

Set this to the IP of the machine running the instance. (I<Why? I never touched
this variable, and even when switching my VM IP around it doesn't seem to break
anything>).

Example value: C<10.44.0.11>

=item invoke_assets_minified

This item influences how Zaaksysteem references frontend assets from within
templates. When set to a true-ish value, C<zaaksysteem.min.js> & friends will
be used when building the templates presented to the user.

Example value: C<0>

=item libreoffice_version

This variable defines the version to the systems headless LibreOffice instance.
The old value of C<2> has been deprecated.

Example value: C<3>

B<TODO>: Evict this variable, no environment of us runs LibreOffice version < 3
anymore.

=item default_customer

This option allows you to set a default fallback customer. So if you are on a
VM (like vagrant) this makes sure you can still get a consistent customer.d
config when approaching the machine via different IPs or hostnames.

Example value: C<10.44.0.11>

=item dev

This option currently dictates in an absolute fashion whether this instance of
Zaaksysteem is allowed at all to spood SAML requests. If set to a true-ish
value, it will allow IdP interfaces to set the "Spoofmode" implementation. For
more info, see L<Zaaksysteem::SAML2::Manual::Spoofmode>.

=item google_api_key

This item looks like it's no longer being used, or is in a broken state.

B<TODO>: Evaluate eviction of this item.

=back

=head3 Authentication

    <authentication>
        default_realm = zaaksysteem

        <realms>
            interface = authldap

            <credential>
                class = "+Zaaksysteem::Auth::Credential::LDAP"
            </credential>

            <store>
                class = "+Zaaksysteem::Auth::Store"
            </store>
        </realms>
    </authentication>

This block defines the classes and interfaces required to authenticate
employee-class users in Zaaksysteem. The configuration is at this point pretty
much set in stone, variations will likely produce a non-functioning login
process.

Note that this is configuration B<exclusively> meant to influence
employee-class users, and has nothing to do with DigiD, eHerkenning or other
authenticators that work on different facets of the system.

=over 4

=item default_realm

This is the name of the realm the users get authenticated in. At this point
all users get authenticated in the same realm, but this could be changed if
you wish to seperate 'employee-class' users into several categories.

=item interface

This item references the interface identifier to be used. It will be looked up
during startup of the Catalyst engines. It's not uncommon for a deployment of
Zaaksysteem to complain about this item not being found, usually it means
running a SQL data insertion on the interfaces table will fix the problems.

=item credential class

This item references the package name to be used as a Credential checker.

The default value, as seen above, will load our generic LDAP authenticator.

For more detailed info, see
L<Catalyst::Plugin::Authentication/Credential_Verifiers>.

=item store class

This item references the package name to be used as a Store for the users.

For more detailed info, see
L<Catalyst::Plugin::Authentication/Storage_Backends>.

=back

=head3 Plugin::Session

    <Plugin::Session>
        cookie_expires = 0
    </Plugin::Session>

Generic configuration for the L<Catalyst> L<session|Catalyst::Plugin::Session>
plugin.

=over 4

=item cookie_expires

Set to C<0>.

B<TODO>: Explain why.

=back

=head3 customers

This block contains a listing of all customers served by the Zaaksysteem
instance. It's rather complex, and as such, has it's own documentation at
L<Zaaksysteem::Manual::Config::Customer>.




__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

