=head1 NAME

Zaaksysteem::Manual::API::V1::Types::File::Lock - Type definition for
C<file/lock> objects.

=head1 DESCRIPTION

This page documents the serialization of C<file/lock> object instances.

=head1 JSON

=begin javascript

{
    "type": "file/lock",
    "reference": null,
    "instance": {
        "date_expires": "2009-02-13T01:58:31Z",
        "subject": {
            "type": "subject",
            "reference": "eba01343-b543-4525-9c52-a62ea4907b51",
            "instance": null
        }
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 date_expires E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Timestamp of the expiration date of the lock.

=head2 subject E<raquo> L<C<reference>|Zaaksysteem::Manual::API::V1::ValueTypes/reference>

Reference to a C<subject>, representing the owner of the lock.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
