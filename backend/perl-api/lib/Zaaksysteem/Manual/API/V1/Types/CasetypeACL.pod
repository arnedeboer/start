=head1 NAME

Zaaksysteem::Manual::API::V1::Types::CasetypeACL - Type definition for
casetype user permission objects

=head1 DESCRIPTION

This page documents the serialization for C<casetype_acl> objects.

=head1 JSON

=begin javascript

{
    "reference": null,
    "type": "casetype_acl"
    "instance": {
        "date_created": "2016-09-13T17:27:04Z",
        "date_modified": "2016-09-13T21:13:23Z",
        "casetype": {
            "type": "casetype",
            "reference": "961fb45e-0e14-4a95-bfb4-ffb43955c84c"
        },
        "casetype_active": true,
        "public": {
            "read": [ ... ]
        },
        "trusted": {
            "read": [ ... ]
        }
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 date_created E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Object creation timestamp.

=head2 date_modified E<raquo> L<C<datetime>|Zaaksysteem::Manual::API::V1::ValueTypes/datetime>

Object modification timestamp.

=head2 casetype E<raquo> L<C<reference>|Zaaksysteem::Manual::API::V1::ValueTypes/reference>

Reference to the casetype referenced by the ACL.

=head2 casetype_active E<raquo> L<C<boolean>|Zaaksysteem::Manual::API::V1::ValueTypes/boolean>

Indicates whether the referenced casetype is currently active.

=head2 public E<raquo> L<C<complex>|Zaaksysteem::Manual::API::V1::ValueTypes/complex>

Object containing the names of users who have capabilities on cases
permissions on cases of the referenced casetype.

=head2 trusted E<raquo> L<C<complex>|Zaaksysteem::Manual::API::V1::ValueTypes/complex>

Object containing the names of users who have capabilities on cases of
the referenced casetype, when the case is confidential.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
