=encoding UTF-8

=head1 NAME

Zaaksysteem::Manual::API::V1::LegalEntityTypes - Legal entity type retreival

=head1 Description

Get legal entity types as defined by Zaaksysteem


=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/general/legal_entity_types

Make sure you use the HTTP Method C<GET> for retrieving.
You need to be logged in to Zaaksysteem to speak to this API.

=head1 Retrieve data

=head2 List all legal entity types

   /api/v1/general/legal_entity_types

This API differs from most API's as the entity do not have a UUID, they are not stored in our database.
In other API's paging is set in stone, this API endpoint does allow it by use of a paging attribute.
You can retrieve all entries by upping the paging attribute to 50. At time of writing there are 47 legal
entity types.

    /api/v1/general/legal_entity_types/?paging=50

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab_sprint-bcc00d-a79ffc",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : "https://sprint.zaaksysteem.nl/api/v1/general/legal_entity_types/?page=2",
            "page" : 1,
            "pages" : 5,
            "prev" : null,
            "rows" : 10,
            "total_rows" : 47
         },
         "rows" : [
            {
               "instance" : {
                  "code" : "01",
                  "description" : null,
                  "label" : "Eenmanszaak"
               },
               "reference" : null,
               "type" : "legal_entity_type"
            },
            {
               "instance" : {
                  "code" : "02",
                  "description" : null,
                  "label" : "Eenmanszaak met meer dan één eigenaar"
               },
               "reference" : null,
               "type" : "legal_entity_type"
            }
            // Snipped for brevity
         ]
      }
   }
}

=end javascript

=head2 get

   /api/v1/general/legal_entity_type/01

In our API you should only retrieve objects based on their UUID, the 'reference' as found in the JSON. Because legal entity types do no have a UUID, you cannot get one by using a UUID.
You can however get one by using the 'code' found in the JSON response. You are however not advised to use this, as we may change this behaviour in the future.

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab_sprint-bcc00d-6888e2",
   "development" : false,
   "result" : {
      "instance" : {
         "code" : "01",
         "description" : null,
         "label" : "Eenmanszaak"
      },
      "reference" : null,
      "type" : "legal_entity_type"
   },
   "status_code" : 200
}

=end javascript

=head1 Mutate data

Mutations for legal entity types are not supported at this moment.

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::General::LegelEntityTypes>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
