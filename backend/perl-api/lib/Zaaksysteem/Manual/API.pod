=head1 NAME

Zaaksysteem::Manual::API - Central documentation about the zaaksysteem.nl API

=head1 Description

Welcome to the documentation space of our API. Via this document we would like to give you a basic
understanding about our API, but also give you an idea about what you could expect from zaaksysteem.nl

Make sure you read the information below.

=head1 Versions

Development on Zaaksysteem is a continious process. Features are developed every day, which means
implementations can change.

To make sure your API will always work as expected, our API is versioned. What means we won't break the
format of our API or remove default attributes from our JSON response. We can, however, add attributes to the
response.

Zaaksysteem current version:

L<Zaaksysteem::Manual::API::V1> - B<Version 1>

=head2 Support

Because agile development is our vision, we aim not to keep every version supported until the end of days. But
to prevent you from upgrading your software every time we release a new zaaksysteem.nl, we will support the
latest two stable API versions. Make sure you have an upgrade path ready when new versions arrive.

To prove our version implementation still works with the latest Zaaksysteem, we create extensive
test scripts which test our API against the latest Zaaksysteem Core. You can find this script
in the following location:

  t/lib/TestFor/Catalyst/Controller/API/V1/<component>

Please make sure you develop within the scope of this testscript, this way your implementation will
work after every zaaksysteem release.

Every use of our API outside this scope is unsupported: B<You have been warned>

=head1 See also

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API::V1>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
