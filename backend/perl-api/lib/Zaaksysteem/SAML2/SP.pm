package Zaaksysteem::SAML2::SP;

use Zaaksysteem::Moose;

extends 'Net::SAML2::SP';

use Zaaksysteem::Constants qw(SAML_TYPE_EIDAS SAML_TYPE_KPN_LO);
use Net::SAML2::SP;
use URN::OASIS::SAML2 qw(:bindings);

has key_fh => (
    isa      => 'FileHandle',
    required => 1,
    is       => 'ro',
);

has cert_fh => (
    isa      => 'FileHandle',
    required => 1,
    is       => 'ro',
);

has interface => (
    required => 1,
    is => 'ro',
);

=head1 SAML2::SP

This class wraps L<Net::SAML2::SP> for a bit nicer integration with
L<Zaaksysteem>.

=head1 Example usage

    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        $my_interface_object
    );

    my $metadata = $sp->metadata;

=head1 Methods

=head2 new_from_interface

This methods wraps the logic of building a SP object from an interface
definition. Works like the normal C<new> function, except that it requires
an interface object be supplied as the first argument. Remaining parameters
are interpreted as a hash passed directly to C<new>.

=head3 interface

An L<Zaaksysteem::Backend::Sysin::Interface::Component> instance.

=cut

define_profile new_from_interface => (
    required => {
        interface => 'Zaaksysteem::Model::DB::Interface',
        idp       => 'Zaaksysteem::Model::DB::Interface',
    },
);

sub new_from_interface {
    my ($class, %params) = @_;

    my $valid = assert_profile(\%params)->valid;

    my $sp_interface = $valid->{interface};
    my $sp_config    = $sp_interface->get_interface_config;
    my $schema       = $sp_interface->result_source->schema;

    my $idp          =  $valid->{idp};

    my $cid = $idp->jpath('$.idp_cert[0].id') // $sp_interface->jpath('$.sp_cert[0].id');
    unless ($cid) {
        throw('saml2/sp/certificate',
            'No certificated configured in either SP/IdP');
    }

    my $cert = $schema->resultset('Filestore')->find($cid);
    unless ($cert) {
        throw('saml2/sp/certificate',
            'Certificate configured for SP not found in filestore.');
    }

    my $contact;
    if (my $contact_data = $sp_config->{contact_id}) {
        $contact = $schema->betrokkene_model->get(
            {type => $contact_data->{object_type}},
            $contact_data->{id},
        );
    }

    my $config = $idp->get_interface_config();
    my $uri = $sp_config->{sp_webservice};
    $uri = URI->new($uri);
    if ($uri->path ne '/auth/saml') {
        $uri->path('/auth/saml');
    }

    my @slo = (
        {
            Binding => BINDING_HTTP_REDIRECT,
            Location => $uri . '/sls-redirect',
        }

    );

    if ($config->{use_saml_slo}) {
        push(
            @slo,
            {
                Binding  => BINDING_SOAP,
                Location => $uri . '/sls-soap'
            }
        );
    }

    my $binding = $config->{binding} // BINDING_HTTP_ARTIFACT;

    my @acs = (
        {
            Binding  => BINDING_HTTP_ARTIFACT,
            Location => $uri . '/consumer-artifact',
            index    => 1,
            $binding eq BINDING_HTTP_ARTIFACT ? (isDefault => 1) : (isDefault => 0)
        },
        {
            Binding  => BINDING_HTTP_POST,
            Location => $uri . '/consumer-post',
            index    => 2,
            $binding eq BINDING_HTTP_POST ? (isDefault => 1) : (isDefault => 0)
        },
    );

    # Perhaps remove after live testing with ADFS and binding stuff
    if (($config->{saml_type} //'') eq 'adfs') {
        $acs[0]->{index} = 2;
        $acs[0]->{isDefault} = 0;
        $acs[1]->{index} = 1;
        $acs[1]->{isDefault} = 1;
    }

    my $id = $config->{idp_entity_id};
    if (!defined $id || !length($id)) {
        $id = "$uri";
    };

    my $fh = $cert->get_path;

    my %new_args = (
        id               => $id,
        url              => $uri,

        single_logout_service => \@slo,
        assertion_consumer_service => \@acs,

        cacert           => '',
        org_name         => $sp_config->{sp_application_name},
        org_display_name => $sp_config->{sp_application_name},
        org_contact      => ($contact ? $contact->email : ''),

        error_url        => '/error',

        key              => "$fh",
        cert             => "$fh",

        key_fh  => $fh,
        cert_fh => $fh,

        # I don't think we really require this further?
        interface        => $sp_interface,
        idp              => $idp,
    );

    return $class->new(%new_args);
}

# Our descriptor ID is always going to the the interface UUID

sub generate_sp_desciptor_id {
    my $self = shift;
    return $self->interface->uuid;
}

sub signed_metadata {
    my $self = shift;
    return $self->metadata(@_);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2022, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
