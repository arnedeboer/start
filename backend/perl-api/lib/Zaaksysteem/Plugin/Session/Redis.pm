package Zaaksysteem::Plugin::Session::Redis;
use Moose;
use MooseX::NonMoose;
use namespace::autoclean;

extends qw/Catalyst::Plugin::Session::Store/;

use IO::Socket::SSL qw(SSL_VERIFY_PEER);
use Zaaksysteem::StatsD;
use Zaaksysteem::Store::Redis;

=head1 NAME

Zaaksysteem::Plugin::Session::Redis - Redis session store using Sereal

=head1 DESCRIPTION

Catalyst session store plugin, for storing session data in Redis, serialized
using Sereal.

=cut

use Sereal::Encoder qw(SRL_SNAPPY);
use Sereal::Decoder;
use MIME::Base64 qw(encode_base64 decode_base64);
use Redis;
use Try::Tiny;
use Zaaksysteem::StatsD;

has _redis_connection => (
    is        => 'rw',
    isa       => 'Zaaksysteem::Store::Redis',
    predicate => 'has_session_redis',
    clearer   => 'clear_session_redis',
    builder   => '_build_redis_connection',
);

=head2 get_session_data

Retrieve session data from Redis and decode it.

=cut

sub get_session_data {
    my ($c, $key) = @_;

    $c->_ensure_redis_connection;


    my $data;
    if (my ($sid) = $key =~ /^expires:(.*)/) {
        $data = $c->_redis_connection->get($key);
    }
    else {
        my $t0 = Zaaksysteem::StatsD->statsd->start;
        $data = $c->_redis_connection->get_sereal($key);
        Zaaksysteem::StatsD->statsd->end('session_database_read_duration', $t0);
        Zaaksysteem::StatsD->statsd->increment('session_database_read_number', 1);
    }


    return $data;
}

=head2 store_session_data

Save session data by encoding it using Sereal and storing in Redis.

=cut


sub store_session_data {
    my ($c, $key, $data) = @_;

    $c->_ensure_redis_connection;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $exp = $c->session_expires;

    if (my ($sid) = $key =~ /^expires:(.*)/) {
        $c->_redis_connection->set($key, $data);
        $c->_redis_connection->expire_at($key, $exp);
    }
    else {
        $c->_redis_connection->set_sereal($key, $data);
        $c->_redis_connection->expire_at($key, $exp);

        $c->_redis_connection->set_json("json:$key", $data);
        $c->_redis_connection->expire_at("json:$key", $exp);
    }

    Zaaksysteem::StatsD->statsd->end('session_database_write_duration', $t0);
    Zaaksysteem::StatsD->statsd->increment('session_database_write_number', 1);

    return;
}

=head2 delete_session_data

Delete a specific session from Redis.

=cut

sub delete_session_data {
    my ($c, $key) = @_;

    $c->_ensure_redis_connection;

    $c->_redis_connection->del($key);
    $c->_redis_connection->del("json:$key");

    return;
}

=head2 delete_session_data

No-op, Redis does its own expiry.

=cut

sub delete_expired_sessions {
    # Redis does its own expiry
}

sub setup_session {
    my ($c) = @_;

    $c->maybe::next::method(@_);
}

=head2 _ensure_redis_connection

Ensures a Redis connection is available and ready for use.

=cut

sub _ensure_redis_connection {
    my ($c) = @_;

    my $cfg = $c->_session_plugin_config;

    try {
        $c->_redis_connection->has_connection;
    } catch {
        $c->clear_session_redis;
        $c->_redis_connection;
    };
}

sub _build_redis_connection {
    my $c = shift;

    my $cfg = $c->_session_plugin_config;
    my %args;
    foreach (qw(server debug reconnect db)) {
        next unless exists $cfg->{'redis_' . $_};
        $args{$_} = $cfg->{'redis_' . $_};
    }

    if ($cfg->{'redis_ssl'}) {
        $args{ssl} = 1;
        $args{SSL_verify_mode} = SSL_VERIFY_PEER;
    }

    my $t0 = Zaaksysteem::StatsD->statsd->start;
    my $connection = Zaaksysteem::Store::Redis->new(%args);
    Zaaksysteem::StatsD->statsd->end('session_database_connect_duration', $t0);
    Zaaksysteem::StatsD->statsd->increment('session_database_connect_number', 1);

    return $connection;
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
