package Zaaksysteem::SAML2;
use Zaaksysteem::Moose;

use v5.24;

=head1 NAME

Zaaksysteem::SAML2 - SAML2 implementation of Zaaksysteem

=head1 SYNOPSIS

    my $saml = Zaaksysteem::SAML2->new();


=head1 DESCRIPTION

=cut


use Moose::Util::TypeConstraints;
use MIME::Base64 qw(decode_base64);

use Net::SAML2::Binding::Redirect;
use Net::SAML2::Protocol::ArtifactResolve;
use Net::SAML2::Protocol::Assertion;
use Net::SAML2::Protocol::AuthnRequest;
use Net::SAML2::Protocol::LogoutRequest;
use Net::SAML2::Protocol::LogoutResponse;

use List::Util qw(any first);
use BTTW::Tools::UA qw(new_user_agent);

use URN::OASIS::SAML2 qw(:bindings :status :urn);

use Zaaksysteem::Constants ':SAML_TYPES';
use Zaaksysteem::Types qw(NonEmptyStr SAMLNameIDFormat);

use Zaaksysteem::SAML2::SP;
use Zaaksysteem::SAML2::IdP;

use Zaaksysteem::SAML2::Binding::SOAP;
class_type('Zaaksysteem::SAML2::IdP');
class_type('Zaaksysteem::SAML2::SP');
class_type('Zaaksysteem::SAML2::Spoof');

has context_id => (
    is  => 'rw',
    isa => 'Str',
);

has sp => (
    is  => 'ro',
    isa => 'Zaaksysteem::SAML2::SP|Zaaksysteem::SAML2::Spoof',
);

has idp => (
    is  => 'ro',
    isa => 'Zaaksysteem::SAML2::IdP|Zaaksysteem::SAML2::Spoof',
);

has uri => (is => 'ro');

has authenticated_identifier => (
    is      => 'rw',
    isa     => 'HashRef',
);

has authenticated_assertion => (
    is      => 'rw',
    isa     => 'Net::SAML2::Protocol::Assertion',
);

has auth_protocol => (
    is        => 'rw',
    isa       => 'Str',
    predicate => 'has_auth_protocol',
);

has response => (
    is        => 'rw',
    isa       => 'Str',
    init_arg  => undef,
    writer    => '_set_response',
);

=head1 CONSTRUCTORS

=head2 new_from_interfaces

Builds a new SAML2 instance with information derived from one or two
L<Zaaksysteem::Backend::Sysin::Interface> definitions. Supplying the
C<sp> parameter is optional, as it can be found using the IdP provided
in C<idp>.

=cut

define_profile new_from_interfaces => (
    required => { idp => 'Zaaksysteem::Model::DB::Interface' },
    optional => { sp  => 'Zaaksysteem::Model::DB::Interface' }
);

sub new_from_interfaces {
    my ($class, %params) = @_;
    my $opts  = assert_profile(\%params)->valid;

    if($opts->{idp}->jpath('$.saml_type') eq 'spoof') {
        require Zaaksysteem::SAML2::Spoof;
        my $sp = Zaaksysteem::SAML2::Spoof->new_from_interface(
            interface => $opts->{sp}
        );
        my $idp = Zaaksysteem::SAML2::Spoof->new_from_interface(
            interface => $opts->{idp}
        );

        my $idp_entity_id = $opts->{ idp }->jpath('$.idp_entity_id');
        $sp->id($idp_entity_id) if $idp_entity_id;
        return $class->new(sp => $sp, idp => $idp);
    }

    unless($opts->{ sp }) {
        my $schema = $opts->{ idp }->result_source->schema;
        my $interfaces = $schema->resultset('Interface');

        ($opts->{ sp }) = $interfaces->find_by_module_name('samlsp');
    }

    my $idp = Zaaksysteem::SAML2::IdP->new_from_interface(
        interface => $opts->{idp}
    );
    my $sp = Zaaksysteem::SAML2::SP->new_from_interface(
        interface => $opts->{sp},
        idp       => $opts->{idp},
    );

    return $class->new(sp => $sp, idp => $idp);
}

=head1 Methods

=head2 handle_response($artifact_resolved_xml)

Return value: L<Net::SAML2::Protocol::Assertion> or error on failure

=cut

sub handle_response {
    my $self   = shift;
    my $params = shift;

    # Artifact binding
    if (my $artifact = $params->{SAMLart}) {
        return $self->handle_artifact_response($artifact);
    }

    # POST BINDING
    if (my $response = $params->{SAMLResponse}) {
        return $self->handle_post_response($response);
    }

    croak("Not a POST or ARTIFACT binding, please check your configuration");
}

sub handle_post_response {
    my $self     = shift;
    return $self->_handle_response(decode_base64(shift));
}

sub handle_artifact_response {
    my $self = shift;
    return $self->_handle_response($self->resolve_artifact(shift));
}

sub _handle_response {
    my $self = shift;
    my $xml  = shift;

    $self->_log_xml("Incoming SAML authentication response XML", $xml);

    my $assertion = $self->_verify_saml_response_status($xml);
    # TODO: Rename this function
    $self->_load_identifiers_from_assertion($assertion);
}

=head2 resolve_artifact

Contacts the IdP using the given SAML Artificat

=cut

sub resolve_artifact {
    my $self     = shift;
    my $artifact = shift;

    my $resolve = Net::SAML2::Protocol::ArtifactResolve->new(
        artifact    => $artifact,
        issuer      => $self->sp->id,
        destination => $self->idp->art_url(BINDING_SOAP),
    );

    my $binding = Zaaksysteem::SAML2::Binding::SOAP->new(
        url      => $resolve->destination,
        key      => $self->sp->cert,
        cert     => $self->sp->cert,
        idp_cert => $self->idp->cert('signing'),

        #cacert   => $self->idp->cacert
        #  ? $self->idp->cacert
        #  : $self->sp->cert,
        #
        verify  => { ns => 'urn:oasis:names:tc:SAML:2.0:protocol', attr_id => 'ArtifactResponse[@ID]' },

    );

    my $xml = $resolve->as_xml;
    $self->_log_xml("Resolve artifact XML", $xml);
    return $binding->request($xml);
}

sub _verify_saml_response_status {
    my $self = shift;
    my $xml  = shift;

    my $assertion;
    try {
        $assertion = Net::SAML2::Protocol::Assertion->new_from_xml(
            xml      => $xml,
            key_file => $self->sp->cert,
            key_name => $self->sp->key_name('signing'),
        );

    }
    catch {
        $self->log->info("$_");
        throw(
            'saml2/handle_response/assertion/failed',
            'Unable to parse assertion'
        );
    };

    return $assertion if $assertion->success;

    $self->_log_xml("SAML status did not return success", $xml);
    my $status = $assertion->response_status;
    my $substatus = $assertion->response_substatus // $status;

    throw(
        'saml2/handle_response/invalid_status',
        "Invalid status received from SOAP Response: $substatus",
        {
            topstatus => $status,
            status    => $substatus,
        }
    );
}

sub _load_identifiers_from_assertion {
    my $self      = shift;
    my $assertion = shift;

    my $authenticated_identifier = try {
        return $self->_load_authenticated_identifier($assertion);
    }
    catch {
        my $object = $_->object || {};
        throw(
            $_->type,
            $_->message,
            {
                %$object,
            }
        );
    };

    $self->authenticated_assertion($assertion);
    $self->authenticated_identifier($authenticated_identifier);
    return $self->authenticated_assertion
}

sub _load_authenticated_identifier {
    my ($self, $assertion) = @_;

    return unless $assertion;

    my $config = $self->idp->interface->get_interface_config;
    my $saml_type = $config->{saml_type};

    my %identifier = (
        used_profile  => $saml_type,
        success       => 1,

        nameid        => $assertion->nameid //'',
        nameid_format => $assertion->nameid_format //'',
        session_index => $assertion->session // '',
    );

    if ($saml_type eq SAML_TYPE_LOGIUS) {
        no warnings qw(misc);
        # Some entities send us wrong data, we get warning here when they do,
        # silence it
        my %namespec = split m[:], $assertion->nameid;
        use warnings;

        # Sectorcode for BSN
        $identifier{uid} = $namespec{s00000000} if exists $namespec{s00000000};
        die "No BSN found in SAML assertion" unless defined $identifier{uid};
    }
    elsif ($saml_type eq SAML_TYPE_KPN_LO) {

        $identifier{uid} = $self->_get_eherkenning_identifier($assertion->{attributes});
    }
    elsif ($saml_type eq SAML_TYPE_EIDAS) {

        %identifier
            = (%identifier, %{ $self->_get_eidas_identifier($assertion) });

    }
    elsif ($saml_type eq SAML_TYPE_ADFS) {
        my %attributes = $self->_extract_adfs_attributes($assertion, $config);
        @identifier{ keys %attributes } = values %attributes;
    }
    elsif ($saml_type eq SAML_TYPE_MINIMAL) {
        my $email;
        if ($identifier{nameid} =~ /\@/) {
            $identifier{email} = $identifier{nameid};
            $identifier{uid}   = ($identifier{nameid} =~ s/\@.*$//r);
        }
        else {
            $identifier{email} = undef;
            $identifier{uid}   = $identifier{nameid};
        }
    }
    else {
        throw("saml2/saml_type/unkown", "Unknown SAML type: $saml_type");
    }

    return \%identifier;
}

=head2 authentication_redirect

Generates a authnrequest and returns the redirect url for use

=cut

define_profile authentication_redirect => (
    required    => [],
    optional    => [qw[relaystate]],
);

sub authentication_redirect {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;


    my $interface_config = $self->idp->interface->get_interface_config;
    my $saml_type = $interface_config->{saml_type};

    if ($saml_type eq SAML_TYPE_SPOOF) {
        return '/auth/saml/prepare-spoof';
    }

    my $level
        = $self->has_auth_protocol
        ? $self->auth_protocol
        : $interface_config->{authentication_level};

    my $identity_providers;
    if ($interface_config->{saml_type} eq SAML_TYPE_EIDAS && $interface_config->{idp_scoping}) {
        $identity_providers = [ $interface_config->{idp_scoping} ]
    }

    my $auth_request = try {
        return Net::SAML2::Protocol::AuthnRequest->new(
            issuer      => $self->resolve_entity_id,
            destination => $self->idp->sso_url(BINDING_HTTP_REDIRECT),

            $level
            ? (
                RequestedAuthnContext_Comparison => 'minimum',
                AuthnContextClassRef             => [$level],
                )
            : (),
            #
            # Why do we need to communicate this in the auth request? Our
            # metadata is rather specific in our needs and what we support.
            #
            $self->_get_service_identifier($saml_type, $interface_config),
            $identity_providers
            ? (identity_providers => $identity_providers)
            : (),
        );
    }
    catch {
        $self->log->info($_);
        throw('saml2/authn_request',
              'Unable to construct AuthnRequest, intercepted error: ' . $_);
    };

    my $xml = $auth_request->as_xml;
    $self->_log_xml("Outgoing SAML AuthnRequest XML", $xml);

    my $redirect = Net::SAML2::Binding::Redirect->new(
        cert     => $self->sp->cert,
        key      => $self->sp->key,
        url      => $auth_request->destination,
        sig_hash => $self->signature_algorithm,
    );

    return $redirect->sign($xml, $opts->{relaystate})
}

sub _get_service_identifier {
    my ($self, $type, $config) = @_;

    my $preferred = $self->sp->get_default_assertion_service;

    if ($type eq SAML_TYPE_LOGIUS) {
        # We want to use the index, but that requires regenerating all the
        # digid metadata stuf :/
        return (assertion_url   => $preferred->{Location});
        return (assertion_index => $preferred->{index});
    }
    if (any { $type eq $_ } (SAML_TYPE_KPN_LO, SAML_TYPE_EIDAS)) {
        return (
            assertion_index => $preferred->{index},
            attribute_index => $config->{idp_acs_index} || 0,
        );
    }
    if (any { $type eq $_ } (SAML_TYPE_ADFS, SAML_TYPE_MINIMAL)) {
        return (assertion_url => $preferred->{Location});
    }

    throw('saml2/authn_request', "Unrecognized SAML type: $type");

}

=head2 handle_logout_request

Handle a SAML LogoutRequest message: IdP initiated logout

=cut

define_profile handle_logout_request => (
    required => {
        query_string => NonEmptyStr,
        nameid => NonEmptyStr,
        session => NonEmptyStr,
    },
    optional => {
        nameid_format => SAMLNameIDFormat,
    }
);

sub handle_logout_request {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $binding = Net::SAML2::Binding::Redirect->new(
        param => 'SAMLRequest',
        cert => $self->idp->cert('signing'),
    );

    my ($result, $relaystate) = $binding->verify($opts->{query_string});

    $self->_log_xml("Incoming SAML Logout Request XML", $result);

    my $request = Net::SAML2::Protocol::LogoutRequest->new_from_xml(
        xml => $result
    );

    my $logout_status = STATUS_SUCCESS;
    for my $key (qw(nameid nameid_format session)) {
        if ($request->$key ne $opts->{$key}) {
            my $message = sprintf(
                "Logout request failed: '%s' = '%s', session has '%s'",
                $key,
                $request->$key,
                $opts->{$key},
            );
            $self->log->debug($message);

            $logout_status = STATUS_REQUESTER;
        } else {
            $self->log->trace("Logout request: $key matches");
        }
    }

    my $response = Net::SAML2::Protocol::LogoutResponse->new(
        issuer      => $self->resolve_entity_id,
        destination => $self->idp->slo_url('urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'),
        status      => $logout_status,
        response_to => $request->id,
    );

    my $xml = $response->as_xml;
    $self->_log_xml("Outgoing SAML Logout Response XML", $xml);

    my $redirect = Net::SAML2::Binding::Redirect->new(
        key                 => $self->sp->cert,
        url                 => $response->destination,
        signature_algorithm => $self->signature_algorithm,
        param               => 'SAMLResponse',
    );


    return (
        1,
        $redirect->sign(request => "$xml")
    );
}

=head2 handle_logout_response

Handle a SAML LogoutResponse message: response to SP-initiated logout

=cut

sub handle_logout_response {
    my ($self, $query) = @_;

    my $binding = Net::SAML2::Binding::Redirect->new(
        param => 'SAMLResponse',
        cert => $self->idp->cert('signing'),
    );

    my ($result, $relaystate) = $binding->verify($query);

    $self->_log_xml("Received SAML Logout Response XML:", $result);

    my $response = Net::SAML2::Protocol::LogoutResponse->new_from_xml(
        xml => $result
    );

    return $response if $response->success;

    my $status = $response->status;
    $self->log->error("SAML Logout failed: $status");
    throw(
        'saml2/logout_failure',
        "SAML2 logout failed: $status",
        {
            status => $status,
        }
    );
}

=head2 logout_redirect

Return a redirection URL for the SingleLogoutService on the IdP for the
session, nameid specified.

=cut

define_profile logout_redirect => (
    required => {
        nameid => NonEmptyStr,
        session_index => NonEmptyStr,
    },
    optional => {
        nameid_format => SAMLNameIDFormat,
    }
);

sub logout_redirect {
    my ($self, %params) = @_;
    my $opts = assert_profile(\%params)->valid;

    my $logout_request = Net::SAML2::Protocol::LogoutRequest->new(
        issuer        => $self->resolve_entity_id,
        destination   => $self->idp->slo_url(BINDING_HTTP_REDIRECT),
        nameid        => $opts->{nameid},
        $opts->{nameid_format} ? (
            nameid_format => $opts->{nameid_format}
        ) : (),
        session       => $opts->{session_index},
    );

    my $redirect = Net::SAML2::Binding::Redirect->new(
        cert     => $self->sp->cert,
        key      => $self->sp->key,
        url      => $logout_request->destination,
        sig_hash => $self->signature_algorithm,
    );

    my $xml = $logout_request->as_xml;

    $self->_log_xml("Outgoing SAML Logout Request XML", $xml);

    return $redirect->sign($xml);
}

sub _get_soap_binding {
    my $self = shift;
    my $binding = Zaaksysteem::SAML2::Binding::SOAP->new(
        key      => $self->sp->cert,
        cert     => $self->sp->cert,
        idp_cert => $self->idp->cert('signing'),
        $self->idp->_cacert_fh
            ? (cacert => $self->idp->_cacert_fh)
            : (),
    );
    return $binding;

}

sub handle_single_logout_request {
    my ($self, $xml) = @_;

    $self->_ssl_verify_xml($xml);

    my $binding = $self->_get_soap_binding();

    my @result = $binding->handle_response($xml);

    return \@result;
}

sub generate_logout_response {
    my $self = shift;

    my $logout_status = STATUS_SUCCESS;

    my $response = Net::SAML2::Protocol::LogoutResponse->new(
        issuer      => $self->resolve_entity_id,
        destination => "",
        status      => $logout_status,
        response_to => $self->context_id,
    );

    my $binding = $self->_get_soap_binding();
    return $binding->create_soap_envelope($response->as_xml);
}

sub _log_xml {
    my ($self, $msg, $xml) = @_;

    my $id = $self->context_id // '<unknown context id>';

    $self->idp->interface->process_trigger(
        'log_saml_xml',
        {
            context_id => $id,
            message    => $msg,
            xml        => $xml,
        }
    );

    if ($self->log->is_trace) {
        $self->log->trace($msg);
        while ($xml =~ m/(.{0,500})/g) {
            next unless length($1);
            next unless defined($1);
            $self->log->trace("$1");
        }
    }
    return 1;
}

=head2 signature_algorithm

Returns the propper signature_algorithm for a integration.

=cut

sub signature_algorithm {
    my $self = shift;

    my $type = $self->idp->interface->jpath('$.saml_type');

    if($type eq SAML_TYPE_KPN_LO || $type eq SAML_TYPE_ADFS) {
        return 'sha256';
    }

    if ($type eq SAML_TYPE_EIDAS) {
        return $self->idp->interface->jpath('signature_algorithm');
    }

    return 'sha1';
}

=head2 resolve_entity_id

Getter for the configured entity_id. By default this will return the id
configured on the SP, which defaults to a URI that identifies it. The
returned id can be overridden by specifying an idp_entity_id in the IdP
configuration

=cut

sub resolve_entity_id {
    my $self = shift;

    my $config = $self->idp->interface->get_interface_config;
    my $idp_entity_id = $config->{idp_entity_id};

    unless (defined($idp_entity_id) && length($idp_entity_id)) {
        $idp_entity_id = $self->sp->id;
    }

    return $idp_entity_id;
}


sub _get_eidas_identifier {
    my ($self, $assertion) = @_;

    my %mapping = (
        date_of_birth     => 'urn:etoegang:1.9:attribute:DateOfBirth',
        surname           => 'urn:etoegang:1.9:attribute:FamilyName',
        firstname         => 'urn:etoegang:1.9:attribute:FirstName',
        acting_subject_id => 'urn:etoegang:core:ActingSubjectID',
    );

    my %id = map { $_ => $assertion->{attributes}{$mapping{$_}}[0] } keys %mapping;
    return \%id;

}

sub _get_eherkenning_identifier_from_version {
    my ($self, $attributes) = @_;

    my %versions = (
        '1.09' => {
            coc_number          => 'urn:etoegang:1.9:EntityConcernedID:KvKnr',
            coc_location_number => 'urn:etoegang:1.9:ServiceRestriction:Vestigingsnr',
        },
        '1.13' => {
            coc_number          => 'urn:etoegang:core:LegalSubjectID',
            coc_location_number => 'urn:etoegang:1.9:ServiceRestriction:Vestigingsnr',
        }
    );

    my ($kvk, $location);
    foreach my $v (sort keys %versions) {

        my $attr = $versions{$v}{coc_number};
        next if !exists $attributes->{$attr};
        $kvk = $attributes->{$attr}[0];

        $attr = $versions{$v}{coc_location_number};

        if (exists $attributes->{$attr}) {
            $location = $attributes->{$attr}[0];
        }
        return ($kvk, $location);
    }
    return;

}

sub _get_eherkenning_identifier {
    my ($self, $attributes) = @_;

    my ($kvk, $location) = $self->_get_eherkenning_identifier_from_version($attributes);

    throw('saml2/eherkenning',
        "Unable to find chamber of commerce number in SAML response")
        unless $kvk;

    $kvk = sprintf("%08d", $kvk); # force 8 digits
    $location = sprintf("%012d", $location) if $location; # force 12 digits

    return join("", $kvk, $location // '');
}

sub _extract_adfs_attributes {
    my ($self, $assertion, $config) = @_;

    $config //= {};

    my $msbase = 'http://schemas.microsoft.com/ws/2008/06/identity/claims';
    my $soapbase = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims';

    my %required_attributes = (
        email     => "$soapbase/emailaddress",
        name      => "$soapbase/name",
        givenname => "$soapbase/givenname",
        surname   => "$soapbase/surname",
    );

    # If 'use_nameid' is set, the 'nameid' in the assertion is used as the user id,
    # instead of looking for an attribute.
    if (! $config->{use_nameid}) {
        if ($config->{use_upn}) {
            $required_attributes{uid} = "$soapbase/upn"
        } else {
            $required_attributes{uid} = "$msbase/windowsaccountname";
        }
    }

    my %optional_attributes = (
        initials => "$soapbase/initials",
        phone    => "$soapbase/otherphone",
        title    => "$soapbase/title",
    );

    my %extracted_attributes;
    while (my ($attr, $name) = each %required_attributes) {
        if (!exists $assertion->{attributes}{ $name }) {
            throw(
                'saml/entity_id',
                sprintf(
                    "Required attribute '%s' is missing from assertion.",
                    $name,
                ),
            );
        }

        $extracted_attributes{$attr} = $assertion->{attributes}{ $name }[0];
    }

    while (my ($attr, $name) = each %optional_attributes) {
        next unless exists $assertion->{attributes}{ $name };
        $extracted_attributes{$attr} = $assertion->{attributes}{ $name }[0];
    }

    if ($config->{use_nameid}) {
        $extracted_attributes{uid} = $assertion->nameid;
    }

    return %extracted_attributes;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2023 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
