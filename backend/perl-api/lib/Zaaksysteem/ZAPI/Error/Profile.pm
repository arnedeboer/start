package Zaaksysteem::ZAPI::Error::Profile;

use Moose::Role;

use Zaaksysteem::Constants qw/PARAMS_PROFILE_DEFAULT_MSGS/;

use constant ZAPI_PARAM_CAT     => 'ZAPIProfile';

sub BUILD {
    my $self            = shift;

    $self->_generate_profile_data;
}

sub _load_profile_error {
    my $self            = shift;
    my $error           = shift;

    if (
        !UNIVERSAL::isa($error, 'Data::FormValidator::Results')
    ) {
        die('Not a valid Data::FormValidator::Results object given');
    }

    $error->{profile}{msgs} = PARAMS_PROFILE_DEFAULT_MSGS;

    my (@rv, @message);
    for my $param (
        $error->missing,
        $error->invalid,
    ) {
        my $result      = 'invalid';
        $result         = 'missing' if $error->missing($param);

        push(@rv,
            {
                parameter   => $param,
                result      => $result,
                message     => $error->msgs->{$param},
            }
        );

        push(
            @message,
            'Invalid field "' . $param . '": ' .
                $error->msgs->{$param}
        );
    }

    $self->messages(\@message);
    $self->data(\@rv);
    $self->type('params/profile');
}

### When this is a data::formvalidator object, make sure we TO_JSON it.
sub _generate_profile_data {
    my $self    = shift;

    return unless (
        UNIVERSAL::isa($self->data, 'Data::FormValidator::Results')
    );

    return unless (
        $self->type eq ZAPI_PARAM_CAT ||
        $self->type eq 'params/profile'
    );

    $self->_load_profile_error($self->data);
}

before '_validate_response' => sub {
    my $self    = shift;

    $self->_generate_profile_data;
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 PARAMS_PROFILE_DEFAULT_MSGS

TODO: Fix the POD

=cut

=head2 ZAPI_PARAM_CAT

TODO: Fix the POD

=cut

