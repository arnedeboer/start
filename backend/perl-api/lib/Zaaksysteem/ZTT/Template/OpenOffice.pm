package Zaaksysteem::ZTT::Template::OpenOffice;

use Moose;

use Zaaksysteem::ZTT;
use Zaaksysteem::ZTT::Tag;
use Zaaksysteem::ZTT::MagicDirective;
use Zaaksysteem::ZTT::Selection::OpenOffice;

use URI;
use OpenOffice::OODoc;

extends 'Zaaksysteem::ZTT::Template';

# with ( ... ) is at the bottom of the file
# From Moose::Manual::Roles - Required Methods Provided by Attributes:
#
# you must define the attribute before consuming the role, or else the role will
# not see the generated accessor. --- In general, we recommend that you always
# consume roles after declaring all your attributes.

=head1 NAME

Zaaksysteem::ZTT::Template::OpenOffice - Extend template class with specific
implementation details for OpenOffice documents.

=head1 ATTRIBUTES

=head2 document

This attribute holds a reference to an L<OpenOffice::OODoc::Document> object
which will be used as the source of template data.

=cut

has document => (
    is => 'ro',
    isa => 'OpenOffice::OODoc::Document',
    required => 1
);

=head2 context

May hold an L<OpenOffice::OODoc::Element> object. If set it will be used
to reduce the scope of this template's effects. Instead of search the entire
document for tags and sections, only items descending from the referenced
element will be searched.

Provides C<has_context>.

=cut

has context => (
    is => 'rw',
    isa => 'OpenOffice::OODoc::Element',
    predicate => 'has_context'
);

=head2 manifest

The L<manifest|OpenOffice::OODoc::Manifest> for the loaded Document.

=cut

has manifest => (
    is => 'rw',
    isa => 'OpenOffice::OODoc::Manifest',
    init_arg => undef,
    default => sub {
        my $self = shift;
        return odfManifest(file => $self->document);
    },
);

=head2 styles

Convenience accessor for the L<OpenOffice::OODoc::Styles> object associated
with the L</document>.

=cut

has styles => (
    is => 'rw',
    isa => 'OpenOffice::OODoc::Styles',
    default => sub {
        return odfStyles(file => shift->document);
    }
);

=head2 style_paragraph_selectors

Convenience data bag with an array of XPaths that point to the header
and footer paragraph styles.

=cut

has style_paragraph_selectors => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    default => sub {
        return [qw[
            /office:master-styles/style:master-page/style:header/text:p
            /office:master-styles/style:master-page/style:footer/text:p
        ]];
    },
    handles => {
        paragraph_selectors => 'elements'
    }
);

=head1 METHODS

=head2 post_process

Executes a subset of template processing for the document's header and
footer sections.

Expects a list of string fetchers as argument.

=cut

sub post_process {
    my $self = shift;
    my $ztt2 = shift;

    my $ztt = Zaaksysteem::ZTT->new(cache => $ztt2->cache);

    $ztt->add_string_fetcher($ztt2->all_string_fetchers);

    for my $paragraph (map { $self->styles->getNodesByXPath($_) } $self->paragraph_selectors) {
        my $subtemplate = $ztt->process_template($paragraph->text);

        # No modifications, no need to replace the header/footer content
        # FMI: ZS-4715
        next unless $subtemplate->modification_count;

        $paragraph->set_text($subtemplate->string);
    }
}

=head2 tag_selections

Returns a list of L<Zaaksysteem::ZTT::Selection> objects that reference
tags defined in the template.

    for my $selection ($openoffice_template->tag_selections) {
        # Process selection
    }

=cut

sub tag_selections {
    my $self = shift;

    my @retval;

    my $regexp = $self->tag_regexp;

    for my $element ($self->document->selectElementsByContent($self->context, "$regexp")) {
        my $element_text = $element->text;
        while ($element_text =~ /$regexp/g) {
            my ($selection, $directive_text) = ($1, $2);
            my $directive = $self->directive_parser->parse($directive_text);

            # Just plain tags pls
            next unless defined $directive;
            next if exists $directive->{ iterate_context };

            push @retval, Zaaksysteem::ZTT::Selection::OpenOffice->new(
                element => $element,
                selection => $selection,

                tag => Zaaksysteem::ZTT::Tag->new(
                    expression => $directive->{ expression },
                    formatter => $directive->{ filter }
                )
            );
        }
    }

    return @retval;
}

=head2 inline_iterators

This method returns a list of L<selections|Zaaksysteem::ZTT::Selection> found
in the template where subcontext content is to be injected in-line.

=cut

sub inline_iterators {
    my $self = shift;

    my @retval;

    my $regexp = $self->tag_regexp;

    for my $element ($self->document->selectElementsByContent($self->context, "$regexp")) {
        my ($selection, $directive_text) = $element->text =~ $regexp;

        my $directive = $self->directive_parser->parse($directive_text);

        # Just plain tags pls
        next unless defined $directive;
        next unless exists $directive->{ iterate_context };

        push @retval, Zaaksysteem::ZTT::Selection::OpenOffice->new(
            element => $element,
            selection => $selection,
            iterate => $directive->{ iterate_context },

            tag => Zaaksysteem::ZTT::Tag->new(
                expression => $directive->{ expression },
                formatter => $directive->{ filter }
            )
        );
    }

    return @retval;
}

=head2 sections

This method returns the combined output of L</plain_sections> and
L</table_sections>.

=cut

sub sections {
    my $self = shift;

    my @sections;
    my $parser = Zaaksysteem::ZTT::MagicDirective->new;

    # Refuse to build sections from subdocuments
    return @sections if $self->has_context;

    for my $element ($self->document->getSections, $self->document->getTableList) {
        my $element_name = $element->getAttribute($element->isTable ? 'table:name' : 'text:name');

        # Deprecated old-style iteration configuration
        if (defined $element_name && $element_name =~ m[^itereer:\w+]) {
            push @sections, Zaaksysteem::ZTT::Selection::OpenOffice->new(
                element => $element,
                decode_section_name($element_name)
            );

            next;
        }

        my @scripts = grep {
            lc($_->getAttribute('script:language')) eq 'zttscript'
        } $element->descendants('text:script');

        for my $cmd (map { $parser->parse_script($_->getValue) } @scripts) {
            if (exists $cmd->{ show_when }) {
                push @sections, Zaaksysteem::ZTT::Selection::OpenOffice->new(
                    element => $element,
                    show_when => $cmd->{ show_when }
                );
            }

            if (exists $cmd->{ iterate }) {
                my $section = Zaaksysteem::ZTT::Selection::OpenOffice->new(
                    element => $element,
                    iterate => $cmd->{ iterate },
                    name => $element_name,
                );

                if ($cmd->{ constraint }) {
                    $section->show_when($cmd->{ constraint });
                }

                push @sections, $section;
            }
        }

        # Cleanup
        map { $_->delete } @scripts;
    }

    for my $frame ($self->document->getFrameList) {
        my @scripts = grep {
            lc($_->getAttribute('script:language')) eq 'zttscript'
        } $frame->descendants('text:script');

        for my $cmd (map { $parser->parse_script($_->getValue) } @scripts) {
            next unless exists $cmd->{ show_when };

            push @sections, Zaaksysteem::ZTT::Selection::OpenOffice->new(
                element => $frame,
                show_when => $cmd->{ show_when }
            );
        }

        map { $_->delete } @scripts;
    }

    return @sections;
}

=head2 decode_section_name

This method attempts to decode a C<itereer:> name string found in sections and
table elements in templates as a hash specifying the subcontext name to be
iterated, and the specific name of the iteration.

    my %info = $openoffice_template->decode_section_name('itereer:zaak_relaties # loop1')

The C<%info> hash will have the following content:

    {
        iterate => 'zaak_relaties',
        name => 'loop1'
    }

If no name is specified, the name key will have an undefined value.

=cut

sub decode_section_name {
    shift =~ m[^itereer:(?<iterate>[\w\.]+)(?:\s*\#\s*(?<name>[\w\.]+))?];

    return (
        iterate => $+{ iterate },
        name => $+{ name }
    );
}

=head2 replace

Use a L<Zaaksysteem::ZTT::Selection> to replace a tag in the template with the
actual value. Second argument is expected to be a L<Zaaksysteem::ZTT::Element>,
or at least something that implements the sanitize method and has a getter at
for the value of the replacement.

=cut

sub replace {
    my $self = shift;

    my $ztt2 = shift;
    my $selection = shift;
    my $replacement = shift;
    my $contexts = shift;

    return unless $selection->element->in($self->document->{ body });
    return unless $replacement;

    $replacement->sanitize;

    my $value = $replacement->value // '';

    if($replacement->type eq 'richtext') {
        my $oodoc_element_to_replace = up($selection->element, sub { shift->isParagraph });

        $self->substitute_element_from_markup (
            oodoc_element => $oodoc_element_to_replace,
            markup_text   => $replacement->original_value // '',
            markup_type   => 'html',
        );

        return;
    } elsif ($replacement->type eq 'image') {
        $self->substitute_image($selection->element, $value);
        $value = '';
    } elsif ($replacement->type eq 'hyperlink') {
        my $text = $selection->selection;

        # OpenOffice::OODoc->replaceText uses regexes to replace text content
        # and we need to escape all regex-y chars in the text to be replaced
        $text =~ s/([\[\]\(\)\|])/\\$1/g;

        $self->document->replaceText(
            $selection->element,
            $text,
            $self->document->outputTextConversion($replacement->title)
        );

        $self->document->setHyperlink(
            up($selection->element, sub { shift->isParagraph }),
            $replacement->title,
            $replacement->value
        );

        return;
    } elsif($replacement->type eq 'list') {
        my $p_to_replace = up($selection->element, sub { shift->isParagraph });

        my $value = $replacement->value;
        my @list_items = grep { defined } (
            ref $value eq 'ARRAY' ? @{ $value } : $value
        );

        if (scalar @list_items) {
            my $list = $self->document->insertItemList($p_to_replace,
                position => 'before'
            );

            for my $list_item (@list_items) {
                $self->document->appendListItem($list,
                    text => $self->document->outputTextConversion("$list_item")
                );
            }
        } else {
            # If we have no items, we want to sweep clean the <text:p>
            # below our origin element, so the document compacts itself.
            my $next = $p_to_replace->next_sibling_matches('text:p');

            if (defined $next && $next->is_empty) {
                $next->delete;
            }
        }

        $p_to_replace->delete;

        return;
    }

    # Convert newlines to something we can capture after replacing the text.
    $value =~ s/(, )?\n/[[BR]]/g;

    # Quote our to-replace-substring of the element, so we can safely
    # let OODoc's regexp matching do it's thing.
    my $filter = quotemeta $selection->selection;

    $self->document->substituteText(
        $selection->element,
        $filter,
        $self->document->outputTextConversion($value)
    );

    # Reify newlines by injecting line-breaks
    $self->document->setChildElements($selection->element, 'text:line-break',
        replace => quotemeta '[[BR]]'
    );

    if ($replacement->strip_when_empty) {
        my $paragraph = up($selection->element, sub { shift->isParagraph });

        unless ($self->document->getFlatText($paragraph) =~ m[\w]u) {
            $paragraph->delete;
        }
    }
}

=head2 remove_section

Deletes the element provided in the argument from the final document.

=cut

sub remove_section {
    my $self = shift;
    my $selection = shift;

    # Almost looks like a stub, but deleting stuff just happens to be a lot
    # easier than iterating and stuff.
    $selection->element->delete;
}

=head2 iterate

=cut

sub iterate {
    my $self = shift;
    my $ztt = shift;

    my $contexts = shift;
    my $selection = shift;

    if ($selection->element->isTable) {
        return $self->iterate_table($ztt, $contexts, $selection);
    }

    if ($selection->element->isSection) {
        return $self->iterate_section($ztt, $contexts, $selection);
    }

    throw('ztt/iterator/unknown_type', 'Selection based on an element type I can\'t handle (yet..)');
}

=head2 iterate_table

=cut

sub iterate_table {
    my $self = shift;
    my $ztt2 = shift;

    my $contexts = shift;
    my $selection = shift;

    # Assume the first row in the table is our template.
    my $tpl = $selection->element->child(0, 'table:table-row');

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new(cache => $ztt2->cache);
        $ztt->add_context($context);

        # Copy the template row
        my $row = $self->document->insertTableRow($tpl);

        # Evaluate each row as a OpenOffice template with a specific element
        my $template_inside_element = Zaaksysteem::ZTT::Template::OpenOffice->new(
            document => $self->document,
            styles   => $self->styles,
            context  => $row,
            manifest => $self->manifest,
            style_paragraph_selectors => [], # do not go down on headers/footers
        );
        $ztt->process_template($template_inside_element);
    }

    # Delete the template row
    $tpl->delete;

    # Rename the table so the next loop doesn't re-eval this one.
    $selection->element->setAttribute('table:name', 'iterdone');
}

=head2 iterate_section

=cut

sub iterate_section {
    my $self = shift;
    my $ztt2 = shift;

    my $contexts = shift;
    my $selection = shift;

    # An 'is_orphan' helper would've been nice here.
    return unless $selection->element->{ parent };

    my $element_to_replace = up($selection->element, sub { shift->isParagraph });

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new(cache => $ztt2->cache);
        $ztt->add_context($context);

        my $sect_copy = $selection->element->copy;

        # Set unique names so {Open,Libre}Office won't act up.
        $sect_copy->setAttribute('text:name', sprintf('Iteration %s', $context));
        $sect_copy->paste(before => $selection->element);

        my $tpl = Zaaksysteem::ZTT::Template::OpenOffice->new(
            document => $self->document,
            context => $sect_copy
        );

        $ztt->process_template($tpl);
    }

    $selection->element->delete;
}

=head2 iterate_inline

=cut

sub iterate_inline {
    my $self = shift;
    my $ztt2 = shift;

    my $contexts = shift;
    my $selection = shift;

    my @values;

    for my $context (@{ $contexts }) {
        my $ztt = Zaaksysteem::ZTT->new(cache => $ztt2->cache);

        $ztt->add_context($context);

        my $value = $ztt->process_template(sprintf('[[%s]]', $selection->tag->name))->string;

        if(length $value) {
            push @values, $value;
        }
    }

    my $replacement = Zaaksysteem::ZTT::Element->new(value => join(', ', @values));

    $self->replace($ztt2, $selection, $replacement);
}

=head1 FUNCTIONS

These functions are not class methods, they are just generic helpers
for easy traversal of the oodoc or for converting L<Zaaksysteem::ZTT::Element>s

=head2 up

This function returns the first element to return true for the test condition
supplied as the second argument by walking up the document tree. If no such
node can be found returns undef.

=cut

sub up {
    my $element = shift;
    my $code = shift;

    while($element && !$code->($element)) {
        $element = $element->getParentNode;
    }

    return $element;
}

=head2 substitute_image

This function will create a new OpenOffice image element and attach it to
the first paragraph in the node lineage of the provided element.

It will add the image to the document's manifest.

Currently only works for C<image/jpg> files.

=cut

sub substitute_image {
    my ($self, $element, $value) = @_;

    my ($image_path, $size);
    if (ref($value) eq 'HASH') {
        $image_path = $value->{image_path};
        $size = sprintf(
            "%.3f,%.3f",
            $value->{width},
            $value->{height}
        );
    }
    else {
        $image_path = $value;

        # Default size value, for backwards compatibility with older
        # "signatures" templates.
        #
        # Based on OpenOffice import behaviour. Imported a sample image into
        # OpenOffice, and recorded it's measurements in inches, converted these
        # to metric (cm).
        # 350px x 120px (3.65" x 1.25") equals in cm:
        $size = "9.271,3.175";
    }

    my $paragraph = up($element, sub { shift->isParagraph });

    # All images and styles must have unique names, numbers are acceptable
    my $style_name = sprintf("PhotoStyle%d", $self->modification_count);
    my $style = $self->document->createImageStyle($style_name);

    my $image = $self->document->createImageElement(
        sprintf('zsimage_%d', $self->modification_count),
        style      => $style_name,
        attachment => $paragraph,
        import     => $image_path,
        size       => $size,
    );

    $self->document->setAttributes($image, 'text:anchor-type' => 'paragraph');

    my $link = $self->document->imageLink($image);

    # TODO - detect actually mimetype
    $self->manifest->setEntry( $link, 'image/jpeg');

    return;
}

=head1 ROLES

The following roles are consumed:

=over

=item L<Zaaksysteem::ZTT::Template::OpenOffice::Element>

=back

=cut

with (
    'Zaaksysteem::ZTT::Template::OpenOffice::Element',
);

__PACKAGE__->meta->make_immutable;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
