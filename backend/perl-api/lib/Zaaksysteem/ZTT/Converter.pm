package Zaaksysteem::ZTT::Converter;

=head1 NAME

Zaaksysteem::ZTT::Converter - Define conversions

=head1 SYNOPSIS

    my $html = '<h1>Foo</h1><p>important<strong>[[ magic_here ]]</strong></p>';
    my $converter = Zaaksysteem::ZTT::Converter->new;
    my $oodoc = $converter->from_markup_text_into_openoffice_oodoc($html);

=cut

use Moose;

=head1 DESCRIPTION

This module is merely an interface class that describes a variety of conversion
methods. These conversion could potentially be from any data format to any other
data format. It has specifically been been designed to convert markup text to
another markup text, althought it has its promary usecase for Zaaksysteem.

Each conversion method will need to be implemented by some converter role. Such
role will implement conversions by best means possible and can use it's own ways
to do those conversion.

The methods here are quite narrowed down to what they expect and what they will
return. See NOTES below

=cut

=head1 METHODS - to be implemented by Moose::Roles

See L<Zaaksysteem::ZTT::Converter::Pandoc>

=cut

=head2 from_markup_text_into_openoffice_oodoc

Converts a piece of markup text into an L<OpenOffice::OODoc> object.

    my $oodoc = $converter->markup_to_oodoc(
        '<b>Foo</b>',
        convert_from => 'html',
        reference_odt => 'my_reference.odt',
    );

=head3 Options

=over

=item reference_odt

An .odt file that contains style definitions that will applied to render the
html with the given styles.

=back

=cut

sub from_markup_text_into_openoffice_oodoc {
    die "method not implemented: 'from_markup_text_into_openoffice_oodoc'"
}

=head1 ROLES

The following roles are consumed:

=over

=item L<Zaaksysteem::ZTT::Converter::Pandoc>

=back

=cut

with (
    'Zaaksysteem::ZTT::Converter::Pandoc',
);

=head1 NOTES

We have these very narrowed down methods is because of the following reasoning:
We could try to create a generic object that can contain any data-type. Surely,
it could do with a 'one-type-to-rule-all'. But it will just be something like a
micro-object with two atrtributes "mime-type" and "data". And then we still need
to split it out in specific routines here.

=cut

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
