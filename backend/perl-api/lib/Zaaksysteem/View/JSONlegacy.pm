package Zaaksysteem::View::JSONlegacy;

use strict;
use base 'Catalyst::View::JSON';

use JSON::XS ();

=head2 encode_json

Simple helper to return JSON-formatted data.

=cut

sub encode_json {
    my($self, $c, $data) = @_;

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub { shift->iso8601 . 'Z' };

    my $encoder = JSON::XS->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed;
    $encoder->encode($data);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
