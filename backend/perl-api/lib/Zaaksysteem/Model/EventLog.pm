package Zaaksysteem::Model::EventLog;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Model::EventLog - Catalyst model factory for
L<Zaaksysteem::EventLog::Model>

=head1 SYNOPSIS

    my $model = $c->model('EventLog');

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::EventLog::Model',
    constructor => 'new',
);

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments to create a new L<Zaaksysteem::Store::EventLog> instance.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        schema           => $c->model('DB')->schema,
        object_model     => $c->model('Object'),
        subject_model    => $c->model('BR::Subject'),
        betrokkene_model => $c->model('DB::ZaakBetrokkenen'),
        user             => $c->user,
        zs_version       => $c->config->{ZS_VERSION},
        environment      => $c->config->{gemeente_id},
    };
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
