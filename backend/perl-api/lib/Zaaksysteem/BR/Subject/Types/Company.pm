package Zaaksysteem::BR::Subject::Types::Company;

use Moose::Role;
use BTTW::Tools;

with 'MooseX::Log::Log4perl';

use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_CONFIGURATION SUBJECT_MAPPING/;

use constant SUBJECT_TYPE => 'company';
use constant SCHEMA_TABLE => 'Bedrijf';

=head1 NAME

Zaaksysteem::BR::Subject::Types::Company - Bridge specific role for this type

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating various types.

=head1 METHODS

=head2 _table_id

Private method: contains the reference to the original table ('bedrijf.id')

=cut

has '_table_id' => (
    is      => 'rw',
    isa     => 'Int',
);

=head2 new_from_row

Please do not use this function directly, instead, use the "bridge": L<Zaaksysteem::Bridge::Subject>

=cut

sub new_from_row {
    my ($class, $row) = @_;

    my $table_name  = $row->result_source->name;
    my $subjecttype = SUBJECT_MAPPING->{ $table_name };
    my $mapping     = SUBJECT_CONFIGURATION->{ $subjecttype }->{mapping};

    my %values;
    foreach my $col (keys %$mapping) {
        next unless defined $row->get_column($col);
        my $val = $row->$col;
        $values{$mapping->{$col}} = defined $val ? $val : undef;
    }

    my $main = $row->main_activity;
    if (%{$main}) {
        $values{main_activity} = Zaaksysteem::Object::Types::Company::Activity->new(
            %{$main}
        );
    }

    my $secondairy = $row->secondairy_activities;
    if (@{$secondairy}) {

        $values{secondairy_activities} = [

            map { Zaaksysteem::Object::Types::Company::Activity->new(%{$_}) }
                @{$secondairy}

        ];

    }

    ### Fix legal_entity
    $values{company_type}    = Zaaksysteem::Object::Types::LegalEntityType->new_from_code($values{company_type}) if $values{company_type};

    my %contact     = $class->_load_contact_data_from_row($row);
    my %addresses   = $class->_load_addresses_from_row($row);

    my $subject     = $class->new(%values, %contact, %addresses);

    $subject->id($row->uuid);
    $subject->_table_id($row->id);

    return $subject;
}

=head2 new_from_params

Please do not use this function directly, instead, use the "bridge": L<Zaaksysteem::Bridge::Subject>

=cut



sub _get_address_from_params {
    my $class  = shift;
    my $type   = shift;
    my $params = shift;

    return unless $params->{$type};
    my $address = $params->{$type};

    if (defined $address->{instance}) {
        $address = $address->{instance};
    }

    if ($address->{street} || $address->{foreign_address_line1}) {
        return $class->_get_address_object($address);
    }
    return;

}

sub new_from_params {
    my $class       = shift;
    my $rawparams   = { %{ (shift || {}) } };
    my %params      = map({ $_ => $rawparams->{ $_ } } grep( { defined($rawparams->{ $_ }) } keys %$rawparams));

    foreach (qw(address_residence address_correspondence)) {
        $params{$_} = $class->_get_address_from_params($_, \%params);
    }

    $params{company_type}
        = Zaaksysteem::Object::Types::LegalEntityType->new_from_code(
        $params{company_type}->{code})
        if $params{company_type};

    delete($params{$_}) for grep({ !defined($params{$_}) } keys %params);

    return $class->new(%params);
}

=head2 save_to_tables

Please do not use this function directly, instead, use the "bridge": L<Zaaksysteem::Bridge::Subject>

=cut

define_profile 'save_to_tables' => (
    required    => {
        schema  => 'Zaaksysteem::Schema'
    }
);


sub _map_activity {
    my $activity = shift;
    return {} unless $activity;

    return {
        code => $activity->code,
        description => $activity->description,
    }
}

sub save_to_tables {
    my $self        = shift;
    my $options     = assert_profile({ @_ })->valid;
    my $schema      = $options->{schema};

    $self->check_object(schema => $schema);

    my $mapping = SUBJECT_CONFIGURATION->{ SUBJECT_TYPE() }->{mapping};

    my %values;
    foreach my $k (keys %{$mapping}) {
        my $key = $mapping->{$k};
        my $v = $self->$key;

        if (blessed($v) && $v->isa('DateTime')) {
            $v = $schema->format_datetime_object($v);
        }

        $values{$k} = $v;
    }

    $values{main_activity} = _map_activity($self->main_activity);

    $values{secondairy_activities} = [
        map { _map_activity($_) } @{ $self->secondairy_activities // [] }
    ];

    ### Correct address and legal type
    %values = (
        %values,
        (map ({ $self->_get_address_values(type => $_) } qw/address_correspondence address_residence/))
    );
    $values{rechtsvorm} = $self->company_type->code if $self->company_type;

    ### TODO: Some validation logic in here: Does every address has the necessary fields filled, like
    ### there is not a landcode of NL and no street set, or it is an international without any foreign_address1

    ### Work the magic
    $schema->txn_do(
        sub {
            my $row;
            my $rs = $schema->resultset(SCHEMA_TABLE);
            if ($self->id) {
                $row = $rs->search({ uuid => $self->id })->first;

                unless ($row->update(\%values)) {
                    throw(
                        'object/types/company',
                        sprintf(
                            'Failed updating company by uuid: %s / id: %d',
                            $row->uuid, $row->id
                        )
                    );
                }
            } else {

                my %search =  (
                    deleted_on => undef,
                    $values{vestigingsnummer}
                        ? (vestigingsnummer =>
                            [$values{vestigingsnummer}, undef])
                        : (),
                );
                if ($values{dossiernummer}) {
                    $search{"NULLIF(dossiernummer,'')::bigint"} = $values{dossiernummer};
                }
                # We only have deleted_on
                if (keys %search == 1) {
                    $search{handelsnaam} = $values{handelsnaam};
                }

                my $rv = $rs->search(\%search);

                my @companies = $rv->all;
                if (@companies > 1) {
                    my @main_branches = grep { !$_->vestigingsnummer } @companies;
                    my @want;
                    if ($values{vestigingsnummer}) {
                        @want = grep { $_->vestigingsnummer } @companies;
                        if (@want > 1) {
                            throw('br/subject/company/exists',
                                "Cannot add a company, it already exists as a duplicate");
                        }
                        elsif (@want) {
                            @companies = @want;
                        }
                        elsif (@main_branches < 2 && !@want) {
                            @companies = @main_branches;
                        }
                    }
                    elsif (@main_branches <2) {
                        @companies = @main_branches;
                    }
                }

                if (@companies == 1) {
                    $row = shift @companies;
                    unless ($row->update(\%values)) {
                        throw(
                            'object/types/company',
                            sprintf(
                                'Failed updating company by uuid: %s / id: %d',
                                $row->uuid, $row->id
                            )
                        );
                    }
                }
                elsif(@companies == 0) {
                    $row = $schema->resultset(SCHEMA_TABLE)->create(\%values);
                }
                else {
                    $self->log->info("Duplicate companies found!: " . dump_terse(\@companies));
                    throw('br/subject/company/duplicates',
                        "Cannot add a company, there are duplicates found in the database");
                }
            }

            # Ensure we update contact data (email, phone numbers) too
            $self->_update_contact_data($schema, $row);
            $row->enable_bedrijf;

            ### Discard changes to retrieve uuid from database
            $row->discard_changes();
            $self->_table_id($row->id);
            $self->id($row->uuid);
        }
    );

    return $self;
}

=head2 check_object

    $entity->check_object(schema => $schema);

Will return the result of a C<assert_profile> when the object is missing some params. The entity
objects are rather free form. Mostly because of our legacy, but sometimes also when we just want
to see part of a subject.

This function makes sure we create or save objects into our database in a complete form, to prevent
further "legacy"

=cut

define_profile check_object => (
    %{ SUBJECT_CONFIGURATION->{ SUBJECT_TYPE() }{profiles}{save} }
);

sub check_object {
    my $self        = shift;
    my (%opts)      = @_;

    my %values      = map(
        { my $key = $_->name; $key => $self->$key }
        grep({ $_->does('Zaaksysteem::Metarole::ObjectAttribute') || $_->does('Zaaksysteem::Metarole::ObjectRelation') } $self->meta->get_all_attributes)
    );

    assert_profile(
        {
            %values,
            _schema            => $opts{schema},
            '_update_existing' => ($self->id || 0)
        },
    );

    foreach my $a (qw(address_correspondence address_residence)) {
        my $predicate = "has_$a";
        $self->$a->check_object if $self->$predicate;
    }

    return 1;
}

=head1 PRIVATE METHODS

=head2 _update_contact_data

Updates contact data for this company.

=cut

my %CONTACT_KEY_MAP = (
    mobile_phone_number => "mobiel",
    phone_number => "telefoonnummer",
    email_address => "email",
);

sub _update_contact_data {
    my $self = shift;
    my $schema = shift;
    my $row = shift;

    my $contactdata = $schema->resultset('ContactData')->search({
        gegevens_magazijn_id => $row->id,
        betrokkene_type => 2,
    });

    my %new_values;
    for my $contact_field (qw(mobile_phone_number phone_number email_address)) {
        $new_values{ $CONTACT_KEY_MAP{$contact_field} } = $self->$contact_field;
    }


    if (my $contactdata = $contactdata->first) {
        $contactdata->update(
            {
                %new_values,
                'last_modified' => \"now() at time zone 'utc'",
            }
        );
    } else {
        $schema->resultset('ContactData')->create(
            {
                'gegevens_magazijn_id' => $row->id,
                'betrokkene_type' => 2,
                'created' => \"now() at time zone 'utc'",
                'last_modified' => \"now() at time zone 'utc'",
                %new_values,
            }
        );
    }

    return;
}

=head2 _get_address_values

=cut

define_profile _get_address_values => (
    required => {
        type => 'Str',
    }
);

sub _get_address_values {
    my $self    = shift;
    my $options = assert_profile({ @_ })->valid;
    my $attrkey = $options->{type};

    my %premap  = (address_correspondence => 'correspondentie', address_residence => 'vestiging');

    my $address = $self->$attrkey;

    my $mapping = SUBJECT_CONFIGURATION->{SUBJECT_TYPE()}->{address_mapping};

    my %values;
    for my $key (keys %$mapping) {
        my $attr  = $mapping->{$key};
        my $dbkey = $premap{$attrkey} . "_" . $key;
        my $value = $address ? $address->$attr : undef;

        if ($attr eq 'country') {
            # default to NL, ideally we should have this in the update call but
            # I don't want to change the DB code tooo much
            $value = $value ? $value->dutch_code : '6030';
        }

        if ($attr eq 'municipality' && $value) {
            $value = $value->code;
        }

        $values{$dbkey} = $value;
    }

    return %values if $attrkey ne 'address_residence';

    my $res = $self->address_residence;

    if ($res && $res->latitude) {
        $values{vestiging_latlong} = sprintf("(%s,%s)",
            $res->latitude, $res->longitude
        );
    } else {
        $values{vestiging_latlong} = undef;
    }

    if ($res && $res->bag_id) {
        $values{vestiging_bag_id} = $res->bag_id;
    } else {
        $values{vestiging_bag_id} = undef;
    }

    return %values;
}

=head2 _load_contact_data_from_row

Loads the contact data into the subject (phone numbers, email address)

=cut

sub _load_contact_data_from_row {
    my ($class, $row) = @_;

    my %rv;

    my $contactdata = $row->result_source->schema->resultset('ContactData')->search({
        gegevens_magazijn_id    => $row->id,
        betrokkene_type         => 2, # "Company"
    })->first;

    return unless $contactdata;

    $rv{mobile_phone_number} = $contactdata->mobiel         if $contactdata->mobiel;
    $rv{phone_number}        = $contactdata->telefoonnummer if $contactdata->telefoonnummer;
    $rv{email_address}       = $contactdata->email          if $contactdata->email;

    return %rv;
}

=head2 _load_addresses_from_row

Loads the addresses into this subject

=cut

sub _load_addresses_from_row {
    my ($class, $row) = @_;
    my %rv;

    my $correspondence  = $class->_get_address_object({ $row->get_columns }, 'correspondentie');
    my $residence       = $class->_get_address_object({ $row->get_columns }, 'vestiging');

    $rv{address_correspondence} = $correspondence if $correspondence;
    $rv{address_residence}      = $residence if $residence;

    return %rv;
}

=head2 _get_address_object

Returns a filled object of type L<Zaaksysteem::Object::Types::Address>

=head3 WARNING:

This routine has two different ways of operation that expect entire different
sets of parameters:

One operation mode is without C<$prefix> at the end and expects the following
structure:

    {
        street                      => ... ,
        street_number               => ... ,
        ...
        country                     => {
            dutch_code                  => '1234',
        }
    }

or

    {
        foreign_address_line1       => ... ,
        foreign_address_line2       => ... ,
        foreign_address_line3       => ... ,
        country                     => {
            dutch_code                  => '1234',
        }
    }

The other operation mode is with the C<$prefix> at the end, which needs to be
either C<'correspondentie'> or C<'vestiging'>. This suposedly expects a flat
hash (non nested) that would come from a database row and looks like:

    {
        correspondentie_adres       => ... ,
        ...
        correspondentie_landcode    => '1234',
    }

for a dutch correspondence address or for a foreign 'vestiging' address:

    {
        vestiging_adres_buitenland1 => ... ,
        vestiging_adres_buitenland2 => ... ,
        vestiging_adres_buitenland3 => ... ,
        vestiging_landcode          => '1234',
    }

There is NO relation between the country code given and the values for normal
dutch address or the three foreign address lines.

Not only that, the set of params is completely different and the subroutine will
happily accept any passed in bogus set of silly params.

And hopefully, ofter mapping the data, there will be the right params to be
transfered to L<Zaaksysteem::Object::Types::Address->new>, or it will break
there

=cut

sub _get_address_object {
    my ($class, $params, $prefix) = @_;
    my %values;

    unless ($params) {
        throw('object/types/company/no_row', 'No row given, code error');
    }

    if ($prefix) {
        my $mapping = SUBJECT_CONFIGURATION->{company}{address_mapping};

        for my $key (keys %$mapping) {
            my $dbkey = $prefix . "_" . $key;

            next unless defined $params->{$dbkey};

            $values{ $mapping->{$key} } = $params->{$dbkey};
        }

        if ($values{country}) {
            $values{country} = { dutch_code => $values{country} };
        }

        if ($prefix eq 'vestiging') {
            my $latlong = $params->{vestiging_latlong};
            my ($lat, $long);
            if ($latlong) {
                $latlong =~ s/[\(\)]//g;
                ($lat, $long) = split(/,/, $latlong);
            }

            if ($lat) {
                $values{latitude} = $lat;
                $values{longitude} = $long;
            }

            if ($params->{vestiging_bag_id}) {
                $values{bag_id} = $params->{vestiging_bag_id};
            }
        }
    } else {
        %values = map({ $_ => $params->{ $_ } } grep( { defined($params->{ $_ }) } keys %$params));
    }

    foreach my $key (qw(country municipality)) {
        if ($values{$key}) {
            if (defined $values{$key}{instance}) {
                $values{$key} = $values{$key}{instance};
            }
        }
    }


    $values{country}
        = Zaaksysteem::Object::Types::CountryCode->new_from_dutch_code(
        $values{country}->{dutch_code})
        if $values{country};

    my $country = delete $values{country};

    if ($country && keys(%values)) {
        return Zaaksysteem::Object::Types::Address->new(%values,
            country => $country);
    }

    return;
}

=head2 _build_display_name

Returns the contents of $self->company;

=cut

sub _build_display_name {
    my $self            = shift;

    return $self->company;
}


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
