package Zaaksysteem::BR::Subject::Types::Roles::DB;
use Moose::Role;

with 'MooseX::Log::Log4perl';
use BTTW::Tools;

=head1 NAME

Zaaksysteem::BR::Subject::Types::Roles::DB - Database role for subject types

=head1 DESCRIPTION

Role to aid subject to DB components transitioning. When consuming this role you can either use the plain defaults
or override and/or modify the various methods (both private and public).

=head1 ATTRIBUTES

=head2 source_name

The name of the L<DBIx::Class::ResultSet>, eg C<NatuurlijkPersoon>

=cut

has source_name => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 mapping

Mapping of Object attributes to DB column names

=cut

has mapping => (
    is       => 'ro',
    isa      => 'HashRef',
    lazy     => 1,
);

=head2 METHODS


=head2 convert_object_to_rs_values

Convert the object to a hashref that can be used in the <update> and/or
<create> call

=cut

sig convert_rs_to_object_values => '?Zaaksysteem::Schema';

sub convert_object_to_rs_values {
    my ($self, $schema) = @_;

    my %values;
    my %mapping = %{$self->mapping};
    foreach (keys %mapping) {
        my $key = $mapping{$_};
        $values{$_} = $self->$key if ($self->can('key') && defined $self->$key);
    }
    return \%values;
}

=head2 convert_rs_to_object_values

Convert the database object to an hashref that can be used for the
C<new> call on an object. Is used for new_from_row

=cut

sig convert_rs_to_object_values => 'DBIx::Class::Row';

sub convert_rs_to_object_values {
    my ($self, $row) = @_;

    my %values;
    my %mapping = %{$self->mapping};
    foreach my $col (keys %mapping) {
        $values{$mapping{$col}} = $row->$col if defined $row->get_column($col);
    }
    return \%values;
}

=head2 trigger_after_save

This method allows you to trigger specific actions after a save call,
eg, running queue items or other things

=cut

sig trigger_after_save => "DBIx::Class::Row, ?HashRef";

sub trigger_after_save {
    my ($self, $row, $values) = @_;
    return 1;
}

=head2 save_to_tables

Implements a save call for subject objects

=cut

define_profile 'save_to_tables' => (
    required    => {
        schema  => 'Zaaksysteem::Schema'
    }
);

sub save_to_tables {
    my $self        = shift;
    my $options     = assert_profile({ @_ })->valid;
    my $schema      = $options->{schema};

    $self->check_object(schema => $schema);

    try {
        $schema->txn_do(
            sub {
                my $values = $self->convert_object_to_values($self);

                my $row = $self->id
                    ? $self->_update_entry($schema, $values)
                    : $self->_create_entry($schema, $values);

                $row->discard_changes;
                $self->id($row->uuid);
                $self->_table_id($row->id);

                $self->trigger_after_save($row, $values);
            }
        );
    }
    catch {
        $self->log->error($_);
        if (blessed($_) && $_->can('throw')) {
            $_->throw();
        }
        die $_;
    };
    return $self;
}

=head2 new_from_row

Get an object based on the row in the database

=cut

sig new_from_row => 'DBIx::Class::Row';

sub new_from_row {
    my ($class, $row) = @_;

    my $values = $class->convert_rs_to_object_values($row);

    my $object = $class->new(%$values);

    if ($row->can('uuid')) {
        $object->id($row->uuid);
    }

    if ($object->can('_table_id')) {
        $object->_table_id($row->id);
    }

    return $object;
}

sig _update_entry => 'Zaaksysteem::Schema, HashRef';

sub _update_entry {
    my ($self, $schema, $values) = @_;
    my $row = $schema->resultset($self->source_name)->search({ uuid => $self->id })
        ->first;

    try {
        $row->update($values);
    }
    catch {
        $self->log->error("Error updating object %d with values [%s]: %s", __PACKAGE__, $_);
        throw(
            'object/types/subject/update',
            sprintf(
                'Failed updating %s by uuid %s [%d]',
                __PACKAGE__, $row->uuid, $row->id
            )
        );
    };
    return $row;
}

sig _create_entry => 'Zaaksysteem::Schema, HashRef';

sub _create_entry {
    my ($self, $schema, $values) = @_;
    return try {
        return $schema->resultset($self->source_name)->create($values);
    }
    catch {
        $self->log->error("Error inserting %s with values [%s]: %s", __PACKAGE__, dump_terse($values), $_);
        throw('object/types/subject/create',
            sprintf('Failed saving new %s', __PACKAGE__));
    };
}

1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
