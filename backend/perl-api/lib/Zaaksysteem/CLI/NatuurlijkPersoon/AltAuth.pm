package Zaaksysteem::CLI::NatuurlijkPersoon::AltAuth;
use Moose;

=head1 NAME

Zaaksysteem::CLI::NatuurlijkPersoon::AltAuth - Alternative authentication 
cleanups

=cut

extends 'Zaaksysteem::CLI';

with 'Zaaksysteem::CLI::Roles::Auditlog';

use BTTW::Tools;
use DateTime;
use JSON::XS;
use Zaaksysteem::Types qw(BSN);
use Zaaksysteem::Backend::Sysin::Auth::Alternative;

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->$orig(@_);

    $self->do_transaction(
        sub {
            my ($self, $schema) = @_;

            $self->log->info("Searching correct alt auth accounts");
            $self->search_alt_auth();

            $self->log->info("Searching duplicates based on correct accounts");
            $self->dedupe_alt_auth();

            $self->log->info("Linked unlinked accounts to NP table");
            $self->link_legacy_accounts();
        }
    );

    return 1;

};

has bsn_mapper => (
    traits  => ['Hash'],
    isa     => 'HashRef',
    is      => 'rw',
    default => sub { {} },
    handles => {
        set_bsn => 'set',
        get_bsn => 'get',
        has_bsn => 'exists',
    },
);

has np_rs => (
    is      => 'rw',
    isa     => 'Defined',
    default => sub {
        my $self = shift;
        return $self->schema->resultset('NatuurlijkPersoon')
            ->search_rs({ deleted_on => undef },
            { column => [qw(uuid burgerservicenummer)] });

    },
);

has subject_rs => (
    is      => 'rw',
    isa     => 'Defined',
    default => sub {
        my $self = shift;
        return $self->schema->resultset('Subject')
            ->search_rs({ subject_type => 'person' },
            { order_by => { -desc => 'last_modified' } });
    },
);

=head2 search_alt_auth

Search for all the alternative authentication accounts and map UUID to BSN for
legacy accounts

=cut

sub search_alt_auth {
    my $self = shift;

    my $rs = $self->subject_rs->search(
        { uuid => { -in => $self->np_rs->get_column('uuid')->as_query } });

    while (my $subject = $rs->next) {
        my $bsn = $subject->properties->{bsn};
        next unless $bsn;
        $bsn = int($bsn);
        $self->set_bsn($bsn, $subject->uuid);
    }
}

=head2 dedupe_al_auth

Remove all duplicate accounts one natural person may have based on the BSN
found in the properties

=cut

sub dedupe_alt_auth {
    my $self = shift;

    my $rs = $self->subject_rs->search_rs();
    while (my $subject = $rs->next) {
        my $bsn = $subject->properties->{bsn};
        next unless $bsn;
        $bsn = int($bsn);

        if ($self->has_bsn($bsn)) {
            my $uuid = $self->get_bsn($bsn);
            if ($subject->uuid ne $uuid) {
                $self->_delete_account($subject, $bsn);
            }
        }
    }
}

sub log_subject {
    my ($self, $intro, $subject, $np) = @_;

    my $properties =$subject->properties;

    my $email = $properties->{email_address}
        // $properties->{initial_email_address};

    my $msg = sprintf("%s uuid %s date: %s mail: %s  username: %s",
        $intro, $subject->uuid, $subject->user_entities->first->date_created, $email, $subject->username);

    if ($np) {
        $msg .= sprintf(
            " url: https://%s/betrokkene/%d?gm=1&type=natuurlijk_persoon",
             $self->hostname, $np->id,
        );
        $msg .= sprintf(" cases: %s", $np->has_cases ? "yes" : "no");
        $msg .= sprintf(" concept-cases: %s", $np->has_concept_cases ? "yes" : "no");
        $msg .= sprintf(" aanhef: %s", lc($np->geslachtsaanduiding) eq 'm' ? "Dhr" : "Mevr");
        $msg .= sprintf(" achternaam: %s", $np->naamgebruik);
    }
    $self->log->info($msg);
}

sub _delete_account {
    my ($self, $subject, $bsn) = @_;

    $bsn //= $subject->properties->{bsn};
    my $np;
    if ($bsn) {
       $np = $self->np_rs->find_by_gov_id($bsn, { authenticated => [ 1, 0 ]});
    }
    $self->log_subject("Deleting subject", $subject, $np);

    $subject->user_entities->update(
        {
            date_deleted =>
                $self->schema->format_datetime_object(DateTime->now())
        }
    );

    $subject->user_entities->delete;
    $subject->delete;
}

=head2 link_legacy_accounts

Link all accounts that can be matched on BSN from the properties table.

=cut

sub link_legacy_accounts {
    my $self = shift;
    my $rs   = $self->subject_rs->search_rs();

    my $total = 0;
    while (my $subject = $rs->next) {
        my $bsn = $subject->properties->{bsn};
        next unless $bsn;
        $bsn = int($bsn);
        if ($self->has_bsn($bsn)) {
            my $uuid = $self->get_bsn($bsn);
            $self->_delete_account($subject) if $subject->uuid ne $uuid;
            next;
        }

        my $np = $self->np_rs->find_by_gov_id($bsn, { authenticated => [ 0, 1 ] });
        my $email = $subject->properties->{email_address}
            // $subject->properties->{initial_email_address};

        if ($np) {
            $subject->update({ uuid => $np->uuid });
            $self->set_bsn($bsn, $np->uuid);

            $total++;
            $self->log_subject("Linking subject", $subject, $np);
        }
        else {
            $self->set_bsn($bsn, $subject->uuid);
        }
    }

    $self->log->info("Linked $total accounts");
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
