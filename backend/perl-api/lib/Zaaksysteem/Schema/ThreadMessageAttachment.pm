use utf8;
package Zaaksysteem::Schema::ThreadMessageAttachment;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ThreadMessageAttachment

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<thread_message_attachment>

=cut

__PACKAGE__->table("thread_message_attachment");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'thread_message_attachment_id_seq'

=head2 filestore_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 thread_message_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 filename

  data_type: 'text'
  is_nullable: 0
  original: {data_type => "varchar"}

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "thread_message_attachment_id_seq",
  },
  "filestore_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "thread_message_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "filename",
  {
    data_type   => "text",
    is_nullable => 0,
    original    => { data_type => "varchar" },
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<thread_message_attachment_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("thread_message_attachment_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 filestore_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Filestore>

=cut

__PACKAGE__->belongs_to(
  "filestore_id",
  "Zaaksysteem::Schema::Filestore",
  { id => "filestore_id" },
);

=head2 thread_message_attachment_derivatives

Type: has_many

Related object: L<Zaaksysteem::Schema::ThreadMessageAttachmentDerivative>

=cut

__PACKAGE__->has_many(
  "thread_message_attachment_derivatives",
  "Zaaksysteem::Schema::ThreadMessageAttachmentDerivative",
  { "foreign.thread_message_attachment_id" => "self.id" },
  undef,
);

=head2 thread_message_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ThreadMessage>

=cut

__PACKAGE__->belongs_to(
  "thread_message_id",
  "Zaaksysteem::Schema::ThreadMessage",
  { id => "thread_message_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-04-15 12:07:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EVOz3AsBGt01WjZ1ye90PA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
