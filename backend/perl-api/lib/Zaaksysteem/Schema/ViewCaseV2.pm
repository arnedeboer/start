use utf8;
package Zaaksysteem::Schema::ViewCaseV2;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ViewCaseV2

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<view_case_v2>

=cut

__PACKAGE__->table("view_case_v2");
__PACKAGE__->result_source_instance->view_definition(" SELECT z.id,\n    z.uuid,\n    z.onderwerp,\n    z.route_ou,\n    z.route_role,\n    z.vernietigingsdatum,\n    z.archival_state,\n    z.status,\n    z.contactkanaal,\n    z.created,\n    z.registratiedatum,\n    z.streefafhandeldatum,\n    z.afhandeldatum,\n    z.stalled_until,\n    z.milestone,\n    z.last_modified,\n    z.behandelaar_gm_id,\n    z.coordinator_gm_id,\n    z.aanvrager,\n    z.aanvraag_trigger,\n    z.onderwerp_extern,\n    z.resultaat,\n    z.resultaat_id,\n    z.payment_amount,\n    z.behandelaar,\n    z.coordinator,\n    z.urgency,\n    z.urgency_date_medium,\n    z.urgency_date_high,\n    z.preset_client,\n    z.prefix,\n    z.number_master,\n    z.html_email_template,\n    z.payment_status,\n    z.dutch_price AS price,\n    z.zaaktype_node_id,\n    z.pid AS number_parent,\n    z.vervolg_van AS number_previous,\n    z.leadtime AS lead_time_real,\n    z.aanvrager_gm_id,\n    z.aanvrager_type,\n    z.zaaktype_id,\n    z.deleted,\n    z.confidentiality,\n    zm.current_deadline,\n    zm.deadline_timeline,\n    ztr.id AS result_id,\n    ztr.uuid AS result_uuid,\n    ztr.resultaat AS result,\n    ztr.selectielijst AS active_selection_list,\n        CASE\n            WHEN (z.status = 'stalled'::text) THEN zm.opschorten\n            ELSE NULL::character varying\n        END AS suspension_rationale,\n        CASE\n            WHEN (z.status = 'resolved'::text) THEN zm.afhandeling\n            ELSE NULL::character varying\n        END AS premature_completion_rationale,\n    'Dossier'::text AS aggregation_scope,\n    ztr.archiefnominatie AS type_of_archiving,\n    rpt.label AS period_of_preservation,\n    ztr.label AS result_description,\n    ztr.comments AS result_explanation,\n    ((ztr.properties)::jsonb -> 'selectielijst_nummer'::text) AS result_selection_list_number,\n    ((ztr.properties)::jsonb -> 'procestype_nummer'::text) AS result_process_type_number,\n    ((ztr.properties)::jsonb -> 'procestype_naam'::text) AS result_process_type_name,\n    ((ztr.properties)::jsonb -> 'procestype_omschrijving'::text) AS result_process_type_description,\n    ((ztr.properties)::jsonb -> 'procestype_toelichting'::text) AS result_process_type_explanation,\n    ((ztr.properties)::jsonb -> 'procestype_object'::text) AS result_process_type_object,\n    ((ztr.properties)::jsonb -> 'procestype_generiek'::text) AS result_process_type_generic,\n    ((ztr.properties)::jsonb -> 'herkomst'::text) AS result_origin,\n    ((ztr.properties)::jsonb -> 'procestermijn'::text) AS result_process_term,\n        CASE\n            WHEN (z.status = 'stalled'::text) THEN NULL::integer\n            ELSE ((z.streefafhandeldatum)::date - COALESCE((z.afhandeldatum)::date, (now())::date))\n        END AS days_left,\n    get_date_progress_from_case(hstore(z.*)) AS progress_days,\n    ARRAY( SELECT json_build_object('naam', bibliotheek_kenmerken.naam, 'magic_string', bibliotheek_kenmerken.magic_string, 'value', zaak_kenmerk.value, 'type', bibliotheek_kenmerken.value_type, 'is_multiple', bibliotheek_kenmerken.type_multiple) AS json_build_object\n           FROM (zaak_kenmerk\n             JOIN bibliotheek_kenmerken ON ((zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id)))\n          WHERE (zaak_kenmerk.zaak_id = z.id)) AS custom_fields,\n    ARRAY( SELECT json_build_object('status', zs.status, 'type', ca.type, 'data', ca.data, 'automatic', ca.automatic) AS json_build_object\n           FROM (case_action ca\n             JOIN zaaktype_status zs ON ((zs.id = ca.casetype_status_id)))\n          WHERE (ca.case_id = z.id)) AS case_actions,\n    is_destructable((z.vernietigingsdatum)::timestamp with time zone) AS destructable,\n    COALESCE(( SELECT string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', '::text) AS string_agg\n           FROM file f\n          WHERE ((f.case_id = z.id) AND (f.date_deleted IS NULL) AND (f.active_version = true))), ''::text) AS documents,\n    COALESCE(( SELECT string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', '::text) AS string_agg\n           FROM (file case_documents\n             JOIN file_case_document fcd ON (((fcd.case_id = case_documents.case_id) AND (case_documents.id = fcd.file_id))))\n          WHERE (case_documents.case_id = z.id)), ''::text) AS case_documents,\n    json_build_object('uuid', ro.uuid, 'name', ro.name, 'description', ro.description, 'parent_uuid', gr.uuid, 'parent_name', gr.name) AS case_role,\n    json_build_object('opschorten', zm.opschorten, 'afhandeling', zm.afhandeling, 'stalled_since', zm.stalled_since, 'unaccepted_files_count', zm.unaccepted_files_count, 'unaccepted_attribute_update_count', zm.unaccepted_attribute_update_count, 'unread_communication_count', zm.unread_communication_count) AS case_meta,\n    json_build_object('id', zts.id, 'zaaktype_node_id', zts.zaaktype_node_id, 'status', zts.status, 'status_type', zts.status_type, 'naam', zts.naam, 'created', zts.created, 'last_modified', zts.last_modified, 'ou_id', zts.ou_id, 'role_id', zts.role_id, 'checklist', zts.checklist, 'fase', zts.fase, 'role_set', zts.role_set) AS case_status,\n    ( SELECT json_build_object('uuid', grp.uuid, 'name', grp.name, 'description', grp.description, 'parent_uuid', grp_alias.uuid, 'parent_name', grp_alias.name) AS json_build_object\n           FROM (groups grp\n             LEFT JOIN groups grp_alias ON (((array_length(grp.path, 1) > 1) AND (grp.path[(array_upper(grp.path, 1) - 1)] = grp_alias.id))))\n          WHERE (z.route_ou = grp.id)) AS case_department,\n    ARRAY( SELECT json_build_object('betrokkene_type', zb.betrokkene_type, 'type', vc.type, 'subject_id', zb.subject_id, 'rol', zb.rol, 'magic_string_prefix', zb.magic_string_prefix, 'display_name', vc.display_name) AS json_build_object\n           FROM (zaak_betrokkenen zb\n             JOIN view_contacts vc ON ((vc.uuid = zb.subject_id)))\n          WHERE ((zb.zaak_id = z.id) AND (zb.deleted IS NULL) AND (zb.id <> z.aanvrager) AND (zb.id <> z.behandelaar) AND (zb.id <> z.coordinator))) AS case_subjects,\n    ( SELECT json_build_object('uuid', vc.uuid, 'type', vc.type, 'display_name', vc.display_name) AS json_build_object\n           FROM view_contacts vc\n          WHERE ((vc.id = z.aanvrager_gm_id) AND (vc.legacy_type = (z.aanvrager_type)::text))) AS requestor_obj,\n    ( SELECT json_build_object('uuid', vc.uuid, 'type', vc.type, 'display_name', vc.display_name) AS json_build_object\n           FROM view_contacts vc\n          WHERE ((vc.id = z.behandelaar_gm_id) AND (vc.type = 'employee'::text))) AS assignee_obj,\n    ( SELECT json_build_object('uuid', vc.uuid, 'type', vc.type, 'display_name', vc.display_name) AS json_build_object\n           FROM view_contacts vc\n          WHERE ((vc.id = z.coordinator_gm_id) AND (vc.type = 'employee'::text))) AS coordinator_obj,\n    json_build_object('uuid', ztn.uuid, 'name', ztn.titel) AS case_type_version,\n    json_build_object('uuid', zt.uuid) AS case_type,\n    ARRAY( SELECT json_build_object('naam', bibliotheek_kenmerken.naam, 'magic_string', bibliotheek_kenmerken.magic_string, 'type', bibliotheek_kenmerken.value_type, 'is_multiple', bibliotheek_kenmerken.type_multiple, 'value', ARRAY( SELECT json_build_object('md5', filestore.md5, 'size', filestore.size, 'uuid', filestore.uuid, 'filename', (file.name || (file.extension)::text), 'mimetype', filestore.mimetype, 'is_archivable', filestore.is_archivable, 'original_name', filestore.original_name, 'thumbnail_uuid', filestore.thumbnail_uuid) AS json_build_object\n                   FROM ((file_case_document\n                     JOIN file ON (((file_case_document.file_id = file.id) AND (file_case_document.case_id = z.id))))\n                     JOIN filestore ON ((file.filestore_id = filestore.id)))\n                  WHERE (file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id))) AS json_build_object\n           FROM ((zaaktype_kenmerken\n             JOIN bibliotheek_kenmerken ON (((zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id) AND (bibliotheek_kenmerken.value_type = 'file'::text))))\n             JOIN zaaktype_node ON ((zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id)))\n          WHERE (zaaktype_node.id = z.zaaktype_node_id)) AS file_custom_fields\n   FROM ((((((((zaak z\n     JOIN zaaktype_node ztn ON ((ztn.id = z.zaaktype_node_id)))\n     JOIN zaaktype zt ON ((zt.id = z.zaaktype_id)))\n     LEFT JOIN zaak_meta zm ON ((zm.zaak_id = z.id)))\n     LEFT JOIN zaaktype_resultaten ztr ON ((z.resultaat_id = ztr.id)))\n     LEFT JOIN result_preservation_terms rpt ON ((ztr.bewaartermijn = rpt.code)))\n     LEFT JOIN zaaktype_status zts ON (((z.zaaktype_node_id = zts.zaaktype_node_id) AND (zts.status = (z.milestone + 1)))))\n     LEFT JOIN roles ro ON ((z.route_role = ro.id)))\n     LEFT JOIN groups gr ON ((ro.parent_group_id = gr.id)))\n  WHERE (z.deleted IS NULL)");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  is_nullable: 1

=head2 uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 onderwerp

  data_type: 'text'
  is_nullable: 1

=head2 route_ou

  data_type: 'integer'
  is_nullable: 1

=head2 route_role

  data_type: 'integer'
  is_nullable: 1

=head2 vernietigingsdatum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 archival_state

  data_type: 'text'
  is_nullable: 1

=head2 status

  data_type: 'text'
  is_nullable: 1

=head2 contactkanaal

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 registratiedatum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 streefafhandeldatum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 afhandeldatum

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 stalled_until

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 milestone

  data_type: 'integer'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 behandelaar_gm_id

  data_type: 'integer'
  is_nullable: 1

=head2 coordinator_gm_id

  data_type: 'integer'
  is_nullable: 1

=head2 aanvrager

  data_type: 'integer'
  is_nullable: 1

=head2 aanvraag_trigger

  data_type: 'enum'
  extra: {custom_type_name => "zaaksysteem_trigger",list => ["extern","intern"]}
  is_nullable: 1

=head2 onderwerp_extern

  data_type: 'text'
  is_nullable: 1

=head2 resultaat

  data_type: 'text'
  is_nullable: 1

=head2 resultaat_id

  data_type: 'integer'
  is_nullable: 1

=head2 payment_amount

  data_type: 'numeric'
  is_nullable: 1
  size: [100,2]

=head2 behandelaar

  data_type: 'integer'
  is_nullable: 1

=head2 coordinator

  data_type: 'integer'
  is_nullable: 1

=head2 urgency

  data_type: 'text'
  is_nullable: 1

=head2 urgency_date_medium

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 urgency_date_high

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 preset_client

  data_type: 'boolean'
  is_nullable: 1

=head2 prefix

  data_type: 'text'
  is_nullable: 1

=head2 number_master

  data_type: 'bigint'
  is_nullable: 1

=head2 html_email_template

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 payment_status

  data_type: 'text'
  is_nullable: 1

=head2 price

  data_type: 'text'
  is_nullable: 1

=head2 zaaktype_node_id

  data_type: 'integer'
  is_nullable: 1

=head2 number_parent

  data_type: 'bigint'
  is_nullable: 1

=head2 number_previous

  data_type: 'bigint'
  is_nullable: 1

=head2 lead_time_real

  data_type: 'integer'
  is_nullable: 1

=head2 aanvrager_gm_id

  data_type: 'integer'
  is_nullable: 1

=head2 aanvrager_type

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 zaaktype_id

  data_type: 'integer'
  is_nullable: 1

=head2 deleted

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 confidentiality

  data_type: 'enum'
  extra: {custom_type_name => "confidentiality",list => ["public","internal","confidential"]}
  is_nullable: 1

=head2 current_deadline

  data_type: 'jsonb'
  is_nullable: 1

=head2 deadline_timeline

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_id

  data_type: 'integer'
  is_nullable: 1

=head2 result_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 result

  data_type: 'text'
  is_nullable: 1

=head2 active_selection_list

  data_type: 'text'
  is_nullable: 1

=head2 suspension_rationale

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 premature_completion_rationale

  data_type: 'text'
  is_nullable: 1
  original: {data_type => "varchar"}

=head2 aggregation_scope

  data_type: 'text'
  is_nullable: 1

=head2 type_of_archiving

  data_type: 'text'
  is_nullable: 1

=head2 period_of_preservation

  data_type: 'text'
  is_nullable: 1

=head2 result_description

  data_type: 'text'
  is_nullable: 1

=head2 result_explanation

  data_type: 'text'
  is_nullable: 1

=head2 result_selection_list_number

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_process_type_number

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_process_type_name

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_process_type_description

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_process_type_explanation

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_process_type_object

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_process_type_generic

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_origin

  data_type: 'jsonb'
  is_nullable: 1

=head2 result_process_term

  data_type: 'jsonb'
  is_nullable: 1

=head2 days_left

  data_type: 'integer'
  is_nullable: 1

=head2 progress_days

  data_type: 'text'
  is_nullable: 1

=head2 custom_fields

  data_type: 'json[]'
  is_nullable: 1

=head2 case_actions

  data_type: 'json[]'
  is_nullable: 1

=head2 destructable

  data_type: 'boolean'
  is_nullable: 1

=head2 documents

  data_type: 'text'
  is_nullable: 1

=head2 case_documents

  data_type: 'text'
  is_nullable: 1

=head2 case_role

  data_type: 'json'
  is_nullable: 1

=head2 case_meta

  data_type: 'json'
  is_nullable: 1

=head2 case_status

  data_type: 'json'
  is_nullable: 1

=head2 case_department

  data_type: 'json'
  is_nullable: 1

=head2 case_subjects

  data_type: 'json[]'
  is_nullable: 1

=head2 requestor_obj

  data_type: 'json'
  is_nullable: 1

=head2 assignee_obj

  data_type: 'json'
  is_nullable: 1

=head2 coordinator_obj

  data_type: 'json'
  is_nullable: 1

=head2 case_type_version

  data_type: 'json'
  is_nullable: 1

=head2 case_type

  data_type: 'json'
  is_nullable: 1

=head2 file_custom_fields

  data_type: 'json[]'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "bigint", is_nullable => 1 },
  "uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "onderwerp",
  { data_type => "text", is_nullable => 1 },
  "route_ou",
  { data_type => "integer", is_nullable => 1 },
  "route_role",
  { data_type => "integer", is_nullable => 1 },
  "vernietigingsdatum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "archival_state",
  { data_type => "text", is_nullable => 1 },
  "status",
  { data_type => "text", is_nullable => 1 },
  "contactkanaal",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "registratiedatum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "streefafhandeldatum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "afhandeldatum",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "stalled_until",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "milestone",
  { data_type => "integer", is_nullable => 1 },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "behandelaar_gm_id",
  { data_type => "integer", is_nullable => 1 },
  "coordinator_gm_id",
  { data_type => "integer", is_nullable => 1 },
  "aanvrager",
  { data_type => "integer", is_nullable => 1 },
  "aanvraag_trigger",
  {
    data_type => "enum",
    extra => {
      custom_type_name => "zaaksysteem_trigger",
      list => ["extern", "intern"],
    },
    is_nullable => 1,
  },
  "onderwerp_extern",
  { data_type => "text", is_nullable => 1 },
  "resultaat",
  { data_type => "text", is_nullable => 1 },
  "resultaat_id",
  { data_type => "integer", is_nullable => 1 },
  "payment_amount",
  { data_type => "numeric", is_nullable => 1, size => [100, 2] },
  "behandelaar",
  { data_type => "integer", is_nullable => 1 },
  "coordinator",
  { data_type => "integer", is_nullable => 1 },
  "urgency",
  { data_type => "text", is_nullable => 1 },
  "urgency_date_medium",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "urgency_date_high",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "preset_client",
  { data_type => "boolean", is_nullable => 1 },
  "prefix",
  { data_type => "text", is_nullable => 1 },
  "number_master",
  { data_type => "bigint", is_nullable => 1 },
  "html_email_template",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "payment_status",
  { data_type => "text", is_nullable => 1 },
  "price",
  { data_type => "text", is_nullable => 1 },
  "zaaktype_node_id",
  { data_type => "integer", is_nullable => 1 },
  "number_parent",
  { data_type => "bigint", is_nullable => 1 },
  "number_previous",
  { data_type => "bigint", is_nullable => 1 },
  "lead_time_real",
  { data_type => "integer", is_nullable => 1 },
  "aanvrager_gm_id",
  { data_type => "integer", is_nullable => 1 },
  "aanvrager_type",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "zaaktype_id",
  { data_type => "integer", is_nullable => 1 },
  "deleted",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "confidentiality",
  {
    data_type => "enum",
    extra => {
      custom_type_name => "confidentiality",
      list => ["public", "internal", "confidential"],
    },
    is_nullable => 1,
  },
  "current_deadline",
  { data_type => "jsonb", is_nullable => 1 },
  "deadline_timeline",
  { data_type => "jsonb", is_nullable => 1 },
  "result_id",
  { data_type => "integer", is_nullable => 1 },
  "result_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "result",
  { data_type => "text", is_nullable => 1 },
  "active_selection_list",
  { data_type => "text", is_nullable => 1 },
  "suspension_rationale",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "premature_completion_rationale",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "aggregation_scope",
  { data_type => "text", is_nullable => 1 },
  "type_of_archiving",
  { data_type => "text", is_nullable => 1 },
  "period_of_preservation",
  { data_type => "text", is_nullable => 1 },
  "result_description",
  { data_type => "text", is_nullable => 1 },
  "result_explanation",
  { data_type => "text", is_nullable => 1 },
  "result_selection_list_number",
  { data_type => "jsonb", is_nullable => 1 },
  "result_process_type_number",
  { data_type => "jsonb", is_nullable => 1 },
  "result_process_type_name",
  { data_type => "jsonb", is_nullable => 1 },
  "result_process_type_description",
  { data_type => "jsonb", is_nullable => 1 },
  "result_process_type_explanation",
  { data_type => "jsonb", is_nullable => 1 },
  "result_process_type_object",
  { data_type => "jsonb", is_nullable => 1 },
  "result_process_type_generic",
  { data_type => "jsonb", is_nullable => 1 },
  "result_origin",
  { data_type => "jsonb", is_nullable => 1 },
  "result_process_term",
  { data_type => "jsonb", is_nullable => 1 },
  "days_left",
  { data_type => "integer", is_nullable => 1 },
  "progress_days",
  { data_type => "text", is_nullable => 1 },
  "custom_fields",
  { data_type => "json[]", is_nullable => 1 },
  "case_actions",
  { data_type => "json[]", is_nullable => 1 },
  "destructable",
  { data_type => "boolean", is_nullable => 1 },
  "documents",
  { data_type => "text", is_nullable => 1 },
  "case_documents",
  { data_type => "text", is_nullable => 1 },
  "case_role",
  { data_type => "json", is_nullable => 1 },
  "case_meta",
  { data_type => "json", is_nullable => 1 },
  "case_status",
  { data_type => "json", is_nullable => 1 },
  "case_department",
  { data_type => "json", is_nullable => 1 },
  "case_subjects",
  { data_type => "json[]", is_nullable => 1 },
  "requestor_obj",
  { data_type => "json", is_nullable => 1 },
  "assignee_obj",
  { data_type => "json", is_nullable => 1 },
  "coordinator_obj",
  { data_type => "json", is_nullable => 1 },
  "case_type_version",
  { data_type => "json", is_nullable => 1 },
  "case_type",
  { data_type => "json", is_nullable => 1 },
  "file_custom_fields",
  { data_type => "json[]", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-02-28 15:30:56
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:KslUahilbWRXS+poGTLwLA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
