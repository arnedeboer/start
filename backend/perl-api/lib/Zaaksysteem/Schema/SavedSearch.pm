use utf8;
package Zaaksysteem::Schema::SavedSearch;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SavedSearch

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<saved_search>

=cut

__PACKAGE__->table("saved_search");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 kind

  data_type: 'text'
  is_nullable: 0

=head2 owner_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 filters

  data_type: 'jsonb'
  default_value: '{}'
  is_nullable: 0

=head2 permissions

  data_type: 'text[]'
  default_value: '{}'::text[]
  is_nullable: 0

=head2 sort_column

  data_type: 'text'
  is_nullable: 0

=head2 sort_order

  data_type: 'text'
  default_value: 'asc'
  is_nullable: 0

=head2 columns

  accessor: 'column_columns'
  data_type: 'jsonb'
  default_value: '[]'
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp with time zone'
  is_nullable: 1
  timezone: 'UTC'

=head2 date_updated

  data_type: 'timestamp with time zone'
  is_nullable: 1
  timezone: 'UTC'

=head2 updated_by

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "kind",
  { data_type => "text", is_nullable => 0 },
  "owner_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "filters",
  { data_type => "jsonb", default_value => "{}", is_nullable => 0 },
  "permissions",
  {
    data_type     => "text[]",
    default_value => \"'{}'::text[]",
    is_nullable   => 0,
  },
  "sort_column",
  { data_type => "text", is_nullable => 0 },
  "sort_order",
  { data_type => "text", default_value => "asc", is_nullable => 0 },
  "columns",
  {
    accessor      => "column_columns",
    data_type     => "jsonb",
    default_value => "[]",
    is_nullable   => 0,
  },
  "date_created",
  {
    data_type   => "timestamp with time zone",
    is_nullable => 1,
    timezone    => "UTC",
  },
  "date_updated",
  {
    data_type   => "timestamp with time zone",
    is_nullable => 1,
    timezone    => "UTC",
  },
  "updated_by",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<saved_search_owner_id_name_key>

=over 4

=item * L</owner_id>

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("saved_search_owner_id_name_key", ["owner_id", "name"]);

=head2 C<saved_search_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("saved_search_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 owner_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "owner_id",
  "Zaaksysteem::Schema::Subject",
  { id => "owner_id" },
);

=head2 saved_search_label_mappings

Type: has_many

Related object: L<Zaaksysteem::Schema::SavedSearchLabelMapping>

=cut

__PACKAGE__->has_many(
  "saved_search_label_mappings",
  "Zaaksysteem::Schema::SavedSearchLabelMapping",
  { "foreign.saved_search_id" => "self.id" },
  undef,
);

=head2 saved_search_permissions

Type: has_many

Related object: L<Zaaksysteem::Schema::SavedSearchPermission>

=cut

__PACKAGE__->has_many(
  "saved_search_permissions",
  "Zaaksysteem::Schema::SavedSearchPermission",
  { "foreign.saved_search_id" => "self.id" },
  undef,
);

=head2 updated_by

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "updated_by",
  "Zaaksysteem::Schema::Subject",
  { id => "updated_by" },
);


# Created by DBIx::Class::Schema::Loader v0.07051 @ 2023-02-23 19:28:26
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:s1TYrlwVYxfw7shSCoFfIA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
