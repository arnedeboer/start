use utf8;
package Zaaksysteem::Schema::Dashboard;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::Dashboard

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<dashboard>

=cut

__PACKAGE__->table("dashboard");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 1
  size: 16

=head2 widgets

  data_type: 'jsonb'
  default_value: '[]'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "uuid",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 1, size => 16 },
  "widgets",
  { data_type => "jsonb", default_value => "[]", is_nullable => 0 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<dashboard_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("dashboard_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 uuid

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to("uuid", "Zaaksysteem::Schema::Subject", { uuid => "uuid" });


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-07-22 14:04:12
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:qOI4I2dx87AwE4KkEyPyNA
use JSON::XS qw();

__PACKAGE__->inflate_column('widgets', {
    inflate => sub { JSON::XS->new->decode(shift // '[]') },
    deflate => sub { JSON::XS->new->encode(shift // []) },
});

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
