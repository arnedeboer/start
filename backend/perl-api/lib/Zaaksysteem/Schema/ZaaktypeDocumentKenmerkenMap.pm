use utf8;
package Zaaksysteem::Schema::ZaaktypeDocumentKenmerkenMap;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaaktypeDocumentKenmerkenMap

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype_document_kenmerken_map>

=cut

__PACKAGE__->table("zaaktype_document_kenmerken_map");

=head1 ACCESSORS

=head2 zaaktype_node_id

  data_type: 'integer'
  is_nullable: 1

=head2 referential

  data_type: 'boolean'
  is_nullable: 1

=head2 bibliotheek_kenmerken_id

  data_type: 'integer'
  is_nullable: 1

=head2 magic_string

  data_type: 'text'
  is_nullable: 1

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 public_name

  data_type: 'text'
  is_nullable: 1

=head2 case_document_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 case_document_id

  data_type: 'integer'
  is_nullable: 1

=head2 show_on_pip

  data_type: 'boolean'
  is_nullable: 1

=head2 show_on_website

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "zaaktype_node_id",
  { data_type => "integer", is_nullable => 1 },
  "referential",
  { data_type => "boolean", is_nullable => 1 },
  "bibliotheek_kenmerken_id",
  { data_type => "integer", is_nullable => 1 },
  "magic_string",
  { data_type => "text", is_nullable => 1 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "public_name",
  { data_type => "text", is_nullable => 1 },
  "case_document_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "case_document_id",
  { data_type => "integer", is_nullable => 1 },
  "show_on_pip",
  { data_type => "boolean", is_nullable => 1 },
  "show_on_website",
  { data_type => "boolean", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-08-16 10:52:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:NfednekEHQoFalJzcHoKYQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
