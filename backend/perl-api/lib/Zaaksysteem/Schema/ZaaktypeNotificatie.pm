use utf8;
package Zaaksysteem::Schema::ZaaktypeNotificatie;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ZaaktypeNotificatie

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<zaaktype_notificatie>

=cut

__PACKAGE__->table("zaaktype_notificatie");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'zaaktype_notificatie_id_seq'

=head2 zaaktype_node_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 zaak_status_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 label

  data_type: 'text'
  is_nullable: 1

=head2 rcpt

  data_type: 'text'
  is_nullable: 1

=head2 onderwerp

  data_type: 'text'
  is_nullable: 1

=head2 bericht

  data_type: 'text'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  is_nullable: 1
  timezone: 'UTC'

=head2 intern_block

  data_type: 'integer'
  is_nullable: 1

=head2 email

  data_type: 'text'
  is_nullable: 1

=head2 bibliotheek_notificaties_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 behandelaar

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 automatic

  data_type: 'integer'
  is_nullable: 1

=head2 cc

  data_type: 'text'
  is_nullable: 1

=head2 bcc

  data_type: 'text'
  is_nullable: 1

=head2 betrokkene_role

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "zaaktype_notificatie_id_seq",
  },
  "zaaktype_node_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "zaak_status_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "label",
  { data_type => "text", is_nullable => 1 },
  "rcpt",
  { data_type => "text", is_nullable => 1 },
  "onderwerp",
  { data_type => "text", is_nullable => 1 },
  "bericht",
  { data_type => "text", is_nullable => 1 },
  "created",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "last_modified",
  { data_type => "timestamp", is_nullable => 1, timezone => "UTC" },
  "intern_block",
  { data_type => "integer", is_nullable => 1 },
  "email",
  { data_type => "text", is_nullable => 1 },
  "bibliotheek_notificaties_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "behandelaar",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "automatic",
  { data_type => "integer", is_nullable => 1 },
  "cc",
  { data_type => "text", is_nullable => 1 },
  "bcc",
  { data_type => "text", is_nullable => 1 },
  "betrokkene_role",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 bibliotheek_notificaties_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::BibliotheekNotificaties>

=cut

__PACKAGE__->belongs_to(
  "bibliotheek_notificaties_id",
  "Zaaksysteem::Schema::BibliotheekNotificaties",
  { id => "bibliotheek_notificaties_id" },
);

=head2 zaak_status_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeStatus>

=cut

__PACKAGE__->belongs_to(
  "zaak_status_id",
  "Zaaksysteem::Schema::ZaaktypeStatus",
  { id => "zaak_status_id" },
);

=head2 zaaktype_node_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::ZaaktypeNode>

=cut

__PACKAGE__->belongs_to(
  "zaaktype_node_id",
  "Zaaksysteem::Schema::ZaaktypeNode",
  { id => "zaaktype_node_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-09-28 09:26:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MoKDrRPriCj1BjZEpsnNnA

__PACKAGE__->resultset_class('Zaaksysteem::DB::ResultSet::ZaaktypeNotificatie');

__PACKAGE__->load_components(
    "+Zaaksysteem::DB::Component::ZaaktypeNotificatie",
    "+Zaaksysteem::Helper::ToJSON",
    __PACKAGE__->load_components()
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

