package Zaaksysteem::XML::Generator::StUF0301::BG0310;
use Moose;

with 'Zaaksysteem::XML::Generator';

=head1 NAME

Zaaksysteem::XML::Generator::StUF0301::BG0310 - Definitions for StUF 0301, Sector BG 0310 XML templates

=head1 SYNOPSIS

    use Zaaksysteem::XML::Generator::StUF0301::BG0310;

    my $generator = Zaaksysteem::XML::Generator::StUF0301::BG0310->new(...);

    # Returns and accepts character strings, not byte strings!
    $xml_data = $generator->search_nps({
        stuurgegevens => { zender => '...', ontvanger => '...', etc. }
    });

    # One method is created for every .xml file in the template directory.

=head1 METHODS

=head2 name

Short name for this module, for use as the accessor name in
L<Zaaksysteem::XML::Compile::Backend>.

=cut

sub name { return 'bg0310' }

=head2 path_prefix

Path (under share/xml-templates) to search for . "stuf0310".

=cut

sub path_prefix {
    return 'stuf0301/bg0310';
}

=head2 _parse_xpc

    $self->_parse_xpc(XML::LibXML->load_xml(string => $xml));

Returns the L<XML::LibXML::XPathContext> for the given libxml object

=cut

sub _parse_xpc {
    my $self        = shift;
    my $libxml      = shift;

    my $xc = XML::LibXML::XPathContext->new($libxml);
    $xc->registerNs('SOAP',  'http://schemas.xmlsoap.org/soap/envelope/');
    $xc->registerNs('st',    'http://www.egem.nl/StUF/StUF0301');
    $xc->registerNs('bg',    'http://www.egem.nl/StUF/sector/bg/0310');

    return $xc;
}

=head2 parser

    $processor->parser(STRING_XML);

Returns a L<XML::LibXML::XPathContext> object

=cut

sub parser {
    my $self        = shift;
    my $xml         = shift;

    my $libxml      = XML::LibXML->load_xml(string => $xml);
    return $self->_parse_xpc($libxml);
}

__PACKAGE__->build_generator_methods();

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
