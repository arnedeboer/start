package Zaaksysteem::Controller::Zaak::Intake;
use Moose;

use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

sub intake_base : Chained('/') : PathPart('zaak/intake') : CaptureArgs(1) {
    my ($self, $c, $file_id) = @_;

    $c->stash->{file} = $c->model('DB::File')->find($file_id);

    if (!$c->stash->{file}) {
        throw '/zaak/intake/download/file_not_found', "File with ID $file_id not found";
    }
}

sub download : Chained('intake_base') : PathPart('download') {
    my ($self, $c, $format) = @_;

    my $file = $c->stash->{file};

    if ($file->get_column('case_id')) {
        throw '/zaak/intake/file_has_case_defined',
            sprintf(
                "File with ID %d is already bound to a case and cannot be downloaded through the document intake.",
                $file->id
            );
    }

    $self->download_allowed($c, $file);

    $c->serve_file($file, $format, $c->req->param('inline'), 1);
}

=head2 download_allowed

Checks if the current authenticated user is allowed to download a document or not.

=cut

sub download_allowed {
    my ($self, $c, $file) = @_;
    my $betrokkene = $c->model('Betrokkene')->get(
        {
            intern  => 0,
            type    => 'medewerker',
        },
        $c->user->uidnumber,
    );

    # If a file is being downloaded that isn't owned by the current authenticated
    # user we validate if it is somehow that has document intake global permissions.
    if ($file->intake_owner ne $betrokkene->betrokkene_identifier &&
        !$c->check_any_user_permission('documenten_intake_all')) {
        throw 'zaak/intake/download_not_allowed', 'This file is not owned by the current user';

    }
    # If the above fails, check if the user is allowed to download personal documents.
    elsif (!$c->check_any_user_permission('documenten_intake_subject')) {
        throw 'zaak/intake/download_not_allowed', 'The current user cannot download from the document intake';
    }

    return 1;
}

=head2 get_visibility_for_user

Call that looks up the permissions of the current user and returns if said user can
view either: no documents, documents assigned to the user or all documents.

=cut

sub get_visibility_for_user : Local {
    my ($self, $c) = @_;

    my $visibility;
    if ($c->check_any_user_permission('documenten_intake_all')) {
        $visibility = [
            {
                value => 'all',
                label => 'Alle documenten'
            },
            {
                value => 'subject',
                label => 'Persoonlijke documenten'
            }
        ];
    }
    elsif ($c->check_any_user_permission('documenten_intake_subject')) {
        $visibility = [
            {
                value => 'subject',
                label => 'Persoonlijke documenten'
            }
        ];
    }

    $c->stash->{json} = $visibility;
    $c->forward('Zaaksysteem::View::JSON');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 download

TODO: Fix the POD

=cut

