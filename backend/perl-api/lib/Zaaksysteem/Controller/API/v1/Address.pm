package Zaaksysteem::Controller::API::v1::Address;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Address - APIv1 controller for Address objects

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Constants::Users qw(:all);

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('public_access');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/address> routing namespace.

=cut

sub base : Chained('/api/v1/base') : PathPart('address') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_user(FORM|PIP|REGULAR);
}

=head2 search

=head3 URL Path

C</api/v1/address/search>

=head3 ES query

You can only use this endpoint with ES query parameters as described in
L<Zaaksysteem::Manual::API::V1::Query::Elasticsearch>.

=cut

sub search : Chained('base') : PathPart('search') {
    my ($self, $c) = @_;

    my $model = $c->model('OverheidIO::BAG');
    my $found = $model->get_bag_objects_from_overheid_io($c->parse_es_query_params);

    $c->stash->{set} = Zaaksysteem::API::v1::ArraySet->new(content => $found);
    $self->list_set($c);
}

has '+namespace' => (
    default => 'address'
);


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
