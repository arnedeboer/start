package Zaaksysteem::Controller::Gegevensmagazijn::Bedrijf;
use Zaaksysteem::Moose;

=head1 NAME

Zaaksysteem::Controller::Gegevensmagazijn::Bedrijf  - Gegevensmagazijn controller for companies

=head1 DESCRIPTION

Implements actions which you can do for companies in the gegevensmagazijn

=cut

use Moose::Util::TypeConstraints qw(enum);

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

sub base : Chained('/') : PathPart('gegevensmagazijn/nnp') : CaptureArgs(0) : ZAPI {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');
    $c->stash->{zapi} = [{ foo => 'bar' }];
}

=head2 delete

Deletes object subscriptions for companies from our system
or inactivates/deletes companies based on if they have cases
or not

=head3 URI

/gegevensmagazijn/nnp/delete

=cut

sub delete : Chained('base') : PathPart('delete') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $total = $self->_handle_object_subscription_for_nnp($c,
        { action => 'disable', label => 'Verwijder' });

    $c->stash->{zapi} = [{ queue_items => $total }];
}


=head2 create

Creates object subscriptions for organisations from our system

=head3 URI

/gegevensmagazijn/np/create

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : ZAPI {
    my ($self, $c) = @_;

    my $total = $self->_handle_object_subscription_for_nnp($c,
        { action => 'enable', label => 'Voeg toe' });

    $c->stash->{zapi} = [{ queue_items => $total }];
}


define_profile _handle_object_subscription_for_nnp => (
    required => {
        action => enum([qw(disable enable)]),
        label  => 'Str',
    }
);

sub _handle_object_subscription_for_nnp {
    my ($self, $c)  = @_;
    my $options = assert_profile($_[2] || {})->valid;

    throw('httpmethod/post', 'Invalid HTTP Method, no POST', [])
        unless lc($c->req->method) eq 'post';

    my $params = $c->req->params;
    my $gm     = $c->model("Gegevensmagazijn::Bedrijf");

    my $search_opts = {};
    my $search      = {};

    $search->{'me.deleted_on'} = undef;
    if ($params->{selection_type} eq 'subset') {
        $search->{'me.id'} = { 'in' => $params->{selection_id} };
    }

    my $nnp = $gm->search($search, $search_opts, %$params);

    my $update = $nnp->search_rs(undef);
    $update->update({pending => 1});

    my (@companies, @subscriptions);

    my $q = $c->model("DB::Queue");
    while (my $company = $nnp->next) {
        if (my $os = $company->subscription_id) {
            push(@subscriptions,
                {
                    priority => 100,
                    label => "$options->{label} object subscription: "
                        . $os->external_id,
                    data => {
                        subscription_id => $os->id,
                        interface_id    => $os->get_column('interface_id'),
                        config_interface_id =>
                            $os->get_column('config_interface_id'),
                    }
                }
            );
        }
        else {
            push(
                @companies,
                {
                    priority => 100,
                    label    => $options->{label}
                        . ' organisatie met ID '
                        . $company->id,
                    data => { bedrijf_id => $company->id }
                }
            );
        }
    }


    $q->create_items($options->{action} . '_bedrijf', @companies);
    $q->create_items($options->{action} . '_object_subscription', @subscriptions);

    return scalar @companies;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
