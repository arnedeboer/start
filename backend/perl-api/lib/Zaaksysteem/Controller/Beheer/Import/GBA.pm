package Zaaksysteem::Controller::Beheer::Import::GBA;

use Moose;
use namespace::autoclean;

use File::Spec::Functions qw/catfile/;
use File::Basename qw/basename/;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('beheer/import/gba'): CaptureArgs(1) {
    my ( $self, $c, $import_id ) = @_;

    $c->assert_any_user_permission('beheer');

    $c->stash->{import_data} = $c->model('DB::BeheerImport')->find($import_id);

    if (!$c->stash->{import_data}) {
        $c->res->redirect($c->uri_for('/beheer/import/gba'));
        $c->detach;
    }
}

sub index : Chained('/') : PathPart('beheer/import/gba'): Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{paging_page} = $c->req->params->{paging_page} || 1;
    $c->stash->{paging_rows} = $c->req->params->{paging_rows} || 25;

    $c->stash->{import_list} = $c->model('DB::BeheerImport')->search(
        {
            importtype  => 'GBA',
        },
        {
            order_by    => { -desc => ['id'] },
            page        => $c->stash->{paging_page},
            rows        => $c->stash->{paging_rows},
        }
    );

    $c->stash->{paging_total}       = $c->stash->{import_list}->pager->total_entries;
    $c->stash->{paging_lastpage}    = $c->stash->{import_list}->pager->last_page;

    $c->stash->{template} = 'beheer/import/gba/list.tt';
}

sub view : Chained('base') : PathPart(''): Args() {
    my ( $self, $c ) = @_;

    $c->stash->{paging_page} = $c->req->params->{paging_page} || 1;
    $c->stash->{paging_rows} = $c->req->params->{paging_rows} || 25;

    $c->stash->{import_log} = $c->stash->{import_data}->beheer_import_logs->search(
        {},
        {
            order_by    => { -desc => ['id'] },
            page        => $c->stash->{paging_page},
            rows        => $c->stash->{paging_rows},
        }
    );

    $c->stash->{paging_total}       = $c->stash->{import_log}->pager->total_entries;
    $c->stash->{paging_lastpage}    = $c->stash->{import_log}->pager->last_page;

    $c->stash->{template}   = 'beheer/import/gba/view.tt'
}


sub run : Local {
    my ( $self, $c ) = @_;

    my $interface   = $c->model('DB::Interface')->search_active({ module => 'importplugin' })->first;

    $c->res->body('FAIL');

    unless ($interface) {
        $c->log->error('Cannot find interface for importplugin');
        return;
    }

    my $config = $interface->get_interface_config;

    unless ($config->{gba_enabled}
        && $config->{gba_module}
        && $config->{gba_filename})
    {
        $c->log->error(
            'Found interface importplugin, but did not find gba import script');
        return;
    }

    my $shortname = $c->config->{instance_uuid} // $c->get_customer_info->{naam_kort};
    $shortname =~ s/\s+//g;
    $shortname = lc($shortname);

    my $filepath = catfile(
        $c->config->{import_home_directory},
        $shortname,
        'import',
        basename($interface->get_interface_config->{gba_filename})
    );

    if (!-f $filepath) {
        $c->log->error(
            "Unable to import GBA from file $filepath, file is missing");
        return;
    }

    $c->log->info('Going to import GBA from file: ' . $filepath);

    $c->model('Beheer::Import::GBA')->import_gba(
        'type'      => $interface->get_interface_config->{gba_module},
        'options'   => {
            'filename'  => $filepath
        }
    );

    $c->res->body('OK');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

=head2 run

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut

