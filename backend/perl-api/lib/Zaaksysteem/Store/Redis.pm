package Zaaksysteem::Store::Redis;
use Moose;

use BTTW::Tools;
use IO::Socket::SSL qw(SSL_VERIFY_PEER);
use JSON::XS;
use MIME::Base64 qw(encode_base64 decode_base64);
use Sereal qw(SRL_SNAPPY);

=head1 NAME

Zaaksysteem::Store::Redis - A Redis client for Zaaksysteem

=head1 DESCRIPTION

A default way to store data in Redis for Zaaksysteem purposes.

=head1 SYNOPSIS

    use Zaaksysteem::Store::Redis;

    my $client = Zaaksysteem::Store::Redis->new(
        server    => 'some.server:port',
        db        => 'optional',
        debug     => 1, # defaults to 0
        reconnect => 1, # defaults to 0
    );

    $client->set("key", "value");
    $client->get("key");

    $client->set_base64("key");
    $client->set_base64("key");

    $client->set_json("key", { data => 'structure' });
    $client->get_json("key");

    $client->set_sereal("key", { data => 'structure' });
    $client->get_sereal("key");

=cut

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 redis

A L<Redis> object, build for you via a builder C<_build_redis>

=cut

has redis => (
    is      => 'rw',
    isa     => 'Redis',
    builder => '_build_redis',
    clearer => '_clear_redis',
    lazy    => 1,
    handles => {
        'has_connection' => 'ping',

        'set'     => 'set',
        'get'     => 'get',

        'del'       => 'del',
        'expire_at' => 'expireat',
        'expire'    => 'expire',
    },
);

=head2 server

A servername, must be a string in C<< server:port >> syntax. Defaults to 
C<< localhost:6379 >>

=cut

has server => (
    is      => 'ro',
    isa     => 'Str',
    default => '127.0.0.1:6379',
);

=head2 debug

Add debug to the Redis object, defaults to false.

=cut

has debug => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0,
);

=head2 ssl

Use TLS/SSL for the Redis connection; defaults to false.

=cut

has ssl => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0,
);

=head2 reconnect

Let the Redis client automaticly reconnect, defaults to false.

=cut

has reconnect => (
    is      => 'ro',
    isa     => 'Int',
    default => 0,
);

has prepend_key => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_prepend_key',
);

=head2 db

Tell Redis to connect to a specific database, ask me via C<has_db>

=cut

has db => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_db'
);

=head2 sereal_encoder

A L<Sereal::Encoder> object, build for you via a builder
C<_build_sereal_encoder>

=cut

has sereal_encoder => (
    is      => 'ro',
    isa     => 'Sereal::Encoder',
    builder => '_build_sereal_encoder',
    reader  => 'get_sereal_encoder',
    lazy    => 1,
);

=head2 sereal_decoder

A L<Sereal::Decoder> object, build for you via a builder
C<_build_sereal_decoder>

=cut

has sereal_decoder => (
    is      => 'ro',
    isa     => 'Sereal::Decoder',
    builder => '_build_sereal_decoder',
    reader  => 'get_sereal_decoder',
    lazy    => 1,
);

=head2 json

A L<JSON::XS> object, build for you via a builder C<_build_json>

=cut

has json => (
    is      => 'ro',
    isa     => 'JSON::XS',
    lazy    => 1,
    builder => '_build_json',
);

=head1 METHODS

=head2 del

See L<Redis/del>.

=head2 has_connection

See L<Redis/ping>

=head2 expire_at

See L<Redis/expireat>

=head2 get_sereal_decoder

Get the sereal decoder object

=head2 get_sereal_encoder

Get the sereal encoder object

=head2 set_json

Set a data structure in Redis as JSON

=cut

sub set_json {
    my ($self, $key, $value) = @_;
    my $data = try {
        $self->json->encode($value);
    }
    catch {
        $self->log->info(
            "Unable to encode json data for redis with key '$key': $_");
        die $_;
    };

    return $self->set_base64($key, $data);

}

=head2 get_json

Get the JSON decoded datastructure from Redis

=cut

sub get_json {
    my ($self, $key) = @_;

    my $data = $self->get_base64($key);
    return unless defined $data;

    return try {
        $self->json->decode($data);
    }
    catch {
        $self->log->info(
            "Unable to decode json data from redis with key '$key': $_");
        die $_;
    };
}

=head2 set_sereal

Set a data structure in Redis as Sereal

=cut

sub set_sereal {
    my ($self, $key, $value) = @_;

    my $data = try {
        $self->get_sereal_encoder->encode($value);
    }
    catch {
        $self->log->info(
            "Unable to encode sereal data for redis with key '$key': $_");
        die $_;
    };

    return $self->set_base64($key, $data);
}

=head2 get_sereal

Get the sereal decoded datastructure from Redis

=cut

sub get_sereal {
    my ($self, $key) = @_;
    my $data = $self->get_base64($key);
    return unless defined $data;

    return try {
        $self->get_sereal_decoder->decode($data);
    }
    catch {
        $self->log->info(
            "Unable to decode sereal data from redis with key '$key': $_");
        die $_;
    };
}

=head2 set

Set a value in Redis, see L<Redis/set>

=cut

around set => sub {
    my ($orig, $self, $key, @data) = @_;

    $key = $self->_get_key($key);

    if ($self->log->is_trace) {
        $self->log->trace(
            sprintf(
                "Setting '%s' in Redis with data: %s",
                $key, dump_terse(\@data)
            )
        );
    }

    return $self->$orig($key, @data);
};

=head2 get

Get a value from Redis, see L<Redis/get>.

=cut

around get => sub {
    my ($orig, $self, $key, @data) = @_;

    $key = $self->_get_key($key);

    $self->log->trace("Getting '$key' from Redis") if $self->log->is_trace;

    return $self->$orig($key, @data);
};

=head2 del

Delete a value from Redis, see L<Redis/del>.

=cut

around del => sub {
    my ($orig, $self, $key, @data) = @_;

    $key = $self->_get_key($key);

    $self->log->trace("Deleting '$key' from Redis") if $self->log->is_trace;

    return $self->$orig($key, @data);
};

=head2 expire_at

Expire a value at in Redis L<Redis/expireat>.

=cut

around expire_at => sub {
    my ($orig, $self, $key, @data) = @_;

    $key = $self->_get_key($key);

    if ($self->log->is_trace) {
        $self->log->trace(
            sprintf("Expire '%s' in Redis at %s", $key, dump_terse(\@data))
        );
    }

    return $self->$orig($key, @data);
};

=head2 expire

Expire a value in Redis L<Redis/expire>.

=cut

around expire => sub {
    my ($orig, $self, $key, @data) = @_;

    $key = $self->_get_key($key);

    if ($self->log->is_trace) {
        $self->log->trace(
            sprintf("Expire '%s' in Redis in %s seconds", $key, dump_terse(\@data))
        );
    }

    return $self->$orig($key, @data);
};

=head2 set_base64

Set a value with by base64 encoding it

=cut

sub set_base64 {
    my ($self, $key, $data, @rest) = @_;

    my $value = try {
        encode_base64($data // '');
    }
    catch {
        $self->log->error("Unable to encode data for '$key': '$_'");
        die $_;
    };

    return $self->set($key, $value, @rest);
};

=head2 get_base64

Get a value by base64 decoding it

=cut

sub get_base64 {
    my ($self, $key, @data) = @_;

    my $data = $self->get($key, @data);
    return unless defined $data;

    return try {
        decode_base64($data);
    }
    catch {
        $self->log->error("Unable to decode data for '$key': '$_'");
        die $_;
    };
};

=head2 get_or_set_json

Get the data from Redis if it not exists, use a callback to set the data and
return this. Allows a third hashref where you can define post actions, such as
setting expire times

    $self->get_or_set_json(
        'key',
        sub {
            return $foo->expensive_call();
        },
        {
            # pick one
            expire => 60,
            expire_at => time(),
        }
    );

=cut

sig get_or_set_json => 'Str,CodeRef,?HashRef';

sub get_or_set_json {
    my ($self, $key, $callback, $opts) = @_;

    if (my $data = $self->get_json($key)) {
        return $data;
    }

    my $result = $callback->();

    $self->set_json($key, $result);

    if (defined $opts->{expire_at}) {
        $self->expire_at($key, $opts->{expire_at});
    }
    elsif (defined $opts->{expire}) {
        $self->expire($key, $opts->{expire});
    }

    return $result;
}

=head2 get_or_set

Get the data from Redis if it not exists, use a callback to set the data and
return this. Allows a third hashref where you can define post actions, such as
setting expire times. Be advised that we cannot set undef in a value, an empty
string is stored in Redis. When the getter finds an empty string it will return
undef.

    $self->get_or_set(
        'key',
        sub {
            return $foo->expensive_call();
        },
        {
            # pick one
            expire => 60,
            expire_at => time(),
        }
    );

=cut

sig get_or_set => 'Str,CodeRef,?HashRef';

sub get_or_set {
    my ($self, $key, $callback, $opts) = @_;

    if (my $data = $self->get($key)) {
        return unless length($data);
        return $data;
    }

    my $result = $callback->() // '';

    $self->set($key, $result);

    if (defined $opts->{expire_at}) {
        $self->expire_at($key, $opts->{expire_at});
    }
    elsif (defined $opts->{expire}) {
        $self->expire($key, $opts->{expire});
    }

    return $result;
}

# Builders and private methods

sub _build_json {
    my $self = shift;
    return JSON::XS->new->convert_blessed->allow_blessed->utf8;
}

# Used to store connections to Redis -- so all "Zaaksysteem::Store::Redis" instances use the same
# Redis connection.
my %redis;

sub _build_redis {
    my $self = shift;

    my $cache_key = $self->server;
    $cache_key .= "|" . $self->db if ($self->has_db);

    if (exists $redis{$cache_key}) {
        my $existing_connection = $redis{$cache_key};

        if ($existing_connection->ping()) {
            return $existing_connection;
        }
    }

    my $connection = Redis->new(
        server    => $self->server,
        debug     => $self->debug,
        reconnect => $self->reconnect,
        ssl       => $self->ssl,
        $self->ssl
            ? (SSL_verify_mode => SSL_VERIFY_PEER)
            : (),
    );

    $connection->select($self->db) if $self->has_db;

    $redis{$cache_key} = $connection;

    return $connection;
}

sub _build_sereal_decoder {
    my $self = shift;
    return Sereal::Decoder->new();
}

sub _build_sereal_encoder {
    my $self = shift;
    return Sereal::Encoder->new(
        {
            compress       => SRL_SNAPPY,
            croak_on_bless => 0,
            dedupe_strings => 1,
        }
    );
}

sub _get_key {
    my ($self, $key) = @_;

    return $key unless $self->has_prepend_key;
    return join(":", $self->prepend_key, $key);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
