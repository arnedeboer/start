package Zaaksysteem::DB::Component::Logging::Subject::Update;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


# THIS CLASS COVERS HISTORIC DATA AND SHOULD NOT BE MODIFIED FURTHER.
# See ZS::DB::Component::Logging::Subject::UpdateContactData for replacement
# implementation.

sub onderwerp {
    my $self = shift;

    return sprintf('Betrokkene "%s" geupdatet', $self->subject->display_name);
}

sub _add_magic_attributes {
    shift->meta->add_attribute('subject' => ( is => 'ro', lazy => 1, default => sub {
        my $self = shift;

        $self->result_source->schema->betrokkene_model->get({}, $self->data->{ subject_id });
    }));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

