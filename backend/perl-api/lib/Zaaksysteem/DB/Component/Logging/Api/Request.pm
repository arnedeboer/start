package Zaaksysteem::DB::Component::Logging::Api::Request;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Logging::Api::Request - Event handler for API
requests

=head1 METHODS

=head2 event_category

Hardcoded to C<api>.

=cut

sub event_category { 'api' }

=head2 onderwerp

Overrides L<Zaaksysteem::Schema::Logging/onderwerp> and provides a
contextualized summary of the event.

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        'API actie "%s" aangeroepen door %s via koppeling "%s".',
        $self->data->{ action },
        $self->data->{ client_ip },
        $self->data->{ interface_name }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
