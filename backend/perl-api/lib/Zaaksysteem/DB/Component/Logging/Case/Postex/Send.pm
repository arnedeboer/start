package Zaaksysteem::DB::Component::Logging::Case::Postex::Send;

use Moose::Role;

with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use HTML::Strip;
use Encode qw(encode_utf8 decode_utf8);
use JSON;

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::SendPostex
- Event message handler for case/send_postex events.

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    my $onderwerp = sprintf(
        'Aangeboden aan Postex: %s (kenmerk %d)',
        join(', ', @{ $data->{filenames} }),
        $data->{ptx_result}{id}
    );

    return $onderwerp;
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);
    my $stripper = HTML::Strip->new;

    my @content;
    if ($data->{recipient}) {
        push(@content,
            "Aan: " . $stripper->parse($data->{recipient}{display_name}));
        push(@content,
            "Adres: " . $stripper->parse($data->{recipient}{address}));
    }
    push(@content, "Onderwerp: " . $stripper->parse($data->{subject}))
        if $data->{subject};
    push(@content, $stripper->parse($data->{body})) if $data->{body};

    $data->{ content }  = join("\n", @content);
    $data->{ expanded } = $JSON::false;

    return $data;
};

=head2 event_category

defines the category, used by the frontend in the timeline, to display the right
icons etc.

=cut

sub event_category { 'contactmoment'; }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2023, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
