package Zaaksysteem::DB::ResultSet::ZaaktypeAuthorisation;

use strict;
use warnings;

use Moose;

extends 'DBIx::Class::ResultSet', 'Zaaksysteem::Zaaktypen::BaseResultSet';

use constant    PROFILE => {
    required        => [qw/
    /],
    optional        => [qw/
    /],
};

sub _validate_session {
    my $self            = shift;
    my $profile         = PROFILE;
    my $rv              = {};

    $self->__validate_session(
        @_,
        $profile,
    );
}

sub _commit_session {
    my $self    = shift;
    my $node    = shift;
    my $data    = shift;
    my $profile = PROFILE;
    my $rv      = {};


    # Remove old authorisations
    if ($node->zaaktype_id) {
        my $old_authorisations  = $self->search(
            {
                zaaktype_id     => $node->get_column('zaaktype_id')
            }
        )->delete;
    }


    # $data = { 1 => $auth, 2 => $auth };
    my @new_list;
    my %mapping;

    foreach my $auth (values %$data) {
        # Make sure "confidential", a not-null value, has a value
        # (default to the database default: FALSE)
        $auth->{confidential} //= 0;

        my $key = join(',', $auth->{ou_id}, $auth->{role_id}, $auth->{confidential});

        # Skip if we already have the correct right in place
        next if $mapping{$key}{$auth->{recht}};

        my @capabilities;

        if ($auth->{recht} eq 'zaak_beheer') {
            @capabilities = qw(zaak_search zaak_read zaak_edit zaak_beheer);
        }
        elsif ($auth->{recht} eq 'zaak_edit') {
            @capabilities = qw(zaak_search zaak_read zaak_edit);
        }
        elsif ($auth->{recht} eq 'zaak_read') {
            @capabilities = qw(zaak_search zaak_read);
        }
        elsif ($auth->{recht} eq 'zaak_search') {
            @capabilities = qw(zaak_search);
        }

        foreach (@capabilities) {
            $mapping{$key}{$_} = 1;
            push(
                @new_list,
                {
                    confidential => $auth->{confidential},
                    ou_id        => $auth->{ou_id},
                    role_id      => $auth->{role_id},
                    recht        => $_,
                }
            );
        }
    }

    my $i = 1;
    $data = { map { $i++ => $_ } @new_list };

    $self->next::method( $node, $data, @_ );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

