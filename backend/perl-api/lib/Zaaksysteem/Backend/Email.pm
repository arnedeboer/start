package Zaaksysteem::Backend::Email;
use Moose;

use Authen::SASL;
use Authen::SASL::Perl::XOAUTH2;
use BTTW::Tools;
use DateTime;
use Email::Address;
use Email::Valid;
use Encode qw(encode_utf8);
use File::Temp;
use HTML::Entities qw(encode_entities);
use IO::All;
use List::Util qw(first all);
use Mail::DKIM::Signer;
use Mail::Track;
use POSIX qw(strftime);
use BTTW::Tools::RandomData qw(generate_uuid_v4);
use Zaaksysteem::Backend::Email::Transport;
use Zaaksysteem::Email::ZTT;
use Zaaksysteem::Object::Model;

with 'MooseX::Log::Log4perl';

=head2 case

The case from which the email is sent, used for retrieval of magic string
properties.

=cut

has case  => (
    is       => 'ro',
    required => 1,
    isa      => 'Zaaksysteem::Schema::Zaak',
);

has schema => (
    is => 'ro',
    isa => 'Zaaksysteem::Schema',
    lazy => 1,
    default => sub {
        my $self = shift;
        return $self->case->result_source->schema;
    }
);

=head2 additional_ztt_context

Used to provide an extra context for magic string retrieval. ZTT will
also source magic strings from this context if present.

=cut

has additional_ztt_context => (
    is => 'rw',
);

=head2 send_case_notification

Extract emails parameters from notifications and defer to send_from_case

=cut

define_profile send_case_notification => (
    required => [qw/notification recipient/],
    optional => [qw/attachments cc bcc additional_ztt_context/],
    typed => {
        recipient => 'Str',
        notification => 'Zaaksysteem::Model::DB::BibliotheekNotificaties'
    }
);

sub send_case_notification {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $notification = $params->{notification};

    return $self->send_from_case({
        recipient      => $params->{recipient},
        subject        => $notification->subject,
        body           => $notification->message,
        attachments    => $params->{attachments},
        sender_address => $notification->sender_address,
        sender         => $notification->sender,
        cc             => $params->{cc},
        bcc            => $params->{bcc},
        log_error      => 1,
        $params->{additional_ztt_context} ? (
            additional_ztt_context => $params->{additional_ztt_context},
        ) : (),
    });
}

=head2 sender

Determine the sender, varying from

=head3 Arguments

=over

=item sender [optional]

The name of the email sender, e.g. 'Internal Affairs Department'

=item from [optional]

The email-address of the sender, e.g. internal.affairs@shield.org

If empty, the default configured from address will be used.

=back

=cut

define_profile sender => (
    optional => [qw/sender_address sender/]
);

sub sender {
    my $self = shift;
    my $params = assert_profile(shift)->valid;

    my $from = defined $params->{sender_address} ? $self->replace_magic_strings($params->{sender_address}) : undef;
    $from ||= $self->schema->resultset('Config')->get_customer_config->{zaak_email};

    my $sender = $params->{sender} && $self->replace_magic_strings($params->{sender});
    my $email = Email::Address->new($sender, $from);

    return $email if $self->schema->is_allowed_mailhost($email->host);
    throw(
        "zaaksysteem/email/not_allowed",
        $email->host . " is not allowed to send e-mail"
    );

}

=head2 send_from_case

Send email based on a slew of case settings.

=cut

define_profile send_from_case => (
    required => [qw/body subject recipient/],
    optional => [
        qw/
            additional_ztt_context
            attachments
            bcc
            cc
            contactmoment
            file_attachments
            sender
            sender_address
            log_error
            sender_subject
            request_id
            recipient_type
            /
    ],
    typed => {
        body    => 'Str',
        subject => 'Str',
        sender_subject => 'Zaaksysteem::Schema::Subject',
    },
    defaults           => { contactmoment => 'balie', log_error => 1 },
    constraint_methods => {
        recipient => sub {
            my ($dfv, $value) = @_;

            my @emails = split /[;,]/, $value;
            return all { Email::Valid->address($_) } @emails;
        },
    },
);

sub send_from_case {
    my $self = shift;
    my $params  = assert_profile(shift)->valid;

    my $case         = $self->case;
    my $schema       = $self->schema;

    $self->additional_ztt_context($params->{additional_ztt_context});

    my $body         = $self->replace_magic_strings($params->{body});
    my $subject      = $self->replace_magic_strings($params->{subject});

    my $interface = $schema->resultset('Interface')->search_active({module => 'emailconfiguration'})->first;

    # For now be backward compatible
    my $mt;
    my $transport;
    my ($dkim, $dkim_key_file);
    my ($html_template, $html_images);
    my $max_size = 10;
    if ($interface) {
        my $config = $interface->get_interface_config;
        my $id = join("-", $case->id, substr($case->get_column('uuid'), -6));
        my $regexp = qr/(\d+-[a-z0-9]{6})/;

        $max_size = $config->{max_size} || 10;
        if ($config->{use_smarthost}) {
            $transport = $self->_create_smtp_transport($interface, $config);
        }

        if ($config->{use_dkim}) {
            my $dkim_selector;
            if ($config->{dkim_key}[0]{id}) {
                $self->log->trace("Creating DKIM signing object from filestore");

                my $keyfile = $schema->resultset('Filestore')->find($config->{dkim_key}[0]{id});
                $dkim_key_file = $keyfile->get_path;
                $dkim_selector = $config->{dkim_selector};
            } else {
                $self->log->trace("Creating DKIM signing object from environment");

                $dkim_key_file = $ENV{DKIM_KEY_FILE};
                $dkim_selector = $ENV{DKIM_CURRENT_SELECTOR};
            }

            unless ($dkim_key_file && $dkim_selector) {
                my $error = "DKIM enabled, but no DKIM key or selector configured.";
                $self->log->error($error);

                throw(
                    "case/mail/dkim_settings",
                    $error,
                );
            }

            try {
                $dkim = Mail::DKIM::Signer->new(
                    Algorithm => $config->{dkim_algorithm} // 'rsa-sha256',
                    Method    => $config->{dkim_method} // 'relaxed',
                    Domain    => $config->{dkim_domain},
                    Selector  => $dkim_selector,
                    KeyFile   => $dkim_key_file,
                );
            }
            catch {
                $self->log->error("Error while sending email: $_");

                throw(
                    "case/email/dkim_signer",
                    'Interne fout bij het versturen van email (DKIM signer)',
                );
            };
        }

        if ($config->{rich_email}) {
            my $template_name = $case->html_email_template // '';
            my @templates = @{ $config->{rich_email_templates} };

            my ($template_to_use) = first {
                $_->{label} eq $template_name
            } @templates;

            # Fallback: HTML mail enabled and at least one template configured
            # but unknown template found in case (type).
            if (@templates and not defined $template_to_use) {
                $template_to_use = $templates[0];
            }

            if ($template_to_use) {
                $html_template = $template_to_use->{template};
                $html_images = $template_to_use->{image};
            }
        }

        $mt = Mail::Track->new(
            subject_prefix_name  => $config->{subject},
            identifier           => $id,
            identifier_regex     => $regexp,
        );

        if (!defined $params->{sender_address}) {
            $params->{sender_address} = $config->{api_user};
        }
        if (!defined $params->{sender} && $config->{sender_name}) {
            $params->{sender} = $config->{sender_name};
        }
    }
    else {
        $mt = Mail::Track->new();
    }

    my $from = $self->sender($params);

    $self->log->debug(sprintf(
        "Sending email. Sender: '%s'; Recipient: '%s'",
        $from->format,
        $params->{recipient},
    ));

    foreach (qw(recipient cc bcc)) {
        $params->{$_} = _sanitize_mail($params->{$_});
    }

    my $msg = $mt->prepare({
        from    => $from->format,
        to      => $params->{recipient},
        subject => $subject,
        cc      => $params->{cc},
        bcc     => $params->{bcc},

        extra_headers => {
            'X-ZS-Request' => $params->{request_id} // 'unknown',
        },

        # Pass in a Mail::DKIM::Signer
        ($dkim)
            ? (dkim => $dkim)
            : (),

        # Override SMTP settings, if requested in interface configuration
        (defined $transport)
            ? (transport => $transport)
            : (),
    });

    $msg->add_body_part({content => encode_utf8($body)});

    if ($html_template) {
        # Escape 
        my $html_escaped_body = encode_entities($body);

        # Single newlines disappear (HTML whitespace folding), double newlines
        # start a new "paragraph".
        $html_escaped_body =~ s/\n\n/<br><br>/g;

        (my $html_body = $html_template) =~ s/\{\{message\}\}/$html_escaped_body/;

        my $image_url = "";
        my @related_parts;
        if ($html_images && @$html_images > 0) {
            my %image_data = %{ $html_images->[0] };
            $image_url = "cid:$image_data{uuid}";

            my $filestore = $schema->resultset('Filestore')->find($image_data{id});

            push @related_parts, {
                "id" => $image_data{uuid},
                "type" => $image_data{mimetype},
                "data" => $filestore->get_path,
                "name" => $image_data{original_name},
            }
        }

        $html_body =~ s/\{\{image_url\}\}/$image_url/;

        $msg->add_body_part({
            content_type => 'text/html',
            content => $html_body,
            related_parts => \@related_parts,
        });
    }

    # Case type documents
    my @attached_files = $params->{attachments} ? $self->_get_files($params->{attachments}) : ();
    my %has_files;

    $self->log->debug("Number of attached files: " . @attached_files);
    foreach (@attached_files) {
        $has_files{ $_->filestore->md5 } = $_->filestore->get_path;

        $msg->add_attachment(
            filename     => $_->filename,
            content_type => $_->filestore->mimetype,
            path         => $has_files{ $_->filestore->md5 },
        );

        $self->log->debug("Added attachment through template: " . $_->filename);
    }

    # Individual files (selected on the document tab etc.)
    if ($params->{file_attachments}) {
        $self->log->debug("Number of specific file attachments: " . @{ $params->{file_attachments} });

        my $files = $schema->resultset('File')->search({id => $params->{file_attachments}});
        while (my $f = $files->next) {
            # Prevent dupes
            next if exists $has_files{ $f->filestore->md5 };

            $has_files{ $f->filestore->md5 } = $f->filestore->get_path;

            $msg->add_attachment(
                filename     => $f->filename,
                content_type => $f->filestore->mimetype,
                path         => $has_files{ $f->filestore->md5 },
            );

            push(@attached_files, $f);

            $self->log->debug("Added specific file attachment: " . $f->filename);
        }
    } else {
        $self->log->debug("No specific file attachments specified.");
    }

    my $size = $msg->size;

    if (!$subject) {
        $self->_log_or_throw(
            {
                log_error => $params->{log_error},
                type      => 'subject',
                message   => "Er is geen onderwerp gevonden",
                subject   => '<geen ingevuld onderwerp>',
            }
        );
        return 0;
    }
    elsif (!$body) {
        $self->_log_or_throw(
            {
                log_error => $params->{log_error},
                type      => 'body',
                message   => "Er is geen mailinhoud gevonden",
                subject   => $subject,
            }
        );
        return 0;
    }
    elsif ($size >= ($max_size * (1000 * 1000))) {
        $self->_log_or_throw(
            {
                log_error => $params->{log_error},
                type      => 'size',
                subject   => $subject,
                message   => sprintf(
                    'De maximale grootte van de mail is overschreden (%.2fMB / Max: %dMB)',
                    ($size / (1000 * 1000)), $max_size
                ),
            }
        );
        return 0;
    }

    # You shouldn't mess with the internals..
    my $msg_subject = $msg->_build_subject;

    my $sent_message = try {
        my $sent = $msg->send;
        return $sent;
    }
    catch {
        $self->_log_or_throw(
            {
                log_error => 1,
                type      => 'send',
                message   => $_,
                subject   => $msg_subject,
            }
        );
        return;
    };
    if ($sent_message) {
        my $requestor = $self->case->aanvrager_object;

        my $args = {
            subject_display_name => $from->phrase || $from->address,
            case_id      => $self->case->id,
            email        => {
                body      => $body,
                subject   => $msg_subject,
                recipient => [ Email::Address->parse($params->{recipient}) ],
                cc        => [ Email::Address->parse($params->{cc}) ],
                bcc       => [ Email::Address->parse($params->{bcc}) ],
                from      => [ $from ],
                attachments =>
                    [$self->_log_attachments(@attached_files)],
            },
            message_source => $sent_message->stringify,
        };

        $self->_create_message_thread($args);

        $self->schema->resultset('Logging')->trigger(
            'case/email/created', {
            component    => 'case',
            component_id => $self->case->id,
            zaak_id      => $self->case->id,
            onderwerp    => 'E-mail toegevoegd',
            data         => {
                case_id => $self->case->id,
                content => "",
                message_type => "E-mail",
            }
        });
    } else {
        $self->log->error("Empty message sent?");
    }

    return;
}

sub _create_smtp_transport {
    my $self = shift;
    my ($interface, $config) = @_;

    my %smtp_options = (
        ssl => 'starttls',
        ssl_options => {
            # Use system-default SSL
            SSL_ca_path => '/etc/ssl/certs'
        },
    );

    if (($config->{kind} //'') eq 'microsoft') {
        my $interface_module = $interface->module_object;
        my $access_token = $interface_module->refresh_access_token($interface);

        $smtp_options{host} = 'smtp.office365.com';
        $smtp_options{port} = 587;

        $smtp_options{sasl} = Authen::SASL->new(
            mechanism => 'XOAUTH2',
            callback => {
                user => $config->{ms_username},
                access_token => $access_token,
            },
        );

        $self->log->debug(sprintf(
            "Creating SMTP transport for host:port '%s:%d', oauth2 address: '%s'",
            $smtp_options{host},
            $smtp_options{port},
            $config->{ms_username},
        ));
    } else {
        $smtp_options{host} = $config->{smarthost_hostname};
        $smtp_options{port} = $config->{smarthost_port};

        if ($config->{smarthost_username} && $config->{smarthost_password}) {
            $smtp_options{sasl_username} = $config->{smarthost_username};
            $smtp_options{sasl_password} = $config->{smarthost_password};
        }

        $self->log->debug(sprintf(
            "Creating SMTP transport for host:port '%s:%d', username '%s' (%s)",
            $smtp_options{host},
            $smtp_options{port},
            ($smtp_options{sasl_username} ? $smtp_options{sasl_username} : 'without username'),
            ($smtp_options{sasl_password} ? "with password" : "without password")
        ));
    }

    return Zaaksysteem::Backend::Email::Transport->new(%smtp_options);
}

sub _log_or_throw {
    my $self = shift;
    my $params = shift;

    $self->log->error("Error while sending mail: $params->{message}");
    if ($params->{log_error}) {
        my $logging = $self->schema->resultset('Logging');
        $logging->trigger(
            'case/email', {
            component    => 'case',
            component_id => $self->case->id,
            zaak_id      => $self->case->id,
            data         => {
                destination => "mail",
                subject => $params->{subject},
                message => $params->{message},
                error   => 1,
            }
        });
        return;
    }
    throw("case/email/$params->{type}",  $params->{message});
}

=head2 replace_magic_strings

Interpolate magic strings like [[this_is_a_magic_string]]
in the given string, using information from the case.

=cut

sub replace_magic_strings {
    my ($self, $body) = @_;

    throw 'email/replace_magic_strings/empty_body', 'Geen bericht aangeboden',
        unless defined $body;

    my $ztt = Zaaksysteem::ZTT->new;

    $ztt->add_context($self->case);

    $ztt->add_context($self->additional_ztt_context)
        if $self->additional_ztt_context;

    return $ztt->process_template($body)->string;
}

=head2 retrieve_attachments

Given a list of file objects, retrieve the actual files and create Email::MIME
objects.

=cut

sub _retrieve_attachments {
    my ($self, @attached_files) = @_;

    return map { Email::MIME->create(
            attributes => {
                filename     => $_->filename,
                content_type => $_->filestore->mimetype,
                encoding     => 'base64',
                disposition  => 'attachment',
                name         => $_->filename,
            },
            body => io($_->filestore->get_path),
        )
    } @attached_files;
}

=head2 _log_attachments

Transform a list of attached files into a format that will fit into
what contactmoment_create expects.

=cut

sub _log_attachments {
    my ($self, @attached_files) = @_;

    return map {
        {
            filename       => $_->filename,
            file_id        => $_->id,
            filestore_id   => $_->get_column('filestore_id'),
            email_filename => $_->filename,
        }
    } @attached_files;
}

=head2 _get_files

Retrieve file objects given a list of zaaktype_kenmerken_ids.

=cut

sub _get_files {
    my ($self, $zaaktype_kenmerken_ids) = @_;

    throw('email/get_files', 'Systeemfout: lijst met zaaktype kenmerk ids verwacht')
        unless ref $zaaktype_kenmerken_ids eq 'ARRAY';

    my $schema = $self->schema;
    my $rs = $schema->resultset('ZaaktypeKenmerken')->search_rs(
        {
            'id' => $zaaktype_kenmerken_ids,
        },
        {
            columns  => ['bibliotheek_kenmerken_id'],
            distinct => 1.
        },
    );

    my $rv = $schema->resultset('File')->search(
        {
            'case_documents.bibliotheek_kenmerken_id' => { -in => $rs->as_query },
            'me.case_id'                      => $self->case->id,
            'me.date_deleted'                 => undef,
            'me.accepted'                     => 1,
            'me.active_version'               => 1,
        },
        { join => { case_documents => 'file_id' }, }
    );

    if ($self->log->is_debug) {
        my $case_id = $self->case->id;
        my @found;
        while (my $file = $rv->next) {
            push(@found, $file);
            $self->log->debug(
                sprintf("Filename %s with ID %d found for mail attachement in case %d", $file->filename, $file->id, $case_id)
            );
        }
        return @found;
    }
    return $rv->all;
}


sub _sanitize_mail {
    my $value  = shift // '';
    my @emails = split /\s*[;,]\s*/, $value;
    return join(',', grep { Email::Valid->address($_) } @emails);
}

sub _create_message_thread {
    my $self = shift;
    my $args = shift;

    if (not $args->{case_id}) {
        $self->log->warn("No case ID found. Not saving email in thread list.");
        return;
    }

    my $message_slug = substr($args->{email}{body}, 0, 180);

    my $attachment_count = scalar @{ $args->{email}{attachments} };
    my $thread = $self->schema->resultset('Thread')->create({
        contact_uuid => undef,
        contact_displayname => undef,
        case_id => $args->{case_id},
        thread_type => 'external',
        last_message_cache => {
            message_type => 'email',
            slug => $message_slug,
            subject => $args->{email}{subject},
            created_name => $args->{subject_display_name},
            created => strftime('%Y-%m-%dT%H:%M:%SZ', gmtime)
        },
        unread_pip_count => 0,
        unread_employee_count => 0,
        attachment_count => $attachment_count,
    });

    my $participants = [];
    for my $role (qw(recipient cc bcc from)) {
        for my $participant (grep { defined } @{ $args->{email}{$role} }) {
            my $participant_role;
            if ($role eq 'recipient') {
                $participant_role = 'to';
            }
            else {
                $participant_role = $role;
            }

            push @$participants, {
                role => $participant_role,
                display_name => $participant->phrase,
                address => $participant->address,
            };
        }
    }

    $self->log->trace("Participants: " . dump_terse($participants));

    my $message_uuid = generate_uuid_v4();
    my $filestore = $self->_upload_message_source(
        message_source => $args->{message_source},
        message_uuid => $message_uuid,
    );

    my $message_ext = $self->schema->resultset('ThreadMessageExternal')->create({
        type => 'email',
        content => $args->{email}{body},
        subject => $args->{email}{subject},
        direction => 'outgoing',
        participants => $participants,
        source_file_id => $filestore->id,
        read_pip => DateTime->now(),
        read_employee => DateTime->now(),
        attachment_count => $attachment_count,
    });

    my $message = $self->schema->resultset('ThreadMessage')->create({
        thread_id => $thread->id,
        thread_message_external_id => $message_ext->id,
        type => 'external',
        message_slug => $message_slug,
        created_by_uuid => $args->{subject_uuid},
        created_by_displayname => $args->{subject_display_name},
        uuid => $message_uuid,
    });

    my $message_id = $message->id;

    for my $attachment (@{ $args->{email}{attachments}}) {
        $self->schema->resultset('ThreadMessageAttachment')->create({
            filestore_id => $attachment->{filestore_id},
            filename => $attachment->{filename},
            thread_message_id => $message_id,
        });
    }

    return;
}

sub _upload_message_source {
    my $self = shift;
    my %args = @_;

    # write message_source to tempfile
    my $temp_file = File::Temp->new();
    print $temp_file $args{message_source};
    $temp_file->close();

    return $self->schema->resultset('Filestore')->filestore_create({
        original_name => sprintf("message-%s.eml", $args{message_uuid}),
        file_path => "$temp_file",
        ignore_extension => 1,
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
