package Zaaksysteem::Backend::Subject::ResultSet;

use Moose;

extends 'Zaaksysteem::Backend::ResultSet';

with qw[
    MooseX::Log::Log4perl
    Zaaksysteem::BR::Subject::ResultSet::Employee
];

=head1 NAME

Zaaksysteem::Backlend::Subject::ResultSet - Interface to C<subject> resultsets.

=head1 SYNOPSIS

=cut

use Crypt::SaltedHash;
use Hash::Merge qw//;
use Moose::Util::TypeConstraints qw[enum];

use BTTW::Tools;
# TODO: Investige the use of ZS::B::S:Modules
use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Constants;
use MooseX::Types::Moose qw(Int);
use Zaaksysteem::Types qw(NonEmptyStr ObjectSubjectType UUID);

=head1 CONSTANTS

=head2 ROLE_PROFILES

This constant embeds L<Data::FormValidator>-compatible validation profiles for
subjects by C<subject_type>.

=cut

use constant ROLE_PROFILES  => {
    'employee'  => {
        required    => [qw/sn givenname initials mail displayname/],
        optional    => [qw/telephonenumber title password/],
    }
};

=head1 ATTRIBUTES

=head2 _get_role_profiles

Public(?) accessor for the L</ROLE_PROFILES> constant.

=cut

has '_get_role_profiles' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return ROLE_PROFILES();
    }
);

=head1 METHODS

=head2 employees

Convenience search method for retrieval of C<employee>-typed subjects.

    my $employees = $rs->employees;

    # OR

    my @employees = $rs->employees;

    # OR

    my $active_employees = $rs->search_active->employees;

=cut

sub employees {
    my $self = shift;

    return $self->search({ subject_type => 'employee' });
}

=head2 search_active

Drill down the resultset so that only active subjects are returned.

    my $active_users = $rs->search_active;

    # OR

    my @active_users = $rs->search_active;

=cut

sub search_active {
    my $self = shift;

    my $active = $self->search(
        {
            'user_entities.date_deleted' => undef,
            'user_entities.active'       => 1,
        },
        { join => 'user_entities' }
    );

    return $active->search(@_);
}

=head2 create_from_eidas

Create a subject + user entity from the information we get from an eIDAS login.

=cut

define_profile create_from_eidas => (
    required => {
        idp_id => Int,
        uuid => UUID,
        subject_type => ObjectSubjectType,
        username => NonEmptyStr,
    },
);

sub create_from_eidas {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $subject = $self->create({
        uuid => $opts->{uuid},
        subject_type => $opts->{subject_type},
        username => $opts->{username},
    });

    my $user_entity = $subject->create_related(
        'user_entities',
        {
            source_identifier => $opts->{username},
            source_interface_id => $opts->{idp_id},
        }
    );

    return $user_entity;
}

=head2 create_from_saml

Create a subject based on a hash of SAML authentication info (as created by
L<Zaaksysteem::SAML> when a user logs in).

=cut

define_profile create_from_saml => (
    required => [
        'interface_id',
        'saml_data',
    ],
);

sub create_from_saml {
    my $self = shift;
    my $params = assert_profile(
        shift || {}
    )->valid;

    my $username        = $params->{saml_data}{uid};
    my $interface_id    = $params->{interface_id};

    my $user_entity     = $self->create_user(
        {
            username    => $username,
            interface   => $self->result_source->schema->resultset('Interface')->find($interface_id),
            empty_user  => 1,
        }
    );

    my $subject         = $user_entity->subject_id;

    $subject->update_properties_from_saml(
        $params->{saml_data},
        {
            interface_id => $interface_id,
            created      => 1,
        },
    );
    $subject->login_entity($user_entity);

    return $subject;
}

=head2 create_user

Create a subject + user entity, given a username and an interface id.

Returns ($subject, $user_entity).

=cut

has 'default_create_user_profile' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return {
            required    => [qw/
                username

                interface
            /],
            optional    => [qw/
                role_ids

                set_behandelaar
                group_id

                empty_user
            /],
            # require_some => {
            #     'group_id_or_queue' => [1, qw/group_id queue/],
            # }
        };
    }
);

sub _get_create_user_profile {
    my $self        = shift;
    my $type        = shift;

    my $profile     = $self->default_create_user_profile;

    return $profile unless ROLE_PROFILES->{$type};

    return Hash::Merge::merge($profile, ROLE_PROFILES->{$type});
}

sub create_user {
    my $self        = shift;
    my $rparams     = shift || {};

    ### Validation
    my $opts        = assert_profile(
        $rparams,
        profile => ($rparams->{empty_user} ? $self->default_create_user_profile : $self->_get_create_user_profile($rparams->{interface}->module_object->subject_type))
    )->valid;

    # There's a unique index on these fields, so it can only return one entry.
    my $existing_entity = $self->result_source->schema->resultset('UserEntity')->search(
        {
            source_identifier   => $opts->{username},
            source_interface_id => $opts->{interface}->id,
        },
    )->first;

    throw(
        'subject/create_user/exists',
        'Error creating subject, entity already exists in database'
    ) if $existing_entity && $existing_entity->active;

    my $subject_data = {
        subject_type => (
            $opts->{interface}->module_object->subject_type
        ),
        username     => lc($opts->{username}),
    };

    my $subject_rs = $self->search($subject_data);

    my $subject;
    if ($subject_rs->count) {
        $subject = $subject_rs->first;
    }
    else {
        $subject = $self->create($subject_data);
    }

    my $user_entity;
    if ($existing_entity) {
        $existing_entity->update(
            {
                date_deleted => undef,
                subject_id   => $subject->id,
                active       => 1,
            }
        );
        $user_entity = $existing_entity;
    } else {
        my $user_entity_data = {
            source_identifier   => $opts->{username},
            source_interface_id => $opts->{interface}->id,
            subject_id          => $subject->id,
            active              => 1,
        };

        if (defined $opts->{password}) {
            my $csh = Crypt::SaltedHash->new(algorithm => 'SHA-1');
            $csh->add($opts->{password});
            my $salted = $csh->generate;

            $user_entity_data->{password} = $salted;
        }

        $user_entity = $subject->user_entities->create($user_entity_data);
    }

    unless ($rparams->{empty_user}) {
        $subject->update_user($opts);
    }

    return $user_entity;
}

=head2 search_by_group_id

Arguments: $NUMBER_GROUP_ID

Return value: L<Zaaksysteem::Backend::Subject::ResultSet>

    $rs->search_by_group_id(44);

Will return a resultset containing all subject who belong to given group_id

=cut

sub search_by_group_id {
    my $self            = shift;
    my $group_id        = shift;

    throw(
        'subject/search_by_group_id/invalid_id',
        'Given ID not valid integer'
    ) unless $group_id && $group_id =~ /^\d+$/;

    return $self->search(
        {
            $group_id => \'= ANY (group_ids)',
        },
    );
}

=head2 create_nobody_user

Create a nobody user in the system.

=cut

define_profile create_nobody_user => (
    required => { username => 'Str' },
    optional => {
        subject_type => enum([ SUBJECT_TYPE_EMPLOYEE ]),
        system_user  => 'Bool',
    },
    defaults => {
        subject_type => SUBJECT_TYPE_EMPLOYEE,
        system_user  => 0,
    },
    field_filters => {
        username     => ['lc'],
        subject_type => ['lc']
    }
);

sub create_nobody_user {
    my $self = shift;
    my $opt = assert_profile({@_})->valid;

    my $username = $opt->{username};

    my $rs = $self->search_rs({username => $username});
    my $c = $rs->count;
    if ($c > 1) {
        throw("subject/multiple_users_found", "Multiple users found with username '$username': $c");
    }
    elsif ($c == 1) {
        throw("subject/user_found", "There is already a user with username '$username'");
    }

    my $subject = $rs->create(
        {
            username     => $username,
            subject_type => $opt->{subject_type},
            properties   => {
                cn              => $username,
                sn              => $username,
                displayname     => $username,
                givenname       => $username,
                initials        => $username,
                mail            => 'devnull@zaaksysteem.nl',
                telephonenumber => '0612345678',
            },
            nobody => 1,
            system => $opt->{system_user},
        }
    );

    return $subject;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 search_active

TODO: Fix the POD

=cut

