package Zaaksysteem::Backend::Sysin::Roles::UA;
use Moose::Role;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Backend::Sysin::Roles::UA - A user agent role

=head1 SYNOPSIS

    #!perl

    package Foo;
    use Moose;

    with 'Zaaksysteem::Backend::Sysin::Roles::UA';

=head1 DESCRIPTION

A typical interface has some kind backend it needs to communicate with, this role
sets some defaults, such as username, password, and timeouts.

Classes consuming this role may want to override the C<_build_useragent> and C<_build_endpoints> methods.

=cut

use HTTP::Cookies;
use JSON::XS qw(decode_json);
use LWP::UserAgent;

use Zaaksysteem::Types qw(NonEmptyStr);
use BTTW::Tools;

=head1 ATTRIBUTES

=head2 username

The username

=cut

has username => (
    is       => 'ro',
    isa      => NonEmptyStr,
    required => 1,
);

=head2 password

The password

=cut

has password => (
    is       => 'ro',
    isa      => NonEmptyStr,
    required => 1,
);

=head2 ua

An L<LWP::UserAgent> object.
One is created for you, you may feel the need to override certain defaults by implementing your own builder C<_build_useragent>.

=cut

has ua => (
    lazy    => 1,
    is      => 'ro',
    isa     => 'LWP::UserAgent',
    builder => '_build_useragent',
);

=head2 cacert

The path to the CA certificate, if not provided it defaults to whatever the defaults are of L<IO::Socket::SSL>.

=cut

has cacert => (
    is  => 'ro',
    isa => 'Str',
);

=head2 capath

The path where the certificates are stored. If not provided it defaults to C</etc/ssl/certs>.

=cut

has capath => (
    is      => 'ro',
    isa     => 'Str',
    default => '/etc/ssl/certs',
);

=head2 timeout

A timeout value for the useragent

=cut

has timeout => (
    is      => 'ro',
    isa     => 'Int',
    default => 10,
);

=head2 endpoints

A hashref of all the endpoints you have defined. You want to override the builder C<_build_endpoints>.

=cut

has endpoints => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    builder => '_build_endpoints',
);


=head2 assert_http_response

    my $content = $self->assert_http_response;

Asserts a HTTP::Response. Will return the decoded content of the response.

=cut

sub assert_http_response {
    my ($self, $response) = @_;

    if ($self->log->is_trace) {
        $self->log->trace("Got the following response " . dump_terse($response));
    }

    if (!$response->is_success) {
        throw(
            "response/failure",
            sprintf(
                "Response from %s is not succesful (%s): %s",
                $response->request->uri, $response->status_line,
                $response->decoded_content
            )
        );
    }

    return $response->decoded_content;
}

=head2 assert_endpoint

Asserts if the endpoint is defined.

=head3 SYNOPSIS

    my $ok = $self->assert_endpoint('https://foo.bar/Baz');

=cut

sub assert_endpoint {
    my ($self, $endpoint) = @_;

    if (!defined $endpoint) {
        $self->log->error("No endpoint defined in the call");
        throw("endpoint/undefined", "Undefined endpoint in the call, not submitting anything");
    }
    $self->log->trace("Endpoint is defined: $endpoint");
    return 1;
}

=head2 parse_json_response

    my $json = $self->parse_json_response;

Decodes a JSON response and returns the datastructure.

=cut

sub parse_json_response {
    my ($self, $response) = @_;

    my $json = decode_json($self->assert_http_response($response));
    $self->log->trace("Got the following JSON: " . dump_terse($json));
    return $json;
}

=head1 BUILDERS

=head2 _build_useragent

A builder for an L<LWP::UserAgent> object.

=cut

sub _build_useragent {
    my $self  = shift;
    my $agent = LWP::UserAgent->new(
        agent    => "Zaaksysteem",
        ssl_opts => {
            verify_hostname => 1,
            SSL_ca_file     => $self->cacert // undef,
            SSL_ca_path     => $self->capath,
        },
        requests_redirectable => [qw(POST GET HEAD)],
        cookie_jar            => HTTP::Cookies->new(),
        protocols_allowed     => [qw(https)],
        timeout               => $self->timeout // 10,
    );
    return $agent;
}

=head2 _build_endpoints

A builder endpoints defined.:

=cut

sub _build_endpoints { return {} }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
