package Zaaksysteem::Zorginstituut::Message;
use Moose;

=head1 NAME

Zaaksysteem::Zorginstituut::Message - A base class for Zorginstituut message types

=head1 DESCRIPTION

A base class for Zorginstituut message types, extended from
L<Zaaksysteem::Zorginstituut::Model>. You should not instantiate this
class

=cut

extends 'Zaaksysteem::Zorginstituut::Model';
with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=head2 case

An L<Zaaksysteem::Schema::Zaak> object, optional.

=cut

has case => (
    is        => 'rw',
    isa       => 'Zaaksysteem::Schema::Zaak',
    predicate => 'has_case',
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 METHODS

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
