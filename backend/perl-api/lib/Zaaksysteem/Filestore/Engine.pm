package Zaaksysteem::Filestore::Engine;
use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Filestore::Engine - Base

=head1 SYNOPSIS

    package Zaaksysteem::Filestore::Engine::Something;
    use Moose;
    with 'Zaaksysteem::Filestore::Engine';

    sub get_fh { $uuid }
    sub write { $uuid, $fh }
    sub erase { $uuid }

=head1 ATTRIBUTES

=head2 name [Required]

The name (as specified in the configuration) of this engine.

=cut

has name => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head1 REQUIRED METHODS

Classes implementing this role are required to implement the following methods:

=head2 download_url(uuid)

Return a secure direct download URL for the file with temporary validity - on S3,
it's a presigned URL.

=head2 get_path(uuid)

Get the path to the file on a local disk, suitable for "open".

Returning a "self-cleaning" L<File::Temp> handle (which stringifies to the
file name) instead is allowed.

=head2 get_fh(uuid)

Get a file handle to read the file.

=head2 $id = write($uuid, $fh)

Stores a file identified by $uuid, with data from the specified file handle.

=head2 erase($uuid)

Erases (removes or deletes) of file from a Filestore with given $uuid.

=cut

requires 'download_url', 'get_path', 'get_fh', 'write', 'erase';

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
