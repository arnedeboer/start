import {
    openPageAs
} from './../../functions/common/navigate';
import {
    voteButtons,
    voteSave,
    voteTextarea,
    proposals,
    meetings,
    getProposalViewValue
} from './../../functions/meeting/meeting';

describe('when opening a case and adding a note in the meeting app', () => {

    beforeAll(() => {
        openPageAs('burgemeester', '/vergadering/bbv/');

        meetings.click();

        proposals
            .first()
            .click();

        browser.waitForAngular();

        voteButtons
            .first()
            .click();

        voteTextarea.sendKeys('this is my comment');

        voteSave.click();
    });

    it('the note should have the given content', () => {
        expect(getProposalViewValue('Voorstel burgemeester')).toEqual('Akkoord');
        expect(getProposalViewValue('Voorstel burgemeester opmerkingen')).toEqual('this is my comment');
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
