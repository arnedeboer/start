#!/usr/bin/python


import urllib
import json
import sys


# get interface from arguments
if len(sys.argv) > 3:
    base_url = sys.argv[1]
    interface_id = sys.argv[2]
    api_key = sys.argv[3]
    print "base_url: " + base_url
    print "interface_id: " + interface_id
    print "api_key: " + api_key
else:
    print "syntax: export_key2finance.py <base_url> <interface_id> <api_key>"
    exit(1)


def get_url(interface_id, api_call, params):
    "retrieve a url"
    url = base_url + "sysin/interface/" + interface_id + "/trigger/" + api_call + params
    print "getting url: " + url
    response = urllib.urlopen(url, "dummy")
    return response.read()


def get_json(interface_id, api_call, params):
    "retrieve a url, parse the resulting json"
    response = get_url(interface_id, api_call, params)
    return json.loads(response)

# get transaction id from json output

data = get_json(interface_id, "set_case_list", "?api_key=" + api_key)
if data['status_code'] == '500':
    print "error" + str(data)
    exit()
else:
    transaction_id = data['result'][0]['id']
    print "transaction_id: " + str(transaction_id)

# get naw csv
data = get_url(interface_id, "get_csv_naw", "?zapi_format=csv&transaction_id=" + str(transaction_id) + "&api_key=" + api_key)
print "naw_csv:\n" + str(data)


# get deb csv
data = get_url(interface_id, "get_csv_deb", "?zapi_format=csv&transaction_id=" + str(transaction_id) + "&api_key=" + api_key)
print "deb_csv:\n" + str(data)


# close cases - end transaction
#data = get_json(interface_id, "close_cases", "?transaction_id=" + str(transaction_id) + "&api_key=" + api_key)
#print data
error_count = data['result'][0]['error_count']
print "result.error_count (0 = good): " + str(error_count)
