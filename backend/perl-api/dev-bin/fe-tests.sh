#!/bin/bash

set -e

CONTAINER_BUILD_DIR="/opt/zaaksysteem"

for i in client frontend
do
    cd ${CONTAINER_BUILD_DIR}/$i
    npm run lint
    npm test
done

