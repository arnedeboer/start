#!/usr/bin/env perl

use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;
use File::Basename;
use Digest::SHA;
use MIME::Base64;
use Net::LDAP;
use BTTW::Tools qw/throw/;

use Encode qw/decode_utf8/;

use Time::HiRes qw(gettimeofday tv_interval);

use Zaaksysteem::Constants qw/ZAAKSYSTEEM_AUTHORIZATION_ROLES/;

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    n          => 0,
);


{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                config=s
                customer_d=s
                hostname=s
                organization=s
                skip_root=s
                ldapserver=s
                ldapuser=s
                ldappassword=s
                ldapbase=s
                )
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config hostname organization ldapserver ldapuser ldappassword ldapbase)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

$opt{password} = 'zaaksysteem' unless $opt{password};

if (!defined $opt{customer_d}) {
    $opt{customer_d} = catdir(dirname($opt{config}), 'customer.d');
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1);

my $interface   = $schema->resultset('Interface')->find_by_module_name('authldap');

my ($role_dn_to_id, $group_dn_to_id, $role_id_to_old_id, $group_id_to_old_id) = ({},{}, {}, {});


my @SYSTEM_ROLES = qw/
    Administrator
    Zaaksysteembeheerder
    Zaaktypebeheerder
    Zaakbeheerder
    Contactbeheerder
    Basisregistratiebeheerder
    Wethouder
    Directielid
    Afdelingshoofd
    Kcc-medewerker
    Zaakverdeler
    Behandelaar
    Gebruikersbeheerder
/;


my $roles = ZAAKSYSTEEM_AUTHORIZATION_ROLES;

my $ldaproles = {};
for my $role (keys %{ $roles }) {
    my $data    = $roles->{ $role };

    $ldaproles->{ $data->{ldapname} } = $data;
    $ldaproles->{ $data->{ldapname} }->{
        'internalname'
    } = $role;
}

my $subject_counter;

migrate_roles_and_groups($schema);

sub migrate_roles_and_groups {
    my $dbic    = shift;

    # print STDERR 'TREE: ' . Data::Dumper::Dumper($tree);
    do_transaction(
        $dbic,
        sub {
            create_missing_system_roles($dbic);

        }
    );
}

sub create_missing_system_roles {
    my $dbic            = shift;

    my $root_group      = $dbic->resultset('Groups')->search(
        {},
        {
            order_by    => { '-asc' => [\"array_length(me.path, 1)"] },
            rows        => 1
        }
    )->first;

    print STDERR "Creating missing system roles below: " . $root_group->name . "\n";

    for my $role (@SYSTEM_ROLES) {
        next if $dbic->resultset('Roles')->search(
            {
                system_role => 1,
                name        => $role,
            }
        )->count;

        print STDERR "  Adding missing system role: " . $role . "\n";

        $dbic->resultset('Roles')->create_role({
            name        => $role,
            description => $role,
            parent_group_id => $root_group->id,
            system_role => 1
        });
    }

    print STDERR "  [DONE]\n";
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(
        sub {
            $sub->();
            if ($opt{n}) {
                $dbic->txn_rollback;
            }
        }
    );
}

1;

__END__

=head1 NAME

migrate_ldap_to_db.pl - A migration script migrating LDAP users/organizationunits and roles into the db

=head1 SYNOPSIS

migrate_ldap_to_db.pl OPTIONS --organization Mintlab

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * organization

Organization name, to use as primary root of the Organization Tree

=item * skip_root

Some LDAP customers already have a root OU, use that.

=item * ldapserver

    localhost

The LDAP server (hostname) to connect to

=item * ldapuser

    cn=admin,dc=zaaksysteem,dc=nl

The user to bind to LDAP with

=item * ldappassword

    AJASdsaDjsdj

Password to use when logging on to LDAP

=item * ldapbase

    o=zaaksysteem,dc=zaaksysteem,dc=nl

The customer base DN to get the users from

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
