CREATE INDEX result_preservation_terms_code_idx ON public.result_preservation_terms USING btree (code);
CREATE INDEX adres_natuurlijk_persoon_id_idx ON public.adres USING btree (natuurlijk_persoon_id);
CREATE INDEX bibliotheek_kenmerken_magic_string_value_type_idx ON public.bibliotheek_kenmerken USING btree (magic_string, value_type);

CREATE INDEX bibliotheek_kenmerken_values_bibliotheek_kenmerken_id_idx ON public.bibliotheek_kenmerken_values USING btree (bibliotheek_kenmerken_id);
DROP INDEX bibliotheek_kenmerken_values_idx_bibliotheek_kenmerken_id;

CREATE INDEX zaaktype_kenmerken_bibliotheek_kenmerken_id_idx ON public.zaaktype_kenmerken USING btree (bibliotheek_kenmerken_id);

CREATE INDEX zaaktype_kenmerken_zaakstatus_id_idx ON public.zaaktype_kenmerken USING btree (zaak_status_id);
DROP INDEX idx_zaaktype_kenmerken_zaak_status_id;

CREATE INDEX zaaktype_kenmerken_zaaktype_node_id_idx ON public.zaaktype_kenmerken USING btree (zaaktype_node_id);
DROP INDEX zaaktype_kenmerken_zaaktype_node_idx;

CREATE INDEX casetype_authorisation_idx ON public.zaaktype_authorisation USING btree (confidential, ou_id, role_id);
CREATE INDEX zaaktype_authorisation_zaaktype_id_idx ON public.zaaktype_authorisation USING btree (zaaktype_id);
CREATE INDEX zaaktype_authorisation_zaaktype_idx ON public.zaaktype_authorisation USING btree (zaaktype_id, confidential);
CREATE INDEX zaak_group_id_idx ON public.zaak USING btree (route_ou);
CREATE INDEX zaak_role_id_idx ON public.zaak USING btree (route_role);
CREATE INDEX zaak_status_idx ON public.zaak USING btree (status);
CREATE INDEX case_action_case_id_and_casetype_status_id_idx ON public.case_action USING btree (case_id, casetype_status_id);
CREATE INDEX case_action_case_id_idx ON public.case_action USING btree (case_id);

CREATE INDEX case_action_casetype_status_id_idx ON public.case_action USING btree (casetype_status_id);
DROP INDEX idx_case_action_casetype_status_id;

CREATE INDEX file_filestore_id_idx ON public.file USING btree (id, filestore_id);
CREATE INDEX zaaktype_status_status_id_idx ON public.zaaktype_status USING btree (status);
CREATE UNIQUE INDEX filestore_uuid_idx ON public.filestore USING btree (uuid);
CREATE INDEX filestore_virus_found_idx ON public.filestore USING btree (uuid) WHERE (virus_scan_status !~~ 'found%'::text);
CREATE INDEX file_case_document_file_id_idx ON public.file_case_document USING btree (file_id);

CREATE INDEX gm_natuurlijk_persoon_adres_id_idx ON public.gm_natuurlijk_persoon USING btree (adres_id);
DROP INDEX gm_natuurlijk_persoon_idx_adres_id;

CREATE INDEX logging_event_type_created_for_subject_id_zaak_id_idx ON public.logging USING btree (event_type, created_for, (((event_data)::json ->> 'subject_id'::text)), zaak_id);
CREATE INDEX logging_zaak_view_idx ON public.logging USING btree (zaak_id) WHERE (((component)::text = 'zaak'::text) AND (event_type = 'case/view'::text));
CREATE INDEX gm_adres_natuurlijk_persoon_id_idx ON public.gm_adres USING btree (natuurlijk_persoon_id);
CREATE INDEX object_acl_entity_type_id_and_scope_idx ON public.object_acl_entry USING btree (entity_id, entity_type, scope);
CREATE INDEX object_acl_entity_type_id_where_scope_idx ON public.object_acl_entry USING btree (entity_id, entity_type) WHERE (scope = 'instance'::text);
CREATE INDEX object_acl_entity_type_idx ON public.object_acl_entry USING btree (entity_type);
CREATE UNIQUE INDEX object_acl_entry_unique_scope_not_null_idx ON public.object_acl_entry USING btree (object_uuid, entity_type, entity_id, capability, scope, groupname) WHERE (scope IS NOT NULL);

CREATE UNIQUE INDEX natuurlijk_persoon_adres_id_unique_idx ON public.natuurlijk_persoon USING btree (adres_id);
DROP INDEX natuurlijk_persoon_idx_adres_id;

CREATE INDEX queue_data_gin_idx ON public.queue USING gin (((data)::jsonb));
CREATE INDEX queue_status_pending_idx ON public.queue USING btree (status) WHERE (status = 'pending'::text);
CREATE INDEX queue_status_postponed_idx ON public.queue USING btree (status) WHERE (status = 'postponed'::text);
CREATE INDEX queue_type_idx ON public.queue USING btree (type);
CREATE INDEX object_relation_object_id_idx ON public.object_relation USING btree (object_id);
CREATE INDEX object_relation_object_uuid_idx ON public.object_relation USING btree (object_uuid);
CREATE INDEX zaak_meta_zaak_id_idx ON public.zaak_meta USING btree (zaak_id);
CREATE INDEX zaaktype_regel_name_zaaktype_node_id_idx ON public.zaaktype_regel USING btree (zaaktype_node_id, active, is_group) WHERE ((active = true) AND (is_group = false) AND (naam IS NOT NULL));

CREATE INDEX zaaktype_regel_zaak_status_id_idx ON public.zaaktype_regel USING btree (zaak_status_id);
DROP INDEX idx_zaaktype_regel_zaak_status_id;

CREATE INDEX zaaktype_regel_zaaktype_node_id_idx ON public.zaaktype_regel USING btree (zaaktype_node_id);
CREATE INDEX object_subscripton_local_id_idx ON public.object_subscription USING btree (((local_id)::numeric));
CREATE INDEX object_subscripton_local_table_and_id_idx ON public.object_subscription USING btree (((local_id)::numeric), local_table);
CREATE INDEX transaction_error_count_idx ON public.transaction USING btree (error_count);
CREATE INDEX zaak_kenmerk_bibliotheek_kenmerken_id_idx ON public.zaak_kenmerk USING btree (bibliotheek_kenmerken_id);
CREATE INDEX zaak_subcase_zaak_id_idx ON public.zaak_subcase USING btree (zaak_id);
