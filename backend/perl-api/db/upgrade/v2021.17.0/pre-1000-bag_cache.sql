BEGIN;

-- DROP TABLE bag_cache;
CREATE TABLE bag_cache (
    id integer GENERATED ALWAYS AS IDENTITY,
    bag_type varchar NOT NULL,
    bag_id char(16) NOT NULL,
    bag_data jsonb NOT NULL,
    date_created timestamp without time zone NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    last_modified timestamp without time zone NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    refresh_after timestamp without time zone NOT NULL DEFAULT ((CURRENT_TIMESTAMP AT TIME ZONE 'UTC') + '24 hours'::interval),
    PRIMARY KEY (id),
    UNIQUE (bag_type, bag_id),
    CHECK (bag_type IN ('nummeraanduiding', 'openbareruimte'))
);

COMMIT;

