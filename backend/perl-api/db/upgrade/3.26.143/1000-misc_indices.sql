BEGIN;

    CREATE INDEX IF NOT EXISTS file_case_id_idx ON file(case_id);

    CREATE INDEX IF NOT EXISTS
        filemeta_trust_level_idx
    ON
        file_metadata (trust_level);

    CREATE INDEX IF NOT EXISTS
        casetype_authorisation_idx
    ON
        zaaktype_authorisation(confidential, ou_id, role_id);

COMMIT;
