BEGIN;

    ALTER TABLE natuurlijk_persoon ADD COLUMN related_custom_object_id INTEGER NULL REFERENCES custom_object(id);
    ALTER TABLE bedrijf            ADD COLUMN related_custom_object_id INTEGER NULL REFERENCES custom_object(id);

    -- No need to update subject_position_matrix when adding this column:
    ALTER TABLE subject DISABLE TRIGGER USER;
    ALTER TABLE subject ADD COLUMN related_custom_object_id INTEGER NULL REFERENCES custom_object(id);
    ALTER TABLE subject ENABLE TRIGGER USER;

COMMIT;
