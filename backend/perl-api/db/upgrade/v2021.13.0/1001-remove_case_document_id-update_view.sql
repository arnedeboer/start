BEGIN;

    DROP MATERIALIZED VIEW IF EXISTS zaaktype_document_kenmerken_map;
    CREATE MATERIALIZED VIEW zaaktype_document_kenmerken_map AS
    SELECT
        zaaktype_node_id,
        referential,
        bibliotheek_kenmerken_id,
        magic_string,
        name,
        public_name,
        case_document_uuid,
        case_document_id
    FROM (
        SELECT
            ztk.zaaktype_node_id AS zaaktype_node_id,
            ztk.referential AS referential,
            ztk.bibliotheek_kenmerken_id AS bibliotheek_kenmerken_id,
            bk.magic_string AS magic_string,
            bk.naam AS name,
            COALESCE(NULLIF(ztk.label, ''), bk.naam) AS public_name,
            ztk.uuid AS case_document_uuid,
            ztk.id AS case_document_id,
            ROW_NUMBER() OVER (PARTITION BY ztk.bibliotheek_kenmerken_id, ztk.zaaktype_node_id ORDER BY zs.status, ztk.id) AS row_num
        FROM
            zaaktype_kenmerken ztk
            JOIN zaaktype_status zs ON zs.id = ztk.zaak_status_id
            JOIN bibliotheek_kenmerken bk ON bk.id = ztk.bibliotheek_kenmerken_id AND bk.value_type = 'file') t
    WHERE
        t.row_num = 1;

    CREATE INDEX zaaktype_document_kenmerken_map_by_casetype_idx on zaaktype_document_kenmerken_map(zaaktype_node_id, bibliotheek_kenmerken_id);
    CREATE INDEX zaaktype_document_kenmerken_map_by_case_document_id on zaaktype_document_kenmerken_map(zaaktype_node_id, case_document_id);
    CREATE INDEX zaaktype_document_kenmerken_map_by_case_document_uuid on zaaktype_document_kenmerken_map(case_document_uuid);

COMMIT;
