BEGIN;

ALTER TABLE thread_message_attachment_derivative
    DROP CONSTRAINT thread_message_attachment_der_thread_message_attachment_id_fkey,
    ADD CONSTRAINT thread_message_attachment_der_thread_message_attachment_id_fkey
        FOREIGN KEY (thread_message_attachment_id)
        REFERENCES thread_message_attachment(id)
        ON DELETE CASCADE;

COMMIT;
