BEGIN;

ALTER TABLE  custom_object_type ADD COLUMN date_deleted timestamp NULL;

UPDATE custom_object_type ct
SET date_deleted = date_deleted_versions
FROM ( SELECT custom_object_type_id, NULLIF(MAX(COALESCE(date_deleted, 'infinity'::timestamp)), 'infinity'::timestamp) AS date_deleted_versions 
       FROM custom_object_type_version
       GROUP BY custom_object_type_id
     ) subquery
WHERE ct.id = subquery.custom_object_type_id
;

UPDATE custom_object_type SET catalog_folder_id = null WHERE date_deleted IS NOT null;

COMMIT;