BEGIN;

    ALTER TABLE bedrijf DROP CONSTRAINT "bedrijf_related_custom_object_id_fkey";
    ALTER TABLE ONLY bedrijf
    ADD CONSTRAINT bedrijf_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES custom_object(id) ON DELETE SET NULL;

    ALTER TABLE custom_object_relationship DROP CONSTRAINT "custom_object_relationship_custom_object_id_fkey";
    ALTER TABLE ONLY custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_custom_object_id_fkey FOREIGN KEY (custom_object_id) REFERENCES custom_object(id) ON DELETE CASCADE;

    ALTER TABLE custom_object_relationship DROP CONSTRAINT "custom_object_relationship_related_custom_object_id_fkey";
    ALTER TABLE ONLY custom_object_relationship
    ADD CONSTRAINT custom_object_relationship_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES custom_object(id) ON DELETE CASCADE;

    ALTER TABLE custom_object_version DROP CONSTRAINT "custom_object_version_custom_object_id_fkey";
    ALTER TABLE ONLY custom_object_version
    ADD CONSTRAINT custom_object_version_custom_object_id_fkey FOREIGN KEY (custom_object_id) REFERENCES custom_object(id) ON DELETE CASCADE; 

    ALTER TABLE natuurlijk_persoon DROP CONSTRAINT "natuurlijk_persoon_related_custom_object_id_fkey";
    ALTER TABLE ONLY natuurlijk_persoon
    ADD CONSTRAINT natuurlijk_persoon_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES custom_object(id) ON DELETE SET NULL;

    ALTER TABLE subject DROP CONSTRAINT "subject_related_custom_object_id_fkey";
    ALTER TABLE ONLY subject
    ADD CONSTRAINT subject_related_custom_object_id_fkey FOREIGN KEY (related_custom_object_id) REFERENCES custom_object(id) ON DELETE SET NULL;

COMMIT;