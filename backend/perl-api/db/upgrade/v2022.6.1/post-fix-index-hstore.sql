
BEGIN;

  UPDATE zaak_meta SET index_hstore = index_hstore || hstore('case.status', z.status)
  FROM zaak z
  WHERE z.id = zaak_meta.zaak_id
  AND zaak_meta.index_hstore->'case.status' != z.status
  AND z.deleted is null;

COMMIT;
