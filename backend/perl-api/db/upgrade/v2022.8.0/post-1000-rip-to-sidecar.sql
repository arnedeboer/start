BEGIN;
  UPDATE interface set interface_config = interface_config::jsonb || '{"export_type" : "sidecar"}' where module = 'export_mapping_topx';
COMMIT;
