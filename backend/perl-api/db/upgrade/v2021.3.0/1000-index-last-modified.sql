DROP INDEX IF EXISTS zaak_last_modified;
CREATE INDEX CONCURRENTLY zaak_last_modified ON public.zaak USING btree (last_modified);
