BEGIN;

    ALTER TABLE groups ALTER COLUMN uuid SET NOT NULL;
    ALTER TABLE roles  ALTER COLUMN uuid SET NOT NULL;

    DROP INDEX IF EXISTS groups_uuid_idx;
    DROP INDEX IF EXISTS roles_uuid_idx;
    CREATE UNIQUE INDEX groups_uuid_idx ON groups USING btree (uuid);
    CREATE UNIQUE INDEX roles_uuid_idx ON roles USING btree (uuid);

COMMIT;
