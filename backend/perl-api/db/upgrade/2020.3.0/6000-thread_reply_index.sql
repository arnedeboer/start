BEGIN;
    -- The 48 most significant bits are used as the "reply marker", for emails
    -- to end up in existing threads.
    CREATE INDEX thread_uuid_reply_idx ON thread(
        CAST(
            CAST(
                'x' || translate(right(uuid::text, 12), '-', '')
                AS bit(48)
            )
            AS bigint
        )
    );
COMMIT;
