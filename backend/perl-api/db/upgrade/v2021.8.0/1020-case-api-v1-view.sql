
BEGIN;

  WITH cte AS (
    SELECT DISTINCT ON (l.zaak_id) zaak_id, l.id, l.event_data::jsonb->>'reason' reason
    FROM logging l
    JOIN zaak z
    ON z.id = l.zaak_id
    WHERE l.event_type = 'case/suspend'
    AND z.status = 'stalled'
    ORDER BY l.zaak_id, l.id desc
  )
    UPDATE zaak_meta zm set opschorten = cte.reason
    FROM cte WHERE cte.zaak_id = zm.zaak_id;

  WITH cte AS (
    SELECT DISTINCT ON (l.zaak_id) zaak_id, l.id, l.event_data::jsonb->>'reason' reason
    FROM logging l
    JOIN zaak z
    ON z.id = l.zaak_id
    WHERE l.event_type = 'case/early_settle'
    AND z.status = 'resolved'
    ORDER BY l.zaak_id, l.id desc
  )
    UPDATE zaak_meta zm set afhandeling = cte.reason, opschorten = null
    FROM cte WHERE cte.zaak_id = zm.zaak_id;

  DROP VIEW IF EXISTS case_v1;

  CREATE VIEW case_v1 AS
  SELECT
    z.id AS number,
    z.uuid AS id,
    z.pid AS number_parent,
    z.number_master AS number_master,
    z.vervolg_van AS number_previous,

    z.onderwerp AS subject,
    z.onderwerp_extern AS subject_external,

    z.status AS status,

    z.created AS date_created,
    z.last_modified AS date_modified,
    z.vernietigingsdatum AS date_destruction,
    z.afhandeldatum AS date_of_completion,
    z.registratiedatum AS date_of_registration,
    z.streefafhandeldatum AS date_target,

    z.payment_status AS payment_status,
    z.payment_amount AS price,

    z.contactkanaal AS channel_of_contact,
    z.archival_state AS archival_state,

    CASE WHEN z.status = 'stalled' THEN
      zm.stalled_since
    ELSE
      NULL
    END AS stalled_since,

    CASE WHEN z.status = 'stalled' THEN
      z.stalled_until
    ELSE
      NULL
    END AS stalled_until,

    zm.current_deadline AS current_deadline,
    zm.deadline_timeline AS deadline_timeline,

    jsonb_object_agg(ca.magic_string, ca.value) AS attributes,

    ztr.id AS result_id,
    ztr.resultaat as result,

    CASE WHEN (ztr.id IS NOT NULL) THEN
      json_build_object(
        'reference', NULL,
        'type', 'case/result',
        'preview', CASE WHEN ztr.label IS NOT NULL THEN
          ztr.label
        ELSE
          ztr.resultaat
        END,
        'instance', json_build_object(
          'date_created', NOW(),
          'date_modified', NOW(),
          'archival_type', ztr.archiefnominatie,
          'dossier_type', ztr.dossiertype,
          'name', CASE WHEN ztr.label IS NOT NULL THEN
            ztr.label
          ELSE
            ztr.resultaat
          END,
          'result', ztr.resultaat,
          'retention_period', ztr.bewaartermijn,
          'selection_list', CASE WHEN ztr.selectielijst = '' THEN
                              NULL
                            ELSE
                              ztr.selectielijst
                            END,
          'selection_list_start', ztr.selectielijst_brondatum,
          'selection_list_end', ztr.selectielijst_einddatum
        )
      )
    ELSE
      NULL
    END AS outcome,

    null AS active_selection_list,

    json_build_object(
      'preview', ct_ref.title,
      'reference', ct_ref.uuid,
      'instance', json_build_object(
        'version', ct_ref.version,
        'name', ct_ref.title
      ),
      'type', 'casetype'
    ) AS casetype,

    -- routing info
    null AS group,
    null AS role,

    CASE WHEN z.status = 'stalled' THEN
      zm.opschorten
    ELSE
      NULL
    END AS suspension_rationale,

    CASE WHEN z.status = 'resolved' THEN
      zm.afhandeling
    ELSE
      NULL
    END AS premature_completion_rationale,

    -- Subject API/v1 shit, mind boggling
    null AS requestor,
    null AS assignee,
    null AS coordinator,

    -- phase can volgende fase, show the next phase number
    null AS phase,

    -- milestones, similar to phase
    -- json blob
    null AS milestone,

    -- complex queries here
    null AS relations,
    null AS case_relationships,

    -- static values
    'Dossier' AS aggregation_scope,

    -- Not available via api/v1
    null AS case_location,
    null AS correspondence_location

  FROM zaak z

  JOIN zaak_meta zm
  ON zm.zaak_id = z.id

  JOIN case_attributes ca
  ON  ca.case_id = z.id

  JOIN casetype_v1_reference ct_ref
  ON z.zaaktype_node_id = ct_ref.casetype_node_id

  LEFT JOIN zaaktype_resultaten ztr
  ON z.resultaat_id = ztr.id

  GROUP BY
    z.id,
    zm.stalled_since,
    zm.current_deadline,
    zm.deadline_timeline,
    zm.opschorten,
    zm.afhandeling,
    ct_ref.title,
    ct_ref.uuid,
    ct_ref.version,
    ztr.id
  ;

COMMIT;
