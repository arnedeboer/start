package Zaaksysteem::Test::SAML2::Protocol::Assertion;
use Moose;

extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use IO::All;
use Net::SAML2::Protocol::Assertion;

sub test_MINTY_4162 {

    my $assertion = Net::SAML2::Protocol::Assertion->new_from_xml(
        xml => scalar io->catfile(qw(t data eherkenning assertion.xml))->slurp
    );
    isa_ok $assertion, 'Net::SAML2::Protocol::Assertion';

    isa_ok($assertion->not_after,  "DateTime", "not_after checks out");
    isa_ok($assertion->not_before, "DateTime", ".. as is not_before");

}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
