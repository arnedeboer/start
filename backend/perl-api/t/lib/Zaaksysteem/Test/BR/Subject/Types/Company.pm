package Zaaksysteem::Test::BR::Subject::Types::Company;

use strict;
use warnings;
use utf8;

=head1 NAME

Zaaksysteem::Test::BR::Subject::Types::Company

=head1 DESCRIPTION

Test BR for Subject::Types::Company

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::BR::Subject::Types::Company

=cut

use Zaaksysteem::Test;

=head2 test__get_address_object

=cut

sub test__get_address_object {

    my %params = (
        street        => 'korte wold straat',
        street_number => '42',
        city          => 'Steenwijk',
        country       => { dutch_code => '6030', },
    );

    my %expect = (
        street        => 'korte wold straat',
        street_number => '42',
        city          => 'Steenwijk',
    );

    subtest 'basic' => sub {
        my $object
            = Zaaksysteem::BR::Subject::Types::Company->_get_address_object(
            \%params);
        isa_ok($object, "Zaaksysteem::Object::Types::Address");

        my %got;
        foreach (qw(street street_number city)) {
            $got{$_} = $object->$_;
        }

        cmp_deeply(\%got, \%expect, "Got all the right fields");
        is($object->country->dutch_code, '6030', "Country code as well");
    };

    subtest 'basic + instance country code' => sub {
        my $object
            = Zaaksysteem::BR::Subject::Types::Company->_get_address_object(
            { %params, country => { instance => { dutch_code => '6030' } } });

        isa_ok($object, "Zaaksysteem::Object::Types::Address");
        my %got;
        foreach (qw(street street_number city)) {
            $got{$_} = $object->$_;
        }

        cmp_deeply(\%got, \%expect, "Got all the right fields");
        is($object->country->dutch_code, '6030', "Country code as well");
    };

    %params = (
        correspondentie_straatnaam => 'korte wold straat',
        correspondentie_huisnummer => '42',
        correspondentie_woonplaats => 'Steenwijk',
        correspondentie_landcode   => '6030',
        vestiging_huisletter       => 'X',    # should not have this
    );

    subtest 'prefix' => sub {
        my $object
            = Zaaksysteem::BR::Subject::Types::Company->_get_address_object(
            \%params, 'correspondentie');
        isa_ok($object, "Zaaksysteem::Object::Types::Address");

        my %got;
        foreach (qw(street street_number city)) {
            $got{$_} = $object->$_;
        }

        cmp_deeply(\%got, \%expect, "Got all the right fields");
        is($object->country->dutch_code, '6030', "Country code as well");
    };

}

sub test__get_address_values {

    my $TEST_OBJECT;

    $TEST_OBJECT = _mock_obj__Zaaksysteem_BR_Subject_Types_Company();

    cmp_deeply(
        { $TEST_OBJECT->_get_address_values( type => 'address_correspondence' ) },
        {
            correspondentie_adres_buitenland1    => undef,
            correspondentie_adres_buitenland2    => undef,
            correspondentie_adres_buitenland3    => undef,
            correspondentie_straatnaam           => "korte wold straat",
            correspondentie_huisnummer           => 42,
            correspondentie_huisletter           => undef,
            correspondentie_huisnummertoevoeging => undef,
            correspondentie_postcode             => "1234ZZ",
            correspondentie_woonplaats           => "Steenwijk",
            correspondentie_landcode             => 6030,
        },
        "... prooduces the right database keys for 'correspondentie'"
    );

    cmp_deeply(
        { $TEST_OBJECT->_get_address_values( type => 'address_residence' ) },
        {
            vestiging_adres_buitenland1    => '__1__',
            vestiging_adres_buitenland2    => '__2__',
            vestiging_adres_buitenland3    => '__3__',
            vestiging_straatnaam           => undef,
            vestiging_huisnummer           => undef,
            vestiging_huisletter           => undef,
            vestiging_huisnummertoevoeging => undef,
            vestiging_postcode             => undef,
            vestiging_woonplaats           => undef,
            vestiging_landcode             => 6059,

            vestiging_bag_id => undef,
            vestiging_latlong => undef,
        },
        "... prooduces the right database keys for 'vestiging'"
    );

}

sub test_new_from_row {

    my $TEST_CLASS = 'Zaaksysteem::BR::Subject::Types::Company';

    my $mock = {};

    $mock->{mod}{'Zaaksysteem::Object::Types::LegalEntityType'}
        = _mock_mod__Zaaksysteem_Object_Types_LegalEntityType();

    $mock->{new}{'Zaaksysteem::BR::Subject::Types::Company'}
        = _mock_new__Zaaksysteem_BR_Subject_Types_Company();

    # set expected hash for cmp_deeply and pass in a mock object
    $mock->{mod}{'Zaaksysteem::BR::Subject::Types::Company'}
        = _mock_mod__Zaaksysteem_BR_Subject_Types_Company(
            {
                company                 => 'Mintlab BV',
#               company_type            => methods (
#                   code                    => '1',
#                   label                   => 'Eenmanszaak',
#               ),
                mobile_phone_number     => '+31634567890',
                email_address           => 'test@mintlab.nl',
                phone_number            => '020-1234567',
                address_residence       => methods (
                    street                  => 'vestiging',
                ),
                address_correspondence  => methods (
                    street                  => 'correspondentie',
                ),
            },
            $mock->{new}{'Zaaksysteem::BR::Subject::Types::Company'},
        );

    my $row_data = _bedrijf_row();
    $row_data->{data}{correspondentie_landcode} = '1234';
    my $row = _mock_obj__ResultObject__row($row_data);
    
    $TEST_CLASS->new_from_row($row);
    
}

sub test_new_from_params {

    my $object = Zaaksysteem::Object::Types::Company->new_from_params(
        {
            company           => "Mintlab BV",
            address_residence => {
                street  => 'Some street',
                country => { dutch_code => '6030' }
            },
            address_correspondence => { street => 'Some other street', }

        },
    );
    isa_ok($object, "Zaaksysteem::Object::Types::Company");
    is($object->address_correspondence,
        undef, "No address when no country is defined");

    my $address = $object->address_residence;
    is($address->street, 'Some street', "Residence street found");
    is($address->country->dutch_code, '6030', "... with correct country");

    $object = Zaaksysteem::Object::Types::Company->new_from_params(
        {
            company           => "Mintlab BV",
            address_residence => {
                street  => 'Some street',
                country => { instance => { dutch_code => '6030'} }
            },
        },
    );
    isa_ok($object, "Zaaksysteem::Object::Types::Company");
    $address = $object->address_residence;
    is($address->street, 'Some street', "Residence street found");
    is($address->country->dutch_code, '6030', "... with correct country");
}


sub test_save_to_tables {
    pass 'nearly untestable, perhaps a sub _get_address_values';
}

# done testing
#
# mocks follow

sub _mock_mod__Zaaksysteem_BR_Subject_Types_Company {
    my $expected = shift;
    my $mock_new = shift;

    my $mock_mod = Test::MockModule->new('Zaaksysteem::BR::Subject::Types::Company');
    $mock_mod->mock(
        'new' => sub {
            my $class = shift;
            my %params = @_;
            cmp_deeply(
                \%params => $expected,
                "... got the right arguments to create Company"
            );
            return $mock_new;
        }
    );

    $mock_mod->mock(
        '_load_contact_data_from_row'
            => \&_mock_sub__load_contact_data_from_row
    );

#     $mock_mod->mock(
#         '_load_addresses_from_row'
#             => \&_mock_sub__load_addresses_from_row
#     );

    $mock_mod->mock(
        '_get_address_object'
            => \&_mock_sub__get_address_object
    );

    return $mock_mod;
}

sub _mock_new__Zaaksysteem_BR_Subject_Types_Company {
    my $mock_obj = Test::MockObject->new();

    $mock_obj->mock(
        'id' => sub {
            is $_[1], '5cb87638-31bf-4e90-947f-c5bb6cdd5c88',
            '... got the right UUID'
        }
    );

    $mock_obj->mock(
        '_table_id' => sub {
            is $_[1], '42',
            '... got the right row id'
        }
    );
    
    return $mock_obj
}

sub _mock_obj__Zaaksysteem_BR_Subject_Types_Company {
    my $mock_obj = Test::MockObject->new;
    $mock_obj->set_isa('Zaaksysteem::BR::Subject::Types::Company');

    $mock_obj->mock(
        'address_correspondence' => sub {
            return _mock_obj__Zaaksysteem_Object_Types_Address__NL()
        }
    );

    $mock_obj->mock(
        'address_residence' => sub {
            return _mock_obj__Zaaksysteem_Object_Types_Address__foreign()
        }
    );

    $mock_obj->mock(
        '_get_address_values' =>
            \&Zaaksysteem::BR::Subject::Types::Company::_get_address_values
    );

    return $mock_obj
}

sub _mock_mod__Zaaksysteem_Object_Types_Address {
    my $expected = shift;

    my $mock_mod = Test::MockModule->new('Zaaksysteem::Object::Types::Address');
    $mock_mod->mock(
        'new' => sub {
            my $class = shift;
            my %params = @_;
            cmp_deeply(
                \%params => $expected,
                "... got the right arguments to create Address"
            );

            return 1
        }
    );

    return $mock_mod;
}

sub _mock_obj__Zaaksysteem_Object_Types_Address {
    my $params = shift;
    # we need all methods, to avoid warnings
    my %attributes = (
        street                  => undef,
        street_number           => undef,
        street_number_letter    => undef,
        street_number_suffix    => undef,
        zipcode                 => undef,
        city                    => undef,
        country                 => undef, # Zaaksysteem::Object::Types::CountryCode
        foreign_address_line1   => undef,
        foreign_address_line2   => undef,
        foreign_address_line3   => undef,
        municipality            => undef, # Zaaksysteem::Object::Types::MunicipalityCode

        latitude => undef,
        longitude => undef,
        bag_id => undef,
        %$params
    );
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('Zaaksysteem::Object::Types::Address');
    # add accessors
    foreach my $attr (keys %attributes ) {
        $mock_obj->mock(
            $attr => sub { $attributes{$attr} }
        )
    }

    return $mock_obj
}

sub _mock_obj__Zaaksysteem_Object_Types_Address__NL {
    my $mock_obj = _mock_obj__Zaaksysteem_Object_Types_Address (
        {
            street                  => 'korte wold straat',
            street_number           => '42',
            zipcode                 => '1234ZZ', # no space!!!
            city                    => 'Steenwijk',
            country
                => _mock_obj__Zaaksysteem_Object_Types_CountryCode__NL(),
        }
    );

    return $mock_obj
}

sub _mock_obj__Zaaksysteem_Object_Types_Address__foreign {
    my $mock_obj = _mock_obj__Zaaksysteem_Object_Types_Address (
        {
            foreign_address_line1   => '__1__',
            foreign_address_line2   => '__2__',
            foreign_address_line3   => '__3__',
            country
                => _mock_obj__Zaaksysteem_Object_Types_CountryCode__foreign(),
        }
    );

    return $mock_obj
}

sub _mock_mod__Zaaksysteem_Object_Types_CountryCode {
    my $mock_mod = Test::MockModule
        ->new('Zaaksysteem::Object::Types::CountryCode');

    my $country_codes = {
        '1234' => _mock_obj__Zaaksysteem_Object_Types_CountryCode(),
        '6030' => _mock_obj__Zaaksysteem_Object_Types_CountryCode__NL(),
        '6059' => _mock_obj__Zaaksysteem_Object_Types_CountryCode__foreign(),
    };

    $mock_mod->mock(
        'new_from_dutch_code' => sub {
            my $class = shift;
            my $code = shift;
            return $country_codes->{$code}
        }
    );

    return $mock_mod;
}

sub _mock_obj__Zaaksysteem_Object_Types_CountryCode {
    my $params = shift || { dutch_code => '1234', label => 'Timbuktu'};
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('Zaaksysteem::Object::Types::CountryCode');
    $mock_obj->set_always( dutch_code => $params->{dutch_code} );
    $mock_obj->set_always( label      => $params->{label}      );
    return $mock_obj;
}

sub _mock_obj__Zaaksysteem_Object_Types_CountryCode__NL {
    my $mock_obj = _mock_obj__Zaaksysteem_Object_Types_CountryCode(
        {
            dutch_code => '6030',
            label      => 'Nederland',
        }
    );
    return $mock_obj;
}

sub _mock_obj__Zaaksysteem_Object_Types_CountryCode__foreign {
    my $mock_obj = _mock_obj__Zaaksysteem_Object_Types_CountryCode(
        {
            dutch_code => '6059',
            label      => 'São Tomé en Princip',
        }
    );
    return $mock_obj;
}

sub _mock_mod__Zaaksysteem_Object_Types_LegalEntityType {
    my $mock_mod = Test::MockModule->new('Zaaksysteem::Object::Types::LegalEntityType');
    $mock_mod->mock(
        'new_from_code' => sub {
            my $class = shift;
            my $code = shift;
            my $mock_obj = Test::MockObject->new();
            $mock_obj->set_isa('Zaaksysteem::Object::Types::LegalEntityType');
            $mock_obj->set_always(code => $code);
            $mock_obj->set_always(label => 'Eenmanszaak');
            return $mock_obj;
        }
    );

    return $mock_mod;
}

sub _bedrijf_row {
    return
        {
            meta => {
                name => 'bedrijf',
            },
            data => {
                uuid                                 => '5cb87638-31bf-4e90-947f-c5bb6cdd5c88',
                id                                   => '42',
                rechtsvorm                           => undef,
                # to avoid calling:
                # Zaaksysteem::Object::Types::LegalEntityType->new_from_code
                handelsnaam                          => 'Mintlab BV',
                correspondentie_adres                => '',
                correspondentie_straatnaam           => '',
                correspondentie_huisnummer           => '',
                correspondentie_huisletter           => '',
                correspondentie_huisnummertoevoeging => '',
                correspondentie_postcodewoonplaats   => '',
                correspondentie_postcode             => '',
                correspondentie_woonplaats           => '',
                correspondentie_adres_buitenland1    => '',
                correspondentie_adres_buitenland2    => '',
                correspondentie_adres_buitenland3    => '',
                correspondentie_landcode             => '',

                main_activity => {},
                secondairy_activities => [],
                # this MUST be set or ZS::Obj::Types::CountryCode->new_from_dutch_code
                # would not be called
                # and thus _get_address_object() would not return an object
            }
        }

}

sub _mock_obj__ResultObject__row {
    my $row = shift;
    my $mock_obj = Test::MockObject->new();

    
    $mock_obj->mock(
        'result_source' => sub {
            my $mock_sub = Test::MockObject->new();
            $mock_sub->set_always (
                'name' => $row->{meta}->{name}
            );
            return $mock_sub;
        }
    );

    $mock_obj->mock(
        'get_column' => sub {
            return $row->{data}->{$_[1]}
        }
    );

    $mock_obj->mock(
        'get_columns' => sub { return %{ $row->{data} } }
    );

    # add accessors
    foreach my $attr ( keys %{$row->{data}} ) {
        $mock_obj->set_always($attr => $row->{data}->{$attr})
    }

    return $mock_obj;
}

sub _bedrijf_params {
    return {
        company                 => 'Mintlab BV',
        company_type            => {
            code                    => '1',
            ignored_data            => 'foo',
        },
        address_correspondence  => {
            street                  => 'korte wold straat',
            street_number           => '42',
            city                    => 'Steenwijk',
            country                 => {
                dutch_code              => '6030',
            },
        },
        address_residence       => {
            street                  => undef,
            foreign_address_line1   => undef,
        }, # so, we won't get an address_residence
        empty_space             => '',
        zero                    => 0,
        # we skip undefined, not false
    }
}

sub _mock_sub__load_contact_data_from_row {
    my $class = shift;
    my $row = shift;

    my $contact = {
        mobile_phone_number => '+31634567890',
        phone_number        => '020-1234567',
        email_address       => 'test@mintlab.nl',
    };

    return %$contact;

}

sub _mock_sub__get_address_object {
    my $class = shift;
    my $params = shift; # ignore
    my $prefix = shift;

    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('Zaaksysteem::Object::Types::Address');
    $mock_obj->mock(
        'street' => sub { $prefix || 'from params' }
    );
    return $mock_obj;
}
=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;
