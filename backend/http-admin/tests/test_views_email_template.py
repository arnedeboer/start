# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from .test_zsnl_admin_http import MockRequest
from pyramid.httpexceptions import HTTPBadRequest, HTTPForbidden
from unittest import mock
from uuid import uuid4
from zsnl_admin_http.views import email_template
from zsnl_domains.admin.catalog.entities.email_template import EmailTemplate


class TestEmailTemplateViews:
    def test_email_template_no_admin(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
        )
        views = [
            email_template.create_an_email_template,
            email_template.edit_an_email_template,
            email_template.get_email_template_detail,
            email_template.delete_email_template,
        ]
        for view in views:
            with pytest.raises(HTTPForbidden):
                view(request)

    def test_get_email_template_detail_incorrect_params(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
        )

        with pytest.raises(HTTPBadRequest):
            email_template.get_email_template_detail(request)

    def test_get_email_template_detail(self):
        uuid = str(uuid4())
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={"email_template_id": uuid},
            query_instances={
                "zsnl_domains.admin.catalog": mock_query_instance
            },
            command_instances={"none": None},
            current_route=f"/api/v2/admin/catalog/get_email_template_detail?email_template_id={uuid}",
        )
        mock_email_template = EmailTemplate(
            uuid=uuid,
            id=2,
            label="label",
            sender="sender",
            sender_address="sender_address",
            subject="subject",
            message="message",
            category_uuid=None,
            attachments=[],
        )
        mock_query_instance.get_email_template_details_for_edit.return_value = (
            mock_email_template
        )

        res = email_template.get_email_template_detail(request)
        assert res == {
            "data": {
                "type": "email_template",
                "id": uuid,
                "attributes": {
                    "label": mock_email_template.label,
                    "sender": mock_email_template.sender,
                    "sender_address": mock_email_template.sender_address,
                    "subject": mock_email_template.subject,
                    "message": mock_email_template.message,
                    "category_uuid": mock_email_template.category_uuid,
                    "attachments": [],
                },
            },
            "links": {
                "self": f"/api/v2/admin/catalog/get_email_template_detail?email_template_id={uuid}"
            },
        }

    def test_edit_email_template(self):
        uuid = str(uuid4())
        mock_command = mock.MagicMock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={},
            command_instances={"zsnl_domains.admin.catalog": mock_command},
            current_route=f"/api/v2/admin/catalog/get_email_template_detail?email_template_id={uuid}",
        )
        # validation of fields will happen in the domain
        request.body = {
            "email_template_uuid": str(uuid4()),
            "fields": {"label": "label"},
        }
        res = email_template.edit_an_email_template(request)
        assert res == {"data": {"success": True}}

        with pytest.raises(HTTPBadRequest):
            request.body = {}
            res = email_template.edit_an_email_template(request)

    def test_create_email_template(self):
        uuid = str(uuid4())
        mock_command = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={},
            command_instances={"zsnl_domains.admin.catalog": mock_command},
            current_route=f"/api/v2/admin/catalog/get_email_template_detail?email_template_id={uuid}",
        )
        # validation of fields will happen in the domain
        request.body = {
            "email_template_uuid": str(uuid4()),
            "fields": {"label": "label"},
        }
        res = email_template.create_an_email_template(request)
        assert res == {"data": {"success": True}}
        with pytest.raises(HTTPBadRequest):
            request.body = {}
            res = email_template.create_an_email_template(request)

    def test_delete_email_template(self):
        mock_command = mock.MagicMock()

        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={},
            command_instances={"zsnl_domains.admin.catalog": mock_command},
        )
        # validation of fields will happen in the domain
        request.body = {
            "email_template_uuid": str(uuid4()),
            "reason": "reason for delete",
        }
        res = email_template.delete_email_template(request)
        assert res == {"data": {"success": True}}

        with pytest.raises(HTTPBadRequest):
            request.body = {}
            res = email_template.delete_email_template(request)
