# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import date, datetime
from minty.cqrs import UserInfo
from uuid import uuid4
from zsnl_admin_http.views import serializers
from zsnl_domains.admin.catalog import entities
from zsnl_domains.admin.integrations.entities import Integration


def generate_user_info():
    return UserInfo(
        str(uuid4()), {"beheer_zaaktype_admin": True, "gebruiker": True}
    )


class TestSerializers:
    def test_related_case_types(self):
        rct = entities.RelatedCaseType(
            uuid=uuid4(),
            name="Case Type",
            active=True,
            version=6,
            is_current_version=True,
        )

        assert serializers.related_case_type(rct) == {
            "type": "case_type",
            "id": str(rct.uuid),
            "attributes": {
                "name": rct.name,
                "active": rct.active,
                "version": rct.version,
                "is_current_version": rct.is_current_version,
            },
        }

    def test_case_type_details(self):
        rct = entities.RelatedCaseType(
            uuid=uuid4(),
            name="Case Type",
            active=True,
            version=6,
            is_current_version=True,
        )
        ct = entities.CaseTypeEntity(
            id=123,
            name="Another Case Type",
            uuid=uuid4(),
            last_modified=datetime.now(),
            active=True,
            identification="Piet",
            current_version=48,
            current_version_uuid=uuid4(),
            initiator_source=["internal", "external"],
        )

        folder_uuid = uuid4()
        folder = entities.Folder(
            id=None,
            name="folder_name",
            uuid=folder_uuid,
            parent_name=None,
            parent_uuid=None,
            last_modified=datetime.now(),
        )
        ct.used_in_case_types = [rct]
        ct.folder = folder
        case_type_uuid = ct.uuid

        user_info = generate_user_info()
        assert serializers.case_type_details(ct, user_info) == {
            "type": "case_type",
            "id": str(case_type_uuid),
            "attributes": {
                "name": "Another Case Type",
                "last_modified": ct.last_modified.isoformat(),
                "active": True,
                "identification": "Piet",
                "current_version": 48,
                "context": ["internal", "external"],
            },
            "relationships": {
                "used_in_case_types": [
                    {
                        "type": "case_type",
                        "id": str(rct.uuid),
                        "attributes": {
                            "name": "Case Type",
                            "active": True,
                            "version": 6,
                            "is_current_version": True,
                        },
                    }
                ],
                "folder": {
                    "attributes": {
                        "last_modified": ct.folder.last_modified.isoformat(),
                        "name": "folder_name",
                        "parent_id": None,
                        "parent_name": None,
                    },
                    "id": str(folder_uuid),
                    "links": {
                        "self": f"/api/v2/admin/catalog/get_folder_contents?folder_id={folder_uuid!s}"
                    },
                    "type": "folder",
                },
            },
            "links": {
                "export": f"/beheer/zaaktypen/{case_type_uuid}/export",
                "version_history": f"/api/v2/admin/catalog/get_case_type_history?case_type_id={case_type_uuid}",
            },
        }

    def test_case_type_version_history_details(self):
        case_type_uuid = uuid4()
        ct = entities.CaseTypeVersion(
            uuid="1",
            case_type_uuid=str(case_type_uuid),
            reason="Test case type version",
            name="case_type",
            username="case_type",
            display_name="case_type",
            active=True,
            version=1,
            last_modified="",
            created="",
            change_note="test",
            modified_components={
                "basic_properties": True,
                "case_actions": False,
                "phases_and_assignment": True,
                "custom_fields": False,
            },
        )
        assert serializers.case_type_version_history_details(ct) == {
            "type": "case_type_version",
            "case_type_id": str(case_type_uuid),
            "attributes": {
                "id": "1",
                "name": "case_type",
                "username": "case_type",
                "display_name": "case_type",
                "created": None,
                "last_modified": None,
                "active": True,
                "reason": "Test case type version",
                "version": 1,
                "change_note": "test",
                "modified_components": {
                    "basic_properties": True,
                    "case_actions": False,
                    "phases_and_assignment": True,
                    "custom_fields": False,
                },
            },
        }

    def test_attribute_details(self):
        rct = entities.RelatedCaseType(
            uuid=uuid4(),
            name="Case Type",
            active=True,
            version=6,
            is_current_version=True,
        )
        folder_uuid = uuid4()
        folder = entities.Folder(
            id=None,
            name="folder_name",
            uuid=folder_uuid,
            parent_name=None,
            parent_uuid=None,
            last_modified=datetime.now(),
        )

        attribute = entities.AttributeDetail(
            name="Attribute",
            uuid=uuid4(),
            last_modified=datetime.now(),
            is_multiple=True,
            magic_string="piet",
            value_type="richtext",
            used_in_case_types=[rct],
            used_in_object_types=[],
            folder=folder,
        )

        user_info = generate_user_info()

        assert serializers.attribute_details(attribute, user_info) == {
            "type": "attribute",
            "id": str(attribute.uuid),
            "attributes": {
                "name": "Attribute",
                "last_modified": attribute.last_modified.isoformat(),
                "is_multiple": True,
                "magic_string": "piet",
                "value_type": "richtext",
            },
            "relationships": {
                "used_in_case_types": [
                    {
                        "type": "case_type",
                        "id": str(rct.uuid),
                        "attributes": {
                            "name": "Case Type",
                            "active": True,
                            "version": 6,
                            "is_current_version": True,
                        },
                    }
                ],
                "folder": {
                    "attributes": {
                        "last_modified": attribute.folder.last_modified.isoformat(),
                        "name": "folder_name",
                        "parent_id": None,
                        "parent_name": None,
                    },
                    "id": str(folder_uuid),
                    "links": {
                        "self": f"/api/v2/admin/catalog/get_folder_contents?folder_id={folder_uuid!s}"
                    },
                    "type": "folder",
                },
            },
        }

    def test_email_template_details(self):
        rct = entities.RelatedCaseType(
            uuid=uuid4(),
            name="Case Type",
            active=True,
            version=6,
            is_current_version=True,
        )
        folder_uuid = uuid4()
        folder = entities.Folder(
            id=None,
            name="folder_name",
            uuid=folder_uuid,
            parent_name=None,
            parent_uuid=None,
            last_modified=datetime.now(),
        )
        email_template = entities.EmailTemplateDetail(
            name="Email Template",
            uuid=uuid4(),
            last_modified=datetime.now(),
            used_in_case_types=[rct],
            folder=folder,
        )

        user_info = generate_user_info()

        assert serializers.email_template_details(
            email_template, user_info
        ) == {
            "type": "email_template",
            "id": str(email_template.uuid),
            "attributes": {
                "name": "Email Template",
                "last_modified": email_template.last_modified.isoformat(),
            },
            "relationships": {
                "used_in_case_types": [
                    {
                        "type": "case_type",
                        "id": str(rct.uuid),
                        "attributes": {
                            "name": "Case Type",
                            "active": True,
                            "version": 6,
                            "is_current_version": True,
                        },
                    }
                ],
                "folder": {
                    "attributes": {
                        "last_modified": email_template.folder.last_modified.isoformat(),
                        "name": "folder_name",
                        "parent_id": None,
                        "parent_name": None,
                    },
                    "id": str(folder_uuid),
                    "links": {
                        "self": f"/api/v2/admin/catalog/get_folder_contents?folder_id={folder_uuid!s}"
                    },
                    "type": "folder",
                },
            },
        }

    def test_document_template_details(self):
        rct = entities.RelatedCaseType(
            uuid=uuid4(),
            name="Case Type",
            active=True,
            version=6,
            is_current_version=True,
        )
        folder_uuid = uuid4()
        folder = entities.Folder(
            id=None,
            name="folder_name",
            uuid=folder_uuid,
            parent_name=None,
            parent_uuid=None,
            last_modified=datetime.now(),
        )

        doc_template = entities.DocumentTemplateDetail(
            name="Document Template",
            filename="a_file.xls",
            uuid=uuid4(),
            has_default_integration=True,
            last_modified=datetime.now(),
            used_in_case_types=[rct],
            folder=folder,
        )

        user_info = generate_user_info()

        assert serializers.document_template_details(
            doc_template, user_info
        ) == {
            "type": "document_template",
            "id": str(doc_template.uuid),
            "attributes": {
                "name": "Document Template",
                "filename": "a_file.xls",
                "has_default_integration": True,
                "last_modified": doc_template.last_modified.isoformat(),
            },
            "relationships": {
                "used_in_case_types": [
                    {
                        "type": "case_type",
                        "id": str(rct.uuid),
                        "attributes": {
                            "name": "Case Type",
                            "active": True,
                            "version": 6,
                            "is_current_version": True,
                        },
                    }
                ],
                "folder": {
                    "attributes": {
                        "last_modified": doc_template.folder.last_modified.isoformat(),
                        "name": "folder_name",
                        "parent_id": None,
                        "parent_name": None,
                    },
                    "id": str(folder_uuid),
                    "links": {
                        "self": f"/api/v2/admin/catalog/get_folder_contents?folder_id={folder_uuid!s}"
                    },
                    "type": "folder",
                },
            },
            "links": {
                "download": f"/beheer/bibliotheek/sjablonen/x/0/download/{doc_template.uuid}"
            },
        }

    def test_object_type_details(self):
        folder = entities.Folder(
            id=None,
            name=None,
            uuid=None,
            parent_name=None,
            parent_uuid=None,
            last_modified=None,
        )
        ot = entities.ObjectTypeDetail(
            name="Object Type",
            uuid=uuid4(),
            used_in_case_types=[],
            used_in_object_types=[],
            folder=folder,
        )

        user_info = generate_user_info()

        assert serializers.object_type_details(ot, user_info) == {
            "type": "object_type",
            "id": str(ot.uuid),
            "attributes": {"name": "Object Type"},
            "relationships": {
                "used_in_case_types": [],
                "folder": {
                    "attributes": {
                        "last_modified": None,
                        "name": None,
                        "parent_id": None,
                        "parent_name": None,
                    },
                    "id": None,
                    "links": {
                        "self": "/api/v2/admin/catalog/get_folder_contents"
                    },
                    "type": "folder",
                },
            },
        }

    def test_custom_object_type_details(self):
        folder = entities.Folder(
            id=None,
            name=None,
            uuid=None,
            parent_name=None,
            parent_uuid=None,
            last_modified=None,
        )
        now = datetime.now()
        ot = entities.CustomObjectTypeDetail(
            name="Object Type",
            uuid=uuid4(),
            title="Title",
            status="active",
            version=1,
            date_created=now,
            last_modified=now,
            version_independent_uuid=uuid4(),
            used_in_case_types=[],
            used_in_object_types=[],
            folder=folder,
        )

        user_info = generate_user_info()

        assert serializers.custom_object_type_details(ot, user_info) == {
            "attributes": {
                "date_created": now.isoformat(),
                "external_reference": None,
                "last_modified": now.isoformat(),
                "name": "Object Type",
                "status": "active",
                "title": "Title",
                "version": 1,
                "version_independent_uuid": str(ot.version_independent_uuid),
            },
            "id": str(ot.uuid),
            "relationships": {
                "folder": {
                    "attributes": {
                        "last_modified": None,
                        "name": None,
                        "parent_id": None,
                        "parent_name": None,
                    },
                    "id": None,
                    "links": {
                        "self": "/api/v2/admin/catalog/get_folder_contents"
                    },
                    "type": "folder",
                }
            },
            "type": "custom_object_type",
        }

    def test_integration(self):
        integration = Integration(
            id=1, uuid=uuid4(), name="name", active=True, module="appointment"
        )

        assert serializers.integration(integration) == {
            "type": "integration",
            "id": str(integration.uuid),
            "attributes": {
                "module": integration.module,
                "name": integration.name,
                "active": integration.active,
            },
        }

    def test_attribute_search_results(self):
        uuid = uuid4()
        atr = entities.attribute.AttributeDetailSearchResult(
            id=1,
            uuid=uuid,
            name="attribute",
            magic_string="attribute_1",
            value_type="file",
        )
        res = serializers.attribute_search_results([atr])
        assert res[0]["id"] == str(uuid)
        assert res[0]["attributes"]["name"] == atr.name
        assert res[0]["attributes"]["magic_string"] == atr.magic_string
        assert res[0]["attributes"]["value_type"] == atr.value_type

    def test_get_iso_format_date(self):
        result = serializers.get_iso_format_date(date(2019, 12, 4))

        assert result == "2019-12-04"

        result = serializers.get_iso_format_date(None)

        assert result is None
