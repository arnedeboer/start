# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from .test_zsnl_admin_http import MockRequest
from pyramid.httpexceptions import HTTPForbidden
from unittest import mock
from zsnl_admin_http.views.integrations import appointment, document


class TestIntegrationsViews:
    def test_get_active_document_integrations(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
        )
        mock_query_instance.get_active_integrations_for_document.return_value = (
            []
        )
        rv = document.get_active_document_integrations(request)

        assert rv == {"data": [], "links": {"self": "mock_current_route"}}

    def test_get_active_document_integrations_no_admin(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            command_instances={"none": None},
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
        )

        with pytest.raises(HTTPForbidden):
            document.get_active_document_integrations(request)

    def test_get_active_appointment_integrations_no_admin(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin":false} }
                    """
                }
            },
            params={},
            command_instances={"none": None},
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
        )

        with pytest.raises(HTTPForbidden):
            appointment.get_active_appointment_integrations(request)

    def test_get_active_appointment_integrations(self):
        mock_query_instance = mock.MagicMock()
        request = MockRequest(
            cookies={"zaaksysteem_session": "a"},
            sessions={
                "a": {
                    "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"beheer_zaaktype_admin": true} }
                    """
                }
            },
            params={},
            command_instances={"none": None},
            query_instances={
                "zsnl_domains.admin.integrations": mock_query_instance
            },
        )
        mock_query_instance.get_active_appointment_integrations.return_value = (
            []
        )

        rv = appointment.get_active_appointment_integrations(request)

        assert rv == {"data": [], "links": {"self": "mock_current_route"}}
