# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.3.9"

import logging
import minty
import minty_pyramid
import os
from .routes import routes
from minty.middleware import AmqpPublisherMiddleware
from minty_infra_sqlalchemy import (
    DatabaseTransactionMiddleware,
    DatabaseTransactionQueryMiddleware,
)
from zsnl_domains.admin import catalog, integrations, users
from zsnl_perl_migration import (
    LegacyEventBroadcastMiddleware,
    LegacyEventQueueMiddleware,
)

legacy_broadcast_casetype_events = [
    ("zsnl_domains.admin.catalog", "CaseTypeOnlineStatusChanged"),
    ("zsnl_domains.admin.catalog", "CaseTypeDeleted"),
    ("zsnl_domains.admin.catalog", "CaseTypeVersionUpdated"),
    ("zsnl_domains.admin.catalog", "AttributeEdited"),
    ("zsnl_domains.admin.catalog", "DocumentTemplateEdited"),
    ("zsnl_domains.admin.catalog", "EmailTemplateEdited"),
]

ZS_COMPONENT = "zsnl_admin_http"

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = ZS_COMPONENT  # type: ignore
    return record


def main(*args, **kwargs):
    logging.setLogRecordFactory(log_record_factory)

    minty.STATSD_PREFIX = ".".join(
        [ZS_COMPONENT, "silo", os.environ.get("ZS_SILO_ID", "unknown")]
    )

    loader = minty_pyramid.Engine(
        domains=[catalog, integrations, users],
        query_middleware=[DatabaseTransactionQueryMiddleware("database")],
        command_wrapper_middleware=[
            LegacyEventQueueMiddleware(
                database_infra_name="database",
                interesting_casetype_events=legacy_broadcast_casetype_events,
            ),
            DatabaseTransactionMiddleware("database"),
            LegacyEventBroadcastMiddleware(
                amqp_infra_name="amqp",
                interesting_casetype_events=legacy_broadcast_casetype_events,
            ),
            AmqpPublisherMiddleware(
                publisher_name="admin_catalog", infrastructure_name="amqp"
            ),
        ],
    )
    config = loader.setup(*args, **kwargs)

    routes.add_routes(config)
    return loader.main()
