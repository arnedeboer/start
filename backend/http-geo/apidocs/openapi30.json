{
  "openapi": "3.0.3",
  "servers": [
    {
      "url": "/"
    }
  ],
  "info": {
    "title": "Geo management API documentation",
    "description": "Retrieve GeoJSON data associated with various kinds of entities in Zaaksysteem",
    "version": "v2",
    "x-package-name": "zsnl_http_geo",
    "x-url-base": [
      "/api/v2/geo/"
    ],
    "contact": {
      "name": "Zaaksysteem Development"
    }
  },
  "tags": [
    {
      "name": "GeoFeature"
    }
  ],
  "paths": {
    "/api/v2/geo/get_geo_features": {
      "get": {
        "x-view": "geo_feature",
        "x-view-class": "GeoFeatureViews",
        "tags": [
          "GeoFeature"
        ],
        "operationId": "get_geo_features",
        "summary": "Retrieve geo features",
        "description": "Retrieve geo features",
        "parameters": [
          {
            "name": "uuid",
            "in": "query",
            "description": "List of UUID's to retrieve geo features for.",
            "required": true,
            "style": "form",
            "explode": false,
            "schema": {
              "type": "array",
              "uniqueItems": true,
              "items": {
                "type": "string",
                "format": "uuid"
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "GeoFeature",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/DefaultSuccessMultiple"
                    },
                    {
                      "$ref": "./generated_entities.json#/components/schemas/EntityGeoFeatureList"
                    }
                  ]
                }
              }
            }
          },
          "400": {
            "description": "Bad request",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseError"
                }
              }
            }
          }
        }
      }
    },
    "/api/v2/geo/create_geo_feature": {
      "post": {
        "x-view": "geo_feature",
        "x-view-class": "GeoFeatureViews",
        "tags": [
          "GeoFeature"
        ],
        "operationId": "create_geo_feature",
        "summary": "Creates a new geo feature",
        "description": "Creates a new geo feature",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "uuid": {
                    "type": "string",
                    "format": "uuid",
                    "description": "UUID of the geo feature to be created."
                  },
                  "geojson": {
                    "type": "object",
                    "description": "Geo Feature object."
                  }
                },
                "required": [
                  "uuid",
                  "geojson"
                ]
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseCreated"
                }
              }
            }
          },
          "400": {
            "description": "Bad request",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ResponseError"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "DefaultSuccess": {
        "type": "object",
        "required": [
          "meta"
        ],
        "properties": {
          "meta": {
            "type": "object",
            "required": [
              "api_version"
            ],
            "properties": {
              "api_version": {
                "type": "integer",
                "example": 2
              }
            }
          }
        }
      },
      "DefaultSuccessMultiple": {
        "allOf": [
          {
            "$ref": "#/components/schemas/DefaultSuccess"
          },
          {
            "type": "object",
            "properties": {
              "links": {
                "type": "object",
                "properties": {
                  "self": {
                    "type": "string",
                    "example": "http://example.zs.nl/?page=2"
                  },
                  "prev": {
                    "type": "string",
                    "example": "http://example.zs.nl/?page=1"
                  },
                  "next": {
                    "type": "string",
                    "example": "http://example.zs.nl/?page=3"
                  }
                }
              }
            }
          }
        ]
      },
      "ResponseCreated": {
        "properties": {
          "data": {
            "type": "object",
            "properties": {
              "created": {
                "type": "boolean"
              }
            }
          }
        }
      },
      "ResponseError": {
        "required": [
          "errors"
        ],
        "properties": {
          "errors": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "title": {
                  "type": "string"
                }
              }
            }
          }
        }
      }
    }
  }
}