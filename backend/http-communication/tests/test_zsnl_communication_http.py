# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from datetime import datetime
from minty.exceptions import ValidationError
from pyramid.httpexceptions import HTTPBadRequest
from unittest import mock
from uuid import uuid4
from zsnl_communication_http import log_record_factory
from zsnl_communication_http.views import communication
from zsnl_communication_http.views.communication import (
    get_contact_moment_list,
    search_case,
    search_contact,
    thread_list,
)
from zsnl_domains.communication.entities import (
    Case,
    Contact,
    ContactMomentOverview,
    Thread,
)


class MockRequest:
    def __init__(
        self,
        cookies,
        sessions,
        params,
        query_instances,
        command_instances=None,
        current_route=None,
        body=None,
    ):
        self.cookies = cookies
        self.sessions = sessions
        self.params = params
        self.query_instances = query_instances
        self.command_instances = command_instances
        self.body = body
        self.authorization = None
        if current_route is None:
            self.current_route = "mock_current_route"
        else:
            self.current_route = current_route

    @property
    def json_body(self):
        return self.body

    @property
    def session_id(self):
        return self.cookies["zaaksysteem_session"]

    @property
    def response(self):
        return mock.MagicMock()

    def retrieve_session(self):
        return self.sessions[self.session_id]

    def get_query_instance(self, domain, user_uuid, user_info=None):
        return self.query_instances[domain]

    def get_command_instance(self, domain, user_uuid, user_info=None):
        return self.command_instances[domain]

    def current_route_path(self):
        return self.current_route

    def route_path(self, name, _query=None):
        route = self.current_route.split("?")[0]
        if _query:
            route = f"{route}?keyword={_query['keyword']}"
        return route


def mock_protected_route(view):
    def view_wrapper(view):
        def request_wrapper(request):
            UserInfo = namedtuple("UserInfo", "user_uuid permissions")

            user_info = UserInfo(
                user_uuid="subject_uuid", permissions="permissions"
            )

            return view(request=request, user_info=user_info)

        return request_wrapper

    return view_wrapper


def test_log_record_factory():
    record = log_record_factory(
        name="foo",
        level="WARN",
        pathname="/dev/null",
        lineno=42,
        args={},
        msg="boo",
        exc_info=None,
    )

    assert record.msg == "boo"
    assert record.zs_component == "zsnl_communication_http"


class TestCommunicationViews:
    def setup_method(self):
        with mock.patch(
            "minty_pyramid.session_manager.protected_route",
            mock_protected_route,
        ):
            self.communication = communication
            self.current_route = "mock_current_route"
            self.request = MockRequest(
                cookies={"zaaksysteem_session": "a"},
                sessions={
                    "a": {
                        "__user": """
                    {"subject_uuid": "f60b8a3d-5496-463d-bd15-5180bde7cc34", "permissions": {"gebruiker": true} }
                    """
                    }
                },
                params={"keyword": "test search"},
                query_instances={
                    "zsnl_domains.communication": mock.MagicMock()
                },
                command_instances={
                    "zsnl_domains.communication": mock.MagicMock()
                },
            )

    def test_search_contact(self):
        uuid1 = uuid4()
        uuid2 = uuid4()
        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_contact.return_value = [
            Contact(
                id=123,
                uuid=uuid1,
                name="Test contact entry 1",
                type="contact_type",
                address="TEst test",
            ),
            Contact(
                id=1235,
                uuid=uuid2,
                name="Test contact entry 2",
                type="contact_type",
                address="TEst test",
            ),
        ]
        contents = search_contact(self.request)
        assert contents == {
            "data": [
                {
                    "id": str(uuid1),
                    "type": "contact",
                    "attributes": {
                        "type": "contact_type",
                        "name": "Test contact entry 1",
                        "address": "TEst test",
                        "email_address": None,
                    },
                },
                {
                    "id": str(uuid2),
                    "type": "contact",
                    "attributes": {
                        "type": "contact_type",
                        "name": "Test contact entry 2",
                        "address": "TEst test",
                        "email_address": None,
                    },
                },
            ],
            "links": {"self": {"href": "mock_current_route"}},
        }

    def test_search_contact_missing_parameter(self):
        self.request.params = {"test": str(uuid4())}
        self.request.query_instances[
            "zsnl_domains.communication"
        ].thread_list.return_value = []

        with pytest.raises(HTTPBadRequest):
            self.communication.search_contact(self.request)

    def test_search_contact_no_result(self):
        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_contact.return_value = []

        assert search_contact(self.request) == {
            "data": [],
            "links": {"self": {"href": "mock_current_route"}},
        }

    def _get_thread_messages(
        self,
        case_uuid,
        subject_uuid,
        thread_uuid,
        created_date,
        last_modified,
        slug,
        type,
        id,
        number_of_messages,
        unread_employee_count=0,
        unread_pip_count=0,
        attachment_count=0,
    ):
        case_id = 2
        case = Case(
            id=case_id,
            uuid=case_uuid,
            description=None,
            description_public=None,
            case_type_name=None,
            status="open",
        )
        contact = Contact(
            id=1, uuid=subject_uuid, name="admin", type="employee"
        )

        return Thread(
            uuid=thread_uuid,
            id=id,
            thread_type=type,
            contact=contact,
            created=created_date,
            last_modified=last_modified,
            last_message_cache={"slug": slug},
            case=case,
            number_of_messages=number_of_messages,
            unread_employee_count=unread_employee_count,
            unread_pip_count=unread_pip_count,
            attachment_count=attachment_count,
        )

    def test_thread_list(self):
        uuid = uuid4()
        case_uuid = uuid4()
        contact_uuid = uuid4()
        thread_uuid = uuid4()
        thread_uuid1 = uuid4()
        last_modified = created_date = datetime.utcnow()
        summary = "1-note-note message text"
        summary1 = "2-contact_moment-contact moment message text"

        self.request.params = {
            "filter[case_uuid]": str(uuid),
            "filter[message_types]": "contact_moment,note",
            "page": 5,
            "page_size": 50,
        }

        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_thread_list.return_value = [
            self._get_thread_messages(
                case_uuid=case_uuid,
                subject_uuid=contact_uuid,
                thread_uuid=thread_uuid,
                created_date=created_date,
                last_modified=last_modified,
                slug="note message text",
                type="note",
                id=1,
                number_of_messages=10,
                unread_pip_count=10,
                unread_employee_count=0,
                attachment_count=0,
            ),
            self._get_thread_messages(
                case_uuid=case_uuid,
                subject_uuid=contact_uuid,
                thread_uuid=thread_uuid1,
                created_date=created_date,
                last_modified=last_modified,
                slug="contact moment message text",
                type="contact_moment",
                id=2,
                number_of_messages=12,
                unread_pip_count=4,
                unread_employee_count=0,
                attachment_count=3,
            ),
        ]
        contents = thread_list(self.request)
        assert contents == {
            "data": [
                {
                    "id": str(thread_uuid),
                    "type": "message_thread",
                    "meta": {
                        "created": created_date.isoformat(),
                        "last_modified": last_modified.isoformat(),
                        "number_of_messages": 10,
                        "unread_pip_count": 10,
                        "unread_employee_count": 0,
                        "summary": summary,
                        "attachment_count": 0,
                    },
                    "attributes": {
                        "thread_type": "note",
                        "last_message": {"slug": "note message text"},
                    },
                    "relationships": {
                        "case": {
                            "data": {
                                "type": "case",
                                "id": str(case_uuid),
                                "attributes": {
                                    "display_id": 2,
                                    "description": None,
                                    "case_type_name": None,
                                    "status": "open",
                                },
                            }
                        },
                        "contact": {
                            "data": {
                                "type": "contact",
                                "id": str(contact_uuid),
                                "attributes": {
                                    "name": "admin",
                                    "type": "employee",
                                    "address": None,
                                    "email_address": None,
                                },
                            }
                        },
                    },
                    "links": {
                        "thread_messages": {
                            "href": f"/v2/communication/get_message_list?thread_uuid={thread_uuid}"
                        }
                    },
                },
                {
                    "id": str(thread_uuid1),
                    "type": "message_thread",
                    "meta": {
                        "created": created_date.isoformat(),
                        "last_modified": last_modified.isoformat(),
                        "number_of_messages": 12,
                        "unread_pip_count": 4,
                        "unread_employee_count": 0,
                        "summary": summary1,
                        "attachment_count": 3,
                    },
                    "attributes": {
                        "thread_type": "contact_moment",
                        "last_message": {
                            "slug": "contact moment message text"
                        },
                    },
                    "relationships": {
                        "case": {
                            "data": {
                                "type": "case",
                                "id": str(case_uuid),
                                "attributes": {
                                    "display_id": 2,
                                    "description": None,
                                    "case_type_name": None,
                                    "status": "open",
                                },
                            }
                        },
                        "contact": {
                            "data": {
                                "type": "contact",
                                "id": str(contact_uuid),
                                "attributes": {
                                    "name": "admin",
                                    "type": "employee",
                                    "address": None,
                                    "email_address": None,
                                },
                            }
                        },
                    },
                    "links": {
                        "thread_messages": {
                            "href": f"/v2/communication/get_message_list?thread_uuid={thread_uuid1}"
                        }
                    },
                },
            ],
            "links": {"self": {"href": "mock_current_route"}},
        }

    def test_thread_list_no_result(self):
        self.request.params = {
            "filter[case_uuid]": str(uuid4()),
            "filter[message_types]": "note",
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].thread_list.return_value = []

        assert thread_list(self.request) == {
            "data": [],
            "links": {"self": {"href": "mock_current_route"}},
        }

    def test_thread_list_paging_error_page_not_int(self):
        self.request.params = {
            "filter[case_uuid]": str(uuid4()),
            "filter[message_types]": "note",
            "page": "a",
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].thread_list.return_value = []

        with pytest.raises(HTTPBadRequest):
            _ = thread_list(self.request) == {
                "data": [],
                "links": {"self": {"href": "mock_current_route"}},
            }

    def test_thread_list_paging_error_page_size_not_int(self):
        self.request.params = {
            "filter[case_uuid]": str(uuid4()),
            "filter[message_types]": "note",
            "page_size": "a",
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].thread_list.return_value = []

        with pytest.raises(HTTPBadRequest):
            _ = thread_list(self.request) == {
                "data": [],
                "links": {"self": {"href": "mock_current_route"}},
            }

    def test_thread_list_paging_error_page_too_small(self):
        self.request.params = {
            "filter[case_uuid]": str(uuid4()),
            "filter[message_types]": "note",
            "page": "-1",
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].thread_list.return_value = []

        with pytest.raises(HTTPBadRequest):
            _ = thread_list(self.request) == {
                "data": [],
                "links": {"self": {"href": "mock_current_route"}},
            }

    def test_thread_list_paging_error_page_size_too_small(self):
        self.request.params = {
            "filter[case_uuid]": str(uuid4()),
            "filter[message_types]": "note",
            "page_size": "-1",
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].thread_list.return_value = []

        with pytest.raises(HTTPBadRequest):
            _ = thread_list(self.request) == {
                "data": [],
                "links": {"self": {"href": "mock_current_route"}},
            }

    def test_thread_list_paging_error_page_size_too_big(self):
        self.request.params = {
            "filter[case_uuid]": str(uuid4()),
            "filter[message_types]": "note",
            "page_size": "99999",
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].thread_list.return_value = []

        with pytest.raises(HTTPBadRequest):
            _ = thread_list(self.request) == {
                "data": [],
                "links": {"self": {"href": "mock_current_route"}},
            }

    def test_create_contact_moment(self):
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        contact_uuid = str(uuid4())
        cm_uuid = str(uuid4())
        self.request.body = {
            "thread_uuid": thread_uuid,
            "case_uuid": case_uuid,
            "contact_uuid": contact_uuid,
            "channel": "email",
            "content": "test content test content",
            "direction": "incoming",
            "contact_moment_uuid": cm_uuid,
        }

        return_value = self.communication.create_contact_moment(
            request=self.request
        )
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].create_contact_moment.assert_called_with(
            thread_uuid=thread_uuid,
            case_uuid=case_uuid,
            contact_uuid=contact_uuid,
            channel="email",
            content="test content test content",
            direction="incoming",
            contact_moment_uuid=cm_uuid,
        )

    def test_create_contact_moment_no_case(self):
        thread_uuid = str(uuid4())
        contact_uuid = str(uuid4())
        cm_uuid = str(uuid4())

        self.request.body = {
            "thread_uuid": thread_uuid,
            "contact_uuid": contact_uuid,
            "channel": "email",
            "content": "test content test content",
            "direction": "incoming",
            "contact_moment_uuid": cm_uuid,
        }

        return_value = self.communication.create_contact_moment(
            request=self.request
        )
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].create_contact_moment.assert_called_with(
            thread_uuid=thread_uuid,
            case_uuid=None,
            contact_uuid=contact_uuid,
            channel="email",
            content="test content test content",
            direction="incoming",
            contact_moment_uuid=cm_uuid,
        )

    def test_create_contact_moment_missing_body_payload(self):
        self.request.body = {
            "contact_uuid": str(uuid4()),
            "channel": "email",
            "direction": "incoming",
        }
        with pytest.raises(HTTPBadRequest):
            self.communication.create_contact_moment(self.request)

    def test_create_note_related_case(self):
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        note_uuid = str(uuid4())
        self.request.body = {
            "note_uuid": note_uuid,
            "thread_uuid": thread_uuid,
            "content": "test content test content",
            "case_uuid": case_uuid,
            "contact_uuid": None,
        }

        return_value = self.communication.create_note(request=self.request)
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].create_note.assert_called_with(
            note_uuid=note_uuid,
            thread_uuid=thread_uuid,
            content="test content test content",
            case_uuid=case_uuid,
            contact_uuid=None,
        )

    def test_create_note_related_contact(self):
        thread_uuid = str(uuid4())
        note_uuid = str(uuid4())
        contact_uuid = str(uuid4())
        self.request.body = {
            "note_uuid": note_uuid,
            "thread_uuid": thread_uuid,
            "content": "test content test content",
            "case_uuid": None,
            "contact_uuid": contact_uuid,
        }

        return_value = self.communication.create_note(request=self.request)
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].create_note.assert_called_with(
            note_uuid=note_uuid,
            thread_uuid=thread_uuid,
            content="test content test content",
            contact_uuid=contact_uuid,
            case_uuid=None,
        )

    def test_create_note_no_case_or_contact(self):
        thread_uuid = str(uuid4())
        note_uuid = str(uuid4())
        self.request.body = {
            "note_uuid": note_uuid,
            "thread_uuid": thread_uuid,
            "content": "test content test content",
        }

        return_value = self.communication.create_note(request=self.request)
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].create_note.assert_called_with(
            thread_uuid=thread_uuid,
            note_uuid=note_uuid,
            content="test content test content",
            case_uuid=None,
            contact_uuid=None,
        )

    def test_create_note_missing_body_payload(self):
        self.request.body = {"contact_uuid": str(uuid4())}
        with pytest.raises(HTTPBadRequest):
            self.communication.create_note(self.request)

    def test_get_message_list(self):
        self.request.params = {"thread_uuid": str(uuid4())}

        CaseMock = namedtuple(
            "Case", "uuid id description case_type_name status"
        )
        uuid_case = str(uuid4())
        case = CaseMock(
            id=1234,
            uuid=uuid_case,
            description="test",
            case_type_name="test",
            status="open",
        )

        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_message_list.return_value = {"messages": [], "case": case}

        res = self.communication.get_message_list(self.request)

        assert res == {
            "data": [],
            "meta": {"api_version": 2},
            "links": {"self": {"href": "mock_current_route"}},
            "relationships": {
                "case": {
                    "data": {
                        "type": "case",
                        "id": uuid_case,
                        "attributes": {
                            "display_id": 1234,
                            "description": "test",
                            "case_type_name": "test",
                            "status": "open",
                        },
                    }
                }
            },
        }

        self.request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.communication.get_message_list(self.request)

    def test_search_case(self):
        uuid1 = uuid4()
        uuid2 = uuid4()
        case_row = namedtuple(
            "case",
            "uuid id description description_public case_type_name status",
        )
        case1 = case_row(
            uuid=uuid1,
            id=1,
            description="desc1",
            description_public="pubdesc1",
            case_type_name="case_search_name1",
            status="open",
        )

        case2 = case_row(
            uuid=uuid2,
            id=2,
            description="desc2",
            description_public="pubdesc2",
            case_type_name="case_search_name2",
            status="open",
        )
        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_cases.return_value = [case1, case2]
        self.request.params = {
            "search_term": "case1",
            "minimum_permission": "search",
        }

        contents = search_case(self.request)
        assert contents == {
            "data": [
                {
                    "type": "case",
                    "id": str(uuid1),
                    "attributes": {
                        "display_id": 1,
                        "description": "desc1",
                        "case_type_name": "case_search_name1",
                        "status": "open",
                    },
                },
                {
                    "type": "case",
                    "id": str(uuid2),
                    "attributes": {
                        "display_id": 2,
                        "description": "desc2",
                        "case_type_name": "case_search_name2",
                        "status": "open",
                    },
                },
            ],
            "links": {"self": {"href": "mock_current_route"}},
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_cases.assert_called_once_with(
            search_term="case1",
            permission="search",
            case_status_filter=None,
            limit=None,
        )

    def test_search_case_filtered(self):
        uuid1 = uuid4()
        uuid2 = uuid4()
        case_row = namedtuple(
            "case",
            "uuid id description description_public case_type_name status",
        )
        case1 = case_row(
            uuid=uuid1,
            id=1,
            description="desc",
            description_public="pubdesc",
            case_type_name="case_search_name1",
            status="open",
        )

        case2 = case_row(
            uuid=uuid2,
            id=2,
            description="desc2",
            description_public="pubdesc2",
            case_type_name="case_search_name2",
            status="open",
        )

        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_cases.return_value = [case1, case2]
        self.request.params = {
            "search_term": "case1",
            "minimum_permission": "search",
            "filter[status]": "open,resolved",
            "limit": 1234,
        }

        contents = search_case(self.request)
        assert contents == {
            "data": [
                {
                    "type": "case",
                    "id": str(uuid1),
                    "attributes": {
                        "display_id": 1,
                        "description": "desc",
                        "case_type_name": "case_search_name1",
                        "status": "open",
                    },
                },
                {
                    "type": "case",
                    "id": str(uuid2),
                    "attributes": {
                        "display_id": 2,
                        "description": "desc2",
                        "case_type_name": "case_search_name2",
                        "status": "open",
                    },
                },
            ],
            "links": {"self": {"href": "mock_current_route"}},
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_cases.assert_called_once_with(
            search_term="case1",
            permission="search",
            case_status_filter=["open", "resolved"],
            limit=1234,
        )

    def test_get_case_list_for_contact(self):
        case_row = namedtuple(
            "case", """uuid id description case_type_name status"""
        )
        uuid1 = str(uuid4())
        contact_uuid = str(uuid4())

        self.request.params = {"contact_uuid": contact_uuid}
        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_case_list_for_contact.return_value = [
            case_row(
                uuid=uuid1,
                id=1,
                description="desc",
                case_type_name="case_search_name1",
                status="open",
            )
        ]

        result = self.communication.get_case_list_for_contact(self.request)
        assert result == {
            "data": [
                {
                    "type": "case",
                    "id": uuid1,
                    "attributes": {
                        "display_id": 1,
                        "description": "desc",
                        "case_type_name": "case_search_name1",
                        "status": "open",
                    },
                }
            ],
            "links": {"self": {"href": "mock_current_route"}},
        }
        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_case_list_for_contact.assert_called_once_with(
            contact_uuid=contact_uuid
        )

    def test_get_case_list_for_contact_no_contact_uuid(self):
        self.request.params = {}

        with pytest.raises(HTTPBadRequest):
            self.communication.get_case_list_for_contact(self.request)

    def test_search_case_missing_parameter(self):
        self.request.params = {"test": str(uuid4())}
        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_cases.return_value = []

        with pytest.raises(HTTPBadRequest):
            self.communication.search_case(self.request)

    def test_search_case_limit_bad_parameter(self):
        self.request.params = {"limit": str(uuid4())}
        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_cases.return_value = []

        with pytest.raises(ValidationError):
            self.communication.search_case(self.request)

    def test_search_case_no_result(self):
        self.request.query_instances[
            "zsnl_domains.communication"
        ].search_cases.return_value = []

        assert search_contact(self.request) == {
            "data": [],
            "links": {"self": {"href": "mock_current_route"}},
        }

    def test_create_external_message(self):
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        message_uuid = str(uuid4())
        participants = [
            {
                "address": "test@test.com",
                "role": "to",
                "display_name": "test",
                "uuid": "7b191dec-ad52-47e1-ac80-7d63a68b2acc",
            }
        ]

        self.request.body = {
            "thread_uuid": thread_uuid,
            "case_uuid": case_uuid,
            "subject": "a subject",
            "content": "test content test content",
            "message_type": "pip",
            "message_uuid": message_uuid,
            "attachments": [],
            "direction": "incoming",
            "participants": participants,
        }

        return_value = self.communication.create_external_message(
            request=self.request
        )
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].create_external_message.assert_called_with(
            external_message_uuid=message_uuid,
            thread_uuid=thread_uuid,
            subject="a subject",
            content="test content test content",
            message_type="pip",
            case_uuid=case_uuid,
            attachments=[],
            direction="incoming",
            participants=participants,
        )

    def test_create_external_message_no_case(self):
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        message_uuid = str(uuid4())

        self.request.body = {
            "thread_uuid": thread_uuid,
            "case_uuid": case_uuid,
            "subject": None,
            "content": "test content test content",
            "message_uuid": message_uuid,
            "message_type": "pip",
            "attachments": [],
        }

        return_value = self.communication.create_external_message(
            request=self.request
        )
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].create_external_message.assert_called_with(
            external_message_uuid=message_uuid,
            thread_uuid=thread_uuid,
            subject=None,
            content="test content test content",
            message_type="pip",
            case_uuid=case_uuid,
            attachments=[],
            participants=[],
            direction="unspecified",
        )

    def test_create_external_message_missing_body_payload(self):
        self.request.body = {
            "case_uuid": str(uuid4()),
            "subject": "a subject",
            "contact": "some content",
        }
        with pytest.raises(HTTPBadRequest):
            self.communication.create_external_message(self.request)

    def test_link_thread_to_case(self):
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        self.request.body = {
            "thread_uuid": thread_uuid,
            "case_uuid": case_uuid,
            "type": "email",
        }

        return_value = self.communication.link_thread_to_case(
            request=self.request
        )
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].link_thread_to_case.assert_called_with(
            thread_uuid=thread_uuid,
            external_message_type="email",
            case_uuid=case_uuid,
        )

    def test_link_thread_to_case_no_case_with_wrong_type(self):
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())

        self.request.body = {
            "thread_uuid": thread_uuid,
            "case_uuid": case_uuid,
            "type": "pip",
        }

        return_value = self.communication.link_thread_to_case(
            request=self.request
        )
        assert return_value == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].link_thread_to_case.assert_called_with(
            thread_uuid=thread_uuid,
            external_message_type="pip",
            case_uuid=case_uuid,
        )

    def test_link_thread_to_case_missing_body_payload(self):
        self.request.body = {}
        with pytest.raises(HTTPBadRequest):
            self.communication.link_thread_to_case(self.request)

    def test_download_attachment_no_parameters(self):
        self.request.params = {"no_id": str(uuid4())}
        with pytest.raises(HTTPBadRequest):
            self.communication.download_attachment(self.request)

    def test_download_attachment(self):
        uuid = str(uuid4())
        self.request.params = {"id": uuid}

        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_download_link.return_value = "https://url.to_download.from"

        response = self.communication.download_attachment(self.request)
        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_download_link.assert_called_with(attachment_uuid=uuid)
        assert response.status_int == 302
        assert response.location == "https://url.to_download.from"

    def test_delete_message_no_message_uuid(self):
        self.request.body = {}
        with pytest.raises(HTTPBadRequest):
            self.communication.delete_message(self.request)

    def test_delete_message(self):
        message_uuid = str(uuid4())
        self.request.body = {"message_uuid": message_uuid}

        response = self.communication.delete_message(self.request)

        assert response == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].delete_message.asssert_called_once_with(message_uuid=message_uuid)

    def test_import_email_message(self):
        file_uuid = str(uuid4())
        case_uuid = str(uuid4())
        self.request.body = {"case_uuid": case_uuid, "file_uuid": file_uuid}
        response = self.communication.import_email_message(self.request)

        assert response == {"data": {"success": True}}

        self.request.command_instances[
            "zsnl_domains.communication"
        ].process_email_file.asssert_called_once_with(
            case_uuid=case_uuid, file_uuid=file_uuid
        )

    def test_import_email_message_no_params(self):
        self.request.body = {}
        with pytest.raises(HTTPBadRequest):
            self.communication.import_email_message(self.request)

    def test_mark_messages_read(self):
        message_uuids = [str(uuid4())]
        self.request.body = {"message_uuids": message_uuids, "context": "pip"}

        response = self.communication.mark_messages_read(self.request)
        assert response == {"data": {"success": True}}
        self.request.command_instances[
            "zsnl_domains.communication"
        ].mark_messages_read.asssert_called_once_with(
            message_uuids=message_uuids, context="pip"
        )

        with pytest.raises(HTTPBadRequest):
            self.request.body = {"message_uuids": message_uuids}
            response = self.communication.mark_messages_read(self.request)

    def test_mark_messages_unread(self):
        message_uuids = [str(uuid4())]
        self.request.body = {"message_uuids": message_uuids, "context": "pip"}

        response = self.communication.mark_messages_unread(self.request)
        assert response == {"data": {"success": True}}
        self.request.command_instances[
            "zsnl_domains.communication"
        ].mark_messages_unread.asssert_called_once_with(
            message_uuids=message_uuids, context="pip"
        )

        with pytest.raises(HTTPBadRequest):
            self.request.body = {"message_uuids": message_uuids}
            response = self.communication.mark_messages_unread(self.request)

    def test_preview_attachment(self):
        uuid = str(uuid4())
        self.request.params = {"id": uuid}

        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_preview_link.return_value = "https://url.to_preview.from"

        response = self.communication.preview_attachment(self.request)
        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_preview_link.assert_called_with(attachment_uuid=uuid)
        assert response.status_int == 302
        assert response.location == "https://url.to_preview.from"

        with pytest.raises(HTTPBadRequest):
            self.request.params = {}
            self.communication.preview_attachment(self.request)

    def _get_contact_moment_overview(
        self,
        contact_uuid,
        contact,
        case_id,
        direction,
        uuid,
        created,
        summary,
        channel,
        thread_uuid,
        contact_type,
    ):
        return ContactMomentOverview(
            contact_uuid=contact_uuid,
            contact=contact,
            case_id=case_id,
            direction=direction,
            uuid=uuid,
            created=created,
            summary=summary,
            channel=channel,
            thread_uuid=thread_uuid,
            contact_type=contact_type,
        )

    def test_get_contact_moment_list(self):
        uuid1 = uuid4()
        uuid2 = uuid4()
        contact_uuid1 = uuid4()
        thread_uuid1 = uuid4()
        contact_uuid2 = uuid4()
        thread_uuid2 = uuid4()
        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )

        self.request.query_instances[
            "zsnl_domains.communication"
        ].get_contact_moment_list.return_value = [
            self._get_contact_moment_overview(
                contact_uuid=contact_uuid1,
                contact="testOrg",
                case_id=20,
                direction="outgoing",
                uuid=uuid1,
                created=created1,
                summary="Test organization.",
                channel="mail",
                thread_uuid=thread_uuid1,
                contact_type="organization",
            ),
            self._get_contact_moment_overview(
                contact_uuid=contact_uuid2,
                contact="testOrg1",
                case_id=21,
                direction="incoming",
                uuid=uuid2,
                created=created2,
                summary="Test organization1.",
                channel="post",
                thread_uuid=thread_uuid2,
                contact_type="organization",
            ),
        ]
        contents = get_contact_moment_list(self.request)
        assert contents == {
            "data": [
                {
                    "type": "contact_moment_overview",
                    "id": str(uuid1),
                    "attributes": {
                        "contact_type": "organization",
                        "contact": "testOrg",
                        "contact_uuid": contact_uuid1,
                        "channel": "mail",
                        "summary": "Test organization.",
                        "registration_date": created1,
                        "direction": "outgoing",
                        "case_id": 20,
                    },
                },
                {
                    "type": "contact_moment_overview",
                    "id": str(uuid2),
                    "attributes": {
                        "contact_type": "organization",
                        "contact": "testOrg1",
                        "contact_uuid": contact_uuid2,
                        "channel": "post",
                        "summary": "Test organization1.",
                        "registration_date": created2,
                        "direction": "incoming",
                        "case_id": 21,
                    },
                },
            ]
        }
