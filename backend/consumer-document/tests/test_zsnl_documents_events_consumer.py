# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from zsnl_documents_events_consumer.constants import external_message_prefix
from zsnl_documents_events_consumer.consumers import DocumentsEventsConsumer
from zsnl_documents_events_consumer.handlers import (
    BaseHandler,
    MessageArchivedHandler,
)


class TestCaseEventsConsumers:
    def test_base_handler(self):
        base_handler = BaseHandler(mock.MagicMock())
        assert base_handler.routing_keys == []

    def test_consumer(self):
        documents_events_consumer = DocumentsEventsConsumer(
            queue="fake_queue_name",
            exchange="fake_exchange",
            cqrs=mock.MagicMock(),
            qos_prefetch_count=2,
            dead_letter_config={},
        )
        assert documents_events_consumer.routing_keys == [
            external_message_prefix + "MessageArchived"
        ]

    def test_message_archived_handler(self):
        mock_cqrs = mock.MagicMock()
        mock_cmd = mock.MagicMock()
        mock_cqrs.get_command_instance.return_value = mock_cmd
        message_archived_handler = MessageArchivedHandler(mock_cqrs)
        assert message_archived_handler.routing_keys == [
            external_message_prefix + "MessageArchived"
        ]
        assert message_archived_handler.cqrs == mock_cqrs

        mock_event = mock.MagicMock()
        mock_event.event_name = ("MessageArchived",)
        mock_event.correlation_id = "fake_correlaton_id"
        mock_event.context = "fake_context"
        mock_event.user_uuid = "fake_user_uuid"
        mock_event.entity_id = "fake_message_uuid"

        mock_event.changes = [
            {
                "key": "original_message_file",
                "old_value": None,
                "new_value": "fake_file_uuid",
            }
        ]

        message_archived_handler.handle(mock_event)
        mock_cmd.create_document_from_message_archive.assert_called_once_with(
            message_uuid="fake_message_uuid", file_uuid="fake_file_uuid"
        )
