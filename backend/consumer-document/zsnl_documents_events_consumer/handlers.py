# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty import Base
from zsnl_documents_events_consumer.constants import external_message_prefix


class BaseHandler(Base):
    def __init__(self, cqrs):
        self.cqrs = None
        self.routing_keys = []

    def get_command_instance(self, event):
        return self.cqrs.get_command_instance(
            event.correlation_id,
            "zsnl_domains.document",
            event.context,
            event.user_uuid,
        )


class MessageArchivedHandler(BaseHandler):
    def __init__(self, cqrs):
        self.routing_keys = [external_message_prefix + "MessageArchived"]
        self.cqrs = cqrs

    def handle(self, event):
        command_instance = self.get_command_instance(event)
        changes = {
            change["key"]: change["new_value"] for change in event.changes
        }
        message_uuid = event.entity_id
        command_instance.create_document_from_message_archive(
            message_uuid=message_uuid,
            file_uuid=changes["original_message_file"],
        )
