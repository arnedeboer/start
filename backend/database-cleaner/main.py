import os
from helper import Connection
import time
import logging
import sys
from queue import Queue
import threading

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger(__name__)

def worker(worker_id: int):
    while not DATABASE_QUEUE.empty():
        db, index = DATABASE_QUEUE.get()
        conn = Connection(db=db)
        now = time.perf_counter()
        for function in FUNCTIONS:
            logger.info(f"Worker-{worker_id}: Running {function=} on {db=}")
            try:
                conn.query(f"SELECT {function}();")
                logger.info(f"Worker-{worker_id}: Finished running {function=} on {db=}")
            except RuntimeError as ex:
                logger.error(ex)
        post = time.perf_counter()
        logger.info(f"Worker-{worker_id}: Queries on {db} took {(post - now):.2f} seconds [{index}/{DATABASE_COUNT}]")
        conn.close()

FUNCTIONS = [function for function in os.environ.get("DB_FUNCTIONS").split(",")]
DATABASE_QUEUE = Queue()
DATABASE_LIST = Connection().list_databases()
DATABASE_COUNT = len(DATABASE_LIST)
WORKER_COUNT = 50

for index, db in enumerate(DATABASE_LIST):
    DATABASE_QUEUE.put((db, index))

threads = []
for worker_id in range(WORKER_COUNT):
    thread = threading.Thread(target=worker, args=(worker_id,))
    threads.append(thread)
    thread.start()

for thread in threads:
    thread.join()
