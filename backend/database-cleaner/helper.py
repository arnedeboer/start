import psycopg2
import os

DB_HOST = "DB_HOST"
DB_PORT = "DB_PORT"
DB_USER = "DB_USER"
DB_PASS = "DB_PASS"

class Connection:
    def __init__(self, db="template1"):
        # Setup connection to db server
        self.host = os.environ.get(DB_HOST)
        self.port = os.environ.get(DB_PORT)
        self.user = os.environ.get(DB_USER)
        self.password = os.environ.get(DB_PASS)

        self.conn = psycopg2.connect(
            host=self.host,
            database=db,
            port=self.port,
            password=self.password,
            user=self.user
        )

        self._cursor = self.conn.cursor()

    def query(self, query: str) -> list:
        try:
            self._cursor.execute(query)
        except psycopg2.errors.UndefinedFunction:
            self.conn.rollback()
            raise RuntimeError(f"Clould not execute {query=}")

    def list_databases(self):
        self._cursor.execute(
            "SELECT datname FROM pg_database where datconnlimit=-1 and datdba!=10 and datname not in ('zaaksysteem_template', 'template1', 'postgres', 'template0');"
        )
        return [db[0] for db in self._cursor.fetchall()]        

    def close(self):
        self.conn.close()
        self._cursor.close()
