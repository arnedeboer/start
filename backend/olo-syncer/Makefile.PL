#!/usr/bin/perl -w

use ExtUtils::MakeMaker;

WriteMakefile(
    NAME => 'Zaaksysteem::OLO::Sync',
    VERSION => 'v0.0.4',
    AUTHOR => 'Rudolf Leermakers <rudolf@mintlab.nl>',
    ABSTRACT => 'Zaaksysteem OLO::Sync (micro)service',
    TEST_REQUIRES => {
        'CPAN::Audit' => 0,
        'Sub::Override' => 0,
        'Test::Class::Moose' => 0,
        'Test::Compile' => 0,
        'Test::Mock::One' => 0,
        'Test::More' => 0,
        'Test::Pod' => 0,
        'Test::Pod::Coverage' => 0,
    },
    PREREQ_PM => {
        'BTTW::Tools' => 0,
        'DateTime::Format::ISO8601' => 0,
        'HTTP::Request' => 0,
        'HTTP::Response' => 0,
        'HTTP::Headers' => 0,
        'IO::Scalar' => 0,
        'IO::File' => 0,
        'JSON::XS' => 0,
        'LWP::UserAgent' => 0,
        'LWP::Protocol::https' => 0,
        'Net::FTPSSL' => 0,
        'Path::Class' => 0,
        'Syzygy' => 0,
        'URI' => 0,
        'XML::LibXML' => 0,
        'XML::LibXML::XPathContext' => 0
    }
);

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::OLO::Sync uses the EUPL license, for more information please have a look at the C<LICENSE> file.

