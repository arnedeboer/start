package Zaaksysteem::OLO::Repository::FTP;

use Moose;
use namespace::autoclean;

with qw[
    MooseX::Log::Log4perl
    Zaaksysteem::OLO::Interface::Repository
];

=head1 NAME

Zaaksysteem::OLO::Repository::FTP - FTP-based OmgevingsLoketOline repository
implementation.

=head1 DESCRIPTION

This class implements a L<Zaaksysteem::OLO::Interface::Repository> which uses
an FTP server as source of messages.

=head1 SYNOPSIS

    my $repo = Zaaksysteem::OLO::Repository::FTP->new(
        hostname => 'my.ftp.server.tld',
        username => 'user',
        password => '****'
    );

    my @messages = $repo->get_messages;

=cut

use BTTW::Tools;
use IO::Scalar;
use File::Temp;
use Net::FTPSSL;
use Zaaksysteem::OLO::Message;

=head1 ATTRIBUTES

=head2 hostname

Hostname of the FTP server to connect to.

=cut

has hostname => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head2 port

Port number of the FTP server to connect to.

=cut

has port => (
    is => 'rw',
    isa => 'Int',
    default => 21
);

=head2 username

Username for the FTP account to login with.

Defaults to value C<anonymous>.

=cut

has username => (
    is => 'rw',
    isa => 'Str',
    default => 'anonymous'
);

=head2 password

Password for the FTP account to login with.

Defaults to value C<anonymous>.

=cut

has password => (
    is => 'rw',
    isa => 'Str',
    default => 'anonymous'
);

=head1 METHODS

=head2 get_messages

Implements L<Zaaksysteem::OLO::Interface::Repository/get_messages>.

=cut

sub get_messages {
    my $self = shift;
    my $since = shift;

    my @messages;

    my $ftp = $self->_build_client;

    my $since_dt = $since ? $since->iso8601 : undef;

    for my $application_dir ($ftp->nlst('/berichten')) {
        for my $message_filename ($ftp->nlst($application_dir)) {
            next unless $message_filename =~ m[\.xml$];

            $self->log->trace(sprintf(
                'Found message "%s"',
                $message_filename
            ));

            my ($timestamp_str, $id) = split m[_], $message_filename;

            my $timestamp = $self->parse_timestamp($timestamp_str);

            if ($since && $since->compare($timestamp) > 0) {
                $self->log->debug(
                    sprintf(
                        "Skipping message '%s', older than %s",
                        $message_filename, $since_dt
                    )
                );
                next;
            }

            $self->log->info("Got message $message_filename");

            push @messages, Zaaksysteem::OLO::Message->new(
                entry_id  => $message_filename,
                timestamp => $timestamp,
                _content_resolver => sub {
                    my $content;
                    my $fh = IO::Scalar->new(\$content);

                    $ftp->get($message_filename, $fh);
                    $fh->close;

                    return $content;
                }
            );
        }
    }

    return @messages
}

=head2 get_file

Implements L<Zaaksyteem::OLO::Interface::Repository/get_file>.

Returns a L<IO::Handle>-based object from which the file content can be read.

=cut

sub get_file {
    my $self = shift;
    my $path_str = shift;
    
    my $ftp = $self->_build_client;
    my $tmp = File::Temp->new();

    $ftp->get($path_str, $tmp);

    # Reset the file so we can read from the start
    $tmp->seek(0, 0);

    return $tmp;
}

=head1 PRIVATE METHODS

=head2 _build_client

Builds a new L<Net::FTPSSL> object.

=head3 Exceptions

=over 4

=item olo/repository/ftp/login_failure

Login failure

=item olo/repository/ftp/server_not_reachable

Server did not respond with C<OK> after sending a C<NOOP>.

=back

=cut

sub _build_client {
    my $self = shift;

    my $ftp = Net::FTPSSL->new($self->hostname, (
        Encryption => EXP_CRYPT,
        Debug => 0,
        ReuseSession => 1,
        Port => $self->port
    ));

    unless ($ftp->login($self->username, $self->password)) {
        throw('olo/repository/ftp/login_failure', sprintf(
            'Login failure for user "%s" on "%s:%d"',
            $self->username,
            $self->hostname,
            $self->port
        ));
    }

    $ftp->force_epsv(1);
    $ftp->binary;

    # Check server is alive
    unless ($ftp->noop) {
        throw('olo/repository/ftp/server_not_reachable', sprintf(
            'Could not ping "%s"',
            $self->hostname
        ));
    }

    return $ftp;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
