package Zaaksysteem::OLO::Sync::Service;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Service::HTTP';

=head1 NAME

Zaaksysteem::OLO::Sync::Service - HTTP service for triggering synchronisation
of OLO messages

=head1 DESCRIPTION

This class implements a L<Zaaksysteem::Service::HTTP> for
OmgevingsLoketOnline repository->zaaksysteem synchronisation.

=cut

use BTTW::Tools;
use Syzygy::Object::Model;
use Zaaksysteem::API::Client;

=head1 ATTRIBUTES

=head2 object_model

Holds a reference to the singleton L<Syzygy::Object::Model>.

=cut

has object_model => (
    is => 'rw',
    isa => 'Syzygy::Object::Model',
    default => sub {
        Syzygy::Object::Model->get_instance
    }
);

=head2 new_api_client

Instantiates a new L<Zaaksysteem::API::Client> instance, using the
C<Zaaksysteem::API::Client> configuration.

=cut

sub new_api_client {
    my $self = shift;

    my %config = %{
        $self->service_config->{ 'Zaaksysteem::API::Client' } || {}
    };

    $config{ hostname } = shift;

    return Zaaksysteem::API::Client->new(%config);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem::OLO::Sync uses the EUPL license, for more information please have a look at the C<LICENSE> file.

