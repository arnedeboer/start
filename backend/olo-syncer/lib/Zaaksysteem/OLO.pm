package Zaaksysteem::OLO;

use strict;
use warnings;

our $VERSION = 'v0.0.4';

=head1 NAME

Zaaksysteem::OLO - OmgevingsLoketOnline service for Zaaksysteem

=head1 DESCRIPTION

OmgevingsLoketOnline is a provider of a webservice that manages planning
permit requests for municipalities. This service is a one-way integration
from OLO's infrastructure to Zaaksysteem.

The service exposes a handful of HTTP APIs that can trigger a synchronisation
between the platforms.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the C<LICENSE> file.
