# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class DocumentLabelViews(JSONAPIEntityView):
    # Move this away in new version, and do this sexyer
    def create_link_from_entity(
        self, entity=None, entity_type=None, entity_id=None
    ):
        return None

    # Move this to openapi.json
    view_mapper = {
        "GET": {
            "get_document_labels": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.document",
                "run": "get_document_labels_for_case",
                "from": {"request_params": {"case_uuid": "case_uuid"}},
            }
        }
    }
