# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import importlib
import io
import json
import pytest
from collections import namedtuple
from datetime import datetime
from minty.cqrs import UserInfo
from pyramid.exceptions import HTTPBadRequest, HTTPForbidden
from unittest import mock
from uuid import UUID, uuid4
from webob import multidict
from zsnl_document_http import log_record_factory

user_info = UserInfo(user_uuid="subject_uuid", permissions="permissions")


def mock_protected_route(permission, *args):
    def view_wrapper(view):
        def request_wrapper(request):
            return view(request=request, user_info=user_info)

        return request_wrapper

    return view_wrapper


class TestDocument:
    def setup_method(self):
        with mock.patch(
            "minty_pyramid.session_manager.protected_route",
            mock_protected_route,
        ):
            from zsnl_document_http.views import views

            importlib.reload(views)
            self.views = views

            document_uuid = uuid4()
            self.mock_directory_entry_document = mock.MagicMock()
            self.mock_directory_entry_document.configure_mock(
                uuid=document_uuid,
                type="directory_entry",
                entry_type="document",
                name="doc1",
                extension="pdf",
                mimetype="application/pdf",
                accepted=True,
                document_number=3,
                last_modified_date_time=datetime(2015, 5, 19),
                document={
                    "uuid": document_uuid,
                    "preview_available": True,
                    "thumbnail_uuid": uuid4(),
                    "thumbnail_mimetype": "application/pdf",
                },
                directory=None,
                case={
                    "uuid": uuid4(),
                    "type": "case",
                    "display_number": 1234,
                },
                parent={"uuid": uuid4(), "display_name": "folder_1"},
                modified_by={
                    "uuid": uuid4(),
                    "type": "subject",
                    "display_name": "beheerder",
                },
                rejection_reason=None,
                rejected_by_display_name=None,
            )

            directory_uuid = uuid4()
            self.mock_directory_entry_directory = mock.MagicMock()
            self.mock_directory_entry_directory.configure_mock(
                uuid=directory_uuid,
                type="directory_entry",
                entry_type="directory",
                name="folder",
                description=None,
                extension=None,
                mimetype=None,
                accepted=None,
                document_number=None,
                last_modified_date_time=None,
                document=None,
                directory={"uuid": directory_uuid, "type": "directory"},
                case={"uuid": uuid4(), "type": "case", "display_number": 1234},
                parent={"uuid": uuid4(), "display_name": "folder_1"},
                modified_by={
                    "uuid": uuid4(),
                    "type": "subject",
                    "display_name": "beheerder",
                },
                rejection_reason=None,
                rejected_by_display_name=None,
            )

            self.mock_directory = mock.MagicMock()
            self.mock_directory.configure_mock(
                uuid=directory_uuid,
                name="dir1",
                parent={"uuid": uuid4()},
            )

    def test_create_file(self):
        request = mock.MagicMock()
        request.POST = {"file": None}

        with pytest.raises(HTTPBadRequest):
            self.views.create_file(request)

        request.POST = {"file": mock.MagicMock()}
        request.POST["file"].filename = "test_file.txt"
        request.POST["file"].file = io.BytesIO(b"test_content")

        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_file.return_value = True
        request.get_command_instance.return_value = mock_command_instance
        request.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": None}]
        }
        return_value = self.views.create_file(request)
        mock_command_instance.create_file.assert_called()

        assert return_value["data"]["type"] == "file"
        assert return_value["data"]["id"] is not None

        request.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": "s3Legacy"}]
        }
        return_value = self.views.create_file(request)
        mock_command_instance.create_file.assert_called()

        assert return_value["data"]["type"] == "file"
        assert return_value["data"]["id"] is not None

        request.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": "s3"}]
        }
        return_value = self.views.create_file(request)
        mock_command_instance.create_file.assert_called()

        assert return_value["data"]["type"] == "file"
        assert return_value["data"]["id"] is not None

    def _get_request(self):
        req_post = mock.MagicMock()
        req_post.POST = multidict.MultiDict()
        req_post.POST["case_uuid"] = str(uuid4())
        req_post.POST["document_file"] = mock.Mock()
        req_post.POST["document_file"].filename = "test_file.txt"
        req_post.POST["document_file"].file = io.BytesIO(b"test_content")
        req_post.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": "swift"}]
        }
        req_post.get_command_instance = mock.MagicMock()

        return req_post

    def test_create_document(self):
        req = mock.MagicMock()
        req.POST = {}
        with pytest.raises(HTTPBadRequest):
            self.views.create_document(req)

        req = self._get_request()
        del req.POST["case_uuid"]
        with pytest.raises(HTTPBadRequest):
            self.views.create_document(req)

        req = self._get_request()
        self.views.create_document(req)
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_document = mock.MagicMock()
        mock_command_instance.create_document().return_value = True

        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": None}]
        }

        self.views.create_document(req)
        mock_command_instance.create_document.assert_called()
        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": "s3Legacy"}]
        }

        self.views.create_document(req)
        mock_command_instance.create_document.assert_called()

    def test_create_document_bad_upload(self):
        req = self._get_request()
        req.POST["document_file"] = "some string"

        with pytest.raises(HTTPBadRequest):
            self.views.create_document(req)

    def test_create_document_skip_intake(self):
        req = self._get_request()
        del req.POST["case_uuid"]
        req.POST["skip_intake"] = True

        self.views.create_document(req)
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_document = mock.MagicMock()
        mock_command_instance.create_document().return_value = True

        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": None}]
        }

        self.views.create_document(req)
        mock_command_instance.create_document.assert_called()
        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": "s3Legacy"}]
        }

        self.views.create_document(req)
        mock_command_instance.create_document.assert_called()

    def test_create_document_with_magicstring(self):
        req = self._get_request()
        req.POST.add("magic_string", "magic1")
        req.POST.add("magic_string", "magic2")

        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_document = mock.MagicMock()
        mock_command_instance.create_document().return_value = True

        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": None}]
        }

        self.views.create_document(req)
        mock_command_instance.create_document.assert_called()

    def test_create_document_with_directory_and_replaces(self):
        req = self._get_request()
        req.POST["directory_uuid"] = str(uuid4())
        req.POST["replaces"] = str(uuid4())

        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_document = mock.MagicMock()
        mock_command_instance.create_document().return_value = True

        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": None}]
        }

        self.views.create_document(req)
        mock_command_instance.create_document.assert_called()

    def test_create_document_from_attachment(self):
        req = mock.MagicMock()
        req.json_body = {}
        with pytest.raises(HTTPBadRequest):
            self.views.create_document_from_attachment(req)

        req = self._get_request()
        self.views.create_document_from_attachment(req)
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_document_from_attachment = (
            mock.MagicMock()
        )
        mock_command_instance.create_document_from_attachment().return_value = (
            True
        )

        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": None}]
        }

        self.views.create_document_from_attachment(req)
        mock_command_instance.create_document_from_attachment.assert_called()

    def test__serialize_document(self):
        fake_data = {
            "mimetype": "fake_mimetype",
            "size": 1,
            "storage_location": "fake_storage_location",
            "md5": "fake_md5",
            "is_archivable": "fake_is_archivable",
            "virus_scan_status": "fake_virus_scan_status",
            "document_uuid": "fake_document_uuid",
            "basename": "fake_filename.fake_extension",
            "extension": ".fake_extension",
            "store_uuid": "fake_store_uuid",
            "directory_uuid": "fake_directory_uuid",
            "case_uuid": "fake_case_uuid",
            "case_display_number": 123,
            "type": "document",
            "created_by": "fake_created_by",
            "date_modified": datetime(2019, 10, 10),
            "thumbnail": "fake_thumbnail",
            "creator_uuid": "fake_creator_uuid",
            "creator_displayname": "fake_creator_displayname",
            "accepted": "fake_accepted",
            "document_number": 1,
            "description": "fake_description",
            "confidentiality": "fake_confidentiality",
            "document_category": "Aangifte",
            "current_version": 1,
            "origin": "Intern",
            "origin_date": datetime(2019, 10, 10),
            "integrity_check_successful": None,
        }
        FakeDocument = namedtuple(
            "FakeDocument",
            "mimetype size storage_location md5 is_archivable virus_scan_status document_uuid basename extension store_uuid directory_uuid case_uuid case_display_number type created_by date_modified thumbnail creator_uuid creator_displayname accepted document_number description confidentiality document_category current_version origin origin_date integrity_check_successful",
        )

        fake_document = FakeDocument(
            mimetype=fake_data["mimetype"],
            size=fake_data["size"],
            storage_location=fake_data["storage_location"],
            md5=fake_data["md5"],
            is_archivable=fake_data["is_archivable"],
            virus_scan_status=fake_data["virus_scan_status"],
            document_uuid=fake_data["document_uuid"],
            basename=fake_data["basename"],
            extension=fake_data["extension"],
            store_uuid=fake_data["store_uuid"],
            directory_uuid=fake_data["directory_uuid"],
            case_uuid=fake_data["case_uuid"],
            case_display_number=fake_data["case_display_number"],
            accepted=fake_data["accepted"],
            created_by=fake_data["created_by"],
            date_modified=fake_data["date_modified"],
            thumbnail=fake_data["thumbnail"],
            creator_uuid=fake_data["creator_uuid"],
            creator_displayname=fake_data["creator_displayname"],
            type=fake_data["type"],
            document_number=fake_data["document_number"],
            description=fake_data["description"],
            confidentiality=fake_data["confidentiality"],
            document_category=fake_data["document_category"],
            current_version=fake_data["current_version"],
            origin=fake_data["origin"],
            origin_date=fake_data["origin_date"],
            integrity_check_successful=fake_data["integrity_check_successful"],
        )

        serialized_obj = self.views._serialize_document(fake_document)

        assert serialized_obj["id"] == fake_data["document_uuid"]
        assert serialized_obj["type"] == fake_data["type"]
        assert (
            serialized_obj["attributes"]["mimetype"] == fake_data["mimetype"]
        )
        assert serialized_obj["attributes"]["filesize"] == fake_data["size"]

        assert serialized_obj["attributes"]["md5"] == fake_data["md5"]
        assert serialized_obj["attributes"]["archive"] == {
            "is_archivable": fake_data["is_archivable"]
        }
        assert serialized_obj["attributes"]["security"] == {
            "virus_status": fake_data["virus_scan_status"]
        }
        assert (
            serialized_obj["attributes"]["name"]
            == fake_data["basename"] + fake_data["extension"]
        )

        assert (
            serialized_obj["attributes"]["current_version"]
            == fake_data["current_version"]
        )

        assert serialized_obj["relationships"]["directory"] == {
            "data": {"type": "directory", "id": fake_data["directory_uuid"]}
        }
        assert serialized_obj["relationships"]["case"] == {
            "data": {"type": "case", "id": fake_data["case_uuid"]},
            "meta": {"display_number": fake_data["case_display_number"]},
        }

        assert serialized_obj["meta"] == {
            "document_number": 1,
            "last_modified_datetime": "2019-10-10T00:00:00",
        }

    @mock.patch("zsnl_document_http.views.views._serialize_document")
    def test_get_document(self, mock_serialize):
        valid_document_uuid = str(uuid4())

        mock_serialize.return_value = {"uuid": valid_document_uuid}

        request = mock.MagicMock()
        request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.views.get_document(request)

        request.params["document_uuid"] = valid_document_uuid
        result = self.views.get_document(request)
        assert result["data"]["uuid"] == valid_document_uuid

    def test_search_document(self):
        case_uuid = str(uuid4())
        document_uuid = uuid4()
        mock_query = mock.MagicMock()
        creator_uuid = uuid4()
        store_uuid = uuid4()

        FakeDocument = namedtuple(
            "FakeDocument",
            "mimetype size storage_location md5 is_archivable virus_scan_status document_uuid basename extension store_uuid directory_uuid case_uuid case_display_number type created_by date_modified thumbnail creator_uuid creator_displayname accepted document_number description confidentiality document_category current_version origin origin_date integrity_check_successful",
        )(
            mimetype="appilcation",
            size=5,
            storage_location=["Minion"],
            md5=None,
            is_archivable=True,
            virus_scan_status="pending",
            document_uuid=document_uuid,
            basename="test",
            extension=".txt",
            store_uuid=store_uuid,
            directory_uuid=None,
            case_uuid=case_uuid,
            case_display_number=1234,
            type="text",
            created_by="behreeder",
            date_modified=datetime(2019, 10, 10),
            thumbnail=None,
            creator_uuid=creator_uuid,
            creator_displayname="beheerder",
            accepted=True,
            document_number=2,
            description="description",
            confidentiality="Intern",
            document_category="Plan",
            current_version=1,
            origin="Intern",
            origin_date=datetime(2019, 10, 10),
            integrity_check_successful=False,
        )

        request = mock.MagicMock()
        request.get_query_instance.return_value = mock_query
        mock_query.search_document.return_value = [FakeDocument]

        request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.views.search_document(request)

        request.params["case_uuid"] = case_uuid
        result = self.views.search_document(request)
        document = result["data"][0]

        assert document["type"] == "document"
        assert document["id"] == str(document_uuid)

        assert document["attributes"]["security"] == {
            "virus_status": "pending"
        }
        assert document["attributes"]["archive"] == {"is_archivable": True}
        assert document["attributes"]["accepted"] is True
        assert document["attributes"]["integrity_check_successful"] is False

        assert document["relationships"]["created_by"] == {
            "data": {"type": "subject", "id": str(creator_uuid)},
            "meta": {"display_name": "beheerder"},
        }
        assert document["relationships"]["case"] == {
            "data": {"type": "case", "id": case_uuid},
            "meta": {"display_number": 1234},
        }
        assert document["relationships"]["file"] == {
            "data": {"type": "file", "id": str(store_uuid)}
        }
        assert document["links"] == {
            "self": f"/api/v2/document/get_document?document_uuid={document_uuid}"
        }

    def test_old_record_factory(self):
        record = log_record_factory(
            pathname="fake_pathname",
            lineno=1,
            msg="fake_msg",
            args=[],
            exc_info=None,
            name="fake_name",
            level=0,
        )
        assert record.zs_component == "zsnl_document_http"

    def test_get_directory_entries_for_case(self):
        request = mock.MagicMock()
        mock_query = mock.MagicMock()
        request.get_query_instance.return_value = mock_query
        mock_query.get_directory_entries_for_case.return_value = [
            self.mock_directory_entry_document
        ]

        request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_case(request)

        request.params = {
            "case_uuid": self.mock_directory_entry_document.case["uuid"],
            "filter[relationships.parent_directory.id]": self.mock_directory_entry_document.parent[
                "uuid"
            ],
        }
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_case(request)

        del request.params["filter[relationships.parent_directory.id]"]

        request.params["no_empty_folders"] = 1
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_case(request)

        request.params["no_empty_folders"] = "1"
        result = self.views.get_directory_entries_for_case(request)

        directory_entry = result["data"][0]

        assert (
            directory_entry["type"] == self.mock_directory_entry_document.type
        )
        assert directory_entry["id"] == str(
            self.mock_directory_entry_document.uuid
        )

        assert (
            directory_entry["attributes"]["name"]
            == self.mock_directory_entry_document.name
        )
        assert (
            directory_entry["attributes"]["entry_type"]
            == self.mock_directory_entry_document.entry_type
        )

        assert directory_entry["relationships"]["modified_by"] == {
            "data": {
                "type": "subject",
                "id": str(
                    self.mock_directory_entry_document.modified_by["uuid"]
                ),
            },
            "meta": {
                "display_name": self.mock_directory_entry_document.modified_by[
                    "display_name"
                ]
            },
        }

        assert directory_entry["relationships"]["case"] == {
            "data": {
                "type": "case",
                "id": str(self.mock_directory_entry_document.case["uuid"]),
            },
            "meta": {
                "display_number": self.mock_directory_entry_document.case[
                    "display_number"
                ]
            },
        }

        assert directory_entry["relationships"]["document"] == {
            "data": {
                "type": "document",
                "id": str(self.mock_directory_entry_document.document["uuid"]),
            }
        }
        document_uuid = self.mock_directory_entry_document.document["uuid"]
        assert directory_entry["links"]["download"] == {
            "href": f"/api/v2/document/download_document?id={document_uuid}"
        }

    def test__serialize_directory_entry(self):
        res = self.views._serialize_directory_entry(
            entry=self.mock_directory_entry_document
        )

        assert res["type"] == self.mock_directory_entry_document.type
        assert res["id"] == str(self.mock_directory_entry_document.uuid)

        assert (
            res["attributes"]["name"]
            == self.mock_directory_entry_document.name
        )
        assert (
            res["attributes"]["entry_type"]
            == self.mock_directory_entry_document.entry_type
        )
        assert (
            res["attributes"]["description"]
            == self.mock_directory_entry_document.description
        )
        assert (
            res["attributes"]["extension"]
            == self.mock_directory_entry_document.extension
        )
        assert (
            res["attributes"]["mimetype"]
            == self.mock_directory_entry_document.mimetype
        )
        assert (
            res["attributes"]["accepted"]
            is self.mock_directory_entry_document.accepted
        )
        assert (
            res["attributes"]["document_number"]
            == self.mock_directory_entry_document.document_number
        )
        assert (
            res["attributes"]["last_modified_date_time"]
            == self.mock_directory_entry_document.last_modified_date_time.isoformat()
        )

        assert res["relationships"]["modified_by"] == {
            "data": {
                "type": "subject",
                "id": str(
                    self.mock_directory_entry_document.modified_by["uuid"]
                ),
            },
            "meta": {
                "display_name": self.mock_directory_entry_document.modified_by[
                    "display_name"
                ]
            },
        }

        assert res["relationships"]["case"] == {
            "data": {
                "type": "case",
                "id": str(self.mock_directory_entry_document.case["uuid"]),
            },
            "meta": {
                "display_number": self.mock_directory_entry_document.case[
                    "display_number"
                ]
            },
        }

        assert res["relationships"]["document"] == {
            "data": {
                "type": "document",
                "id": str(self.mock_directory_entry_document.document["uuid"]),
            }
        }

        assert res["relationships"]["parent"] == {
            "data": {
                "type": "directory",
                "id": str(self.mock_directory_entry_document.parent["uuid"]),
            },
            "meta": {
                "display_name": self.mock_directory_entry_document.parent[
                    "display_name"
                ]
            },
        }

        document_uuid = str(self.mock_directory_entry_document.uuid)
        assert res["links"]["download"] == {
            "href": f"/api/v2/document/download_document?id={document_uuid}"
        }

        assert res["links"]["preview"] == {
            "href": f"/api/v2/document/preview_document?id={document_uuid}",
            "meta": {"content-type": "application/pdf"},
        }

        assert res["links"]["thumbnail"] == {
            "href": f"/api/v2/document/thumbnail_document?id={document_uuid}",
            "meta": {
                "content-type": self.mock_directory_entry_document.document[
                    "thumbnail_mimetype"
                ]
            },
        }

    def test__serialize_directory_entry_directory(self):
        res = self.views._serialize_directory_entry(
            entry=self.mock_directory_entry_directory
        )

        assert res["type"] == self.mock_directory_entry_directory.type
        assert res["id"] == str(self.mock_directory_entry_directory.uuid)

        assert (
            res["attributes"]["name"]
            == self.mock_directory_entry_directory.name
        )
        assert (
            res["attributes"]["entry_type"]
            == self.mock_directory_entry_directory.entry_type
        )

        assert res["relationships"]["case"] == {
            "data": {
                "type": "case",
                "id": str(self.mock_directory_entry_directory.case["uuid"]),
            },
            "meta": {
                "display_number": self.mock_directory_entry_directory.case[
                    "display_number"
                ]
            },
        }

        assert res["relationships"]["directory"] == {
            "data": {
                "type": "directory",
                "id": str(self.mock_directory_entry_directory.uuid),
            }
        }

        assert res["relationships"]["parent"] == {
            "data": {
                "type": "directory",
                "id": str(self.mock_directory_entry_directory.parent["uuid"]),
            },
            "meta": {
                "display_name": self.mock_directory_entry_directory.parent[
                    "display_name"
                ]
            },
        }

        assert res["links"] == {}

    def test__serialize_directory_entry_for_image(self):
        self.mock_directory_entry_document.mimetype = "image/jpeg"
        self.mock_directory_entry_document.basename = "test_img"
        self.mock_directory_entry_document.extension = ".jpeg"

        res = self.views._serialize_directory_entry(
            entry=self.mock_directory_entry_document
        )

        document_uuid = str(self.mock_directory_entry_document.uuid)

        assert res["type"] == self.mock_directory_entry_document.type
        assert res["id"] == document_uuid

        assert (
            res["attributes"]["name"]
            == self.mock_directory_entry_document.name
        )
        assert (
            res["attributes"]["extension"]
            == self.mock_directory_entry_document.extension
        )
        assert (
            res["attributes"]["mimetype"]
            == self.mock_directory_entry_document.mimetype
        )

        assert res["links"]["download"] == {
            "href": f"/api/v2/document/download_document?id={document_uuid}"
        }
        assert res["links"]["preview"] == {
            "href": f"/api/v2/document/preview_document?id={document_uuid}",
            "meta": {"content-type": "image/jpeg"},
        }

    def test__serialize_directory(self):
        res = self.views._serialize_directory(directory=self.mock_directory)

        assert res["type"] == "directory"
        assert res["id"] == str(self.mock_directory.uuid)

        assert res["attributes"]["name"] == self.mock_directory.name

        assert res["relationships"]["parent"] == {
            "data": {
                "type": "directory",
                "id": self.mock_directory.parent["uuid"],
            }
        }

    def test_get_directory_entries_for_case_with_directory_filter(self):
        request = mock.MagicMock()
        request.params = {}

        mock_query = mock.MagicMock()
        request.get_query_instance.return_value = mock_query

        mock_query.get_parent_directories_for_directory.return_value = [
            self.mock_directory
        ]

        request.params["case_uuid"] = uuid4()
        request.params[
            "filter[relationships.parent.id]"
        ] = self.mock_directory.parent["uuid"]
        result = self.views.get_directory_entries_for_case(request)

        included = result["included"]

        assert included[0]["type"] == "directory"
        assert included[0]["id"] == str(self.mock_directory.uuid)
        assert included[0]["relationships"]["parent"] == {
            "data": {
                "id": self.mock_directory.parent["uuid"],
                "type": "directory",
            }
        }

    @mock.patch("zsnl_document_http.views.views.uuid4")
    def test_create_document_from_file(self, mock_uuid4):
        req = mock.MagicMock()
        req.json_body = {}
        with pytest.raises(HTTPBadRequest):
            self.views.create_document_from_file(req)

        case_uuid = str(uuid4())
        file_uuid = str(uuid4())
        document_uuid = str(uuid4())
        mock_uuid4.return_value = document_uuid

        req.json_body["case_uuid"] = case_uuid
        req.json_body["file_uuid"] = file_uuid

        mock_command_instance = mock.MagicMock()
        req.get_command_instance.return_value = mock_command_instance

        self.views.create_document_from_file(req)
        mock_command_instance.create_document_from_file.assert_called_once_with(
            file_uuid=file_uuid,
            case_uuid=case_uuid,
            document_uuid=document_uuid,
            directory_uuid=None,
            magic_strings=None,
            skip_intake=False,
        )

    def test_download_document(self):
        valid_document_uuid = str(uuid4())

        request = mock.MagicMock()
        request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.views.download_document(request)

        request.params["id"] = valid_document_uuid
        result = self.views.download_document(request)

        assert result.status == "302 Found"

    def test_get_directory_entries_for_intake(self):
        case_uuid = str(uuid4())
        document_uuid = uuid4()
        subject_uuid = uuid4()
        parent_directory_uuid = uuid4()

        FakeDirectoryEntry = namedtuple(
            "FakeDirectoryEntry",
            "uuid type entry_type name description basename extension mimetype accepted document_number last_modified_date_time document directory parent case modified_by rejection_reason rejected_by_display_name assignment",
        )(
            uuid=document_uuid,
            type="directory_entry",
            entry_type="document",
            name="doc1.pdf",
            description="test doc",
            basename="doc1",
            extension="pdf",
            mimetype="application/pdf",
            accepted=True,
            document_number=18,
            last_modified_date_time=datetime(2015, 5, 19),
            document={"uuid": document_uuid, "preview_available": True},
            directory=None,
            case={"uuid": case_uuid, "type": "case", "display_number": 1234},
            parent={"uuid": parent_directory_uuid, "display_name": "folder_1"},
            modified_by={
                "uuid": subject_uuid,
                "type": "subject",
                "display_name": "beheerder",
            },
            rejection_reason=None,
            rejected_by_display_name=None,
            assignment=None,
        )

        request = mock.MagicMock()
        mock_query = mock.MagicMock()
        request.get_query_instance.return_value = mock_query
        mock_query.get_directory_entries_for_intake.return_value = [
            FakeDirectoryEntry
        ]

        request.params = {
            "filter[relationships.parent.id]": "84de92ac-fe8d-46af-b3cd-e34be715f2ab",
            "filter[fulltext]": "doc",
        }
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_intake(request)

        del request.params["filter[relationships.parent.id]"]
        result = self.views.get_directory_entries_for_intake(request)

        directory_entry = result["data"][0]

        assert directory_entry["type"] == "directory_entry"
        assert directory_entry["id"] == str(document_uuid)

        assert directory_entry["attributes"]["name"] == "doc1.pdf"
        assert directory_entry["attributes"]["entry_type"] == "document"
        assert directory_entry["attributes"]["description"] == "test doc"
        assert directory_entry["attributes"]["extension"] == "pdf"
        assert directory_entry["attributes"]["mimetype"] == "application/pdf"
        assert directory_entry["attributes"]["accepted"] is True
        assert directory_entry["attributes"]["document_number"] == 18
        assert (
            directory_entry["attributes"]["last_modified_date_time"]
            == "2015-05-19T00:00:00"
        )

        assert directory_entry["relationships"]["modified_by"] == {
            "data": {"type": "subject", "id": str(subject_uuid)},
            "meta": {"display_name": "beheerder"},
        }

        assert directory_entry["relationships"]["case"] == {
            "data": {"type": "case", "id": str(case_uuid)},
            "meta": {"display_number": 1234},
        }

        assert directory_entry["relationships"]["document"] == {
            "data": {"type": "document", "id": str(document_uuid)}
        }

        assert directory_entry["links"]["download"] == {
            "href": f"/api/v2/document/download_document?id={document_uuid}"
        }

        assert directory_entry["links"]["preview"] == {
            "href": f"/api/v2/document/preview_document?id={document_uuid}",
            "meta": {"content-type": "application/pdf"},
        }

    def test_get_directory_entries_for_intake_has_rejection_reason(self):
        case_uuid = str(uuid4())
        document_uuid = uuid4()
        subject_uuid = uuid4()
        parent_directory_uuid = uuid4()

        FakeDirectoryEntry = namedtuple(
            "FakeDirectoryEntry",
            "uuid type entry_type name description basename extension mimetype accepted document_number last_modified_date_time document directory parent case modified_by rejection_reason rejected_by_display_name assignment",
        )(
            uuid=document_uuid,
            type="directory_entry",
            entry_type="document",
            name="doc1.pdf",
            description="test doc",
            basename="doc1",
            extension="pdf",
            mimetype="application/pdf",
            accepted=True,
            document_number=18,
            last_modified_date_time=datetime(2015, 5, 19),
            document={"uuid": document_uuid, "preview_available": True},
            directory=None,
            case={"uuid": case_uuid, "type": "case", "display_number": 1234},
            parent={"uuid": parent_directory_uuid, "display_name": "folder_1"},
            modified_by={
                "uuid": subject_uuid,
                "type": "subject",
                "display_name": "beheerder",
            },
            rejection_reason="Some valid rejection reason",
            rejected_by_display_name="Ad Min",
            assignment=None,
        )

        request = mock.MagicMock()
        mock_query = mock.MagicMock()
        request.get_query_instance.return_value = mock_query
        mock_query.get_directory_entries_for_intake.return_value = [
            FakeDirectoryEntry
        ]

        request.params = {"filter[fulltext]": "doc"}
        result = self.views.get_directory_entries_for_intake(request)

        directory_entry = result["data"][0]

        assert (
            directory_entry["attributes"]["rejection_reason"]
            == "Some valid rejection reason"
        )

        assert (
            directory_entry["attributes"]["rejected_by_display_name"]
            == "Ad Min"
        )

    def test_get_directory_entries_for_intake_has_invalid_parameters(self):
        case_uuid = str(uuid4())
        document_uuid = uuid4()
        subject_uuid = uuid4()
        parent_directory_uuid = uuid4()

        FakeDirectoryEntry = namedtuple(
            "FakeDirectoryEntry",
            "uuid type entry_type name description basename extension mimetype accepted document_number last_modified_date_time document directory parent case modified_by rejection_reason rejected_by_display_name assignment",
        )(
            uuid=document_uuid,
            type="directory_entry",
            entry_type="document",
            name="doc1.pdf",
            description="test doc",
            basename="doc1",
            extension="pdf",
            mimetype="application/pdf",
            accepted=True,
            document_number=18,
            last_modified_date_time=datetime(2015, 5, 19),
            document={"uuid": document_uuid, "preview_available": True},
            directory=None,
            case={"uuid": case_uuid, "type": "case", "display_number": 1234},
            parent={"uuid": parent_directory_uuid, "display_name": "folder_1"},
            modified_by={
                "uuid": subject_uuid,
                "type": "subject",
                "display_name": "beheerder",
            },
            rejection_reason="Some valid rejection reason",
            rejected_by_display_name="Ad Min",
            assignment=None,
        )

        request = mock.MagicMock()
        mock_query = mock.MagicMock()
        request.get_query_instance.return_value = mock_query
        mock_query.get_directory_entries_for_intake.return_value = [
            FakeDirectoryEntry
        ]

        # test for minimum page value
        request.params = {"filter[fulltext]": "doc", "page": "0"}
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_intake(request)

        # test for minimum page_size value
        request.params = {"filter[fulltext]": "doc", "page_size": "0"}
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_intake(request)

        # test for maximum page_size value
        request.params = {"filter[fulltext]": "doc", "page_size": "10001"}
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_intake(request)

        # test for type of page
        request.params = {"filter[fulltext]": "doc", "page": "a"}
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_intake(request)

        # test for type of page_size
        request.params = {"filter[fulltext]": "doc", "page_size": "b"}
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_intake(request)

        # test for not supported parameter
        request.params = {
            "filter[fulltext]": "doc",
            "the_unkown_parameter": "is_wrong",
        }
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_intake(request)

        # test for missing non-required parameter 'filter[fulltext]'
        request.params = {}
        self.views.get_directory_entries_for_intake(request)

        # test for assigned parameter incorrect
        request.params = {
            "filter[fulltext]": "doc",
            "filter[attributes.assignment.is_set]": "not_a_boolean_value",
        }
        with pytest.raises(HTTPBadRequest):
            self.views.get_directory_entries_for_intake(request)

        # test for assigned parameter true
        request.params = {
            "filter[fulltext]": "doc",
            "filter[attributes.assignment.is_set]": "true",
        }
        self.views.get_directory_entries_for_intake(request)

        # test for assigned parameter true
        request.params = {
            "filter[fulltext]": "doc",
            "filter[attributes.assignment.is_set]": "false",
        }
        self.views.get_directory_entries_for_intake(request)

        # test for assigned to self parameter true
        request.params = {
            "filter[fulltext]": "doc",
            "filter[attributes.assignment.is_self]": "true",
        }
        self.views.get_directory_entries_for_intake(request)

        # test for assigned to self parameter false
        request.params = {
            "filter[fulltext]": "doc",
            "filter[attributes.assignment.is_self]": "false",
        }
        self.views.get_directory_entries_for_intake(request)

    def test_preview_document(self):
        document_uuid = str(uuid4())

        request = mock.MagicMock()
        request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.views.preview_document(request)

        request.params["id"] = document_uuid
        result = self.views.preview_document(request)

        assert result.status == "302 Found"

    def test_create_document_from_message_without_output_format_defaults_to_pdf(
        self,
    ):
        valid_message_uuid = str(uuid4())
        req = mock.MagicMock()
        req.json_body = {"message_uuid": valid_message_uuid}
        mock_command_instance = mock.MagicMock()
        req.get_command_instance.return_value = mock_command_instance

        self.views.create_document_from_message(request=req)
        mock_command_instance.create_document_from_message.assert_called_once_with(
            message_uuid=valid_message_uuid, output_format="pdf"
        )

    def test_create_document_from_message(self):
        mock_request = mock.MagicMock()
        mock_request.json_body = {}
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_document_from_message = mock.MagicMock()

        message_uuid = str(uuid4())

        with pytest.raises(HTTPBadRequest):
            result = self.views.create_document_from_message(
                request=mock_request
            )

        mock_request.json_body = {"message_uuid": message_uuid}

        result = self.views.create_document_from_message(request=mock_request)

        assert result == {"data": {"success": True}}

    def test_delete_document(self):
        mock_request = mock.MagicMock()

        mock_request.json_body = {}
        mock_command_instance = mock.MagicMock()
        mock_command_instance.delete_document = mock.MagicMock()

        document_uuid = str(uuid4())
        reason = "Because I felt like it."

        # delete_document requires document_uuid
        with pytest.raises(HTTPBadRequest) as exception_info:
            self.views.delete_document(request=mock_request)

        assert exception_info.value.json == {
            "error": "Missing required arguments: 'document_uuid'"
        }

        # delete_document accepts document_id, and uses default reason == ""
        mock_request.json_body = {"document_uuid": document_uuid}
        mock_request.get_command_instance.return_value = mock_command_instance

        self.views.delete_document(request=mock_request)

        mock_command_instance.delete_document.assert_called_once_with(
            user_info=user_info, document_uuid=document_uuid, reason=""
        )

        mock_command_instance.delete_document.reset_mock()

        # delete_document accepts document_uuid and reason
        mock_request.json_body = {
            "document_uuid": document_uuid,
            "reason": reason,
        }
        self.views.delete_document(request=mock_request)

        mock_command_instance.delete_document.assert_called_once_with(
            user_info=user_info, document_uuid=document_uuid, reason=reason
        )

    def test_thumbnail_document(self):
        document_uuid = str(uuid4())

        request = mock.MagicMock()
        request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.views.thumbnail_document(request)

        request.params["id"] = document_uuid
        result = self.views.thumbnail_document(request)

        assert result.status == "302 Found"

    def _get_request_from_external_source(self, editor_type: str = "zoho"):
        mock_content = mock.MagicMock()
        mock_content.filename = "test_file.txt"
        mock_content.file = io.BytesIO(b"test_content")
        post_data = {
            "authentication_token": "fake_token",
            "content": mock_content,
            "document_uuid": str(uuid4()),
            "editor_type": editor_type,
            "user_uuid": str(uuid4()),
        }
        req_post = mock.MagicMock()
        req_post.POST.getall.return_value = []
        req_post.POST.__getitem__.side_effect = post_data.__getitem__
        req_post.POST.get.return_value = post_data["editor_type"]
        req_post.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": "swift"}]
        }

        req_post.get_command_instance = mock.MagicMock()

        return req_post

    def test_update_document_from_external_editor(self):
        req = mock.MagicMock()
        req.POST = {}
        with pytest.raises(HTTPBadRequest):
            self.views.update_document_from_external_editor(req)

        req = self._get_request_from_external_source()
        shortname = str(uuid4())

        req.infrastructure_factory.get_config.return_value = {
            "instance_uuid": shortname,
            "filestore": [{"type": "swift"}],
        }
        req.infrastructure_factory = mock.MagicMock()
        redis_infra = req.infrastructure_factory.get_infrastructure(
            "context", infrastructure_name="redis"
        )
        redis_infra.get.return_value = json.dumps(
            {
                "user_uuid": str(uuid4()),
                "case_uuid": str(uuid4()),
                "token": "fake_token",
            }
        ).encode("utf-8")
        self.views.update_document_from_external_editor(req)
        mock_command_instance = mock.MagicMock()
        mock_command_instance.create_document = mock.MagicMock()
        mock_command_instance.create_document().return_value = True

        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": None}],
            "instance_uuid": str(uuid4()),
        }
        self.views.update_document_from_external_editor(req)
        mock_command_instance.create_document.assert_called()
        req.get_command_instance().return_value = mock_command_instance
        req.infrastructure_factory.get_config.return_value = {
            "filestore": [{"type": "s3Legacy"}],
            "instance_uuid": str(uuid4()),
        }
        self.views.update_document_from_external_editor(req)
        mock_command_instance.create_document.assert_called()

    def test_edit_document_online_with_msonline(self):
        valid_document_uuid = str(uuid4())

        request = mock.MagicMock()
        request.configure_mock(
            host="test.zaaksyeem.nl",
            params={
                "editor_type": "msonline",
                "id": valid_document_uuid,
                "close_url": "close_url",
                "host_page_url": "host_page_url",
            },
        )

        query_result_wopi_configuration = mock.MagicMock()
        query_result_wopi_configuration.configure_mock(status=200)
        request.get_query_instance().get_ms_wopi_configuration.return_value = (
            query_result_wopi_configuration
        )

        result = self.views.edit_document_online(request)

        assert result.status == 200

        request.params["close_url"] = ""
        with pytest.raises(HTTPBadRequest) as excinfo:
            self.views.edit_document_online(request)
        assert excinfo.value.json == {
            "errors": [{"title": "Empty 'close_url'"}]
        }

        del request.params["close_url"]
        with pytest.raises(HTTPBadRequest) as excinfo:
            self.views.edit_document_online(request)
        assert excinfo.value.json == {
            "errors": [{"title": "Missing parameter 'close_url'"}]
        }

        request.params["close_url"] = "cloes_url"
        request.params["host_page_url"] = ""
        with pytest.raises(HTTPBadRequest) as excinfo:
            self.views.edit_document_online(request)
        assert excinfo.value.json == {
            "errors": [{"title": "Empty 'host_page_url'"}]
        }

        del request.params["host_page_url"]
        with pytest.raises(HTTPBadRequest) as excinfo:
            self.views.edit_document_online(request)
        assert excinfo.value.json == {
            "errors": [{"title": "Missing parameter 'host_page_url'"}]
        }

    def test_update_document_from_external_editor_msonline(self):
        req = mock.MagicMock()
        req.POST = {}
        with pytest.raises(HTTPBadRequest):
            self.views.update_document_from_external_editor(req)

        req = self._get_request_from_external_source(editor_type="msonline")
        req.get_command_instance().create_document().return_value = True

        document = mock.MagicMock()
        document.configure_mock(document_uuid=str(uuid4()), current_version=3)
        req.get_query_instance().get_document_by_uuid.return_value = document
        req.get_query_instance().get_document_download_link.return_value = (
            "s3 signed url"
        )

        req.infrastructure_factory = mock.MagicMock()

        shortname = str(uuid4())
        req.infrastructure_factory.get_config.return_value = {
            "instance_uuid": shortname,
            "filestore": [{"type": "swift"}],
        }

        redis_infra = req.infrastructure_factory.get_infrastructure(
            "context", infrastructure_name="redis"
        )
        redis_infra.get.return_value = json.dumps(
            {
                "user_uuid": str(uuid4()),
                "case_uuid": str(uuid4()),
                "token": "fake_token",
                "user_permissions": json.dumps({"is_admin": False}),
            }
        ).encode("utf-8")

        document_uuid = req.POST["document_uuid"]
        result = self.views.update_document_from_external_editor(req)
        req.get_command_instance().create_document.assert_called()
        user_uuid = req.POST["user_uuid"]
        redis_infra.get.assert_called_once_with(
            f"msonline:edit_document_online:{shortname}:{document_uuid}:{user_uuid}"
        )
        assert result == {
            "new_file_url": "s3 signed url",
            "new_version": "3",
        }

        # When Redis data is empty.
        redis_infra.get.return_value = None
        with pytest.raises(HTTPForbidden) as excinfo:
            self.views.update_document_from_external_editor(req)
        assert excinfo.value.json == {"error": "Unknown security token"}

        # When authentication_token is not valid.
        redis_infra.get.return_value = json.dumps(
            {
                "user_uuid": str(uuid4()),
                "case_uuid": str(uuid4()),
                "token": "token",
                "user_permissions": json.dumps({"is_admin": False}),
            }
        ).encode("utf-8")
        with pytest.raises(HTTPForbidden) as excinfo:
            self.views.update_document_from_external_editor(req)
        assert excinfo.value.json == {"error": "Failed to update document"}

        # When editor is not supported by Zaaksysteem
        req = self._get_request_from_external_source(
            editor_type="unknown-editor"
        )
        req.infrastructure_factory = mock.MagicMock()

        shortname = str(uuid4())
        req.infrastructure_factory.get_config.return_value = {
            "instance_uuid": shortname,
            "filestore": [{"type": "swift"}],
        }
        with pytest.raises(HTTPForbidden) as excinfo:
            self.views.update_document_from_external_editor(req)
        assert excinfo.value.json == {
            "errors": [{"title": "Editor 'unknown-editor' is not supported."}]
        }


class TestDocumentDirectoryViews:
    def setup_method(self):
        with mock.patch(
            "minty_pyramid.session_manager.protected_route",
            mock_protected_route,
        ):
            from zsnl_document_http.views import views

            importlib.reload(views)
            self.views = views

            self.case_uuid = str(uuid4())
            self.directory_uuid = str(uuid4())
            self.parent_uuid = str(uuid4())
            self.directory_name = "My New Directory"

            self.mock_request = mock.MagicMock()
            self.mock_request.json_body = {}
            self.mock_command_instance = mock.MagicMock()
            self.mock_command_instance.create_directory = mock.MagicMock()

    def test_create_directory_missing_required_parameters(self):
        with pytest.raises(HTTPBadRequest) as exception_info:
            self.views.create_directory(request=self.mock_request)

        assert exception_info.value.json == {
            "error": "Missing required arguments: ['case_uuid', 'name']"
        }

    def test_create_directory_with_only_case_uuid_and_name(self):
        self.mock_request.json_body = {
            "case_uuid": self.case_uuid,
            "name": self.directory_name,
        }
        self.mock_request.get_command_instance.return_value = (
            self.mock_command_instance
        )

        self.views.create_directory(request=self.mock_request)

        self.mock_command_instance.create_directory.assert_called_once_with(
            case_uuid=self.case_uuid,
            name=self.directory_name,
            parent_uuid="",
            directory_uuid=mock.ANY,
        )

    def test_create_directory_with_case_uuid_and_name_and_parent_uuid(self):
        self.mock_request.json_body = {
            "case_uuid": self.case_uuid,
            "parent_uuid": self.parent_uuid,
            "name": self.directory_name,
        }
        self.mock_request.get_command_instance.return_value = (
            self.mock_command_instance
        )

        self.views.create_directory(request=self.mock_request)

        self.mock_command_instance.create_directory.assert_called_once_with(
            case_uuid=self.case_uuid,
            name=self.directory_name,
            parent_uuid=self.parent_uuid,
            directory_uuid=mock.ANY,
        )

    def test_create_directory_with_case_uuid_and_name_and_parent_uuid_and_directory_uuid(
        self,
    ):
        self.mock_request.json_body = {
            "case_uuid": self.case_uuid,
            "parent_uuid": self.parent_uuid,
            "name": self.directory_name,
            "directory_uuid": self.directory_uuid,
        }
        self.mock_request.get_command_instance.return_value = (
            self.mock_command_instance
        )

        self.views.create_directory(request=self.mock_request)

        self.mock_command_instance.create_directory.assert_called_once_with(
            case_uuid=self.case_uuid,
            name=self.directory_name,
            parent_uuid=self.parent_uuid,
            directory_uuid=self.directory_uuid,
        )

    def test_create_directory_with_given_directory_uuid_returns_correct_directory_uuid(
        self,
    ):
        self.mock_request.json_body = {
            "case_uuid": self.case_uuid,
            "name": self.directory_name,
            "directory_uuid": self.directory_uuid,
        }
        self.mock_request.get_command_instance.return_value = (
            self.mock_command_instance
        )

        result = self.views.create_directory(request=self.mock_request)

        assert result == {"data": {"directory_uuid": self.directory_uuid}}

    def test_create_directory_without_directory_uuid_returns_generated_directory_uuid(
        self,
    ):
        self.mock_request.json_body = {
            "case_uuid": self.case_uuid,
            "name": self.directory_name,
        }
        self.mock_request.get_command_instance.return_value = (
            self.mock_command_instance
        )

        result = self.views.create_directory(request=self.mock_request)

        try:
            UUID(result["data"]["directory_uuid"], version=4)
        except ValueError as error:
            raise AssertionError(
                "There is no valid directory_uuid generated!"
            ) from error

    def test_edit_document_online(self):
        valid_document_uuid = str(uuid4())

        request = mock.MagicMock()
        request.params = {}
        with pytest.raises(HTTPBadRequest):
            self.views.edit_document_online(request)

        request.params["id"] = valid_document_uuid
        result = self.views.edit_document_online(request)

        assert result.status == "302 Found"
