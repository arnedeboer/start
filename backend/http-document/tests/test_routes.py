# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

# AUTOGENERATED FILE - No need to edit
# GENERATED AT: 2022-04-07T10:13:31"
# Re-create by running 'generate-routes' command
# Run 'generate-routes --help' for more options


import webtest
from unittest import TestCase, mock
from zsnl_document_http import main, views


class TestRoutes_apidocs(TestCase):
    def setUp(self):
        self.patched_apidocs = mock.patch(
            "zsnl_document_http.views.apidocs", spec=views.apidocs
        )
        self.apidocs = self.patched_apidocs.start()

        settings = {
            "minty_service.infrastructure.config_file": "tests/data/config.conf"
        }
        app = main({}, **settings)
        self.mock_app = webtest.TestApp(app)

    def tearDown(self):
        self.patched_apidocs.stop()

    def test_routes_apidocs(self):
        self.apidocs.apidocs_fileserver.return_value = 200
        self.mock_app.get("/api/v2/document/openapi/{filename}", status=200)


class TestRoutes_views(TestCase):
    def setUp(self):
        self.patched_views = mock.patch(
            "zsnl_document_http.views.views", spec=views.views
        )
        self.views = self.patched_views.start()

        settings = {
            "minty_service.infrastructure.config_file": "tests/data/config.conf"
        }
        app = main({}, **settings)
        self.mock_app = webtest.TestApp(app)

    def tearDown(self):
        self.patched_views.stop()

    def test_routes_views(self):
        self.views.get_document.return_value = 200
        self.mock_app.get("/api/v2/document/get_document", status=200)
        self.views.create_document.return_value = 200
        self.mock_app.post("/api/v2/document/create_document", status=200)
        self.views.create_file.return_value = 200
        self.mock_app.post("/api/v2/file/create_file", status=200)
        self.views.create_document_from_attachment.return_value = 200
        self.mock_app.post(
            "/api/v2/document/create_document_from_attachment", status=200
        )
        self.views.create_document_from_message.return_value = 200
        self.mock_app.post(
            "/api/v2/document/create_document_from_message", status=200
        )
        self.views.search_document.return_value = 200
        self.mock_app.get("/api/v2/document/search_document", status=200)
        self.views.get_directory_entries_for_case.return_value = 200
        self.mock_app.get(
            "/api/v2/document/get_directory_entries_for_case", status=200
        )
        self.views.create_document_from_file.return_value = 200
        self.mock_app.post(
            "/api/v2/document/create_document_from_file", status=200
        )
        self.views.download_document.return_value = 200
        self.mock_app.get("/api/v2/document/download_document", status=200)
        self.views.get_directory_entries_for_intake.return_value = 200
        self.mock_app.get(
            "/api/v2/document/get_directory_entries_for_intake", status=200
        )
        self.views.preview_document.return_value = 200
        self.mock_app.get("/api/v2/document/preview_document", status=200)
        self.views.delete_document.return_value = 200
        self.mock_app.post("/api/v2/document/delete_document", status=200)
        self.views.edit_document_online.return_value = 200
        self.mock_app.get("/api/v2/document/edit_document_online", status=200)
        self.views.update_document_from_external_editor.return_value = 200
        self.mock_app.post(
            "/api/v2/document/update_document_from_external_editor", status=200
        )
        self.views.create_directory.return_value = 200
        self.mock_app.post("/api/v2/document/create_directory", status=200)
        self.views.thumbnail_document.return_value = 200
        self.mock_app.get("/api/v2/document/thumbnail_document", status=200)


class TestRoutes_document_label(TestCase):
    def setUp(self):
        self.patched_document_label = mock.patch(
            "zsnl_document_http.views.document_label",
            spec=views.document_label,
        )
        self.document_label = self.patched_document_label.start()

        settings = {
            "minty_service.infrastructure.config_file": "tests/data/config.conf"
        }
        app = main({}, **settings)
        self.mock_app = webtest.TestApp(app)


class TestRoutes_document(TestCase):
    def setUp(self):
        self.patched_document = mock.patch(
            "zsnl_document_http.views.document", spec=views.document
        )
        self.document = self.patched_document.start()

        settings = {
            "minty_service.infrastructure.config_file": "tests/data/config.conf"
        }
        app = main({}, **settings)
        self.mock_app = webtest.TestApp(app)

    def tearDown(self):
        self.patched_document.stop()
