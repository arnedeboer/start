## Framework
minty~=3.1
minty-pyramid~=3.0

minty_infra_sqlalchemy~=3.2

python-json-logger

jsonpickle

##  Project specific requirements
../domains
../migration_libraries
../pyramid
