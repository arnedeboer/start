# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class CustomObjectViews(JSONAPIEntityView):
    # Move this away in new version, and do this sexyer
    create_link_from_entity = create_link_from_entity

    # Move this to openapi.json
    view_mapper = {
        "GET": {
            "get_custom_object": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_custom_object_by_uuid",
                "from": {"request_params": {"uuid": "uuid"}},
            },
            "get_custom_objects": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_custom_objects",
                "from": {"request_params": {"filter[]": "filter_params"}},
            },
            "get_related_cases": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_related_cases_for_custom_object",
                "from": {"request_params": {"uuid": "object_uuid"}},
            },
            "get_related_objects": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_related_objects_for_custom_object",
                "from": {"request_params": {"uuid": "object_uuid"}},
            },
            "get_related_subjects": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_related_subjects_for_custom_object",
                "from": {"request_params": {"uuid": "object_uuid"}},
            },
            "get_custom_object_event_logs": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_custom_object_event_logs",
                "from": {
                    "request_params": {
                        "period_start": "period_start",
                        "period_end": "period_end",
                        "custom_object_uuid": "custom_object_uuid",
                    }
                },
                "paging": {
                    "max_results": 100,
                    "page": "page",
                    "page_size": "page_size",
                },
            },
            "download_file": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "download_file_for_custom_object",
                "from": {
                    "request_params": {
                        "object_uuid": "object_uuid",
                        "file_uuid": "file_uuid",
                    }
                },
            },
        },
        "POST": {
            "create_custom_object": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "create_custom_object",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "custom_object",
            },
            "update_custom_object": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "update_custom_object",
                "from": {
                    "request_params": {"uuid": "existing_uuid"},
                    "json": {"_all": True},
                },
                "command_primary": "uuid",
                "entity_type": "custom_object",
            },
            "relate_custom_object_to": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "relate_custom_object_to",
                "from": {
                    "request_params": {"uuid": "custom_object_uuid"},
                    "json": {"_all": True},
                },
                "command_primary": "uuid",
                "entity_type": "custom_object",
            },
            "unrelate_custom_object_from": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "unrelate_custom_object_from",
                "from": {
                    "request_params": {"uuid": "custom_object_uuid"},
                    "json": {"_all": True},
                },
                "command_primary": "uuid",
                "entity_type": "custom_object",
            },
            "delete_custom_object": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "delete_custom_object",
                "from": {"json": {"uuid": "custom_object_uuid"}},
                "command_primary": "uuid",
                "entity_type": "custom_object",
            },
            "upgrade_custom_object": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "upgrade_custom_object",
                "from": {
                    "request_params": {
                        "custom_object_uuid": "custom_object_uuid"
                    },
                },
                "command_primary": "cutom_object_uuid",
                "entity_type": "custom_object",
            },
        },
    }
