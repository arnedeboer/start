# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class CaseViews(JSONAPIEntityView):
    create_link_from_entity = create_link_from_entity

    view_mapper = {
        "GET": {
            "get_case_new": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_case_new",
                "from": {"request_params": {"case_uuid": "case_uuid"}},
            },
            "get_case": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_case_by_uuid",
                "from": {"request_params": {"case_uuid": "case_uuid"}},
            },
            "get_case_available_result_types": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_case_available_result_types",
                "from": {"request_params": {"case_uuid": "case_uuid"}},
            },
            "get_case_basic": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_case_basic_by_uuid",
                "from": {"request_params": {"case_uuid": "case_uuid"}},
            },
            "get_case_event_logs": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_case_event_logs",
                "from": {
                    "request_params": {
                        "period_start": "period_start",
                        "period_end": "period_end",
                        "case_uuid": "case_uuid",
                        "filter[]": "filters",
                    }
                },
                "paging": {
                    "max_results": 100,
                    "page": "page",
                    "page_size": "page_size",
                },
            },
            "case_allocation_check": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "case_allocation_check",
                "from": {
                    "request_params": {
                        "case_uuid": "case_uuid",
                        "casetype_uuid": "casetype_uuid",
                        "department_uuid": "department_uuid",
                        "role_uuid": "role_uuid",
                    }
                },
            },
        },
        "POST": {
            "set_target_completion_date": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "set_case_target_completion_date",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "case",
            },
            "set_completion_date": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "set_case_completion_date",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "case",
            },
            "set_registration_date": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "set_case_registration_date",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "case",
            },
            "assign_case_to_self": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "assign_case_to_self",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "case",
            },
            "change_case_coordinator": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "change_case_coordinator",
                "from": {"json": {"_all": True}},
                "command_primary": "case_uuid",
                "entity_type": "case",
            },
            "assign_case_to_user": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "assign_case_to_user",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "case",
            },
            "assign_case_to_department": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "assign_case_to_department",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "case",
            },
            "pause_case": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "pause_case",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "case",
            },
            "resume_case": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "resume_case",
                "from": {"json": {"_all": True}},
                "command_primary": "uuid",
                "entity_type": "case",
            },
            "create_case": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "create_case",
                "from": {"json": {"_all": True}},
                "command_primary": "case_uuid",
                "entity_type": "case",
            },
            "export_case_events_timeline": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "request_case_timeline_export",
                "from": {
                    "request_params": {
                        "period_start": "period_start",
                        "period_end": "period_end",
                        "case_uuid": "case_uuid",
                    },
                },
                "command_primary": "case_uuid",
                "entity_type": "case",
            },
            "set_case_parent": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "set_case_parent",
                "from": {
                    "request_params": {
                        "case_uuid": "case_uuid",
                        "parent_uuid": "parent_uuid",
                    },
                },
                "command_primary": "case_uuid",
                "entity_type": "case",
            },
            "set_case_result": {
                "cq": "command",
                "auth_permissions": {"admin"},
                "domain": "zsnl_domains.case_management",
                "run": "set_case_result",
                "from": {
                    "request_params": {
                        "case_uuid": "case_uuid",
                        "case_type_result_uuid": "case_type_result_uuid",
                    },
                },
                "command_primary": "case_uuid",
                "entity_type": "case",
            },
            "set_destruction_date": {
                "cq": "command",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "set_destruction_date",
                "from": {"json": {"_all": True}},
                "command_primary": "case_uuid",
                "entity_type": "case",
            },
            "search_case": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "search_case",
                "from": {"json": {"_all": True}},
            },
            "search_case_total_results": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "search_case_total_results",
                "from": {"json": {"_all": True}},
            },
        },
    }
