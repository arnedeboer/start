# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ._shared import create_link_from_entity
from minty_pyramid.views.pydantic_entity import JSONAPIEntityView


class ExportFileViews(JSONAPIEntityView):
    create_link_from_entity = create_link_from_entity

    view_mapper = {
        "GET": {
            "get_export_file_list": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "get_export_file_list",
                "from": {"request_params": {}},
                "paging": {
                    "max_results": 100,
                    "page": "page",
                    "page_size": "page_size",
                },
            },
            "download_export_file": {
                "cq": "query",
                "auth_permissions": {"gebruiker"},
                "domain": "zsnl_domains.case_management",
                "run": "download_export_file",
                "from": {"request_params": {"token": "token"}},
            },
        },
    }
