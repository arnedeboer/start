# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from .handlers import Flattener
from minty.cqrs import UserInfo
from minty_pyramid.session_manager import protected_route
from pyramid.httpexceptions import HTTPBadRequest
from uuid import uuid4

log = logging.getLogger(__name__)


@protected_route("gebruiker")
def get_case(request, user_info: UserInfo):
    try:
        case_uuid = request.params["case_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    query_instance = request.get_query_instance(
        "zsnl_domains.case_management", user_info.user_uuid
    )

    case = query_instance.get_case_by_uuid(case_uuid=case_uuid)["result"]
    pickler = Flattener(unpicklable=False)
    case_dict = pickler.flatten(case)

    return {"data": case_dict}


@protected_route("gebruiker")
def set_registration_date(request, user_info: UserInfo):
    """Set registration date on case.

    :param request: request object
    :type request: pyramid.Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPBadRequest: BadRequest(400) error
    :return: response
    :rtype: dict
    """
    try:
        case_uuid = request.json_body["case_uuid"]
        target_date = request.json_body["target_date"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"case_uuid": case_uuid, "target_date": target_date}

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )

    cmd.set_case_registration_date(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def set_target_completion_date(request, user_info: UserInfo):
    """Set target completion date on case.

    :param request: request object
    :type request: pyramid.Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPBadRequest: BadRequest(400) error
    :return: response
    :rtype: dict
    """

    try:
        case_uuid = request.json_body["case_uuid"]
        target_date = request.json_body["target_date"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )

    command_params = {"case_uuid": case_uuid, "target_date": target_date}

    cmd.set_case_target_completion_date(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def set_completion_date(request, user_info: UserInfo):
    """Set completion date on case.

    :param request: request object
    :type request: pyramid.Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPBadRequest: BadRequest(400) error
    :return: response
    :rtype: dict
    """

    try:
        case_uuid = request.json_body["case_uuid"]
        target_date = request.json_body["target_date"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )

    command_params = {"case_uuid": case_uuid, "target_date": target_date}

    cmd.set_case_completion_date(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def assign_case_to_self(request, user_info: UserInfo):
    """Assign case to self.

    :param request: request object
    :type request: pyramid.Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPBadRequest: BadRequest(400) error
    :return: response
    :rtype: dict
    """
    try:
        case_uuid = request.json_body["case_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"case_uuid": case_uuid}

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )

    cmd.assign_case_to_self(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def change_case_coordinator(request, user_info: UserInfo):
    """Assign case to self.

    :param request: request object
    :type request: pyramid.Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPBadRequest: BadRequest(400) error
    :return: response
    :rtype: dict
    """
    try:
        case_uuid = request.json_body["case_uuid"]
        coordinator_uuid = request.json_body["coordinator_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"case_uuid": case_uuid, "subject_uuid": coordinator_uuid}

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )

    cmd.change_case_coordinator(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def pause_case(request, user_info: UserInfo):
    """Pause/postpone a case.

    :param request: request object
    :type request: pyramid.Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPBadRequest: BadRequest(400) error
    :return: response
    :rtype: dict
    """
    try:
        # mandatory fields
        case_uuid = request.json_body["case_uuid"]
        suspension_reason = request.json_body["suspension_reason"]
        suspension_term_type = request.json_body["suspension_term_type"]

    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    suspension_term_value = request.json_body.get("suspension_term_value")
    command_params = {
        "case_uuid": case_uuid,
        "suspension_reason": suspension_reason,
        "suspension_term_type": suspension_term_type,
        "suspension_term_value": suspension_term_value,
    }

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )

    cmd.pause_case(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def resume_case(request, user_info: UserInfo):
    """Pause/postpone a case.

    :param request: request object
    :type request: pyramid.Request
    :param user_info: user information
    :type user_info: UserInfo
    :raises HTTPBadRequest: BadRequest(400) error
    :return: response
    :rtype: dict
    """
    try:
        case_uuid = request.json_body["case_uuid"]
        resume_reason = request.json_body["resume_reason"]
        stalled_since_date = request.json_body["stalled_since_date"]
        stalled_until_date = request.json_body["stalled_until_date"]

    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {
        "case_uuid": case_uuid,
        "resume_reason": resume_reason,
        "stalled_since_date": stalled_since_date,
        "stalled_until_date": stalled_until_date,
    }

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )

    cmd.resume_case(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def create_case(request, user_info: UserInfo):
    required_fields = [
        "case_type_version_uuid",
        "contact_channel",
        "requestor",
    ]
    for field in required_fields:
        if request.json_body.get(field, None) is None:
            raise HTTPBadRequest(
                json={"errors": [{"title": f"Missing parameter '{field}'"}]}
            )

    case_uuid = request.json_body.get("case_uuid") or str(uuid4())

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )
    command_params = {
        "user_info": {
            "uuid": str(user_info.user_uuid),
            "permissions": user_info.permissions,
        },
        "case_uuid": case_uuid,
        "case_type_version_uuid": request.json_body["case_type_version_uuid"],
        "contact_channel": request.json_body["contact_channel"],
        "requestor": request.json_body["requestor"],
        "custom_fields": request.json_body.get("custom_fields"),
        "confidentiality": request.json_body.get("confidentiality"),
        "assignment": request.json_body.get("assignment"),
        "contact_information": request.json_body.get("contact_information"),
        "options": request.json_body.get("options"),
    }

    cmd.create_case(**command_params)

    return {"data": {"success": True, "type": "case", "id": case_uuid}}


@protected_route("gebruiker")
def get_case_type_version(request, user_info: UserInfo):
    try:
        version_uuid = request.params["version_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    query_instance = request.get_query_instance(
        "zsnl_domains.case_management", user_info.user_uuid
    )
    case_type = query_instance.get_case_type_version_by_uuid(
        version_uuid=version_uuid
    )["result"]
    pickler = Flattener(unpicklable=False)
    case_type_dict = pickler.flatten(case_type)

    return {"data": case_type_dict}


@protected_route("gebruiker")
def get_case_type_active_version(request, user_info: UserInfo):
    try:
        case_type_uuid = request.params["case_type_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    query_instance = request.get_query_instance(
        "zsnl_domains.case_management", user_info.user_uuid
    )
    case_type = query_instance.get_case_type_active_version(
        case_type_uuid=case_type_uuid
    )["result"]
    pickler = Flattener(unpicklable=False)
    case_type_dict = pickler.flatten(case_type)

    # We want to be able to fall back to the old API in case of bugs, etc.
    # This is done by making the "eligibility" always be False through
    # configuration.
    enable_eligibility = request.configuration.get(
        "enable_v2_case_eligibility", False
    )

    if not enable_eligibility:
        log.debug(
            "Case type eligibility not enabled in configuration. Forcing to 'False'"
        )
        case_type_dict["meta"]["is_eligible_for_case_creation"] = False

    return {"data": case_type_dict}


@protected_route("admin")
def set_subject_related_custom_object(request, user_info: UserInfo):
    command_instance = request.get_command_instance(
        "zsnl_domains.case_management",
        user_info.user_uuid,
        user_info=user_info,
    )
    command_instance.set_subject_related_custom_object(**request.json_body)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def get_subject(request, user_info: UserInfo):
    if len(request.params) > 1:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {"title": "More than one query parameter is not allowed"}
                ]
            }
        )

    organization_uuid = request.params.get("organization_uuid")
    employee_uuid = request.params.get("employee_uuid")
    person_uuid = request.params.get("person_uuid")

    query_instance = request.get_query_instance(
        "zsnl_domains.case_management", user_info.user_uuid
    )
    pickler = Flattener(unpicklable=False)

    if organization_uuid:
        organization = query_instance.get_organization_by_uuid(
            organization_uuid
        )["result"]
        return {"data": pickler.flatten(organization)}
    elif employee_uuid:
        employee = query_instance.get_employee_by_uuid(employee_uuid)["result"]
        return {"data": pickler.flatten(employee)}
    elif person_uuid:
        person = query_instance.get_person_by_uuid(person_uuid)["result"]
        return {"data": pickler.flatten(person)}
    else:
        raise HTTPBadRequest(
            json={
                "errors": [
                    {
                        "title": "Missing parameter 'organization_uuid' or 'employee_uuid' or 'person_uuid' "
                    }
                ]
            }
        )


@protected_route("gebruiker")
def get_subject_relations(request, user_info: UserInfo):
    try:
        case_uuid = request.params["case_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    include = request.params.get("include")
    if include:
        include = set(include.split(","))
    else:
        include = set()

    if not include <= {"subject"}:
        raise HTTPBadRequest(
            json={
                "errors": [{"title": "Unsupported 'include' parameter value"}]
            }
        )

    query_instance = request.get_query_instance(
        "zsnl_domains.case_management",
        user_info.user_uuid,
        user_info=user_info,
    )
    related_subjects = query_instance.get_subject_relations_for_case(
        case_uuid
    )["result"]
    pickler = Flattener(unpicklable=False)
    links = {"self": request.current_route_path()}
    related_subjects_list = [
        pickler.flatten(subject) for subject in related_subjects
    ]

    result = {"links": links, "data": related_subjects_list}

    for relation in include:
        if "included" not in result:
            result["included"] = []

        if relation == "subject":
            included_subjects = _get_included_subjects(
                query_instance, related_subjects
            )
            result["included"].extend(
                pickler.flatten(subject) for subject in included_subjects
            )

    if "included" in result:
        result["included"] = result["included"]

    return result


def _get_included_subjects(query, related_subjects):
    subjects = {}
    for related_subject in related_subjects:
        st = related_subject.subject.type
        si = related_subject.subject.id

        if st not in subjects:
            subjects[st] = set()
        subjects[st].add(str(si))

    response = []
    if "person" in subjects:
        response.extend(
            query.get_persons_by_uuid(person_uuids=subjects["person"])
        )
    if "organization" in subjects:
        response.extend(
            query.get_organizations_by_uuid(
                organization_uuids=subjects["organization"]
            )
        )
    if "employee" in subjects:
        response.extend(
            query.get_employees_by_uuid(employee_uuids=subjects["employee"])
        )

    return response


@protected_route("gebruiker")
def create_subject_relation(request, user_info: UserInfo):
    required_fields = ["case_uuid", "subject", "magic_string_prefix", "role"]

    for field in required_fields:
        if request.json_body.get(field, None) is None:
            raise HTTPBadRequest(
                json={"errors": [{"title": f"Missing parameter '{field}'"}]}
            )

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    command_params = {
        "case_uuid": request.json_body.get("case_uuid"),
        "subject": request.json_body["subject"],
        "role": request.json_body["role"],
        "magic_string_prefix": request.json_body["magic_string_prefix"],
        "authorized": request.json_body.get("authorized", False),
        "send_confirmation_email": request.json_body.get(
            "send_confirmation_email", False
        ),
        "permission": request.json_body.get("permission"),
    }

    cmd.create_subject_relation(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def create_case_relation(request, user_info: UserInfo):
    try:
        uuid1 = request.json_body["uuid1"]
        uuid2 = request.json_body["uuid2"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"uuid1": uuid1, "uuid2": uuid2}

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    cmd.create_case_relation(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def get_case_relations(request, user_info: UserInfo):
    try:
        case_uuid = request.params["case_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    authorization = request.params.get("authorization", None)

    include = request.params.get("include")
    if include:
        include = set(include.split(","))
    else:
        include = set()

    if not include <= {"this_case", "other_case"}:
        raise HTTPBadRequest(
            json={
                "errors": [{"title": "Unsupported 'include' parameter value"}]
            }
        )

    query_instance = request.get_query_instance(
        "zsnl_domains.case_management",
        user_info.user_uuid,
        user_info=user_info,
    )

    params = {"case_uuid": case_uuid}
    if authorization:
        params["authorization"] = authorization

    case_relations = query_instance.get_case_relations(**params)["result"]
    pickler = Flattener(unpicklable=False)
    links = {"self": request.current_route_path()}
    case_relations_list = [
        pickler.flatten(case_relation) for case_relation in case_relations
    ]

    included = _get_included_cases(case_relations, include, query_instance)

    return {"links": links, "data": case_relations_list, "included": included}


def _get_included_cases(case_relations, include, query):
    case_uuids = []
    pickler = Flattener(unpicklable=False)

    for case_relation in case_relations:
        if case_relation.object1_uuid == case_relation.current_case_uuid:
            this_case_uuid, other_case_uuid = (
                case_relation.object1_uuid,
                case_relation.object2_uuid,
            )
        else:
            this_case_uuid, other_case_uuid = (
                case_relation.object2_uuid,
                case_relation.object1_uuid,
            )

        for relation in include:
            if relation == "this_case" and this_case_uuid not in case_uuids:
                case_uuids.append(str(this_case_uuid))
            elif (
                relation == "other_case" and other_case_uuid not in case_uuids
            ):
                case_uuids.append(str(other_case_uuid))

    case_summaries = query.get_case_summaries_by_uuid(case_uuids=case_uuids)[
        "result"
    ]

    return [pickler.flatten(case_summary) for case_summary in case_summaries]


@protected_route("gebruiker")
def update_subject_relation(request, user_info: UserInfo):
    required_fields = ["relation_uuid", "magic_string_prefix", "role"]
    for field in required_fields:
        if request.json_body.get(field, None) is None:
            raise HTTPBadRequest(
                json={"errors": [{"title": f"Missing parameter '{field}'"}]}
            )
    command_params = {
        "relation_uuid": request.json_body["relation_uuid"],
        "role": request.json_body["role"],
        "magic_string_prefix": request.json_body["magic_string_prefix"],
        "authorized": request.json_body.get("authorized"),
        "permission": request.json_body.get("permission"),
    }
    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    cmd.update_subject_relation(**command_params)
    return {"data": {"success": True}}


@protected_route("gebruiker")
def delete_subject_relation(request, user_info: UserInfo):
    required_fields = ["relation_uuid"]
    for field in required_fields:
        if request.json_body.get(field, None) is None:
            raise HTTPBadRequest(
                json={"errors": [{"title": f"Missing parameter '{field}'"}]}
            )
    command_params = {"relation_uuid": request.json_body["relation_uuid"]}
    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    cmd.delete_subject_relation(**command_params)
    return {"data": {"success": True}}


@protected_route("gebruiker")
def reorder_case_relation(request, user_info: UserInfo):
    try:
        relation_uuid = request.json_body["relation_uuid"]
        case_uuid = request.json_body["case_uuid"]
        new_index = request.json_body["new_index"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )
    cmd.reorder_case_relation(
        relation_uuid=relation_uuid, case_uuid=case_uuid, new_index=new_index
    )

    return {"data": {"success": True}}


@protected_route("gebruiker")
def delete_case_relation(request, user_info: UserInfo):
    try:
        relation_uuid = request.json_body["relation_uuid"]
        case_uuid = request.json_body["case_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    command_params = {"relation_uuid": relation_uuid, "case_uuid": case_uuid}
    cmd.delete_case_relation(**command_params)
    return {"data": {"success": True}}


@protected_route("gebruiker")
def assign_case_to_user(request, user_info: UserInfo):
    try:
        case_uuid = request.POST.get("case_uuid")
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error
    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )
    user_uuid = request.POST.get("user_uuid", None)

    command_params = {"case_uuid": case_uuid, "user_uuid": user_uuid}
    cmd.assign_case_to_user(**command_params)
    return {"data": {"success": True}}


@protected_route("gebruiker")
def assign_case_to_department(request, user_info: UserInfo):
    try:
        case_uuid = request.POST.get("case_uuid")
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error
    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management", user_uuid=user_info.user_uuid
    )

    department_uuid = request.POST.get("department_uuid", None)
    role_uuid = request.POST.get("role_uuid", None)
    command_params = {
        "case_uuid": case_uuid,
        "department_uuid": department_uuid,
        "role_uuid": role_uuid,
    }

    cmd.assign_case_to_department(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def recover_case_relations(request, user_info: UserInfo):
    try:
        case_uuid = request.params["case_uuid"]
    except KeyError as error:
        raise HTTPBadRequest(
            json={"errors": [{"title": f"Missing parameter '{error}'"}]}
        ) from error

    command_params = {"case_uuid": case_uuid}

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    cmd.recover_case_relations(**command_params)

    return {"data": {"success": True}}


@protected_route("gebruiker")
def sync_geo_values(request, user_info: UserInfo):
    custom_object_uuid = request.params.get("custom_object_uuid", None)
    case_uuid = request.params.get("case_uuid", None)
    contact_uuid = request.params.get("contact_uuid", None)
    type = None
    if contact_uuid:
        try:
            type = request.params["type"]
        except KeyError as error:
            raise HTTPBadRequest(
                json={"errors": [{"title": f"Missing parameter '{error}'"}]}
            ) from error

    command_params = {
        "custom_object_uuid": custom_object_uuid,
        "case_uuid": case_uuid,
        "contact_uuid": contact_uuid,
        "type": type,
    }

    cmd = request.get_command_instance(
        domain="zsnl_domains.case_management",
        user_uuid=user_info.user_uuid,
        user_info=user_info,
    )

    cmd.sync_geo_values(**command_params)

    return {"data": {"success": True}}
