minty~=3.0,>=3.0.4
minty-pyramid~=3.0

minty_infra_amqp~=2.0
minty_infra_sqlalchemy~=3.2

python-json-logger
requests~=2.24
setuptools~=65.6

# project dependencies 
../domains
../pyramid