# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

__version__ = "0.0.1"

import logging

import minty_pyramid

from .domain import hello
from .routes import routes

old_factory = logging.getLogRecordFactory()


def log_record_factory(*args, **kwargs):
    record = old_factory(*args, **kwargs)
    record.zs_component = "zsnl_style_http"
    return record


logging.setLogRecordFactory(log_record_factory)


def main(*args, **kwargs):
    loader = minty_pyramid.Engine([hello])
    config = loader.setup(*args, **kwargs)

    routes.add_routes(config)
    return loader.main()
