package Syzygy::Test::Object::Reference;

use Test::Class::Moose;

use Syzygy::Object::Reference;
use Syzygy::Object::Type;
use BTTW::Tools::RandomData qw(generate_uuid_v4);

sub test_object_reference {
    my $type = Syzygy::Object::Type->new(name => 'foo');
    my $id = generate_uuid_v4;

    subtest 'object reference constructor' => sub {
        my $ref = Syzygy::Object::Reference->new(
            id => $id,
            type => $type
        );

        isa_ok $ref, 'Syzygy::Object::Reference', 
            '$ref->new retval';

        is $ref->id, $id, 'sanity check on id';
        is $ref->type_name, $type->name, 'sanity check on type';
    };

    subtest 'object reference default preview' => sub {
        my $ref = Syzygy::Object::Reference->new(
            id => $id,
            type => $type
        );

        ok $ref->preview =~ m[^foo\(\.\.\.(?<id>\w+)\)$],
            'preview string';

        ok $id =~ m[\Q$+{id}\E$], 'preview string id part';
    };

    subtest 'object reference explicit preview' => sub {
        my $ref = Syzygy::Object::Reference->new(
            id => $id,
            preview => 'abc',
            type => $type
        );

        is $ref->preview, 'abc', 'string matches';

        my $ref2;

        lives_ok {
            $ref2 = Syzygy::Object::Reference->new(
                id => $id,
                preview => undef,
                type => $type
            );
        } 'explicit undef preview param lives';


        ok $ref2->preview =~ m[^foo\(\.\.\.(?<id>\w+)\)$],
            'preview string';

        ok $id =~ m[\Q$+{id}\E$], 'preview string id part';
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
