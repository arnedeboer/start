#!/usr/bin/env perl

use strict;
use warnings;

use Test::Class::Moose::Load 't/lib';
use Test::Class::Moose::Runner;

use Log::Log4perl ':easy';

#Log::Log4perl::easy_init($TRACE);

my $runner = Test::Class::Moose::Runner->new(
    test_classes => \@ARGV
);

$runner->runtests;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
