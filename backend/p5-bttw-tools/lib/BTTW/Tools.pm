package BTTW::Tools;

use warnings;
use strict;

use autodie;
use v5.14;

use Data::Dump qw(dumpf);
use Data::Dumper;
use Devel::StackTrace;
use Scalar::Util qw[blessed];
use Exporter ();
use DateTime::Format::Strptime;
use DateTime::Format::DateParse;
use Carp qw(longmess);

use Moose::Util::TypeConstraints qw[];

require Locale::Maketext::Simple;

use BTTW::Exception;
use BTTW::Profile;
use BTTW::Types;

=head1 NAME

BTTW::Tools - Basic utility functions and imports for Zaaksysteem projects

=head1 SYNOPSIS

    package SomePackage::SomePart;
    use BTTW::Tools;

    # We now have: warnings, strict, feature(:5.14), autodie
    # and everything from BTTW::Exception and BTTW::Profile

    define_profile foo => ( etc. );

    sub foo {
        throw("etc", "etc.");
    }

=cut

our $VERSION = '0.014';

our @EXPORT = (
    @BTTW::Exception::EXPORT,
    @BTTW::Profile::EXPORT,

    qw(
        burp
        barf

        blessed
        assert_date

        sig

        dump_terse
        elfproef
        tombstone
        date_filter
    ),
);

sub import {
    warnings->import();
    strict->import();
    feature->import(':5.14');
    autodie->import(':all');

    BTTW::Exception->import();
    BTTW::Profile->import();
    BTTW::Types->import(':all');

    goto &Exporter::import;
}

=head1 EXPORTED FUNCTIONS

=head2 sig

This procedure adds the ability to do runtime checks on the types of arguments
and returnvalues of Moose method calls.

    package My::Package;

    use Moose;

    # This signature defines a method that takes at least 2 arguments, both
    # string, a 0+ element array of strings and returns a stringish value.
    sig test => 'Num, Num, @Str => Str';

    sub test { ... }

    # This signature only enforces the first argument to be string. Staying in
    # line with perl's lenient nature, additional arguments are allowed.
    sig simple => 'Str';

    sub simple { ... }

    # Only enforce the returnvalue to be stringish.
    sig retcheck => '=> Str';

    sub retcheck { return 'abc' }

Note that due to the runtime nature of checks that enforce the return value
type may be undesired, as the method is still executed, and may have had
side-effects (db, global state, static class stuff).

=cut

sub sig ($$) {
    my $method = shift;
    my $signature = shift;
    my $package = caller;

    die 'needs moose' unless $package->can('meta');

    my ($in, $out) = split m[\s*=>\s*], $signature;

    return unless $in || $out;

    my @ins = $in ? check_chain(split(m[\s*,\s*], $in)) : ();
    my @outs = $out ? check_chain(split(m[\s*,\s*], $out)) : ();

    $package->meta->add_around_method_modifier($method, sub {
        my $orig = shift;
        my $self = shift;

        my @args = @_;

        try {
            map { $_->(\@args) } @ins;
        } catch {
            throw(sprintf('sig/%s/check_arguments', $method), sprintf(
                'Argument signature check for "%s" failed: %s',
                $method,
                $_
            ));
        };

        my @rets = $self->$orig(@_);
        my @temp = @rets;

        try {
            map { $_->(\@temp) } @outs;
        } catch {
            throw(sprintf('sig/%s/check_return_value', $method), sprintf(
                'Return value signature check for "%s" failed: %s',
                $method,
                $_
            ));
        };

        unless (defined wantarray) {
            warn sprintf(
                "Explicitly defined return value ignored by caller %s:%d",
                (caller 2)[1, 2]
            ) if scalar @outs;
        }

        return wantarray ? @rets : $rets[0];
    });
}

=head2 check_chain

This function supports L</sig>. It consumes a string with a type-signature
definition and produces a list of coderefs that validate the provided values.

=cut

sub check_chain {
    my @chain;

    for my $type (@_) {
        my ($prefix, $role, $name) = $type =~ m/^([\@\%\?]?)(:)?(.*)/;

        my $constraint = $role
            ? Moose::Util::TypeConstraints::create_role_type_constraint($name)
            : Moose::Util::TypeConstraints::find_or_create_isa_type_constraint($name);

        unless ($prefix) {
            push @chain, sub {
                $constraint->assert_valid(shift(@{ $_[0] }))
            };
        }

        if ($prefix eq '?') {
            push @chain, sub {
                if ($constraint->check($_[0][0])) {
                    shift(@{ $_[0] });
                }
            };
        }

        if ($prefix eq '@') {
            push @chain, sub {
                map { $constraint->assert_valid($_) } @{ $_[0] };
            };

            last;
        }

        if ($prefix eq '%') {
            push @chain, sub {
                my %hash = @{ $_[0] };

                map { $constraint->assert_valid($hash{ $_ }) } keys %hash;
            };

            last;
        }
    }

    return @chain;
}

=head2 burp

Print a warning.

This is smart about its arguments: if there is only one argument, and it's a
reference, L<Data::Dump::dump> is called on it. In other cases, it's passed to
sprintf().

This function is exported by default.

=cut

sub burp {
    my $msg = _generate_message(@_);

    return _log($msg);
}

=head2 barf

Print a warning, like L<burp>, but include a stack trace.

This function is exported by default.

=cut

sub barf {
    my $msg = _generate_message(@_);
    my $stack = Devel::StackTrace->new();

    return _log($msg, $stack->as_string());
}

=head2 set_logger

Set the logger object that C<barf> and C<burp> use. This object should provide
a C<debug> and an C<warn> method.

By default, or when the logger is set to C<undef>, the built-in C<warn>
function will be used.

=cut

my $logger;

sub set_logger {
    $logger = shift;
    return;
}

=head2 tombstone

    tombstone('20151102', '<yourname>');

This method is used to mark I<possible> dead code. It will then log a message
to a file so you can see whether or not the code is actually dead. If you
don't see the message, the function is not called. If you do, the function
is called and therefore not dead.

For more information see
L<this blogpost at nestoria|http://devblog.nestoria.com/post/115930183873/tombstones-for-dead-code>.

=cut

sub tombstone {
    my ($date, $author) = @_;

    my $msg = longmess("$author issued tombstone on $date") ;

    (defined $logger)
        ? $logger->warn($msg)
        : warn(scalar(localtime), " ", $msg);
}

=head1 INTERNAL FUNCTIONS

=head2 _log

Internal function that doest the actual logging. If called in void context,
the built-in C<warn> is used if no logger is set, and C<< $logger->debug() >>
if it is.

In other contexts (scalar, list), the value to be logged is returned instead.

=cut

sub _log {
    return join($/, @_)
        if (defined wantarray);

    (defined $logger)
        ? $logger->debug(join($/, @_))
        : warn(scalar(localtime), " ", join($/, @_), $/);
}

=head2 _generate_message

Generate the "real" error message. If one argument is passed, and it's a
reference, L<Data::Dumper::dump()> will be called on it.

In all other cases, the argument(s) will be passed to C<sprintf>.

=cut

sub _generate_message {
    my $msg;

    if (@_ == 1 && ref($_[0])) {
        $msg = dumpf($_[0], \&_dump_filter);
    }
    else {
        # sprintf()'s prototype expects a scalar as the first argument, so we
        # have to split it out manually first.
        $msg = sprintf(shift, @_);
    }

    return $msg . $/;
}

=head2 _dump_filter

Filter for L<Data::Dumper::dumpf> that collapses known huge data structures
down to manageable size.

The following classes are made a bit easier on the eyes:

=over

=item * L<DBIx::Class::Schema>

=back

=cut

sub _dump_filter {
    my ($ctx, $object_ref) = @_;

    if ($ctx->is_blessed && $object_ref->isa('DBIx::Class::Schema')) {
        my $storage = $object_ref->storage;
        return {
            dump => sprintf(
                "<%s database handle connected to %s>",
                $ctx->class,
                $storage
                    ? $storage->connect_info->[0]{dsn}
                    : 'nothing'
            ),
        };
    }

    return;
}

=head2 assert_date

Asserts a date sting, and returns a datetime object, otherwise dies.

=cut

sub assert_date {
    my $date = shift;
    throw('assert_date', "Need a datestring") unless defined $date;

    # If this function is called several times:
    # assert_date(assert_date(foo))
    # don't b0rk
    if (blessed($date) && $date->isa('DateTime')) {
        return $date;
    }

    my $dtfs = DateTime::Format::Strptime->new(pattern => '%d-%m-%Y', on_error => 'croak');
    my $dt = try {
        $dtfs->parse_datetime($date);
    }
    catch {
      my $dtfs = DateTime::Format::Strptime->new(pattern => '%Y-%m-%d', on_error => 'croak');
      return try {
        $dtfs->parse_datetime($date);
      }
      catch {
        throw('assert_date', 'Incorrect datumformaat');
      };
    };
    return $dt;
}

=head2 date_filter

    print date_filter('2014-04-05');
    print date_filter(DateTime->now(year => 2014, day => 4, month => 5));

    # Both print: 20140405

Will transform a L<DateTime> object or another form into proper date

=cut

sub date_filter {
    my $field = shift;

    if (!defined $field) {
        return undef;
    }

    my $dt;
    if (blessed($field) && $field->isa('DateTime')) {
        $dt = $field;
    } elsif ($field =~ /^\d{8}$/) {
        my ($year, $month, $day) = $field =~ /^([0-9]{4})([0-9]{2})([0-9]{2})$/;

        $month  = 1 if $month    < 1;
        $day    = 1 if $day      < 1;

        eval {
            $dt = DateTime->new(
                year => $year,
                month => $month,
                day => $day,
            );
        };
    } elsif ($field =~ /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}(?:.*Z)?$/) {
        eval {
            $dt = DateTime::Format::DateParse->parse_datetime($field);
        };
    }

    if ($dt) {
        return $dt->strftime('%Y%m%d');
    }

    return undef;
}

=head2 dump_terse

Sometimes you don't want a log as big as the map of Holland, you want it one a single line.

=cut

sub dump_terse {
    my $objects = shift;
    local $Data::Dumper::Terse      = 1;
    local $Data::Dumper::Indent     = 0;
    local $Data::Dumper::Varname    = "";
    local $Data::Dumper::Sparseseen = 0;
    local $Data::Dumper::Quotekeys  = 0;
    local $Data::Dumper::Maxdepth   = shift // 3;
    return Dumper $objects;
}

=head2 elfproef

Check if the value passes the elf-proef.
If you supply an extra boolean, you will test if a value
passes the BSN elf-proef.

=cut

sub elfproef {
    my $p = shift;
    my $bsn = shift;

    my $l = length($p);
    if ($l > 6 && $l < 10) {
        $p = sprintf("%09d", $p);
    }
    else {
        throw('elfproef', "'$p' isn't 9 characters long");
    }

    my $sum = 0;
    foreach (reverse(-9 .. -1)) {
        if ($bsn && $_ == -1) {
            $sum += (substr($p, $_, 1) * $_);
        }
        else {
            $sum += (substr($p, $_, 1) * abs($_));
        }
    }

    return $sum % 11 ? 0 : 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

BTTW::Tools uses the EUPL license, for more information please have a look at the L<LICENSE> file.
