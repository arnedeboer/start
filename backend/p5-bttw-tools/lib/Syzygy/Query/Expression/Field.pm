package Syzygy::Query::Expression::Field;
our $VERSION = '0.006';
use Moose;

use BTTW::Tools;

with 'Syzygy::Query::Expression';

=head1 NAME

Syzygy::Query::Expression::Field - Object attribute reference
expression

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 name

Name of the field instances of this expression class reference.

=cut

has name => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Syzygy::Query::Expression/stringify>.

    # "field:my_field"
    qb_field('my_field')->stringify

=cut

sub stringify {
    my $self = shift;

    return sprintf('field:%s', $self->name);
}

=head2 as_complex_value

=cut

sub as_complex_value {
    my $self = shift;

    return {
        field => {
            name => $self->name
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
