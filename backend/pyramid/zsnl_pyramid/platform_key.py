# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from collections import defaultdict
from minty.exceptions import Conflict, Forbidden, NotFound
from sqlalchemy.sql import and_, join, select
from zsnl_domains.case_management.entities._shared import is_valid_bsn
from zsnl_domains.database import schema

subject_user_entity_join = join(
    schema.Subject,
    schema.UserEntity,
    schema.UserEntity.subject_id == schema.Subject.id,
).join(
    schema.Interface,
    schema.Interface.id == schema.UserEntity.source_interface_id,
)
admin_user_query = (
    select(schema.Subject.uuid)
    .where(
        and_(
            schema.Subject.username == "admin",
            schema.Interface.module == "authldap",
            schema.UserEntity.date_deleted.is_(None),
        )
    )
    .select_from(subject_user_entity_join)
)

subject_person_query = select(schema.NatuurlijkPersoon.uuid).where(
    schema.NatuurlijkPersoon.deleted_on.is_(None)
)

organization_query = select(schema.Bedrijf.uuid).where(
    schema.Bedrijf.deleted_on.is_(None)
)


# This class is added because when using a default dict with a lambda to
# provide a default value if no key is present, the get method will not
# always return the default value, but None instead. A small example below:
#
# permissions.get("admin")  # None
# permissions["admin"]      # True
# permissions.get("admin")  # True
class default_true_dict(defaultdict):
    def __init__(self, **kwargs):
        super().__init__(lambda: True, **kwargs)

    def get(self, __key, __default=None):
        # ignore _default here, use the callable default form the constructor
        return self[__key]


def _get_platform_user_uuid(request):
    """Retrieve the UUID of the "admin" user of the system.

    All "platform key" access will be done in this user's name.

    :raises NotFound: Raised when the platform user cannot be found
    :raises Conflict: Raised when multiple platform users are somehow found
    """
    db = request.get_infrastructure_ro("database")
    user = request.headers.get("zs-user-override")

    # natural person
    if user and user.startswith("person:"):
        user_bsn = user.replace("person:", "")

        if is_valid_bsn(user_bsn):
            subjects = (
                db.execute(
                    subject_person_query.where(
                        schema.NatuurlijkPersoon.burgerservicenummer
                        == user_bsn
                    )
                )
            ).fetchall()
            permissions = {"pip_user": True}
        else:
            raise Conflict(
                "Given BSN format is wrong",
                "platform_key/wrong_bsn_format",
            )
    # organization
    elif user and user.startswith("organization:"):
        user = user.replace("organization:", "")

        if user.startswith("rsin"):
            user = user.replace("rsin:", "")
            query = organization_query.where(schema.Bedrijf.rsin == user)
        elif user.startswith("vestigingsnummer"):
            user = user.replace("vestigingsnummer:", "")
            query = organization_query.where(
                schema.Bedrijf.vestigingsnummer == user
            )
        else:
            raise Conflict(
                "Wrong header data",
                "platform_key/wrong_header_data",
            )
        subjects = db.execute(query).fetchall()
        permissions = {"pip_user": True}

    else:
        subjects = db.execute(admin_user_query).fetchall()
        permissions: dict[str, bool] = default_true_dict(pip_user=False)

    # Because we're outside of the CQRS cycle, cleanup isn't guaranteed to be
    # run. So we call close() ourselves.
    db.close()

    if len(subjects) == 0:
        raise NotFound("No user found", "platform_key/no_user_found")
    elif len(subjects) > 1:
        raise Conflict("Multiple users found", "platform_key/multiple_users")

    return str(subjects[0].uuid), permissions


def _assert_platform_key(request):
    """Add platform key assertion to the request

    :param request: Pyramid request to check for a valid platform key
    :raises Forbidden: Raised when the platform key doesn't match
    :raises KeyError: When no platform key is configured
    :return: Nothing
    :rtype: None
    """
    configured_platform_key = request.configuration["zs_platform_key"]
    request_platform_key = request.headers["zs-platform-key"]

    if request_platform_key == configured_platform_key:
        return

    raise Forbidden(
        "The supplied platform key does not match the configured one",
        "platform_key/wrong",
    )


def includeme(config):
    """Function used by Pyramid's `include` system to the platform-user configuration.

    :param config: Pyramid configuration object to extend
    :type config: Configurator
    :return: Nothing
    :rtype: None
    """

    config.add_request_method(
        _get_platform_user_uuid, "get_platform_user_uuid"
    )
    config.add_request_method(_assert_platform_key, "assert_platform_key")
