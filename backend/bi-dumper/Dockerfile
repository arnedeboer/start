FROM python:3.11-slim-bullseye AS production

ARG YOUR_ENV="production"
ARG UNAME=dumper
ARG UID=1000
ARG GID=1000

RUN groupadd --gid $GID $UNAME \
  && useradd --create-home --home-dir /code \
  --uid $UID --gid $GID \
  --shell /bin/bash $UNAME

WORKDIR /code

ENV YOUR_ENV=${YOUR_ENV} \
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100

# System deps:
RUN apt-get update && \
  apt-get install -y curl

# Install Poetry
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python && \
  cd /usr/local/bin && \
  ln -s /opt/poetry/bin/poetry && \
  poetry config virtualenvs.create false

# Copy only requirements to cache them in docker layer
COPY poetry.lock pyproject.toml /code/

# Project initialization:
RUN poetry install $(test "$YOUR_ENV" == production && echo "--without dev") --no-interaction --no-ansi  --no-root

# Creating folders, and files for a project:
COPY . /code

USER $UID

ENTRYPOINT /code/startup.sh
