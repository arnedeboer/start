#!/bin/bash

set -e

runat="${1:-00:00}"
#verbose="$2:-0"

log () {
#    if [ "$verbose" -eq 1 ]; then
        >&2 echo "$1"
#    fi
}

log "Will run at $runat"

while true ; do
    DATE=$( date +%H:%M )

    if [ "$DATE" == "$runat" ]; then
        log "Running dump"
        python3 /code/dumper.py

        # Ensure the minute is over.. so we don't trigger twice in a row
        sleep 60
    else
        log "Not yet time"
    fi

    log "Sleeping for a bit"
    sleep 15s
done
