import boto3.session
import os
import psycopg2
import redis
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

REDIS_URL = os.environ["REDIS_URL"]
S3_BUCKET = os.environ["S3_BUCKET"]


session = boto3.session.Session()
if s3_endpoint := os.environ.get("S3_ENDPOINT"):
    s3_client = session.client("s3", endpoint_url=s3_endpoint)
else:
    s3_client = session.client("s3")


def create_tables(cursor):
    cursor.execute("CREATE TABLE config(parameter VARCHAR, value VARCHAR)")
    cursor.execute("CREATE TABLE zaak(id INTEGER, deleted TIMESTAMP)")
    cursor.execute(
        "CREATE TABLE case_v1(id UUID, number INTEGER, dummy VARCHAR)"
    )


def setup_instances(redis_conn: redis.Redis) -> None:
    redis_conn.set(
        "saas:instance:first",
        """
<instance>
    zaaksysteemdb_ro.url = postgresql://zs:zs@database:5432/first
</instance>
    """,
    )
    redis_conn.set(
        "saas:instance:second",
        """
<instance>
    zaaksysteemdb_ro.url = postgresql://zs:zs@database:5432/second
</instance>
    """,
    )

    connection = psycopg2.connect(
        "postgresql://postgres:database123@database/template1"
    )
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    with connection.cursor() as cursor:
        cursor.execute("DROP DATABASE IF EXISTS first")
        cursor.execute("DROP DATABASE IF EXISTS second")
        cursor.execute("DROP USER IF EXISTS zs")

        cursor.execute("CREATE USER zs WITH PASSWORD 'zs'")
        cursor.execute("CREATE DATABASE first OWNER zs")
        cursor.execute("CREATE DATABASE second OWNER zs")

    connection.close()

    for env in {"first", "second"}:
        connection = psycopg2.connect(f"postgresql://zs:zs@database/{env}")
        with connection.cursor() as cursor:
            create_tables(cursor)
            cursor.execute(
                "INSERT INTO config(parameter, value) VALUES('enable_bi_dump', '1')"
            )
            cursor.execute("INSERT INTO zaak(id) VALUES(1), (3)")
            cursor.execute(
                f"""
            INSERT INTO case_v1(id, number, dummy) VALUES
                (gen_random_uuid(), 1, '{env} dummy data here'),
                (gen_random_uuid(), 2, '{env} another dummy'),
                (gen_random_uuid(), 3, '{env} more dummy data'),
                (gen_random_uuid(), 4, '{env} not in dump')
            """
            )
            connection.commit()


if __name__ == "__main__":
    redis_conn = redis.Redis.from_url(REDIS_URL)

    setup_instances(redis_conn)

    response = s3_client.list_buckets()
    if S3_BUCKET in map(lambda r: r["Name"], response["Buckets"]):
        print("Minio/S3 bucket exists")
    else:
        s3_client.create_bucket(Bucket=S3_BUCKET)
        print("Minio/S3 bucket created")

    print("Test data inserted into Redis and PostgreSQL; ready to run tests")
