# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.exceptions import NotFound
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import UUID, uuid4
from zsnl_amqp_consumers.event_logger_admin_catalog import (
    AdminCatalogBase,
    CaseTypeDeleted,
    CaseTypeOnlineStatusChanged,
    CaseTypeVersionUpdated,
    DocumentTemplateLogging,
    EmailTemplateLogging,
    FolderCreated,
    FolderDeleted,
    FolderEntryMoved,
)
from zsnl_amqp_consumers.event_logger_attribute import (
    AttributeBase,
    AttributeCreated,
    AttributeDeleted,
    AttributeEdited,
)
from zsnl_amqp_consumers.event_logger_case import (
    CaseAllocationSet,
    CaseAssigneeChanged,
    CaseAttributeUpdated,
    CaseCompletionDateSet,
    CaseCoordinatorSet,
    CaseCreated,
    CaseDestructionDateCleared,
    CaseDestructionDateRecalculated,
    CaseDestructionDateSet,
    CaseManagementBase,
    CasePaused,
    CaseRegistrationDateSet,
    CaseResultSet,
    CaseResumed,
    CaseStatusSet,
    CaseTargetCompletionDateSet,
)
from zsnl_amqp_consumers.event_logger_case_relation import (
    CaseRelationBase,
    CaseRelationDeleted,
)
from zsnl_amqp_consumers.event_logger_communication import (
    CommunicationBase,
    ContactMomentCreated,
    ExternalMessageCreated,
    MessageDeleted,
    NoteCreated,
    ThreadToCaseLinked,
)
from zsnl_amqp_consumers.event_logger_custom_object import (
    CustomObjectBase,
    CustomObjectCreated,
    CustomObjectDeleted,
    CustomObjectRelatedTo,
    CustomObjectUnrelatedFrom,
    CustomObjectUpdated,
)
from zsnl_amqp_consumers.event_logger_custom_object_type import (
    CustomObjectTypeBase,
    CustomObjectTypeCreated,
    CustomObjectTypeDeleted,
    CustomObjectTypeUpdated,
)
from zsnl_amqp_consumers.event_logger_document import (
    DocumentAddedToCase,
    DocumentAssignedToRole,
    DocumentAssignedToUser,
    DocumentAssignmentRejected,
    DocumentBase,
    DocumentCreated,
    DocumentDeleted,
    DocumentUpdated,
)
from zsnl_amqp_consumers.event_logger_objecttype import (
    ObjectTypeBase,
    ObjectTypeDeleted,
)
from zsnl_amqp_consumers.event_logger_subject import (
    BsnRetrieved,
    ContactInformationSaved,
    NonAuthenticBsnUpdated,
    PersonUpdated,
    SubjectBase,
)
from zsnl_amqp_consumers.event_logger_subject_relation import (
    SubjectRelationBase,
    SubjectRelationCreated,
    SubjectRelationDeleted,
    SubjectRelationUpdated,
)
from zsnl_amqp_consumers.repository import (
    AttributeInformation,
    CaseInformation,
    CaseRelationInformation,
    CaseTypeInformation,
    CaseTypeVersionInformation,
    CatalogFolderInformation,
    Contact,
    CustomObjectInformation,
    CustomObjectTypeInformation,
    DepartmentInformation,
    DocumentInformation,
    DocumentTemplateInformation,
    EmailTemplateInformation,
    FileInformation,
    ObjectTypeInformation,
    PersonInformation,
    RoleInformation,
    SubjectRelationInformation,
    UserInformation,
)
from zsnl_domains.database.schema import Logging


class TestEventLoggerCase:
    def test_case_base_generate_event_parameters(self):
        base = CaseManagementBase()
        with pytest.raises(NotImplementedError):
            session = mock.MagicMock()
            base.generate_event_parameters(
                session=session, entity_data={}, case_id=12345
            )

    def test_casetargetcompletiondateset_generate_event_parameters(self):
        logger = CaseTargetCompletionDateSet()

        case_id = 1234
        entity_data = {"target_completion_date": "2019-12-23"}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "target_date": "23-12-2019",
            "case_id": case_id,
        }

    def test_caseregistrationdateset_generate_event_parameters(self):
        logger = CaseRegistrationDateSet()

        case_id = 1234
        entity_data = {"registration_date": "2019-12-23"}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "registration_date": "23-12-2019",
            "case_id": case_id,
        }

    def test_casecompletiondateset_generate_event_parameters(self):
        logger = CaseCompletionDateSet()
        case_id = 1234
        entity_data = {"completion_date": "2019-12-23"}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "target_date": "23-12-2019",
            "case_id": case_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_assignee_set(self, mock_get_case, mock_get_user):
        case_event = CaseAssigneeChanged()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "assignee",
                    "new_value": {
                        "department": {
                            "description": "",
                            "name": "Backoffice",
                            "parent": None,
                            "uuid": str(uuid4()),
                        },
                        "group_ids": [2],
                        "id": 1,
                        "last_modified": "2021-03-09",
                        "nobody": False,
                        "properties": {
                            "cn": "admin",
                            "default_dashboard": 1,
                            "displayname": "Ad Mïn",
                            "givenname": "Ad",
                            "initials": "A.",
                            "mail": "devnull@zaaksysteem.nl",
                            "sn": "Mïn",
                            "telephonenumber": "0612345678",
                        },
                        "related_custom_object_uuid": None,
                        "role_ids": [1, 12],
                        "settings": {},
                        "subject_type": "employee",
                        "system": True,
                        "username": "admin",
                        "uuid": str(uuid4()),
                    },
                    "old_value": {
                        "department": None,
                        "group_ids": [2],
                        "id": 1,
                        "last_modified": "2021-03-09",
                        "nobody": False,
                        "properties": {
                            "cn": "admin",
                            "default_dashboard": 1,
                            "displayname": "Ad Mïn",
                            "givenname": "Ad",
                            "initials": "A.",
                            "mail": "devnull@zaaksysteem.nl",
                            "sn": "Mïn",
                            "telephonenumber": "0612345678",
                        },
                        "related_custom_object_uuid": None,
                        "role_ids": [1, 12],
                        "settings": {},
                        "subject_type": "employee",
                        "system": True,
                        "username": "admin",
                        "uuid": str(uuid4()),
                    },
                },
                {
                    "key": "send_email_to_assignee",
                    "new_value": False,
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:45:35.786808",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseAssigneeChanged",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Zaak 1234 geaccepteerd door name"
        assert log_record.event_type == "case/accept"

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_case.get_attribute_by_magic_string"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_attribute_updated(self, mock_get_case, mock_get_attribute):
        attribute_updated_handler = CaseAttributeUpdated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "custom_fields",
                    "new_value": {
                        "relation_field": {
                            "specifics": None,
                            "type": "relationship",
                            "value": [
                                {
                                    "specifics": {
                                        "metadata": {
                                            "description": "subtitel : ",
                                            "summary": "titel:  112233",
                                        },
                                        "relationship_type": "custom_object",
                                    },
                                    "type": "relationship",
                                    "value": "5ee8658f-ae4a-4759-9abf-d67bba3e8b96",
                                }
                            ],
                        }
                    },
                    "old_value": {},
                }
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": "1846262d-1499-4015-92d9-f7f2d20f0389",
            "created_date": "2022-02-25T08:17:52.685723",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": "39d1b4b6-e9ad-4e78-a9e0-114ccb335931",
            "entity_type": "Case",
            "event_name": "CustomFieldUpdated",
            "id": "a1d4389f-201c-468d-a9ec-1ff4c73cddf9",
            "user_uuid": "7145766b-472c-4316-9634-6470ea51c4c9",
        }
        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_attribute.return_value = AttributeInformation(
            id=4433, name="attr_name", magic_string="magic_string"
        )
        log_record = attribute_updated_handler(session=session, event=event)
        assert log_record.component == "kenmerk"
        assert log_record.onderwerp == 'Kenmerk "relation_field" gewijzigd'
        assert log_record.event_type == "case/attribute/update"
        assert log_record.event_data == {
            "case_id": 1234,
            "attribute_id": 4433,
            "attribute_name": "relation_field",
            "attribute_value": {
                "specifics": None,
                "type": "relationship",
                "value": [
                    {
                        "specifics": {
                            "metadata": {
                                "description": "subtitel : ",
                                "summary": "titel:  112233",
                            },
                            "relationship_type": "custom_object",
                        },
                        "type": "relationship",
                        "value": "5ee8658f-ae4a-4759-9abf-d67bba3e8b96",
                    }
                ],
            },
            "correlation_id": "1846262d-1499-4015-92d9-f7f2d20f0389",
        }

    def test_case_attribute_updated_invalid_number_of_fields(self):
        attribute_updated_handler = CaseAttributeUpdated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "custom_fields",
                    "new_value": {
                        "the_first_field": {"value": "abc"},
                        "the_second_field": {"value": "textvalue"},
                    },
                    "old_value": {},
                }
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": "1846262d-1499-4015-92d9-f7f2d20f0389",
            "created_date": "2022-02-25T08:17:52.685723",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": "39d1b4b6-e9ad-4e78-a9e0-114ccb335931",
            "entity_type": "Case",
            "event_name": "CustomFieldUpdated",
            "id": "a1d4389f-201c-468d-a9ec-1ff4c73cddf9",
            "user_uuid": "7145766b-472c-4316-9634-6470ea51c4c9",
        }
        with pytest.raises(AttributeError) as error:
            attribute_updated_handler(session=session, event=event)
        assert error.value.args == (
            "Only one attribute should be changed for each CaseAttributeUpdated event",
        )

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_coordinator_set(self, mock_get_case, mock_get_user):
        case_event = CaseCoordinatorSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "coordinator",
                    "new_value": {
                        "department": {
                            "description": "",
                            "name": "Backoffice",
                            "parent": None,
                            "uuid": str(uuid4()),
                        },
                        "group_ids": [2],
                        "id": 34,
                        "last_modified": "2021-03-15",
                        "nobody": False,
                        "properties": {
                            "displayname": "auser",
                            "givenname": "auser",
                            "initials": "a",
                            "mail": "auser@mintlab.nl",
                            "sn": "testname",
                            "title": "Administrator",
                        },
                        "related_custom_object_uuid": None,
                        "role_ids": [12],
                        "settings": {},
                        "subject_type": "employee",
                        "system": False,
                        "username": "auser",
                        "uuid": str(uuid4()),
                    },
                    "old_value": {
                        "department": None,
                        "group_ids": [2],
                        "id": 1,
                        "last_modified": "2021-03-09",
                        "nobody": False,
                        "properties": {
                            "cn": "admin",
                            "default_dashboard": 1,
                            "displayname": "Ad Mïn",
                            "givenname": "Ad",
                            "initials": "A.",
                            "mail": "devnull@zaaksysteem.nl",
                            "sn": "Mïn",
                            "telephonenumber": "0612345678",
                        },
                        "related_custom_object_uuid": None,
                        "role_ids": [1, 12],
                        "settings": {},
                        "subject_type": "employee",
                        "system": True,
                        "username": "admin",
                        "uuid": "bb402e57-bd86-4fe0-b2e2-21f2a552f1c9",
                    },
                }
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2021-03-17T12:10:51.085405",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseCoordinatorSet",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == 'Coordinator ingesteld op "name"'
        assert log_record.event_type == "case/relation/update"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_status_set(self, mock_get_case, mock_get_user):
        case_event = CaseStatusSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {"key": "status", "old_value": "new", "new_value": "open"}
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseStatusSet",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Status voor zaak: 1234 gewijzigd naar: open"
        )
        assert log_record.event_type == "case/update/status"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_paused(self, mock_get_case, mock_get_user):
        case_event = CasePaused()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "suspension_reason",
                    "new_value": "a reason for suspension",
                    "old_value": "",
                },
                {
                    "key": "stalled_since_date",
                    "new_value": "2019-02-01",
                    "old_value": "",
                },
                {
                    "key": "stalled_until_date",
                    "new_value": "2019-03-02",
                    "old_value": "",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CasePaused",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Zaak 1234 opgeschort tot 2019-03-02: a reason for suspension"
        )
        assert log_record.event_type == "case/suspend"

    def test_generate_subject_case_paused(self):
        case = CasePaused()
        event_parameters = {
            "case_id": 1234,
            "reason": "for a reason",
            "stalled_since": "20-01-2019",
            "stalled_until": "23-01-2019",
        }
        expected = "Zaak 1234 opgeschort tot 23-01-2019: for a reason"
        result = case._generate_subject(event_parameters=event_parameters)
        assert result == expected

    def test_generate_subject_case_paused_indefinite_date(self):
        case = CasePaused()
        event_parameters = {
            "case_id": 1234,
            "reason": "for a reason",
            "stalled_since": "20-01-2019",
        }
        expected = "Zaak 1234 opgeschort voor onbepaalde tijd: for a reason"
        result = case._generate_subject(event_parameters=event_parameters)
        assert result == expected

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_resumed(self, mock_get_case, mock_get_user):
        case_event = CaseResumed()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "resume_reason",
                    "new_value": "a reason for resuming",
                    "old_value": "",
                },
                {
                    "key": "stalled_since_date",
                    "new_value": "2019-02-01",
                    "old_value": "",
                },
                {
                    "key": "stalled_until_date",
                    "new_value": "2019-03-02",
                    "old_value": "",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseResumed",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp == "Zaak 1234 hervat: a reason for resuming"
        )
        assert log_record.event_type == "case/resume"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_management_base_call(self, mock_get_case, mock_get_user):
        session = mock.MagicMock()
        case = CaseRegistrationDateSet()

        event = {
            "changes": [
                {
                    "key": "registration_date",
                    "new_value": "2019-02-01",
                    "old_value": "2019-01-21",
                }
            ],
            "context": "localhost:6543",
            "correlation_id": "1322e1d8-7741-4f7f-9f4f-2cc95ecca010",
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "domain": "zsnl_domains.case_management",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "entity_type": "Case",
            "event_name": "CaseRegistrationDateSet",
            "id": "2f971350-3db9-431c-b724-2d1bc8279a72",
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case(session=session, event=event)
        assert log_record.zaak_id == 1234
        assert log_record.created_by_name_cache == "name"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_management_base_call_no_data(
        self, mock_get_case, mock_get_user
    ):
        session = mock.MagicMock()
        case = CaseRegistrationDateSet()
        event = {
            "changes": [
                {
                    "key": "registration_date",
                    "new_value": "2019-02-01",
                    "old_value": "2019-01-21",
                }
            ],
            "context": "localhost:6543",
            "correlation_id": "1322e1d8-7741-4f7f-9f4f-2cc95ecca010",
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "domain": "zsnl_domains.case_management",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "entity_type": "Case",
            "event_name": "CaseRegistrationDateSet",
            "id": "2f971350-3db9-431c-b724-2d1bc8279a72",
            "entity_data": {},
        }

        mock_get_case.side_effect = NoResultFound
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        with pytest.raises(NoResultFound):
            case(session=session, event=event)

    def test_generate_subject(self):
        case = CaseRegistrationDateSet()
        event = {
            "parameters": {"case_id": 1234, "registration_date": "23-01-2019"}
        }
        expected = (
            "Registratiedatum voor zaak: 1234 gewijzigd naar: 23-01-2019"
        )
        result = case._generate_subject(event_parameters=event["parameters"])
        assert result == expected

    def test_create_logging_record(self):
        case = CaseRegistrationDateSet()
        user_info = UserInformation(
            id=444, display_name="name", type="medewerker"
        )

        log_record = case._create_logging_record(
            user_info=user_info,
            event_type="case/publish",
            subject="always something to log",
            component="zaak",
            component_id=1234,
            case_id=4567,
            created_date="2019-12-21",
            event_data={"event": "data"},
        )
        assert isinstance(log_record, Logging)
        assert log_record.event_data == {"event": "data"}
        assert log_record.created_by_name_cache == "name"
        assert log_record.created == "2019-12-21"
        assert log_record.last_modified == "2019-12-21"
        assert log_record.created_by == "betrokkene-medewerker-444"
        assert log_record.zaak_id == 4567

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_role")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_department")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_allocation_set(
        self, mock_get_case, mock_get_user, mock_get_department, mock_get_role
    ):
        case_event = CaseAllocationSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "department",
                    "old_value": None,
                    "new_value": {
                        "entity_id": str(uuid4()),
                        "type": "Department",
                    },
                },
                {
                    "key": "role",
                    "old_value": None,
                    "new_value": {"entity_id": str(uuid4()), "type": "Role"},
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseAssigneeChanged",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_department.return_value = DepartmentInformation(
            id=3, name="Frontoffice"
        )
        mock_get_role.return_value = RoleInformation(
            id=1, name="Administrator"
        )
        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Toewijzing gewijzigd naar afdeling 'Frontoffice' en rol 'Administrator'"
        )
        assert log_record.event_type == "case/update/allocation"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_role")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_department")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_allocation_set_uuid_dept_and_rol(
        self, mock_get_case, mock_get_user, mock_get_department, mock_get_role
    ):
        case_event = CaseAllocationSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "department",
                    "old_value": None,
                    "new_value": {"uuid": str(uuid4()), "type": "Department"},
                },
                {
                    "key": "role",
                    "old_value": None,
                    "new_value": {"uuid": str(uuid4()), "type": "Role"},
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseAssigneeChanged",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_department.return_value = DepartmentInformation(
            id=3, name="Frontoffice"
        )
        mock_get_role.return_value = RoleInformation(
            id=1, name="Administrator"
        )
        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Toewijzing gewijzigd naar afdeling 'Frontoffice' en rol 'Administrator'"
        )
        assert log_record.event_type == "case/update/allocation"

    def test_casedestructiondatecleared_generate_event_parameters(self):
        logger = CaseDestructionDateCleared()

        case_id = 1234
        entity_data = {
            "changes": [
                {
                    "key": "destruction_date",
                    "new_value": None,
                    "old_value": None,
                }
            ]
        }
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "case_id": case_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_casedestructiondatecleared_with_reason_generate_event_parameters(
        self, mock_get_case
    ):
        logger = CaseDestructionDateCleared()
        event = {
            "changes": [
                {
                    "key": "destruction_date",
                    "new_value": None,
                    "old_value": "2022-08-23",
                },
                {
                    "key": "destruction_reason",
                    "new_value": {
                        "label": None,
                        "reason": "Reason for destruction",
                    },
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseDestructionDateCleared",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )

        session = mock.MagicMock()
        log_record = logger(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Vernietigingsdatum voor zaak: 1234 gewist: Reason for destruction"
        )
        assert log_record.event_type == "case/update/purge_date"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_casedestructiondaterecalculated_with_reason_generate_event_parameters(
        self, mock_get_case
    ):
        logger = CaseDestructionDateRecalculated()
        event = {
            "changes": [
                {
                    "key": "destruction_date",
                    "new_value": "2022-08-23",
                    "old_value": "2022-08-23",
                },
                {
                    "key": "destruction_reason",
                    "new_value": {
                        "label": "4 weken",
                        "reason": "Reason for destruction",
                    },
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseDestructionDateRecalculated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )

        session = mock.MagicMock()
        log_record = logger(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Vernietigingsdatum voor zaak 1234 herberekend naar 4 weken: Reason for destruction"
        )
        assert log_record.event_type == "case/update/purge_date"

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_casedestructiondateset_with_reason_generate_event_parameters(
        self, mock_get_case
    ):
        logger = CaseDestructionDateSet()
        event = {
            "changes": [
                {
                    "key": "destruction_date",
                    "new_value": "2022-10-20",
                    "old_value": "2022-08-23",
                },
                {
                    "key": "destruction_reason",
                    "new_value": {
                        "label": None,
                        "reason": "Reason for destruction",
                    },
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseDestructionDateSet",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )

        session = mock.MagicMock()
        log_record = logger(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Vernietigingsdatum voor zaak: 1234 gewijzigd naar 20-10-2022 : Reason for destruction"
        )
        assert log_record.event_type == "case/update/completion_date"

    def test_casedestructiondateset_generate_event_parameters(self):
        logger = CaseDestructionDateSet()

        case_id = 1234
        entity_data = {
            "destruction_date": "2022-05-16",
        }
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "case_id": case_id,
            "destruction_date": "16-05-2022",
        }

    def test_case_update_result_event_parameters(self):
        logger = CaseResultSet()

        case_id = 1234
        entity_data = {
            "archival_state": "vernietigen",
            "archival_state_old": "vernietigen",
            "result": {
                "archival_attributes": None,
                "result": "aangegaan",
                "result_id": 113,
            },
            "result_old": {
                "archival_attributes": {
                    "selection_list": "",
                    "state": "vernietigen",
                },
                "result": "aangegaan",
                "result_id": 113,
            },
        }
        session = mock.MagicMock()
        result_mock = mock.MagicMock()
        result_mock.configure_mock(label="label_value")
        session.execute().fetchone.return_value = result_mock

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "case_id": 1234,
            "result_new": "aangegaan",
            "result_new_label": "label_value",
            "result_old": "aangegaan",
        }

    def test_case_set_result_event_parameters(self):
        logger = CaseResultSet()

        case_id = 1234
        entity_data = {
            "archival_state": "vernietigen",
            "archival_state_old": "vernietigen",
            "result": {
                "archival_attributes": None,
                "result": "aangegaan",
                "result_id": 113,
            },
        }
        session = mock.MagicMock()
        result_mock = mock.MagicMock()
        result_mock.configure_mock(label="label_value")
        session.execute().fetchone.return_value = result_mock

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {
            "case_id": 1234,
            "result_new": "aangegaan",
            "result_new_label": "label_value",
        }


class TestEventLoggerCaseType:
    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_case_type")
    def test_case_type_online_status_changed_activated(
        self, mock_get_case_type, mock_get_user
    ):
        session = mock.MagicMock()
        case_type_event = CaseTypeOnlineStatusChanged()

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {"key": "active", "old_value": False, "new_value": True},
                {
                    "key": "reason",
                    "old_value": "",
                    "new_value": "reason for activating",
                },
            ],
            "entity_data": {},
        }
        mock_get_case_type.return_value = CaseTypeInformation(
            id=1234, title="case_type_title"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case_type_event(session=session, event=event)
        assert log_record.component == "zaaktype"
        assert (
            log_record.onderwerp
            == 'Zaaktype "case_type_title" geactiveerd: reason for activating'
        )
        assert log_record.event_type == "casetype/publish"

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_case_type")
    def test_case_type_online_status_changed_deactivated(
        self, mock_get_case_type, mock_get_user
    ):
        session = mock.MagicMock()
        case_type_event = CaseTypeOnlineStatusChanged()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {"key": "active", "old_value": True, "new_value": False},
                {
                    "key": "reason",
                    "old_value": "",
                    "new_value": "reason for de-activating",
                },
            ],
            "entity_data": {},
        }
        mock_get_case_type.return_value = CaseTypeInformation(
            id=1234, title="case_type_title"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case_type_event(session=session, event=event)
        assert log_record.component == "zaaktype"
        assert (
            log_record.onderwerp
            == 'Zaaktype "case_type_title" gedeactiveerd: reason for de-activating'
        )
        assert log_record.event_type == "casetype/unpublish"

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_case_type")
    def test_case_type_deleted(self, mock_get_case_type, mock_get_user):
        session = mock.MagicMock()
        case_type_event = CaseTypeDeleted()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {
                    "key": "reason",
                    "old_value": "",
                    "new_value": "reason for de-activating",
                }
            ],
            "entity_data": {},
        }
        mock_get_case_type.return_value = CaseTypeInformation(
            id=1234, title="case_type_title"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case_type_event(session=session, event=event)
        assert log_record.component == "zaaktype"
        assert (
            log_record.onderwerp
            == 'Zaaktype "case_type_title" verwijderd: reason for de-activating'
        )
        assert log_record.event_type == "casetype/remove"

    def test_generate_event_parameters_not_implemented(self):
        logger = AdminCatalogBase()
        with pytest.raises(NotImplementedError):
            logger.generate_event_parameters(entity_data={}, case_type_info={})

    def test_get_event_type_format_not_implemented(self):
        logger = AdminCatalogBase()
        with pytest.raises(NotImplementedError):
            logger.get_event_type_format(event_params={})

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_catalog_folder"
    )
    def test_folder_entry_moved(self, mock_get_catalog_folder, mock_get_user):
        session = mock.MagicMock()
        moved_event = FolderEntryMoved()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_folder = CatalogFolderInformation(
            id=10, uuid=str(uuid4()), name="Test Folder"
        )

        # Move single item to a folder

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [{"key": "folder_id", "new_value": 2, "old_value": 2}],
            "entity_data": {"entry_type": "folder", "name": "test5"},
        }

        mock_get_catalog_folder.return_value = mock_folder

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert (
            log_record.onderwerp == "Map 'test5' verplaatst naar 'Test Folder'"
        )
        assert log_record.component_id == mock_folder.id
        assert log_record.zaak_id is None

        assert log_record.event_type == "admin/catalog/folder_entries_moved"
        assert log_record.event_data == {
            "destination_folder_id": mock_folder.id,
            "destination_folder_uuid": mock_folder.uuid,
            "destination_folder_name": f"'{mock_folder.name}'",
            "entry_type": "folder",
            "name": "test5",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_catalog_folder"
    )
    def test_something_unknown_moved(
        self, mock_get_catalog_folder, mock_get_user
    ):
        session = mock.MagicMock()
        moved_event = FolderEntryMoved()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_folder = CatalogFolderInformation(
            id=10, uuid=str(uuid4()), name="Test Folder"
        )

        # Move single item to a folder

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [{"key": "folder_id", "new_value": 2, "old_value": 2}],
            "entity_data": {
                "entry_type": "something_unknown",
                "name": "test5",
            },
        }

        mock_get_catalog_folder.return_value = mock_folder

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert (
            log_record.onderwerp
            == "something_unknown 'test5' verplaatst naar 'Test Folder'"
        )
        assert log_record.component_id == mock_folder.id
        assert log_record.zaak_id is None

        assert log_record.event_type == "admin/catalog/folder_entries_moved"
        assert log_record.event_data == {
            "destination_folder_id": mock_folder.id,
            "destination_folder_uuid": mock_folder.uuid,
            "destination_folder_name": f"'{mock_folder.name}'",
            "entry_type": "something_unknown",
            "name": "test5",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    def test_folder_entry_deleted(self, mock_get_user):
        session = mock.MagicMock()
        moved_event = FolderDeleted()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        entity_id = str(uuid4())
        user_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": user_uuid,
            "entity_id": entity_id,
            "changes": [
                {
                    "key": "commit_message",
                    "new_value": "test delete",
                    "old_value": "delete",
                }
            ],
            "entity_data": {"entry_type": "folder", "name": "test5"},
        }

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert log_record.onderwerp == "Folder 'test5' verwijderd: test delete"
        assert log_record.component_id is None
        assert log_record.zaak_id is None

        assert log_record.event_type == "folder/remove"
        assert log_record.event_data == {
            "name": "test5",
            "reason": "test delete",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    def test_folder_created(self, mock_get_user):
        session = mock.MagicMock()
        moved_event = FolderCreated()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        entity_id = str(uuid4())
        user_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": user_uuid,
            "entity_id": entity_id,
            "changes": [
                {"key": "parent_uuid", "old_value": None, "new_value": None},
                {"key": "name", "old_value": None, "new_value": "test5"},
            ],
            "entity_data": {},
        }

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert log_record.onderwerp == "Folder 'test5' toegevoegd"
        assert log_record.component_id is None
        assert log_record.zaak_id is None

        assert log_record.event_type == "folder/create"
        assert log_record.event_data == {"name": "test5"}

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_catalog_folder"
    )
    def test_folder_entry_moved_to_root_folder(
        self, mock_get_catalog_folder, mock_get_user
    ):
        session = mock.MagicMock()
        moved_event = FolderEntryMoved()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        # Move single item to a folder

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {"key": "folder_id", "new_value": None, "old_value": 2}
            ],
            "entity_data": {"entry_type": "folder", "name": "test5"},
        }

        log_record = moved_event(session, event)

        assert log_record.component == "folder"
        assert log_record.onderwerp == "Map 'test5' verplaatst naar 'hoofdmap'"
        assert log_record.component_id is None
        assert log_record.zaak_id is None

        assert log_record.event_type == "admin/catalog/folder_entries_moved"
        assert log_record.event_data == {
            "destination_folder_id": None,
            "destination_folder_uuid": None,
            "destination_folder_name": "'hoofdmap'",
            "entry_type": "folder",
            "name": "test5",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_case_type_version"
    )
    def test_case_type_active_version_changed(
        self, mock_get_case_type_version, mock_get_user
    ):
        session = mock.MagicMock()
        case_type_event = CaseTypeVersionUpdated()
        old_version_uuid = str(uuid4())
        new_version_uuid = str(uuid4())

        event = {
            "created_date": "2019-02-25",
            "user_uuid": "46fede5c-39ce-11e9-935c-1b25e587a0d0",
            "entity_id": "f79226e4-39cd-11e9-b676-4b5bd959f2fb",
            "changes": [
                {
                    "key": "current_version_uuid",
                    "old_value": old_version_uuid,
                    "new_value": new_version_uuid,
                },
                {
                    "key": "reason",
                    "old_value": "",
                    "new_value": "reason for changing",
                },
            ],
            "entity_data": {},
        }
        mock_get_case_type_version.return_value = CaseTypeVersionInformation(
            case_type_id=1234, title="case_type_title", version=6
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        log_record = case_type_event(session=session, event=event)
        assert log_record.component == "zaaktype"
        assert (
            log_record.onderwerp
            == 'Zaaktype "case_type_title" gewijzigd naar versie 6: reason for changing'
        )
        assert log_record.event_type == "casetype/update/version"


class TestEventLoggerAttribute:
    def test_attribute_base_generate_event_parameters(self):
        base = AttributeBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={}, attribute_info=None, commit_message=""
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_attribute")
    def test_attribute_edited(self, mock_get_attribute, mock_get_user):
        session = mock.MagicMock()
        attribute_event = AttributeEdited()

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "public_name",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "type_multiple",
                    "old_value": False,
                    "new_value": True,
                },
                {
                    "key": "attribute_values",
                    "old_value": [],
                    "new_value": [
                        {"value": "a", "active": True, "sort_order": 0},
                        {"value": "b", "active": False, "sort_order": 1},
                    ],
                },
            ],
            "entity_data": {"commit_message": "commit_message"},
        }
        mock_get_attribute.return_value = AttributeInformation(
            id=1234, name="attr_name", magic_string="magic_string"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = attribute_event(session=session, event=event)

        mock_get_attribute.assert_called_once_with(
            session=session, uuid=event["entity_id"]
        )
        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )
        assert log_record.component == "kenmerk"
        assert (
            log_record.onderwerp
            == "Kenmerk 'magic_string' opgeslagen (commit_message), Opties: a, b(inactief)"
        )
        assert log_record.event_type == "attribute/update"
        assert log_record.created_by_name_cache == "name"

    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_attribute")
    def test_attribute_created(self, mock_get_attribute, mock_get_user):
        session = mock.MagicMock()
        attribute_event = AttributeCreated()

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "public_name",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "type_multiple",
                    "old_value": False,
                    "new_value": True,
                },
            ],
            "entity_data": {"commit_message": "commit_message"},
        }
        mock_get_attribute.return_value = AttributeInformation(
            id=1234, name="attr_name", magic_string="magic_string"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = attribute_event(session=session, event=event)

        mock_get_attribute.assert_called_once_with(
            session=session, uuid=event["entity_id"]
        )
        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )
        assert log_record.component == "kenmerk"
        assert (
            log_record.onderwerp
            == "Kenmerk 'magic_string' toegevoegd: commit_message"
        )
        assert log_record.event_type == "attribute/create"
        assert log_record.created_by_name_cache == "name"

    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_attribute.get_attribute")
    def test_attribute_deleted(self, mock_get_attribute, mock_get_user):
        session = mock.MagicMock()
        attribute_event = AttributeDeleted()

        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "public_name",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "type_multiple",
                    "old_value": False,
                    "new_value": True,
                },
            ],
            "entity_data": {"commit_message": "commit_message"},
        }
        mock_get_attribute.return_value = AttributeInformation(
            id=1234, name="attr_name", magic_string="magic_string"
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = attribute_event(session=session, event=event)

        mock_get_attribute.assert_called_once_with(
            session=session, uuid=event["entity_id"]
        )
        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )
        assert log_record.component == "kenmerk"
        assert (
            log_record.onderwerp
            == "Kenmerk attr_name (magic string magic_string) verwijderd: commit_message"
        )
        assert log_record.event_type == "attribute/remove"
        assert log_record.created_by_name_cache == "name"


class TestEmailTemplateInformationLogger:
    def setup_method(self):
        self.logger = EmailTemplateLogging()

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_email_template"
    )
    def test_call(self, mock_get_email_template, mock_get_user):
        session = mock.MagicMock()
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_email_template.return_value = EmailTemplateInformation(
            id=100, label="label1"
        )

        event = {
            "event_name": "EmailTemplateCreated",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "label",
                    "old_value": "template1",
                    "new_value": "template2",
                },
            ],
            "entity_data": {},
        }
        log_record = self.logger(session, event)
        assert log_record.component == "notificatie"
        assert log_record.event_type == "template/email/create"
        assert log_record.component_id == 100
        assert log_record.event_data == {
            "reason": "new_name",
            "template_id": 100,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_email_template"
    )
    def test_call_email_template_deleted(
        self, mock_get_email_template, mock_get_user
    ):
        session = mock.MagicMock()
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_email_template.return_value = EmailTemplateInformation(
            id=100, label="label1"
        )

        event = {
            "event_name": "EmailTemplateDeleted",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "label",
                    "old_value": "template1",
                    "new_value": "template2",
                },
            ],
            "entity_data": {},
        }
        log_record = self.logger(session, event)
        assert log_record.component == "notificatie"
        assert log_record.event_type == "template/email/remove"
        assert log_record.component_id == 100
        assert log_record.event_data == {
            "reason": "new_name",
            "template_id": 100,
        }

    def test_generate_subject(self):
        subject = self.logger._generate_subject(
            label="template 1",
            reason="new_template",
            event_name="EmailTemplateCreated",
        )
        assert (
            subject == 'E-mailsjabloon "template 1" toegevoegd: new_template'
        )

        subject2 = self.logger._generate_subject(
            label="template 1",
            reason="template edited",
            event_name="EmailTemplateEdited",
        )
        assert (
            subject2
            == 'E-mailsjabloon "template 1" opgeslagen: template edited'
        )

    def test_generate_event_paremeters(self):
        ent_data = {"commit_message": "just because"}
        email_t_info = EmailTemplateInformation(id=100, label="label1")
        event_params = self.logger.generate_event_parameters(
            entity_data=ent_data, email_template_info=email_t_info
        )
        assert event_params == {"reason": "just because", "template_id": 100}


class TestEventLoggerObjectType:
    def test_attribute_base_generate_event_parameters(self):
        base = ObjectTypeBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={}, object_type_info=None, commit_message=""
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_objecttype.get_user")
    def test_call_object_type_deleted(self, mock_get_user):
        session = mock.MagicMock()

        object_type_event = ObjectTypeDeleted()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        event = {
            "event_name": "ObjectTypeDeleted",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": "old_namme",
                    "new_value": "new_name",
                },
                {
                    "key": "name",
                    "old_value": "objectype1",
                    "new_value": "objectype2",
                },
            ],
            "entity_data": {},
        }
        log_record = object_type_event(session, event)
        assert log_record.component == "object_type"
        assert log_record.event_type == "object/delete"
        assert log_record.component_id is None
        assert log_record.event_data == {
            "object_type": "ObjectType",
            "object_label": "objectype2",
            "reason": "new_name",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_objecttype.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_objecttype.get_object_type")
    def test_call_object_type_other(self, mock_get_object_type, mock_get_user):
        session = mock.MagicMock()

        object_type_event = ObjectTypeDeleted()

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        mock_get_object_type.return_value = ObjectTypeInformation(
            id=100, title="label1"
        )

        event = {
            "event_name": "ObjectTypeCreated",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": None,
                    "new_value": "new_name",
                },
                {"key": "name", "old_value": None, "new_value": "objectype2"},
            ],
            "entity_data": {},
        }
        object_type_event(session, event)
        mock_get_object_type.assert_called_once_with(
            session=session, uuid=event["entity_id"]
        )
        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

    def test_generate_subject(self):
        event_params = {
            "object_type": "ObjectType",
            "object_label": "object type 1",
            "reason": "deleted object type",
        }
        object_type_event = ObjectTypeDeleted()
        subject = object_type_event._generate_subject(
            event_parameters=event_params
        )
        assert (
            subject
            == 'ObjectType "object type 1" verwijderd: deleted object type'
        )

        event_params1 = {
            "object_type": "ObjectType",
            "object_label": "object type 2",
            "reason": "object type deleted",
        }
        subject2 = object_type_event._generate_subject(
            event_parameters=event_params1
        )
        assert (
            subject2
            == 'ObjectType "object type 2" verwijderd: object type deleted'
        )

    def test_generate_event_paremeters(self):
        ent_data = {"commit_message": "just because", "name": "test obj"}
        object_type_event = ObjectTypeDeleted()
        event_params = object_type_event.generate_event_parameters(
            entity_data=ent_data, object_type_info=None, commit_message="str"
        )
        assert event_params == {
            "object_type": "ObjectType",
            "object_label": "test obj",
            "reason": "str",
        }


class TestDocumentTemplateInformationLogger:
    def setup_method(self):
        self.logger = DocumentTemplateLogging()

    @mock.patch("zsnl_amqp_consumers.event_logger_admin_catalog.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_admin_catalog.get_document_template"
    )
    def test_call(self, mock_get_document_template, mock_get_user):
        session = mock.MagicMock()
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_document_template.return_value = DocumentTemplateInformation(
            id=100, name="document"
        )

        event = {
            "event_name": "DocumentTemplateCreated",
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "commit_message",
                    "old_value": None,
                    "new_value": "new template",
                },
                {"key": "name", "old_value": None, "new_value": "document"},
            ],
            "entity_data": {},
        }
        log_record = self.logger(session, event)
        assert log_record.component == "sjabloon"
        assert log_record.event_type == "template/create"
        assert log_record.component_id == 100
        assert log_record.event_data == {
            "reason": "new template",
            "template_id": 100,
        }

    def test_generate_subject(self):
        subject = self.logger._generate_subject(
            name="document",
            reason="template created",
            event_name="DocumentTemplateCreated",
        )
        assert subject == 'Sjabloon "document" toegevoegd: template created'

        subject2 = self.logger._generate_subject(
            name="document 1",
            reason="template edited",
            event_name="DocumentTemplateEdited",
        )
        assert subject2 == 'Sjabloon "document 1" opgeslagen: template edited'

    def test_generate_event_paremeters(self):
        entity_data = {"commit_message": "just because"}
        document_template_info = DocumentTemplateInformation(
            id=100, name="document"
        )
        event_params = self.logger.generate_event_parameters(
            entity_data=entity_data,
            document_template_info=document_template_info,
        )
        assert event_params == {"reason": "just because", "template_id": 100}

    def test_casecreated(self):
        logger = CaseCreated()
        case_id = 1234
        entity_data = {}
        session = mock.MagicMock()

        event_params = logger.generate_event_parameters(
            session=session, entity_data=entity_data, case_id=case_id
        )
        assert event_params == {"case_id": case_id}


class TestEventLoggerCommunication:
    def test_communication_base_generate_event_parameters(self):
        base = CommunicationBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(entity_data={})

    def test_communication_base_generate_component_attributes(self):
        base = CommunicationBase()
        with pytest.raises(NotImplementedError):
            base.generate_component_attributes()

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    def test_thread_to_case_linked(
        self, mock_get_case_from_thread, mock_get_user
    ):
        session = mock.MagicMock()
        contact_moment_event = ThreadToCaseLinked()
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": "a072d0cd-a261-477d-8aba-1971a486e6a5",
                }
            ],
            "entity_data": {"message_type": "note"},
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_case_from_thread.return_value = CaseInformation(
            id=3, confidentiality="public", uuid=uuid4()
        )

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        mock_get_case_from_thread.assert_called_once_with(
            session=session, thread_uuid="a072d0cd-a261-477d-8aba-1971a486e6a5"
        )

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Bericht toegevoegd aan zaak '3'"
        assert log_record.event_type == "case/thread/link"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.zaak_id == 3

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_message_deleted(self, mock_get_user):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        contact_moment_event = MessageDeleted()
        thread_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "note",
                "case_uuid": case_uuid,
                "thread_uuid": thread_uuid,
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        assert log_record.component == "betrokkene"
        assert log_record.onderwerp == "name heeft notitie verwijderd."
        assert log_record.event_type == "subject/message/delete"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.zaak_id == session.query().filter().one().id

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_message_deleted_external_message(self, mock_get_user):
        session = mock.MagicMock()
        case_uuid = str(uuid4())
        contact_moment_event = MessageDeleted()
        thread_uuid = str(uuid4())
        event = {
            "created_date": "2019-02-25",
            "user_uuid": uuid4(),
            "entity_id": uuid4(),
            "changes": [],
            "entity_data": {
                "message_type": "external",
                "case_uuid": case_uuid,
                "external_message_type": "pip",
                "thread_uuid": thread_uuid,
            },
        }

        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = contact_moment_event(session=session, event=event)

        mock_get_user.assert_called_once_with(
            session=session, uuid=event["user_uuid"]
        )

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "name heeft bericht van type pip verwijderd."
        )
        assert log_record.event_type == "subject/message/delete"
        assert log_record.created_by_name_cache == "name"
        assert log_record.created_for is None
        assert log_record.zaak_id == session.query().filter().one().id

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_external_message_created_event(
        self, mock_get_user, mock_get_contact, mock_get_case
    ):
        communication_event = ExternalMessageCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "pip",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )

        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "PIP-bericht toegevoegd"
        assert log_record.event_type == "case/pip/feedback"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "PIP-bericht",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_note_created_event(
        self, mock_get_user, mock_get_contact, mock_get_case_from_thread
    ):
        communication_event = NoteCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                }
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "Note",
            "event_name": "NoteCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case_from_thread.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )
        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Notitie toegevoegd"
        assert log_record.event_type == "case/note/created"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "Notitie",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_case_from_thread"
    )
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_contact_moment_created_event(
        self, mock_get_user, mock_get_contact, mock_get_case_from_thread
    ):
        communication_event = ContactMomentCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                }
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ContactMoment",
            "event_name": "ContactMomentCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case_from_thread.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )
        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Contactmoment toegevoegd"
        assert log_record.event_type == "case/contact_moment/created"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "Contactmoment",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_file")
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_import_email_event(
        self, mock_get_user, mock_get_contact, mock_get_case, mock_get_file
    ):
        communication_event = ExternalMessageCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()

        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {"key": "is_imported", "old_value": None, "new_value": True},
                {
                    "key": "original_message_file",
                    "old_value": None,
                    "new_value": uuid4(),
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "email",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )
        mock_get_file.return_value = FileInformation(id=13, name="test.msg")

        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "E-mail 'test.msg' als bericht geïmporteerd."
        )
        assert log_record.event_type == "case/email/created"
        assert log_record.created_by_name_cache == "test_user"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "E-mail",
            "filename": "test.msg",
        }

        # When case id cannot be found.
        mock_get_case.return_value = CaseInformation(
            id=None, confidentiality="public", uuid=None
        )
        log_record = communication_event(session=session, event=event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_file")
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_case")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_communication.get_contact_from_thread"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_communication.get_user")
    def test_email_received_event(
        self, mock_get_user, mock_get_contact, mock_get_case, mock_get_file
    ):
        communication_event = ExternalMessageCreated()
        session = mock.MagicMock()
        thread_uuid = uuid4()
        case_uuid = uuid4()

        event = {
            "changes": [
                {
                    "key": "thread_uuid",
                    "old_value": None,
                    "new_value": thread_uuid,
                },
                {
                    "key": "original_message_file",
                    "old_value": None,
                    "new_value": uuid4(),
                },
                {
                    "key": "external_message_type",
                    "old_value": None,
                    "new_value": "email",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": "None",
            "domain": "zsnl_domains.communication",
            "entity_id": str(uuid4()),
            "entity_type": "ExternalMessage",
            "event_name": "ExternalMessageCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=12, display_name="test_user", type="medewerker"
        )
        mock_get_contact.return_value = Contact(id=12, type="employee")
        mock_get_case.return_value = CaseInformation(
            id=13, confidentiality="public", uuid=case_uuid
        )
        mock_get_file.return_value = FileInformation(id=13, name="test.msg")

        log_record = communication_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "E-mail toegevoegd"
        assert log_record.event_type == "case/email/created"
        assert log_record.created_by_name_cache == "None"
        assert log_record.created_for is None
        assert log_record.zaak_id == 13
        assert log_record.event_data == {
            "case_id": 13,
            "content": "",
            "message_type": "E-mail",
        }

        # When user with uuid cannot be found.
        event["user_uuid"] = uuid4()
        mock_get_user.side_effect = NotFound
        log_record = communication_event(session=session, event=event)
        assert log_record is None


class TestEventLoggerSubjectRelation:
    def test_case_base_generate_event_parameters(self):
        base = SubjectRelationBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(entity_data={}, case_id=12345)

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_created_for_employee(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationCreated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "magic_string_prefix",
                    "old_value": None,
                    "new_value": "advocaat",
                },
                {"key": "role", "old_value": None, "new_value": "Advocaat"},
                {
                    "key": "subject",
                    "old_value": None,
                    "new_value": {
                        "type": "employee",
                        "id": str(uuid4()),
                        "name": "beheerder",
                    },
                },
                {"key": "permission", "old_value": None, "new_value": "write"},
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationCreated",
            "id": str(uuid4()),
            "entity_data": {"case": {"id": str(uuid4()), "type": "case"}},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=654, subject_type="medewerker", subject_id=2
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Betrokkene 'beheerder' toegevoegd aan zaak 1234 als Advocaat"
        )
        assert log_record.event_type == "case/subject/add"
        assert log_record.event_data == {
            "case_id": 1234,
            "case_subject_id": 654,
            "params": {
                "magic_string_prefix": "advocaat",
                "rol": "Advocaat",
                "employee_authorisation": "write",
                "betrokkene_identifier": "betrokkene-medewerker-2",
            },
            "subject_name": "beheerder",
            "role": "Advocaat",
            "subject_id": 654,
            "correlation_id": "req-1234",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_created_for_organization(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationCreated()
        session = mock.MagicMock()

        event = {
            "changes": [
                {
                    "key": "magic_string_prefix",
                    "old_value": None,
                    "new_value": "advocaat",
                },
                {"key": "role", "old_value": None, "new_value": "Advocaat"},
                {"key": "authorised", "old_value": None, "new_value": True},
                {
                    "key": "subject",
                    "old_value": None,
                    "new_value": {
                        "type": "organization",
                        "id": str(uuid4()),
                        "name": "test_org",
                    },
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationCreated",
            "id": str(uuid4()),
            "entity_data": {"case": {"id": str(uuid4()), "type": "case"}},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=800, subject_type="bedrijf", subject_id=5
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Betrokkene 'test_org' toegevoegd aan zaak 1234 als Advocaat"
        )
        assert log_record.event_type == "case/subject/add"
        assert log_record.event_data == {
            "case_id": 1234,
            "case_subject_id": 800,
            "params": {
                "magic_string_prefix": "advocaat",
                "pip_authorized": "0",
                "rol": "Advocaat",
                "betrokkene_identifier": "betrokkene-bedrijf-5",
            },
            "subject_name": "test_org",
            "role": "Advocaat",
            "subject_id": 800,
            "correlation_id": "req-1234",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_updated_for_employee(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationUpdated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "magic_string_prefix",
                    "old_value": None,
                    "new_value": "advocaat",
                },
                {"key": "role", "old_value": "Advocaat", "new_value": "Role"},
                {
                    "key": "magic_string_prefix",
                    "old_value": "advocaat",
                    "new_value": "role",
                },
                {
                    "key": "permission",
                    "old_value": "write",
                    "new_value": "write",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationUpdated",
            "id": str(uuid4()),
            "entity_data": {
                "case": {"id": str(uuid4()), "type": "case"},
                "subject": {
                    "id": str(uuid4()),
                    "type": "employee",
                    "name": "beheerder",
                },
            },
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=654, subject_type="medewerker", subject_id=2
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.zaak_id == 1234
        assert (
            log_record.onderwerp
            == "Betrokkene 'beheerder' van zaak 1234 bijgewerkt"
        )
        assert log_record.event_type == "case/subject/update"
        assert log_record.event_data == {
            "case_id": 1234,
            "case_subject_id": 654,
            "params": {
                "magic_string_prefix": "role",
                "rol": "Role",
                "employee_authorisation": "write",
                "betrokkene_identifier": "betrokkene-medewerker-2",
            },
            "subject_name": "beheerder",
            "role": "Role",
            "subject_id": 654,
            "correlation_id": "req-1234",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_updated_for_organization(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationUpdated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "magic_string_prefix",
                    "old_value": None,
                    "new_value": "advocaat",
                },
                {"key": "role", "old_value": "Advocaat", "new_value": "Role"},
                {
                    "key": "magic_string_prefix",
                    "old_value": "advocaat",
                    "new_value": "role",
                },
                {"key": "authorized", "old_value": False, "new_value": True},
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationUpdated",
            "id": str(uuid4()),
            "entity_data": {
                "case": {"id": str(uuid4()), "type": "case"},
                "subject": {
                    "id": str(uuid4()),
                    "type": "organization",
                    "name": "test_org",
                },
            },
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=800, subject_type="bedrijf", subject_id=7
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.zaak_id == 1234
        assert (
            log_record.onderwerp
            == "Betrokkene 'test_org' van zaak 1234 bijgewerkt"
        )
        assert log_record.event_type == "case/subject/update"
        assert log_record.event_data == {
            "case_id": 1234,
            "case_subject_id": 800,
            "params": {
                "magic_string_prefix": "role",
                "pip_authorized": "1",
                "rol": "Role",
                "betrokkene_identifier": "betrokkene-bedrijf-7",
            },
            "subject_name": "test_org",
            "role": "Role",
            "subject_id": 800,
            "correlation_id": "req-1234",
        }

    @mock.patch(
        "zsnl_amqp_consumers.event_logger_subject_relation.get_subject_relation"
    )
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject_relation.get_case")
    def test_subject_relation_deleted(
        self, mock_get_case, mock_get_user, mock_get_subject_relation
    ):
        subject_relation_event = SubjectRelationDeleted()
        session = mock.MagicMock()
        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "SubjectRelation",
            "event_name": "SubjectRelationDeleted",
            "id": str(uuid4()),
            "entity_data": {
                "case": {"id": str(uuid4()), "type": "case"},
                "subject": {
                    "id": str(uuid4()),
                    "type": "employee",
                    "name": "beheerder",
                },
                "role": "Advocaat",
            },
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )
        mock_get_subject_relation.return_value = SubjectRelationInformation(
            id=654, subject_type="medewerker", subject_id=2
        )

        log_record = subject_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.zaak_id == 1234
        assert (
            log_record.onderwerp
            == "Betrokkene 'beheerder' verwijderd van zaak 1234 als Advocaat"
        )
        assert log_record.event_type == "case/subject/remove"
        assert log_record.event_data == {
            "case_id": 1234,
            "role": "Advocaat",
            "subject_name": "beheerder",
            "correlation_id": "req-1234",
        }


class TestEventLoggerCaseRelation:
    def test_case_base_generate_event_parameters(self):
        base = CaseRelationBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={},
                case_id=12345,
                case_relation_info=CaseRelationInformation(12, 13),
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_case_relation.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case_relation.get_case")
    def test_case_relation_deleted(self, mock_get_case, mock_get_user):
        case_relation_event = CaseRelationDeleted()
        session = mock.MagicMock()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "current_case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                }
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "CaseRelation",
            "event_name": "CaseRelationDeleted",
            "id": str(uuid4()),
            "entity_data": {"case_id_a": 10, "case_id_b": 12},
        }

        mock_get_case.return_value = CaseInformation(
            id=12, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="name", type="medewerker"
        )

        log_record = case_relation_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert log_record.onderwerp == "Relatie met zaak 10 verwijderd"
        assert log_record.event_type == "case/relation/remove"
        assert log_record.event_data == {
            "case_id": 12,
            "relation_id": 10,
            "correlation_id": "req-1234",
        }


class TestEventLoggerDocument:
    def test_document_base_generate_event_parameters(self):
        base = DocumentBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                case_id=None,
                entity_data={},
                document_info=DocumentInformation(
                    12, "test_doc", "application/pdf", 1
                ),
                user_info=UserInformation(
                    id=12, display_name="test_user", type="employee"
                ),
                intake_owner_info=None,
                intake_group_info=None,
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    def test_document_base_user_not_found(self, mock_get_user):
        base = DocumentBase()
        session = mock.MagicMock()
        event = {
            "changes": [
                {"key": "origin", "old_value": None, "new_value": "Intern"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.side_effect = NotFound

        log_record = base(session, event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_base_document_not_found(self, mock_get_document):
        base = DocumentBase()
        session = mock.MagicMock()
        event = {
            "changes": [
                {"key": "origin", "old_value": None, "new_value": "Intern"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.side_effect = NotFound

        log_record = base(session, event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_updated_event(
        self, mock_get_document, mock_get_user, mock_get_case
    ):
        document_event = DocumentUpdated()
        session = mock.MagicMock()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "description",
                    "old_value": None,
                    "new_value": "test_desc",
                },
                {"key": "origin", "old_value": None, "new_value": "Intern"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
                {
                    "key": "basename",
                    "old_value": None,
                    "new_value": "test_doc",
                },
                {
                    "key": "confidentiality",
                    "old_value": None,
                    "new_value": "Intern",
                },
                {
                    "key": "document_category",
                    "old_value": None,
                    "new_value": "Plan",
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        mock_get_case.return_value = CaseInformation(
            id=17, confidentiality="Public", uuid=case_uuid
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "Documenteigenschappen van 'test_doc.doc | 12' gewijzigd door 'test_user'."
        )
        assert log_record.event_type == "document/metadata/update"
        assert log_record.event_data == {
            "case_id": None,
            "file_id": 12,
            "file_name": "test_doc.doc",
            "version": 1,
            "metadata": {
                "description": "test_desc",
                "document_category": "Plan",
                "origin": "Intern",
                "origin_date": "2020-03-15",
                "trust_level": "Intern",
            },
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
            "user_name": "test_user",
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )

        event["changes"].append(
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        )

        log_record = document_event(session=session, event=event)

        assert log_record.event_type == "case/document/metadata/update"
        assert log_record.event_data["case_id"] == 17

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_deleted_event(
        self, mock_get_document, mock_get_user, mock_get_case
    ):
        document_event = DocumentDeleted()
        session = mock.MagicMock()

        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "domain": "zsnl_domains.document",
            "entity_data": {
                "destroy_reason": "Document verwijderd uit intake"
            },
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentDeleted",
            "id": "becf2afb-348c-44be-a507-68dbdd22614b",
            "user_uuid": str(uuid4()),
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        case_uuid = uuid4()
        mock_get_case.return_value = CaseInformation(
            id=20, confidentiality="public", uuid=case_uuid
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' verwijderd uit Documentintake door 'test_user'"
        )
        assert log_record.event_type == "document/delete_document"
        assert log_record.event_data == {
            "destroy_reason": "Document verwijderd uit intake",
            "case_id": None,
            "file_id": 12,
            "file_name": "test_doc.doc",
            "version": 1,
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
            "user_name": "test_user",
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )
        event["changes"].append(
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        )

        log_record = document_event(session=session, event=event)

        assert log_record.event_type == "case/document/delete_document"
        assert log_record.event_data["case_id"] == 20

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_deleted_event_no_reason_given_falls_back_to_default(
        self, mock_get_document, mock_get_user
    ):
        document_event = DocumentDeleted()
        session = mock.MagicMock()

        event = {
            "changes": [],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "domain": "zsnl_domains.document",
            "entity_data": {"destroy_reason": ""},
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentDeleted",
            "id": "becf2afb-348c-44be-a507-68dbdd22614b",
            "user_uuid": str(uuid4()),
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' verwijderd uit Documentintake door 'test_user'"
        )
        assert log_record.event_type == "document/delete_document"
        assert log_record.event_data == {
            "destroy_reason": "Niet opgegeven.",
            "case_id": None,
            "file_id": 12,
            "file_name": "test_doc.doc",
            "version": 1,
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
            "user_name": "test_user",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    def test_document_added_to_case_event(
        self, mock_get_user, mock_get_document, mock_get_case
    ):
        document_event = DocumentAddedToCase()
        session = mock.MagicMock()
        case_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "description",
                    "old_value": None,
                    "new_value": "test_desc",
                },
                {"key": "origin", "old_value": None, "new_value": "Inkomend"},
                {
                    "key": "origin_date",
                    "old_value": None,
                    "new_value": "2020-03-15",
                },
                {
                    "key": "case_uuid",
                    "old_value": None,
                    "new_value": case_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAddedToCase",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_case.return_value = CaseInformation(
            id=15, uuid=case_uuid, confidentiality="Intern"
        )

        event["changes"].append(
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        )
        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' toegevoegd aan zaak 15 door 'test_user'"
        )
        assert log_record.event_type == "case/document/assign"
        assert log_record.event_data == {
            "case_id": 15,
            "file_id": 12,
            "file_name": "test_doc.doc",
            "version": 1,
            "metadata": {
                "description": "test_desc",
                "origin": "Inkomend",
                "origin_date": "2020-03-15",
            },
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
            "user_name": "test_user",
        }

        mock_get_case.side_effect = NotFound
        log_record = document_event(session, event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_case")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    def test_document_created_event(
        self, mock_get_user, mock_get_document, mock_get_case
    ):
        document_event = DocumentCreated()
        session = mock.MagicMock()
        event = {
            "changes": [
                {
                    "key": "basename",
                    "old_value": None,
                    "new_value": "test_desc",
                },
                {"key": "extension", "old_value": None, "new_value": ".pdf"},
                {
                    "key": "store_uuid",
                    "old_value": None,
                    "new_value": uuid4(),
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=15, display_name="name", type="medewerker"
        )

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc", mimetype="application/pdf", version=1
        )

        case_uuid = uuid4()
        mock_get_case.return_value = CaseInformation(
            id=20, uuid=case_uuid, confidentiality="Intern"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert log_record.onderwerp == "Document 'test_doc' toegevoegd"
        assert log_record.event_type == "document/create"
        assert log_record.event_data == {
            "case_id": None,
            "file_id": 12,
            "file_name": "test_doc",
            "version": 1,
            "mimetype": "application/pdf",
            "correlation_id": "req-1234",
        }

        event["changes"].append(
            {"key": "case_uuid", "old_value": None, "new_value": case_uuid}
        )
        log_record = document_event(session=session, event=event)
        assert log_record.event_type == "case/document/create"
        assert log_record.event_data["case_id"] == 20

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_department")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_assigned_to_role_event(
        self,
        mock_get_document,
        mock_get_user,
        mock_get_department,
    ):
        document_event = DocumentAssignedToRole()
        session = mock.MagicMock()
        intake_group_uuid = uuid4()
        intake_role_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "intake_group_uuid",
                    "old_value": None,
                    "new_value": intake_group_uuid,
                },
                {
                    "key": "intake_role_uuid",
                    "old_value": None,
                    "new_value": intake_role_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignedToRole",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        mock_get_department.return_value = DepartmentInformation(
            id=17, name="development"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' toegewezen aan Afdeling 'development' door 'test_user'."
        )
        assert log_record.event_type == "document/assign"
        assert log_record.event_data == {
            "case_id": None,
            "file_id": 12,
            "correlation_id": "req-1234",
            "file_name": "test_doc.doc",
            "intake_group_name": "development",
            "user_name": "test_user",
        }

        mock_get_department.side_effect = NotFound
        log_record = document_event(session, event)
        assert log_record is None

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_assigned_to_user_event(
        self, mock_get_document, mock_get_user
    ):
        document_event = DocumentAssignedToUser()
        session = mock.MagicMock()
        intake_owner_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "intake_owner_uuid",
                    "old_value": None,
                    "new_value": intake_owner_uuid,
                }
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignedToUser",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc | 12' toegewezen aan 'test_user' door 'test_user'."
        )
        assert log_record.event_type == "document/assign"
        assert log_record.event_data == {
            "case_id": None,
            "correlation_id": "req-1234",
            "file_id": 12,
            "file_name": "test_doc.doc",
            "intake_owner_name": "test_user",
            "user_name": "test_user",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_document.get_document")
    def test_document_rejected_by_user_event(
        self, mock_get_document, mock_get_user
    ):
        document_event = DocumentAssignmentRejected()
        session = mock.MagicMock()
        intake_owner_uuid = uuid4()
        event = {
            "changes": [
                {
                    "key": "intake_owner_uuid",
                    "old_value": None,
                    "new_value": intake_owner_uuid,
                },
                {
                    "key": "rejection_reason",
                    "new_value": "Rejecting this document as it is not required.",
                    "old_value": None,
                },
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.document",
            "entity_id": str(uuid4()),
            "entity_type": "Document",
            "event_name": "DocumentAssignmentRejected",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_document.return_value = DocumentInformation(
            id=12, name="test_doc.doc", mimetype="application/pdf", version=1
        )
        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        log_record = document_event(session=session, event=event)

        assert log_record.component == "document"
        assert (
            log_record.onderwerp
            == "'test_doc.doc' afgewezen in de documentintake door 'test_user'."
        )
        assert log_record.event_type == "document/assignment/reject"
        assert log_record.event_data == {
            "correlation_id": "req-1234",
            "file_name": "test_doc.doc",
            "user_name": "test_user",
        }


class TestEventLoggerCustomObject:
    def test_case_base_generate_event_parameters(self):
        base = CustomObjectBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={},
                custom_object_info=CustomObjectInformation,
                user_info=UserInformation,
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_created_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectCreated()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [
                {"key": "name", "new_value": "CustObj1", "old_value": None},
                {"key": "title", "new_value": "CustObj1", "old_value": None},
                {"key": "uuid", "new_value": uuid4(), "old_value": None},
                {"key": "status", "new_value": "active", "old_value": None},
                {"key": "version", "new_value": 1, "old_value": None},
                {
                    "key": "date_created",
                    "new_value": "2020-10-12T09:02:21.597690+00:00",
                    "old_value": None,
                },
                {
                    "key": "last_modified",
                    "new_value": "2020-10-12T09:02:21.597690+00:00",
                    "old_value": None,
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectCreated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object CustObj1 van objecttype CustObjType1 is aangemaakt."
        )
        assert log_record.event_type == "custom_object/created"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_updated_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectUpdated()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [
                {"key": "cases", "new_value": uuid4(), "old_value": uuid4()},
                {"key": "uuid", "new_value": uuid4(), "old_value": uuid4()},
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object CustObj1 van objecttype CustObjType1 is aangepast."
        )
        assert log_record.event_type == "custom_object/updated"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_related_to_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectRelatedTo()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [
                {"key": "cases", "new_value": uuid4(), "old_value": None}
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectRelatedTo",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object CustObj1 van objecttype CustObjType1 is gerelateerd."
        )
        assert log_record.event_type == "custom_object/relatedTo"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_unrelated_from_event(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectUnrelatedFrom()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [
                {"key": "cases", "new_value": None, "old_value": uuid4()}
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectUnrelatedFrom",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            custom_object_info=CustomObjectInformation(
                title="CustObj1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Relatie verwijderd voor Object CustObj1 van objecttype CustObjType1."
        )
        assert log_record.event_type == "custom_object/unrelatedFrom"
        assert log_record.event_data == {
            "custom_object_name": "CustObj1",
            "custom_object_type": "CustObjType1",
            "custom_object_uuid": custom_object_uuid,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    def test_custom_object_deleted_from_event(self, mock_get_user):
        custom_object_event = CustomObjectDeleted()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "title",
                    "new_value": "custom_object_1",
                    "old_value": "custom_object_1",
                },
                {
                    "key": "version_independent_uuid",
                    "new_value": None,
                    "old_value": custom_object_uuid,
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "CustomObject",
            "event_name": "CustomObjectDeleted",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_event.generate_event_parameters(
            entity_data={},
            custom_object_info=CustomObjectInformation(
                title="custom_object_1",
                custom_object_type="CustObjType1",
                custom_object_uuid=custom_object_uuid,
            ),
            user_info=UserInformation(
                id=15, display_name="test_user", type="medewerker"
            ),
        )
        assert event_params == {
            "custom_object_name": "custom_object_1",
            "username": "test_user",
            "custom_object_uuid": custom_object_uuid,
        }
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "Object custom_object_1 verwijderd door test_user."
        )
        assert log_record.event_type == "custom_object/deleted"
        assert log_record.event_data == {
            "custom_object_name": "custom_object_1",
            "username": "test_user",
            "custom_object_uuid": custom_object_uuid,
        }


class TestEventLoggerCustomObjectType:
    def test_case_base_generate_event_parameters(self):
        base = CustomObjectTypeBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={},
                custom_object_type_info=CustomObjectTypeInformation,
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object_type.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object_type.get_custom_object_type"
    )
    def test_custom_object_type_created_event(
        self, mock_get_custom_object_type, mock_get_user
    ):
        custom_object_type_event = CustomObjectTypeCreated()
        session = mock.MagicMock()

        event = {
            "changes": [
                {
                    "key": "name",
                    "new_value": "CustObjType1",
                    "old_value": None,
                },
                {"key": "uuid", "new_value": uuid4(), "old_value": None},
                {
                    "key": "title",
                    "new_value": "CustObjType1",
                    "old_value": None,
                },
                {"key": "status", "new_value": "active", "old_value": None},
                {"key": "version", "new_value": 1, "old_value": None},
                {
                    "key": "date_created",
                    "new_value": "2020-11-05T14:24:46.321472+00:00",
                    "old_value": None,
                },
                {
                    "key": "audit_log",
                    "new_value": {
                        "description": "test",
                        "updated_components": [
                            "custom_fields",
                            "authorizations",
                        ],
                    },
                    "old_value": None,
                },
            ],
            "context": "dev.zaaksysteem.nl",
            "correlation_id": str(uuid4()),
            "created_date": "2020-11-05T14:24:46.322418",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "CustomObjectType",
            "event_name": "CustomObjectTypeCreated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_custom_object_type.return_value = CustomObjectTypeInformation(
            id=101, name="CustObjType1"
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_type_event.generate_event_parameters(
            entity_data={},
            custom_object_type_info=CustomObjectTypeInformation(
                id=101, name="CustObjType1"
            ),
        )
        assert event_params == {
            "custom_object_type": "CustObjType1",
        }
        log_record = custom_object_type_event(session=session, event=event)

        assert log_record.component == "custom_object_type"
        assert log_record.onderwerp == "Objecttype 'CustObjType1' aangemaakt."
        assert log_record.event_type == "custom_object_type/created"
        assert log_record.event_data == {
            "custom_object_type": "CustObjType1",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object_type.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object_type.get_custom_object_type"
    )
    def test_custom_object_type_updated_event(
        self, mock_get_custom_object_type, mock_get_user
    ):
        custom_object_type_event = CustomObjectTypeUpdated()
        session = mock.MagicMock()

        event = {
            "changes": [
                {
                    "key": "uuid",
                    "new_value": str(uuid4()),
                    "old_value": str(uuid4()),
                },
                {
                    "key": "name",
                    "new_value": "CustObjType1",
                    "old_value": "CustObjType",
                },
                {
                    "key": "audit_log",
                    "new_value": {
                        "description": "Attributes Updated",
                        "updated_components": [
                            "custom_fields",
                            "authorizations",
                        ],
                    },
                    "old_value": {
                        "description": "Attributes Set",
                        "updated_components": [
                            "custom_fields",
                            "authorizations",
                        ],
                    },
                },
            ],
            "context": "context",
            "correlation_id": str(uuid4()),
            "created_date": "2020-11-05T14:27:08.977004",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "CustomObjectType",
            "event_name": "CustomObjectTypeUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_custom_object_type.return_value = CustomObjectTypeInformation(
            id=101, name="CustObjType1"
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        entity_data = {
            "audit_log": {
                "description": "Updating custom fields",
                "updated_components": ["custom_fields", "authorizations"],
            }
        }
        event_params = custom_object_type_event.generate_event_parameters(
            entity_data=entity_data,
            custom_object_type_info=CustomObjectTypeInformation(
                id=101, name="CustObjType1"
            ),
        )
        assert event_params == {
            "custom_object_type": "CustObjType1",
            "updated_component_list": "Kenmerken,Rechten",
        }
        log_record = custom_object_type_event(session=session, event=event)

        assert log_record.component == "custom_object_type"
        assert (
            log_record.onderwerp
            == "Objecttype 'CustObjType1' gewijzigd (Kenmerken,Rechten)."
        )
        assert log_record.event_type == "custom_object_type/updated"
        assert log_record.event_data == {
            "custom_object_type": "CustObjType1",
            "updated_component_list": "Kenmerken,Rechten",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object_type.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object_type.get_custom_object_type"
    )
    def test_custom_object_type_deleted_event(
        self, mock_get_custom_object_type, mock_get_user
    ):
        custom_object_type_event = CustomObjectTypeDeleted()
        session = mock.MagicMock()

        event = {
            "changes": [],
            "context": "context",
            "correlation_id": str(uuid4()),
            "created_date": "2020-11-05T21:32:08.482378",
            "domain": "zsnl_domains.case_management",
            "entity_data": {},
            "entity_id": str(uuid4()),
            "entity_type": "CustomObjectType",
            "event_name": "CustomObjectTypeDeleted",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_custom_object_type.return_value = CustomObjectTypeInformation(
            id=101, name="CustObjType1"
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = custom_object_type_event.generate_event_parameters(
            entity_data={},
            custom_object_type_info=CustomObjectTypeInformation(
                id=101, name="CustObjType1"
            ),
        )
        assert event_params == {
            "custom_object_type": "CustObjType1",
        }
        log_record = custom_object_type_event(session=session, event=event)

        assert log_record.component == "custom_object_type"
        assert log_record.onderwerp == "Objecttype 'CustObjType1' verwijderd."
        assert log_record.event_type == "custom_object_type/deleted"
        assert log_record.event_data == {
            "custom_object_type": "CustObjType1",
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_custom_object.get_user")
    @mock.patch(
        "zsnl_amqp_consumers.event_logger_custom_object.get_custom_object"
    )
    def test_custom_object_updated_event_status(
        self, mock_get_custom_object, mock_get_user
    ):
        custom_object_event = CustomObjectUpdated()
        session = mock.MagicMock()
        custom_object_uuid = str(uuid4())

        event = {
            "changes": [
                {
                    "key": "status",
                    "new_value": "active",
                    "old_value": "inactive",
                },
            ],
            "context": "context",
            "correlation_id": "test-123",
            "created_date": "2020-10-12T09:02:21.597690+00:00",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": custom_object_uuid,
            "entity_type": "CustomObject",
            "event_name": "CustomObjectUpdated",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_custom_object.return_value = CustomObjectInformation(
            title="CustObj1",
            custom_object_type="CustObjType1",
            custom_object_uuid=custom_object_uuid,
        )

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )
        log_record = custom_object_event(session=session, event=event)

        assert log_record.component == "custom_object"
        assert (
            log_record.onderwerp
            == "De status van object CustObj1 van objecttype CustObjType1 is aangepast naar actief."
        )


class TestEventLoggerSubject:
    def test_case_base_generate_event_parameters(self):
        base = SubjectBase()
        with pytest.raises(NotImplementedError):
            base.generate_event_parameters(
                entity_data={}, subject_info=PersonInformation, uuid=UUID
            )

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_bsn_retrieved_event(self, mock_get_person, mock_get_user):
        person_event = BsnRetrieved()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "BsnRetrieved",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "person_a"},
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Inzage verleend op betrokkene 'person_a', veld 'bsn'."
        )
        assert log_record.event_type == "subject/inspect"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_bsn_updated_non_authentic_contact_event(
        self, mock_get_person, mock_get_user
    ):
        person_event = NonAuthenticBsnUpdated()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "bsn",
                    "new_value": "111",
                    "old_value": "",
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "NonAuthenticBsnUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "person_a"},
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert log_record.onderwerp == "BSN van 'person_a' gewijzigd."
        assert log_record.event_type == "subject/update"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_non_authentic_contact_updated_event(
        self, mock_get_person, mock_get_user
    ):
        person_event = PersonUpdated()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "first_name",
                    "new_value": "name",
                    "old_value": "",
                },
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "PersonUpdated",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }

        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={"name": "person_a"},
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }
        log_record = person_event(session=session, event=event)

        assert log_record.component == "betrokkene"
        assert log_record.onderwerp == "Info van 'person_a' gewijzigd."
        assert log_record.event_type == "subject/update"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
        }

    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_case.get_case")
    def test_case_status_set_stalled(self, mock_get_case, mock_get_user):
        case_event = CaseStatusSet()
        session = mock.MagicMock()
        event = {
            "changes": [
                {"key": "status", "old_value": "open", "new_value": "stalled"}
            ],
            "context": "context",
            "correlation_id": "req-1234",
            "created_date": "2019-02-25",
            "user_uuid": str(uuid4()),
            "domain": "zsnl_domains.case_management",
            "entity_id": str(uuid4()),
            "entity_type": "Case",
            "event_name": "CaseStatusSet",
            "id": str(uuid4()),
            "entity_data": {},
        }

        mock_get_case.return_value = CaseInformation(
            id=1234, confidentiality="public", uuid=uuid4()
        )
        mock_get_user.return_value = UserInformation(
            id=654, display_name="name", type="medewerker"
        )

        log_record = case_event(session=session, event=event)

        assert log_record.component == "zaak"
        assert (
            log_record.onderwerp
            == "Status voor zaak: 1234 gewijzigd naar: opgeschort"
        )
        assert log_record.event_type == "case/update/status"

    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_user")
    @mock.patch("zsnl_amqp_consumers.event_logger_subject.get_person")
    def test_contact_info_update_event(self, mock_get_person, mock_get_user):
        person_event = ContactInformationSaved()
        session = mock.MagicMock()
        entity_id = str(uuid4())
        event = {
            "changes": [
                {
                    "key": "contact_information",
                    "new_value": {
                        "email": "person_a@gmail.com",
                        "internal_note": "phone",
                        "is_an_anonymous_contact_person": False,
                        "mobile_number": "0652387284",
                        "phone_number": "0624844247",
                        "preferred_contact_channel": "email",
                    },
                    "old_value": {
                        "email": "person_a_1@gmail.com",
                        "internal_note": "phone",
                        "is_an_anonymous_contact_person": False,
                        "mobile_number": "0632387284",
                        "phone_number": "0624844247",
                        "preferred_contact_channel": "email",
                    },
                }
            ],
            "correlation_id": "3efaea64-9ff4-4c31-a70a-77e247c83643",
            "created_date": "2021-03-28T16:41:28.572492",
            "entity_data": {"name": "person_a"},
            "entity_id": entity_id,
            "entity_type": "Person",
            "event_name": "ContactInformationSaved",
            "id": str(uuid4()),
            "user_uuid": str(uuid4()),
        }
        mock_get_person.return_value = PersonInformation(id=101, type="Person")

        mock_get_user.return_value = UserInformation(
            id=15, display_name="test_user", type="medewerker"
        )

        event_params = person_event.generate_event_parameters(
            entity_data={
                "name": "person_a",
                "contact_information": {
                    "email": "person_a@gmail.com",
                    "internal_note": "phone",
                    "is_an_anonymous_contact_person": False,
                    "mobile_number": "0652387284",
                    "phone_number": "0624844247",
                    "preferred_contact_channel": "email",
                },
                "contact_information_old": {
                    "email": "person_a_1@gmail.com",
                    "internal_note": "phone",
                    "is_an_anonymous_contact_person": False,
                    "mobile_number": "0632387284",
                    "phone_number": "0624844247",
                    "preferred_contact_channel": "email",
                },
            },
            subject_info=PersonInformation(id=101, type="Person"),
            uuid=event["entity_id"],
        )

        assert event_params == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
            "updated_fields": "e-mailadres, telefoonnummer (mobiel)",
        }
        log_record = person_event(session=session, event=event)
        assert log_record.component == "betrokkene"
        assert (
            log_record.onderwerp
            == "Plusgegevens voor betrokkene 'person_a' gewijzigd: e-mailadres, telefoonnummer (mobiel)"
        )
        assert log_record.event_type == "subject/update_contact_data"

        assert log_record.event_data == {
            "name": "person_a",
            "betrokkene_identifier": "betrokkene-Person-101",
            "uuid": entity_id,
            "updated_fields": "e-mailadres, telefoonnummer (mobiel)",
        }
