## Framework
minty~=3.1
minty-amqp~=4.1
python-json-logger

##  Project specific requirements

requests~=2.22
amqpstorm~=2.7
psycopg2

../domains