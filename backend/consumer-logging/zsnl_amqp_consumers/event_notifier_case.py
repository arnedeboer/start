# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from . import repository
from .event_notifier_base import BaseNotifier
from minty.exceptions import NotFound
from zsnl_domains.database.schema import Message


class CaseBase(BaseNotifier):
    def __call__(self, session, event, logging_id) -> None | Message:
        """Collect and necessary information before creating log record."""

        formatted_entity_data = self.format_entity_data(event=event)
        case_uuid = formatted_entity_data["case"]["id"]
        case = repository.get_case(session, case_uuid)
        formatted_entity_data["case_id"] = case.id
        try:
            user_info = repository.get_contact_by_uuid(
                session=session,
                contact_uuid=formatted_entity_data["subject"]["id"],
            )
        except NotFound:
            return

        if "permission" in formatted_entity_data and formatted_entity_data[
            "permission"
        ] in [
            "read",
            "write",
            "search",
        ]:
            return self._create_message_notifier_record(
                assignee_info=user_info,
                subject=self._generate_subject(formatted_entity_data),
                logging_id=logging_id,
                is_read=False,
                is_archived=False,
            )
        else:
            self.logger.debug(
                "Not creating notification as the user did not get extra permissions"
            )
            return

    def _generate_subject(self, entity_data: dict) -> str:
        """Generate subject line formatted with parameters from decoded message."""

        raise NotImplementedError


class SubjectRelationCreated(CaseBase):
    """Generate subject line for SubjectRelationCreated event"""

    def _generate_subject(self, entity_data: dict) -> str:
        name = entity_data["subject"]["name"]
        role = entity_data["role"]
        case_id = entity_data["case_id"]

        return f"Betrokkene '{name}' toegevoegd aan zaak {case_id} als {role}"
