# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import PersonInformation, get_person, get_user
from uuid import UUID
from zsnl_domains.database.schema import Logging

MAP_UPDATED_FIELD_NAMES = {
    "phone_number": "telefoonnummer",
    "mobile_number": "telefoonnummer (mobiel)",
    "internal_note": "interne notitie",
    "email": "e-mailadres",
}


class SubjectBase(BaseLogger):
    variables = None
    event_type = None
    subject = None

    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record."""

        if event["entity_type"] == "Person":
            subject_info = get_person(session=session, uuid=event["entity_id"])

        user_info = get_user(session=session, uuid=event["user_uuid"])

        formatted_entity_data = self.format_entity_data(event=event)
        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            subject_info=subject_info,
            uuid=event["entity_id"],
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component="betrokkene",
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            created_for=event_parameters.get("betrokkene_identifier", None),
            event_data=event_parameters,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message."""
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        raise NotImplementedError


class BsnRetrieved(SubjectBase):
    subject = "Inzage verleend op betrokkene '{}', veld 'bsn'."
    variables = ["name"]
    event_type = "subject/inspect"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""
        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
        }
        return event_parameters


class PersonUpdated(SubjectBase):
    subject = "Info van '{}' gewijzigd."
    variables = ["name"]
    event_type = "subject/update"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""
        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
        }
        return event_parameters


class NonAuthenticBsnUpdated(SubjectBase):
    subject = "BSN van '{}' gewijzigd."
    variables = ["name"]
    event_type = "subject/update"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""
        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
        }
        return event_parameters


class ContactInformationSaved(SubjectBase):
    subject = "Plusgegevens voor betrokkene '{}' gewijzigd: {}"
    variables = ["name", "updated_fields"]
    event_type = "subject/update_contact_data"

    def generate_event_parameters(
        self,
        entity_data: dict,
        subject_info: PersonInformation,
        uuid: UUID,
    ):
        """Generate event parameters from entity data"""
        updated_fields = [
            MAP_UPDATED_FIELD_NAMES[key]
            for key in entity_data["contact_information"]
            if entity_data["contact_information"][key]
            != entity_data["contact_information_old"][key]
        ]
        updated_fields = ", ".join(updated_fields)

        event_parameters = {
            "name": entity_data["name"],
            "betrokkene_identifier": f"betrokkene-{subject_info.type}-{subject_info.id}",
            "uuid": uuid,
            "updated_fields": updated_fields,
        }
        return event_parameters
