# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
from collections import namedtuple
from datetime import datetime
from minty.exceptions import NotFound
from sqlalchemy import sql, types
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from uuid import UUID
from zsnl_domains.database.schema import (
    Bedrijf,
    BibliotheekCategorie,
    BibliotheekKenmerk,
    BibliotheekNotificatie,
    BibliotheekSjabloon,
    Case,
    CustomObject,
    CustomObjectType,
    CustomObjectTypeVersion,
    CustomObjectVersion,
    File,
    Filestore,
    Group,
    NatuurlijkPersoon,
    ObjectBibliotheekEntry,
    ObjectData,
    Role,
    Subject,
    Thread,
    ZaakBetrokkenen,
    Zaaktype,
    ZaaktypeNode,
    ZaaktypeResultaten,
)

logger = logging.getLogger(__name__)


def format_date_from_iso(date: str) -> str:
    """format date from ISO format to NL format.

    :param date: format (2019-01-23)
    :type date: str
    :return: format (23-01-2019)
    :rtype: str
    """
    ISO_format = "%Y-%m-%d"
    NL_format = "%d-%m-%Y"
    d = datetime.strptime(date, ISO_format)
    return d.strftime(NL_format)


UserInformation = namedtuple("UserInformation", "id type display_name")

CaseInformation = namedtuple("CaseInformation", "id confidentiality uuid")

CaseTypeInformation = namedtuple("CaseTypeInformation", "id title")

CaseTypeVersionInformation = namedtuple(
    "CaseTypeVersionInformation", "case_type_id title version"
)

CatalogFolderInformation = namedtuple(
    "CatalogFolderInformation", "id uuid name"
)

ObjectTypeInformation = namedtuple("ObjectTypeInformation", "id title")

AttributeInformation = namedtuple(
    "AttributeInformation", "id name magic_string"
)

EmailTemplateInformation = namedtuple("EmailTemplateInformation", "id label")
DocumentTemplateInformation = namedtuple(
    "DocumentTemplateInformation", "id name"
)

Contact = namedtuple("Contact", "id type")

SubjectRelationInformation = namedtuple(
    "SubjectRelationInformation", "id subject_id subject_type"
)

DepartmentInformation = namedtuple("Department", "id name")

RoleInformation = namedtuple("Role", "id name")

CaseRelationInformation = namedtuple("CaseRelation", "case_id_a case_id_b")

DocumentInformation = namedtuple("Document", "id name mimetype version")

SubjectInformation = namedtuple("SubjectInformation", "id display_name uuid")

FileInformation = namedtuple("FileInformation", "id name")

CustomObjectInformation = namedtuple(
    "CustomObjectInformation", "title custom_object_type custom_object_uuid"
)

CustomObjectTypeInformation = namedtuple(
    "CustomObjectTypeInformation", "id name"
)

PersonInformation = namedtuple("PersonInformation", "id type")


def get_case(session, uuid: UUID) -> CaseInformation:
    """Get case from database for given uuid.

    :param session: session
    :type session: Sqlalchemy session
    :param uuid: uuid
    :type uuid: UUID
    :raises NoResultFound: no case record found
    :return: case information
    :rtype: CaseInformation
    """
    try:
        case = session.query(Case).filter(Case.uuid == uuid).one()
    except NoResultFound as err:
        raise NotFound(f"No case was found with UUID: '{uuid}'") from err
    case_info = CaseInformation(
        id=case.id, confidentiality=case.confidentiality, uuid=uuid
    )
    return case_info


def get_user(session, uuid: UUID) -> UserInformation:
    """Get user from database for given uuid.

    :param session: session
    :type session: Sqlalchemy session
    :param uuid: uuid
    :type uuid: UUID
    :raises NoResultFound: no user record found
    :return: user information
    :rtype: UserInformation
    """
    person = (
        sql.select(
            NatuurlijkPersoon.id,
            sql.literal("natuurlijk_persoon").label("type"),
            (
                NatuurlijkPersoon.voornamen
                + " "
                + NatuurlijkPersoon.naamgebruik
            ).label("display_name"),
        )
        .select_from(NatuurlijkPersoon)
        .where(sql.and_(NatuurlijkPersoon.uuid == uuid))
    )

    company = sql.select(
        Bedrijf.id,
        sql.literal("bedrijf").label("type"),
        Bedrijf.handelsnaam.label("display_name"),
    ).where(sql.and_(Bedrijf.uuid == uuid))

    subject = sql.select(
        Subject.id,
        sql.literal("medewerker").label("type"),
        sql.cast(Subject.properties, types.JSON)
        .op("->>")("displayname")
        .label("display_name"),
    ).where(sql.and_(Subject.uuid == uuid, Subject.subject_type == "employee"))

    query = sql.union_all(person, company, subject)
    user_row = session.execute(query).fetchone()

    if not user_row:
        raise NotFound(
            f"No user was found with UUID: '{uuid}'", "user/not_found"
        )

    user_info = UserInformation(
        id=user_row.id,
        type=user_row.type,
        display_name=user_row.display_name,
    )

    return user_info


def get_contact_by_uuid(session, contact_uuid: UUID):
    """Search from tables(Bedrijf, NatuurlijkPersoon and Subject) for a contact.

    :param session: session
    :type session: Sqlalchemy session
    :param contact_uuid: The uuid keyword to retrieve a contact.
    :type contact_uuid: string
    :return: SQLAlchemy query for one Contact row
    """

    natural_person = (
        sql.select(
            NatuurlijkPersoon.id,
            sql.literal("person").label("type"),
            NatuurlijkPersoon.voornamen + " " + NatuurlijkPersoon.naamgebruik,
        )
        .select_from(NatuurlijkPersoon)
        .where(sql.and_(NatuurlijkPersoon.uuid == contact_uuid))
    )

    company = sql.select(
        Bedrijf.id,
        sql.literal("company").label("type"),
        Bedrijf.handelsnaam.label("display_name"),
    ).where(sql.and_(Bedrijf.uuid == contact_uuid))

    subject = sql.select(
        Subject.id,
        Subject.subject_type.label("type"),
        sql.cast(Subject.properties, types.JSON)
        .op("->>")("displayname")
        .label("display_name"),
    ).where(
        sql.and_(
            Subject.uuid == contact_uuid, Subject.subject_type == "employee"
        )
    )

    query = sql.union_all(natural_person, company, subject)

    contact_row = session.execute(query).fetchone()

    if not contact_row:
        return Contact(None, None)

    return Contact(contact_row.id, contact_row.type)


def get_contact_from_thread(session, thread_uuid: UUID) -> Contact:
    """Get user, company or person from subject table for given thread uuid,
    which this subject is present in the thread or not.

    :param session: session
    :type session: Sqlalchemy session
    :param thread_uuid: uuid
    :type thread_uuid: UUID
    :raises NoResultFound: no user record found
    :return: Contact information
    :rtype: Contact
    """
    try:
        thread = (
            session.query(Thread.contact_uuid)
            .filter(Thread.uuid == thread_uuid)
            .one()
        )
    except NoResultFound:
        return Contact(None, None)

    return get_contact_by_uuid(
        session=session, contact_uuid=thread.contact_uuid
    )


def get_case_from_thread(session, thread_uuid: UUID) -> CaseInformation:
    """Get a case from the case id in a thread. Given that a thead is connected to a Case.

    :param session: session
    :type session: Sqlalchemy session
    :param thread_uuid: uuid
    :type thread_uuid: UUID
    :return: case information
    :rtype: CaseInformation
    """
    try:
        case = (
            session.query(Case.id, Case.confidentiality, Case.uuid)
            .filter(Thread.uuid == thread_uuid)
            .filter(Case.id == Thread.case_id)
            .one()
        )
    except NoResultFound:
        return CaseInformation(id=None, confidentiality="public", uuid=None)

    return CaseInformation(
        id=case.id, confidentiality=case.confidentiality, uuid=case.uuid
    )


def get_case_type(session, uuid: UUID) -> CaseTypeInformation:
    """Get case_type from database for given uuid.

    :param session: session
    :type session: Sqlalchemy session
    :param uuid: uuid
    :type uuid: UUID
    :raises NoResultFound: no case record found
    :return: case_type information
    :rtype: CaseTypeInformation
    """
    try:
        case_type = (
            session.query(Zaaktype)
            .filter(ObjectData.uuid == uuid)
            .filter(ObjectData.object_class == "casetype")
            .filter(Zaaktype.id == ObjectData.object_id)
        ).one()
    except NoResultFound:
        case_type = None

    if case_type is not None:
        case_type_info = CaseTypeInformation(
            id=case_type.id, title=case_type.current_zaaktype_node.titel
        )
    else:
        case_type_info = CaseTypeInformation(id=0, title="Onbekend zaaktype")

    return case_type_info


def get_case_type_version(session, uuid: UUID) -> CaseTypeVersionInformation:
    """Get case_type from database for given uuid.

    :param session: session
    :type session: Sqlalchemy session
    :param uuid: uuid
    :type uuid: UUID
    :raises NoResultFound: no case record found
    :return: case_type information
    :rtype: CaseTypeInformation
    """
    try:
        case_type_version = (
            session.query(
                ZaaktypeNode.zaaktype_id,
                ZaaktypeNode.titel,
                ZaaktypeNode.version,
            ).filter(ZaaktypeNode.uuid == uuid)
        ).one()
    except NoResultFound as err:
        raise NotFound(
            f"No case_type_version found with UUID: '{uuid}'"
        ) from err
    case_type_version_info = CaseTypeVersionInformation(
        case_type_id=case_type_version.zaaktype_id,
        title=case_type_version.titel,
        version=case_type_version.version,
    )
    return case_type_version_info


def get_catalog_folder(session, id) -> CatalogFolderInformation:
    """Get catalog folder with the specified uuid from database.

    :param session: session
    :type session: Sqlalchemy session
    :param id: folder id
    :type id: int
    :raises NoResultFound: no case record found
    :return: catalog_folder information
    :rtype: CatalogFolderInformation
    """
    try:
        catalog_folder = (
            session.query(BibliotheekCategorie).filter(
                BibliotheekCategorie.id == id
            )
        ).one()
    except NoResultFound as err:
        raise NotFound(f"No catalog folder found with id: '{id}'") from err

    catalog_folder_info = CatalogFolderInformation(
        id=catalog_folder.id,
        uuid=catalog_folder.uuid,
        name=catalog_folder.naam,
    )
    return catalog_folder_info


def get_object_type(session, uuid: UUID) -> ObjectTypeInformation:
    """Get catalog object type with the specified uuid from database.

    :param session: session
    :type session: Sqlalchemy session
    :param id: entity id
    :type id: int
    :raises NoResultFound: no case record found
    :return: object_type_info information
    :rtype: EntityInformation
    """
    try:
        object_type = (
            session.query(
                ObjectBibliotheekEntry.id, ObjectBibliotheekEntry.name
            ).filter(ObjectBibliotheekEntry.object_uuid == uuid)
        ).one()
    except NoResultFound as err:
        raise NotFound(f"No object type found with uuid: '{uuid}'") from err

    object_type_info = ObjectTypeInformation(
        id=object_type.id, title=object_type.name
    )
    return object_type_info


def get_attribute(session, uuid: UUID) -> AttributeInformation:
    """Get attribute information from database.

    :param session: session
    :type session: Sqlalchemy session
    :param uuid: attribute uuid
    :type uuid: UUID
    :raises NotFound: no attribute record found
    :return: Information about attribute
    :rtype: AttributeInformation
    """
    try:
        attribute = (
            session.query(BibliotheekKenmerk).filter(
                BibliotheekKenmerk.uuid == uuid
            )
        ).one()
    except NoResultFound as err:
        raise NotFound(
            f"Attribute with uuid {uuid} not found", "attribute/not_found"
        ) from err

    attribute_info = AttributeInformation(
        id=attribute.id,
        name=attribute.naam,
        magic_string=attribute.magic_string,
    )

    return attribute_info


def get_attribute_by_magic_string(
    session, magic_string: str
) -> AttributeInformation:
    """Get attribute information from database.
    :raises NotFound: no attribute record found
    :return: Information about attribute
    """
    try:
        attribute = (
            session.query(BibliotheekKenmerk).filter(
                BibliotheekKenmerk.magic_string == magic_string
            )
        ).one()
    except NoResultFound as err:
        raise NotFound(
            f"Attribute with magic_string {magic_string} not found",
            "attribute/not_found",
        ) from err

    return AttributeInformation(
        id=attribute.id,
        name=attribute.naam,
        magic_string=attribute.magic_string,
    )


def get_email_template(session, uuid: UUID) -> EmailTemplateInformation:
    """Get email_template info from database

    :param session: session
    :type session: Sqlalchemy session
    :param uuid: email_template uuid
    :type uuid: UUID
    :raises NotFound: email_template not found
    :return: email_template info
    :rtype: EmailTemplateInformation
    """
    try:
        result = (
            session.query(BibliotheekNotificatie).filter(
                BibliotheekNotificatie.uuid == uuid
            )
        ).one()
    except NoResultFound:
        logger.warning(f"Emailtemplate with uuid: {uuid} not found")
        return
    except MultipleResultsFound:
        logger.warning(f"Multiple Emailtemplates with uuid: {uuid} found?!")
        return

    email_template = EmailTemplateInformation(id=result.id, label=result.label)
    return email_template


def get_document_template(session, uuid: UUID) -> DocumentTemplateInformation:
    """Get document template info from database

    :param session: session
    :type session: Sqlalchemy session
    :param uuid: document_template uuid
    :type uuid: UUID
    :raises NotFound: document_template not found
    :return: document_template info
    :rtype: DocumentTemplateInformation
    """
    try:
        result = (
            session.query(BibliotheekSjabloon).filter(
                BibliotheekSjabloon.uuid == uuid
            )
        ).one()
    except NoResultFound as err:
        raise NotFound(
            f"Document template with uuid: {uuid} not found",
            "document_template/not_found",
        ) from err
    document_template = DocumentTemplateInformation(
        id=result.id, name=result.naam
    )
    return document_template


def get_subject_relation(session, uuid: UUID):
    """Get subject_relation for a case(ZaakBetrokkene) by uuid.

    :param session: session
    :type session: session
    :param uuid: UUID of subject_relation
    :type uuid: UUID
    :raises NotFound: When subject_relation with uuid not found
    :return: subject_relation info
    :rtype: SubjectRelationInformation
    """
    try:
        result = (
            session.query(ZaakBetrokkenen)
            .filter(ZaakBetrokkenen.uuid == uuid)
            .one()
        )
    except NoResultFound as err:
        raise NotFound(
            f"Subject relation with uuid: {uuid} not found",
            "subject_relation/not_found",
        ) from err

    subject_relation = SubjectRelationInformation(
        id=result.id,
        subject_id=result.gegevens_magazijn_id,
        subject_type=result.betrokkene_type,
    )
    return subject_relation


def get_department(session, uuid: UUID):
    """Get department by UUID.

    :param session: database session
    :type session: session
    :param uuid: uuid of the department.
    :type uuid: UUID
    :raises NotFound: When department with UUID not found
    :return: department_info
    :rtype: DepartmentInformation
    """
    try:
        result = session.query(Group).filter(Group.uuid == uuid).one()
    except NoResultFound as err:
        raise NotFound(
            f"Department with uuid: {uuid} not found", "department/not_found"
        ) from err

    department = DepartmentInformation(id=result.id, name=result.name)
    return department


def get_role(session, uuid: UUID):
    """Get role by UUID.

    :param session: database session
    :type session: session
    :param uuid: uuid of the role.
    :type uuid: UUID
    :raises NotFound: When role with UUID not found
    :return: role_info
    :rtype: RoleInformation
    """
    try:
        result = session.query(Role).filter(Role.uuid == uuid).one()
    except NoResultFound as err:
        raise NotFound(
            f"Role with uuid: {uuid} not found", "role/not_found"
        ) from err

    role = RoleInformation(id=result.id, name=result.name)
    return role


def get_document(session, uuid: UUID):
    """Get document by UUID.

    :param session: database session
    :type session: session
    :param uuid: uuid of the document.
    :type uuid: UUID
    :raises NotFound: When document with UUID not found
    :return: role_info
    :rtype: DocumentInformation
    """

    query = sql.select(
        File.id, File.name, File.extension, File.version, Filestore.mimetype
    ).where(sql.and_(File.filestore_id == Filestore.id, File.uuid == uuid))
    result = session.execute(query).fetchone()

    if result is None:
        raise NotFound(
            f"Document with uuid: {uuid} not found", "document/not_found"
        )

    document = DocumentInformation(
        id=result.id,
        name=result.name + result.extension,
        mimetype=result.mimetype,
        version=result.version,
    )
    return document


def get_case_assignee(session, case_uuid: UUID):
    """Get assignee of case as UserInformation object by case_uuid.

    :param session: database session
    :type session: session
    :param uuid: uuid of the case.
    :type uuid: UUID
    :raises NotFound: When assignee of case is not found.
    :return: assignee_info
    :rtype: UserInformation
    """

    query = sql.select(Subject.id, Subject.properties, Subject.uuid).where(
        sql.and_(Case.uuid == case_uuid, Case.behandelaar_gm_id == Subject.id)
    )
    result = session.execute(query).fetchone()

    if result is None:
        raise NotFound(
            f"Case with uuid: {case_uuid} is not assigned", "case/not_assigned"
        )

    subject = SubjectInformation(
        id=result.id,
        display_name=result.properties["displayname"],
        uuid=result.uuid,
    )
    return subject


def get_file(session, uuid: UUID):
    """Get file from filestore table by UUID.

    :param session: database session
    :type session: session
    :param uuid: uuid of the filestore.
    :type uuid: UUID
    :raises NotFound: When file with UUID not found
    :return: role_info
    :rtype: FileInformation
    """

    query = sql.select(Filestore.id, Filestore.original_name).where(
        Filestore.uuid == uuid
    )
    result = session.execute(query).fetchone()

    if result is None:
        raise NotFound(f"File with uuid: {uuid} not found", "file/not_found")

    file = FileInformation(
        id=result.id,
        name=result.original_name,
    )
    return file


def get_custom_object(session, uuid: UUID):
    """Get custom object from custom_object_version table by UUID.

    :param session: database session
    :type session: session
    :param uuid: uuid of the custom_object_version.
    :type uuid: UUID
    :raises NotFound: When custom object with UUID not found
    :return: custom_object
    :rtype: CustomObjectInformation
    """
    query = sql.select(
        CustomObjectVersion.title,
        CustomObjectVersion.custom_object_type_version_id,
        CustomObjectTypeVersion.name.label("custom_object_type"),
        CustomObject.uuid.label("custom_object_uuid"),
    ).where(
        sql.and_(
            CustomObjectVersion.custom_object_type_version_id
            == CustomObjectTypeVersion.id,
            CustomObjectVersion.uuid == uuid,
            CustomObjectVersion.custom_object_id == CustomObject.id,
        )
    )

    result = session.execute(query).fetchone()
    if result is None:
        raise NotFound(
            f"Custom Object with uuid: {uuid} not found",
            "custom_object/not_found",
        )

    custom_object = CustomObjectInformation(
        title=result.title,
        custom_object_type=result.custom_object_type,
        custom_object_uuid=result.custom_object_uuid,
    )
    return custom_object


def get_custom_object_type(session, uuid: UUID) -> CustomObjectTypeInformation:
    """Get custom object type from custom_object_type_version table by UUID."""
    query = (
        sql.select(CustomObjectTypeVersion.name, CustomObjectType.id)
        .select_from(
            sql.join(
                CustomObjectType,
                CustomObjectTypeVersion,
                CustomObjectTypeVersion.custom_object_type_id
                == CustomObjectType.id,
            )
        )
        .where(CustomObjectTypeVersion.uuid == uuid)
    )

    result = session.execute(query).fetchone()
    if result is None:
        raise NotFound(
            f"Custom object type with uuid: {uuid} not found",
            "custom_object_type/not_found",
        )

    custom_object_type = CustomObjectTypeInformation(
        id=result.id,
        name=result.name,
    )
    return custom_object_type


def get_person(session, uuid: UUID) -> PersonInformation:
    """Get person information from natuurlijk_persoon table by UUID."""

    query = sql.select(
        NatuurlijkPersoon.id, NatuurlijkPersoon.object_type
    ).where(NatuurlijkPersoon.uuid == uuid)
    result = session.execute(query).fetchone()
    if result is None:
        raise NotFound(
            f"Person with uuid: {uuid} not found",
            "person/not_found",
        )

    person = PersonInformation(id=result.id, type=result.object_type)
    return person


def get_result_label(session, result_id: int) -> str:
    query = sql.select(ZaaktypeResultaten.label).where(
        ZaaktypeResultaten.id == result_id
    )
    result = session.execute(query).fetchone()
    if result is None:
        raise NotFound(
            f"ZaaktypeResultaten with id: {result_id} not found",
            "zaaktype_resultaten/not_found",
        )
    return result.label
