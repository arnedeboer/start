# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    format_date_from_iso,
    get_attribute_by_magic_string,
    get_case,
    get_department,
    get_result_label,
    get_role,
    get_user,
)
from zsnl_domains.database.schema import Logging


class CaseManagementBase(BaseLogger):
    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """
        case_info = get_case(session=session, uuid=event["entity_id"])
        user_info = get_user(session=session, uuid=event["user_uuid"])

        formatted_entity_data = self.format_entity_data(event=event)

        event_parameters = self.generate_event_parameters(
            session=session,
            entity_data=formatted_entity_data,
            case_id=case_info.id,
        )
        event_parameters["correlation_id"] = event["correlation_id"]
        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component=self.component,
            component_id=None,
            case_id=case_info.id,
            created_date=event["created_date"],
            event_data=event_parameters,
            restricted=(case_info.confidentiality != "public"),
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        raise NotImplementedError


class CaseRegistrationDateSet(CaseManagementBase):
    component = "zaak"
    subject = "Registratiedatum voor zaak: {} gewijzigd naar: {}"
    variables = ["case_id", "registration_date"]
    event_type = "case/update/registration_date"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {
            "case_id": case_id,
            "registration_date": format_date_from_iso(
                entity_data["registration_date"]
            ),
        }
        return event_parameters


class CaseTargetCompletionDateSet(CaseManagementBase):
    component = "zaak"
    subject = "Streefafhandeldatum voor zaak: {} gewijzigd naar: {}"
    variables = ["case_id", "target_date"]
    event_type = "case/update/target_date"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {
            "case_id": case_id,
            "target_date": format_date_from_iso(
                entity_data["target_completion_date"]
            ),
        }
        return event_parameters


class CaseCompletionDateSet(CaseManagementBase):
    component = "zaak"
    subject = "Afhandeldatum voor zaak: {} gewijzigd naar: {}"
    variables = ["case_id", "target_date"]
    event_type = "case/update/completion_date"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {
            "case_id": case_id,
            "target_date": format_date_from_iso(
                entity_data["completion_date"]
            ),
        }
        return event_parameters


class CaseStatusSet(CaseManagementBase):
    component = "zaak"
    subject = "Status voor zaak: {} gewijzigd naar: {}"
    variables = ["case_id", "status"]
    event_type = "case/update/status"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {
            "case_id": case_id,
            "status": self._map_status_name(entity_data["status"]),
        }
        return event_parameters

    def _map_status_name(self, status):
        MAP_STATUS_NAMES = {"open": "open", "stalled": "opgeschort"}
        return MAP_STATUS_NAMES[status]


class CaseCoordinatorSet(CaseManagementBase):
    component = "zaak"
    subject = 'Coordinator ingesteld op "{}"'
    variables = ["subject_name"]
    event_type = "case/relation/update"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        try:
            coordinator_uuid = entity_data["coordinator"]["entity_id"]
        except KeyError:
            coordinator_uuid = entity_data["coordinator"]["uuid"]

        coordinator = get_user(session, coordinator_uuid)

        event_parameters = {
            "subject_name": coordinator.display_name,
            "subject_id": coordinator.id,
            "subject_relation": "Coordinator",
        }
        return event_parameters


class CasePaused(CaseManagementBase):
    component = "zaak"
    subject = "Zaak {} opgeschort voor onbepaalde tijd: {}"
    variables = ["case_id", "reason"]
    event_type = "case/suspend"

    def _generate_subject(self, event_parameters) -> str:
        def get_formatted_string(variables, subject, parameters):
            data = [parameters[v] for v in variables]
            return subject.format(*data)

        if event_parameters.get("stalled_until"):
            return get_formatted_string(
                variables=["case_id", "stalled_until", "reason"],
                subject="Zaak {} opgeschort tot {}: {}",
                parameters=event_parameters,
            )

        return get_formatted_string(
            variables=self.variables,
            subject=self.subject,
            parameters=event_parameters,
        )

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {
            "case_id": case_id,
            "reason": entity_data.get("suspension_reason"),
            "stalled_since": entity_data.get("stalled_since_date"),
            "stalled_until": entity_data.get("stalled_until_date"),
        }
        return event_parameters


class CaseResumed(CaseManagementBase):
    component = "zaak"
    subject = "Zaak {} hervat: {}"
    variables = ["case_id", "reason", "stalled_since", "stalled_until"]
    event_type = "case/resume"

    def _generate_subject(self, event_parameters) -> str:
        case_data = [event_parameters[v] for v in self.variables]
        if event_parameters.get("stalled_since") and event_parameters.get(
            "stalled_until"
        ):
            self.subject.join(" Opgeschort van {} tot {}")
        log_subject = self.subject.format(*case_data)
        return log_subject

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {
            "case_id": case_id,
            "reason": entity_data.get("resume_reason"),
            "stalled_since": entity_data.get("stalled_since_date"),
            "stalled_until": entity_data.get("stalled_until_date"),
        }
        return event_parameters


class CaseAssigneeChanged(CaseManagementBase):
    component = "zaak"
    subject = "Zaak {} geaccepteerd door {}"
    variables = ["case_id", "acceptee_name"]
    event_type = "case/accept"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        try:
            assignee_uuid = entity_data["assignee"]["entity_id"]
        except KeyError:
            assignee_uuid = entity_data["assignee"]["uuid"]

        assignee = get_user(session, assignee_uuid)

        event_parameters = {
            "case_id": case_id,
            "acceptee_name": assignee.display_name,
            "acceptee_id": assignee.id,
        }
        return event_parameters


class CaseAttributeUpdated(CaseManagementBase):
    component = "kenmerk"
    subject = 'Kenmerk "{}" gewijzigd'
    variables = [
        "attribute_name",
    ]
    event_type = "case/attribute/update"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        changed_attribute = {
            attribute: value
            for attribute, value in entity_data["custom_fields"].items()
            if value != entity_data["custom_fields_old"].get(attribute, None)
        }
        num_changed_attributes = len(changed_attribute.keys())

        if num_changed_attributes > 1:
            raise AttributeError(
                "Only one attribute should be changed for each CaseAttributeUpdated event"
            )

        event_parameters = None
        if num_changed_attributes == 1:
            magic_string, value = next(iter(changed_attribute.items()))
            attr = get_attribute_by_magic_string(
                session, magic_string=magic_string
            )
            event_parameters = {
                "case_id": case_id,
                "attribute_id": attr.id,
                "attribute_name": magic_string,
                "attribute_value": value,
            }
        return event_parameters


class CaseCreated(CaseManagementBase):
    component = "zaak"
    subject = "Zaak {} aangemaakt."
    variables = ["case_id"]
    event_type = "case/create"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {"case_id": case_id}
        return event_parameters


class CaseAllocationSet(CaseManagementBase):
    component = "zaak"
    subject = "Toewijzing gewijzigd naar afdeling '{}' en rol '{}'"
    variables = ["ou_name", "role_name"]
    event_type = "case/update/allocation"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        try:
            department_uuid = entity_data["department"]["entity_id"]
        except KeyError:
            department_uuid = entity_data["department"]["uuid"]

        try:
            role_uuid = entity_data["role"]["entity_id"]
        except KeyError:
            role_uuid = entity_data["role"]["uuid"]

        department = get_department(session, department_uuid)
        role = get_role(session, role_uuid)
        event_parameters = {
            "ou_id": department.id,
            "ou_name": department.name,
            "role_id": role.id,
            "role_name": role.name,
        }
        return event_parameters


class CaseDestructionDateSet(CaseManagementBase):
    component = "zaak"
    subject = "Vernietigingsdatum voor zaak: {} gewijzigd naar {}"
    variables = ["case_id", "destruction_date"]
    event_type = "case/update/completion_date"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {
            "case_id": case_id,
            "destruction_date": format_date_from_iso(
                entity_data["destruction_date"]
            ),
        }
        if (
            "destruction_reason" in entity_data
            and entity_data["destruction_reason"]["reason"] is not None
        ):
            self.subject = (
                "Vernietigingsdatum voor zaak: {} gewijzigd naar {} : {}"
            )
            self.variables = [
                "case_id",
                "destruction_date",
                "destruction_reason",
            ]
            event_parameters.update(
                {
                    "destruction_reason": entity_data["destruction_reason"][
                        "reason"
                    ],
                }
            )
        return event_parameters


class CaseDestructionDateCleared(CaseManagementBase):
    component = "zaak"
    subject = "Vernietigingsdatum voor zaak: {} gewist"
    variables = ["case_id"]
    event_type = "case/update/purge_date"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {"case_id": case_id}
        if (
            "destruction_reason" in entity_data
            and entity_data["destruction_reason"]["reason"] is not None
        ):
            self.subject = "Vernietigingsdatum voor zaak: {} gewist: {}"
            self.variables = ["case_id", "destruction_reason"]
            event_parameters.update(
                {
                    "destruction_reason": entity_data["destruction_reason"][
                        "reason"
                    ],
                }
            )
        return event_parameters


class CaseDestructionDateRecalculated(CaseManagementBase):
    component = "zaak"
    subject = "Vernietigingsdatum voor zaak {} herberekend naar {}: {}"
    variables = ["case_id", "label", "destruction_reason"]
    event_type = "case/update/purge_date"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        event_parameters = {
            "case_id": case_id,
            "destruction_reason": entity_data["destruction_reason"]["reason"],
            "label": entity_data["destruction_reason"]["label"],
        }

        return event_parameters


class CaseResultSet(CaseManagementBase):
    component = "zaak"
    event_type = "case/update/result"

    def generate_event_parameters(
        self, session, entity_data: dict, case_id: int
    ):
        self.logger.info(f"RS {entity_data}")
        event_parameters = {
            "case_id": case_id,
            "result_new": entity_data["result"]["result"],
            "result_new_label": get_result_label(
                session, entity_data["result"]["result_id"]
            ),
        }

        if "result_old" in entity_data:
            # update result
            self.subject = (
                "Zaak {}: Zaakresultaat gewijzigd van {} naar {} ({})"
            )
            self.variables = [
                "case_id",
                "result_old",
                "result_new",
                "result_new_label",
            ]
            event_parameters.update(
                {
                    "result_old": entity_data["result_old"]["result"],
                }
            )
        else:
            # set result
            self.subject = "Zaak {}: Zaakresultaat ingesteld op: {} ({})"
            self.variables = ["case_id", "result_new", "result_new_label"]

        return event_parameters
