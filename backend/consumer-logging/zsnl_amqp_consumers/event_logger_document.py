# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    DepartmentInformation,
    DocumentInformation,
    UserInformation,
    get_case,
    get_department,
    get_document,
    get_user,
)
from minty.exceptions import NotFound
from zsnl_domains.database.schema import Logging


class DocumentBase(BaseLogger):
    def __call__(self, session, event) -> Logging:
        """Collect and necessary information before creating log record.

        :param BaseLogger: base class for logging
        :type BaseLogger: BaseLogger
        :param session: database session
        :type session: Session
        :param event: event
        :type event: Event
        :raises error: no user or case found
        :return: logging record
        :rtype: Logging
        """

        try:
            user_info = get_user(session=session, uuid=event["user_uuid"])
        except NotFound:
            # User not found.
            return

        try:
            document_info = get_document(
                session=session, uuid=event["entity_id"]
            )
        except NotFound:
            # Document not found.
            return

        formatted_entity_data = self.format_entity_data(event=event)
        case_uuid = formatted_entity_data.get("case_uuid")
        case_id = None
        case_info = None

        if case_uuid:
            try:
                case_info = get_case(session, case_uuid)
                case_id = case_info.id
            except NotFound:
                return

        try:
            assignment_info = self._get_assignment_info(
                session, formatted_entity_data
            )
        except NotFound:
            return

        event_parameters = self.generate_event_parameters(
            case_id=case_id,
            entity_data=formatted_entity_data,
            document_info=document_info,
            user_info=user_info,
            intake_owner_info=assignment_info.get("intake_owner_info"),
            intake_group_info=assignment_info.get("intake_group_info"),
        )

        event_parameters["correlation_id"] = event["correlation_id"]
        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component=self.component,
            component_id=None,
            case_id=case_id,
            created_date=event["created_date"],
            event_data=event_parameters,
            restricted=False,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        """Generate subject line formatted with parameters from decoded message.

        :param event_parameters: to use in formatting
        :type event_parameters: dict
        :return: subject line to log
        :rtype: str
        """
        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        case_id: int,
        entity_data: dict,
        document_info: DocumentInformation,
        user_info: UserInformation,
        intake_owner_info: UserInformation,
        intake_group_info: DepartmentInformation,
    ):
        raise NotImplementedError

    def _get_assignment_info(self, session, formatted_entity_data) -> dict:
        """Get assignment info (intake_owner_info, intake_group_info) for document.

        :param session: session
        :type session: database session
        :param caseentity_data_uuid: formatted_entity_data
        :type entity_data: dict
        :return: assignment_info
        :rtype: dict
        """
        intake_group_info = None
        intake_owner_info = None

        if formatted_entity_data.get("intake_group_uuid"):
            intake_group_info = get_department(
                session=session,
                uuid=formatted_entity_data["intake_group_uuid"],
            )

        if formatted_entity_data.get("intake_owner_uuid"):
            intake_owner_info = get_user(
                session=session,
                uuid=formatted_entity_data["intake_owner_uuid"],
            )

        return {
            "intake_group_info": intake_group_info,
            "intake_owner_info": intake_owner_info,
        }


class DocumentUpdated(DocumentBase):
    component = "document"
    subject = "Documenteigenschappen van '{} | {}' gewijzigd door '{}'."
    variables = ["file_name", "file_id", "user_name"]
    event_type = "document/metadata/update"

    def generate_event_parameters(
        self,
        case_id: int,
        entity_data: dict,
        document_info: DocumentInformation,
        user_info: UserInformation,
        intake_owner_info: UserInformation,
        intake_group_info: DepartmentInformation,
    ):
        event_parameters = {
            "case_id": case_id,
            "file_id": document_info.id,
            "file_name": document_info.name,
            "version": document_info.version,
            "metadata": {
                "description": entity_data["description"],
                "origin": entity_data["origin"],
                "origin_date": entity_data["origin_date"],
                "document_category": entity_data["document_category"],
                "trust_level": entity_data["confidentiality"],
            },
            "mimetype": document_info.mimetype,
            "user_name": user_info.display_name,
        }

        if case_id:
            self.event_type = "case/document/metadata/update"

        return event_parameters


class DocumentDeleted(DocumentBase):
    component = "document"
    subject = "'{} | {}' verwijderd uit Documentintake door '{}'"
    variables = ["file_name", "file_id", "user_name"]
    event_type = "document/delete_document"

    def generate_event_parameters(
        self,
        case_id: int,
        entity_data: dict,
        document_info: DocumentInformation,
        user_info: UserInformation,
        intake_owner_info: UserInformation,
        intake_group_info: DepartmentInformation,
    ):
        if entity_data["destroy_reason"] == "":
            entity_data["destroy_reason"] = "Niet opgegeven."

        event_parameters = {
            "case_id": case_id,
            "destroy_reason": entity_data["destroy_reason"],
            "file_id": document_info.id,
            "file_name": document_info.name,
            "version": document_info.version,
            "mimetype": document_info.mimetype,
            "user_name": user_info.display_name,
        }

        if case_id:
            self.event_type = "case/document/delete_document"

        return event_parameters


class DocumentAddedToCase(DocumentBase):
    component = "document"
    subject = "'{} | {}' toegevoegd aan zaak {} door '{}'"
    variables = ["file_name", "file_id", "case_id", "user_name"]
    event_type = "case/document/assign"

    def generate_event_parameters(
        self,
        case_id: int,
        entity_data: dict,
        document_info: DocumentInformation,
        user_info: UserInformation,
        intake_owner_info: UserInformation,
        intake_group_info: DepartmentInformation,
    ):
        event_parameters = {
            "case_id": case_id,
            "file_id": document_info.id,
            "file_name": document_info.name,
            "version": document_info.version,
            "metadata": {
                "description": entity_data["description"],
                "origin": entity_data["origin"],
                "origin_date": entity_data["origin_date"],
            },
            "mimetype": document_info.mimetype,
            "user_name": user_info.display_name,
        }
        return event_parameters


class DocumentCreated(DocumentBase):
    component = "document"
    subject = "Document '{}' toegevoegd"
    variables = ["file_name"]
    event_type = "document/create"

    def generate_event_parameters(
        self,
        case_id: int,
        entity_data: dict,
        document_info: DocumentInformation,
        user_info: UserInformation,
        intake_owner_info: UserInformation,
        intake_group_info: DepartmentInformation,
    ):
        event_parameters = {
            "case_id": case_id,
            "file_id": document_info.id,
            "file_name": document_info.name,
            "version": document_info.version,
            "mimetype": document_info.mimetype,
        }

        if case_id:
            self.event_type = "case/document/create"

        return event_parameters


class DocumentAssignedToRole(DocumentBase):
    component = "document"
    subject = "'{} | {}' toegewezen aan Afdeling '{}' door '{}'."
    variables = ["file_name", "file_id", "intake_group_name", "user_name"]
    event_type = "document/assign"

    def generate_event_parameters(
        self,
        case_id: int,
        entity_data: dict,
        document_info: DocumentInformation,
        user_info: UserInformation,
        intake_owner_info: UserInformation,
        intake_group_info: DepartmentInformation,
    ):
        return {
            "case_id": case_id,
            "file_id": document_info.id,
            "file_name": document_info.name,
            "user_name": user_info.display_name,
            "intake_group_name": intake_group_info.name,
        }


class DocumentAssignedToUser(DocumentBase):
    component = "document"
    subject = "'{} | {}' toegewezen aan '{}' door '{}'."
    variables = ["file_name", "file_id", "intake_owner_name", "user_name"]
    event_type = "document/assign"

    def generate_event_parameters(
        self,
        case_id: int,
        entity_data: dict,
        document_info: DocumentInformation,
        user_info: UserInformation,
        intake_owner_info: UserInformation,
        intake_group_info: DepartmentInformation,
    ):
        return {
            "case_id": case_id,
            "file_id": document_info.id,
            "file_name": document_info.name,
            "user_name": user_info.display_name,
            "intake_owner_name": intake_owner_info.display_name,
        }


class DocumentAssignmentRejected(DocumentBase):
    component = "document"
    subject = "'{}' afgewezen in de documentintake door '{}'."
    variables = ["file_name", "user_name"]
    event_type = "document/assignment/reject"

    def generate_event_parameters(
        self,
        case_id: int | None,
        entity_data: dict | None,
        document_info: DocumentInformation,
        user_info: UserInformation,
        intake_owner_info: UserInformation | None,
        intake_group_info: DepartmentInformation | None,
    ):
        return {
            "file_name": document_info.name,
            "user_name": user_info.display_name,
        }
