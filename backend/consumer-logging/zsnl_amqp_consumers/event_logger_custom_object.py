# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .event_logger_base import BaseLogger
from .repository import (
    CustomObjectInformation,
    UserInformation,
    get_custom_object,
    get_user,
)
from collections import namedtuple
from zsnl_domains.database.schema import Logging


class CustomObjectBase(BaseLogger):
    variables = None
    event_type = None
    subject = None

    def __call__(self, session, event) -> Logging:
        "Collect and necessary information before creating log record."

        formatted_entity_data = self.format_entity_data(event=event)
        if event["event_name"] == "CustomObjectDeleted":
            custom_object_info = namedtuple(
                "CustomObjectInformation",
                "title custom_object_type custom_object_uuid",
            )(
                title=formatted_entity_data["title"],
                custom_object_type=None,
                custom_object_uuid=formatted_entity_data[
                    "version_independent_uuid_old"
                ],
            )
        else:
            custom_object_info = get_custom_object(
                session=session, uuid=event["entity_id"]
            )

        user_info = get_user(session=session, uuid=event["user_uuid"])

        event_parameters = self.generate_event_parameters(
            entity_data=formatted_entity_data,
            custom_object_info=custom_object_info,
            user_info=user_info,
        )

        return self._create_logging_record(
            user_info=user_info,
            event_type=self.event_type,
            subject=self._generate_subject(event_parameters),
            component="custom_object",
            component_id=None,
            case_id=None,
            created_date=event["created_date"],
            event_data=event_parameters,
        )

    def _generate_subject(self, event_parameters: dict) -> str:
        "Generate subject line formatted with parameters from decoded message."

        vars = [event_parameters[v] for v in self.variables]
        log_subject = self.subject.format(*vars)
        return log_subject

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ):
        raise NotImplementedError


class CustomObjectCreated(CustomObjectBase):
    subject = "Object {} van objecttype {} is aangemaakt."
    variables = ["custom_object_name", "custom_object_type"]
    event_type = "custom_object/created"

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."

        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
        }

        return event_params


class CustomObjectUpdated(CustomObjectBase):
    subject = "Object {} van objecttype {} is aangepast."
    variables = ["custom_object_name", "custom_object_type"]
    event_type = "custom_object/updated"

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."

        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
        }
        if entity_data.get("status") and (
            entity_data.get("status") != entity_data.get("status_old")
        ):
            event_params["custom_object_status"] = (
                "actief"
                if entity_data.get("status") == "active"
                else "inactief"
            )
            self.subject = "De status van object {} van objecttype {} is aangepast naar {}."
            self.variables.append("custom_object_status")

        return event_params


class CustomObjectRelatedTo(CustomObjectBase):
    subject = "Object {} van objecttype {} is gerelateerd."
    variables = ["custom_object_name", "custom_object_type"]
    event_type = "custom_object/relatedTo"

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."
        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
        }

        return event_params


class CustomObjectUnrelatedFrom(CustomObjectBase):
    subject = "Relatie verwijderd voor Object {} van objecttype {}."
    variables = ["custom_object_name", "custom_object_type"]
    event_type = "custom_object/unrelatedFrom"

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."

        event_params = {
            "custom_object_name": custom_object_info.title,
            "custom_object_type": custom_object_info.custom_object_type,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
        }

        return event_params


class CustomObjectDeleted(CustomObjectBase):
    subject = "Object {} verwijderd door {}."
    variables = ["custom_object_name", "username"]
    event_type = "custom_object/deleted"

    def generate_event_parameters(
        self,
        entity_data: dict,
        custom_object_info: CustomObjectInformation,
        user_info: UserInformation,
    ) -> dict:
        "Generate event parameters from entity data."

        event_params = {
            "custom_object_name": custom_object_info.title,
            "username": user_info.display_name,
            "custom_object_uuid": custom_object_info.custom_object_uuid,
        }

        return event_params
