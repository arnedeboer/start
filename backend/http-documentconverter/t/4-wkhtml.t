#! /usr/bin/env perl

# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

use warnings;
use strict;

use Test::More;

use ConvertService::Plugin::WKHTMLtoPDF;
use FindBin;
use Image::ExifTool qw(ImageInfo);

my $cs = ConvertService::Plugin::WKHTMLtoPDF->new();

for my $dest (qw(image/png image/jpeg application/pdf)) {
    ok(
        $cs->can_convert(from => 'text/html', to => $dest),
        "Can convert from HTML to $dest"
    );
}

{
    my $converted = $cs->convert(
        source_file => "$FindBin::Bin/../t/data/example.html",
        from_type   => "text/html",
        to_type     => "image/png",
        options     => { width => 320, height => 200 },
    );

    my $info = ImageInfo(\$converted, 'MIMEType', 'ImageWidth', 'ImageHeight');
    is($info->{MIMEType}, "image/png", "HTML to PNG conversion successful");
    is($info->{ImageWidth}, 320, "Image has the expected width"); 
    is($info->{ImageHeight}, 200, "Image has the expected height"); 
}

{
    my $converted = $cs->convert(
        source_file => "$FindBin::Bin/../t/data/example.html",
        from_type   => "text/html",
        to_type     => "image/jpeg",
        options     => { width => 640, height => 480 },
    );

    my $info = ImageInfo(\$converted, 'MIMEType', 'ImageWidth', 'ImageHeight');
    is($info->{MIMEType}, "image/jpeg", "HTML to JPG conversion successful");
    is($info->{ImageWidth}, 640, "Image has the expected width"); 
    is($info->{ImageHeight}, 480, "Image has the expected height"); 
}

{
    my $converted = $cs->convert(
        source_file => "$FindBin::Bin/../t/data/example.html",
        from_type   => "text/html",
        to_type     => "application/pdf",
        options     => {},
    );

    my $info = ImageInfo(\$converted, 'MIMEType');
    is($info->{MIMEType}, "application/pdf", "HTML to PDF conversion successful");
}

done_testing();
