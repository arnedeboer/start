# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

#####################################################################
### Stage 1: Setup base python with requirements
#####################################################################
FROM python:3.11-slim-bullseye AS base

ARG BUILD_TIMESTAMP=undefined
ARG BUILD_REF=master

# Add necessary packages to system
RUN apt-get update && apt-get install -y \
  build-essential \
  libpq-dev \
  libmagic1 \
  git

COPY backend/domains /opt/domains
COPY backend/consumer-communication/requirements/base.txt /opt/consumer-communication/requirements.txt

WORKDIR /opt/consumer-communication

RUN python3 -m venv /opt/virtualenv \
  && . /opt/virtualenv/bin/activate \
  && pip install -r requirements.txt

ENV PATH="/opt/virtualenv/bin:$PATH"

RUN echo "$BUILD_TIMESTAMP" > /opt/build-timestamp


COPY backend/consumer-communication /opt/consumer-communication

#####################################################################
## Stage 2: Production build
#####################################################################
FROM python:3.11-slim-bullseye AS production

ENV OTAP=production
ENV PYTHONUNBUFFERED=on
ENV PATH="/opt/virtualenv/bin:$PATH"
RUN apt-get update && apt-get install -y libmagic1 libpq5

# Set up application
COPY --from=base /opt /opt/

WORKDIR /opt/consumer-communication
RUN pip install -e .

# Run as non-root user
USER 65534

#####################################################################
## Stage 3: QA environment
#####################################################################
FROM base AS quality-and-testing

ENV OTAP=test

COPY backend/consumer-communication/requirements/test.txt /opt/consumer-communication/requirements-test.txt

WORKDIR /opt/consumer-communication
RUN pip install -r requirements-test.txt

RUN  mkdir -p /root/test_result \
  && bin/git-hooks/pre-commit -c -j /root/test_result/junit.xml

#####################################################################
## Stage 4: Development image
#####################################################################
FROM quality-and-testing AS development

ENV OTAP=development

COPY backend/consumer-communication/requirements/dev.txt /opt/consumer-communication/requirements-dev.txt

WORKDIR /opt/consumer-communication
RUN pip install -r requirements-dev.txt
