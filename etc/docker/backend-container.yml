version: "3.6"
x-common-http-command: &common-http-command
    command:
        - "pserve"
        - "--reload"
        - "--server-name"
        - "docker"
        - "development.ini"
x-common-logging: &common-logging
    logging:
        driver: "json-file"
        options:
            max-size: "200k"
            max-file: "10"

x-volume-customerd: &volume-customerd
    type: bind
    source: "./etc/customer.d"
    target: "/etc/zaaksysteem/customer.d"

x-volume-dependencies-domains: &volume-dependencies-domains
    type: bind
    source: "./backend/domains"
    target: "/opt/domains"

x-volume-dependencies-pyramid: &volume-dependencies-pyramid
    type: bind
    source: "./backend/pyramid"
    target: "/opt/pyramid"

x-volume-dependencies-migration_libraries: &volume-dependencies-migration_libraries
    type: bind
    source: "./backend/migration_libraries"
    target: "/opt/migration_libraries"

x-common-environment: &common-environment
    AWS_CA_BUNDLE: "/etc/custom-certs/ca.crt"
    PERL_LWP_SSL_CA_FILE: "/etc/custom-certs/ca.crt"
    REQUESTS_CA_BUNDLE: "/etc/custom-certs/ca.crt"
    SSL_CERT_FILE: "/etc/custom-certs/ca.crt"
    PYTHONWARNINGS: "default"
    MQ_EXCHANGE: "minty_exchange"

services:
    # Homemade DDD Python services: HTTP
    http-admin:
        <<: [*common-http-command, *common-logging]
        build:
            context: "."
            dockerfile: "./backend/http-admin/Dockerfile"
            target: "development"
        expose:
            - "9084"
        volumes:
            - "./backend/http-admin/zsnl_admin_http:/opt/http-admin/zsnl_admin_http"
            - "./backend/http-admin/tests:/opt/http-admin/tests"
            - "./backend/http-admin/bin:/opt/http-admin/bin"
            - "./etc/minty_config.conf:/opt/http-admin/package.conf"
            - "./backend/http-admin/apidocs:/opt/http-admin/apidocs"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        depends_on:
            - "redis"
            - "database"
            - "message-queue"
            - "statsd"
        environment:
            <<: *common-environment
    http-cm:
        <<: [*common-http-command, *common-logging]
        build:
            context: "."
            dockerfile: "./backend/http-cm/Dockerfile"
            target: "development"
        expose:
            - "9085"
        volumes:
            - "./backend/http-cm/zsnl_case_management_http:/opt/http-cm/zsnl_case_management_http"
            - "./backend/http-cm/apidocs:/opt/http-cm/apidocs"
            - "./backend/http-cm/tests:/opt/http-cm/tests"
            - "./backend/http-cm/bin:/opt/http-cm/bin"
            - "./etc/minty_config.conf:/opt/http-cm/package.conf"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        depends_on:
            - "redis"
            - "database"
            - "message-queue"
            - "statsd"
        environment:
            <<: *common-environment
    http-communication:
        <<: [*common-http-command, *common-logging]
        build:
            context: "."
            dockerfile: "./backend/http-communication/Dockerfile"
            target: "development"
        expose:
            - "9087"
        volumes:
            - "./backend/http-communication/zsnl_communication_http:/opt/http-communication/zsnl_communication_http"
            - "./backend/http-communication/apidocs:/opt/http-communication/apidocs"
            - "./backend/http-communication/tests:/opt/http-communication/tests"
            - "./backend/http-communication/bin:/opt/http-communication/bin"
            - "./etc/minty_config.conf:/opt/http-communication/package.conf"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        depends_on:
            - "redis"
            - "database"
            - "message-queue"
            - "statsd"
        environment:
            <<: *common-environment
    http-document:
        <<: [*common-http-command, *common-logging]
        build:
            context: "."
            dockerfile: "./backend/http-document/Dockerfile"
            target: "development"
        expose:
            - "9086"
        volumes:
            - "./backend/http-document/zsnl_document_http:/opt/http-document/zsnl_document_http"
            - "./backend/http-document/tests:/opt/http-document/tests"
            - "./backend/http-document/bin:/opt/http-document/bin"
            - "./backend/http-document/apidocs:/opt/http-document/apidocs"
            - "./etc/minty_config.conf:/opt/http-document/package.conf"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        depends_on:
            - "redis"
            - "database"
            - "message-queue"
            - "statsd"
        environment:
            <<: *common-environment
    http-geo:
        <<: [*common-http-command, *common-logging]
        build:
            context: "."
            dockerfile: "./backend/http-geo/Dockerfile"
            target: development
        command:
            - pserve
            - "--reload"
            - "--server-name"
            - docker
            - development.ini
        expose:
            - "9084"
        volumes:
            - "./backend/http-geo/zsnl_http_geo:/opt/http-geo/zsnl_http_geo"
            - "./backend/http-geo/tests:/opt/http-geo/tests"
            - "./backend/http-geo/bin:/opt/http-geo/bin"
            - "./backend/http-geo/apidocs:/opt/http-geo/apidocs"
            - "./etc/minty_config.conf:/opt/http-geo/package.conf"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        depends_on:
            - redis
            - database
            - message-queue
            - statsd
        environment:
            <<: *common-environment

    # Homemade DDD Python services: AMQP consumers
    consumer-cm:
        build:
            context: "."
            dockerfile: "./backend/consumer-cm/Dockerfile"
            target: "development"
        volumes:
            - "./backend/consumer-cm/zsnl_case_events_consumer:/opt/consumer-cm/zsnl_case_events_consumer"
            - "./backend/consumer-cm/tests:/opt/consumer-cm/tests"
            - "./backend/consumer-cm/bin:/opt/consumer-cm/bin"
            - "./etc/minty_config.conf:/opt/consumer-cm/config.conf"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        command:
            - "hupper"
            - "-m"
            - "zsnl_case_events_consumer"
        depends_on:
            - "message-queue"
            - "database"
            - "statsd"
        environment:
            <<: *common-environment
    consumer-communication:
        build:
            context: "."
            dockerfile: "./backend/consumer-communication/Dockerfile"
            target: "development"
        volumes:
            - "./backend/consumer-communication/zsnl_communication_consumer:/opt/consumer-communication/zsnl_communication_consumer"
            - "./backend/consumer-communication/tests:/opt/consumer-communication/tests"
            - "./backend/consumer-communication/bin:/opt/consumer-communication/bin"
            - "./etc/minty_config.conf:/opt/consumer-communication/config.conf"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        command:
            - "hupper"
            - "-m"
            - "zsnl_communication_consumer"
        depends_on:
            - "message-queue"
            - "database"
            - "statsd"
        environment:
            <<: *common-environment
    consumer-geo:
        build:
            context: "."
            dockerfile: "./backend/consumer-geo/Dockerfile"
            target: "development"
        volumes:
            - "./backend/consumer-geo/zsnl_consumer_geo:/opt/consumer-geo/zsnl_consumer_geo"
            - "./backend/consumer-geo/tests:/opt/consumer-geo/tests"
            - "./backend/consumer-geo/bin:/opt/consumer-geo/bin"
            - "./etc/minty_config.conf:/opt/consumer-geo/config.conf"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        command:
            - "hupper"
            - "-m"
            - "zsnl_consumer_geo"
        depends_on:
            - "message-queue"
            - "database"
            - "statsd"
        environment:
            <<: *common-environment
                    
    consumer-logging:
        build:
            context: "."
            dockerfile: "./backend/consumer-logging/Dockerfile"
            target: "development"
        volumes:
            - "./backend/consumer-logging/zsnl_amqp_consumers:/opt/consumer-logging/zsnl_amqp_consumers"
            - "./backend/consumer-logging/tests:/opt/consumer-logging/tests"
            - "./backend/consumer-logging/bin:/opt/consumer-logging/bin"
            - "./etc/minty_config.conf:/opt/consumer-logging/config.conf"
            - "certificates:/etc/custom-certs"
            - <<: *volume-customerd
            - <<: *volume-dependencies-domains
            - <<: *volume-dependencies-pyramid
            - <<: *volume-dependencies-migration_libraries
        command:
            - "hupper"
            - "-m"
            - "zsnl_amqp_consumers"
        depends_on:
            - "message-queue"
            - "database"
            - "statsd"
        environment:
            <<: *common-environment
