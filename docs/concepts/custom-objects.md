# Custom Objects

Custom objects are objects which a user of our system can freely define
themself. The concept of objects tend to be a little bit hard to grasp, so this
document aims to give you an overview about objects, and how it is implemented
in this system.

## Background

You could say that everything in the end is an object in our world or system.
For instance. A case is just a bunch of data with attributes and actions. And
if you look further, you could say the same about a subject, or a location. But
the problem with building "generic object handling"-system, is that a developer
"always" ends up in a spiral it can't get out: making a system in a way
everything in the world is supported, and it gets overly complex.

That's why we decided to implement our generic objects hard. In plain DDD,
without generating definitions etc. So when we talk about "custom objects", we
are not talking about cases, subjects, locations, casetypes etc.

No, we are talking about a new concept. Custom Objects.

## About the name

To make things *a little less* confusing. We chose to call it "custom objects",
instead of "objects". Because in the world of development, objects can mean a
lot of things. So it is just one concept: "custom object" is an objects which
can be defined by our end users.

## Explanation by example

A custom object is a set of data according and the set of data is restricted by
a definition, which we call a "custom object type". Let's start by using an
example.

Let's assume we want to make an inventory of our old media collection, se we
can start our own little "private amazon" and sell that stuff online. When we
look in our drawer, we might find the following media types:

* A CD with audio
* A DVD with concert recordings
* Some great vinyl from our previouw DJ-era

These are all types of media, but they are a little bit different from one
another. For instance, the DVD is the only media type containing visual concert
registrations. And the vinyl comes in different sizes (7 inc, 10 inch and 12
inch).

To catalog these items, we could create three "custom object types"
* CD
* DVD
* Vinyl

And because we want to register information regarding the "media type", we
could define a set of fields. For instance, the following fields are true for
all of them, so we can define them in every media type:

* The title (text)
* Maybe a description (text)
* The total time of the media (int: minutes)

And for the different types, we might end up with an extra field for the
"vinyl":

* The size of the media in inch (enum: 7/10/12)

**In summary**:

We now created three custom object types. All with one filled attribute "name"
(cd, vinyl or dvd) and a couple of custom fileds (title, description, etc).
Which could like this for vinyl:

![vinyl](../../assets/concepts_custom_objects_vinyl.png)

You can imagine that we can introduce this concept to all kind of stuff, like
an inventory of "computers" within our organization, with *custom fields* like
"cpu", "memory", "diskspace", "manifacturer", etc.

## A complete picture

Looking at the above example, you could say we miss some stuff. For instance, I
do not find the author of the track. I have no idea of knowing that the CD with
title "Stand by Me" is from "Ben. E. King".

We could solve this by just adding an extra field, but we might want to get a
list of every piece of Vinyl from a certain artist. This is where relationships
come in. We also might want to restrict the viewing of the objects from this
type and we want to know what is changed to this type by adding some kind of
logging.

And when changing stuff, we do not want to affect objects created in the past
with this change, so we want to have some kind of versioning for the types.

So summarized:

* Fixed fields (attributes)
* Custom fields (custom_fields)
* Relationships
* Authorizations
* Logging

![vinyl](../../assets/concepts_custom_objects_relationships.png)

## Back to our world

So what does this mean in relation to our government? Well, there are a lot of
objects within our government. But most of them are related to locations, think
of "custom object types" like:

* Water
* Bridge
* Water pump station

Or closer to our company:

* personnel contract
* laptop
* leasecar
* purchase contract
* client contract

## To the code!

### DDD

The implementation of "custom objects" take place in the **case_management**
domain in our zsnl_domains repository. Here you find our entities and
repositories:

* custom_object
* custom_object_type

Which should be self-explanetory by now.

### JSON:API

We made a couple of choices to translate our custom_object to a json:api ouput. The
format of json:api is actually very close to our custom_object concept. In json:api
we find:

* attributes
* relationships

But we were missing a spot to show the user defined attributes. We chose to make
it explicit, and not include it in the basic set of attributes, but created
our own block: "custom_fields".

In short:

```
{
    "data": {
        "type": "custom_object",
        "id": "c7ca18a1-a8f0-475d-b335-453a327d7736",
        "meta": {
            "summary": "My Girl"
        },
        "attributes": {
            "name": "Vinyl",
            "title": "My Girl",
            "version_independent_uuid": "2a05ff52-e2f9-4626-a587-2ffcde4bd409",
            "custom_fields": {
                "media_title": {
                    "type": "text",
                    "value": "Test"
                },
                "media_minutes": {
                    "type": "int",
                    "value": "73"
                },
                "media_size": {
                    "type": "dropdown",
                    "value": "12"
                }
            }
            [...]
        },
        "relationships": {
            "artist": {
                "data": {
                    "type": "artist",
                    "id": "574ff4d9-2705-450e-9e99-29e139304145"
                },
                "links": {
                    "self": "/api/v2/cm/custom_object_type/get_custom_object_type?uuid=20b3d45d-97c9-480e-9d36-1652922cb6dc"
                }
            }
        }
    [...]
    }
}
```

## In depth: Custom Object Type

### Structure:

The custom object type consists of the following sets of data:

* name: name of the objecttype, which is getting copied to the name of a object
* title: a field which accepts "magic_strings", so we can create a unique title
  for the created object
* external_reference: a field which accepts "magic_strings", so we can create a
  unique title for the created object
* custom_field_definition: the definition of an object
* archive_metadata: some meta information which get copied to a created object
* relationship_definition: definition of what kind of relationships are allowed
  for the created objects
* authorizations: who is able to view these objects
* audit_log: a log entry for this version
* status: the status of this custom_object_type
* version: the version number of this object

This information is saved to the database in two tables:

* custom_object_type
* custom_object_type_version

We use these two tables to implement versioning. The `custom_object_type`
table contains a unique row for every "object_type". The
`custom_object_type_version` table contains a unique row for every version. So
for instance, there is one row in `custom_object_type_ for the media type "vinyl",
but there could be a couple of versions of the media type "vinyl" in the table
`custom_object_type_version`.
