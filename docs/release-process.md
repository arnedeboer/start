# Release process

As a team we want to go to a situation where continuous integrating and
continuous delivery (CI/CD) is our standard practice. With the transition to
the new XCP this is something we can set in motion.

To go from the current situation to CI/CD there are several steps to take.
One of the first step to take is that the deployment is done from the Gitlab
CI/CD to the different development, test and customer environments in the XCP.

## Current process

Currently there is a release cycle of six weeks or three sprints.
The main focus of the first sprint, in the release cycle, is adding new features
The second sprint the preprod customer environment is updated and testers
and customers test the new features. Bugs and issues are solved in that sprint.
The third sprint's main goal is to update production.

To support this release cycle there are four branches and each feature is
developed on its own feature branch. The four branches master, development,
preprod and production have a one on one connection to the different environments

| Branch      | Environment                 |
|-------------|-----------------------------|
| master      | mintlab.zaaksysteem.nl      |
| development | develeopment.zaaksysteem.nl |
| preprod     | preprod.zaaksysteem.nl      |
| production  | productie.zaaksysteem.nl    |
| feauture    | localhost developer         |

### Branching strategy

```mermaid
%%{init: { 'logLevel': 'debug', 'theme': 'default' } }%%
gitGraph
   commit
   branch feature_1
   branch development
   checkout feature_1
   commit
   commit
   checkout development
   merge feature_1
   checkout main
   merge feature_1
   branch feature_2
   checkout feature_2
   commit
   commit
   commit
   checkout development
   merge feature_2
   checkout main
   merge feature_2
   branch preprod
   commit
   checkout main
   branch feature_3
   commit
   commit
   checkout development
   merge feature_3
   checkout main
   merge feature_3
   checkout preprod
   merge main
   branch production
   checkout production
   commit
```

### Process

- All new features are developed in their own branch and start from the master branch.
- To test the feature they are merged to development.
- When there is a merge to development the pipeline starts running and new artifacts are created. These artifacts are deployed to development.zaaksysteem.nl
- After the deployment to development.zaaksysteem.nl:
  - The new feature is tested by the e2e tests
  - The new feature is tested by the developer/tester
- When the new feature is approved it is merged to master
- The pipeline starts running on master and creates new artifacts that are tagged and deployed to mintlab.zaaksysteem.nl and master.zaaksysteem.nl
- After deployment:
  - The deploy is tested by the e2e tests on master.zaaksysteem.nl
  - The new version is used by support on mintlab.zaaksysteem.nl
- At the end of the sprint master is merged to preprod
- The pipeline starts running and builds new artifacts and deploys these automatically to preprod.zaaksysteem.nl
- After the deployment to preprod.zaaksysteem.nl:
  - The e2e tests run on preprod.zaaksysteem.nl
  - The new release is tested by stakeholders
- When the tests where successful all code is merged from preprod to the production branch
- The pipeline creates new artifacts from the production branch
- A release tag is added to the production branch
- A pipeline starts running when the release tag is added and the production environment is updated.

### Deploy critically bug fixes

When critical bugs are found than these will be fixed on developoment and master
Then the fixes are up-merged to preprod and production and are delivered on the
corresponding environments

## Future

- Find a way to reduce the number of branches and therefore elimination risky and timeconsuming merges
- Add enough (e2e) tests to feel confident to deploy to production
- Make small changes and reduce the livetime of feature branches. Max two days before a feature branch is tested,merged and deployed.
- Start using feature flags instead of long lived feature branches. This will eliminate lare merge requests and difficult merges. And it will give an early warning when something breaks.

background information: <https://minimumcd.org/minimumcd/>
