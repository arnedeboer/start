# Merging Python changes to development

The Python code is available in one git repository. To make the code available
on `development.zaaksysteem.nl`, the following steps should be followed:

## Library

For each library/dependency (minty-\*, domains, etc.) you made changes too,
that's not its own server/service:

1. Checkout the `development` branch locally, make sure you have the latest
   version.

   example:

    ```bash
    git checkout development
    git pull
    ```

   *Note:* This branch gets reset to `master` at the start of every sprint,
   so you may have to `git reset` your local copy.

2. Merge your feature-branch into `development`:

   `git merge --ff username/minty-123-my_cool_feature`

3. Push to `development`:

   `git push origin development`

## Services (http, consumer)

Next, go to the services depending on the changed library. If you also made a
branch with changes there (usually you do: API specs, etc.) follow these steps:

1. Checkout the `development` branch locally, make sure you have the latest
   version.

   example:

    ```bash
    git checkout development
    git pull
    ```

   *Note:* This branch gets reset to `master` at the start of every sprint,
   so you may have to `git reset` your local copy.

2. Merge your feature-branch into `development`:

   `git merge --ff username/minty-123-my_cool_feature`

3. In `requirements/base.txt`, edit the updated dependencies to point to the
   development branch and commit. For instance:

   `git+https://gitlab.com/minty-python/zsnl_domains.git@development`

   Commit this change.

4. Push to `development`:

   `git push origin development`

If there were no other changes (say, you made a bugfix in some business logic
in a domain), the steps are as follows:

1. Checkout the `development` branch locally, make sure you have the latest
   version.

   example:

    ```bash
    git checkout development
    git pull
    ```

   *Note:* This branch gets reset to `master` at the start of every sprint,
   so you may have to `git reset` your local copy.

2. In `requirements/base.txt`, edit the updated dependencies to point to the
   development branch and commit. For instance:

   `git+https://gitlab.com/minty-python/zsnl_domains.git@development`

   If this is already the case, add a commented line at the bottom of the file.
   We usually put something there like "Force rebuild"

   Commit this change.

3. Push to `development`:

   `git push origin development`

4. Once the build is finished (check the Gitlab web interface, CI/CD pipelines
   page),the newly built version will be deployed to `development.zaaksysteem.nl`
   automatically.

## Jira

Now you can move your subtask, story or bug to the "Test" column in Jira.

In the case of a bug or story, this tell the tester(s) that they can start
their work on the ticket.

If the ticket you moved is a subtask, talk to the frontend developer who is
going to use your code, and tell them it's ready for testing.

# Merging to master

Merging to master works in the same order: first libraries, then services.

## Libraries

1. Push your branch (you should be doing this regularly anyway, in case your
   laptop breaks down):

   `git push origin username/minty-123-my_cool_feature`

2. Create a merge request for this branch
3. Once the merge request has been approved, click the green `Merge` button.
4. Locally, switch to the `master` branch
5. Activate the virtual environment for the library `. ../.venv/xxx/bin/activate`
6. Run `bumpversion`: `bumpversion patch`
7. Push the newly generated tag & commit: `git push origin master v1.2.3`

(we want to automate this full section)

## Services (http, consumer)

1. Make sure your dependencies are merged first.
2. Update your dependencies (`requirements/base.txt`) to the new version in
   your branch
3. Push your branch (you should be doing this regularly anyway, in case your
   laptop breaks down):

   `git push origin username/minty-123-my_cool_feature`

4. Create a merge request for this branch
5. Once the merge request has been approved, click the green `Merge` button.
6. Done! Code will be automatically deployed to, among others, `mintlab.zaaksysteem.nl`
