// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable react-hooks/rules-of-hooks */

import useUnauthorizedBanner from '@zaaksysteem/common/src/hooks/useUnauthorizedBanner';
import { hasCapability } from '@zaaksysteem/common/src/hooks/useSession';
import { CapabilitiesType } from '@zaaksysteem/common/src/hooks/useSession/useSession.types';

const useBannerForCapability = (capability: CapabilitiesType) =>
  useUnauthorizedBanner(session => hasCapability(session, capability));

export const useAdminBanner = {
  system: () => useBannerForCapability('admin'),
  catalog: () => useBannerForCapability('beheer_zaaktype_admin'),
  users: () => useBannerForCapability('useradmin'),
};
