// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { runQueue } from './runQueue';

/**
 * @test {runQueue}
 */
describe('The `runQueue` module', () => {
  let firstFunction;
  let secondFunction;
  let thirdFunction;

  beforeEach(() => {
    firstFunction = jest.fn();
    secondFunction = jest.fn();
    thirdFunction = jest.fn();
  });

  test('executes an array of functions', () => {
    runQueue([firstFunction, secondFunction, thirdFunction]);
    expect(firstFunction).toHaveBeenCalled();
    expect(secondFunction).toHaveBeenCalled();
    expect(thirdFunction).toHaveBeenCalled();
  });

  test('executes an array of functions with a givven parameter', () => {
    runQueue([firstFunction, secondFunction, thirdFunction], 'foo');
    expect(firstFunction).toHaveBeenCalledWith('foo');
    expect(secondFunction).toHaveBeenCalledWith('foo');
    expect(thirdFunction).toHaveBeenCalledWith('foo');
  });

  test('executes an array of functions with an array of parameters', () => {
    runQueue([firstFunction, secondFunction, thirdFunction], ['foo', 'bar']);
    expect(firstFunction).toHaveBeenCalledWith('foo', 'bar');
    expect(secondFunction).toHaveBeenCalledWith('foo', 'bar');
    expect(thirdFunction).toHaveBeenCalledWith('foo', 'bar');
  });
});
