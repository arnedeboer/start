// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useRouteError } from 'react-router';
import Exception from '../Exception';

export default () => {
  const error = useRouteError() as { message: string; stack: string } | void;

  return error ? (
    <Exception
      reason={error.message}
      trace={error.stack.trim().replace(/\n\s+/gm, '\n')}
    />
  ) : null;
};
