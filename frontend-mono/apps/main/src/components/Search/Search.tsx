// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useRef, useEffect } from 'react';
import TextField from '@mintlab/ui/App/Material/TextField';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { Box } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import {
  queryToObject,
  objectToQuery,
} from '../../modules/catalog/library/searchQuery';
import { useSearchStyles } from './Search.style';

const Search = ({
  query = '',
  formDefinition = [],
  rules = [],
  placeholder = '',
  onClear = () => {},
  onSearch = (term: string) => {},
  scope = '',
}) => {
  const style = useSearchStyles();
  const [value, setValue] = useState(query);
  const [open] = useState(false);
  const previousQuery = useRef();
  const queryObject = queryToObject(value);
  const formDefinitionWithValues = formDefinition.map(item =>
    //@ts-ignore
    queryObject[item.name] ? { ...item, value: queryObject[item.name] } : item
  );
  const clearValue = () => {
    setValue('');
    onClear && onClear();
  };

  const icon = (
    <Box sx={style.icon}>
      <Icon size="small" color="inherit">
        {iconNames.search}
      </Icon>
    </Box>
  );
  const closeAction = value || query ? clearValue : undefined;

  useEffect(() => {
    if (previousQuery.current !== query) {
      setValue(query);
    }

    //@ts-ignore
    previousQuery.current = query;
  });

  //@ts-ignore
  const { fields, values } = useForm({
    formDefinition: formDefinitionWithValues,
    rules,
  });

  const openedAndHasForm = open && formDefinition.length > 0;

  if (openedAndHasForm) {
    setValue(objectToQuery(values));
  }

  return (
    <Box sx={style.wrapper}>
      <TextField
        disabled={open}
        value={value}
        onChange={event => setValue(event.target.value)}
        placeholder={placeholder}
        onKeyPress={event => {
          const { key } = event;

          if (key.toLowerCase() === 'enter' && value && value.length > 0) {
            onSearch && onSearch(value);
          }
        }}
        startAdornment={icon}
        closeAction={closeAction}
        scope={scope}
        variant="generic2"
      />
      {openedAndHasForm &&
        fields.map(({ FieldComponent, name, ...rest }) => {
          const props = {
            ...cloneWithout(rest, 'type', 'mode'),
            compact: true,
            name,
            key: `attribute-form-component-${name}`,
            scope: `attribute-form-component-${name}`,
          };

          return (
            <FormControlWrapper key={name} {...props}>
              <FieldComponent {...props} />
            </FormControlWrapper>
          );
        })}
    </Box>
  );
};

export default Search;
