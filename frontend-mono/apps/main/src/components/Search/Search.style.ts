// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/material';

export const useSearchStyles = () => {
  const {
    mintlab: { greyscale },
  } = useTheme<Theme>();

  return {
    wrapper: {
      maxWidth: 700,
      margin: '0 auto',
    },
    icon: {
      color: greyscale.evenDarker,
    },
  };
};
