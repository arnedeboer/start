// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { setLegacyBodyClass } from '../library/legacy';
import InlineFrame from './InlineFrame';

type IFramePropsType = {
  url: string;
};

export const InlineFrameLoader = ({ url }: IFramePropsType) => {
  const [t] = useTranslation();
  const [loading, setLoading] = React.useState(true);

  return (
    <React.Fragment>
      {loading && (
        <div style={{ position: 'absolute', top: '120px', left: '50vw' }}>
          <Loader active={true} />
        </div>
      )}
      <InlineFrame
        t={t}
        onLoad={() => setLoading(false)}
        onOverlayOpen={() => setLegacyBodyClass(true)}
        onOverlayClose={() => setLegacyBodyClass(false)}
        url={url}
      />
    </React.Fragment>
  );
};
