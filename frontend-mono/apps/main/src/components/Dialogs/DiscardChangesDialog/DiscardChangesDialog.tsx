// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { TFunction } from 'i18next';
import Alert from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';

type DiscardChangesDialogPropsType = {
  t: TFunction;
  invoke: (arg1: any) => void;
  hide: () => void;
  options: any;
};
const DiscardChangesDialog = ({
  t,
  hide,
  options,
  invoke,
}: DiscardChangesDialogPropsType) => (
  <Alert
    open={true}
    title={t('dialog:discardChanges:title')}
    icon="alarm"
    primaryButton={{
      text: t('dialog:ok'),
      onClick: () => {
        invoke({ ...options, force: true });
        hide();
      },
    }}
    secondaryButton={{
      text: t('dialog:cancel'),
      onClick: () => hide(),
    }}
    onClose={() => hide()}
  >
    {t('dialog:discardChanges:text')}
  </Alert>
);

export default DiscardChangesDialog;
