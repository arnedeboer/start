// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export { default as Alert } from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';
export { default as Dialog } from './Dialog/Dialog';
export { default as Error } from './Error/Error';
