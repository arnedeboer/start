// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const convertBlobToBase64 = (blob: Blob) =>
  new Promise<string>((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      typeof reader.result === 'string' && resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

export const applyOdfCss = () => {
  const style = document.createElement('style');

  style.innerText = `
    .webodf-virtualSelections {
      margin: 10px 15px;
      box-shadow: 0px 0px 10px 1px #b2b2b2;
    }
  `;

  document.body.appendChild(style);
  document.body.style.backgroundColor = '#eee';
  document.body.style.boxSizing = 'border-box';
  document.body.style.margin = '0';
  document.body.style.fontSize = '15px';
  //@ts-ignore
  [...document.head.children]
    //@ts-ignore
    .find(el => el.href && el.href.includes('/main.css'))
    .remove();
};

export const startOdfSession = (
  odfCanvas: any,
  setParams: any,
  memberId = 'localuser'
) => {
  //@ts-ignore
  const gui = window.gui;
  //@ts-ignore
  const ops = window.ops;
  const session = new ops.Session(odfCanvas);
  const odfDocument = session.getOdtDocument();
  const cursor = new gui.ShadowCursor(odfDocument);
  const sessionController = new gui.SessionController(
    session,
    memberId,
    cursor,
    {
      directTextStylingEnabled: true,
      directParagraphStylingEnabled: true,
    }
  );
  const formattingController =
    sessionController.getDirectFormattingController();

  const viewOptions = {
    editInfoMarkersInitiallyVisible: false,
    caretAvatarsInitiallyVisible: false,
    caretBlinksOnRangeSelect: true,
  };
  const caretManager = new gui.CaretManager(
    sessionController,
    odfCanvas.getViewport()
  );
  const selectionViewManager = new gui.SelectionViewManager(
    gui.SvgSelectionView
  );
  const sessionConstraints = sessionController.getSessionConstraints();
  new gui.SessionView(
    viewOptions,
    memberId,
    session,
    sessionConstraints,
    caretManager,
    selectionViewManager
  );
  selectionViewManager.registerCursor(cursor, true);
  sessionController.setUndoManager(new gui.TrivialUndoManager());

  var op = new ops.OpAddMember();
  op.init({
    memberid: memberId,
    setProperties: {
      fullName: '',
      color: 'black',
      imageUrl: '',
    },
  });
  session.enqueue([op]);

  sessionController.insertLocalCursor();
  sessionController.startEditing();

  formattingController.subscribe(
    gui.DirectFormattingController.textStylingChanged,
    (params: any) => {
      setParams(params);
    }
  );

  formattingController.subscribe(
    gui.DirectFormattingController.paragraphStylingChanged,
    (params: any) => {
      setParams(params);
    }
  );

  return {
    formattingController,
    sessionController,
  };
};
