// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useEmailTemplatePreviewerStyle = makeStyles(() => ({
  container: {
    width: '100%',
    backgroundColor: '#eee',
  },
  row: {
    borderBottom: '1px solid lightgrey',
    padding: 10,
  },
  iframe: {
    width: '100%',
    border: 'none',
    margin: '10px -8px 0px -8px',
  },
  shadowEl: {
    padding: 10,
  },
  attachment: {
    boxShadow: '2px 2px 5px 0px rgba(0,0,0,0.15)',
    padding: '12px',
  },
}));
