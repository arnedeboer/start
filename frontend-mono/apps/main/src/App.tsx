// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { RouterProvider } from 'react-router';
import { QueryClientProvider } from '@tanstack/react-query';
import ThemeProvider from '@mintlab/ui/App/Material/ThemeProvider/ThemeProvider';
import LoginWrapper from '@zaaksysteem/common/src/components/LoginWrapper';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { LicenseInfo } from '@mui/x-license-pro';
import router from './Routes';
import { queryClient } from './queryClient';

if (process.env.MUI_LICENSE_KEY) {
  LicenseInfo.setLicenseKey(process.env.MUI_LICENSE_KEY);
}

function App() {
  const [t] = useTranslation();
  const [, addMessages] = useMessages();

  React.useEffect(() => {
    addMessages(
      t('common:snackMessages', {
        returnObjects: true,
      })
    );
  }, []);

  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider>
        <LoginWrapper>
          <React.Suspense fallback={<Loader />}>
            <RouterProvider router={router} />
          </React.Suspense>
        </LoginWrapper>
      </ThemeProvider>
    </QueryClientProvider>
  );
}

export default App;
