// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { performBulkAction } from '../DataStore.library';
import { DataTypeType } from '../DataStore.types';
import { useCleanUpDialogStyles } from './CleanUpDialog.style';

type FiltersDialogPropsType = {
  dataType: DataTypeType;
  setSnack: (text: string) => void;
  onClose: () => void;
  open: boolean;
  refreshData: () => void;
};

const FiltersDialog: React.ComponentType<FiltersDialogPropsType> = ({
  dataType,
  setSnack,
  onClose,
  open,
  refreshData,
}) => {
  const [t] = useTranslation('dataStore');
  const classes = useCleanUpDialogStyles();
  const dialogEl = useRef();

  const title = t('cleanUp.dialog.title');

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'dataStore-cleanup-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="delete"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.contentWrapper}>
            <span>{t('cleanUp.dialog.explanation')}</span>
            <span>{t('cleanUp.dialog.warning')}</span>
          </div>
        </DialogContent>
        <>
          <Divider />
          <DialogActions>
            {createDialogActions(
              [
                {
                  color: 'danger',
                  text: title,
                  onClick: () => {
                    performBulkAction({
                      type: 'delete',
                      selectedRows: [],
                      everythingSelected: true,
                      dataType,
                      filters: {
                        active: '2',
                        cases: '2',
                      },
                    }).then(() => {
                      setSnack(t('cleanUp.snack'));
                      onClose();
                      refreshData();
                    });
                  },
                },
                {
                  text: t('common:dialog.cancel'),
                  onClick: onClose,
                },
              ],
              'datastore-cleanup-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default FiltersDialog;
