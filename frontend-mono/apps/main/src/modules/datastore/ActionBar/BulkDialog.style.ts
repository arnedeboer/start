// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useBulkDialogStyles = makeStyles(({ typography }: Theme) => ({
  contentWrapper: {
    width: 500,
    fontFamily: typography.fontFamily,
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
  },
  checkboxLabel: {
    fontSize: typography.body2.fontSize,
  },
}));
