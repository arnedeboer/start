// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import { DataTypeType, RowType, ActionTypeType } from '../DataStore.types';

type CellRendererType = (
  rowData: {
    dataKey: any;
    rowData: RowType;
  },
  dataType: DataTypeType,
  updateContact: (actionType: ActionTypeType, id: number) => void,
  t: i18next.TFunction,
  classes: any
) => React.ReactElement;

/* eslint complexity: [2, 9] */
export const cellRenderer: CellRendererType = (
  { dataKey, rowData },
  dataType,
  updateContact,
  t,
  classes
) => {
  switch (dataKey) {
    case 'create': {
      return (
        <IconButton
          title={t('table.actions.create.button')}
          onClick={(event: any) => {
            event.preventDefault();

            updateContact('create', rowData.id);
          }}
        >
          <Icon size="small">{iconNames.add_circle_outline}</Icon>
        </IconButton>
      );
    }
    case 'delete': {
      return (
        <IconButton
          title={t('table.actions.delete.button')}
          onClick={(event: any) => {
            event.preventDefault();

            updateContact('delete', rowData.id);
          }}
        >
          <Icon size="small">{iconNames.delete}</Icon>
        </IconButton>
      );
    }
    case 'uuid': {
      const uuid = rowData.realUuid;
      const contactType =
        dataType === 'NatuurlijkPersoon' ? 'person' : 'organization';

      return (
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={`/main/contact-view/${contactType}/${uuid}`}
        >
          {uuid}
        </a>
      );
    }
    default: {
      return <div className={classes.tableCell}>{rowData[dataKey] || '-'}</div>;
    }
  }
};
