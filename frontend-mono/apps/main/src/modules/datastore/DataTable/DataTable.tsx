// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { SortDirectionType } from 'react-virtualized';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Pagination from '@mintlab/ui/App/Material/Pagination';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { adminSnackbarPadding } from '../../../admin.style';
import {
  ActionTypeType,
  DataType,
  RowType,
  DataTypeType,
  FiltersType,
  SetFiltersType,
  SetSelectedType,
} from '../DataStore.types';
import { useDataTableStyles, useTableStyles } from './DataTable.style';
import {
  getColumns,
  columnWidth,
  actionColumns,
  actionColumnWidth,
  updateContactAction,
} from './DataTable.library';

export const toggleSelected = (
  selected: string[],
  setSelected: SetSelectedType,
  rowData: RowType
) => {
  if (!selected.includes(rowData.uuid)) {
    setSelected([...selected, rowData.uuid]);
  } else {
    setSelected(selected.filter((uuid: string) => uuid !== rowData.uuid));
  }
};

type DataTablePropsType = {
  data?: DataType;
  dataType: DataTypeType;
  filters: FiltersType;
  setFilters: SetFiltersType;
  refreshData: () => void;
  selectionProps: useSelectionBehaviourReturnType;
};

const DataTable: React.ComponentType<DataTablePropsType> = ({
  data,
  dataType,
  filters,
  setFilters,
  refreshData,
  selectionProps,
}) => {
  const classes = useDataTableStyles();
  const tableStyles = useTableStyles();
  const sortableTableStyles = useSortableTableStyles();
  const [t] = useTranslation('dataStore');
  const [snack, setSnack] = useState('');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  if (!data) {
    return <Loader />;
  }

  const updateContact = (actionType: ActionTypeType, id: number) => {
    updateContactAction(actionType, dataType, id)
      .then(() => {
        refreshData();
        const snack = t(`table.actions.${actionType}.success`, { id });

        setSnack(snack);
      })
      .catch(openServerErrorDialog);
  };

  const columns = getColumns(dataType, updateContact, t, classes);
  const { rows } = data;

  const tableWidth =
    (columns.length - actionColumns.length) * (columnWidth + 10) +
    actionColumns.length * (actionColumnWidth + 10);

  return (
    <div className={classes.wrapper}>
      <div className={classes.tableWrapper}>
        <div
          style={{
            flex: '1 1 auto',
            width: `${tableWidth}px`,
            paddingRight: '16px',
            height: `calc(${rows.length} * 42px + 50px)`,
          }}
        >
          <SortableTable
            styles={{ ...sortableTableStyles, ...tableStyles }}
            rows={rows}
            //@ts-ignore
            columns={columns}
            rowHeight={42}
            loading={false}
            noRowsMessage={t('table.noResults')}
            sortDirectionDefault="DESC"
            sortInternal={false}
            sorting="column"
            onSort={(sortBy: string, sortDirection: SortDirectionType) => {
              setFilters({
                ...filters,
                zapi_page: 0,
                zapi_order_by: sortBy,
                zapi_order_by_direction: sortDirection,
              });
            }}
            {...selectionProps}
          />
        </div>
      </div>
      <div className={classes.pagination}>
        <Pagination
          scope="datastore-pagination"
          component={'div'}
          count={data.count}
          labelRowsPerPage={`${t('table.labelRowsPerPage')}:`}
          rowsPerPageOptions={[5, 10, 20, 50]}
          rowsPerPage={filters.zapi_num_rows}
          changeRowsPerPage={(event: any) =>
            setFilters({
              ...filters,
              zapi_page: 0,
              zapi_num_rows: event.target.value,
            })
          }
          page={filters.zapi_page}
          getNewPage={(page: number) =>
            setFilters({ ...filters, zapi_page: page })
          }
          labelDisplayedRows={({ from, to, count }: any) =>
            `${from}-${to} ${t('common:of')} ${count}`
          }
        />
      </div>
      <Snackbar
        scope="datastore-table-snack"
        handleClose={() => setSnack('')}
        message={snack}
        open={Boolean(snack)}
        sx={adminSnackbarPadding}
      />
      {ServerErrorDialog}
    </div>
  );
};

export default DataTable;
