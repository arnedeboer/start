// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { useAdminBanner } from '../../library/auth';
import DataStore from './DataStore';
import locale from './DataStore.locale';

type DatastoreRouteType = {};

const DataStoreModule: React.ComponentType<DatastoreRouteType> = () => {
  const [t] = useTranslation('dataStore');
  const banner = useAdminBanner.system();

  return (
    banner || (
      <>
        <DataStore />
        <TopbarTitle title={t('dataStore')} />
      </>
    )
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="dataStore">
    <DataStoreModule />
  </I18nResourceBundle>
);
