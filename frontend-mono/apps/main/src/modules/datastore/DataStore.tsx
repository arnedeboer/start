// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { DataType, DataTypeType, FiltersType } from './DataStore.types';
import { defaultDataType, defaultFilters, getData } from './DataStore.library';
import ActionBar from './ActionBar/ActionBar';
import DataTable from './DataTable/DataTable';

const DataStore = () => {
  const [t] = useTranslation('dataStore');
  const [dataType, setDataType] = useState<DataTypeType>(defaultDataType);
  const [filters, setFilters] = useState<FiltersType>(defaultFilters);

  const { data, error } = useQuery<DataType, V2ServerErrorsType>(
    ['data', dataType, filters],
    () => getData(dataType, filters),
    {
      onSuccess: () => selectionProps.resetAll(),
    }
  );
  const selectionProps = useSelectionBehaviour({
    rows: data?.rows,
    selectEverythingTranslations: t('table.selectEverything', {
      returnObjects: true,
    }),
    page: filters.zapi_page,
    resultsPerPage: filters.zapi_num_rows,
  });

  const queryClient = useQueryClient();
  const refreshData = () => queryClient.invalidateQueries(['data']);
  const { selectedRows, everythingSelected } = selectionProps;

  return (
    <Sheet>
      {error && <ServerErrorDialog err={error} />}
      <ActionBar
        data={data}
        dataType={dataType}
        filters={filters}
        selectedRows={selectedRows}
        everythingSelected={everythingSelected}
        setDataType={setDataType}
        setFilters={setFilters}
        refreshData={refreshData}
      />
      <DataTable
        data={data}
        dataType={dataType}
        filters={filters}
        setFilters={setFilters}
        refreshData={refreshData}
        selectionProps={selectionProps}
      />
    </Sheet>
  );
};

export default DataStore;
