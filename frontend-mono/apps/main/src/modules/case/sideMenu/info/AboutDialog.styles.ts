// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAboutDialogStyles = makeStyles(({ typography }: Theme) => ({
  wrapper: {
    width: '500px',
    display: 'flex',
    flexDirection: 'column',
    fontFamily: typography.fontFamily,
  },
  itemWrapper: {
    padding: 20,
    display: 'flex',
    justifyContent: 'space-between',
  },
  label: {
    fontWeight: typography.fontWeightBold,
  },
  value: {},
}));
