// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { CaseActionType, CaseObjType, CaseTypeType } from '../../Case.types';
import { useCaseActionDialogStyles } from './CaseActionDialog.style';
import { caseActions } from './Caseactions.library';
import getFormAction from './CaseActions.requests';
import {
  getFormDefinition,
  getRules,
} from './CaseActionDialog.formDefinitions';
import { AuthorizationField } from './AuthorizationField/AuthorizationField';

type CaseActionDialogPropsType = {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  caseAction: CaseActionType;
  onClose: () => void;
  refreshCaseObj: () => void;
};

const CaseActionDialog: React.ComponentType<CaseActionDialogPropsType> = ({
  caseObj,
  caseType,
  caseAction,
  onClose,
  refreshCaseObj,
}) => {
  const [t] = useTranslation('case');
  const classes = useCaseActionDialogStyles();
  const dialogEl = useRef();
  const session = useSession();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const title = t(`caseActions.${caseAction}.title`);
  const formDefinition = getFormDefinition[caseAction]({
    t,
    caseObj,
    caseType,
  });
  const rules = getRules[caseAction]();
  const validationMap = generateValidationMap(formDefinition);

  let {
    fields,
    formik: { isValid, values },
  } = useForm({
    formDefinition,
    validationMap,
    rules,
    fieldComponents: {
      AuthorizationField,
    },
  });

  return (
    <>
      {ServerErrorDialog}
      <Dialog
        disableBackdropClick={true}
        open={caseActions.includes(caseAction)}
        onClose={onClose}
        scope={`case-${caseAction}-dialog`}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="info"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.wrapper}>
            {fields.map(
              ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
                const props = cloneWithout(rest, 'mode');

                return (
                  <FormControlWrapper
                    {...props}
                    label={suppressLabel ? false : props.label}
                    compact={true}
                    key={`${props.name}-formcontrol-wrapper`}
                  >
                    <FieldComponent
                      {...props}
                      t={t}
                      containerRef={dialogEl.current}
                    />
                  </FormControlWrapper>
                );
              }
            )}
          </div>
        </DialogContent>
        <>
          <Divider />
          <DialogActions>
            {createDialogActions(
              [
                {
                  text: title,
                  disabled: !isValid,
                  onClick: () => {
                    getFormAction[caseAction]({
                      values,
                      caseObj,
                      session,
                    })
                      .then(() => {
                        // this is the only scenario where the caseType of the case is altered
                        // a full reload is warranted for this exceptional scenario
                        if (caseAction === 'changeCaseType') {
                          location.reload();
                          return;
                        }

                        if (caseAction !== 'copy') {
                          refreshCaseObj();
                        }
                        onClose();
                      })
                      .catch(openServerErrorDialog);
                  },
                },
                {
                  text: t('common:dialog.cancel'),
                  onClick: onClose,
                },
              ],
              'case-action-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default CaseActionDialog;
