// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  DEPARTMENT_FINDER,
  ROLE_FINDER,
  RADIO_GROUP,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

const getFormDefinition = ({ t }: { t: i18next.TFunction }, value: any) => [
  {
    name: 'department',
    type: DEPARTMENT_FINDER,
    value: value.department,
    required: true,
    placeholder: t(
      'caseActions.customiseAuthorizations.placeholders.department'
    ),
  },
  {
    name: 'role',
    type: ROLE_FINDER,
    value: value.role,
    required: true,
    placeholder: t('caseActions.customiseAuthorizations.placeholders.role'),
  },
  {
    name: 'capabilities',
    type: RADIO_GROUP,
    value: value.capabilities,
    required: true,
    choices: ['search', 'read', 'readwrite', 'admin'].map(auth => ({
      label: t(
        `caseActions.customiseAuthorizations.authorizations.capabilities.${auth}`
      ),
      value: auth,
    })),
  },
];

export default getFormDefinition;
