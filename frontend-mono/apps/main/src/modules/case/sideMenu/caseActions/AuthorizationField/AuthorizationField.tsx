// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { useListStyle } from '@mintlab/ui/App/Zaaksysteem/List';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { getCapabilities } from './../Caseactions.library';
import { useAuthorizationFieldStyle } from './Authorization.style';
import AuthorizationForm from './AuthorizationForm';
import { AuthorizationType } from './types';

export const AuthorizationField: React.ComponentType<
  FormRendererFormField<any, any, AuthorizationType[]>
> = props => {
  const [t] = useTranslation('case');
  const classes = useAuthorizationFieldStyle();
  const listClasses = useListStyle();
  const { fields, add } = useMultiValueField<any, AuthorizationType>(props);

  if (props.readOnly) {
    const authorizations = props.definition.value;

    return (
      <div className={classes.displayWrapper}>
        {
          //@ts-ignore
          authorizations.map((auth, index) => (
            <div key={index} className={classes.displayItem}>
              <span>{`${auth.department.label}/${auth.role.label}`}</span>
              <span className={classes.displayItemCapability}>
                {getCapabilities(auth.capabilities)
                  .map(auth =>
                    t(
                      `caseActions.customiseAuthorizations.authorizations.capabilities.${auth}`
                    )
                  )
                  .join(', ')}
              </span>
            </div>
          ))
        }
      </div>
    );
  }

  return (
    <div className={classNames(listClasses.listContainer)}>
      {fields.map(fieldProps => (
        <AuthorizationForm {...fieldProps} key={fieldProps.name} t={t} />
      ))}

      <div className={classes.addButtonWrapper}>
        <button
          type="button"
          className={classes.addButton}
          onClick={add as any}
          title={t(
            'caseActions.customiseAuthorizations.authorizations.addButton'
          )}
        >
          {t('caseActions.customiseAuthorizations.authorizations.addButton')}
          <Icon>{iconNames.add_circle_outline}</Icon>
        </button>
      </div>
    </div>
  );
};
