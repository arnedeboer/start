// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    serverErrors: {
      'case_relation/already_exists': 'Deze zaak is al gerelateerd.',
      'case_management/custom_object/object_type/not_found':
        'U heeft onvoldoende rechten om objecten van dit type aan te maken, te bewerken of te verwijderen.',
      'case/completion_date_not_allowed_for_resolved':
        'Afhandeldatum wijzigen kan enkel voor afgehandelde zaken.',
      'case/invalid_completion_date':
        'De afhandeldatum kan niet eerder zijn dan de registratiedatum.',
      'case/invalid_completion_date_in_future':
        'De afhandeldatum kan niet in de toekomst liggen.',
      'case/v1/insufficient_rights':
        'U heeft onvoldoende rechten om deze actie uit te voeren.',
    },
    save: 'Bewaren',
    views: {
      phases: 'Fasen',
      documents: 'Documenten',
      timeline: 'Tijdlijn',
      communication: 'Communicatie',
      location: 'Kaart',
      relations: 'Relaties',
    },
    status: {
      new: 'Nieuw',
      open: 'In behandeling',
      stalled: 'Opgeschort',
      resolved: 'Afgehandeld',
    },
    info: {
      result: 'Resultaat',
      status: 'Status',
      requestor: 'Aanvrager',
      presetClient: 'Vooringevulde aanvrager',
      recipient: 'Ontvanger',
      assignee: 'Behandelaar',
      assignToSelf: 'In behandeling nemen',
      department: 'Afdeling',
      registrationDate: 'Registratiedatum',
      targetDate: 'Streefafhandeldatum',
      completionDate: 'Afhandeldatum',
      location: 'Zaakadres',
      confidentiality: 'Vertrouwelijkheid',
      paymentStatus: 'Betaalstatus',
      about: 'Meer informatie',
    },
    about: {
      caseNumber: 'Zaaknummer',
      caseType: 'Zaaktype',
      summary: 'Extra informatie',
      contactChannel: 'Contactkanaal',
      departmentAndRole: 'Afdeling en rol',
      requestor: 'Aanvrager',
      recipient: 'Ontvanger',
      assignee: 'Behandelaar',
      coordinator: 'Coördinator',
      registrationDate: 'Registratiedatum',
      targetDate: 'Streefafhandeldatum',
      completionDate: 'Afhandeldatum',
      leadTimeLegal: 'Afhandeltermijn wettelijk',
      leadTimeService: 'Afhandeltermijn norm',
      archiveClassificationCode: 'Archiefnominatie',
      destructionDate: 'Uiterste vernietigingsdatum',
      paymentStatus: 'Betaalstatus',
      paymentAmount: 'Zaak bedrag',
      legalBasis: 'Wettelijke grondslag',
      localBasis: 'Lokale grondslag',
      designationOfconfidentiality: 'Openbaarheid',
      responsibleRelationship: 'Verantwoordingsrelatie',
      processDescription: 'Procesbeschrijving',
      requestorSnapshot: 'Correspondentiegegevens bij registratie',
      uuid: 'UUID',
    },
    notification: {
      status: {
        stalled: {
          definite: 'De zaak is opgeschort tot {{date}}.',
          indefinite: 'De zaak is opgeschort voor onbepaalde tijd.',
        },
        closed:
          'Deze zaak is afgehandeld. U kunt geen wijzigingen meer aanbrengen.',
      },
      assignment: {
        acceptAssignment:
          'Deze zaak is aan u toegewezen, maar u heeft hem nog niet in behandeling genomen.',
        assignToSelf: 'Deze zaak heeft nog geen behandelaar.',
        userIsNotAssignee: 'U bent niet de behandelaar van de zaak.',
      },
      requestor: {
        correspondenceAddress:
          'Aanvrager heeft een briefadres/correspondentieadres.',
        deceased: 'Aanvrager is overleden.',
        investigated: 'Aanvrager staat in onderzoek.',
        moved: 'Aanvrager is verhuisd.',
        secret: 'Aanvrager heeft geheimhouding.',
      },
      payment: {
        pending: 'De betaling van de zaak is nog niet afgerond.',
        failed: 'De betaling van de zaak is mislukt.',
        offline:
          'De aanvrager heeft gekozen om de zaak niet online te betalen.',
      },
      jobs: {
        unfinished:
          'Er worden op de achtergrond nog taken uitgevoerd voor deze zaak.',
      },
      actions: {
        payment: 'Betaling verwerken',
        assignToSelf: 'In behandeling nemen',
        seeJobs: 'Bekijk voortgang',
      },
    },
    confidentiality: {
      public: 'Openbaar',
      internal: 'Intern',
      confidential: 'Vertrouwelijk',
    },
    paymentStatus: {
      success: 'Geslaagd',
      failed: 'Niet geslaagd',
      pending: 'Wachten op bevestiging',
      offline: 'Later betalen',
    },
    snack: {
      jobs: 'Achtergrondtaken worden uitgevoerd',
    },
    dialogs: {
      about: {
        title: 'Meer informatie',
      },
      confidentiality: {
        title: 'Vertrouwelijkheid wijzigen',
        warning:
          'Let op! Als u het vertrouwelijkheidsniveau wijzigt naar een niveau waarop u geen rechten heeft op de zaak, kunt u dit niet terugzetten.',
      },
    },
    caseActions: {
      button: 'Zaakacties',
      selection: { title: 'Zaakactie selecteren' },
      assign: {
        title: 'Toewijzing wijzigen',
        allocationType: 'Type toewijzing',
        choices: {
          departmentRole: 'Afdeling en rol',
          employee: 'Specifieke behandelaar',
          self: 'Zelf in behandeling nemen',
        },
        department: 'Afdeling',
        role: 'Rol',
        employee: 'Nieuwe behandelaar',
        placeholders: {
          employee: 'Zoek een behandelaar…',
          department: 'Selecteer een afdeling…',
          role: 'Selecteer een rol…',
        },
        addAsAssignee: 'Toevoegen als betrokkene',
        changeDepartment: 'Ook afdeling wijzigen',
        notify: 'Verstuur e-mail',
        comment: 'Opmerking',
      },
      stall: {
        title: 'Opschorten',
        reason: 'Reden opschorting',
        choices: {
          suspensionType: {
            indefinite: 'Onbepaald',
            definite: 'Bepaald, namelijk',
          },
          termType: {
            calendar_days: 'Kalenderdagen',
            work_days: 'Werkdagen',
            weeks: 'Weken',
            fixed_date: 'Vaste einddatum',
          },
        },
        termType: 'Type termijn',
        termLength: 'Aantal',
        fixedDate: 'Vaste einddatum',
      },
      resume: {
        title: 'Hervatten',
        reason: 'Reden hervatten',
        since: 'Ingangsdatum opschorting',
        until: 'Einddatum opschorting',
      },
      resolvePrematurely: {
        title: 'Vroegtijdig afhandelen',
        reason: 'Reden',
        result: 'Resultaat',
      },
      prolong: {
        title: 'Termijn wijzigen',
        reason: 'Reden wijzigen termijn',
        choices: {
          prolongType: {
            fixedDate: 'Verlengen',
            changeTerm: 'Behandeltermijn wijzigen',
          },
          termType: {
            kalenderdagen: 'Kalenderdagen',
            werkdagen: 'Werkdagen',
            weken: 'Weken',
          },
        },
        termLength: 'Aantal',
        fixedDate: 'Vaste einddatum',
      },
      copy: {
        title: 'Zaak kopiëren',
        text: 'Weet u zeker dat u deze zaak wilt kopiëren?',
      },
      setRequestor: {
        title: 'Aanvrager wijzigen',
        requestor: 'Aanvrager',
        placeholder: 'Selecteer een {{requestorType}}…',
      },
      setAssignee: {
        title: 'Behandelaar wijzigen',
        requestor: 'Behandelaar',
        placeholder: 'Selecteer een medewerker…',
      },
      setCoordinator: {
        title: 'Coördinator wijzigen',
        coordinator: 'Coördinator',
        placeholder: 'Selecteer een medewerker…',
      },
      setDepartmentAndRole: {
        title: 'Afdeling en rol wijzigen',
        department: 'Afdeling',
        role: 'Rol',
        placeholders: {
          department: 'Selecteer een afdeling…',
          role: 'Selecteer een rol…',
        },
      },
      setRegistrationDate: {
        title: 'Registratiedatum wijzigen',
        registrationDate: 'Registratiedatum',
        placeholder: 'Selecteer een datum…',
      },
      setTargetCompletionDate: {
        title: 'Streefafhandeldatum wijzigen',
        targetCompletionDate: 'Streefafhandeldatum',
        placeholder: 'Selecteer een datum…',
      },
      setCompletionDate: {
        title: 'Afhandeldatum wijzigen',
        completionDate: 'Afhandeldatum',
        placeholder: 'Selecteer een datum…',
      },
      setDestructionDate: {
        title: 'Vernietigingsdatum wijzigen',
        destructionDate: 'Vernietigingsdatum',
        reason: 'Reden van wijziging',
        term: 'Termijn',
        choices: {
          termType: {
            fixedDate: 'Nieuwe vernietigingsdatum',
            calculate: 'Herberekenen',
          },
        },
        placeholders: {
          fixedDate: 'Selecteer een datum…',
          reason: 'Selecteer een reden…',
          term: 'Selecteer een term…',
        },
      },
      updateFieldValue: { title: 'Kenmerken wijzigen' },
      setPhase: { title: 'Fase wijzigen' },
      setStatus: { title: 'Status wijzigen' },
      customiseAuthorizations: {
        title: 'Rechten wijzigen',
        cascadeTo: {
          label: 'Geldt ook voor',
          choices: {
            continuation: 'Gerelateerde zaken',
            partial: 'Deelzaken',
            related: 'Vervolgzaken',
          },
        },
        authorizations: {
          addButton: 'Afdeling en rol toevoegen',
          loading: 'Bezig met laden…',
          capabilities: {
            search: 'Zoeken',
            read: 'Raadplegen',
            readwrite: 'Behandelen',
            admin: 'Beheren',
          },
        },
        placeholders: {
          department: 'Selecteer een afdeling…',
          role: 'Selecteer een rol…',
        },
      },
      setResult: { title: 'Resultaat wijzigen' },
      changeCaseType: {
        title: 'Zaaktype wijzigen',
        caseType: 'Zaaktype',
        placeholder: 'Selecteer een zaaktype…',
      },
      setPaymentStatus: { title: 'Betaalstatus wijzigen' },
    },
  },
};
