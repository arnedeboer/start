// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import useSession, {
  hasActiveIntegration,
} from '@zaaksysteem/common/src/hooks/useSession';
import { RelationsPropsType } from './Relations.types';
import { useRelationsStyles } from './Relations.style';
import CaseTables from './Tables/Cases/CaseTables';
import PlannedCasesTable from './Tables/PlannedCases/PlannedCasesTable';
import SubjectsTable from './Tables/Subjects/SubjectsTable';
import ObjectsTable from './Tables/Objects/ObjectsTable';
import CustomObjectsTable from './Tables/CustomObjects/CustomObjectsTable';
import PlannedEmailsTable from './Tables/PlannedEmails/PlannedEmailsTable';

const Relations: React.ComponentType<RelationsPropsType> = ({
  caseObj,
  caseType,
}) => {
  const classes = useRelationsStyles();
  const session = useSession();
  const {
    configurable: { show_object_v1, show_object_v2 },
  } = session;
  const showPlannedEmails = hasActiveIntegration(session, 'email');
  const { uuid, number, canEdit } = caseObj;

  return (
    <div className={classes.wrapper}>
      <CaseTables caseObj={caseObj} caseType={caseType} />
      <PlannedCasesTable
        caseUuid={uuid}
        caseNumber={number}
        canEdit={canEdit}
      />
      <SubjectsTable caseObj={caseObj} canEdit={canEdit} />
      {show_object_v1 && (
        <ObjectsTable
          caseUuid={caseObj.uuid}
          caseNumber={caseObj.number}
          canEdit={canEdit}
          caseType={caseType}
        />
      )}
      {show_object_v2 && (
        <CustomObjectsTable
          caseUuid={caseObj.uuid}
          canEdit={canEdit}
          caseType={caseType}
        />
      )}
      {showPlannedEmails && (
        <PlannedEmailsTable caseNumber={caseObj.number} canEdit={canEdit} />
      )}
    </div>
  );
};

export default Relations;
