// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    familyCases: {
      title: 'Hoofd- en deelzaken',
    },
    relatedCases: {
      title: 'Gerelateerde zaken',
      new: {
        placeholder: 'Begin te typen…',
        label: '',
      },
      draggingText: 'Sleep de zaak en laat los om de volgorde te wijzigen.',
    },
    cases: {
      columns: {
        level: 'Niveau',
        relation_type: 'Type',
        number: '#',
        progressStatus: 'Voortgang',
        casetype_title: 'Zaaktype',
        summary: 'Extra informatie',
        assignee: 'Behandelaar',
        result: 'Resultaat',
        delete: '',
      },
      values: {
        relation_type: {
          initiator: 'Initiator',
          plain: 'Gerelateerd',
          continuation: 'Vervolgzaak',
        },
      },
    },
    plannedCases: {
      title: 'Geplande zaken',
      columns: {
        casetype_title: 'Zaaktype',
        pattern: 'Patroon',
        reach: 'Bereik',
        edit: '',
        delete: '',
      },
      dialog: {
        add: { title: 'Geplande zaak toevoegen', submit: 'Zaak plannen' },
        edit: { title: 'Geplande zaak bewerken', submit: 'Opslaan' },
        labels: {
          casetype: 'Zaaktype',
          copy_relations: 'Objectrelaties kopiëren',
          next_run: 'Vanaf',
          repeating: 'Terugkeerpatroon',
          interval_value: 'Herhaling',
          interval_period: 'Elke',
          runs_left: 'Aantal herhalingen',
        },
        placeholders: {
          casetype: 'Selecteer een zaaktype…',
        },
      },
      values: {
        interval_period: {
          days: 'Dagen',
          weeks: 'Weken',
          months: 'Maanden',
          years: 'Jaren',
        },
        pattern: {
          each: 'Elke',
          once: 'Eenmalig',
        },
        reach: {
          startAt: 'Start op',
          repeats: 'herhalingen',
        },
      },
      add: 'Voeg toe',
      download: 'Download als .csv',
    },
    subjects: {
      title: 'Betrokkenen',
      columns: {
        name: 'Naam',
        type: 'Type',
        role: 'Rol',
        authorized: 'Gemachtigd',
        edit: '',
        delete: '',
      },
      cells: {
        type: {
          person: 'Natuurlijk persoon',
          organization: 'Organisatie',
          employee: 'Medewerker',
        },
      },
      dialog: {
        add: {
          title: 'Betrokkene toevoegen',
          submit: 'Toevoegen',
        },
        edit: {
          title: 'Betrokkene bewerken',
          submit: 'Opslaan',
        },
        labels: {
          subject: 'Betrokkene',
          role: 'Rol betrokkene',
          magic_string_prefix: 'Magicstring prefix',
          authorized: 'Gemachtigd voor deze zaak',
          send_confirmation_email: 'Verstuur bevestiging per e-mail',
          permission: 'Toegestane rechten op de zaak',
        },
        placeholders: {
          subject: 'Selecteer een betrokkene…',
        },
        values: {
          permission: {
            none: 'Geen rechten',
            search: 'Zoeken',
            read: 'Raadplegen',
            write: 'Behandelen',
          },
        },
      },
      add: 'Voeg toe',
    },
    objects: {
      title: 'Gerelateerde objecten v1',
      columns: {
        name: 'Naam',
        type: 'Type',
      },
      relateObject: 'Relateer object',
      dialog: {
        title: 'Object relateren',
        submit: 'Relateren',
        labels: {
          objectType: 'Type object',
          object: 'Te relateren object',
          copyValues: 'Kopieer waarden object naar zaak',
        },
      },
    },
    customObjects: {
      title: 'Gerelateerde objecten v2',
      columns: {
        name: 'Naam',
        type: 'Type',
        customFieldName: 'Kenmerk',
        delete: '',
      },
    },
    plannedEmails: {
      title: 'Geplande e-mails',
      columns: {
        date: 'Ingepland voor',
        template: 'Sjabloon',
        recipient: 'Ontvanger',
      },
      reschedule: 'Herplannen',
    },
    noRowsMessage: 'Geen resultaten',
  },
};
