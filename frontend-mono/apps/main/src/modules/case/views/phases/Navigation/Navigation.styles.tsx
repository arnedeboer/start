// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useNavigationStyles = makeStyles(
  ({ palette: { primary, elephant } }: Theme) => ({
    counter: {
      backgroundColor: primary.main,
    },
    counterInactive: {
      backgroundColor: elephant.dark,
    },
  })
);

export type ClassesType = ReturnType<typeof useNavigationStyles>;
