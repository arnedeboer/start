// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import Typography from '@mui/material/Typography';
import { useEmailPreviewStyle } from './EmailPreview.style';

type EmailPreviewPropsType = {
  caseNumber: number;
  values: {
    notificaties_rcpt: string;
    notificaties_email: string;
    notificaties_body: string;
    notificaties_subject: string;
    case_document_attachments: string[];
  };
};
type EmailTemplateResponseType = {
  to: string;
  subject: string;
  body: string;
};

/* eslint complexity: [2, 9] */
const EmailPreview: React.ComponentType<EmailPreviewPropsType> = ({
  caseNumber,
  values,
}) => {
  const [emailPreviewData, setEmailPreviewData] =
    React.useState<EmailTemplateResponseType | null>(null);
  const classes = useEmailPreviewStyle();
  const [t] = useTranslation('phaseActions');

  React.useEffect(() => {
    request<EmailTemplateResponseType>(
      'POST',
      buildUrl('/api/mail/preview', { case_id: caseNumber }),
      {
        case_id: caseNumber,
        recipient_type: values.notificaties_rcpt,
        to: values.notificaties_email,
        subject: values.notificaties_subject,
        body: values.notificaties_body,
        attachments: values.case_document_attachments,
      }
    ).then(response => {
      setEmailPreviewData(response);
    });
  }, []);

  return emailPreviewData ? (
    <div className={classes.wrapper}>
      <div>
        <Typography variant="h6" classes={{ root: classes.label }}>
          {t(`email.preview.labels.to`)}
        </Typography>
        {/* @ts-ignore */}
        <div>{emailPreviewData.to || '-'}</div>
      </div>
      <div>
        <Typography variant="h6" classes={{ root: classes.label }}>
          {t(`email.preview.labels.cc`)}
        </Typography>
        {/* @ts-ignore */}
        <div>{values.notificaties_cc || '-'}</div>
      </div>
      <div>
        <Typography variant="h6" classes={{ root: classes.label }}>
          {t(`email.preview.labels.bcc`)}
        </Typography>
        {/* @ts-ignore */}
        <div>{values.notificaties_bcc || '-'}</div>
      </div>
      <div>
        <Typography variant="h6" classes={{ root: classes.label }}>
          {t(`email.preview.labels.subject`)}
        </Typography>
        {/* @ts-ignore */}
        <div>{emailPreviewData.subject || '-'}</div>
      </div>
      <div>
        <Typography variant="h6" classes={{ root: classes.label }}>
          {t(`email.preview.labels.body`)}
        </Typography>
        {/* @ts-ignore */}
        <div>{emailPreviewData.body || '-'}</div>
      </div>
      {values.case_document_attachments && (
        <div>
          <Typography variant="h6" classes={{ root: classes.label }}>
            {t(`email.preview.labels.attachments`)}
          </Typography>
          {/* @ts-ignore */}
          <div className={classes.attachmentWrapper}>
            {values.case_document_attachments.map((attachment: string) => (
              <span key={attachment}>{attachment}</span>
            ))}
          </div>
        </div>
      )}
    </div>
  ) : (
    <Loader />
  );
};

export default EmailPreview;
