// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint react/no-danger: 0 */
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { useQuery } from '@tanstack/react-query';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import generateCustomFieldFormDefinition from '@zaaksysteem/common/src/components/form/library/generateCustomFieldFormDefinition';
import generateCustomFieldValues from '@zaaksysteem/common/src/components/form/library/generateCustomFieldValues';
import formatCaseCustomFields from '@zaaksysteem/common/src/components/form/library/formatCaseCustomFields';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { Button } from '@mintlab/ui/App/Material/Button';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import useSession, {
  hasCapability,
} from '@zaaksysteem/common/src/hooks/useSession';
import { CaseObjType, CaseTypeType } from '../../../../Case.types';
import { ObjectTypeType, ObjectType } from './ObjectForm.types';
import {
  createObject,
  updateObject,
  deactivateObject,
} from './ObjectForm.library';
import { useObjectFormStyles } from './ObjectForm.style';
import { getObjectTypeAndObject, mapCustomFields } from './ObjectForm.library';

export interface ObjectFormPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  objectUuid: string;
  objectTypeUuid: string;
  attributeId: string;
  type: 'create' | 'update';
}

type ObjectFormInnerProps = {
  object?: ObjectType;
  objectType: ObjectTypeType;
} & Pick<ObjectFormPropsType, 'caseObj' | 'caseType' | 'attributeId' | 'type'>;

/* eslint complexity: [2, 17] */
const InnerObjectForm: React.FunctionComponent<ObjectFormInnerProps> = ({
  objectType,
  object,
  caseObj,
  caseType,
  attributeId,
  type,
}) => {
  const updateMode = type === 'update';
  const mappedFields = mapCustomFields(caseObj, caseType, attributeId);
  const session = useSession();
  const isObjectTypeAdmin = objectType.authorizations.includes('admin');
  const isAllowedSystemAttributes =
    hasCapability(session, 'admin') || isObjectTypeAdmin;

  const customFieldsValues =
    updateMode && object?.customFieldsValues
      ? object?.customFieldsValues
      : formatCaseCustomFields(mappedFields);

  const formDefinition = generateCustomFieldFormDefinition({
    customFieldsDefinition: objectType.customFieldsDefinition,
    customFieldsValues: customFieldsValues || {},
    config: {
      context: {
        type: 'CaseObjectForm' as const,
        data: {
          magic_string: '',
          case: caseObj.caseV1,
          caseType: caseObj.caseV1.casetype,
          objectType: objectType || null,
          object: object || null,
        },
      },
    },
  });

  formDefinition.forEach(field => {
    field.config = {
      ...field.config,
      context: {
        type: 'CaseObjectForm' as const,
        data: {
          ...field.config.context.data,
          magic_string: field.name,
        },
      },
    };
    if (isAllowedSystemAttributes) {
      field.hidden = false;
    }
  });
  const validationMap = generateValidationMap(formDefinition);

  const {
    fields,
    formik: { isValid, values },
  } = useForm({
    isInitialValid: true,
    enableReinitialize: true,
    formDefinition,
    validationMap,
  });

  const classes = useObjectFormStyles();
  const [t] = useTranslation('objectForm');
  const [showConfirm, setShowConfirm] = useState(false);

  if (updateMode && !object) {
    throw new Error('Cannot enter update mode without object instance');
  } else if (updateMode && object && object.status === 'inactive') {
    const isAdmin = object.authorizations.includes('admin');

    return (
      <div className={classes.wrapper}>
        <p className={classes.inactiveWarning}>
          {isAdmin ? t('inactiveWarning.admin') : t('inactiveWarning.normal')}
        </p>
      </div>
    );
  } else {
    return (
      <div className={classes.wrapper}>
        <div className={classes.scrollWrapper}>
          {fields.map(
            ({ FieldComponent, name, error, touched, value, ...rest }) => {
              const restValues = {
                ...cloneWithout(rest, 'type', 'classes'),
                disabled: values.completed,
              };

              return (
                <FormControlWrapper
                  {...restValues}
                  compact={true}
                  error={error}
                  touched={touched}
                  key={name}
                >
                  <FieldComponent
                    name={name}
                    value={value}
                    key={name}
                    {...restValues}
                  />
                </FormControlWrapper>
              );
            }
          )}

          <div className={classes.actionWrapper}>
            <Button
              name="updateOrCreateObject"
              sx={{ marginTop: '20px' }}
              action={() =>
                updateMode && object
                  ? updateObject(
                      object.relatedCasesUuids || [],
                      object.uuid,
                      generateCustomFieldValues(values, formDefinition)
                    )
                  : createObject(
                      objectType.versionUuid,
                      generateCustomFieldValues(values, formDefinition)
                    )
              }
              disabled={!isValid}
            >
              {t(`form.${type}`)}
            </Button>
            {updateMode && object && (
              <Button
                name="deactivateObject"
                // color="danger"
                action={() => setShowConfirm(true)}
                sx={{ marginTop: '20px' }}
              >
                {t('form.deactivate')}
              </Button>
            )}
            {updateMode && object && (
              <ConfirmDialog
                open={showConfirm}
                onConfirm={() => {
                  deactivateObject(
                    object.relatedCasesUuids || [],
                    object.uuid,
                    generateCustomFieldValues(values, formDefinition)
                  );
                  setShowConfirm(false);
                }}
                onClose={() => setShowConfirm(false)}
                title={t('confirm.deactivate.title')}
                body={<div>{t('confirm.deactivate.description')}</div>}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
};

export const ObjectForm: React.ComponentType<ObjectFormPropsType> = ({
  caseObj,
  caseType,
  objectUuid,
  objectTypeUuid,
  attributeId,
  type,
}) => {
  const { data, isLoading, error } = useQuery<
    Awaited<ReturnType<typeof getObjectTypeAndObject>>,
    V2ServerErrorsType
  >(['GET_OBJECT_FORM'], () =>
    getObjectTypeAndObject(objectTypeUuid, objectUuid)
  );

  return error ? (
    <ServerErrorDialog err={error} />
  ) : isLoading ? (
    <Loader />
  ) : (
    <InnerObjectForm
      caseObj={caseObj}
      caseType={caseType}
      objectType={data[1]}
      object={data[0]}
      attributeId={attributeId}
      type={type}
    />
  );
};

export default ObjectForm;
