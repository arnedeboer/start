// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link } from 'react-router-dom';
import Counter from '@mintlab/ui/App/Zaaksysteem/Counter/Counter';
import Icon from '@mintlab/ui/App/Material/Icon';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { SideMenuItemType } from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { useTheme } from '@mui/material';

import { useNavigationStyles } from './Navigation.styles';

const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
  ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
);

type NavigationPropsType = {
  items: {
    href: string;
    selected: boolean;
    icon?: string;
    label: string;
    fullWidth?: boolean;
  }[];
};

/* eslint complexity: [2, 7] */
export const Navigation: React.ComponentType<NavigationPropsType> = ({
  items,
}) => {
  const {
    typography,
    mintlab: { greyscale },
    palette: { primary, common, elephant },
  } = useTheme<Theme>();
  const classes = useNavigationStyles();

  return (
    <List
      sx={{
        display: 'flex',
        borderBottom: `1px solid ${greyscale.dark}`,
        padding: 0,
        height: '55px',
      }}
      component="nav"
      role="navigation"
    >
      {items.map(
        (
          { href, selected, icon, label, count, fullWidth = true }: any,
          index: number
        ) => (
          <ListItem
            button
            role="button"
            href={href}
            component={MenuItemLink}
            key={index}
            selected={selected}
            sx={{
              padding: '10px 20px',
              color: elephant.dark,
              width: fullWidth ? '100%' : 'auto',
              display: 'flex',
              justifyContent: 'center',
              ...(selected
                ? {
                    borderBottom: `2px solid ${primary.main}`,
                    '&&': {
                      color: primary.main,
                      backgroundColor: common.white,
                    },
                  }
                : {}),
            }}
          >
            {icon && (
              <ListItemIcon
                sx={{
                  ...(selected
                    ? {
                        color: primary.main,
                        minWidth: 30,
                      }
                    : {
                        minWidth: 30,
                        color: elephant.dark,
                      }),
                }}
              >
                <Icon size="small">{icon}</Icon>
              </ListItemIcon>
            )}
            <ListItemText
              sx={{ flex: 'none', fontWeight: typography.fontWeightMedium }}
            >
              <Counter
                classes={{
                  counter: selected ? classes.counter : classes.counterInactive,
                }}
                count={count}
              >
                <span
                  style={
                    selected ? { fontWeight: typography.fontWeightMedium } : {}
                  }
                >
                  {label}
                </span>
              </Counter>
            </ListItemText>
          </ListItem>
        )
      )}
    </List>
  );
};

export default Navigation;
