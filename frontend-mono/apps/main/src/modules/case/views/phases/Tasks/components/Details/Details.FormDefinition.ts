// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  FormDefinition,
  FormDefinitionField,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  Rule,
  isTruthy,
  showFields,
  hideFields,
  setFieldValue,
} from '@zaaksysteem/common/src/components/form/rules';
import { ContactFinderConfigType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import { RelatedContactType, TaskType } from './../../Tasks.types';

export const getFormDefinition: (
  task: TaskType,
  externalTaskAssignment: boolean,
  dsoActive: boolean,
  supplyChainPartners: RelatedContactType[]
) => FormDefinition<TaskType> = (
  task,
  externalTaskAssignment,
  dsoActive,
  supplyChainPartners
) => [
  {
    name: 'title',
    type: fieldTypes.TEXT,
    required: true,
    label: 'caseTasks:fields.title.placeholder',
    suppressLabel: true,
    placeholder: 'caseTasks:fields.title.placeholder',
    value: task.title,
  },
  {
    name: 'description',
    type: fieldTypes.TEXTAREA,
    isMultiline: true,
    required: false,
    placeholder: 'caseTasks:fields.description.placeholder',
    rows: 5,
    value: task.description,
  },
  {
    name: 'dso_action_request',
    type: fieldTypes.CHECKBOX,
    required: false,
    label: 'caseTasks:fields.dso_action_request.label',
    suppressLabel: true,
    value: task.dso_action_request && externalTaskAssignment && dsoActive,
  },
  <FormDefinitionField<TaskType, ContactFinderConfigType>>{
    name: 'assignee',
    type: fieldTypes.CONTACT_FINDER,
    required: false,
    label: 'caseTasks:fields.assignee.label',
    placeholder: 'caseTasks:fields.assignee.placeholder',
    value: task.assignee,
    config: {
      subjectTypes: externalTaskAssignment
        ? ['employee', 'organization', 'person']
        : ['employee'],
    },
  },
  {
    name: 'supply_chain_partner',
    type: fieldTypes.SELECT,
    required: false,
    label: 'caseTasks:fields.supply_chain_partner.label',
    placeholder: 'caseTasks:fields.supply_chain_partner.placeholder',
    value: task.supply_chain_partner,
    choices: supplyChainPartners,
  },
  {
    name: 'product_code',
    type: fieldTypes.TEXT,
    required: false,
    label: 'caseTasks:fields.product_code.label',
    value: task.product_code,
  },
  {
    name: 'due_date',
    type: fieldTypes.DATEPICKER,
    variant: 'inline',
    required: false,
    label: 'caseTasks:fields.due_date.label',
    fullWidth: true,
    placeholder: 'caseTasks:fields.due_date.placeholder',
    value: task.due_date,
  },
  {
    name: 'completed',
    type: 'hidden',
    value: task.completed,
  },
];

export const getRules = (
  externalTaskAssignment: boolean,
  dsoActive: boolean
) => [
  new Rule()
    .when(() => externalTaskAssignment && dsoActive)
    .then(showFields(['dso_action_request']))
    .else(hideFields(['dso_action_request'])),
  new Rule()
    .when('dso_action_request', isTruthy)
    .then(showFields(['supply_chain_partner', 'product_code']))
    .and(hideFields(['assignee']))
    .and(setFieldValue('assignee', null))
    .else(hideFields(['supply_chain_partner', 'product_code']))
    .and(setFieldValue('supply_chain_partner', null))
    .and(setFieldValue('product_code', ''))
    .and(showFields(['assignee'])),
];
