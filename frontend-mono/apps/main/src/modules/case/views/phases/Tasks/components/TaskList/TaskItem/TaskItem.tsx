// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import Button from '@mintlab/ui/App/Material/Button';

import { useTheme } from '@mui/material';
import {
  TaskType,
  SetTaskCompletionType,
  EnterEditModeType,
} from './../../../Tasks.types';
import { useTaskItemStyles } from './TaskItem.style';
import TaskItemAddons from './TaskItemAddons/TaskItemAddons';

export type TaskItemPropsType = {
  task: TaskType;
  style: any;
  setTaskCompletion: SetTaskCompletionType;
  enterEditMode: EnterEditModeType;
};

const indicatorSize = 23;

/* eslint complexity: [2, 10] */
export const TaskItem: React.ComponentType<TaskItemPropsType> = ({
  task,
  style,
  setTaskCompletion,
  enterEditMode,
}) => {
  const classes = useTaskItemStyles();
  const {
    mintlab: { greyscale },
  } = useTheme<Theme>();

  const {
    assignee,
    supply_chain_partner,
    title,
    completed,
    due_date,
    description,
    task_uuid,
  } = task;
  const hasAddonsRow = Boolean(assignee || description || due_date);

  let titleResizeClass:
    | ''
    | typeof classes.titleSmaller
    | typeof classes.titleExtraSmall;

  if (hasAddonsRow && title.length > 199) {
    titleResizeClass = classes.titleExtraSmall;
  } else if (hasAddonsRow && title.length > 59) {
    titleResizeClass = classes.titleSmaller;
  } else if (title.length > 99) {
    titleResizeClass = classes.titleSmaller;
  } else {
    titleResizeClass = '';
  }

  return (
    <li style={style} className={classes.container}>
      <Button
        name="taskListDone"
        sx={{
          border: `1px solid ${greyscale.evenDarker}`,
          width: indicatorSize,
          height: indicatorSize,
          borderRadius: '50%',
          marginRight: '15px',
          padding: 0,

          '&>span>svg': {
            opacity: 0,
          },
          ...(completed
            ? {
                border: 'none',

                '&>span>svg': {
                  opacity: 1,
                },
              }
            : {}),
        }}
        disabled={completed}
        icon="done"
        iconSize="large"
        action={() => setTaskCompletion(task_uuid, true)}
      />
      <div
        className={classNames(
          { [classes.bodyCompleted]: completed },
          classes.body
        )}
      >
        <div className={classes.details}>
          <span
            className={classNames(
              {
                [classes.titleCompleted]: completed,
                [titleResizeClass]: Boolean(titleResizeClass),
              },
              classes.title
            )}
          >
            {title}
          </span>
          <div>
            <TaskItemAddons
              assignee={assignee || supply_chain_partner}
              description={description}
              due_date={due_date}
            />
          </div>
        </div>
        <Button
          name="goToTaskEdit"
          action={() => enterEditMode(task)}
          icon="navigate_next"
          sx={{
            display: 'flex',
            alignItems: 'center',
            alignSelf: 'center',
            width: '52px',
            height: '52px',
          }}
        />
      </div>
    </li>
  );
};
