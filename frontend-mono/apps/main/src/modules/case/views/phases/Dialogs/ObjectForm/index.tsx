// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { objectifyParams } from '@mintlab/kitchen-sink/source/url';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { useCaseObjectQuery } from '../../../../Case.library';
import ObjectForm, { ObjectFormPropsType } from './ObjectForm';
import locale from './ObjectForm.locale';

type ObjectFormModulePropsType = Pick<ObjectFormPropsType, 'caseType'> & {
  caseUuid: string;
};

type ObjectFormModuleParamsType = {
  objectUuid: string;
  type: 'create' | 'update';
};

const ObjectFormModule: React.ComponentType<ObjectFormModulePropsType> = ({
  caseType,
  caseUuid,
}) => {
  const { objectUuid, type } = useParams<
    keyof ObjectFormModuleParamsType
  >() as ObjectFormModuleParamsType;
  const { objectTypeUuid, attributeId } = objectifyParams(location.search);
  const [t] = useTranslation('objectForm');
  const [, addMessages, removeMessages] = useMessages();

  useEffect(() => {
    const messages: { [key: string]: any } = t('serverErrors', {
      returnObjects: true,
    });

    // we fetch the caseObj here instead of recieving it from angular
    // because it needs to be up to date to prefill values
    // and the case can have changed in the mean time

    addMessages(messages);

    return () => removeMessages(messages);
  }, []);

  const { data: caseObj, error } = useCaseObjectQuery(
    caseUuid,
    Boolean(caseUuid)
  );

  if (error) {
    return <ServerErrorDialog err={error} />;
  }

  if (!caseObj) {
    return <Loader />;
  }

  return (
    <I18nResourceBundle resource={locale} namespace="objectForm">
      <ObjectForm
        caseObj={caseObj}
        caseType={caseType}
        objectUuid={objectUuid}
        objectTypeUuid={objectTypeUuid}
        attributeId={attributeId}
        type={type}
      />
    </I18nResourceBundle>
  );
};

export default ObjectFormModule;
