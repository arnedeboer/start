// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { CaseObjType, CaseTypeType } from '../../../Case.types';
import Navigation from './../Navigation/Navigation';
import { useSideBarStyles } from './SideBar.styles';
import { getSideBarItems } from './../Phases.library';
import Tasks from './../Tasks';
import Actions from './../Actions';

export interface SideBarPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  phaseNumber: number;
  checkedActionsCount?: number;
  setCheckedActionsCount: (checkedActionsCount: number) => void;
  openTasksCount?: number;
  setOpenTasksCount: (openTasksCount: number) => void;
}

type SideBarParamsType = {
  sideBarType: 'actions' | 'tasks';
};

export const SideBar: React.ComponentType<SideBarPropsType> = ({
  caseObj,
  caseType,
  phaseNumber,
  checkedActionsCount,
  setCheckedActionsCount,
  openTasksCount,
  setOpenTasksCount,
}) => {
  const [t] = useTranslation('casePhases');
  const classes = useSideBarStyles();
  const { sideBarType } = useParams<
    keyof SideBarParamsType
  >() as SideBarParamsType;
  const type = sideBarType || 'actions';
  const canEditSideBar = caseObj.canEdit && phaseNumber >= caseObj.phase;

  return (
    <div className={classes.wrapper}>
      <Navigation
        items={getSideBarItems(
          t,
          sideBarType,
          phaseNumber,
          checkedActionsCount,
          openTasksCount
        )}
      />

      <div
        className={classes.content}
        style={{ display: type === 'actions' ? 'block' : 'none' }}
      >
        <Actions
          caseObj={caseObj}
          caseType={caseType}
          canEditSideBar={canEditSideBar}
          phaseNumber={Number(phaseNumber)}
          setCheckedActionsCount={setCheckedActionsCount}
        />
      </div>
      <div
        className={classes.content}
        style={{ display: type === 'tasks' ? 'block' : 'none' }}
      >
        <Tasks
          externalTaskAssignment={
            caseType.settings.allow_external_task_assignment
          }
          caseUuid={caseObj.uuid}
          setOpenTasksCount={setOpenTasksCount}
          canEditSideBar={canEditSideBar}
        />
      </div>
    </div>
  );
};

export default SideBar;
