// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import CommunicationModule from '@zaaksysteem/communication-module/src';
import { EmailTemplateType } from '@zaaksysteem/communication-module/src/types/Context.types';
import { CaseObjType, CaseTypeType } from '../../Case.types';
import { useCaseObjectQuery } from '../../Case.library';
import { getEmailTemplates } from './library';

export type CommunicationPropsType = {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
};

const Communication: React.ComponentType<CommunicationPropsType> = ({
  caseObj,
  caseType,
}) => {
  const canEdit = caseObj.canEdit;
  const canCreatePipMessage =
    caseObj.requestor?.type !== 'employee' &&
    caseType.settings.disable_pip_for_requestor !== true;
  const [emailTemplates, setEmailTemplates] = useState<EmailTemplateType[]>();

  useEffect(() => {
    getEmailTemplates(caseObj, caseType).then(setEmailTemplates);
  }, []);

  return (
    <CommunicationModule
      capabilities={{
        allowSplitScreen: true,
        canAddAttachmentToCase: canEdit,
        canAddSourceFileToCase: canEdit,
        canAddThreadToCase: false,
        canCreateContactMoment: true,
        canCreatePipMessage: canEdit && canCreatePipMessage,
        canCreateEmail: canEdit,
        canCreateNote: true,
        canDeleteMessage: canEdit,
        canImportMessage: canEdit,
        canSelectCase: false,
        canSelectContact: true,
        canFilter: true,
        canOpenPDFPreview: true,
      }}
      context="case"
      caseUuid={caseObj.uuid}
      contactUuid={caseObj.requestor?.uuid}
      contactName={caseObj.requestor?.name}
      htmlEmailTemplateName={caseObj.htmlEmailTemplateName}
      emailTemplates={emailTemplates}
    />
  );
};

// there are rules that change the htmlEmailTemplate (set in caseObj)
// so we need to have an up-to-date caseObj whenever the communication tab is opened

// remove this wrapper when caseView v2 is implemented
const CommunicationWrapper = ({
  caseObj,
  caseType,
}: CommunicationPropsType) => {
  const caseUuid = caseObj.uuid;
  const { data, error } = useCaseObjectQuery(caseUuid, Boolean(caseUuid));

  if (error) {
    return <ServerErrorDialog err={error} />;
  }

  if (!data) {
    return <Loader />;
  }

  return <Communication caseObj={data} caseType={caseType} />;
};

export default CommunicationWrapper;
