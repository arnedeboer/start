// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { EmailTemplateType } from '@zaaksysteem/communication-module/src/types/Context.types';
import { CaseObjType, CaseTypeType } from '../../Case.types';

type GetCanCreatePipMessageType = (
  caseObj: CaseObjType,
  caseType: CaseTypeType
) => boolean;

export const getCanCreatePipMessage: GetCanCreatePipMessageType = (
  caseObj,
  caseType
) =>
  caseObj.requestor?.type !== 'employee' &&
  caseType.settings.disable_pip_for_requestor !== true;

const getAttachments = (
  ids: number[],
  fields: CaseTypeType['fields'],
  customFields: CaseObjType['customFields']
) => {
  const attachmentAttributeNames = ids.reduce<string[]>((acc, id) => {
    const field = fields?.find(fld => fld.id === id);

    return field ? [...acc, field.magic_string] : acc;
  }, []);

  const attachmentValues = attachmentAttributeNames.reduce<any[]>(
    (acc, attachmentAttributeName) => {
      const field = customFields[attachmentAttributeName];

      return field ? [...acc, ...(field.value || [])] : acc;
    },
    []
  );

  const attachments = attachmentValues.map(({ uuid, filename }) => ({
    label: filename,
    value: uuid,
  }));

  return attachments;
};

const getAssignee = async (recipientType: string, behandelaar?: string) => {
  if (recipientType !== 'colleague' || !behandelaar) return await [];

  const responseV0 = await request('GET', `/api/subject/${behandelaar}`);

  const { uuid } = responseV0.result[0];

  const responseV1 = await request('GET', `/api/v1/subject/${uuid}`);

  const {
    result: {
      instance: {
        subject: {
          instance: { display_name, email_address },
        },
      },
    },
  } = responseV1;

  return [
    {
      label: display_name,
      value: email_address,
    },
  ];
};

type GetEmailTemplatesType = (
  caseObj: CaseObjType,
  caseType: CaseTypeType
) => Promise<EmailTemplateType[]>;

export const getEmailTemplates: GetEmailTemplatesType = async (
  caseObj,
  caseType
) => {
  const phases = caseType.phases;
  const rawEmailTemplates = phases.reduce(
    (acc, phase) => [...acc, ...phase.emails],
    [] as EmailTemplateType[]
  );

  const promises = rawEmailTemplates.map(async template => ({
    ...template,
    assignee: await getAssignee(template.recipientType, template.behandelaar),
    attachments: getAttachments(
      template.attributeAttachmentIds,
      caseType.fields,
      caseObj.customFields
    ),
  }));

  const emailTemplates = await Promise.all(promises);

  const uniqueEmailTemplates = [
    ...new Map(emailTemplates.map(item => [item.id, item])).values(),
  ];

  return uniqueEmailTemplates;
};
