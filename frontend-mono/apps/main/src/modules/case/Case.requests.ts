// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { APICaseManagement } from '@zaaksysteem/generated';
import {
  CaseV2Type,
  CaseV1Type,
  CaseTypeV2Type,
  CaseTypeV1Type,
  SubjectV2Type,
} from './Case.types';

export const fetchCase = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCaseBasicRequestParams>(
    '/api/v2/cm/case/get_case_basic',
    { case_uuid: uuid }
  );

  const response = await request<CaseV2Type>('GET', url);

  return response;
};

export const fetchCaseV0 = async (caseNumber: string) => {
  const url = `/api/v0/case/${caseNumber}`;

  const response = await request('GET', url);

  return response.result[0];
};

export const fetchCaseV1 = async (uuid: string) => {
  const url = `/api/v1/case/${uuid}`;

  const response = await request<CaseV1Type>('GET', url);

  return response.result.instance;
};

export const fetchCaseType = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCaseTypeVersionRequestParams>(
    '/api/v2/cm/case_type/get_case_type_version',
    { version_uuid: uuid }
  );

  const response = await request<CaseTypeV2Type>('GET', url);

  return response;
};

export const fetchCaseTypeV1 = async (uuid: string, version: number) => {
  const url = buildUrl<{ version: number }>(`/api/v1/casetype/${uuid}`, {
    version,
  });

  const response = await request<CaseTypeV1Type>('GET', url);

  return response.result.instance;
};

export const fetchSubject = async (type: SubjectTypeType, uuid: string) => {
  const url = buildUrl<APICaseManagement.GetContactRequestParams>(
    '/api/v2/cm/contact/get_contact_limited',
    { type, uuid }
  );

  const response = await request<SubjectV2Type>('GET', url);

  return response;
};

export const fetchJobs = async (uuid: string) => {
  const url = buildUrl(`/api/v1/case/${uuid}/queue`, {
    paging: 100,
  });

  const response = await request('GET', url);

  return response;
};

export const fetchRoles = async () => {
  const result = await request(
    'GET',
    buildUrl('/api/v1/subject/role', {
      page: 1,
      rows_per_page: 100,
    })
  );

  return result.result.instance.rows;
};

export const assignToSelf = async (uuid: string) => {
  const url = '/api/v2/cm/case/assign_case_to_self';
  const data: APICaseManagement.AssignCaseToSelfRequestBody = {
    case_uuid: uuid,
  };

  const response =
    await request<APICaseManagement.AssignCaseToSelfResponseBody>(
      'POST',
      url,
      data
    );

  return response;
};

export const setPaymentStatus = async (
  uuid: string,
  status: 'success' | 'pending' | 'failed' | 'offline'
) => {
  const url = `/api/v1/case/${uuid}/update`;
  const data = { payment_info: { payment_status: status } };

  const response = await request('POST', url, data);

  return response;
};

export const changeConfidentiality = async (
  caseNumber: number,
  confidentiality: string
) => {
  const url = `/api/v0/case/${caseNumber}/update`;
  const data = { confidentiality };

  const response = await request('POST', url, data);

  return response;
};
