// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { EmailTemplateType } from '@zaaksysteem/communication-module/src/types/Context.types';
import { APICaseManagement } from '@zaaksysteem/generated';
import { caseActionIcons } from './sideMenu/caseActions/Caseactions.library';

export type SubjectV2Type = APICaseManagement.GetContactResponseBody;
export type SubjectType = {
  type: SubjectTypeType;
  uuid: string;
  name?: string;
  hasCorrespondenceAddress?: boolean;
  // person
  isSecret?: boolean;
  isDeceased?: boolean;
  hasValidAddress?: boolean;
  isUnderInvestigation?: boolean;
  // organization
  cocNumber?: string;
  cocLocationNumber?: number;
};
export type CaseRolesV1Type = {
  instance: { label: string };
}[];
export type CaseRoleType = {
  label: string;
  value: string;
};
export type SystemRolesType = {
  assignee?: SubjectType;
  requestor?: SubjectType;
  recipient?: SubjectType;
  coordinator?: SubjectType;
};

export type CaseV2Type = APICaseManagement.GetCaseBasicResponseBody['data'];
export type CaseV1Type = any;
export type CaseObjType = {
  uuid: string;
  name: string;
  number: number;
  progressStatus: number;
  hasPresetClient: boolean;
  result: {
    result: string;
    result_name: string;
    [key: string]: any;
  } | null;
  status: string;
  caseOpen: boolean;
  canEdit: boolean;
  canManage: boolean;
  hasEditRights: boolean;
  hasManageRights: boolean;
  summary: string;
  htmlEmailTemplateName?: string;
  caseRoles: CaseRoleType[];
  assignee?: SubjectType;
  requestor?: SubjectType;
  recipient?: SubjectType;
  coordinator?: SubjectType;
  department: {
    uuid: string;
    name: string;
  } | null;
  role: {
    uuid: string;
    name: string;
  } | null;
  registrationDate: string;
  targetDate: string | null;
  completionDate: string | null;
  destructionDate: string | null;
  stalledSinceDate?: string;
  stalledUntilDate?: string;
  contactChannel:
    | 'behandelaar'
    | 'balie'
    | 'telefoon'
    | 'post'
    | 'email'
    | 'webformulier'
    | 'sociale media';
  confidentiality: 'public' | 'internal' | 'confidential';
  location?: string;
  payment?: {
    amount?: number;
    status?: 'success' | 'failed' | 'pending' | 'offline';
  };
  customFields?: any;
  caseV1?: CaseV1Type;
  caseTypeVersionUuid: string;
  caseTypeUuid: string;
  caseTypeVersion: number;
  milestone: number;
  phase: number;
  numUnacceptedFiles: number;
  numUnacceptedUpdates: number;
  numUnreadCommunication: number;
};

export type LeadTimeType = {
  type?: 'werkdagen' | 'weken' | 'kalenderdagen' | 'einddatum';
  value?: string | number;
};

export type RawEmailTemplateType = {
  label: string;
  data: {
    zaaktype_notificatie_id: number;
    rcpt:
      | 'aanvrager'
      | 'behandelaar'
      | 'coordinator'
      | 'gemachtigde'
      | 'betrokkene'
      | 'overig';
    behandelaar: string;
    betrokkene_role: string;
    to: string;
    cc: string;
    bcc: string;
    subject: string;
    body: string;
  };
};

export type PhaseType = {
  phase: string;
  milestone: number;
  emails: EmailTemplateType[];
};

export type CaseTypeV2Type =
  APICaseManagement.GetCaseTypeActiveVersionResponseBody;
export type CaseTypeV1Type = any;
export type CaseTypeType = {
  phases: PhaseType[];
  settings: any;
  name?: string;
  metaData: {
    legalBasis?: string;
    localBasis?: string;
    designationOfConfidentiality?:
      | 'Openbaar'
      | 'Beperkt openbaar'
      | 'Intern'
      | 'Zaakvertrouwelijk'
      | 'Vertrouwelijk'
      | 'Confidentieel'
      | 'Geheim'
      | 'Zeer geheim';
    responsibleRelationship?: string;
    processDescription?: string;
  };
  leadTimeLegal: LeadTimeType;
  leadTimeService: LeadTimeType;
  objectFields?: any;
  fields?: any[];
  results: any[];
};

export type CaseTypeTypeForComponents = Omit<CaseTypeType, 'phaseActions'>;

export type JobTypeV1 = {
  instance: {
    status: 'finished' | 'pending';
  };
};

export type JobType = {
  status: 'finished' | 'pending';
};

export type CaseActionType = keyof typeof caseActionIcons;
export type CaseActionDialogType = CaseActionType;
export type OtherDialogsType = 'about' | 'confidentiality' | 'caseActions';

export type DialogsType = CaseActionType | OtherDialogsType;
