// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import fecha from 'fecha';
import classNames from 'classnames';
import * as i18next from 'i18next';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import parseISO from 'date-fns/parseISO';
import { get } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Button from '@mintlab/ui/App/Material/Button';
import { asArray } from '@mintlab/kitchen-sink/source';
import { QueryClient } from '@tanstack/react-query';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ColumnType, ParseResultsModeType } from '../AdvancedSearch.types';
import {
  QUERY_KEY_SAVEDSEARCHES,
  QUERY_KEY_SAVEDSEARCH,
  QUERY_KEY_RESULTS,
  QUERY_KEY_RESULTS_COUNT,
  QUERY_KEY_CUSTOM_FIELDS,
  QUERY_KEY_EXPORT_RESULTS,
  QUERY_KEY_EXPORT_RESULTS_COUNT,
  QUERY_KEY_LABELS,
} from './constants';

export const invalidateAllQueries = (client: QueryClient) => {
  client.invalidateQueries([QUERY_KEY_SAVEDSEARCHES]);
  client.invalidateQueries([QUERY_KEY_SAVEDSEARCH]);
  client.invalidateQueries([QUERY_KEY_RESULTS]);
  client.invalidateQueries([QUERY_KEY_RESULTS_COUNT]);
  client.invalidateQueries([QUERY_KEY_CUSTOM_FIELDS]);
  client.invalidateQueries([QUERY_KEY_EXPORT_RESULTS]);
  client.invalidateQueries([QUERY_KEY_EXPORT_RESULTS_COUNT]);
  client.invalidateQueries([QUERY_KEY_LABELS]);
};

type APISearchRowType =
  | APICaseManagement.SearchCustomObjectsResponseBody['data'][0]
  | APICaseManagement.SearchCaseResponseBody['data'][0];

type LinkButtonPropsType = {
  title: string;
  url: string;
  t: i18next.TFunction;
};
export const LinkButton: FunctionComponent<LinkButtonPropsType> = ({
  title,
  url,
  t,
}) => (
  <Button
    name="openInNewWindow"
    title={t('results:openInNew')}
    action={(event: any) => {
      event.stopPropagation();
      window.open(url, '_blank');
    }}
    endIcon={<Icon size="extraSmall">{iconNames.open_in_new}</Icon>}
    variant={'text'}
    sx={{
      color: ({ palette: { primary } }: any) => primary.main,
    }}
  >
    {title}
  </Button>
);

/**
 * Currently supports rendering of these attribute types:
  Geo
  Relatie (object + subject)
  Rekeningnummer
  Datum
  E-mail
  Geocoordinaten (lat/lon)
  Numeriek
  Enkelvoudige keuze
  Keuzelijst
  Tekstveld
  Groot tekstveld
  Webadres
  Valuta
  Document
 */
/* eslint complexity: [2, 10] */

type GetResultValueType = {
  classes?: any;
  column: ColumnType;
  row: APISearchRowType;
  t: i18next.TFunction;
  parseResultsMode: ParseResultsModeType;
};

export const getResultValue = ({
  classes,
  column,
  row,
  t,
  parseResultsMode,
}: GetResultValueType) => {
  const { source, type, magicString } = column;
  const joined = source.join('.');
  const baseValue = get(row, joined, '') as any;
  const getType = () => {
    if (magicString) {
      return type;
    } else if (isPopulatedArray(source)) {
      return baseValue?.type || type;
    } else {
      return type;
    }
  };
  const mapType = getType();
  const sanitize = (value: any) =>
    value === null || value === undefined ? '' : value;
  const safeValue = (value: any) =>
    // Explicitly casting to the value to string. If the value is
    // converted by this function we do not want anything but a string
    // to be shown or exported.
    typeof value === 'object' ? JSON.stringify(value) : `${value}`;

  const actualValue = (strict = false) => {
    if (strict) return baseValue;
    return baseValue?.value && baseValue?.type
      ? sanitize(baseValue.value)
      : sanitize(baseValue);
  };

  const getDate = (value: any) => {
    if (!value) {
      return null;
    } else if (/^\d{4}-\d{2}-\d{2}$/.test(value)) {
      //@ts-ignore
      return fecha.format(fecha.parse(value, 'YYYY-MM-DD'), 'DD-MM-YYYY');
    } else if (/^\d{4}-\d{2}-\d{2}T/.test(value)) {
      return fecha.format(
        //@ts-ignore
        fecha.parse(value, 'YYYY-MM-DDTHH:mm:SS'),
        'DD-MM-YYYY HH:mm:SS'
      );
    }
    return null;
  };

  const formatCurrency = () =>
    actualValue()
      ? new Intl.NumberFormat('nl-NL', {
          style: 'currency',
          currency: 'EUR',
        }).format(actualValue())
      : '';

  const formatAddressV2 = () => actualValue()?.address?.full || '';

  const typeMapping: any = () =>
    typeResolvers[parseResultsMode] && typeResolvers[parseResultsMode][mapType]
      ? typeResolvers[parseResultsMode][mapType]
      : null;

  const getRelationships = () => {
    if (baseValue?.specifics) return asArray(baseValue);
    if (baseValue?.value) return asArray(baseValue.value);
    return [];
  };

  const getDaysDiff = () => {
    const {
      attributes: { status, registration_date, target_completion_date },
    } = row;
    if (status !== 'open' || !target_completion_date || !registration_date)
      return null;

    return differenceInCalendarDays(
      parseISO(target_completion_date),
      new Date()
    );
  };

  /*
  // Add more functions to typeResolvers to handle specific mode/type
  // combinations. Any combination not present here will fall back to the
  // generic fallbacks (see bottom). It is also possible to create custom
  // handling for column types that are not directly tied to a field on the
  // backend. This way we can create compound column types, like a combined
  // address.
  */
  const typeResolvers: {
    [key in ParseResultsModeType]: { [type: string]: () => any };
  } = {
    screen: {
      text: () => safeValue(actualValue()),
      document: () =>
        asArray(actualValue()).map(document => (
          <LinkButton
            key={document.value}
            title={document.label}
            url={`/api/v2/cm/custom_object/download_file?object_uuid=${row.id}&file_uuid=${document.value}`}
            t={t}
          />
        )),
      email: () =>
        actualValue() ? (
          <LinkButton
            title={actualValue()}
            url={`mailto:${actualValue()}`}
            t={t}
          />
        ) : null,
      date: () => getDate(actualValue()) || '',
      geojson: () => (
        <LinkButton
          title={t('results:types.geojson')}
          url={`/main/object/${row?.id}/map`}
          t={t}
        />
      ),
      url: () =>
        actualValue() ? (
          <LinkButton title={actualValue()} url={actualValue()} t={t} />
        ) : null,
      address_v2: formatAddressV2,
      valuta: formatCurrency,
      currency: formatCurrency,
      relationship: () => {
        const getRelationshipProps = (relationship: any) => {
          const { specifics, value } = relationship;
          switch (specifics?.relationship_type) {
            case 'custom_object':
              return {
                url: `/main/object/${value}/`,
                title:
                  specifics?.metadata?.summary ||
                  t('results:types.relationshipTypes.customObject'),
              };
            case 'subject':
            case 'organization':
              return {
                url: `/redirect/contact_page?uuid=${value}`,
                title:
                  specifics?.metadata?.summary ||
                  t('results:types.relationshipTypes.subject'),
              };
            default:
              return {
                url: '/',
                title: t('results:types.relationshipTypes.unknown'),
              };
          }
        };

        if (!actualValue()) return null;

        return getRelationships().map((relationship: any) => {
          const props = getRelationshipProps(relationship);
          return props ? (
            <LinkButton title={props.title} url={props.url} t={t} />
          ) : null;
        });
      },
      days: () => {
        const daysDiff = getDaysDiff();
        if (!daysDiff || isNaN(daysDiff)) return null;
        return (
          <div className={classes.daysWrapper}>
            <span
              className={classNames(classes.days, {
                [classes.daysToday]: daysDiff === 0,
                [classes.daysUrgent]: daysDiff < 0,
                [classes.daysNonUrgent]: daysDiff > 0,
              })}
            >
              {`${daysDiff}`}
            </span>
          </div>
        );
      },
    },
    export: {
      document: () =>
        asArray(actualValue()).map((document: any) => document.label),
      geojson: () => {
        const geoValue = actualValue();
        const features =
          geoValue && isPopulatedArray(geoValue.features)
            ? geoValue.features[0]
            : null;

        if (features && isPopulatedArray(features.geometry.coordinates)) {
          const coordinates = features.geometry.coordinates;
          if (Array.isArray(coordinates[0])) {
            return coordinates[0]
              .map((coord: any) => `(${coord.join(', ')})`)
              .join(', ');
          } else {
            return coordinates?.join(', ');
          }
        }
        return '';
      },
      address_v2: formatAddressV2,
      currency: formatCurrency,
      valuta: formatCurrency,
      relationship: () => {
        if (!actualValue()) return '';
        return getRelationships()
          .map(
            (relationship: any) =>
              relationship?.specifics?.metadata?.summary || ''
          )
          .join(', ');
      },
      days: () => getDaysDiff() || '',
    },
    raw: {
      relationship: () => safeValue(actualValue(true)),
      days: () => getDaysDiff() || '',
    },
  };

  if (typeMapping()) {
    //@ts-ignore
    return typeMapping()();
    // Generic fallbacks
  } else if (parseResultsMode === 'raw') {
    return safeValue(actualValue());
  } else if (getDate(actualValue())) {
    return getDate(actualValue());
  } else if (
    typeof actualValue() === 'string' ||
    actualValue() instanceof String
  ) {
    return actualValue();
  } else {
    return safeValue(actualValue());
  }
};
