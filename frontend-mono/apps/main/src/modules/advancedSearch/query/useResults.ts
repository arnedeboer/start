// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { asArray } from '@mintlab/kitchen-sink/source';
import * as i18next from 'i18next';
import set from 'date-fns/set';
//@ts-ignore
import objectScan from 'object-scan';
//@ts-ignore
import addDuration from 'date-fns-duration';
import { useQuery, UseQueryOptions } from '@tanstack/react-query';
import add from 'date-fns/add';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { get } from '@mintlab/kitchen-sink/source';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { APIGeo } from '@zaaksysteem/generated/types/APIGeo.types';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import {
  ColumnType,
  FilterType,
  IdentifierType,
  KindType,
  SortDirectionType,
  ResultRowType,
  DateEntryType,
  FiltersType,
  ParseResultsModeType,
  GetResultsReturnType,
} from '../AdvancedSearch.types';
import {
  getFromQueryValues,
  getUniqueColumnIdentifier,
} from '../library/library';
import { updateObject } from '../library/library';
import { getDateMode } from '../library/config';
import { replaceKeysInJSON } from '../library/library';
import { filtersKeyNamesReplacements } from '../library/config';
import { getResultValue } from './library';
import { QUERY_KEY_RESULTS, QUERY_KEY_RESULTS_COUNT } from './constants';

const COUNT_STALE_TIME = 60 * 1000;

// Rows from the search API --> Table rows

type APIResultsToResultsType = {
  classes?: any;
  columns: ColumnType[];
  t: i18next.TFunction;
  parseResultsMode: ParseResultsModeType;
  data?: GetResultsReturnType;
};

export const APIResultsToResults = ({
  classes,
  data,
  columns,
  t,
  parseResultsMode,
}: APIResultsToResultsType): ResultRowType[] | null => {
  if (!data) return [];
  const { data: rows, included } = data;

  if (!rows || !isPopulatedArray(rows)) return [];

  return rows.map((row: any) => {
    if (row.relationships) {
      Object.entries(row.relationships).forEach(val => {
        const keyName = val[0];
        //@ts-ignore-next-line
        const { id, type } = val[1].data;
        const filterFn = ({ value }: any) =>
          value?.type === type && value?.id === id;
        const includedPath = objectScan(['**'], {
          joined: true,
          filterFn,
        })(included);
        if (isPopulatedArray(includedPath)) {
          updateObject(
            row.relationships,
            get(included, includedPath[0]),
            `${keyName}.data`
          );
        }
      });
    }
    let columnsObj: ResultRowType['columns'] = {};
    const {
      id,
      attributes: { version_independent_uuid = '' },
      geoFeatures,
    } = row;
    columns.map(column => {
      const resultValue = getResultValue({
        column,
        row,
        t,
        parseResultsMode,
        classes,
      });
      if (resultValue)
        columnsObj[getUniqueColumnIdentifier(column)] = resultValue;
    });
    return {
      columns: columnsObj,
      uuid: id,
      versionIndependentUuid: version_independent_uuid,
      geoFeatures,
    };
  });
};

/* eslint complexity: [2, 22] */
export const filtersToAPIFilters = (
  filters: FiltersType | null,
  keyword?: string | null
) => {
  const dateEntriesToSearchFilters = (group: any) => {
    const filterType = group.type;

    if (!group.parameters.value) return;

    return group.parameters.value.flatMap((dateEntry: DateEntryType) => {
      const { type } = dateEntry;

      if (type === 'absolute') {
        let calculatedDate;
        if (getDateMode(filterType) === 'date') {
          const dayLater = add(new Date(dateEntry.value as string), {
            days: 1,
          });
          calculatedDate =
            dateEntry.operator && ['gt', 'le'].includes(dateEntry.operator)
              ? dayLater.toISOString()
              : dateEntry.value;
        } else {
          calculatedDate = dateEntry.value;
        }
        return `${dateEntry.operator} ${calculatedDate}`;
      } else if (type === 'relative') {
        const baseDate = set(new Date(), {
          hours: 0,
          minutes: 0,
          seconds: 0,
          milliseconds: 0,
        });
        const dayLater = add(baseDate, { days: 1 });
        const dateToAddTo =
          dateEntry.operator && ['gt', 'le'].includes(dateEntry.operator)
            ? dayLater
            : baseDate;
        const calculatedDate = addDuration(dateToAddTo, dateEntry.value);
        return `${dateEntry.operator} ${calculatedDate.toISOString()}`;
      } else if (dateEntry.type === 'range') {
        if (dateEntry.timeSetByUser === true) {
          return [`ge ${dateEntry.startValue}`, `le ${dateEntry.endValue}`];
        } else {
          const dayLater = add(new Date(dateEntry.endValue as string), {
            days: 1,
          });
          return [`gt ${dateEntry.startValue}`, `lt ${dayLater.toISOString()}`];
        }
      }
    });
  };

  const customFieldToSearchFilters = (group: FilterType[]) =>
    group.map(filter =>
      replaceKeysInJSON(
        //@ts-ignore-next-line
        filter.parameters.value as any,
        filtersKeyNamesReplacements
      )
    );

  const filterValueToAPIValue = (type: any, group: FilterType[]) => {
    switch (type) {
      case 'attributes.value':
        return customFieldToSearchFilters(group);
      case 'keyword':
        return {
          //@ts-ignore-next-line
          operator: group[0].operator,
          values: group.flatMap(thisGroup =>
            //@ts-ignore-next-line
            thisGroup.parameters.map(parameter => parameter.value)
          ),
        };
      case 'attributes.status':
      case 'attributes.archive_status':
      case 'attributes.archival_state':
      case 'attributes.urgency':
      case 'attributes.channel_of_contact':
      case 'attributes.confidentiality':
        //@ts-ignore-next-line
        return asArray(group[0].parameters.value);
      case 'attributes.last_modified':
      case 'attributes.registration_date':
      case 'attributes.completion_date':
        //@ts-ignore-next-line
        return dateEntriesToSearchFilters(group[0]);
      case 'relationship.requestor.id':
      case 'relationship.assignee.id':
      case 'relationship.coordinator.id':
      case 'relationship.case_type.id':
        //@ts-ignore-next-line
        return group[0].parameters.map(parameter => parameter.value);
      case 'attributes.payment_status':
        //@ts-ignore-next-line
        return group[0].parameters;
      case 'attributes.department_role':
        return {
          //@ts-ignore-next-line
          values: group[0].parameters.value.map(value => ({
            department_uuid: value.department,
            role_uuid: value.role,
          })),
        };
      case 'attributes.case_location':
        return {
          //@ts-ignore-next-line
          values: group[0].parameters.value.map(value => value.id),
        };
      default:
        //@ts-ignore-next-line
        return group[0].parameters.value;
    }
  };

  const getName = (type: string) => {
    switch (type) {
      case 'relationship.custom_object_type':
        return 'relationship.custom_object_type.id';
      default:
        return type;
    }
  };

  if (!filters) return [];

  let grouped = filters.filters.reduce((acc: any, filter: FilterType) => {
    const { type } = filter;
    if (!acc[type]) acc[type] = [];
    acc[type].push(filter);
    return acc;
  }, {});

  let filtersObj = Object.entries(grouped).reduce((acc: any, group: any) => {
    const [type, filters] = group;
    const name = getName(type);
    acc[name] = filterValueToAPIValue(type, filters);
    return acc;
  }, {});

  if (keyword) {
    filtersObj.keyword = [filtersObj.keyword, keyword]
      .filter(Boolean)
      .join(' ');
  }

  return filtersObj;
};

const getIncludedParams = (columns: ColumnType[] | null) => {
  if (!columns || !isPopulatedArray(columns)) return null;
  const includes = columns
    .reduce((acc, current) => {
      if (current.source[0].includes('relationship'))
        acc.push(current.source[1]);
      return acc;
    }, [] as string[])
    .filter((value, index, arr) => arr.indexOf(value) === index);
  return isPopulatedArray(includes) ? includes : null;
};

// const getCustomFields = (columns: ColumnType[] | null) => {
//   if (!columns || !isPopulatedArray(columns)) return null;
//   const customFields = columns
//     .reduce((acc, current) => {
//       if (current.magicString) acc.push(current.magicString);
//       return acc;
//     }, [] as string[])
//     .filter((value, index, arr) => arr.indexOf(value) === index);
//   return isPopulatedArray(customFields) ? customFields : null;
// };

/* eslint complexity: [2, 22] */
export const getResults =
  ({ count }: any) =>
  async ({ queryKey }: { queryKey: any }): Promise<any | number> => {
    const kind = getFromQueryValues<KindType>(queryKey[1], 'kind');
    const page = getFromQueryValues<number>(queryKey[1], 'page');
    const page_size = getFromQueryValues<number>(queryKey[1], 'resultsPerPage');
    const sortColumn = getFromQueryValues<string>(queryKey[1], 'sortColumn');
    const sortOrder = getFromQueryValues<SortDirectionType>(
      queryKey[1],
      'sortOrder'
    );
    const geo = getFromQueryValues<boolean>(queryKey[1], 'geo');
    const filters = getFromQueryValues<FiltersType | null>(
      queryKey[1],
      'filters'
    );
    const columns = getFromQueryValues<ColumnType[]>(queryKey[1], 'columns');
    const keyword = getFromQueryValues<string>(queryKey[1], 'keyword');
    const includes = getIncludedParams(columns);
    //const customFields = getCustomFields(columns);

    const url = `${
      kind === 'custom_object'
        ? '/api/v2/cm/custom_object/search'
        : '/api/v2/cm/case/search'
    }${count ? '/count' : ''}`;

    const sort =
      sortColumn && sortOrder
        ? `${sortOrder === 'desc' ? '-' : ''}${sortColumn.replace(
            'attributes.',
            ''
          )}`
        : null;

    let parsedFilters = filtersToAPIFilters(filters, keyword);
    if (filters?.operator) parsedFilters.operator = filters.operator;

    const postData = {
      ...(page && { page }),
      ...(page_size && { page_size }),
      ...(filters && {
        filters: parsedFilters,
      }),
      ...(sort && { sort }),
      ...(includes && !count && { includes }),
      //...(customFields && { custom_fields: customFields }),
    };

    let results = await request<any>('POST', url, postData).catch(
      (serverError: V2ServerErrorsType) => {
        if (!count) throw serverError;
      }
    );

    if (count) return results?.data?.attributes?.total_results || 0;

    if (geo && results && isPopulatedArray(results?.data)) {
      const geoURL = buildUrl('/api/v2/geo/get_geo_features', {
        uuid: results?.data.map(
          (result: any) =>
            result.attributes.version_independent_uuid || result.id
        ),
      });
      const geoResults = await request<APIGeo.GetGeoFeaturesResponseBody>(
        'GET',
        geoURL
      ).catch((serverError: V2ServerErrorsType) => {
        throw serverError;
      });

      results.data = results.data.map((result: any) => {
        const features =
          geoResults.data.find(
            geoResult =>
              geoResult.id === result.attributes.version_independent_uuid
          )?.attributes?.geo_json?.features || null;
        return {
          ...result,
          ...(features &&
            Array.isArray(features) &&
            features.length && { geoFeatures: features }),
        };
      });
    }

    return {
      data: results.data || [],
      ...(results.included && { included: results.included }),
    };
  };

export type UseResultsQueryQueryParamsType = {
  identifier: IdentifierType;
  page: number;
  resultsPerPage: number;
  kind: KindType;
  geo: boolean;
  sortOrder?: SortDirectionType | null;
  sortColumn?: string | null;
  filters?: FiltersType | null;
  columns?: ColumnType[];
  keyword?: string;
};
type GetQueryConfigParamsType = {
  queryParams: UseResultsQueryQueryParamsType;
  openServerErrorDialog?: OpenServerErrorDialogType;
  onError?: any;
  queryKey?: string;
  keepPreviousData?: boolean;
  enabled?: boolean;
};

export const useResultsQuery = ({
  queryKey,
  queryParams,
  openServerErrorDialog,
  enabled,
  keepPreviousData,
  onError,
}: GetQueryConfigParamsType) =>
  useQuery<any>(
    getQueryConfig({
      queryKey,
      queryParams,
      openServerErrorDialog,
      enabled,
      keepPreviousData,
      onError,
    })
  );

export const useResultsCountQuery = ({
  queryKey = QUERY_KEY_RESULTS_COUNT,
  queryParams,
  openServerErrorDialog,
  onError,
  enabled,
  keepPreviousData = true,
  onSuccess,
}: any) => {
  return useQuery([queryKey, queryParams], getResults({ count: true }), {
    onSuccess,
    onError:
      onError || ((error: V2ServerErrorsType) => openServerErrorDialog(error)),
    enabled,
    keepPreviousData,
    staleTime: COUNT_STALE_TIME,
  });
};

export const getQueryConfig = ({
  queryParams,
  openServerErrorDialog,
  onError,
  queryKey = QUERY_KEY_RESULTS,
  enabled = true,
  keepPreviousData = true,
}: GetQueryConfigParamsType): UseQueryOptions => {
  const {
    identifier,
    page,
    resultsPerPage,
    kind,
    geo,
    sortColumn,
    sortOrder,
    filters,
    columns,
    keyword,
  } = queryParams;

  return {
    queryKey: [
      queryKey,
      {
        identifier,
        page,
        resultsPerPage,
        kind,
        sortColumn,
        sortOrder,
        filters,
        geo,
        columns,
        keyword,
      },
    ],
    queryFn: getResults({
      count: false,
      openServerErrorDialog,
    }),
    onError:
      onError ||
      ((error: V2ServerErrorsType) => {
        if (openServerErrorDialog) openServerErrorDialog(error);
      }),
    enabled,
    keepPreviousData,
  };
};
