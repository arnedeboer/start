// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import {
  REGEXP_DATE_DAY,
  REGEXP_EMAIL_OR_MAGIC_STRING,
  REGEXP_NUMERIC,
  REGEXP_TEXT,
  REGEXP_URL,
} from '../../../../../../packages/common/src/constants/regexes';
import { ValidateFuncType } from '../AdvancedSearch.types';

export const validateCurrency =
  (identifier: string, errorMessage: string) =>
  (value: any): ReturnType<ValidateFuncType<any>> => {
    if (value === null || value === undefined) return;
    return value
      ? undefined
      : {
          uuid: identifier,
          value: errorMessage,
        };
  };

export const validateRegExp =
  (identifier: string, errorMessage: string, regExpType: string) =>
  (value: any): ReturnType<ValidateFuncType<any>> => {
    const map = {
      //eslint-disable-next-line
      url: REGEXP_URL,
      numeric: REGEXP_NUMERIC,
      email: REGEXP_EMAIL_OR_MAGIC_STRING,
      text: REGEXP_TEXT,
      dateDay: REGEXP_DATE_DAY,
    };

    if (value === null || value === undefined) return;
    //@ts-ignore
    const regExp = new RegExp(map[regExpType]);
    return regExp.test(value)
      ? undefined
      : {
          uuid: identifier,
          value: errorMessage,
        };
  };

export const validateMinSelected =
  (identifier: string, errorMessage: string) => (value: any) =>
    !isPopulatedArray(value)
      ? {
          value: errorMessage,
          uuid: identifier,
        }
      : undefined;
