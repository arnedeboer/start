// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useState } from 'react';
import Pagination from '@mintlab/ui/App/Material/Pagination';
import ResultsView from './components/Main/Results/ResultsView';

const WidgetDummy = ({ identifier, kind, keyword }: any) => {
  const [page, setPage] = useState(1);
  const [resultsPerPage, setResultsPerPage] = useState(10);
  const [totalResults, setTotalResults] = useState(0);

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
      }}
    >
      <div style={{ flex: 1, display: 'flex' }}>
        <ResultsView
          kind={kind}
          identifier={identifier}
          view={'table'}
          page={page}
          resultsPerPage={resultsPerPage}
          setTotalResults={setTotalResults}
          keyword={keyword}
        />
      </div>
      <div style={{ height: 100 }}>
        <Pagination
          scope="example"
          changeRowsPerPage={(event: any) =>
            setResultsPerPage(event?.target?.value)
          }
          component={'div'}
          count={totalResults}
          getNewPage={(newPage: number) => setPage(newPage + 1)}
          labelDisplayedRows={({ count: newCount, from, to }: any) =>
            `${from}–${to} van ${newCount}`
          }
          labelRowsPerPage={'Per pagina:'}
          page={page !== 0 ? page - 1 : page}
          rowsPerPage={resultsPerPage}
          rowsPerPageOptions={[5, 10, 25, 50, 100]}
        />
      </div>
    </div>
  );
};

export default WidgetDummy;
