// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import * as i18next from 'i18next';
import { GridColDef, GridInitialState } from '@mui/x-data-grid-pro';
import Tooltip from '@mintlab/ui/App/Material/Tooltip/';
import { UseQueryResult } from '@tanstack/react-query';
import { SORTABLE_COLUMNS } from '../../../library/config';
import { getLabel } from '../Main.library';
import {
  ColumnType,
  ModeType,
  SavedSearchType,
  SortDirectionType,
} from '../../../AdvancedSearch.types';
import { getUniqueColumnIdentifier } from '../../../library/library';
import {
  DATAGRID_KEY_PREFIX,
  CHECKBOX_COLUMN_ID,
} from '../../../library/config';

export const getColumns = (columns: ColumnType[]): GridColDef[] => {
  return (columns || []).map(column => {
    const uniqueIdentifier = getUniqueColumnIdentifier(column);
    const { label } = column;

    const getSizeProps = () => {
      if (column.type === 'id' || column.type === 'uuid') {
        return {
          width: 280,
        };
      } else {
        return {
          flex: 1,
        };
      }
    };

    return {
      field: uniqueIdentifier,
      headerName: label,
      sortable: SORTABLE_COLUMNS.some(
        (identifier: string) => identifier === uniqueIdentifier
      ),
      renderCell: params => {
        const thisRowData = params.value;
        return (
          <div>
            {typeof thisRowData === 'object' ? (
              thisRowData
            ) : (
              <Tooltip title={thisRowData} enterNextDelay={750}>
                {thisRowData}
              </Tooltip>
            )}
          </div>
        );
      },
      ...getSizeProps(),
    };
  });
};

export const Label = ({
  classes,
  t,
  mode,
  currentQuery,
}: {
  classes: any;
  t: i18next.TFunction;
  mode: ModeType;
  currentQuery: UseQueryResult<SavedSearchType>;
}) => {
  const topLabel = getLabel({ mode, currentQuery, t });
  return <span className={classes.mainTopBarName}>{topLabel}</span>;
};

export const getDataGridState = ({
  identifier,
  sortColumn,
  sortOrder,
  columns,
  state,
}: {
  identifier: string;
  sortColumn?: string;
  sortOrder?: SortDirectionType;
  columns?: ColumnType[];
  state?: GridInitialState;
}): GridInitialState => {
  const existingState = state || getLocalDataGridState(identifier) || {};

  const getSorting = () => {
    if (existingState?.sorting) return {};
    return {
      sorting: {
        sortModel:
          sortColumn && sortOrder
            ? [
                {
                  field: sortColumn,
                  sort: sortOrder,
                },
              ]
            : [],
      },
    };
  };

  /* eslint complexity: [2, 12] */
  const getColumns = () => {
    const columnVisibilityModel = existingState?.columns?.columnVisibilityModel
      ? null
      : (columns || []).reduce((acc, current) => {
          acc[getUniqueColumnIdentifier(current)] = current.visible;
          return acc;
        }, {} as any);

    const dimensions =
      existingState?.columns?.dimensions ||
      (columns || []).reduce((acc, current) => {
        acc[getUniqueColumnIdentifier(current)] = {
          width: 100,
          flex: 1,
        };
        return acc;
      }, {} as any);

    const orderedFields = existingState?.columns?.orderedFields || [
      CHECKBOX_COLUMN_ID,
      ...(columns || []).map(column => getUniqueColumnIdentifier(column)),
    ];

    return {
      columns: {
        ...(existingState?.columns || {}),
        ...(columnVisibilityModel && { columnVisibilityModel }),
        dimensions,
        orderedFields,
      },
    };
  };

  const getFilters = () => {
    return (
      existingState?.filter || {
        filter: {
          filterModel: {
            items: [],
          },
        },
      }
    );
  };

  const getPinnedColumns = () => {
    return (
      existingState?.pinnedColumns || {
        pinnedColumns: [],
      }
    );
  };

  return {
    ...existingState,
    ...getSorting(),
    ...getColumns(),
    ...getFilters(),
    ...getPinnedColumns(),
  };
};

export function saveLocalDataGridState(identifier: string, state: any) {
  const key = `${DATAGRID_KEY_PREFIX}${identifier}`;
  localStorage.setItem(key, JSON.stringify(state));
}

export function getLocalDataGridState(identifier: string) {
  const item = localStorage.getItem(`${DATAGRID_KEY_PREFIX}${identifier}`);
  return item ? JSON.parse(item) : null;
}

export function deleteLocaleDataGridState(identifier: string) {
  localStorage.removeItem(`${DATAGRID_KEY_PREFIX}${identifier}`);
}
