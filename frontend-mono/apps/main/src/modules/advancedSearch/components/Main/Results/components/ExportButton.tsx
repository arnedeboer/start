// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import DropdownMenu, {
  DropdownMenuList,
  //@ts-ignore
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import Button from '@mintlab/ui/App/Material/Button';
import { ExportFormatType } from '../../../../AdvancedSearch.types';

type ExportButtonPropsType = {
  t: i18next.TFunction;
  exportAction: (format: ExportFormatType) => void;
  loading: boolean;
};

const ExportButton: FunctionComponent<ExportButtonPropsType> = ({
  t,
  exportAction,
  loading,
}) => {
  const items = [
    {
      action: () => exportAction('CSV'),
      title: t('export.formats.CSV'),
    },
  ];
  return (
    <DropdownMenu
      transformOrigin={{
        horizontal: -20,
      }}
      trigger={
        <Button disabled={loading} name="addDropdown" variant="outlined">
          {t('export.buttonLabel')}
        </Button>
      }
    >
      <DropdownMenuList
        items={items.map(({ action, title }) => ({
          action,
          label: title,
          scope: `export-item-${title}`,
        }))}
      />
    </DropdownMenu>
  );
};

export default ExportButton;
