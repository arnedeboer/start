// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { ClassesType } from '../../AdvancedSearch.types';

type SelectMessagePropsType = {
  classes: ClassesType;
  t: i18next.TFunction;
};

const SelectMessage: FunctionComponent<SelectMessagePropsType> = ({
  classes,
  t,
}) => {
  return (
    <p className={classes.selectMessage}>{t('selectMessage') as string}</p>
  );
};

export default SelectMessage;
