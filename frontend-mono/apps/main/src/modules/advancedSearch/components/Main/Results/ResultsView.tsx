// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  useEffect,
  useState,
  useMemo,
  MutableRefObject,
  Dispatch,
  SetStateAction,
} from 'react';
import {
  GridColDef,
  GridInitialState,
  GridApiCommon,
} from '@mui/x-data-grid-pro';
import Button from '@mintlab/ui/App/Material/Button';
import { useQueryClient } from '@tanstack/react-query';
import { createPortal } from 'react-dom';
import deepEqual from 'fast-deep-equal';
import { useTransition } from '@zaaksysteem/common/src/hooks/useTransition';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { Geojson } from '@mintlab/ui/types/MapIntegration';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useSingleQuery } from '../../../query/useSingle';
import { useResultsQuery } from '../../../query/useResults';
import {
  IdentifierType,
  KindType,
  ViewType,
  ColumnType,
} from '../../../AdvancedSearch.types';
import { useResultsCountQuery } from '../../../query/useResults';
import resultsLocale from '../../../locale/results.locale';
import { APIResultsToResults } from '../../../query/useResults';
import { useStyles } from './ResultsView.styles';
import {
  getColumns,
  getDataGridState,
  saveLocalDataGridState,
  deleteLocaleDataGridState,
} from './Results.library';
import ResultsTable from './ResultsTable';
import ResultsMap from './ResultsMap';
import ExportDialog from './components/ExportDialog/ExportDialog';

type ResultsViewPropsType = {
  identifier: IdentifierType;
  kind: KindType;
  page: number;
  view: ViewType;
  resultsPerPage: number;
  setTotalResults?: Dispatch<SetStateAction<number>>;
  keyword?: string;
  setName?: (name: string) => void;
  portalRef?: HTMLElement | null;
  setSnack?: any;
  setPage?: Dispatch<SetStateAction<number>>;
};

// Resultsview / Datagrid flow:

// When the table is initialized:
// - An initial Datagrid state object is created. This comprises of the
//  state stored in Local Storage, if present, and any defaults set by the
//  owner of the Saved Search.
// - dataGridState state is initialized with this object, and is passed as
//   initialState to the Datagrid.
//
// When the user makes a change (header order, column widths, etc.):
// - The new state of the table is put into local state (dataGridState)
// - The effect watches for changes in this state and saves a copy to Local Storage
// - The rest of the code (like Queries) can use this state (like sorting options)

/* eslint complexity: [2, 20] */
const ResultsView: FunctionComponent<ResultsViewPropsType> = ({
  identifier,
  kind,
  page,
  resultsPerPage,
  setTotalResults,
  view = 'table',
  keyword,
  setName,
  portalRef,
  setSnack,
  setPage,
}) => {
  const classes = useStyles();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [exportDialogOpen, setExportDialogOpen] = useState<boolean>(false);
  const queryClient = useQueryClient();
  const currentQuery = useSingleQuery({
    identifier,
    openServerErrorDialog,
  });
  const columns = currentQuery?.data?.columns;
  const sortColumn = currentQuery?.data?.sortColumn;
  const sortOrder = currentQuery?.data?.sortOrder;
  const memoizedGridColumns: GridColDef[] = useMemo(
    () => getColumns(columns as ColumnType[]),
    [columns]
  );

  const initialState: GridInitialState = getDataGridState({
    identifier,
    sortColumn,
    sortOrder,
    columns,
  });

  const [dataGridState, setDataGridState] =
    useState<GridInitialState>(initialState);

  const resetToInitialState = (apiRef: MutableRefObject<GridApiCommon>) => {
    deleteLocaleDataGridState(identifier);
    const newState = getDataGridState({
      identifier,
      sortColumn,
      sortOrder,
      columns,
    });
    apiRef?.current?.restoreState(newState);
  };

  useTransition(
    (prevDataGridState: any) => {
      saveLocalDataGridState(identifier, dataGridState);
      if (!setPage) return;
      if (
        !deepEqual(
          prevDataGridState?.sorting?.sortModel,
          dataGridState?.sorting?.sortModel
        )
      ) {
        setPage(1);
      }
    },
    [dataGridState]
  );

  useEffect(() => {
    if (setPage) setPage(1);
  }, [identifier, resultsPerPage]);

  const filters = currentQuery?.data?.filters;
  const [t] = useTranslation('results');
  const currentSuccess = currentQuery.isSuccess;
  const queryParams = {
    kind,
    identifier,
    page,
    resultsPerPage,
    columns,
    filters,
    geo: view === 'map',
    keyword,
    ...(dataGridState.sorting?.sortModel?.length && {
      sortColumn: dataGridState.sorting.sortModel[0].field,
      sortOrder: dataGridState.sorting.sortModel[0].sort,
    }),
  };

  const enabled = currentSuccess && Boolean(identifier);

  const resultsQuery = useResultsQuery({
    queryParams,
    openServerErrorDialog,
    enabled,
  });

  const resultsCountQuery = useResultsCountQuery({
    queryParams: cloneWithout(
      queryParams,
      'page',
      'resultsPerPage',
      'sortColumn',
      'sortOrder'
    ),
    t,
    currentSuccess,
    openServerErrorDialog,
    enabled,
  });

  useEffect(() => {
    const count = resultsCountQuery?.data;
    if (setTotalResults && count !== null) setTotalResults(count);
  });

  useEffect(() => {
    if (setName && currentQuery?.data) setName(currentQuery.data.name);
  }, [currentQuery.data]);

  const rows = APIResultsToResults({
    classes,
    data: resultsQuery?.data,
    columns: columns || [],
    t,
    parseResultsMode: 'screen',
  });

  const features: Geojson[] | undefined = rows?.reduce<Geojson[]>(
    (acc, current) => {
      if (current?.geoFeatures && current?.geoFeatures.length) {
        acc = [...acc, ...current.geoFeatures];
      }
      return acc;
    },
    []
  );

  const selectionProps = useSelectionBehaviour({
    rows: rows || [],
    page,
    resultsPerPage,
    selectEverythingTranslations: t('selectable', {
      returnObjects: true,
    }),
  });
  const { everythingSelected, selectedRows } = selectionProps;

  if (
    currentQuery?.status === 'loading' ||
    resultsQuery?.status === 'loading'
  ) {
    return <Loader />;
  }

  const showExportButton =
    portalRef &&
    setSnack &&
    (isPopulatedArray(selectedRows) || everythingSelected);

  return (
    <>
      {ServerErrorDialog}
      {exportDialogOpen && currentQuery.data && (
        <ExportDialog
          open={exportDialogOpen}
          onClose={() => setExportDialogOpen(false)}
          currentQueryData={currentQuery.data}
          kind={kind}
          openServerErrorDialog={openServerErrorDialog}
          t={t}
          queryClient={queryClient}
          page={page}
          everythingSelected={everythingSelected}
          selectedRows={selectedRows}
          resultsPerPage={resultsPerPage}
          setSnack={setSnack}
          dataGridState={dataGridState}
        />
      )}
      {showExportButton &&
        createPortal(
          <Button
            name="exportBtn"
            label={t('buttons.export')}
            action={() => setExportDialogOpen(true)}
          >
            {t('buttons.export')}
          </Button>,
          portalRef
        )}
      {view === 'table' &&
        rows &&
        memoizedGridColumns &&
        memoizedGridColumns.length && (
          <ResultsTable
            columns={memoizedGridColumns}
            rows={rows}
            kind={kind}
            t={t}
            selectionProps={selectionProps}
            initialState={dataGridState}
            setDataGridState={setDataGridState}
            identifier={identifier}
            currentQuery={currentQuery}
            resetToInitialState={resetToInitialState}
            isFetching={resultsQuery.isFetching}
            classes={classes}
          />
        )}

      {view === 'map' && <ResultsMap features={features} />}
    </>
  );
};

export default (props: ResultsViewPropsType) => (
  <I18nResourceBundle resource={resultsLocale} namespace="results">
    <ResultsView {...props} />
  </I18nResourceBundle>
);
