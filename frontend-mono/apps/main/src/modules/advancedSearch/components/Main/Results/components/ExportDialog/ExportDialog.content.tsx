// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useState } from 'react';
import classNames from 'classnames';
import { GridInitialState } from '@mui/x-data-grid-pro';
import * as i18next from 'i18next';
import { QueryClient } from '@tanstack/react-query';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { QUERY_KEY_EXPORT_RESULTS } from '../../../../../query/constants';
import {
  ExportFormatType,
  ParseResultsModeType,
} from '../../../../../AdvancedSearch.types';
import { UseResultsQueryQueryParamsType } from '../../../../../query/useResults';
import { LinearProgressWithLabel } from './ExportProgress';
import { initiateDownload } from './ExportDialog.library';
import ExportGenerator from './ExportDialog.generator';

/* eslint complexity: [2, 10] */
type ExportDialogContentPropsType = {
  classes: any;
  t: i18next.TFunction;
  queryClient: QueryClient;
  numPages: number;
  baseQueryParams: Omit<UseResultsQueryQueryParamsType, 'page'>;
  page: number;
  everythingSelected: boolean;
  selectedRows: string[];
  setSnack: any;
  dataGridState: GridInitialState;
};

const ExportDialogContent: FunctionComponent<ExportDialogContentPropsType> = ({
  classes,
  t,
  queryClient,
  numPages,
  baseQueryParams,
  page,
  everythingSelected,
  selectedRows,
  setSnack,
  dataGridState,
}) => {
  const [started, setStarted] = useState<number | null>(null);
  const [format, setFormat] = useState<ExportFormatType>('CSV');
  const [parseResultsMode, setParseResultsMode] =
    useState<ParseResultsModeType>('export');
  const [progressPercentage, setProgressPercentage] = useState<number>(0);

  const cancel = async (type: 'cancel' | 'error') => {
    queryClient.cancelQueries([QUERY_KEY_EXPORT_RESULTS]);
    setSnack({
      message:
        type === 'cancel'
          ? t('export.messages.cancel')
          : t('export.messages.error'),
      open: true,
      color: '#DD2D4F',
    });
    reset();
  };

  const reset = () => {
    setStarted(null);
    setProgressPercentage(0);
  };

  return (
    <React.Fragment>
      <div className={classes.exportDialogSection}>
        <div className={classes.exportDialogOptionsRow}>
          <span>{t('export.options.exportTo.label') as string}</span>
          <div>
            <Select
              isClearable={false}
              onChange={event => setFormat(event.target.value)}
              choices={['CSV', 'TSV'].map((format: string) => ({
                label: format,
                value: format,
              }))}
              nestedValue={true}
              value={format}
            />
          </div>
          <span>
            <Tooltip
              title={t('export.options.exportTo.help')}
              style={{ width: 'auto' }}
            >
              <Icon size="extraSmall" color="inherit">
                {iconNames.info_outlined}
              </Icon>
            </Tooltip>
          </span>
        </div>
        <div className={classes.exportDialogOptionsRow}>
          <span>{t('export.options.celData.label') as string}</span>
          <div>
            <Select
              isClearable={false}
              onChange={event => setParseResultsMode(event.target.value)}
              choices={[
                {
                  label: t('export.options.celData.choices.parse') as string,
                  value: 'export',
                },
                {
                  label: t(
                    'export.options.celData.choices.dontParse'
                  ) as string,
                  value: 'raw',
                },
              ]}
              nestedValue={true}
              value={parseResultsMode}
            />
          </div>
          <span>
            <Tooltip
              title={t('export.options.celData.help')}
              style={{ width: 'auto' }}
            >
              <Icon size="extraSmall" color="inherit">
                {iconNames.info_outlined}
              </Icon>
            </Tooltip>
          </span>
        </div>
      </div>
      <div
        className={classNames(
          classes.exportDialogSection,
          classes.exportDialogCommands
        )}
      >
        {started && (
          <ExportGenerator
            key={String(started)}
            format={format}
            parseResultsMode={parseResultsMode}
            numPages={numPages}
            baseQueryParams={baseQueryParams}
            t={t}
            queryClient={queryClient}
            page={page}
            everythingSelected={everythingSelected}
            selectedRows={selectedRows}
            handleError={() => cancel('error')}
            handleProgressUpdate={setProgressPercentage}
            handleDataDone={(downloadFile: any) => {
              initiateDownload(downloadFile, format);
              setStarted(null);
            }}
            dataGridState={dataGridState}
          />
        )}
        <div>
          <LinearProgressWithLabel
            disabled={!started}
            value={progressPercentage}
          />
        </div>
        <div>
          {!started && (
            <Button
              name="startBtn"
              onClick={() => {
                reset();
                setStarted(Date.now());
              }}
            >
              {t('export.buttons.start')}
            </Button>
          )}
          {started && (
            <Button
              color="warning"
              disabled={!started}
              name="cancelBtn"
              onClick={() => {
                cancel('cancel');
              }}
            >
              {t('export.buttons.cancel')}
            </Button>
          )}
        </div>
      </div>
    </React.Fragment>
  );
};

export default ExportDialogContent;
