// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { QueryClient } from '@tanstack/react-query';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { GridInitialState } from '@mui/x-data-grid-pro';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import Button from '@mintlab/ui/App/Material/Button';
import DialogActions from '@mui/material/DialogActions';
import {
  Dialog as UIDialog,
  DialogTitle,
} from '@mintlab/ui/App/Material/Dialog';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useResultsCountQuery } from '../../../../../query/useResults';
import {
  QUERY_KEY_EXPORT_RESULTS,
  QUERY_KEY_EXPORT_RESULTS_COUNT,
} from '../../../../../query/constants';
import { KindType, SavedSearchType } from '../../../../../AdvancedSearch.types';
import { useStyles } from './ExportDialog.styles';
import ExportDialogContent from './ExportDialog.content';
import {
  EVERYTHING_SELECTED_PAGE_LENGTH,
  getIntro,
} from './ExportDialog.library';

/* eslint complexity: [2, 10] */
type ExportDialogPropsType = {
  open: boolean;
  onClose: any;
  t: i18next.TFunction;
  kind: KindType;
  openServerErrorDialog: OpenServerErrorDialogType;
  queryClient: QueryClient;
  page: number;
  everythingSelected: boolean;
  selectedRows: string[];
  resultsPerPage: number;
  setSnack: any;
  currentQueryData: SavedSearchType;
  dataGridState: GridInitialState;
};

const ExportDialog: FunctionComponent<ExportDialogPropsType> = ({
  open,
  onClose,
  t,
  currentQueryData,
  kind,
  openServerErrorDialog,
  queryClient,
  page,
  everythingSelected,
  selectedRows,
  resultsPerPage,
  setSnack,
  dataGridState,
}) => {
  const { columns, filters, uuid } = currentQueryData;
  const classes = useStyles();

  const getSortOptions = () => {
    if (dataGridState?.sorting) {
      return dataGridState.sorting.sortModel &&
        isPopulatedArray(dataGridState?.sorting?.sortModel)
        ? {
            sortColumn: dataGridState.sorting.sortModel[0].field,
            sortOrder: dataGridState.sorting.sortModel[0].sort,
          }
        : null;
    } else if (currentQueryData.sortColumn && currentQueryData.sortOrder) {
      return {
        sortColumn: currentQueryData.sortColumn,
        sortOrder: currentQueryData.sortOrder,
      };
    } else {
      return null;
    }
  };

  const sortOptions = getSortOptions();

  const baseQueryParams = {
    identifier: uuid,
    resultsPerPage: everythingSelected
      ? EVERYTHING_SELECTED_PAGE_LENGTH
      : resultsPerPage,
    kind,
    geo: false,
    filters,
    columns,
    ...(sortOptions && { ...sortOptions }),
  };

  // Count
  const countQuery = useResultsCountQuery({
    queryKey: QUERY_KEY_EXPORT_RESULTS_COUNT,
    queryParams: cloneWithout(
      baseQueryParams,
      'page',
      'resultsPerPage',
      'sortColumn',
      'sortOrder'
    ),
    t,
    openServerErrorDialog,
    onError: () => {},
    enabled: everythingSelected,
    keepPreviousData: false,
  });

  useEffect(
    () => () => {
      queryClient.removeQueries([QUERY_KEY_EXPORT_RESULTS]);
      queryClient.removeQueries([QUERY_KEY_EXPORT_RESULTS_COUNT]);
    },
    []
  );

  return (
    <React.Fragment>
      <UIDialog onClose={onClose} open={open}>
        <DialogTitle
          icon="backup_table"
          title={
            kind === 'custom_object'
              ? t('export.title.objects')
              : t('export.title.cases')
          }
        />
        <Divider />
        <DialogContent>
          <div className={classes.exportDialogSection}>
            {getIntro({
              everythingSelected,
              selectedRows,
              t,
            })}
          </div>

          <div className={classes.exportDialogWrapper}>
            {countQuery.isLoading && everythingSelected ? (
              <Loader />
            ) : (
              <ExportDialogContent
                numPages={
                  everythingSelected && countQuery?.data
                    ? Math.ceil(
                        (countQuery.data as number) /
                          EVERYTHING_SELECTED_PAGE_LENGTH
                      )
                    : 1
                }
                classes={classes}
                t={t}
                queryClient={queryClient}
                baseQueryParams={baseQueryParams}
                page={page}
                everythingSelected={everythingSelected}
                selectedRows={selectedRows}
                setSnack={setSnack}
                dataGridState={dataGridState}
              />
            )}
          </div>
        </DialogContent>
        <Divider />
        <DialogActions>
          <Button name="closeBtn" action={onClose}>
            {t('main:dialog.close')}
          </Button>
        </DialogActions>
      </UIDialog>
    </React.Fragment>
  );
};

export default ExportDialog;
