// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(
  ({ palette: { common, error, secondary, review } }: any) => {
    return {
      resultsTableWrapper: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        overflow: 'scroll',
      },
      resultsTable: {
        flex: '1 1 auto',
        overflow: 'auto',
      },
      resultsTableButtonBar: {
        height: 56,
        display: 'flex',
        '&>:nth-child(1)': {
          width: '70%',
          justifyContent: 'flex-start',
          alignItems: 'center',
          display: 'flex',
          margin: '2px 0px 2px 12px',
        },
        '&>:nth-child(2)': {
          display: 'flex',
          flex: 1,
          justifyContent: 'flex-end',
          '&>div': {
            width: 40,
          },
        },
      },
      resultsTableHeaderColumn: {
        '&&': {
          fontWeight: 800,
        },
      },
      daysWrapper: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      days: {
        display: 'block',
        padding: 4,
        color: common.white,
        borderRadius: 6,
        fontSize: 13,
      },
      daysUrgent: {
        backgroundColor: error.dark,
      },
      daysNonUrgent: {
        backgroundColor: secondary.main,
      },
      daysToday: {
        backgroundColor: review.main,
      },
    };
  }
);
