// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { createPortal } from 'react-dom';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { ViewType } from '../../../../AdvancedSearch.types';

type ResultsTopBarPropsType = {
  menuRef: HTMLElement | null;
  view: ViewType;
  t: i18next.TFunction;
  setView: any;
};

const buttonActive = {
  color: ({ palette: { primary } }: any) => primary.main,
};

const ResultsTopBar: FunctionComponent<ResultsTopBarPropsType> = ({
  menuRef,
  view,
  setView,
  t,
}) => {
  if (menuRef) {
    const Buttons = (
      <>
        <div>
          <Tooltip
            enterDelay={200}
            title={t('results:tableView')}
            placement={'bottom'}
          >
            <Button
              name="tableResultsSwitchTable"
              action={() => setView('table')}
              icon="table_chart"
              iconSize="small"
              {...(view === 'table' && {
                sx: buttonActive,
              })}
            />
          </Tooltip>
        </div>
        <div>
          <Tooltip
            enterDelay={200}
            title={t('results:mapView')}
            placement={'bottom'}
          >
            <Button
              name="tableResultsSwitchMap"
              action={() => setView('map')}
              icon="map"
              iconSize="small"
              {...(view === 'map' && {
                sx: buttonActive,
              })}
            />
          </Tooltip>
        </div>
      </>
    );

    return createPortal(Buttons, menuRef);
  }

  return null;
};

export default ResultsTopBar;
