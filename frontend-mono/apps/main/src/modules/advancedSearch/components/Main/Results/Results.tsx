// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useState } from 'react';
import { UseQueryResult } from '@tanstack/react-query';
import { createPortal } from 'react-dom';
import * as i18next from 'i18next';
import classNames from 'classnames';
import {
  AuthorizationsType,
  ClassesType,
  IdentifierType,
  KindType,
  ModeType,
  SavedSearchType,
  ViewType,
} from '../../../AdvancedSearch.types';
import { DEFAULT_RESULTS_PER_PAGE } from '../../../library/config';
import MainButtons from '../MainButtons';
import ResultsTopBar from './components/ResultsTopBar';
import ResultsView from './ResultsView';
import ResultsPagination from './components/ResultsPagination';
import { Label } from './Results.library';

type ResultsType = {
  classes: ClassesType;
  identifier: IdentifierType;
  kind: KindType;
  t: i18next.TFunction;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
  currentQuery: UseQueryResult<SavedSearchType>;
  setSnack: any;
};

const Results: FunctionComponent<ResultsType> = ({
  classes,
  identifier,
  kind,
  t,
  mode,
  authorizations,
  currentQuery,
  setSnack,
}) => {
  const [page, setPage] = useState<number>(1);
  const [resultsPerPage, setResultsPerPage] = useState(
    DEFAULT_RESULTS_PER_PAGE
  );
  const [totalResults, setTotalResults] = useState(0);
  const [view, setView] = useState<ViewType>('table');
  const [topBarLeftRef, setTopBarLeftRef] = useState<HTMLElement | null>(null);
  const [topBarRightRef, setTopBarRightRef] = useState<HTMLElement | null>(
    null
  );

  if (!currentQuery.data) return null;

  return (
    <>
      <div className={classNames(classes.mainTopBar)}>
        <div className={classes.mainTopBarLeft} ref={setTopBarLeftRef} />
        <div className={classes.mainTopBarRight} ref={setTopBarRightRef} />
      </div>

      {topBarLeftRef &&
        createPortal(
          <>
            <Label
              mode={mode}
              classes={classes}
              currentQuery={currentQuery}
              t={t}
            />
            <MainButtons
              identifier={identifier}
              mode={mode}
              authorizations={authorizations}
              t={t}
            />
          </>,
          topBarLeftRef
        )}

      <div className={classes.resultsWrapper}>
        <ResultsTopBar
          menuRef={topBarRightRef}
          view={view}
          t={t}
          setView={setView}
        />
        <div className={classes.resultsMain}>
          <ResultsView
            key={identifier}
            kind={kind}
            identifier={identifier}
            view={view}
            page={page}
            resultsPerPage={resultsPerPage}
            setTotalResults={setTotalResults}
            portalRef={topBarLeftRef}
            setSnack={setSnack}
            setPage={setPage}
          />
        </div>
        <ResultsPagination
          classes={classes}
          totalResults={totalResults}
          page={page}
          resultsPerPage={resultsPerPage}
          setPage={setPage}
          setResultsPerPage={setResultsPerPage}
        />
      </div>
    </>
  );
};

export default Results;
