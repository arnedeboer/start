// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  Dispatch,
  SetStateAction,
  MutableRefObject,
} from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import {
  DataGridPro,
  GridColDef,
  useGridApiRef,
  GridValidRowModel,
  GridInitialState,
  GridLocaleText,
  GridRowSelectionModel,
  GridRowParams,
  MuiEvent,
  GridApiCommon,
} from '@mui/x-data-grid-pro';
import * as i18next from 'i18next';
import datagridLocale from '@zaaksysteem/common/src/locale/datagrid.locale';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import TableSelectionControls from '@zaaksysteem/common/src/components/TableSelectionControls/TableSelectionControls';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { ResultRowType, KindType } from '../../../AdvancedSearch.types';

type ResultsTablePropsType = {
  columns: GridColDef[];
  rows: ResultRowType[];
  t: i18next.TFunction;
  kind: KindType;
  selectionProps: useSelectionBehaviourReturnType;
  initialState: GridInitialState;
  setDataGridState: Dispatch<SetStateAction<GridInitialState>>;
  identifier: string;
  resetToInitialState: (apiRef: MutableRefObject<GridApiCommon>) => void;
  isFetching: boolean;
  classes: any;
};

const DEBOUNCE_DELAY = 100;

/* eslint complexity: [2, 10] */
const ResultsTable: FunctionComponent<ResultsTablePropsType> = ({
  columns,
  rows,
  t,
  kind,
  selectionProps,
  initialState,
  setDataGridState,
  identifier,
  resetToInitialState,
  isFetching,
  classes,
}) => {
  const apiRef = useGridApiRef();

  const [dataGridT] = useTranslation('datagrid');
  const dataGridTranslations = dataGridT('datagrid', {
    returnObjects: true,
  }) as GridLocaleText;

  const handleDoubleClick = (
    params: GridRowParams,
    event: MuiEvent<React.MouseEvent>
  ) => {
    event.stopPropagation();
    event.preventDefault();

    const url =
      kind === 'custom_object'
        ? `/main/object/${params.row.versionIndependentUuid}`
        : `/redirect/case?uuid=${params.row.uuid}`;
    top?.window.open(url, '_new');
  };

  const parsedRows: GridValidRowModel[] = rows.map(
    ({ columns, uuid, versionIndependentUuid }) => ({
      ...columns,
      id: uuid,
      uuid,
      versionIndependentUuid,
    })
  );

  const [debounced] = useDebouncedCallback(() => {
    setDataGridState(
      apiRef.current.exportState({ exportOnlyDirtyModels: true })
    );
  }, DEBOUNCE_DELAY);

  const {
    pageSelected,
    everythingSelected,
    onSelectEverything,
    selectEverythingTranslations,
  } = selectionProps;

  return (
    <div className={classes.resultsTableWrapper}>
      <div className={classes.resultsTableButtonBar}>
        <div>
          <TableSelectionControls
            pageSelected={pageSelected}
            everythingSelected={everythingSelected}
            onSelectEverything={onSelectEverything}
            selectEverythingTranslations={selectEverythingTranslations}
          />
        </div>
        <div>
          <Tooltip title={t('resetToDefault')}>
            <Button
              name="resetSettings"
              icon="delete_sweep"
              scope={`advanced-search:reset-settings`}
              iconSize="medium"
              sx={{ padding: '4px' }}
              action={() => resetToInitialState(apiRef)}
            />
          </Tooltip>
        </div>
      </div>
      <div className={classes.resultsTable}>
        <DataGridPro
          key={identifier}
          apiRef={apiRef}
          initialState={initialState}
          rows={parsedRows}
          columns={columns}
          pagination={false}
          sortingMode="server"
          checkboxSelection={true}
          onRowSelectionModelChange={(
            rowSelectionModel: GridRowSelectionModel
          ) => selectionProps.setSelectedRows(rowSelectionModel as string[])}
          disableRowSelectionOnClick={true}
          localeText={dataGridTranslations}
          onRowDoubleClick={handleDoubleClick}
          loading={isFetching}
          onColumnOrderChange={debounced}
          onColumnResize={debounced}
          onColumnVisibilityModelChange={debounced}
          onFilterModelChange={debounced}
          onPaginationModelChange={debounced}
          onPinnedColumnsChange={debounced}
          onSortModelChange={debounced}
          onMenuClose={debounced}
          onMenuOpen={debounced}
          onPreferencePanelClose={debounced}
          onPreferencePanelOpen={debounced}
          classes={{
            columnHeaderTitle: classes.resultsTableHeaderColumn,
          }}
        />
      </div>
    </div>
  );
};

const ResultsTableWithLocale = (props: any) => {
  return (
    <I18nResourceBundle resource={datagridLocale} namespace="datagrid">
      <ResultsTable {...props} />
    </I18nResourceBundle>
  );
};

export default React.memo(ResultsTableWithLocale);
