// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import * as Papaparse from 'papaparse';
import * as i18next from 'i18next';
import { UseQueryResult } from '@tanstack/react-query';
import { GridInitialState } from '@mui/x-data-grid-pro';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import {
  ColumnType,
  ExportFormatType,
  ExportResultsRowType,
  ParseResultsModeType,
  GetResultsReturnType,
} from '../../../../../AdvancedSearch.types';
import { APIResultsToResults } from '../../../../../query/useResults';
import { getUniqueColumnIdentifier } from '../../../../../library/library';
import { CHECKBOX_COLUMN_ID } from '../../../../../library/config';

export const EVERYTHING_SELECTED_PAGE_LENGTH = 25;

export const getIntro = ({
  everythingSelected,
  selectedRows,
  t,
}: {
  everythingSelected: boolean;
  selectedRows: string[];
  t: i18next.TFunction;
}): string => {
  let output = `${t('export.intro')} `;
  if (everythingSelected) {
    output += `${t('export.allRows')} `;
  } else {
    output +=
      selectedRows.length === 1
        ? `${t('export.oneRow')} `
        : `${t('export.numRows', {
            numRows: selectedRows.length,
          })} `;
  }
  output += t('export.waitInstructions');
  return output;
};

export const parseResults = ({
  pages,
  selectedRows,
  everythingSelected,
  columns,
  dataGridState,
  parseResultsMode,
  t,
}: {
  pages: UseQueryResult<GetResultsReturnType>[];
  selectedRows: string[];
  everythingSelected: boolean;
  columns: ColumnType[];
  dataGridState: GridInitialState;
  parseResultsMode: ParseResultsModeType;
  t: i18next.TFunction;
}): ExportResultsRowType[] => {
  const resultRows: ExportResultsRowType[] = [];

  const getParsedColumns = () => {
    const colNames = isPopulatedArray(dataGridState?.columns?.orderedFields)
      ? dataGridState?.columns?.orderedFields
      : (columns || []).map((column: any) => getUniqueColumnIdentifier(column));

    const orderedColumns = (colNames || [])
      .filter((colName: any) => colName !== CHECKBOX_COLUMN_ID)
      .map((name: string) =>
        columns.find(col => name === getUniqueColumnIdentifier(col))
      );

    const visibleColumns = orderedColumns.filter(
      (column: ColumnType | undefined) => {
        if (!column || !dataGridState?.columns?.columnVisibilityModel)
          return true;

        return dataGridState?.columns?.columnVisibilityModel[
          getUniqueColumnIdentifier(column)
        ] === false
          ? false
          : true;
      }
    );

    return visibleColumns;
  };

  pages.forEach(page => {
    const rows = APIResultsToResults({
      classes: null,
      data: page.data,
      columns: columns || [],
      t,
      parseResultsMode,
    });

    (rows || []).forEach(row => {
      const newRow: ExportResultsRowType = [];

      (getParsedColumns() || []).forEach(column => {
        if (column) {
          const uniqueIdentifier = getUniqueColumnIdentifier(column);
          newRow.push({
            label: column.label,
            value: row.columns[uniqueIdentifier],
            uniqueIdentifier,
          });
        }
      });

      if (!everythingSelected && selectedRows && selectedRows.length) {
        if (selectedRows.includes(row.uuid)) resultRows.push(newRow);
      } else {
        resultRows.push(newRow);
      }
    });
  });

  return resultRows;
};

export const resultsToPapaparse = async (
  results: ExportResultsRowType[],
  delimiter: string
) => {
  const formattedResults = results.map(row =>
    row.reduce((acc, current) => {
      acc[current.label] = current.value;
      return acc;
    }, {} as any)
  );

  return Papaparse.unparse(formattedResults, {
    delimiter,
  });
};

export const initiateDownload = (file: any, format: ExportFormatType) => {
  var blob = new Blob([file], {
    type: `text/${format.toLowerCase()};charset=utf-8;`,
  });
  var link = document.createElement('a');
  if (link.download !== undefined) {
    var url = URL.createObjectURL(blob);
    link.setAttribute('href', url);
    link.setAttribute('download', `export.${format.toLowerCase()}`);
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
};
