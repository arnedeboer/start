// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useState, useEffect, FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { useTransition } from '@zaaksysteem/common/src/hooks/useTransition';
import {
  QueryClient,
  useQueries,
  UseQueryOptions,
  UseQueryResult,
} from '@tanstack/react-query';
import { GridInitialState } from '@mui/x-data-grid-pro';
import {
  getQueryConfig,
  UseResultsQueryQueryParamsType,
} from '../../../../../query/useResults';
import {
  QUERY_KEY_EXPORT_RESULTS,
  QUERY_KEY_EXPORT_RESULTS_COUNT,
} from '../../../../../query/constants';
import {
  ExportFormatType,
  ParseResultsModeType,
  ExportResultsRowType,
} from '../../../../../AdvancedSearch.types';
import { parseResults, resultsToPapaparse } from './ExportDialog.library';

type ExportGeneratorPropsType = {
  key: string;
  numPages: number;
  format: ExportFormatType;
  parseResultsMode: ParseResultsModeType;
  baseQueryParams: Omit<UseResultsQueryQueryParamsType, 'page'>;
  handleDataDone: (downloadFile: any) => void;
  t: i18next.TFunction;
  handleProgressUpdate: (progressPercentage: any) => void;
  queryClient: QueryClient;
  page: number;
  everythingSelected: boolean;
  selectedRows: string[];
  handleError: () => void;
  dataGridState: GridInitialState;
};

const ExportGenerator: FunctionComponent<ExportGeneratorPropsType> = ({
  numPages,
  format,
  parseResultsMode,
  baseQueryParams,
  handleDataDone,
  t,
  handleProgressUpdate,
  queryClient,
  page,
  everythingSelected,
  selectedRows,
  handleError,
  dataGridState,
}) => {
  const [finished, setFinished] = useState<boolean>(false);
  const [finishedPages, setFinishedPages] = useState<number[]>([]);
  let allQueriesConfig: UseQueryOptions[];

  const commonQueryParams = {
    queryKey: QUERY_KEY_EXPORT_RESULTS,
    t,
    parseResultsMode,
    onError: handleError,
    keepPreviousData: false,
  };

  if (everythingSelected) {
    // All pages results
    const nrsArr = Array.from(Array(numPages).keys());
    allQueriesConfig = nrsArr.map(index => {
      const pageNr = index + 1;
      const getEnabled = () => {
        if (pageNr === 1) {
          return true;
        } else if (finishedPages.includes(index - 1)) return true;
        return false;
      };

      return getQueryConfig({
        queryParams: {
          ...baseQueryParams,
          page: pageNr,
        },
        enabled: getEnabled(),
        ...commonQueryParams,
      });
    });
  } else {
    // Single page results
    allQueriesConfig = [
      getQueryConfig({
        queryParams: {
          ...baseQueryParams,
          page,
        },
        enabled: true,
        ...commonQueryParams,
      }),
    ];
  }

  const allQueries: UseQueryResult<any>[] = useQueries({
    queries: allQueriesConfig,
  });

  useEffect(() => {
    const reducedFinishedPages = allQueries.reduce((acc, current, index) => {
      if (current.isSuccess) acc.push(index);

      return acc;
    }, [] as number[]);

    if (finishedPages.length !== reducedFinishedPages.length)
      setFinishedPages(reducedFinishedPages);

    if (reducedFinishedPages.length === numPages) setFinished(true);
  }, [allQueries]);

  useTransition(
    (prevFinishedPages: number[]) => {
      if (prevFinishedPages.length !== finishedPages.length) {
        handleProgressUpdate((finishedPages.length / numPages) * 100);
      }
    },
    [finishedPages]
  );

  useTransition(
    async (prevFinished: boolean) => {
      if (prevFinished === false && finished && baseQueryParams.columns) {
        const results: ExportResultsRowType[] = parseResults({
          pages: allQueries,
          selectedRows,
          everythingSelected,
          columns: baseQueryParams.columns,
          dataGridState,
          parseResultsMode,
          t,
        });

        handleDataDone(
          await resultsToPapaparse(results, format === 'CSV' ? ',' : '\t')
        );
      }
    },
    [finished]
  );

  useEffect(() => {
    return () => {
      queryClient.removeQueries([QUERY_KEY_EXPORT_RESULTS]);
      queryClient.invalidateQueries([QUERY_KEY_EXPORT_RESULTS_COUNT]);
    };
  }, []);

  return null;
};

export default ExportGenerator;
