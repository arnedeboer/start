// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  useState,
  Dispatch,
  SetStateAction,
} from 'react';
import type { unstable_Blocker as Blocker } from 'react-router-dom';
import * as i18next from 'i18next';
import { useNavigate } from 'react-router-dom';
import { UseQueryResult, QueryClient } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { Formik } from 'formik';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  IdentifierType,
  KindType,
  ModeType,
  SavedSearchType,
  ClassesType,
  SnackType,
  AuthorizationsType,
} from '../../AdvancedSearch.types';
import { invalidateAllQueries } from '../../query/library';
import { useSingleSaveMutation } from '../../query/useSingle';
import EditFormTopBar from './EditFormTopBar';
import { getInitialState } from './EditForm.library';
import EditFormInner from './EditFormInner';

type EditFormPropsType = {
  classes: ClassesType;
  mode: ModeType;
  identifier?: IdentifierType | null;
  currentQuery: UseQueryResult<SavedSearchType, V2ServerErrorsType>;
  kind: KindType;
  setSnack: Dispatch<SetStateAction<SnackType | null>>;
  client: QueryClient;
  openServerErrorDialog: OpenServerErrorDialogType;
  authorizations?: AuthorizationsType[];
  t: i18next.TFunction;
};

/* eslint complexity: [2, 12] */
const EditForm: FunctionComponent<EditFormPropsType> = ({
  classes,
  mode,
  identifier,
  currentQuery,
  kind,
  setSnack,
  client,
  openServerErrorDialog,
  authorizations,
  t,
}) => {
  const [tabValue, setTabValue] = useState<number>(0);
  const [blocker, setBlocker] = useState<Blocker | null>(null);
  const handleTabChange = (event: any, newValue: number) =>
    setTabValue(newValue);

  const saveMutation = useSingleSaveMutation({
    kind,
    identifier,
    mode,
    authorizations,
  });
  const navigate = useNavigate();
  if (
    mode === 'edit' &&
    (!authorizations || !currentQuery.isSuccess || !currentQuery.data)
  ) {
    return <Loader />;
  }

  return (
    <Formik
      enableReinitialize={true}
      initialValues={getInitialState({ mode, currentQuery })}
      isInitialValid={false}
      onReset={() => {
        blocker?.reset?.();
      }}
      onSubmit={async (values, formik) => {
        saveMutation.mutate(values, {
          onSuccess: (data: any) => {
            invalidateAllQueries(client);
            formik?.resetForm?.();
            setSnack({
              message: t('snacks.saved'),
              open: true,
            });
            navigate(`../view/${data}`);
          },
          onError: error => openServerErrorDialog(error),
        });
      }}
    >
      {formik => {
        return (
          <>
            <EditFormTopBar
              classes={classes}
              mode={mode}
              t={t}
              currentQuery={currentQuery}
            />
            <EditFormInner
              formik={formik}
              classes={classes}
              tabValue={tabValue}
              handleTabChange={handleTabChange}
              kind={kind}
              client={client}
              setTabValue={setTabValue}
              saveMutation={saveMutation}
              authorizations={authorizations}
              mode={mode}
              t={t}
              identifier={identifier}
              openServerErrorDialog={openServerErrorDialog}
              setBlocker={setBlocker}
              blocker={blocker}
            />
          </>
        );
      }}
    </Formik>
  );
};

export default EditForm;
