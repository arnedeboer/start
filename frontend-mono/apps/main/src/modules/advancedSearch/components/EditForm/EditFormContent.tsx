// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { FormikProps } from 'formik';
import * as i18next from 'i18next';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { hasAccess } from '../../library/library';
import {
  AuthorizationsType,
  CustomFieldType,
  EditFormStateType,
  IdentifierType,
  KindType,
  ModeType,
} from '../../AdvancedSearch.types';
import Filters from './Filters/Filters';
import Permissions from './Permissions/Permissions';
import Columns from './Columns/Columns';
import EditFormTabs from './Tabs';

type EditFormContentPropsType = {
  classes: any;
  handleTabChange: any;
  authorizations?: AuthorizationsType[];
  t: i18next.TFunction;
  mode: ModeType;
  kind: KindType;
  formik: FormikProps<EditFormStateType>;
  client: any;
  tabValue: number;
  customFields: CustomFieldType[];
  identifier?: IdentifierType | null;
  openServerErrorDialog: OpenServerErrorDialogType;
};

const EditFormContent: FunctionComponent<EditFormContentPropsType> = ({
  classes,
  handleTabChange,
  authorizations,
  t,
  mode,
  kind,
  formik,
  client,
  tabValue,
  customFields,
  identifier,
  openServerErrorDialog,
}) => {
  return (
    <>
      <EditFormTabs
        classes={classes}
        tabValue={tabValue}
        handleTabChange={handleTabChange}
        authorizations={authorizations}
        mode={mode}
        t={t}
      />
      <div className={classes.editFormTabsWrapper}>
        <div className={classes.tabsContent}>
          <Filters
            kind={kind}
            formik={formik}
            classes={classes}
            show={tabValue === 0}
            t={t}
            client={client}
            openServerErrorDialog={openServerErrorDialog}
            customFields={customFields}
          />
          {(mode === 'new' ||
            (mode === 'edit' &&
              authorizations &&
              hasAccess(authorizations, 'admin'))) && (
            <Permissions
              classes={classes}
              formik={formik}
              show={tabValue === 1}
              mode={mode}
              t={t}
            />
          )}
          <Columns
            classes={classes}
            show={tabValue === 2}
            t={t}
            customFields={customFields}
            kind={kind}
            mode={mode}
            identifier={identifier}
            openServerErrorDialog={openServerErrorDialog}
          />
        </div>
      </div>
    </>
  );
};

export default EditFormContent;
