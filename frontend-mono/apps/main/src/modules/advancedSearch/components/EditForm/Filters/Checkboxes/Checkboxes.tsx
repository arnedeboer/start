// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { Field, FieldProps } from 'formik';
import { FormControlLabel, Checkbox, FormGroup } from '@mui/material';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { ValidateFuncType } from '../../../../AdvancedSearch.types';
import { FilterCommonPropsType } from '../Filters.types';

interface CheckboxesPropsType extends FilterCommonPropsType {
  choices: ValueType<string>[];
  validate?: ValidateFuncType<string[]> | undefined;
}

type CheckboxesCmpPropsType = {
  field: FieldProps['field'];
} & Pick<CheckboxesPropsType, 'choices'>;

const CheckboxesCmp: FunctionComponent<CheckboxesCmpPropsType> = ({
  field,
  choices,
}) => {
  return (
    <FormGroup
      aria-labelledby={`checkboxes-group-${field.name}`}
      {...field}
      onChange={(event: any) => {
        const { value } = event.target;
        const newValue = field.value.includes(value)
          ? field.value.filter((item: any) => item !== value)
          : [...field.value, value];
        field.onChange({
          ...event,
          target: {
            ...event.target,
            name: field.name,
            value: newValue,
          },
        });
      }}
    >
      {(choices || []).map(choice => (
        <FormControlLabel
          key={`${field.name}.${choice.value}`}
          control={
            <Checkbox
              checked={field.value.includes(choice.value)}
              name={`${field.name}.${choice.value}`}
              value={`${choice.value}`}
              inputProps={{ 'aria-label': choice.label as string }}
            />
          }
          label={choice.label}
        />
      ))}
    </FormGroup>
  );
};

const Checkboxes: FunctionComponent<CheckboxesPropsType> = ({
  name,
  choices,
  validate,
}) => {
  return (
    <div>
      <Field
        name={name}
        component={CheckboxesCmp}
        choices={choices}
        validate={validate}
      />
    </div>
  );
};

export default Checkboxes;
