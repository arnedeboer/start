// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { FormikProps, FieldInputProps } from 'formik';
import Button from '@mintlab/ui/App/Material/Button';
import { get } from '@mintlab/kitchen-sink/source';
import { getReadableValue } from '../DateEntries.library';
import {
  DateEntryType,
  DateModeType,
  EditFormStateType,
} from '../../../../../AdvancedSearch.types';
import { DialogPropertiesType } from '../DateEntries.types';

type DateEntryPropsType = {
  form: FormikProps<EditFormStateType>;
  field: FieldInputProps<any>;
  t: i18next.TFunction;
  setDialogProperties: (dialogProperties: DialogPropertiesType) => void;
  index: number;
  classes: any;
  mode: DateModeType;
};

const DateEntry: FunctionComponent<DateEntryPropsType> = ({
  form,
  field,
  t,
  setDialogProperties,
  index,
  mode,
}) => {
  const { name } = field;
  const { values } = form;
  const entry = get(values, name) as DateEntryType;

  return (
    <Button
      name="getReadableEntry"
      variant="outlined"
      action={() => {
        setDialogProperties({
          open: true,
          entry,
          index,
        });
      }}
    >
      {getReadableValue(entry, t, mode)}
    </Button>
  );
};

export default DateEntry;
