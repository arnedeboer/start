// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';

import * as i18next from 'i18next';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { FormikProps } from 'formik';
import { renderTagsWithIcon } from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  FilterType,
  EditFormStateType,
} from '../../../../AdvancedSearch.types';
import {
  validateRegExp,
  validateMinSelected,
} from '../../../../library/validations';
import CreatableSelect from '../Select/CreatableSelect';
import DateEntries from '../DateEntries/DateEntries';
import SelectField from '../Select/Select';
import CustomField from '../CustomField/CustomField';
import Contacts from '../Contacts/Contacts';
import CaseType from '../CaseType/CaseType';
import Checkboxes from '../Checkboxes/Checkboxes';
import DepartmentRole from '../DepartmentRole/DepartmentRole';
import Address from '../Address/Address';
import { FilterCommonPropsType } from '../Filters.types';
import { getDateMode } from '../../../../library/config';
import { hasNoValue, getChoicesFromTranslation } from './Filters.library';

const TRANSLATION_BASE = 'editForm.fields.filters';

/* eslint complexity: [2, 30] */
export const getField = ({
  t,
  filter,
  index,
  name,
  formik,
}: {
  t: i18next.TFunction;
  filter: FilterType;
  index: number;
  name: string;
  formik: FormikProps<EditFormStateType>;
}) => {
  if (!filter) return null;
  const { type, uuid, parameters } = filter;
  const commonProps: FilterCommonPropsType = {
    name,
    identifier: filter.uuid,
    t,
    index,
    formik,
  };

  switch (type) {
    case 'attributes.last_modified':
    case 'attributes.completion_date':
    case 'attributes.registration_date': {
      return <DateEntries {...commonProps} mode={getDateMode(type)} />;
    }
    case 'keyword': {
      const translatedLabel = t(`${TRANSLATION_BASE}.fields.keyword.label`);
      return (
        <CreatableSelect
          {...commonProps}
          placeholder={
            t(`${TRANSLATION_BASE}.fields.keyword.placeholder`) as string
          }
          validate={validateRegExp(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.keyword.errorMessage`, {
              name: translatedLabel,
            }),
            'text'
          )}
        />
      );
    }
    case 'attributes.status': {
      return (
        <SelectField
          {...commonProps}
          choices={['active', 'inactive', 'draft'].map((entry: string) => ({
            label: t(
              `${TRANSLATION_BASE}.fields.status.choices.${entry}`
            ) as string,
            value: entry,
          }))}
          validate={value =>
            hasNoValue(value)
              ? {
                  value: t(`${TRANSLATION_BASE}.fields.status.errorMessage`),
                  uuid,
                }
              : undefined
          }
          placeholder={
            t(`${TRANSLATION_BASE}.fields.status.placeholder`) as string
          }
        />
      );
    }
    case 'attributes.archive_status': {
      return (
        <SelectField
          {...commonProps}
          choices={[
            [
              t(`${TRANSLATION_BASE}.fields.archiveStatus.choices.archived`),
              'archived',
            ],
            [
              t(`${TRANSLATION_BASE}.fields.archiveStatus.choices.toDestroy`),
              'to destroy',
            ],
            [
              t(`${TRANSLATION_BASE}.fields.archiveStatus.choices.toPreserve`),
              'to preserve',
            ],
          ].map((entry: any[]) => ({
            label: entry[0],
            value: entry[1],
          }))}
          validate={value =>
            hasNoValue(value)
              ? {
                  value: t(
                    `${TRANSLATION_BASE}.fields.archiveStatus.errorMessage`
                  ),
                  uuid,
                }
              : undefined
          }
          placeholder={t(
            `${TRANSLATION_BASE}.fields.archiveStatus.placeholder`
          )}
        />
      );
    }
    case 'attributes.archival_state': {
      const archivalChoices = [
        [
          t(
            `${TRANSLATION_BASE}.fields.archivalState.choices.vernietigen`
          ) as string,
          'vernietigen',
        ],
        [
          t(`${TRANSLATION_BASE}.fields.archivalState.choices.overdragen`),
          'overdragen',
        ],
      ].map((entry: any[]) => ({
        label: entry[0],
        value: entry[1],
      }));

      return (
        <SelectField
          {...commonProps}
          choices={archivalChoices}
          validate={value =>
            !value
              ? {
                  uuid,
                  value: t(
                    `${TRANSLATION_BASE}.fields.archivalState.errorMessage`
                  ),
                }
              : undefined
          }
          placeholder={t(
            `${TRANSLATION_BASE}.fields.archivalState.placeholder`
          )}
          nestedValue={true}
        />
      );
    }
    case 'relationship.assignee.id': {
      return (
        <Contacts
          {...commonProps}
          showContactTypes={false}
          translations={{
            'form:choose': t(`${TRANSLATION_BASE}.fields.assignee.placeholder`),
            errorMessage: t(`${TRANSLATION_BASE}.fields.assignee.errorMessage`),
          }}
        />
      );
    }
    case 'relationship.requestor.id': {
      return (
        <Contacts
          {...commonProps}
          showContactTypes={true}
          translations={{
            'form:choose': t(
              `${TRANSLATION_BASE}.fields.requestor.placeholder`
            ),
            errorMessage: t(
              `${TRANSLATION_BASE}.fields.requestor.errorMessage`
            ),
          }}
        />
      );
    }
    case 'relationship.coordinator.id': {
      return (
        <Contacts
          {...commonProps}
          showContactTypes={false}
          showActiveChoice={true}
          translations={{
            'form:choose': t(
              `${TRANSLATION_BASE}.fields.coordinator.placeholder`
            ),
            errorMessage: t(
              `${TRANSLATION_BASE}.fields.coordinator.errorMessage`
            ),
          }}
        />
      );
    }
    case 'attributes.urgency': {
      return (
        <SelectField
          {...commonProps}
          choices={['normal', 'medium', 'high', 'late'].map(value => ({
            label: t(
              `${TRANSLATION_BASE}.fields.urgency.choices.${value}`
            ) as string,
            value,
          }))}
        />
      );
    }
    case 'relationship.case_type.id': {
      return <CaseType {...commonProps} />;
    }
    case 'attributes.payment_status': {
      const paymentStatusObjects = t(
        `${TRANSLATION_BASE}.fields.paymentStatus.choices`,
        {
          returnObjects: true,
        }
      );
      return (
        <Checkboxes
          {...commonProps}
          choices={Object.entries(paymentStatusObjects).map(paymentStatus => ({
            label: paymentStatus[1],
            value: paymentStatus[0],
          }))}
          validate={value =>
            !isPopulatedArray(value)
              ? {
                  value: t(
                    `${TRANSLATION_BASE}.fields.paymentStatus.errorMessage`
                  ) as string,
                  uuid: filter.uuid,
                }
              : undefined
          }
        />
      );
    }
    case 'attributes.department_role': {
      return <DepartmentRole {...commonProps} />;
    }
    case 'attributes.case_location': {
      return (
        <Address
          {...commonProps}
          placeholder={
            t(`${TRANSLATION_BASE}.fields.caseLocation.placeholder`) as string
          }
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.caseLocation.errorMessage`)
          )}
          isClearable={false}
        />
      );
    }
    case 'attributes.channel_of_contact': {
      return (
        <Checkboxes
          {...commonProps}
          choices={getChoicesFromTranslation(t, 'contactChannel')}
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.contactChannel.errorMessage`)
          )}
        />
      );
    }
    case 'attributes.result': {
      const resultObjects = t(`${TRANSLATION_BASE}.fields.result.choices`, {
        returnObjects: true,
      });
      return (
        <SelectField
          {...commonProps}
          choices={Object.entries(resultObjects).map(result => ({
            label: result[1],
            value: result[0],
          }))}
          isMulti={true}
          placeholder={t(`${TRANSLATION_BASE}.fields.result.placeholder`)}
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.result.errorMessage`)
          )}
          renderTags={renderTagsWithIcon('check_circle')}
        />
      );
    }
    case 'attributes.confidentiality': {
      return (
        <Checkboxes
          {...commonProps}
          choices={getChoicesFromTranslation(t, 'confidentiality')}
          validate={validateMinSelected(
            commonProps.identifier,
            t(`${TRANSLATION_BASE}.fields.confidentiality.errorMessage`)
          )}
        />
      );
    }
    case 'attributes.value': {
      const { type } = parameters.value;
      return <CustomField {...commonProps} type={type} />;
    }
    default:
      return null;
  }
};
