// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';

type HelpInfoPropsType = {
  classes: any;
  t: i18next.TFunction;
  intro?: i18next.TFunctionResult;
  list: i18next.TFunctionResult[];
};

const HelpListing: FunctionComponent<HelpInfoPropsType> = ({
  classes,
  intro,
  list,
}) => {
  return (
    <div className={classes.permissionsText}>
      <>
        {intro || null}
        {list && list.length && (
          <>
            <ul>
              {list.map((item, index: number) => (
                <li key={index}>{item as React.ReactNode}</li>
              ))}
            </ul>
          </>
        )}
      </>
    </div>
  );
};

export default HelpListing;
