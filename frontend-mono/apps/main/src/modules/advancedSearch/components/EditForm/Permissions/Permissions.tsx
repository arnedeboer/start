// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, ReactElement } from 'react';
import * as i18next from 'i18next';
import classNames from 'classnames';
import Button from '@mintlab/ui/App/Material/Button';
import { Field, FieldArray, FormikProps } from 'formik';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  EditFormStateType,
  ModeType,
  PermissionType,
  ClassesType,
} from '../../../AdvancedSearch.types';
import HelpListing from '../components/HelpListing';
import { validateForm } from '../EditForm.library';
import PermissionEntry from './PermissionEntry';

type PermissionsPropsType = {
  classes: ClassesType;
  formik: FormikProps<EditFormStateType>;
  show: Boolean;
  t: i18next.TFunction;
  mode: ModeType;
};

const FIELDNAME = 'permissions';
const TRANSLATION_BASE = 'editForm.fields.permissions.';

const Permissions: FunctionComponent<PermissionsPropsType> = ({
  classes,
  formik,
  show,
  t,
  mode,
}) => {
  const { values } = formik;

  return (
    <div
      className={classNames(classes.editFormSection, {
        [classes.show]: show,
        [classes.hide]: !show,
      })}
      style={{ flexDirection: 'column' }}
    >
      <FieldArray
        name={FIELDNAME}
        render={arrayHelpers => {
          return (
            <div className={classes.permissionsWrapper}>
              <div className={classes.permissionsInfo}>
                <div>
                  <Tooltip
                    enterDelay={300}
                    title={
                      HelpListing({
                        classes,
                        t,
                        intro: t(`${TRANSLATION_BASE}helpInfo.intro`),
                        list: [
                          t(`${TRANSLATION_BASE}helpInfo.owner`),
                          t(`${TRANSLATION_BASE}helpInfo.sharedEditing`),
                          t(`${TRANSLATION_BASE}helpInfo.sharedNotEditing`),
                        ],
                      }) as ReactElement
                    }
                  >
                    <Icon size="small" color="inherit">
                      {iconNames.help}
                    </Icon>
                  </Tooltip>
                </div>
              </div>
              <div className={classes.newPermission}>
                <Button
                  name="addNewPermission"
                  action={() => {
                    arrayHelpers.insert(0, {
                      groupID: null,
                      roleID: null,
                      writePermission: false,
                      saved: false,
                    });
                    validateForm(formik);
                  }}
                  sx={{
                    textAlign: 'right',
                    width: '86px',
                    marginBottom: '24px',
                    marginRight: '50px',
                    alignSelf: 'flex-end',
                  }}
                >
                  {`+ ${t('search:new')}`}
                </Button>
              </div>
              {values.permissions.length === 0 && (
                <p>{t(`${TRANSLATION_BASE}hint`) as string}</p>
              )}
              {values.permissions.map((permission, index) => {
                return (
                  <Field
                    name={`${FIELDNAME}.[${index}]`}
                    component={PermissionEntry}
                    key={`${FIELDNAME}-${index}`}
                    mode={mode}
                    classes={classes}
                    arrayHelpers={arrayHelpers}
                    index={index}
                    t={t}
                    validate={(values: PermissionType) => {
                      if (!values?.groupID)
                        return t(`${TRANSLATION_BASE}group.errorMessage`);
                      if (!values?.roleID)
                        return t(`${TRANSLATION_BASE}role.errorMessage`);
                    }}
                    formik={formik}
                  />
                );
              })}
            </div>
          );
        }}
      />
    </div>
  );
};

export default Permissions;
