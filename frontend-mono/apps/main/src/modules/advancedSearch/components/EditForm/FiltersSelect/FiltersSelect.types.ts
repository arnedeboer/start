// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormikProps, FieldInputProps, FieldArrayRenderProps } from 'formik';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  KindType,
  EditFormStateType,
  CustomFieldType,
} from '../../../AdvancedSearch.types';

export type FiltersSelectPropsType = {
  arrayHelpersRef: FieldArrayRenderProps | null;
  formik: FormikProps<EditFormStateType>;
  t: i18next.TFunction;
  kind: KindType;
  openServerErrorDialog: OpenServerErrorDialogType;
  classes: any;
  customFields: CustomFieldType[];
};

export type SelectSystemAttributesPropsType = {
  arrayHelpers: FieldArrayRenderProps;
  name: string;
  choices: any;
  value: any;
  form: FormikProps<EditFormStateType>;
  field: FieldInputProps<any>;
  t: i18next.TFunction;
  kind: KindType;
};

export type MethodType = 'systemAttributes' | 'searchAttributes';
