// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { SORTABLE_COLUMNS } from '../../../library/config';
import { getUniqueColumnIdentifier } from '../../../library/library';
import {
  ColumnType,
  SortDirectionType,
  ClassesType,
} from '../../../AdvancedSearch.types';

type ColumnEntryPropsType = {
  index: number;
  provided: any;
  item: ColumnType;
  classes: ClassesType;
  onDelete: any;
  setFieldValue: any;
  sortColumn?: String;
  sortOrder?: SortDirectionType;
  t: i18next.TFunction;
  name: string;
};

/* eslint complexity: [2, 12] */
const ColumnEntry: FunctionComponent<ColumnEntryPropsType> = ({
  index,
  provided,
  item,
  classes,
  onDelete,
  setFieldValue,
  sortColumn,
  sortOrder,
  t,
  name,
}) => {
  const uniqueIdentifier = getUniqueColumnIdentifier(item);
  const isSortable = SORTABLE_COLUMNS.some(
    (identifier: string) => identifier === uniqueIdentifier
  );
  const { visible } = item;

  const getIcon = () => {
    if (sortColumn && sortColumn === uniqueIdentifier) {
      return sortOrder === 'asc'
        ? iconNames.arrow_drop_up
        : iconNames.arrow_drop_down;
    } else {
      return iconNames.sort;
    }
  };

  const cycleNext = () => {
    let newSortColumn;
    let newSortOrder;

    if (sortColumn && sortColumn === uniqueIdentifier) {
      if (sortOrder === 'asc') {
        newSortColumn = uniqueIdentifier;
        newSortOrder = 'desc';
      } else if (sortOrder === 'desc') {
        newSortColumn = null;
        newSortOrder = null;
      }
    } else {
      newSortColumn = uniqueIdentifier;
      newSortOrder = 'asc';
    }

    setFieldValue('sortColumn', newSortColumn);
    setFieldValue('sortOrder', newSortOrder);
  };

  const cycleVisibility = () =>
    setFieldValue(`${name}[${index}].visible`, !visible);

  return (
    <>
      <div
        ref={provided.innerRef}
        {...provided.draggableProps}
        {...provided.dragHandleProps}
        className={classes.columnsColumnEntry}
        style={provided.draggableProps.style}
      >
        <div className={classes.columnsEntryDragIndicator}>
          <Icon size="small" color="inherit">
            {iconNames.drag_indicator}
          </Icon>
        </div>
        <div>{item.label}</div>
        <div className={classes.columnsEntryActionButtons}>
          <div className={classes.columnsEntryVis}>
            <IconButton
              onClick={() => cycleVisibility()}
              disableRipple={true}
              size="small"
            >
              <Tooltip
                title={t('editForm.fields.columns.changeVisibility')}
                placement="left-start"
              >
                <Icon size="small" color="inherit">
                  {visible ? iconNames.eye : iconNames.eye_off}
                </Icon>
              </Tooltip>
            </IconButton>
          </div>
          <div className={classes.columnsEntrySorting}>
            {isSortable ? (
              <IconButton
                onClick={() => cycleNext()}
                disableRipple={true}
                size="small"
              >
                <Tooltip
                  title={t('editForm.fields.columns.changeSorting')}
                  placement="left-start"
                >
                  <Icon size="small" color="inherit">
                    {getIcon()}
                  </Icon>
                </Tooltip>
              </IconButton>
            ) : (
              ''
            )}
          </div>
        </div>

        <IconButton
          onClick={() => onDelete(index)}
          disableRipple={true}
          size="small"
          classes={{
            root: classes.rightCornerDeleteButton,
          }}
        >
          <Icon size="extraSmall" color="inherit">
            {iconNames.delete}
          </Icon>
        </IconButton>
      </div>
    </>
  );
};

export default ColumnEntry;
