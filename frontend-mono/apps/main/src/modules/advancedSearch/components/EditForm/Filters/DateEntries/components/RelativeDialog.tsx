// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, {
  FunctionComponent,
  useState,
  useEffect,
  useCallback,
} from 'react';
import * as i18next from 'i18next';
import { parse } from 'tinyduration';
import formatISODuration from 'date-fns/formatISODuration';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import Button from '@mintlab/ui/App/Material/Button';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import {
  Dialog as UIDialog,
  DialogTitle,
} from '@mintlab/ui/App/Material/Dialog';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import TextField from '@mintlab/ui/App/Material/TextField';
import Typography from '@mui/material/Typography';
import { useStyles } from '../DateEntries.style';
import {
  getOperatorChoices,
  getPresets,
  getIntervals,
  getPeriods,
  isPresetValue,
  TRANSLATION_BASE,
} from '../DateEntries.library';
import { IntervalType, PeriodType } from '../DateEntries.types';
import {
  DateEntryTypeRelative,
  DateModeType,
  OperatorType,
} from '../../../../../AdvancedSearch.types';

type RelativeDialogPropsType = {
  onSubmit: (entry: DateEntryTypeRelative) => void;
  onClose: () => void;
  entry?: DateEntryTypeRelative | null;
  open: boolean;
  t: i18next.TFunction;
  mode: DateModeType;
};

const RelativeDialog: FunctionComponent<RelativeDialogPropsType> = ({
  onSubmit,
  entry,
  open,
  onClose,
  t,
  mode,
}) => {
  const [operator, setOperator] = useState<OperatorType | null>(null);
  const [dialogRef, setDialogRef] = useState<HTMLInputElement | null>(null);
  const [relativeNr, setRelativeNr] = useState<string | null>(null);
  const [relativeInterval, setRelativeInterval] = useState<IntervalType | null>(
    null
  );
  const [relativePeriod, setRelativePeriod] = useState<PeriodType | null>(null);
  const [selectedPreset, setSelectedPreset] = useState<string | null>(null);
  const classes = useStyles();

  useEffect(() => {
    const resetFields = () => {
      setRelativeNr(null);
      setRelativeInterval(null);
      setRelativePeriod(null);
      setSelectedPreset(null);
    };
    if (entry?.value) {
      if (entry?.operator) setOperator(entry.operator);
      if (isPresetValue(entry?.value, t, mode)) {
        resetFields();
        setSelectedPreset(entry?.value);
      } else {
        let found;
        const parsed = parse(entry.value);
        const parts = Object.entries(cloneWithout(parsed, 'negative'));
        found = parts.find(part => part[1] !== 0);
        if (found) {
          setRelativeInterval(found[0] as IntervalType);
          setRelativeNr(found[1] as string);
          setRelativePeriod(parsed.negative ? '-' : '+');
        }
      }
    } else {
      resetFields();
      setOperator(null);
    }
  }, [open]);

  const isValid = () => {
    if (!operator) return false;
    if (
      !selectedPreset &&
      (!relativeNr || !relativeInterval || !relativePeriod)
    )
      return false;

    return true;
  };

  const measuredRef = useCallback((node: any) => {
    if (node !== null) setDialogRef(node);
  }, []);

  const presets = getPresets(t, mode);
  const intervals = getIntervals(t, mode);
  const periods = getPeriods(t);

  const toggleSelectedPreset = (value: string) => {
    if (selectedPreset === value) {
      setSelectedPreset(null);
    } else {
      setSelectedPreset(value);
    }
  };

  return (
    <React.Fragment>
      <UIDialog onClose={onClose} open={open} ref={measuredRef}>
        <DialogTitle
          title={t(`${TRANSLATION_BASE}titles.relative`)}
          icon="today"
        />
        <Divider />
        <DialogContent>
          <div className={classes.dialogWrapper}>
            <div className={classes.operator}>
              {dialogRef ? (
                <Select
                  variant="generic"
                  key={`relative-dialog-select-operator`}
                  name={`relative-dialog-select-operator`}
                  choices={getOperatorChoices(t)}
                  value={operator || null}
                  isClearable={false}
                  onChange={(event: React.ChangeEvent<any>) =>
                    setOperator(event.target.value.value)
                  }
                  placeholder={t(`${TRANSLATION_BASE}operator.placeholder`)}
                />
              ) : null}
            </div>
            <Divider />
            <div className={classes.relativeWrapper}>
              <Typography variant="subtitle2">
                {t(`${TRANSLATION_BASE}manualInput`) as string}
              </Typography>
              <div className={classes.manual}>
                <div className={classes.nr}>
                  <TextField
                    name={`relativeNr`}
                    value={relativeNr}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                      setSelectedPreset(null);
                      setRelativeNr(event.target.value);
                    }}
                    placeholder={t(
                      `${TRANSLATION_BASE}placeholders.relativeNr`
                    )}
                  />
                </div>
                <div className={classes.type}>
                  <Select
                    variant="generic"
                    name="relativeInterval"
                    choices={intervals}
                    value={relativeInterval}
                    onChange={(event: React.ChangeEvent<any>) => {
                      setSelectedPreset(null);
                      setRelativeInterval(event.target.value.value);
                    }}
                    placeholder={t(`${TRANSLATION_BASE}placeholders.choose`)}
                  />
                </div>
                <div className={classes.in}>
                  {t(`${TRANSLATION_BASE}inLabel`) as string}
                </div>
                <div className={classes.period}>
                  <Select
                    variant="generic"
                    name="relativePeriod"
                    choices={periods}
                    value={relativePeriod}
                    onChange={(event: React.ChangeEvent<any>) => {
                      setSelectedPreset(null);
                      setRelativePeriod(event.target.value.value);
                    }}
                    placeholder={t(`${TRANSLATION_BASE}placeholders.choose`)}
                  />
                </div>
              </div>
              <Typography variant="subtitle2">
                {t(`${TRANSLATION_BASE}quickSelect`) as string}
              </Typography>
              <div className={classes.common}>
                <ul>
                  {presets.map((preset, index) => {
                    return (
                      <li key={index}>
                        <Button
                          name="togglePreset"
                          variant={
                            selectedPreset === preset.value
                              ? 'contained'
                              : 'outlined'
                          }
                          action={() => {
                            toggleSelectedPreset(preset.value);
                          }}
                        >
                          {preset.label}
                        </Button>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
            <Divider />
          </div>
        </DialogContent>
        <Divider />
        <div className={classes.actionButtons}>
          <Button
            name="closeRelativeDate"
            action={() => {
              onClose();
            }}
          >
            {t(`${TRANSLATION_BASE}cancelButton`)}
          </Button>
          <Button
            name="submitRelativeDate"
            disabled={!isValid()}
            action={() => {
              const value = selectedPreset
                ? selectedPreset
                : `${relativePeriod}${formatISODuration({
                    [relativeInterval as IntervalType]: relativeNr,
                  })}`;

              onSubmit({
                type: 'relative',
                operator,
                value,
              });
              onClose();
            }}
          >
            {t(`${TRANSLATION_BASE}okButton`)}
          </Button>
        </div>
      </UIDialog>
    </React.Fragment>
  );
};

export default RelativeDialog;
