// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { TFunction } from 'i18next';
import Text from '../../Filters/Text/Text';
import Date from '../../Filters/Date/Date';
import Address from '../../Filters/Address/Address';
import {
  CustomFieldTypeTypes,
  ValidateFuncType,
} from '../../../../AdvancedSearch.types';
import { FilterCommonPropsType } from '../Filters.types';
import {
  validateCurrency,
  validateRegExp,
} from '../../../../library/validations';
import { useStyles } from './CustomField.styles';

const TRANSLATION_BASE = 'editForm.fields.filters';

/* eslint complexity: [2, 20] */
const getConfig = ({
  type,
  identifier,
  t,
}: {
  type: CustomFieldTypeTypes;
  identifier: string;
  t: TFunction;
}) => {
  if (['text', 'richtext', 'textarea'].includes(type)) {
    return {
      Component: Text,
      componentProps: {
        placeholder: t(`${TRANSLATION_BASE}.types.text.placeholder`),
        validate: validateRegExp(
          identifier,
          t(`${TRANSLATION_BASE}.types.text.errorMessage`),
          'text'
        ),
      },
    };
  } else if (type === 'email') {
    return {
      Component: Text,
      componentProps: {
        placeholder: t(`${TRANSLATION_BASE}.types.email.placeholder`),
        validate: validateRegExp(
          identifier,
          t(`${TRANSLATION_BASE}.types.email.errorMessage`),
          'email'
        ),
      },
    };
  } else if (type === 'address_v2') {
    return {
      Component: Address,
      componentProps: {
        placeholder: 'ph',
      },
    };
  } else if (type === 'valuta') {
    return {
      Component: Text,
      componentProps: {
        placeholder: 'curcr',
        formatType: 'eurCurrency',
        validate: validateCurrency(
          identifier,
          t(`${TRANSLATION_BASE}.types.valuta.errorMessage`)
        ),
      },
    };
  } else if (type === 'numeric') {
    return {
      Component: Text,
      componentProps: {
        placeholder: t(`${TRANSLATION_BASE}.types.numeric.placeholder`),
        validate: validateRegExp(
          identifier,
          t(`${TRANSLATION_BASE}.types.numeric.errorMessage`),
          'numeric'
        ),
      },
    };
  } else if (type === 'bankaccount') {
    return {
      Component: Text,
      componentProps: {
        placeholder: t(`${TRANSLATION_BASE}.types.bankaccount.placeholder`),
        validate: validateRegExp(
          identifier,
          t(`${TRANSLATION_BASE}.types.bankaccount.errorMessage`),
          'url'
        ),
      },
    };
  } else if (type === 'url') {
    return {
      Component: Text,
      componentProps: {
        placeholder: t(`${TRANSLATION_BASE}.types.url.placeholder`),
        validate: validateRegExp(
          identifier,
          t(`${TRANSLATION_BASE}.types.url.errorMessage`),
          'url'
        ),
      },
    };
  } else if (type === 'date') {
    return {
      Component: Date,
      componentProps: {
        validate: validateRegExp(
          identifier,
          t(`${TRANSLATION_BASE}.types.date.errorMessage`),
          'dateDay'
        ),
      },
    };
  }
};

const CustomFieldFilterComponent: FunctionComponent<
  CustomFieldPropsType
> = props => {
  //@ts-ignore
  const { identifier, t } = props;
  const config = getConfig({
    type: props.type,
    identifier,
    t,
  });

  return config ? (
    <config.Component {...props} {...config.componentProps} />
  ) : null;
};

interface CustomFieldPropsType extends FilterCommonPropsType {
  type: CustomFieldTypeTypes;
  placeholder?: string;
  validate?: ValidateFuncType<any> | undefined;
  [key: string]: any;
}

const CustomField: FunctionComponent<CustomFieldPropsType> = ({
  name,
  t,
  identifier,
  type,
  ...rest
}) => {
  const classes = useStyles();
  const config = getConfig({ type, identifier, t });

  if (!config) {
    return (
      <div>
        {
          t('editForm.fields.filters.typeNotSupported', {
            type,
          }) as string
        }
      </div>
    );
  }

  return (
    <div className={classes.wrapper}>
      <div>
        <CustomFieldFilterComponent
          name={name}
          t={t}
          identifier={identifier}
          key={identifier}
          type={type}
          {...rest}
        />
      </div>
    </div>
  );
};

export default CustomField;
