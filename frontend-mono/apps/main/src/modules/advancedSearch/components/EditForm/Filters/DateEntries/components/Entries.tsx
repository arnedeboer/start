// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { Field, FieldArray, FormikProps, FieldInputProps } from 'formik';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import { DialogPropertiesType } from '../DateEntries.types';
import {
  DateEntryType,
  DateModeType,
  EditFormStateType,
} from '../../../../../AdvancedSearch.types';
import DateEntry from './DateEntry';

type EntriesPropsType = {
  field: FieldInputProps<any>;
  classes: any;
  t: i18next.TFunction;
  setDialogProperties: (dialog: DialogPropertiesType) => void;
  validateForm: (formikInstance: FormikProps<EditFormStateType>) => void;
  helpersRef?: HTMLInputElement | null;
  setHelpersRef: (ref: HTMLInputElement) => void;
  form: FormikProps<EditFormStateType>;
  mode: DateModeType;
};

/* eslint complexity: [2, 12] */
const Entries: FunctionComponent<EntriesPropsType> = ({
  classes,
  t,
  setDialogProperties,
  validateForm,
  helpersRef,
  setHelpersRef,
  field,
  form,
  mode,
}) => {
  const entries: DateEntryType[] = get(form.values, field.name);
  const { name } = field;

  return (
    <FieldArray
      name={name}
      key={name}
      render={arrayHelpersEntries => {
        //@ts-ignore
        if (!helpersRef.current)
          //@ts-ignore
          setHelpersRef(arrayHelpersEntries);
        return (
          <div>
            {entries && entries.length > 0
              ? entries.map((entry, entryIndex) => {
                  return (
                    <div
                      className={classes.entryWrapper}
                      key={`entry-${entryIndex}`}
                    >
                      <div className={classes.entryContent}>
                        <div className={classes.entryDateTimeDuration}>
                          <Field
                            name={`${name}.[${entryIndex}]`}
                            key={`${name}.[${entryIndex}]`}
                            component={DateEntry}
                            t={t}
                            classes={classes}
                            setDialogProperties={setDialogProperties}
                            index={entryIndex}
                            mode={mode}
                          />
                        </div>
                      </div>
                      <div className={classes.entryClose}>
                        <IconButton
                          onClick={() => {
                            arrayHelpersEntries.remove(entryIndex);
                            validateForm(form);
                          }}
                          disableRipple={true}
                          size="small"
                        >
                          <Icon size="small" color="inherit">
                            {iconNames.delete}
                          </Icon>
                        </IconButton>
                      </div>
                    </div>
                  );
                })
              : null}
          </div>
        );
      }}
    />
  );
};

export default Entries;
