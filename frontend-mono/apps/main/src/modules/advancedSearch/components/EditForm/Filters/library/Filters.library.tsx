// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import {
  FilterNamesType,
  KindType,
  FilterType,
} from '../../../../AdvancedSearch.types';
import HelpListing from '../../components/HelpListing';

// to Select choices
type OptionType = {
  label: string;
  value: FilterNamesType;
};

const TRANSLATION_BASE = 'editForm.fields.filters.';

export const hasNoValue = (value: any) =>
  typeof value === 'undefined' ||
  value === null ||
  JSON.stringify(value) === '{}' ||
  value === '';

export const getOperatorOptions = (t: i18next.TFunction, options: string[]) =>
  options.map(option => ({
    label: t(`${TRANSLATION_BASE}operators.${option}`),
    value: option,
  }));

export const getHelp = ({
  filter,
  kind,
  classes,
  t,
}: {
  filter: FilterType;
  kind: KindType;
  classes: any;
  t: i18next.TFunction;
}) => {
  const { type } = filter;

  const getContents = () => {
    if (type === 'keyword') {
      return kind === 'case' ? (
        <HelpListing
          t={t}
          classes={classes}
          intro={t(`${TRANSLATION_BASE}help.keywordCase.intro`)}
          list={[
            t(`${TRANSLATION_BASE}help.keywordCase.requestor`),
            t(`${TRANSLATION_BASE}help.keywordCase.recipient`),
            t(`${TRANSLATION_BASE}help.keywordCase.assignee`),
            t(`${TRANSLATION_BASE}help.keywordCase.other`),
          ]}
        />
      ) : (
        <HelpListing
          t={t}
          classes={classes}
          intro={t(`${TRANSLATION_BASE}help.keywordCustomObject.intro`)}
          list={[t(`${TRANSLATION_BASE}help.keywordCustomObject.fields`)]}
        />
      );
    } else if (type === 'attributes.urgency') {
      return (
        <HelpListing
          t={t}
          classes={classes}
          intro={t(`${TRANSLATION_BASE}help.urgency.intro`)}
          list={[
            t(`${TRANSLATION_BASE}help.urgency.normal`),
            t(`${TRANSLATION_BASE}help.urgency.medium`),
            t(`${TRANSLATION_BASE}help.urgency.high`),
            t(`${TRANSLATION_BASE}help.urgency.late`),
          ]}
        />
      );
    }
    return null;
  };

  const helpContents = getContents();
  return helpContents ? (
    <div className={classes.filterOperatorHelp}>
      <Tooltip enterDelay={200} title={helpContents}>
        <Icon size="extraSmall" color="inherit">
          {iconNames.help}
        </Icon>
      </Tooltip>
    </div>
  ) : null;
};

// Determines which filters are allowed to be added
export const getFiltersChoices = ({
  t,
  kind,
}: {
  t: i18next.TFunction;
  kind: KindType;
}): OptionType[] => {
  let options: string[][];
  if (kind === 'custom_object') {
    options = [
      [t(`${TRANSLATION_BASE}fields.keyword.label`), 'keyword'],
      [
        t(`${TRANSLATION_BASE}fields.modified.label`),
        'attributes.last_modified',
      ],
      [t(`${TRANSLATION_BASE}fields.status.label`), 'attributes.status'],
      [
        t(`${TRANSLATION_BASE}fields.archiveStatus.label`),
        'attributes.archive_status',
      ],
    ];
  } else if (kind === 'case') {
    options = [
      [t(`${TRANSLATION_BASE}fields.keyword.label`), 'keyword'],
      [
        t(`${TRANSLATION_BASE}fields.requestor.label`),
        'relationship.requestor.id',
      ],
      [
        t(`${TRANSLATION_BASE}fields.assignee.label`),
        'relationship.assignee.id',
      ],
      [
        t(`${TRANSLATION_BASE}fields.coordinator.label`),
        'relationship.coordinator.id',
      ],
      [t(`${TRANSLATION_BASE}fields.urgency.label`), 'attributes.urgency'],
      [
        t(`${TRANSLATION_BASE}fields.archivalState.label`),
        'attributes.archival_state',
      ],
      [
        t(`${TRANSLATION_BASE}fields.caseType.label`),
        'relationship.case_type.id',
      ],
      [
        t(`${TRANSLATION_BASE}fields.paymentStatus.label`),
        'attributes.payment_status',
      ],
      [
        t(`${TRANSLATION_BASE}fields.departmentRole.label`),
        'attributes.department_role',
      ],
      [
        t(`${TRANSLATION_BASE}fields.caseLocation.label`),
        'attributes.case_location',
      ],
      [
        t(`${TRANSLATION_BASE}fields.contactChannel.label`),
        'attributes.channel_of_contact',
      ],
      [t(`${TRANSLATION_BASE}fields.result.label`), 'attributes.result'],
      [
        t(`${TRANSLATION_BASE}fields.registrationDate.label`),
        'attributes.registration_date',
      ],
      [
        t(`${TRANSLATION_BASE}fields.completionDate.label`),
        'attributes.completion_date',
      ],
      [
        t(`${TRANSLATION_BASE}fields.confidentiality.label`),
        'attributes.confidentiality',
      ],
    ];
  } else {
    options = [];
  }

  const defaultOptions = options
    .sort((optionA: string[], optionB: string[]) =>
      optionA[0].localeCompare(optionB[0])
    )
    .map(entry => ({
      label: entry[0],
      value: entry[1] as any,
    }));

  return [...defaultOptions];
};

/* eslint complexity: [2, 10] */
export const getEntryPoint = (filter: FilterType, baseName: string) => {
  const { type } = filter;

  switch (type) {
    case 'attributes.value': {
      return `${baseName}.parameters.value.value`;
    }
    case 'relationship.assignee.id':
    case 'relationship.requestor.id':
    case 'relationship.coordinator.id':
    case 'keyword':
    case 'relationship.case_type.id':
    case 'attributes.payment_status':
      return `${baseName}.parameters`;
    default:
      return `${baseName}.parameters.value`;
  }
};

export const getChoicesFromTranslation = (
  t: i18next.TFunction,
  fieldName: string
) => {
  const objects = t(`${TRANSLATION_BASE}fields.${fieldName}.choices`, {
    returnObjects: true,
  });

  return Object.entries(objects).map(paymentStatus => ({
    label: paymentStatus[1] as string,
    value: paymentStatus[0] as string,
  }));
};
