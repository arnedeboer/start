// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { Field } from 'formik';
import { BaseSelectPropsType } from '@mintlab/ui/App/Zaaksysteem/Select/types/BaseSelectPropsType';
import UISelect from '@mintlab/ui/App/Zaaksysteem/Select';
import { ValidateFuncType } from '../../../../AdvancedSearch.types';
import { FilterCommonPropsType } from '../Filters.types';
import { useStyles } from './Select.styles';
interface FilterSelectPropsTypeBase extends FilterCommonPropsType {
  validate?: ValidateFuncType<any> | undefined;
  nestedValue?: boolean;
}

type FilterSelectPropsType = FilterSelectPropsTypeBase &
  BaseSelectPropsType<any>;

const PassThrough = (props: any) => {
  const { field } = props;
  return <UISelect {...props} {...field} />;
};

const Select: FunctionComponent<FilterSelectPropsType> = ({
  name,
  validate = null,
  choices,
  placeholder,
  nestedValue = true,
  freeSolo = true,
  isMulti = false,
  renderTags,
  isClearable = false,
}) => {
  const classes = useStyles();
  return (
    <div className={classes.wrapper}>
      <Field
        name={name}
        validate={validate}
        component={PassThrough}
        choices={choices}
        nestedValue={nestedValue}
        placeholder={placeholder}
        freeSolo={freeSolo}
        isMulti={isMulti}
        renderTags={renderTags}
        isClearable={isClearable}
      />
    </div>
  );
};

export default Select;
