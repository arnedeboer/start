// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, FunctionComponent } from 'react';
import { Field, FieldInputProps, FormikProps } from 'formik';
import fecha from 'fecha';
import { DialogDatePicker } from '@mintlab/ui/App/Material/DatePicker';
import {
  EditFormStateType,
  ValidateFuncType,
} from '../../../../AdvancedSearch.types';

type DatePropsType = {
  name: string;
  validate?: ValidateFuncType<any> | undefined;
};

type DatePassThroughPropsType = {
  field: FieldInputProps<any>;
  form: FormikProps<EditFormStateType>;
  key: string;
};

const DatePassThrough: FunctionComponent<DatePassThroughPropsType> = ({
  field,
  form,
  key,
}) => {
  const { setFieldValue } = form;

  useEffect(() => {
    if (!field.value)
      setFieldValue(field.name, fecha.format(new Date(), 'YYYY-MM-DD'));
  }, []);

  return (
    <DialogDatePicker
      //@ts-ignore
      {...field}
      value={field.value}
      key={key}
    />
  );
};

const DateCmp: FunctionComponent<DatePropsType> = ({
  name,
  validate,
  ...rest
}) => (
  <Field
    name={name}
    component={DatePassThrough}
    {...(Boolean(validate) && { validate })}
    {...rest}
  />
);

export default DateCmp;
