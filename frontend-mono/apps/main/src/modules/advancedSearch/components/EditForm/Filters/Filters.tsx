// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import * as i18next from 'i18next';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FieldArray, FormikProps, FieldArrayRenderProps, Field } from 'formik';
import classNames from 'classnames';
import { FiltersSelect } from '../FiltersSelect/FiltersSelect';
import {
  FilterType,
  KindType,
  ClassesType,
  EditFormStateType,
  CustomFieldType,
} from '../../../AdvancedSearch.types';
import FilterTypeComponent from './FilterType';
import { getOperatorOptions } from './library/Filters.library';

type FiltersPropsType = {
  kind: KindType;
  formik: FormikProps<EditFormStateType>;
  classes: ClassesType;
  show: boolean;
  t: i18next.TFunction;
  customFields: CustomFieldType[];
  [key: string]: any;
};

// This is the main FieldArray
/* eslint complexity: [2, 12] */
const Filters = ({
  kind,
  formik,
  classes,
  show = true,
  t,
  openServerErrorDialog,
  customFields,
}: FiltersPropsType) => {
  const { errors, values } = formik;
  const [arrayHelpersRef, setArrayHelpersRef] =
    useState<FieldArrayRenderProps | null>(null);
  const filters = values.filters?.filters;
  const hasFilters = Boolean(filters && filters.length > 0);
  const objectTypeSelected = Boolean(values.selectedObjectType);
  const showFilters =
    (kind === 'custom_object' && objectTypeSelected) || kind === 'case';
  const showFiltersSelect =
    (kind === 'custom_object' && Boolean(values.selectedObjectType)) ||
    kind === 'case';
  const name = 'filters.filters';

  return (
    <div
      className={classNames(classes.filtersWrapper, {
        [classes.show]: show,
        [classes.hide]: !show,
      })}
    >
      {showFiltersSelect && (
        <FiltersSelect
          classes={classes}
          arrayHelpersRef={arrayHelpersRef}
          t={t}
          kind={kind}
          formik={formik}
          openServerErrorDialog={openServerErrorDialog}
          customFields={customFields}
        />
      )}
      {showFilters && (
        <div className={classes.editFormSection}>
          <span
            className={classNames(classes.filterRowLabel, classes.selfAlignTop)}
          >
            {t('editForm.fields.filters.label') + ':'}
          </span>
          <div className={classes.filterRowContent}>
            <FieldArray
              name={name}
              render={arrayHelpers => {
                if (!arrayHelpersRef) setArrayHelpersRef(arrayHelpers);
                return (
                  <div>
                    {hasFilters && (
                      <div className={classes.filtersOperator}>
                        <Field
                          name={`filters.operator`}
                          t={t}
                          component={(props: any) => {
                            const { choices, value, field } = props;
                            return (
                              <Select
                                {...field}
                                choices={choices}
                                value={value}
                                nestedValue={true}
                                isClearable={false}
                              />
                            );
                          }}
                          value={values.filters?.operator}
                          choices={getOperatorOptions(t, ['and', 'or'])}
                        />
                      </div>
                    )}
                    <div>
                      {values?.filters?.filters?.map(
                        (filter: FilterType, index: number) => (
                          <FilterTypeComponent
                            key={`filter-${index}`}
                            name={name}
                            filter={filter}
                            index={index}
                            arrayHelpers={arrayHelpers}
                            values={values}
                            formik={formik}
                            errors={errors}
                            classes={classes}
                            t={t}
                            kind={kind}
                          />
                        )
                      )}

                      {!hasFilters && (
                        <div>
                          {
                            t(
                              'editForm.fields.filters.noFiltersLabel'
                            ) as string
                          }
                        </div>
                      )}
                    </div>
                  </div>
                );
              }}
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default Filters;
