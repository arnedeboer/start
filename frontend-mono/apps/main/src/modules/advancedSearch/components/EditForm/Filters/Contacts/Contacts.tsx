// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useState } from 'react';
import { Field, FieldProps } from 'formik';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import Typography from '@mui/material/Typography';
//@ts-ignore
import Select, {
  renderOptionWithIcon,
  renderTagsWithIcon,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { FilterCommonPropsType } from '../Filters.types';
import {
  ContactType,
  ValidateFuncType,
} from '../../../../AdvancedSearch.types';
import { getTypeChoices, useContactsChoicesQuery } from './Contacts.library';
import { useStyles } from './Contacts.styles';
import { validateFunc } from './Contacts.library';

interface FilterSelectPropsType extends FilterCommonPropsType {
  validate?: ValidateFuncType<any> | undefined;
  choices?: any[];
  showContactTypes: boolean;
  showActiveChoice?: boolean;
  translations: { [key: string]: string };
}

type ContactsCmpPropsType = {
  key: string;
  field: FieldProps['field'];
} & Pick<
  FilterSelectPropsType,
  'translations' | 't' | 'showContactTypes' | 'showActiveChoice'
>;

export type SearchTypeType = ContactType | 'all';

const Contacts: FunctionComponent<FilterSelectPropsType> = ({
  name,
  validate = null,
  t,
  identifier,
  translations,
  showActiveChoice = false,
  ...rest
}) => (
  <div style={{ width: '100%' }}>
    <Field
      name={name}
      key={identifier}
      validate={validateFunc(identifier, translations)}
      component={ContactsCmp}
      translations={translations}
      t={t}
      showActiveChoice={showActiveChoice}
      {...rest}
    />
  </div>
);

const ContactsCmp: FunctionComponent<ContactsCmpPropsType> = ({
  showContactTypes,
  translations,
  field,
  t,
  showActiveChoice,
}) => {
  const classes = useStyles();
  const typeChoices = getTypeChoices(t);
  const [searchType, setSearchType] = useState<SearchTypeType>(
    typeChoices[0].value
  );
  const [inactiveToggle, setInActiveToggle] = useState<boolean>(false);

  const [selectProps] = useContactsChoicesQuery({
    searchType: showContactTypes ? searchType : 'employee',
    field,
    inactiveToggle,
  });

  const getIcon = (option: any) =>
    option.type === 'organization' ? iconNames.domain : iconNames.person;

  return (
    <div className={classes.wrapper}>
      {showContactTypes && (
        <div className={classes.typesWrapper}>
          <div className={classes.label}>
            <Typography variant="subtitle1">
              {t('searchIn') as string}
            </Typography>
          </div>
          <div className={classes.typeSelect}>
            <Select
              name="searchType"
              variant="generic"
              choices={typeChoices}
              onChange={(event: any) => setSearchType(event.target.value)}
              value={searchType}
              nestedValue={true}
            />
          </div>
        </div>
      )}
      {showActiveChoice && (
        <div className={classes.typesWrapper}>
          <Checkbox
            label={t(
              'editForm.fields.filters.fields.coordinator.activeToggleLabel'
            )}
            name={`${field.name}.inactiveToggle`}
            onChange={() => setInActiveToggle(!inactiveToggle)}
            checked={inactiveToggle}
          />
        </div>
      )}
      <div>
        <Select
          {...field}
          {...selectProps}
          renderOption={renderOptionWithIcon(getIcon)}
          renderTags={renderTagsWithIcon(getIcon)}
          placeholder={translations['form:choose']}
        />
      </div>
    </div>
  );
};

export default Contacts;
