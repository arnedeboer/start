// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FieldProps } from 'formik';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useQuery } from '@tanstack/react-query';
import { useDebouncedCallback } from 'use-debounce';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { PDOK_LOOKUP_URL, PDOK_SUGGEST_URL } from '../../../../library/config';
import { AddressType } from './Address.types';

const ADDRESSFINDER_QUERYKEY = 'advancedSearch-addressFinder';
const DELAY = 400;

export const useAddressChoicesQuery = ({
  field,
  type,
}: {
  field: FieldProps['field'];
  type: AddressType;
}) => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input && input?.length > 2);

  const data = useQuery(
    [ADDRESSFINDER_QUERYKEY, input, type],
    async ({ queryKey: [___, keyword] }) => {
      const result = await request(
        'GET',
        buildUrl(PDOK_SUGGEST_URL, {
          // eslint-disable-next-line id-length
          q: keyword,
          fq: type === 'nummeraanduiding' ? 'type:adres' : 'type:weg',
        })
      ).catch(openServerErrorDialog);

      if (
        !result ||
        !result.response?.docs ||
        result.response.docs.length === 0
      )
        return [];

      return result.response.docs.map((result: any) => ({
        label: result?.weergavenaam || '',
        value: result?.id,
        type,
      }));
    },
    { enabled }
  );

  const [setInputDebounced] = useDebouncedCallback(
    async (val: any) => setInput(val),
    DELAY
  );

  /* eslint complexity: [2, 8] */
  const selectProps = {
    onInputChange: (ev: any, val: any) => setInputDebounced(val),
    onChange: async (event: React.ChangeEvent<any>) => {
      const {
        target: { value },
      } = event;
      let newValue = [];

      for (const val of value) {
        if (!val.id) {
          const result = await request(
            'GET',
            buildUrl(PDOK_LOOKUP_URL, {
              id: val.value,
            })
          ).catch(openServerErrorDialog);
          const docs = result?.response?.docs;
          if (isPopulatedArray(docs)) {
            newValue.push({
              ...val,
              id:
                val.type === 'nummeraanduiding'
                  ? `nummeraanduiding-${docs[0]?.nummeraanduiding_id || ''}`
                  : `openbareruimte-${docs[0]?.openbareruimte_id || ''}`,
            });
          }
        } else {
          newValue.push(val);
        }
      }

      field.onChange({
        ...event,
        target: {
          ...event.target,
          value: newValue,
        },
      });
    },
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
    isMulti: true,
    freeSolo: true,
  };

  return [selectProps, ServerErrorDialog] as const;
};
