// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useState } from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
//@ts-ignore
import set from 'lodash.set';
import { v4 } from 'uuid';
import Button from '@mintlab/ui/App/Material/Button';
import { Field } from 'formik';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { getFiltersChoices } from '../Filters/library/Filters.library';
import { FilterType } from '../../../AdvancedSearch.types';
import AttributeFinder from '../components/AttributeFinder/AttributeFinder';
import { getFieldOperatorsConfig } from '../../../library/config';
import { validateForm } from '../EditForm.library';
import {
  MethodType,
  FiltersSelectPropsType,
  SelectSystemAttributesPropsType,
} from './FiltersSelect.types';
import { getDefaultParameters } from './FiltersSelect.library';

export const FiltersSelect: FunctionComponent<FiltersSelectPropsType> = ({
  formik,
  t,
  classes,
  arrayHelpersRef,
  kind,
  openServerErrorDialog,
  customFields,
}) => {
  const [method, setMethod] = useState<MethodType>('systemAttributes');
  if (!arrayHelpersRef) return null;

  return (
    <div className={classes.editFormSection}>
      <div className={classes.filtersSelectCaseWrapper}>
        <div className={classes.filtersSelectCaseMethod}>
          {['systemAttributes', 'searchAttributes'].map(methodType => (
            <Button
              name="setMethod"
              key={methodType}
              action={() => setMethod(methodType as MethodType)}
              variant={methodType === method ? 'contained' : 'outlined'}
            >
              {t(`editForm.methods.${methodType}`)}
            </Button>
          ))}
        </div>
        <div className={classes.filtersSelectCaseSelectors}>
          <span className={classes.filterRowLabel}>
            {t('editForm.fields.filtersSelect.label') as string}
          </span>
          <div className={classes.filterRowContent}>
            {method === 'systemAttributes' && (
              <Field
                name="selectedFilter"
                component={SelectSystemAttributes}
                choices={getFiltersChoices({ t, kind })}
                arrayHelpers={arrayHelpersRef}
                t={t}
                kind={kind}
              />
            )}
            {method === 'searchAttributes' && (
              <AttributeFinder
                selectKey={`filters-select-case-attribute-finder`}
                name={`filters-select-case-attribute-finder`}
                handleSelectOnChange={(event: React.ChangeEvent<any>) => {
                  const {
                    data: { magicString, type, name },
                  } = event.target.value;

                  const filterObj: FilterType = {
                    type: 'attributes.value',
                    uuid: v4(),
                    parameters: {
                      label: name,
                      value: {
                        magicString,
                        type,
                        value: getDefaultParameters(type),
                      },
                    },
                  };

                  const config = getFieldOperatorsConfig({ filter: filterObj });
                  if (
                    config &&
                    config.location &&
                    isPopulatedArray(config.operators)
                  ) {
                    set(filterObj, config.location, config.operators[0]);
                  }

                  arrayHelpersRef.insert(0, filterObj);
                  validateForm(formik);
                }}
                t={t}
                kind={kind}
                customFields={customFields}
                openServerErrorDialog={openServerErrorDialog}
                filterOption={(option: any) => {
                  const {
                    values: { filters },
                  } = formik;

                  if (!isPopulatedArray(filters?.filters)) return true;
                  return !filters?.filters.some(
                    filter =>
                      filter.type === 'attributes.value' &&
                      filter.parameters.value.magicString ===
                        option.data.magicString
                  );
                }}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

const SelectSystemAttributes: FunctionComponent<
  SelectSystemAttributesPropsType
> = ({ arrayHelpers, name, choices, value, form, field, t }) => {
  const { values } = form;
  const { onChange } = field;

  const filterOptionFunc = (option: any, input?: string) => {
    if (!values.filters) return true;
    const existing = values.filters.filters.reduce((acc, current) => {
      acc[acc.length] = current.type;
      return acc;
    }, [] as string[]);

    const textMatches = Boolean(
      option.label.toLowerCase().indexOf(input?.toLowerCase()) > -1
    );
    const existingMatches = !existing.includes(option.value);

    return input ? textMatches && existingMatches : existingMatches;
  };

  return (
    <div style={{ width: '100%' }}>
      <Select
        variant="generic"
        name={name}
        choices={choices}
        value={value}
        isClearable={true}
        onChange={(event: React.ChangeEvent<any>) => {
          onChange(event);
          const targetValue = event.target?.value?.value;
          if (!targetValue) return;
          const defaultParameters = getDefaultParameters(targetValue);

          let newValues = {
            type: targetValue,
            uuid: v4(),
            parameters: defaultParameters,
          };

          const config = getFieldOperatorsConfig({ filter: newValues as any });
          if (config && config.location && isPopulatedArray(config.operators)) {
            set(newValues, config.location, config.operators[0]);
          }

          arrayHelpers.insert(0, newValues);

          validateForm(form);
        }}
        filterOption={filterOptionFunc}
        placeholder={t('editForm.fields.filters.addFilters.systemAttributes')}
        freeSolo={true}
      />
    </div>
  );
};

export default FiltersSelect;
