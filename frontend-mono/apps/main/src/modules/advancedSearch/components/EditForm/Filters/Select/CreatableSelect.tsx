// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { Field } from 'formik';
import { CreatableSelect } from '@mintlab/ui/App/Zaaksysteem/Select';
import { FilterCommonPropsType } from '../Filters.types';
import { ValidateFuncType } from '../../../../AdvancedSearch.types';
import { useStyles } from './Select.styles';

interface FilterSelectPropsType extends FilterCommonPropsType {
  validate?: ValidateFuncType<any> | undefined;
  choices?: any[];
  placeholder?: string;
}

const PassThrough = (props: any) => {
  const { field } = props;
  return <CreatableSelect {...props} {...field} />;
};

const CreatableSelectCmp: FunctionComponent<FilterSelectPropsType> = ({
  name,
  validate = null,
  t,
  choices,
  ...rest
}) => {
  const classes = useStyles();
  return (
    <div className={classes.wrapper}>
      <Field
        name={name}
        validate={validate}
        component={PassThrough}
        choices={choices}
        {...rest}
      />
    </div>
  );
};

export default CreatableSelectCmp;
