// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint complexity: [2, 16] */
export const getDefaultParameters = (type: string) => {
  switch (type) {
    case 'attributes.last_modified':
    case 'attributes.registration_date':
    case 'attributes.completion_date':
      return {
        label: '',
        value: null,
      };
    case 'relationship.assignee.id':
    case 'relationship.requestor.id':
    case 'relationship.coordinator.id':
    case 'attributes.payment_status':
    case 'keyword':
      return [];
    case 'attributes.channel_of_contact':
    case 'attributes.department_role':
    case 'attributes.result':
    case 'attributes.case_location':
    case 'attributes.confidentiality':
      return {
        value: [],
      };
    case 'attributes.urgency':
      return {
        value: 'normal',
        label: 'urgency',
      };
    default:
      return '';
  }
};
