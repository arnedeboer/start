// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import Chip from '@mui/material/Chip';

export const renderTags =
  ({ editing, classes }: any) =>
  (value: any, getTagProps?: any) => {
    const editingProps = editing
      ? {}
      : {
          onDelete: null,
          deleteIcon: null,
          clickable: false,
        };

    return value.map((option: any, index: number) => (
      // eslint-disable-next-line react/jsx-key
      <Chip
        sx={{
          '& .MuiSvgIcon-root': { fontSize: '14px' },
        }}
        classes={{
          root: classes.chipRoot,
          label: classes.chipLabel,
        }}
        variant="outlined"
        label={option?.label || option}
        {...(getTagProps && getTagProps({ index }))}
        {...editingProps}
      />
    ));
  };
