// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  useState,
  Dispatch,
  SetStateAction,
} from 'react';
import { QueryClient } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import * as i18next from 'i18next';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import DropdownMenu, {
  DropdownMenuList,
  //@ts-ignore
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import Button from '@mintlab/ui/App/Material/Button';
import { useNavigate } from 'react-router-dom';
import {
  SavedSearchType,
  ClassesType,
  SnackType,
  IdentifierType,
} from '../../AdvancedSearch.types';
import { useSingleDeleteMutation } from '../../query/useSingle';
import { invalidateAllQueries } from '../../query/library';
import { hasAccess } from '../../library/library';
import Labels from './Labels/Labels';
import InfoDialog from './InfoDialog/InfoDialog';

type RowPropsType = {
  data: SavedSearchType;
  classes: ClassesType;
  client: QueryClient;
  setSnack: Dispatch<SetStateAction<SnackType | null>>;
  t: i18next.TFunction;
  identifier?: IdentifierType;
  openServerErrorDialog: OpenServerErrorDialogType;
};

const Row: FunctionComponent<RowPropsType> = ({
  data,
  classes,
  client,
  setSnack,
  t,
  identifier,
  openServerErrorDialog,
}) => {
  const [editingLabels, setEditingLabels] = useState<boolean>(false);
  const [infoDialogOpen, setInfoDialogOpen] = useState<boolean>(false);
  const { uuid, authorizations } = data;
  const active = identifier === uuid;
  const deleteMutation = useSingleDeleteMutation();
  const navigate = useNavigate();
  const [deleteUUID, setDeleteUUID] = useState<IdentifierType | null>(null);
  const readWriteAccess = hasAccess(authorizations, 'readwrite');

  const startDelete = () => {
    deleteMutation.mutate(uuid, {
      onSuccess: () => {
        navigate('');
        invalidateAllQueries(client);
        setSnack({
          message: t('snacks.deleted'),
          open: true,
        });
      },
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    });
  };

  let actions = [
    {
      action: () => setInfoDialogOpen(true),
      title: t('actions.moreInfo'),
    },
  ];
  if (readWriteAccess) {
    actions = [
      {
        action: () => navigate(`edit/${uuid}`),
        title: t('actions.edit'),
      },
      {
        action: () => setDeleteUUID(uuid),
        title: t('actions.delete'),
      },
      ...actions,
    ];
  }

  return (
    <>
      <ConfirmDialog
        open={Boolean(deleteUUID)}
        onConfirm={() => startDelete()}
        onClose={() => setDeleteUUID(null)}
        title={t('confirm')}
        body={
          <div>
            {
              t('deleteConfirm', {
                name: data.name,
              }) as string
            }
          </div>
        }
      />
      <InfoDialog
        open={infoDialogOpen}
        onClose={() => setInfoDialogOpen(false)}
        savedSearch={data}
        classes={classes}
        t={t}
      />
      <div className={classes.leftBarRowWrapper}>
        <div
          className={classNames(classes.leftBarTop, {
            [classes.savedSearchLinkActive]: active,
            [classes.savedSearchLinkInactive]: !active,
          })}
        >
          <Link
            className={classes.savedSearchLink}
            key={data.uuid}
            to={`view/${data.uuid}`}
          >
            {data.name}
          </Link>
          <>
            {readWriteAccess && (
              <Tooltip
                title={t('labels.editLabels')}
                enterDelay={400}
                style={{ width: 'auto' }}
              >
                <IconButton
                  onClick={() => setEditingLabels(!editingLabels)}
                  disableRipple={true}
                  size="small"
                >
                  <Icon size="extraSmall" color="inherit">
                    {iconNames.bookmarks}
                  </Icon>
                </IconButton>
              </Tooltip>
            )}

            <Tooltip
              title={t('labels.more')}
              enterDelay={400}
              style={{ width: 'auto' }}
            >
              <DropdownMenu
                transformOrigin={{
                  horizontal: 'center',
                }}
                trigger={
                  <Button
                    name="savedSearchMore"
                    icon="more_vert"
                    scope={`catalog-header:button-bar:advanced`}
                    iconSize="small"
                    sx={{ padding: '4px' }}
                  />
                }
              >
                <DropdownMenuList
                  items={actions.map(({ action, title }) => ({
                    action,
                    label: title,
                    scope: `catalog-header:button-bar:${title}`,
                  }))}
                />
              </DropdownMenu>
            </Tooltip>
          </>
        </div>
        <Labels
          editing={editingLabels}
          data={data}
          classes={classes}
          setEditingLabels={setEditingLabels}
          openServerErrorDialog={openServerErrorDialog}
          t={t}
          readWriteAccess={readWriteAccess}
        />
      </div>
    </>
  );
};

export default Row;
