// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, Dispatch, SetStateAction } from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { useQueryClient } from '@tanstack/react-query';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { CreatableSelect, ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { Popper, PopperProps } from '@mui/material';
import {
  QUERY_KEY_LABELS,
  QUERY_KEY_SAVEDSEARCH,
  QUERY_KEY_SAVEDSEARCHES,
} from '../../../query/constants';
import { useSingleUpdateLabelsMutation } from '../../../query/useLabels';
import { AutocompleteInput } from '../../../styles/leftBar';
import { renderTags } from '../LeftBar.library';
import { LabelsType, SavedSearchType } from '../../../AdvancedSearch.types';
import { useLabelsQuery } from '../../../query/useLabels';

type LabelsPropsType = {
  editing: boolean;
  data: SavedSearchType;
  classes: any;
  setEditingLabels: Dispatch<SetStateAction<boolean>>;
  openServerErrorDialog: OpenServerErrorDialogType;
  t: i18next.TFunction;
  readWriteAccess: boolean;
};

/* eslint complexity: [2, 10] */
const Labels: FunctionComponent<LabelsPropsType> = ({
  editing,
  data,
  classes,
  setEditingLabels,
  openServerErrorDialog,
  t,
  readWriteAccess,
}) => {
  const updateMutation = useSingleUpdateLabelsMutation();
  const queryClient = useQueryClient();
  const labels: LabelsType = data.labels;
  const hasLabels = labels?.length > 0;
  const showLabels = Boolean(editing || hasLabels);
  const allLabels = useLabelsQuery({
    openServerErrorDialog,
  });

  const handleOnChange = (event: React.ChangeEvent<any>) => {
    const values: ValueType<string>[] = event.target.value;
    const newLabels = values.map(item => item.value);

    if (newLabels.length > labels.length) {
      const diff = newLabels.filter(label => !labels.includes(label));
      if (!diff?.length) return;
    }

    updateMutation.mutate(
      {
        uuid: data.uuid,
        labels: newLabels,
      },
      {
        onSuccess: () => {
          queryClient.invalidateQueries([QUERY_KEY_SAVEDSEARCHES]);
          queryClient.invalidateQueries([QUERY_KEY_SAVEDSEARCH]);
          queryClient.invalidateQueries([QUERY_KEY_LABELS]);
        },
        onError: error => {
          openServerErrorDialog(error);
        },
      }
    );
  };

  if (!showLabels) return null;

  const PopperWithLabel = (props: PopperProps) => {
    return (
      <Popper {...props}>
        <>
          <div className={classes.popperLabel}>
            {t('labels.allLabels') as string}
          </div>
          {props.children}
        </>
      </Popper>
    );
  };

  return (
    <div className={classes.leftBarRowLabelsWrapper}>
      {Boolean(!editing && hasLabels) && (
        <div className={classes.leftBarRowLabels}>
          {readWriteAccess && (
            <Tooltip
              title={t('labels.editLabels')}
              enterDelay={400}
              classes={{
                tooltip: classes.leftBarRowLabelsOverlayTooltip,
              }}
            >
              {/* eslint-disable-next-line */}
              <a
                className={classes.leftBarRowLabelsOverlay}
                onClick={(event: React.MouseEvent<any>) => {
                  setEditingLabels(true);
                  event.preventDefault();
                }}
                href="#"
              />
            </Tooltip>
          )}
          {renderTags({ classes, editing })(
            labels.map(label => ({
              label: label,
              value: label,
            }))
          )}
        </div>
      )}
      {Boolean(editing) && (
        <div className={classes.creatableWrapper}>
          <CreatableSelect
            name={'editLabelsSelect'}
            key={'editLabelsSelect'}
            isClearable={false}
            isMulti={true}
            value={labels.map(label => ({
              label,
              value: label,
            }))}
            onChange={handleOnChange}
            renderTags={renderTags({ classes, editing })}
            placeholder={t('labels.createNew')}
            sx={{ ...AutocompleteInput }}
            PopperComponent={PopperWithLabel}
            loading={allLabels.isLoading}
            filterOption={({ value }: ValueType<string>, input?: string) => {
              if (labels.includes(value)) {
                return false;
              }

              if (input && (value === input || value.indexOf(input) === -1)) {
                return false;
              }

              return true;
            }}
            {...(allLabels?.data && {
              choices: allLabels.data.map((label: string) => ({
                label,
                value: label,
              })),
            })}
          />
          <Button
            className={classes.creatableOkBtn}
            name="closeBtn"
            action={() => setEditingLabels(false)}
            sx={{
              fontSize: 10,
            }}
          >
            {t('close')}
          </Button>
        </div>
      )}
    </div>
  );
};

export default Labels;
