// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, {
  FunctionComponent,
  Dispatch,
  SetStateAction,
  useState,
} from 'react';
import { useNavigate } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import { matchPath } from 'react-router';
import * as i18next from 'i18next';
import { useTheme } from '@mui/material';
import { QueryClient } from '@tanstack/react-query';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

import {
  KindType,
  ClassesType,
  SnackType,
  LabelsType,
} from '../../AdvancedSearch.types';
import { useSavedSearchesQuery } from '../../query/useList';
import KindChoices from './KindChoices/KindChoices';
import LabelsFilters from './Labels/LabelsFilters';
import Row from './Row';
import LeftBarLoader from './LeftBarLoader';

type LeftBarPropsType = {
  kind: KindType;
  classes: ClassesType;
  client: QueryClient;
  setSnack: Dispatch<SetStateAction<SnackType | null>>;
  t: i18next.TFunction;
  openServerErrorDialog: OpenServerErrorDialogType;
  params: any;
};

/*eslint complexity: [2, 12] */
const LeftBar: FunctionComponent<LeftBarPropsType> = ({
  kind,
  classes,
  client,
  setSnack,
  t,
  openServerErrorDialog,
  params,
}) => {
  const [labels, setLabels] = useState<LabelsType>([]);
  const theme = useTheme<Theme>();
  const navigate = useNavigate();
  const listQuery = useSavedSearchesQuery({
    kind,
    labels,
    openServerErrorDialog,
  });
  const { data, hasNextPage, fetchNextPage } = listQuery;
  const match = matchPath(
    { path: `/:prefix/:module/:kind/:mode/:identifier` },
    location.pathname
  );
  const isNew = params['*'] === 'new';

  return (
    <section className={classes.leftBar}>
      <LeftBarLoader show={listQuery.isLoading || listQuery.isFetching} />
      <KindChoices kind={kind} classes={classes} t={t} />
      <div className={classes.newSavedSearchWrapper}>
        <div>
          <Typography variant="h6">{t('savedSearches') as string}</Typography>
        </div>
        <div>
          <Tooltip enterDelay={250} title={t('newSavedSearch')}>
            <Button
              action={() => {
                navigate('new');
              }}
              name="newSavedSearch"
              icon="add_circle_outline"
              sx={{ color: theme.palette.primary.main }}
              disabled={isNew}
            />
          </Tooltip>
        </div>
      </div>

      <LabelsFilters
        classes={classes}
        labels={labels}
        setLabels={setLabels}
        t={t}
        openServerErrorDialog={openServerErrorDialog}
      />

      {(data?.pages || []).map((pageGroup: any) =>
        pageGroup.data.map((entry: any) => (
          <Row
            data={entry}
            key={entry.uuid}
            classes={classes}
            client={client}
            setSnack={setSnack}
            t={t}
            identifier={match?.params?.identifier}
            openServerErrorDialog={openServerErrorDialog}
          />
        ))
      )}
      {hasNextPage ? (
        <div className={classes.loadMore}>
          <Button
            name="loadMore"
            iconSize="small"
            action={() => fetchNextPage()}
            sx={{
              backgroundColor: theme.palette.primary.main,
              '&:hover': {
                backgroundColor: theme.palette.primary.main,
              },
              color: theme.palette.common.white,
            }}
            disabled={listQuery.isFetching}
          >
            {t('loadMore')}
          </Button>
        </div>
      ) : null}
    </section>
  );
};

export default LeftBar;
