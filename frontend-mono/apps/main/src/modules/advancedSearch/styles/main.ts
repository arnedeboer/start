// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const mainStyles = ({ typography, primary }: any) => {
  return {
    mainTopBar: {
      height: 50,
      display: 'flex',
    },
    mainTopBarLeft: {
      flexGrow: 0,
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      '&& >*': {
        marginRight: 14,
      },
    },
    mainTopBarRight: {
      flex: 1,
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    mainTopBarName: {
      ...typography.h3,
    },
    resultsTableLoader: {
      position: 'absolute' as 'absolute',
      top: '30%',
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: 500,
    },
    resultsWrapper: {
      display: 'flex',
      flexDirection: 'column' as 'column',
      height: '100%',
      flex: 1,
      overflow: 'hidden',
    },
    resultsMain: {
      flex: 1,
      display: 'flex',
      overflow: 'auto',
    },
  };
};
