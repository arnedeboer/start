// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export const AutocompleteInput = {
  '&& .MuiFilledInput-input': {
    flexBasis: '100%',
    '&&': { padding: '8px' },
  },
  '&& .MuiChip-root + .MuiFilledInput-input': {
    marginTop: '10px',
  },
};

export const leftBarStyles = ({
  primary,
  basalt,
  common,
  greyscale,
  cloud,
  typography,
}: any) => {
  const savedSearchLinkActive = {
    backgroundColor: primary.main,
    '& *': {
      color: 'white',
    },
  };
  return {
    leftBar: {
      width: '25%',
      overflowY: 'auto',
      backgroundColor: cloud.light,
      position: 'relative',
      padding: 14,
      '& $leftBarRowWrapper:last-of-type': {
        borderBottom: `1px solid ${cloud.darkest}`,
      },
    },
    leftBarRowWrapper: {
      display: 'flex',
      flexDirection: 'column',
      borderTop: `1px solid ${cloud.darkest}`,
    },
    leftBarTop: {
      display: 'flex',
      padding: '8px 12px',
      justifyContent: 'center',
      '&:hover': savedSearchLinkActive,
    },
    leftBarRowLabelsWrapper: {
      display: 'flex',
      backgroundColor: primary.lighter,
      padding: 6,
      flexDirection: 'column',
    },
    leftBarRowLabels: {
      display: 'flex',
      position: 'relative',
      gap: 3,
      flexWrap: 'wrap',
      minHeight: 36,
      flex: 1,
      alignItems: 'center',
    },
    leftBarRowLabelsOverlay: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
    },
    leftBarRowLabelsOverlayTooltip: {
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    savedSearchLinkActive,
    savedSearchLinkInactive: {
      '& *': {
        color: basalt.darkest,
      },
    },
    savedSearchLink: {
      textDecoration: 'none',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
    },
    creatableWrapper: {
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
    },
    creatableOkBtn: {
      width: '30%',
      alignSelf: 'flex-end',
      '&&': { margin: '10px 6px 6px 6px' },
    },
    chipRoot: {
      '&&': {
        height: 26,
        borderRadius: 12,
        backgroundColor: primary.main,
        margin: '0 3px',
      },
      '&& .MuiChip-deleteIcon': {
        color: 'white',
      },
    },
    chipLabel: {
      color: 'white',
      '&&': {
        padding: '4px 6px',
        marginRight: 4,
      },
    },
    labelsFiltersWrapper: {
      marginBottom: 20,
      display: 'flex',
    },
    labelsAutoCompleteRoot: {
      flex: 1,
      margin: '0px 6px',
      '& .MuiFormControl-root': {
        border: '1px solid #67718A',
        height: 'auto',
      },
      '&& .MuiFilledInput-root': {
        paddingTop: 0,
      },
      ...AutocompleteInput,
    },
    kindChoices: {
      display: 'flex',
      justifyContent: 'flex-end',
      overflow: 'hidden',
      alignItems: 'center',
      '&>*': {
        marginRight: 12,
      },
      color: common.black,
      borderBottom: '1px solid #dcdcdc',
      paddingBottom: 12,
    },
    typeChip: {
      borderRadius: 12,
      padding: '6px 12px 6px 12px',
      backgroundColor: greyscale.darker,
      fontSize: 12,
      color: common.black,
      textDecoration: 'none',
    },
    typeChipActive: {
      backgroundColor: primary.main,
      color: common.white,
      boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
    },
    newSavedSearchWrapper: {
      display: 'flex',
      alignItems: 'center',
      margin: '6px 0px 12px 0px',
      '& :nth-child(1)': {
        flex: 1,
      },
      '& :nth-child(2)': {
        width: 50,
      },
    },
    popperLabel: {
      backgroundColor: common.white,
      padding: '4px 16px',
      marginTop: 8,
      border: `1px solid ${primary.light}`,
      borderRadius: '0px 3px',
      ...typography.subtitle1,
      fontWeight: typography.fontWeightBold,
      fontSize: 12,
    },
  };
};
