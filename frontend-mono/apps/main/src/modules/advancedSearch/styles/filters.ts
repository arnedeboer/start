// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const filtersStyles = () => {
  return {
    filtersOperator: {
      marginBottom: 18,
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      gap: 10,
      '&>:nth-child(1)': {
        justifyContent: 'flex-end',
      },
      '&>:nth-child(2)': {
        width: 100,
      },
    },
    filtersSelectCaseWrapper: {
      width: '100%',
      marginBottom: 10,
    },
    filtersSelectCaseMethod: {
      display: 'flex',
      flexWrap: 'wrap',
      width: '100%',
      columnGap: 14,
      marginTop: 10,
      marginBottom: 26,
      justifyContent: 'flex-end',
    },
    filtersSelectCaseSelectors: {
      display: 'flex',
      width: '100%',
    },
  };
};
