// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const editFormStyles = ({
  greyscale,
  elephant,
  breakpoints,
  error,
  coral,
  typography,
  common,
}: any) => {
  return {
    // Edit form
    editFormWrapper: {
      border: `1px solid ${greyscale.light}`,
      boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
      borderRadius: 10,
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
    },
    editFormCentered: {
      width: '100%',
      justifyContent: 'space-between',
      alignSelf: 'center',
      [breakpoints.up('md')]: {
        maxWidth: '650px',
      },
      [breakpoints.up('lg')]: {
        maxWidth: '750px',
      },
    },
    editFormTitle: {
      justifyContent: 'flex-end',
      alignItems: 'center',
      ...typography.h3,
    },
    editFormSection: {
      padding: 18,

      display: 'flex',
    },
    editFormTop: {
      flex: '0 0 auto',
      borderBottom: `1px solid ${greyscale.light}`,
    },
    editFormMiddle: {
      flex: '1 1 auto',
      overflowY: 'auto',
      height: 0, // this enables this to work as a scrollcontainer
      borderBottom: `1px solid ${greyscale.light}`,
    },
    editFormBottom: {
      flex: '0 0 auto',
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      flexDirection: 'column' as 'column',
    },
    editFormBottomErrors: {
      padding: 10,
      display: 'flex',
      justifyContent: 'flex-end',
      margin: '14px 14px 0px 14px',
      backgroundColor: error.main,
      color: 'white',
      borderRadius: 8,
      marginLeft: 'auto',
    },
    editFormBottomButtons: {
      padding: 12,
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
    },
    filtersWrapper: {
      display: 'flex',
      flexDirection: 'column',
    },
    filterRowWrapper: {
      display: 'flex',
    },
    filterRowLabel: {
      width: 180,
      alignSelf: 'center',
    },
    selfAlignTop: {
      alignSelf: 'flex-start',
    },
    filterRowContent: {
      flex: 1,
    },
    filterWrapper: {
      display: 'flex',
      border: `1px solid ${elephant.lightest}`,
      marginBottom: 20,
      padding: 16,
      borderRadius: 10,
      flexWrap: 'wrap',
      position: 'relative',
      boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
      width: '100%',
    },
    filterError: {
      flex: '1 1 100%',
      color: coral.main,
      marginTop: 20,
      ...typography.subtitle2,
    },
    filterHeader: {
      boxShadow: `0 2px 4px -4px ${common.black}`,
      display: 'flex',
      alignItems: 'center',
      paddingBottom: 10,
      margin: '0px 0px 14px 0px',
      width: '100%',
      gap: 10,
      '&>:nth-child(2)': {
        flex: 1,
        ...typography.h6,
      },
      // Operators
      '&>:nth-child(3)': {
        gap: 8,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minWidth: 'fit-content',
      },
    },
    filterOperatorStatic: {
      borderRadius: 8,
      border: `2px solid ${greyscale.light}`,
      padding: 10,
    },
    filterOperatorHelp: {
      alignSelf: 'baseline',
      color: elephant.dark,
    },
    editFormTabsWrapper: {
      border: `1px solid ${greyscale.light}`,
      borderRadius: 10,
      borderTopLeftRadius: 0,
      borderTopRightRadius: 0,
      margin: '0px 16px 0px 16px',
    },
    noFiltersMessage: {
      margin: 0,
    },
  };
};
