// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

import { permissionsStyles } from './permissions';
import { columnsStyles } from './columns';
import { editFormStyles } from './editForm';
import { mainStyles } from './main';
import { filtersStyles } from './filters';
import { leftBarStyles } from './leftBar';
import { infoDialogStyles } from './infoDialog';

export const useStyles = makeStyles(
  // @ts-ignore
  ({
    palette: {
      elephant,
      primary,
      common,
      basalt,
      coral,
      cloud,
      error,
      northsea,
    },
    mintlab: { greyscale },
    typography,
    breakpoints,
  }: Theme) => {
    return {
      wrapper: {
        display: 'flex',
        flexFlow: 'column',
        flexDirection: 'column',
        overflow: 'hidden',
        height: '100%',
      },
      scrollWrapper: {
        overflow: 'hidden',
        overflowY: 'scroll',
      },
      hide: {
        '&&': {
          display: 'none',
        },
      },
      show: {
        display: 'inherit',
      },
      // main level blocks
      content: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        overflow: 'hidden',
      },
      tabs: {
        margin: '16px 0px 0px 16px',
      },
      tabsContent: {
        margin: '0px 0px 20px 0px',
      },
      mainContent: {
        padding: 20,
        width: '75%',
        flexDirection: 'column',
        display: 'flex',
        position: 'relative',
      },

      bottomDivider: {},
      dividerTop: {
        borderBottom: 'none',
        borderTop: `1px solid ${greyscale.light}`,
      },
      noDivider: {
        border: 'none',
      },
      selectedTab: {
        backgroundColor: primary.lightest,
        color: primary.main,
      },
      tab: {
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
      },
      loadMore: {
        display: 'flex',
        margin: '20px 0px',
        justifyContent: 'center',
        alignItems: 'center',
      },
      rightCornerDeleteButton: {
        '&&': {
          position: 'absolute',
          top: -10,
          right: -14,
          color: common.white,
          backgroundColor: primary.main,
          boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
          '&:hover': {
            backgroundColor: primary.dark,
          },
        },
      },
      selectMessage: {
        textAlign: 'center' as const,
      },
      ...mainStyles({ primary, typography }),
      ...leftBarStyles({
        typography,
        primary,
        basalt,
        cloud,
        northsea,
        common,
        elephant,
        greyscale,
      }),
      ...editFormStyles({
        breakpoints,
        error,
        elephant,
        coral,
        typography,
        greyscale,
        common,
      }),
      ...permissionsStyles({ greyscale, primary, elephant }),
      ...columnsStyles({ greyscale, elephant }),
      ...filtersStyles(),
      ...infoDialogStyles(),
    };
  }
);
