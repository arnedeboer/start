// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { RowType } from '../Log.types';
import { formatDate } from './DataTable.library';

type CellRendererType = (
  rowData: {
    dataKey: any;
    rowData: RowType;
  },
  t: i18next.TFunction,
  classes: any
) => React.ReactElement;

/* eslint complexity: [2, 9] */
export const cellRenderer: CellRendererType = (
  { dataKey, rowData },
  t,
  classes
) => {
  switch (dataKey) {
    case 'caseId': {
      return rowData.caseId ? (
        <a href={`/intern/zaak/${rowData.caseId}`}>{rowData.caseId}</a>
      ) : (
        <div className={classes.tableCell}>{'-'}</div>
      );
    }
    case 'dateCreated': {
      return <span>{formatDate(rowData.dateCreated)}</span>;
    }
    case 'component': {
      return (
        <span>{t(`table.components.${rowData.component}`) as string}</span>
      );
    }
    case 'event': {
      return (
        <Tooltip title={rowData.event}>
          <span>{rowData.event}</span>
        </Tooltip>
      );
    }
    default: {
      // @ts-ignore
      return <div className={classes.tableCell}>{rowData[dataKey] || '-'}</div>;
    }
  }
};
