// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import classNames from 'classnames';
import { GetDataReturnType } from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Progress from '@mintlab/ui/App/Zaaksysteem/Progress/Progress';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { hasNoValue } from '@zaaksysteem/common/src/library/normalizeValue';
import { formatAddress } from '../ContactView.library';
import { SubjectType } from './../ContactView.types';
import {
  DaysLeftType,
  StatusType,
  rowType,
  CasesRowType,
  DataParams,
  SelectFilterType,
  ArchivalStatusType,
} from './Cases.types';
import { fetchCases } from './requests';
import { useCaseTableStyles } from './Cases.style';

type GetSelectOptionsType = (
  t: i18next.TFunction,
  subject: SubjectType
) => ValueType<SelectFilterType | unknown>[];

//@ts-ignore
export const getSelectOptions: GetSelectOptionsType = (t, subject) => {
  const {
    attributes: { name, has_valid_address },
  } = subject;

  return [
    {
      value: 'owned',
      label: `${t('casesFor')} ${name}`,
    },
    {
      value: 'ownedOpen',
      label: `${t('openCasesFor')} ${name}`,
    },
    {
      value: 'authorized',
      label: t('authorizedCases'),
    },
    {
      value: 'involdedIn',
      label: t('involdedInCases'),
    },
    ...(has_valid_address
      ? [
          {
            value: 'shared_address',
            label: `${t('addressCases')} ${formatAddress(subject)}`,
          },
        ]
      : []),
  ];
};

const systemRoles = ['Aanvrager', 'Behandelaar', 'Coordinator', 'Ontvanger'];

// move the system roles to the front of the array
const sortRoles = (roles: string[]) => {
  const onlySystemRoles = systemRoles.reduce((acc: string[], role) => {
    if (roles.includes(role)) {
      return [...acc, role];
    }

    return acc;
  }, []);

  const withoutSystemRoles = roles.filter(role => !systemRoles.includes(role));

  return [...onlySystemRoles, ...withoutSystemRoles];
};

const transformRoles = (roles: string[] | null) => {
  // contacts can be related to a case while erroneously missing a role for that relation
  if (!roles) {
    return [];
  }

  // contacts can erroneously have multiple relations (with the same case) with the same role
  const uniqueRoles = [...new Set(roles)];

  return sortRoles(uniqueRoles);
};

const getDaysLeft = (days_left: DaysLeftType) =>
  Boolean(days_left) && (Boolean(days_left.amount) || days_left.amount === 0)
    ? days_left.amount
    : '-';

const transformStatus = (
  status: StatusType,
  archivalState: ArchivalStatusType,
  destructionDate: string
) => {
  if (status !== 'resolved') return status;
  if (archivalState === 'overdragen') return 'transfer';
  // cases can be resolved, set to be destroyed, but with destructionDate to be determined
  if (!destructionDate) return status;

  const now = new Date();
  const dateOfDestruction = new Date(destructionDate);

  return now > dateOfDestruction ? 'destroy' : status;
};

export const getRows = (data: rowType): CasesRowType[] =>
  data.map(
    ({
      id,
      attributes: {
        number,
        status,
        summary,
        roles,
        progress,
        days_left,
        archival_state,
        destruction_date,
      },
      relationships,
    }) => ({
      uuid: id,
      name: id,
      number,
      casetype: relationships?.case_type?.meta.summary,
      status: transformStatus(status, archival_state, destruction_date),
      summary,
      roles: transformRoles(roles),
      progress: progress * 100,
      daysLeft: getDaysLeft(days_left),
    })
  );

export const getColumns = (
  t: i18next.TFunction,
  classes: ReturnType<typeof useCaseTableStyles>
) => {
  return [
    {
      name: 'status',
      width: 50,
      minWidth: 50,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CasesRowType }) => (
        <Tooltip
          key={rowData.number}
          title={t(`statusType.${rowData.status}`)}
          placement="bottom"
        >
          <ZsIcon size="small">{`caseStatus.${rowData.status}`}</ZsIcon>
        </Tooltip>
      ),
    },
    {
      name: 'number',
      width: 90,
      minWidth: 90,
      defaultSort: true,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CasesRowType }) => (
        <a href={`/intern/zaak/${rowData.number}`}>{rowData.number}</a>
      ),
    },
    {
      name: 'progress',
      width: 150,
      minWidth: 70,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CasesRowType }) => (
        <Progress percentage={rowData.progress} />
      ),
    },
    {
      name: 'casetype',
      width: 200,
      minWidth: 70,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CasesRowType }) => (
        <div className={classes.casetype}>
          <span>{rowData.casetype}</span>
        </div>
      ),
    },
    {
      name: 'summary',
      width: 400,
      flexGrow: 1,
      disableSort: true,
      cellRenderer: ({ rowData: { summary } }: { rowData: CasesRowType }) => {
        if (summary) {
          return (
            <Tooltip className={classes.summary} title={summary}>
              <span>{summary}</span>
            </Tooltip>
          );
        }
      },
    },
    {
      name: 'roles',
      width: 150,
      disableSort: true,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CasesRowType }) => (
        <ul className={classes.roleList}>
          {rowData.roles.map(role => {
            const translatedRole = systemRoles.includes(role)
              ? t(`common:case.role.${role}`)
              : role;

            return <li key={role}>{translatedRole}</li>;
          })}
        </ul>
      ),
    },
    {
      name: 'daysLeft',
      width: 80,
      minWidth: 80,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CasesRowType }) => {
        const daysLeft = rowData.daysLeft;
        const pastDue = daysLeft < 0;
        const onTime = daysLeft >= 0;

        return (
          <span
            className={classNames(
              classes.daysLeft,
              pastDue && classes.daysLeftNegative,
              onTime && classes.daysLeftPositive
            )}
          >
            {daysLeft}
          </span>
        );
      },
    },
  ].map(row => ({
    ...row,
    label: t(`columns.${row.name}`),
  }));
};

type GetDataType = (
  params: DataParams
) => Promise<GetDataReturnType<CasesRowType>>;
export const getData: GetDataType = async ({
  uuid,
  pageNum,
  pageLength,
  openServerErrorDialog,
  params,
  selectFilter,
  textFilter,
  sortBy,
  sortDirection,
}: Pick<
  DataParams,
  | 'uuid'
  | 'pageNum'
  | 'pageLength'
  | 'params'
  | 'openServerErrorDialog'
  | 'selectFilter'
  | 'textFilter'
  | 'sortBy'
  | 'sortDirection'
>) => {
  const resultsData = await fetchCases({
    uuid,
    pageNum,
    pageLength,
    openServerErrorDialog,
    params,
    selectFilter,
    textFilter,
    sortBy,
    sortDirection,
  });

  return {
    rows: getRows(resultsData.rows),
    ...(!hasNoValue(resultsData.totalResults) && {
      totalResults: resultsData.totalResults,
    }),
  };
};
