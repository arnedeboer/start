// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useCaseTableStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { common, error, lizard } }: Theme) => ({
    wrapper: {
      padding: 20,
      boxSizing: 'border-box',
      height: '100%',
    },
    formWrapper: {
      padding: 20,
    },
    buttonWrapper: {
      display: 'flex',
    },
    button: {
      marginTop: 20,
      marginRight: 20,
    },
    table: {
      height: 'calc(100% - 20px - 2 * 12px)',
    },
    daysLeft: {
      minWidth: 40,
      borderRadius: 10,
      padding: '1px 8px',
      display: 'inline-block',
      textAlign: 'center',
    },
    daysLeftPositive: {
      backgroundColor: lizard.main,
      color: common.white,
    },
    daysLeftNegative: {
      backgroundColor: error.main,
      color: common.white,
    },
    casetype: {
      width: '100%',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    summary: {
      width: '100%',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    roleList: {
      display: 'flex',
      flexDirection: 'column',
      gap: 5,
      listStyleType: 'none',
      margin: 0,
      padding: 0,
    },
    filterBar: {
      margin: '12px 6px 12px 6px',
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      flexWrap: 'wrap',
      gap: 20,
      '&>*': {
        maxWidth: 300,
        height: 36,
        borderRadius: 4,
      },
    },
    totalResults: {
      maxWidth: 'auto',
      marginRight: 'auto',
      height: 'auto',
    },
    loader: {
      position: 'absolute',
      marginLeft: 'auto',
      marginRight: 'auto',
      left: 0,
      right: 0,
      textAlign: 'center',
    },
  })
);
