// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectType } from '../../ContactView.types';
import { ensureEmptyValue } from './library';

export const transformOrganization = (organization: SubjectType) => {
  const {
    id: uuid,
    type,
    attributes: {
      source,
      authenticated,
      name,
      location_address,
      correspondence_address,
      contact_person,
      contact_information,
      date_registered: dateRegistered,
      date_founded: dateFounded,
      date_ceased: dateCeased,
      rsin,
      oin,
      coc_number: cocNumber,
      coc_location_number: cocLocationNumber,
      organization_type: organizationType,
      main_activity: mainActivity,
      secondary_activities: secondaryActivities,
      is_active: isActive,
    },
    relationships,
  } = organization;

  const contactInitials = contact_person?.initials;
  const contactInsertions = contact_person?.insertions;
  const contactFamilyName = contact_person?.family_name;
  const contactFirstName = contact_person?.first_name;

  const locationCountry = location_address?.country;
  const locationCountryCode = location_address?.country_code;
  const locationStreet = location_address?.street;
  const locationHouseNumber = location_address?.street_number;
  const locationHouseNumberLetter = location_address?.street_number_letter;
  const locationHouseNumberSuffix = location_address?.street_number_suffix;
  const locationZipcode = location_address?.zipcode;
  const locationCity = location_address?.city;
  const locationForeignAddress1 = location_address?.address_line_1;
  const locationForeignAddress2 = location_address?.address_line_2;
  const locationForeignAddress3 = location_address?.address_line_3;

  const hasCorrespondenceAddress = correspondence_address ? 'yes' : 'no';

  const correspondenceCountry = correspondence_address?.country;
  const correspondenceCountryCode = correspondence_address?.country_code;
  const correspondenceStreet = correspondence_address?.street;
  const correspondenceHouseNumber = correspondence_address?.street_number;
  const correspondenceHouseNumberLetter =
    correspondence_address?.street_number_letter;
  const correspondenceHouseNumberSuffix =
    correspondence_address?.street_number_suffix;
  const correspondenceZipcode = correspondence_address?.zipcode;
  const correspondenceCity = correspondence_address?.city;

  const correspondenceForeignAddress1 = correspondence_address?.address_line_1;
  const correspondenceForeignAddress2 = correspondence_address?.address_line_2;
  const correspondenceForeignAddress3 = correspondence_address?.address_line_3;

  const email = contact_information?.email;
  const mobileNumber = contact_information?.mobile_number;
  const phoneNumber = contact_information?.phone_number;
  const internalNote = contact_information?.internal_note;
  const anonymousUser = contact_information?.is_an_anonymous_contact_person;
  const preferredContactChannel =
    contact_information?.preferred_contact_channel;

  const relatedObjectUuid = relationships?.related_custom_object?.data?.id;

  return {
    uuid,
    type,
    source,
    authenticated: Boolean(authenticated),
    name: ensureEmptyValue(name),
    rsin,
    oin,
    anonymousUser,
    cocNumber: ensureEmptyValue(cocNumber),
    cocLocationNumber: ensureEmptyValue(cocLocationNumber),
    organizationType,
    contactInitials: ensureEmptyValue(contactInitials),
    contactInsertions: ensureEmptyValue(contactInsertions),
    contactFamilyName: ensureEmptyValue(contactFamilyName),
    contactFirstName: ensureEmptyValue(contactFirstName),
    hasCorrespondenceAddress,
    locationCountry: ensureEmptyValue(locationCountry),
    locationCountryCode,
    locationStreet: ensureEmptyValue(locationStreet),
    locationHouseNumber: ensureEmptyValue(locationHouseNumber),
    locationHouseNumberLetter: ensureEmptyValue(locationHouseNumberLetter),
    locationHouseNumberSuffix: ensureEmptyValue(locationHouseNumberSuffix),
    locationZipcode: ensureEmptyValue(locationZipcode),
    locationCity: ensureEmptyValue(locationCity),
    locationForeignAddress1,
    locationForeignAddress2,
    locationForeignAddress3,
    correspondenceCountry: ensureEmptyValue(correspondenceCountry),
    correspondenceCountryCode,
    correspondenceStreet: ensureEmptyValue(correspondenceStreet),
    correspondenceHouseNumber: ensureEmptyValue(correspondenceHouseNumber),
    correspondenceHouseNumberLetter: ensureEmptyValue(
      correspondenceHouseNumberLetter
    ),
    correspondenceHouseNumberSuffix: ensureEmptyValue(
      correspondenceHouseNumberSuffix
    ),
    correspondenceZipcode: ensureEmptyValue(correspondenceZipcode),
    correspondenceCity: ensureEmptyValue(correspondenceCity),
    correspondenceForeignAddress1,
    correspondenceForeignAddress2,
    correspondenceForeignAddress3,
    dateRegistered,
    dateFounded,
    dateCeased,
    mainActivity: mainActivity?.description,
    secondaryActivities,
    email,
    mobileNumber,
    phoneNumber,
    internalNote,
    preferredContactChannel,
    relatedObjectUuid,
    isActive,
  };
};
