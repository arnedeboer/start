// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession';
import Common from '../tables/common/Common';
import { ObjectType, ObjectTypeType } from '../Information.types';
import Notifications from '../tables/notificationSettings/NotificationSettings';
import Signature from '../tables/signature/Signature';
import PhoneExtension from '../tables/phoneExtension/PhoneExtension';
import RelatedObject from '../tables/relatedObject/RelatedObject';
import Special from '../tables/special/Special';
import { SubjectType } from './../../ContactView.types';

export interface PersonViewPropsType {
  data: {
    subject: SubjectType;
    object: ObjectType;
    objectType: ObjectTypeType;
    hasPhoneIntegration: boolean;
  };
  session: SessionType;
  refreshSubject: () => {};
  setSnackOpen: any;
}

const PersonView: React.FunctionComponent<PersonViewPropsType> = ({
  data: { subject, object, objectType, hasPhoneIntegration },
  session,
  refreshSubject,
  setSnackOpen,
}) => {
  const lookingAtSelf = session.logged_in_user.uuid === subject.uuid;

  return (
    <>
      <Common
        subject={subject}
        refreshSubject={refreshSubject}
        setSnackOpen={setSnackOpen}
        session={session}
      />
      {lookingAtSelf && <Notifications subject={subject} />}
      {lookingAtSelf && hasPhoneIntegration && (
        <PhoneExtension subject={subject} />
      )}
      <Signature subject={subject} lookingAtSelf={lookingAtSelf} />
      {object && <RelatedObject object={object} objectType={objectType} />}
      <Special subject={subject} />
    </>
  );
};

export default PersonView;
