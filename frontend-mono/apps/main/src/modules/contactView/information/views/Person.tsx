// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession';
import { COUNTRY_CODE_NETHERLANDS } from '@zaaksysteem/common/src/constants/countryCodes.constants';
import {
  AltAuthDataType,
  ObjectType,
  ObjectTypeType,
} from '../Information.types';
import Common from '../tables/common/Common';
import Additional from '../tables/additional/Additional';
import RelatedObject from '../tables/relatedObject/RelatedObject';
import AltAuth from '../tables/altAuth/AltAuth';
import Special from '../tables/special/Special';
import Sensitive from '../tables/sensitive/Sensitive';
import { SubjectType } from './../../ContactView.types';

export interface PersonViewPropsType {
  data: {
    subject: SubjectType;
    object: ObjectType;
    objectType: ObjectTypeType;
    altAuthData?: AltAuthDataType;
  };
  refreshSubject: () => {};
  session: SessionType;
  setSnackOpen: any;
}

const PersonView: React.FunctionComponent<PersonViewPropsType> = ({
  data: { subject, object, objectType, altAuthData },
  refreshSubject,
  session,
  setSnackOpen,
}) => {
  const customerCountryCode = session.account.instance.country_code;

  return (
    <>
      <Common
        subject={subject}
        refreshSubject={refreshSubject}
        setSnackOpen={setSnackOpen}
        session={session}
      />
      <Additional
        subject={subject}
        refreshSubject={refreshSubject}
        setSnackOpen={setSnackOpen}
        session={session}
      />
      {altAuthData && customerCountryCode == COUNTRY_CODE_NETHERLANDS && (
        <AltAuth
          altAuthData={altAuthData}
          setSnackOpen={setSnackOpen}
          subject={subject}
        />
      )}
      {object && <RelatedObject object={object} objectType={objectType} />}
      <Special subject={subject} />
      <Sensitive />
    </>
  );
};

export default PersonView;
