// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  saveAltAuth,
  sendAltAuth,
  deleteAltAuth,
} from '../../Information.requests';
import { SubjectType } from '../../../ContactView.types';
import { AltAuthDataType } from '../../Information.types';
import { getFormDefinition } from './altAuth.formDefinition';

type AltAuthPropsType = {
  altAuthData: AltAuthDataType;
  setSnackOpen: any;
  subject: SubjectType;
};

const AltAuth: React.FunctionComponent<AltAuthPropsType> = ({
  altAuthData,
  setSnackOpen,
  subject,
}) => {
  const [t] = useTranslation('information');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [confirmOpen, setConfirmOpen] = useState<boolean>(false);
  const [busy, setBusy] = useState<boolean>(false);
  const [deleteSnackOpen, setDeleteSnackOpen] = useState<boolean>(false);
  const client = useQueryClient();
  const formDefinition = getFormDefinition(t, altAuthData);
  const validationMap = generateValidationMap(formDefinition);

  const parseV1Errors = (errorObj: any): V2ServerErrorsType | null => {
    const results = errorObj?.result;
    const checks = [
      ['missing: kvknummer', t('altAuth.errors.kvk')],
      ['missing: vestigingsnummer', t('altAuth.errors.vestigingsnummer')],
    ];

    const getErrorMessage = (message: any) => {
      for (var check of checks) {
        if (message.indexOf(check[0]) !== -1) return check[1];
      }
    };

    if (results.length && results[0]?.messages?.length) {
      const message = results[0].messages[0] as string;
      const errorMessage = getErrorMessage(message);
      return errorMessage
        ? {
            response: errorObj.response,
            errors: [
              {
                detail: errorMessage,
                code: '',
              },
            ],
          }
        : null;
    }
    return null;
  };

  const {
    fields,
    formik: { isValid, values, dirty, setValues },
  } = useForm({
    formDefinition,
    validationMap,
  });

  return (
    <>
      {ServerErrorDialog}
      <Snackbar
        scope="contact-view-auth-snack"
        handleClose={() => setDeleteSnackOpen(false)}
        message={t('altAuth.deletedMessage')}
        open={deleteSnackOpen}
      />
      <ConfirmDialog
        title={t('altAuth.deleteAccount')}
        body={t('altAuth.confirmDeleteBody')}
        open={confirmOpen}
        onClose={() => setConfirmOpen(false)}
        onConfirm={async () => {
          setBusy(true);
          deleteAltAuth(altAuthData.id, subject.type)
            .then(async () => {
              client.invalidateQueries(['contactInformation']);
              setValues({
                email: '',
                username: '',
                phone: '',
                active: '0',
              });
              setDeleteSnackOpen(true);
              setConfirmOpen(false);
            })
            .catch(error => openServerErrorDialog(error))
            .finally(() => {
              setBusy(false);
            });
        }}
      />
      <SubHeader
        title={t('altAuth.title')}
        description={t('altAuth.subTitle')}
      />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        <Button
          action={() =>
            saveAltAuth(altAuthData.id, values)
              .then(() => {
                client.invalidateQueries(['contactInformation']);
                setSnackOpen(true);
              })
              .catch((err: any) => {
                openServerErrorDialog(parseV1Errors(err) || err);
              })
          }
          name="saveAltAuth"
          disabled={!isValid || !dirty}
        >
          {t('common:verbs.save')}
        </Button>
        <Button
          action={() =>
            sendAltAuth(altAuthData.id, values).catch(openServerErrorDialog)
          }
          sx={{ marginLeft: '20px' }}
          name="sendAltAuth"
        >
          {t('altAuth.sendActivationEmail')}
        </Button>
        <Button
          action={() => setConfirmOpen(true)}
          sx={{ marginLeft: '20px' }}
          name="delete"
          disabled={altAuthData.hasAccount === false || busy}
        >
          {t('altAuth.deleteAccount')}
        </Button>
      </div>
    </>
  );
};

export default AltAuth;
