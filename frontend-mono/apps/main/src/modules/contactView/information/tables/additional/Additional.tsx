// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  hasCapability,
  SessionType,
} from '@zaaksysteem/common/src/hooks/useSession';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { saveAdditionalInfo } from '../../Information.requests';
import { SubjectType } from './../../../ContactView.types';
import { getPersonFormDefinition } from './formDefinition.person';
import { getOrganizationFormDefinition } from './formDefinition.organization';

const getFormDefinition = {
  person: getPersonFormDefinition,
  organization: getOrganizationFormDefinition,
  employee: () => [],
};

export interface AdditionalPropsType {
  subject: SubjectType;
  refreshSubject: () => {};
  setSnackOpen: any;
  session: SessionType;
}

const Additional: React.FunctionComponent<AdditionalPropsType> = ({
  subject,
  refreshSubject,
  setSnackOpen,
  session,
}) => {
  const hasEditCapability = hasCapability(session, 'contact_nieuw');
  const contactChannelEnabled = session.configurable.contact_channel_enabled;
  const [t] = useTranslation('information');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const formDefinition = getFormDefinition[subject.type]({
    t,
    subject,
    hasEditCapability,
    contactChannelEnabled,
  });
  const validationMap = generateValidationMap(formDefinition);
  const [busy, setBusy] = useState(false);
  const {
    fields,
    formik: { values, dirty },
  } = useForm({
    formDefinition,
    validationMap,
  });

  return (
    <>
      {ServerErrorDialog}
      <SubHeader
        title={t('additional.title')}
        description={t('additional.subTitle')}
      />
      <div>
        {fields.map(
          ({
            FieldComponent,
            name,
            error,
            touched,
            defaultValue,
            value,
            ...rest
          }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value || defaultValue || value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        {(hasEditCapability ||
          (!hasEditCapability && !subject.anonymousUser)) && (
          <Button
            action={() => {
              setBusy(true);
              saveAdditionalInfo(subject.uuid, values, subject.type)
                .then(() => {
                  refreshSubject();
                  setSnackOpen(true);
                })
                .catch(openServerErrorDialog)
                .finally(() => setBusy(false));
            }}
            name="saveAdditionalInfo"
            disabled={busy || !dirty}
          >
            {t('common:verbs.save')}
          </Button>
        )}
      </div>
    </>
  );
};

export default Additional;
