// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { SessionType } from '@zaaksysteem/common/src/hooks/useSession';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  showFields,
  setRequired,
  setOptional,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  COUNTRY_CODE_NETHERLANDS,
  COUNTRY_CODE_CURACAO,
} from '@zaaksysteem/common/src/constants/countryCodes.constants';
import { SubjectType } from './../../../ContactView.types';

const DISPLAY_FORMAT = 'DD-MM-YYYY';

/* eslint complexity: [2, 50] */
export const getOrganizationFormDefinition = ({
  t,
  subject,
  canEdit,
  session,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
  canEdit?: boolean;
  session: SessionType;
}): AnyFormDefinitionField[] => {
  const customerCountryCode = session.account.instance.country_code;

  return [
    {
      name: 'generalLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
      label: t('common.organization.generalLabel'),
    },
    {
      name: 'name',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
      required: true,
    },
    {
      name: 'cocNumber',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'cocLocationNumber',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'rsin',
      type: fieldTypes.TEXT,
      readOnly: true,
    },
    {
      name: 'oin',
      type: fieldTypes.TEXT,
      readOnly: true,
    },
    {
      name: 'organizationType',
      type: fieldTypes.FLATVALUE_SELECT,
      isClearable: false,
      readOnly: !canEdit,
      // these values are saved in dutch
      choices: [
        'Eenmanszaak',
        'Maatschap',
        'Vennootschap onder firma',
        'Commanditaire vennootschap met een beherend vennoot',
        'Besloten vennootschap met gewone structuur',
        'Naamloze vennootschap met gewone structuur',
        'Europese naamloze vennootschap (SE) met gewone structuur',
        'Vereniging van eigenaars',
        'Kerkgenootschap',
        'Stichting',
        'Publiekrechtelijke rechtspersoon',
        'Coöperatie',
        'Vereniging',
      ].map(value => ({ value, label: value })),
    },
    {
      name: 'dateRegistered',
      type: fieldTypes.DATEPICKER,
      value: subject['dateRegistered'],
      readOnly: true,
      dateFormat: DISPLAY_FORMAT,
    },
    {
      name: 'dateFounded',
      type: fieldTypes.DATEPICKER,
      value: subject['dateFounded'],
      readOnly: true,
      dateFormat: DISPLAY_FORMAT,
    },
    {
      name: 'dateCeased',
      type: fieldTypes.DATEPICKER,
      value: subject['dateCeased'],
      readOnly: true,
      dateFormat: DISPLAY_FORMAT,
    },
    {
      name: 'mainActivity',
      type: fieldTypes.TEXT,
      readOnly: true,
    },
    {
      name: 'secondaryActivities',
      type: fieldTypes.MULTI_VALUE_TEXT,
      value: subject['secondaryActivities'].map(
        ({ description }: { description: string }) => ({
          value: description,
          label: description,
        })
      ),
      readOnly: true,
    },
    {
      name: 'contactLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
      label: t('common.organization.contactLabel'),
    },
    {
      name: 'contactFirstName',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
      required: true,
    },
    {
      name: 'contactInsertions',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'contactFamilyName',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
      required: true,
    },
    {
      name: 'addressLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
      label: t('common.organization.addressLabel'),
    },
    {
      name: 'locationCountry',
      type: fieldTypes.COUNTRY_FINDER,
      value: {
        label:
          subject['locationCountry'] ||
          (customerCountryCode == COUNTRY_CODE_NETHERLANDS
            ? t('common:countries.netherlands')
            : t('common:countries.curacao')),
        value:
          subject.locationCountryCode === null ||
          subject.locationCountryCode === undefined
            ? customerCountryCode
            : subject.locationCountryCode,
      },
      readOnly: !canEdit,
      required: true,
    },
    {
      name: 'locationStreet',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'locationHouseNumber',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'locationHouseNumberLetter',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'locationHouseNumberSuffix',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'locationZipcode',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'locationCity',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'locationForeignAddress1',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'locationForeignAddress2',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'locationForeignAddress3',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceLabel',
      type: fieldTypes.TEXT,
      readOnly: true,
      isLabel: true,
      label: t('common.organization.correspondenceLabel'),
    },
    {
      name: 'hasCorrespondenceAddress',
      type: fieldTypes.RADIO_GROUP,
      choices: [
        {
          label: t(`common:yes`),
          value: t(`common:yes`),
        },
        {
          label: t(`common:no`),
          value: t(`common:no`),
        },
      ],
      value: t(`common:${subject['hasCorrespondenceAddress']}`),
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceCountry',
      type: fieldTypes.COUNTRY_FINDER,
      readOnly: !canEdit,
      value: {
        label:
          subject['correspondenceCountry'] ||
          (customerCountryCode == COUNTRY_CODE_NETHERLANDS
            ? t('common:countries.netherlands')
            : t('common:countries.curacao')),
        value:
          subject.correspondenceCountryCode === null ||
          subject.correspondenceCountryCode === undefined
            ? customerCountryCode
            : subject.correspondenceCountryCode,
      },
    },
    {
      name: 'correspondenceStreet',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceHouseNumber',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceHouseNumberLetter',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceHouseNumberSuffix',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceZipcode',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceCity',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceForeignAddress1',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceForeignAddress2',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
    {
      name: 'correspondenceForeignAddress3',
      type: fieldTypes.TEXT,
      readOnly: !canEdit,
    },
  ].map(field => ({
    label: t(`common.organization.${field.name}`),
    value: subject[field.name],
    ...field,
  }));
};

const locationAddressFields = [
  'locationStreet',
  'locationHouseNumber',
  'locationHouseNumberLetter',
  'locationHouseNumberSuffix',
  'locationZipcode',
  'locationCity',
];

const locationForeignAddressFields = [
  'locationForeignAddress1',
  'locationForeignAddress2',
  'locationForeignAddress3',
];

const correspondenceAddressFields = [
  'correspondenceStreet',
  'correspondenceHouseNumber',
  'correspondenceHouseNumberLetter',
  'correspondenceHouseNumberSuffix',
  'correspondenceZipcode',
  'correspondenceCity',
];

const correspondenceForeignAddressFields = [
  'correspondenceForeignAddress1',
  'correspondenceForeignAddress2',
  'correspondenceForeignAddress3',
];

const correspondenceRequiredFields = [
  'correspondenceStreet',
  'correspondenceHouseNumber',
  'correspondenceZipcode',
  'correspondenceCity',
  'correspondenceCountry',
];

const locationRequiredFields = [
  'locationStreet',
  'locationHouseNumber',
  'locationZipcode',
  'locationCity',
  'locationCountry',
];

const allCorrespondenceAddressFields = [
  ...correspondenceAddressFields,
  ...correspondenceForeignAddressFields,
];

export const getOrganizationRules = (session: SessionType) => {
  const customerCountryCode = session.account.instance.country_code;

  return [
    new Rule()
      .when('locationCountry', (field: any) => {
        const locationCountryCode = field?.value?.value;

        return customerCountryCode == locationCountryCode;
      })
      .then(showFields(locationAddressFields))
      .then(hideFields(locationForeignAddressFields))
      .then(setRequired(locationRequiredFields))
      .then(setOptional(['locationForeignAddress1']))
      .else(showFields(locationForeignAddressFields))
      .else(hideFields(locationAddressFields))
      .else(setRequired(['locationForeignAddress1']))
      .else(setOptional(locationRequiredFields)),
    new Rule()
      .when('hasCorrespondenceAddress', field => field.value === 'Ja')
      .then(showFields(['correspondenceCountry']))
      .then(setRequired(['correspondenceCountry']))
      .else(hideFields(['correspondenceCountry']))
      .else(setOptional(['correspondenceCountry'])),
    new Rule()
      .when('correspondenceCountry', (field: any) => {
        const correspondenceCountryCode = field?.value?.value;

        return customerCountryCode == correspondenceCountryCode;
      })
      .then(showFields(correspondenceAddressFields))
      .then(hideFields(correspondenceForeignAddressFields))
      .then(setRequired(correspondenceRequiredFields))
      .then(setOptional(['correspondenceForeignAddress1']))
      .else(showFields(correspondenceForeignAddressFields))
      .else(hideFields(correspondenceAddressFields))
      .else(setRequired(['correspondenceForeignAddress1']))
      .else(setOptional(correspondenceRequiredFields)),
    new Rule()
      .when('hasCorrespondenceAddress', field => field.value === 'Nee')
      .then(hideFields(allCorrespondenceAddressFields)),
    new Rule()
      .when(() => customerCountryCode == COUNTRY_CODE_CURACAO)
      .then(hideFields(['locationZipcode', 'correspondenceZipcode']))
      .then(setOptional(['locationZipcode', 'correspondenceZipcode'])),
  ];
};
