// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { SubjectType } from './../../../ContactView.types';
import { getPersonFormDefinition } from './formDefinition.person';
import { getOrganizationFormDefinition } from './formDefinition.organization';
import { getEmployeeFormDefinition } from './formDefinition.employee';

const getFormDefinition = {
  person: getPersonFormDefinition,
  organization: getOrganizationFormDefinition,
  employee: getEmployeeFormDefinition,
};

type SpecialPropsType = {
  subject: SubjectType;
};

const Special: React.FunctionComponent<SpecialPropsType> = ({ subject }) => {
  const [t] = useTranslation('information');
  const formDefinition = getFormDefinition[subject.type]({ t, subject });

  const {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
  });

  return (
    <>
      <SubHeader
        title={t('special.title')}
        description={t('special.subTitle')}
      />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
      </div>
    </>
  );
};

export default Special;
