// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  hasSystemRole,
  SignatureUploadRoleType,
  SessionType,
} from '@zaaksysteem/common/src/hooks/useSession';

type CanUploadType = (
  session: SessionType,
  signatureUploadRole: SignatureUploadRoleType,
  lookingAtSelf: boolean
) => boolean;

export const canUpload: CanUploadType = (
  session,
  signatureUploadRole,
  lookingAtSelf
) => {
  const isAdmin = hasSystemRole(session, 'Administrator');
  const isZsBeheerder = hasSystemRole(session, 'Zaaksysteembeheerder');

  return (
    isAdmin ||
    (signatureUploadRole === 'zaaksysteembeheerder' && isZsBeheerder) ||
    (signatureUploadRole === 'behandelaar' && lookingAtSelf)
  );
};

type FormatSubtitleType = (
  t: i18next.TFunction,
  canEdit: boolean,
  signatureUploadRole: SignatureUploadRoleType
) => string;

export const formatSubtitle: FormatSubtitleType = (
  t,
  canEdit,
  signatureUploadRole
) => {
  if (canEdit) {
    return t('signature.subTitle.allowed');
  }

  const roleForEditing = t(`signature.roleForEditing.${signatureUploadRole}`);

  return t('signature.subTitle.notAllowed', { roleForEditing });
};
