// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from '../../../ContactView.types';

type getChoiceWithTranslationType = (
  t: i18next.TFunction,
  values: string
) => { value: string; label: string };

const getChoiceWithTranslation: getChoiceWithTranslationType = (t, value) => ({
  value,
  label: t(`additional.preferredContactChannelValue.${value}`),
});

export type FormValuesType = {
  phoneNumber: string;
  mobileNumber: string;
  email: string;
  preferredContactChannel: string;
  internalNote: string;
  anynymousUser: boolean;
};

/* eslint complexity: [2, 50] */
export const getPersonFormDefinition = ({
  t,
  subject,
  hasEditCapability,
  contactChannelEnabled,
}: {
  t: i18next.TFunction;
  hasEditCapability?: boolean;
  subject: SubjectType;
  contactChannelEnabled: boolean;
}): AnyFormDefinitionField[] => {
  const { anonymousUser } = subject;
  return [
    {
      name: 'phoneNumber',
      type: fieldTypes.PHONE_NUMBER,
      readOnly: anonymousUser,
    },
    {
      name: 'mobileNumber',
      type: fieldTypes.PHONE_NUMBER,
      readOnly: anonymousUser,
    },
    {
      name: 'email',
      type: fieldTypes.EMAIL,
      readOnly: anonymousUser,
    },
    ...(contactChannelEnabled
      ? [
          {
            name: 'preferredContactChannel',
            type: fieldTypes.FLATVALUE_SELECT,
            isClearable: false,
            defaultValue: 'pip',
            choices: ['pip', 'email', 'mail', 'phone'].map(value =>
              getChoiceWithTranslation(t, value)
            ),
            readOnly: anonymousUser,
          },
        ]
      : []),
    {
      name: 'internalNote',
      type: fieldTypes.TEXT,
      readOnly: anonymousUser,
    },
    {
      name: 'anonymousUser',
      type: fieldTypes.CHECKBOX,
      readOnly: !hasEditCapability,
    },
  ].map(field => ({
    label: t(`additional.${field.name}`),
    value: subject[field.name],
    ...field,
  }));
};
