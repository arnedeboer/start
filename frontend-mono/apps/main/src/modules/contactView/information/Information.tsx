// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useSession, {
  hasActiveIntegration,
} from '@zaaksysteem/common/src/hooks/useSession';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { fetchContactInformation } from './Information.actions';
import { useInformationStyles } from './Information.style';
import Person from './views/Person';
import Organization from './views/Organization';
import Employee from './views/Employee';
import { SubjectType } from './../ContactView.types';
import locale from './Information.locale';

const TableDictionary: { [key: string]: any } = {
  person: Person,
  organization: Organization,
  employee: Employee,
};

type InformationPropsType = {
  subject: SubjectType;
  refreshSubject: () => void;
};

const Information: React.FunctionComponent<InformationPropsType> = ({
  subject,
  refreshSubject,
}) => {
  const session = useSession();
  const classes = useInformationStyles();
  const View = TableDictionary[subject.type];
  const altAuthActive = hasActiveIntegration(session, 'auth_twofactor');
  const [snackOpen, setSnackOpen] = useState<boolean>(false);

  const { status, data, error } = useQuery<
    ReturnType<typeof fetchContactInformation>,
    V2ServerErrorsType
  >(['contactInformation', subject, altAuthActive], () =>
    fetchContactInformation(subject, altAuthActive)
  );

  return (
    <I18nResourceBundle resource={locale} namespace="information">
      <Snackbar
        scope="contact-view-info-snack"
        handleClose={() => setSnackOpen(false)}
        message="De gegevens zijn succesvol opgeslagen."
        open={snackOpen}
      />
      <div className={classes.wrapper}>
        {status === 'loading' ? (
          <Loader />
        ) : error ? (
          <ServerErrorDialog err={error} />
        ) : (
          data && (
            <View
              data={data}
              session={session}
              refreshSubject={refreshSubject}
              setSnackOpen={setSnackOpen}
            />
          )
        )}
      </div>
    </I18nResourceBundle>
  );
};

export default Information;
