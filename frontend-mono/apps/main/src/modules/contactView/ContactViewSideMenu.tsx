// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { ContactViewParamsType } from '.';

export const ContactViewSideMenu: React.ComponentType = () => {
  const session = useSession();
  const [t] = useTranslation('contactView');
  const params = useParams<
    keyof ContactViewParamsType
  >() as ContactViewParamsType;
  const selectedItem = params['*'].split('/')[0];

  const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
    ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
  );
  const MenuItemLinkOld = React.forwardRef<any, SideMenuItemType>(
    // eslint-disable-next-line jsx-a11y/anchor-has-content
    ({ href, ...restProps }, ref) => <a href={href} {...restProps} />
  );
  MenuItemLink.displayName = 'MenuItemLink';
  MenuItemLinkOld.displayName = 'MenuItemLinkOld';

  const menuItems: SideMenuItemType[] = [
    {
      id: 'data',
      label: t('contactView:sideMenu.data'),
      href: 'data',
      icon: <Icon>{iconNames.person}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'communication',
      label: t('contactView:sideMenu.communication'),
      href: 'communication',
      icon: <Icon>{iconNames.alternate_email}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'cases',
      label: t('contactView:sideMenu.cases'),
      href: 'cases',
      icon: <Icon>{iconNames.list}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'map',
      label: t('contactView:sideMenu.map'),
      href: 'map',
      icon: <Icon>{iconNames.map}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'relationships',
      label: t('contactView:sideMenu.relationships'),
      href: 'relationships',
      icon: <Icon>{iconNames.link}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'timeline',
      label: t('contactView:sideMenu.timeline'),
      href: 'timeline',
      icon: <Icon>{iconNames.schedule}</Icon>,
      component: MenuItemLink,
    },
    ...(params.type === 'organization' &&
    session.active_interfaces.includes('controlpanel')
      ? [
          {
            id: 'environments',
            label: t('contactView:sideMenu.environments'),
            href: `environments`,
            icon: <Icon>{iconNames.storage}</Icon>,
            component: MenuItemLink,
          },
        ]
      : []),
  ].map<SideMenuItemType>(menuItem => ({
    ...menuItem,
    selected: selectedItem === menuItem.id,
  }));

  return <SideMenu items={menuItems} />;
};
