// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { NotificationType } from '../../components/NotificationBar/NotificationBar';
import { SubjectType } from './ContactView.types';

type ItemType = string | null | undefined;
type SeparateByCommaType = (list: ItemType[]) => string;

const separateByComma: SeparateByCommaType = list =>
  list.filter(item => Boolean(item)).join(', ');

const nonBreakingSpace = '\u00A0';
const nonBreakingDash = '\u2011';

type FormatDutchAddressType = (address: {
  street: string;
  street_number: string;
  street_number_letter: string | null;
  street_number_suffix: string | null;
  zipcode: string;
  city: string;
}) => string;

const formatDutchAddress: FormatDutchAddressType = ({
  street,
  street_number: number,
  street_number_letter: letter,
  street_number_suffix: suffix,
  zipcode,
  city,
}) =>
  `${street}${nonBreakingSpace}${number}${letter || ''}${
    suffix ? nonBreakingDash : ''
  }${suffix || ''}, ${zipcode ? `${nonBreakingSpace}${zipcode}` : ''}${city}`;

type FormatForeignAddressType = (address: {
  address_line_1: string;
  address_line_2: string;
  address_line_3: string;
  country: string;
}) => string;

const formatForeignAddress: FormatForeignAddressType = ({
  address_line_1: one,
  address_line_2: two,
  address_line_3: three,
  country,
}) => separateByComma([country, one, two, three]);

type FormatAddressType = (subject: SubjectType) => string | null;

export const formatAddress: FormatAddressType = ({
  attributes: {
    residence_address: residence,
    location_address: location,
    correspondence_address: correspondence,
  },
  type,
}) => {
  if (type === 'employee') return null;

  // Correspondence is leading
  const address = correspondence || residence || location;

  if (!address) return '-';

  return address.is_foreign
    ? formatForeignAddress(address)
    : formatDutchAddress(address);
};

type FormatBreadcrumbLabel = (subject: SubjectType) => string;

export const formatTitle: FormatBreadcrumbLabel = subject =>
  subject.attributes.name;

export const formatSubTitle: FormatBreadcrumbLabel = subject => {
  const {
    attributes: { contact_information },
  } = subject;

  return separateByComma([
    formatAddress(subject),
    contact_information?.phone_number,
    contact_information?.mobile_number,
    contact_information?.email,
  ]);
};

type GetNotificationsType = (
  t: i18next.TFunction,
  subject: SubjectType
) => NotificationType[];

export const getNotifications: GetNotificationsType = (t, subject) => {
  const {
    attributes: { is_secret, contact_information },
  } = subject;
  const secretNotification = { message: t('notifications.secret') };
  const internalNote = contact_information?.internal_note;
  const internalNoteNotification = {
    message: `${t(
      'notifications.internalNote'
    )}: "${contact_information?.internal_note}"`,
  };

  return [
    ...(is_secret ? [secretNotification] : []),
    ...(internalNote ? [internalNoteNotification] : []),
  ];
};
