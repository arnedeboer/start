// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(
  ({ typography, palette: { elephant, primary } }: Theme) => ({
    hostsPlaceholder: {
      color: elephant.dark,
    },
    host: {
      display: 'flex',
      width: '100%',
      alignItems: 'center',
      '&:hover': {
        color: primary.main,
      },
    },
    labelWrapper: {
      flexGrow: 1,
      overflow: 'hidden',
    },
    label: {
      width: '100%',
      display: 'block',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
    iconWrapper: {
      width: 30,
    },
    formWrapper: {
      width: 500,
      fontFamily: typography.fontFamily,
      display: 'flex',
      flexDirection: 'column',
    },
  })
);
