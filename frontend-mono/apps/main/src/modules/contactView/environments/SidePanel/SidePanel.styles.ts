// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { elephant } }: Theme) => ({
    wrapper: {
      width: 300,
      padding: 20,
      borderLeft: `1px solid ${greyscale.dark}`,
      display: 'flex',
      flexDirection: 'column',
      paddingRight: 20,
      '&>*:nth-child(odd)': { marginBottom: 10 },
      '&>*:nth-child(even)': { marginBottom: 40 },
    },
  })
);
