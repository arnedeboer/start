// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  ControlPanelType,
  EnvironmentType,
  HostType,
  UpdateHostsType,
} from '../Environments.types';
import { useStyles } from './Hosts.styles';
import HostsDialog from './HostsDialog';

type HostsPropsType = {
  controlPanel: ControlPanelType;
  environments: EnvironmentType[];
  hosts: HostType[];
  updateHosts: UpdateHostsType;
};

const Hosts: React.FunctionComponent<HostsPropsType> = ({
  controlPanel,
  environments,
  hosts,
  updateHosts,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');
  const [dialogOpen, setDialogOpen] = useState<boolean>(false);
  const [hostToEdit, setHostToEdit] = useState<HostType | null>(null);

  return (
    <div>
      {hosts.length ? (
        hosts.map((host, key) => (
          <div key={key} className={classes.host}>
            <div className={classes.labelWrapper}>
              <Tooltip title={host.fqdn}>
                <span className={classes.label}>{host.fqdn}</span>
              </Tooltip>
            </div>
            <div className={classes.iconWrapper}>
              <IconButton
                onClick={() => {
                  setDialogOpen(true);
                  setHostToEdit(host);
                }}
                color="inherit"
              >
                <Icon size="tiny">{iconNames.edit}</Icon>
              </IconButton>
              {/* backend not implemented */}
              {/* <IconButton onClick={() => {}} color="inherit">
              <Icon size="tiny">{iconNames.delete}</Icon>
            </IconButton> */}
            </div>
          </div>
        ))
      ) : (
        <div className={classes.hostsPlaceholder}>{t('hosts.placeholder')}</div>
      )}
      <Button
        name="createEnvHost"
        variant="outlined"
        action={() => setDialogOpen(true)}
        sx={{ marginTop: '10px' }}
      >
        {t('common:verbs.create')}
      </Button>
      {dialogOpen && (
        <HostsDialog
          controlPanel={controlPanel}
          environments={environments}
          host={hostToEdit}
          updateHosts={updateHosts}
          onClose={() => {
            setDialogOpen(false);
            setHostToEdit(null);
          }}
          open={dialogOpen}
        />
      )}
    </div>
  );
};

export default Hosts;
