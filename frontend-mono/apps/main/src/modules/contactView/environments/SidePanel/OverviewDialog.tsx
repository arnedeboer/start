// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { ControlPanelType } from '../Environments.types';
import { getOverviewDialogFormDefinition } from './OverviewDialog.formDefinition';
import { useStyles } from './Overview.styles';

type OverviewDialogPropsType = {
  controlPanel: ControlPanelType;
  onClose: () => void;
  open: boolean;
};

const HostsDialog: React.FunctionComponent<OverviewDialogPropsType> = ({
  controlPanel,
  onClose,
  open,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');
  const dialogEl = useRef();

  const title = t('overview.dialog.title');
  const formDefinition = getOverviewDialogFormDefinition(t, controlPanel);

  let {
    fields,
    formik: { /* values, */ isValid },
  } = useForm({
    formDefinition,
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'environments-overview-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="settings"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.formWrapper}>
            {fields.map(
              ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
                const props = cloneWithout(rest, 'mode');

                return (
                  <FormControlWrapper
                    {...props}
                    label={suppressLabel ? false : props.label}
                    compact={true}
                    key={`${props.name}-formcontrol-wrapper`}
                  >
                    <FieldComponent
                      {...props}
                      t={t}
                      containerRef={dialogEl.current}
                    />
                  </FormControlWrapper>
                );
              }
            )}
          </div>
        </DialogContent>
        <>
          <Divider />
          <DialogActions>
            {createDialogActions(
              [
                {
                  text: title,
                  disabled: !isValid,
                  onClick() {
                    // not developed yet
                  },
                },
                {
                  text: t('common:dialog.cancel'),
                  onClick: onClose,
                },
              ],
              'environments-overview-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default HostsDialog;
