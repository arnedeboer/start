// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  IdType,
  ControlPanelType,
  EnvironmentType,
  HostType,
  UpdateEnvironmentsType,
  UpdateHostsType,
} from './Environments.types';
import {
  getId,
  getEnvironments,
  getHosts,
  getControlPanels,
  saveEnvironmentAction,
  saveHostAction,
  mergeHosts,
  mergeEnvironments,
} from './Environments.library';
import Setup from './Setup/Setup';
import EnvironmentsTable from './EnvironmentsTable/EnvironmentsTable';
import SidePanel from './SidePanel/SidePanel';
import { useStyles } from './Environments.styles';

export type EnvironmentsPropsType = {
  uuid: string;
};

/* eslint complexity: [2, 8] */
const Environments = ({ uuid }: EnvironmentsPropsType) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [snack, setSnack] = useState('');
  const [loading, setLoading] = useState<boolean>(true);
  const [id, setId] = useState<IdType>();
  const [controlPanel, setControlPanel] = useState<ControlPanelType>();
  const [environments, setEnvironments] = useState<EnvironmentType[]>();
  const [hosts, setHosts] = useState<HostType[]>();

  useEffect(() => {
    if (!id) {
      getId(uuid, setId).catch(openServerErrorDialog);
    }

    if (id && !controlPanel) {
      getControlPanels(id)
        .then((controlPanel: ControlPanelType) => {
          setControlPanel(controlPanel);
          setLoading(false);
        })
        .catch(openServerErrorDialog);
    }

    if (controlPanel && controlPanel.uuid) {
      getEnvironments(controlPanel.uuid, setEnvironments).catch(
        openServerErrorDialog
      );
      getHosts(controlPanel.uuid, setHosts).catch(openServerErrorDialog);
    }
  }, [id, controlPanel]);

  if (ServerErrorDialog) {
    return <>{ServerErrorDialog}</>;
  }

  if (loading || !id || !controlPanel) {
    return <Loader />;
  }

  const refreshControlPanel = () => {
    getControlPanels(id)
      .then((controlPanel: ControlPanelType) => {
        setControlPanel(controlPanel);
        setLoading(false);
      })
      .catch(openServerErrorDialog);
  };

  const refreshEnvironments = () => {
    getEnvironments(controlPanel.uuid, setEnvironments).catch(
      openServerErrorDialog
    );
  };

  if (!controlPanel.uuid) {
    return (
      <Setup
        id={id}
        setLoading={setLoading}
        refreshControlPanel={refreshControlPanel}
      />
    );
  }

  if (!environments || !hosts) {
    return <Loader />;
  }

  const updateEnvironments: UpdateEnvironmentsType = (
    actionType,
    values,
    environment,
    onclose
  ) => {
    saveEnvironmentAction(controlPanel.uuid, values, environment?.uuid)
      .then(newEnvironment => {
        const newEnvironments = mergeEnvironments(
          actionType,
          environments,
          newEnvironment
        );

        const actionVerb =
          values.protected || values.active
            ? t('common:verbs.activated')
            : t('common:verbs.deactivated');

        setEnvironments(newEnvironments);
        setSnack(
          t(`table.snacks.${actionType}`, { verb: actionVerb.toLowerCase() })
        );

        if (onclose) {
          onclose();
        }
      })
      .catch(openServerErrorDialog);
  };

  const updateHosts: UpdateHostsType = (actionType, values, host, onClose) => {
    saveHostAction(controlPanel.uuid, actionType, values, host?.uuid)
      .then(newHost => {
        const newHosts = mergeHosts(actionType, hosts, newHost);

        // we need to refresh the environments,
        // because it holds the information for which environment a host is assigned to
        refreshEnvironments();
        setHosts(newHosts);
        setSnack(t(`hosts.snacks.${actionType}`));
        onClose();
      })
      .catch(openServerErrorDialog);
  };

  const sortedHosts = hosts.sort((hostA, hostB) =>
    hostA.label < hostB.label ? -1 : 1
  );

  return (
    <div className={classes.wrapper}>
      <EnvironmentsTable
        controlPanel={controlPanel}
        environments={environments}
        updateEnvironments={updateEnvironments}
      />
      <SidePanel
        controlPanel={controlPanel}
        environments={environments}
        hosts={sortedHosts}
        updateHosts={updateHosts}
      />
      <Snackbar
        scope="contact-view-envs-snack"
        handleClose={() => setSnack('')}
        message={snack}
        open={Boolean(snack)}
      />
    </div>
  );
};

export default Environments;
