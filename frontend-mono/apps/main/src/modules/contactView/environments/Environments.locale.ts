// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    activate: {
      activate: 'activeren',
      intro:
        'Om omgevingen voor deze organisatie te beheren, moet er eerst een keuze gemaakt worden wat voor type omgevingen bij deze organisatie horen.',
    },
    customerTypes: {
      government: 'Overheidscloud',
      commercial: 'Commerciële cloud',
      lab: 'Lab cloud',
      development: 'Development cloud',
      staging: 'Staging cloud',
      acceptance: 'Acceptatie cloud',
      testing: 'Test cloud',
      preprod: 'Preproduction cloud',
      production: 'Production cloud',
    },
    otapTypes: {
      development: 'Ontwikkeling',
      testing: 'Test',
      accept: 'Acceptatie',
      production: 'Productie',
    },
    softwareVersions: {
      master: 'Productie',
    },
    table: {
      noRows: 'Geen omgevingen ingesteld',
      status: {
        header: 'Status',
        values: {
          active: 'Actief',
          processing: 'Bezig',
          disabled: 'Inactief',
        },
      },
      label: { header: 'Titel' },
      webAddress: { header: 'Webadres' },
      hosts: { header: 'Hosts' },
      template: { header: 'Template' },
      otap: {
        header: 'Gebruiksdoel',
      },
      details: {
        title: 'Details',
        labels: {
          uuid: 'UUID',
          fqdn: 'Webadres',
          customerType: 'Type cloud',
          softwareVersion: 'Softwareversie',
          fallbackUrl: 'Maintenance URL',
          provisionedOn: 'Datum provisioning',
          servicesDomain: '"Client Side Cert"-domein',
          apiDomain: 'API domein',
          database: 'Database',
          databaseHost: 'Database host',
          filestore: 'Storage Bucket',
          freeformReference: 'Vrije referentie',
          diskspace: 'Used diskspace (GB)',
        },
      },
      createOrEdit: {
        createEnvironment: 'Omgeving aanmaken',
        labels: {
          label: 'Titel',
          fqdn: 'Webadres',
          customerType: 'Type',
          otap: 'Gebruiksdoel',
          softwareVersion: 'Softwareversie',
          template: 'Template',
          password: 'Wachtwoord',
        },
        placeholders: {
          template: 'Selecteer een template…',
        },
      },
      snacks: {
        create: 'De omgeving is aangemaakt.',
        edit: 'De omgeving is bewerkt.',
        toggle: 'De omgeving is {{verb}}',
        protect: 'De bescherming van de omgeving is {{verb}}',
      },
      actions: {
        details: 'Details bekijken',
        protect: {
          on: 'Bescherming deactiveren',
          off: 'Bescherming activeren',
        },
        toggle: {
          on: 'Omgeving deactiveren',
          off: 'Omgeving activeren',
        },
        edit: 'Omgeving bewerken',
      },
    },
    overview: {
      title: 'Overzicht',
      template: 'Template',
      shortname: 'Short name',
      customerType: 'Type',
      readOnly: 'Read-only',
      dialog: {
        title: 'Configuratie bewerken',
        labels: {
          template: 'Template',
          allowedEnvironments: 'Maximum aantal omgevingen',
          allowedDiskspace: 'Maximum diskspace (GB)',
          readOnly: 'Read-only',
        },
      },
    },
    hosts: {
      title: 'Hosts',
      placeholder: 'Er zijn nog geen hosts toegevoegd.',
      create: 'Aanmaken',
      snacks: {
        create: 'Host succesvol aangemaakt.',
        update: 'Host succesvol bewerkt.',
      },
      dialog: {
        title: 'Host {{verb}}',
        fields: {
          label: 'Titel',
          fqdn: 'Host',
          sslCert: 'SSL Certificaat',
          sslKey: 'SSL Private Key',
          template: 'Template',
          environment: 'Omgeving',
        },
        placeholders: {
          fqdn: 'voorbeeld.domein.nl',
          template: 'Selecteer een template…',
          environment: 'Selecteer een omgeving…',
        },
      },
    },
    diskspace: {
      title: 'Diskspace',
    },
    environmentsCount: {
      title: 'Aantal omgevingen',
    },
    uptime: {
      title: 'Uptime',
    },
  },
};
