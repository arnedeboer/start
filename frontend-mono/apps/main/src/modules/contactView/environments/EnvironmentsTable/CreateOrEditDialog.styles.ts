// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useCreateOrEditDialogStyles = makeStyles(
  ({ typography }: Theme) => ({
    formWrapper: {
      width: 500,
      fontFamily: typography.fontFamily,
    },
  })
);
