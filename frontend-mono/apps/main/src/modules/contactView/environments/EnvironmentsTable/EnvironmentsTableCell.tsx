// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import {
  EnvironmentType,
  RowDataType,
  UpdateEnvironmentsType,
} from './../Environments.types';

type CellRendererType = (
  rowData: RowDataType,
  setEnvironmentToView: (environment: EnvironmentType) => void,
  setEnvironmentToEdit: (environment: EnvironmentType) => void,
  updateEnvironments: UpdateEnvironmentsType,
  t: i18next.TFunction,
  classes: any
) => React.ReactElement;

/* eslint complexity: [2, 12] */
export const cellRenderer: CellRendererType = (
  { dataKey, rowData },
  setEnvironmentToView,
  setEnvironmentToEdit,
  updateEnvironments,
  t,
  classes
) => {
  switch (dataKey) {
    case 'status': {
      return (
        <span>{t(`table.${dataKey}.values.${rowData.status}`) as string}</span>
      );
    }
    case 'label': {
      return (
        <Tooltip title={rowData.label}>
          <span>{rowData.label}</span>
        </Tooltip>
      );
    }
    case 'webAddress': {
      return (
        <Tooltip title={rowData.fqdn}>
          <a
            href={`/auth/token/remote_login?instance_id=${rowData.uuid}`}
            target="_blank"
            rel="noreferrer"
          >
            {rowData.fqdn}
          </a>
        </Tooltip>
      );
    }
    case 'hosts': {
      return (
        <Tooltip
          title={
            <ul className={classes.hostList}>
              {rowData.hosts.map(host => (
                <li key={host.fqdn}>{host.fqdn}</li>
              ))}
            </ul>
          }
        >
          <ul className={classes.hostList}>
            {rowData.hosts.map(host => (
              <li key={host.fqdn}>
                <a
                  href={`/auth/token/remote_login?instance_id=${rowData.uuid}&host_id=${host.uuid}`}
                  target="_blank"
                  rel="noreferrer"
                >
                  {host.fqdn}
                </a>
              </li>
            ))}
          </ul>
        </Tooltip>
      );
    }
    case 'template': {
      return <span>{rowData.template}</span>;
    }
    case 'otap': {
      return <span>{t(`otapTypes.${rowData.otap}`) as string}</span>;
    }
    case 'actions': {
      return (
        <div className={classes.actionWrapper}>
          <IconButton
            title={t('table.actions.details')}
            onClick={(event: any) => {
              event.preventDefault();

              setEnvironmentToView(rowData);
            }}
          >
            <Icon size="small">{iconNames.list}</Icon>
          </IconButton>
          <IconButton
            title={t(
              `table.actions.protect.${rowData.protected ? 'on' : 'off'}`
            )}
            onClick={(event: any) => {
              event.preventDefault();

              updateEnvironments(
                'protect',
                {
                  protected: !rowData.protected,
                },
                rowData
              );
            }}
          >
            <Icon size="small">
              {rowData.protected ? iconNames.lock : iconNames.lock_open}
            </Icon>
          </IconButton>
          <IconButton
            title={t(`table.actions.toggle.${rowData.disabled ? 'off' : 'on'}`)}
            onClick={(event: any) => {
              event.preventDefault();

              updateEnvironments(
                'toggle',
                {
                  disabled: !rowData.disabled,
                },
                rowData
              );
            }}
          >
            <Icon size="small">
              {rowData.disabled ? iconNames.toggle_off : iconNames.toggle_on}
            </Icon>
          </IconButton>
          <IconButton
            title={t('table.actions.edit')}
            onClick={(event: any) => {
              event.preventDefault();

              setEnvironmentToEdit(rowData);
            }}
          >
            <Icon size="small">{iconNames.edit}</Icon>
          </IconButton>
        </div>
      );
    }
    default: {
      return <span>{rowData[dataKey]}</span>;
    }
  }
};
