// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import {
  ControlPanelType,
  EnvironmentType,
  UpdateEnvironmentsType,
} from '../Environments.types';
import { useCreateOrEditDialogStyles } from './CreateOrEditDialog.styles';
import { getCreateOrEditDialogFormDefinition } from './CreateOrEditDialog.formDefinition';
import FqdnField from './fields/FqdnField';

type CreateOrEditDialogPropsType = {
  controlPanel: ControlPanelType;
  environment: EnvironmentType | null;
  updateEnvironments: UpdateEnvironmentsType;
  onClose: () => void;
  open: boolean;
};

const CreateOrEditDialog: React.ComponentType<CreateOrEditDialogPropsType> = ({
  controlPanel,
  environment,
  updateEnvironments,
  onClose,
  open,
}) => {
  const [t] = useTranslation('environments');
  const classes = useCreateOrEditDialogStyles();
  const dialogEl = useRef();

  const title = environment
    ? `${environment.label} ${t('common:verbs.edit').toLowerCase()}`
    : t('table.createOrEdit.createEnvironment');
  const formDefinition = getCreateOrEditDialogFormDefinition(
    t,
    controlPanel,
    environment
  );

  let {
    fields,
    formik: { values, isValid },
  } = useForm({
    formDefinition,
    fieldComponents: {
      fqdn: FqdnField,
    },
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'environments-details-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="eye"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.formWrapper}>
            {fields.map(({ FieldComponent, key, type, ...rest }) => {
              const props = cloneWithout(rest, 'mode');

              return (
                <FormControlWrapper
                  {...props}
                  compact={true}
                  key={`${props.name}-formcontrol-wrapper`}
                >
                  <FieldComponent {...props} containerRef={dialogEl.current} />
                </FormControlWrapper>
              );
            })}
          </div>
        </DialogContent>
        <>
          <Divider />
          <DialogActions>
            {createDialogActions(
              [
                {
                  text: environment
                    ? t('common:verbs.edit')
                    : t('common:verbs.create'),
                  disabled: !isValid,
                  onClick: () => {
                    const {
                      label,
                      fqdn,
                      otap,
                      template,
                      password,
                      customerType,
                      softwareVersion,
                    } = values;

                    updateEnvironments(
                      environment ? 'edit' : 'create',
                      {
                        label,
                        fqdn,
                        otap: otap?.value || otap,
                        template: template?.value,
                        password,
                        ...(environment
                          ? {}
                          : {
                              customer_type: customerType?.value,
                              software_version: softwareVersion?.value,
                            }),
                      },
                      environment,
                      onClose
                    );
                  },
                },
                {
                  text: t('common:dialog.cancel'),
                  onClick: onClose,
                },
              ],
              'environments-edit-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default CreateOrEditDialog;
