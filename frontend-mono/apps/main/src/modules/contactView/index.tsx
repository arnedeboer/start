// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import NotificationBar from '../../components/NotificationBar/NotificationBar';
import ContactView from './ContactView';
import locale from './ContactView.locale';
import { useStyles } from './ContactView.styles';
import { SubjectType, TabType } from './ContactView.types';
import { fetchSubject } from './ContactView.requests';
import {
  formatTitle,
  formatSubTitle,
  getNotifications,
} from './ContactView.library';

export type ContactViewParamsType = {
  uuid: string;
  type: SubjectTypeType;
  ['*']: TabType;
};

const ContactViewModule: React.FunctionComponent = () => {
  const classes = useStyles();
  const { uuid, type } = useParams<
    keyof ContactViewParamsType
  >() as ContactViewParamsType;

  const [t] = useTranslation('contactView');

  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [subject, setSubject] = useState<SubjectType>();

  const getSubject = async () => {
    const subj = await fetchSubject(uuid, type).catch(openServerErrorDialog);

    setSubject(subj);
  };

  useEffect(() => {
    getSubject();
  }, []);

  if (!subject) {
    return (
      <>
        {ServerErrorDialog}
        <Loader />
      </>
    );
  }

  return (
    <div className={classes.wrapper}>
      <NotificationBar notifications={getNotifications(t, subject)} />
      <ContactView
        uuid={uuid}
        type={type}
        subject={subject}
        refreshSubject={getSubject}
      />
      <TopbarTitle
        title={formatTitle(subject)}
        description={formatSubTitle(subject)}
      />
    </div>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="contactView">
    <ContactViewModule />
  </I18nResourceBundle>
);
