// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import locale from '../../objectView/ObjectView.locale';
import RelatedSubjects from './../../objectView/components/Relationships/components/RelatedSubjects';
import RelatedObjects from './../../objectView/components/Relationships/components/RelatedObjects';
import { fetchRelationTables } from './Relationship.actions';
import { useRelationshipStyles } from './Relationship.style';

type RelationshipParamsType = {
  uuid: string;
};

export const Relationship: React.FunctionComponent = () => {
  const classes = useRelationshipStyles();
  const { uuid } = useParams<
    keyof RelationshipParamsType
  >() as RelationshipParamsType;

  const { data, isLoading, error } = useQuery<
    Awaited<ReturnType<typeof fetchRelationTables>>,
    V2ServerErrorsType
  >(['relationship', uuid], () => fetchRelationTables(uuid));

  if (error) {
    return <ServerErrorDialog err={error} />;
  } else if (isLoading) {
    return <Loader />;
  } else {
    return (
      <div className={classes.wrapper}>
        <I18nResourceBundle resource={locale} namespace="objectView">
          <RelatedSubjects subjects={data.subjects} />
          <RelatedObjects objects={data.objects} />
        </I18nResourceBundle>
      </div>
    );
  }
};

export default Relationship;
