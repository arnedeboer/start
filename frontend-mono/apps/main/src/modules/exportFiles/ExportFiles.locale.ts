// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    exportFiles: 'Exportbestanden',
    table: {
      name: 'Bestandsnaam',
      expiresAt: 'Verloopt op',
      downloadCount: 'Aantal downloads',
      noResults: 'Er zijn geen exportbestanden.',
    },
  },
};
