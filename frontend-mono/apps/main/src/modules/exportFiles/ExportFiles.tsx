// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { useStyles } from './ExportFiles.styles';
import { getFiles } from './ExportFiles.library';
import { getColumns } from './getColumns';

const ExportFiles: React.FunctionComponent = () => {
  const classes = useStyles();
  const sortableTableStyles = useSortableTableStyles();
  const [t] = useTranslation('exportFiles');
  const [page] = useState<number>(1);
  const [rowsPerPage] = useState<number>(100);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const { data: files, isFetching: loading } = useQuery(
    ['exportFiles', page],
    () => getFiles(page, rowsPerPage),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
      refetchOnWindowFocus: true,
      staleTime: 0,
    }
  );

  const columns = getColumns(t, classes);
  const rows = files;

  return (
    <Sheet>
      <div className={classes.wrapper}>
        {rows ? (
          <div
            className={classNames(classes.table, {
              [classes.tableLoading]: loading,
            })}
          >
            <div
              style={{
                flex: '1 1 auto',
                minWidth: columns.reduce(
                  (acc, { minWidth }) => acc + minWidth + 10,
                  60
                ),
                height: `calc(${rows.length} * 42px + 50px)`,
              }}
            >
              <SortableTable
                styles={{ ...sortableTableStyles, ...classes }}
                rows={rows}
                //@ts-ignore
                columns={columns}
                rowHeight={42}
                loading={false}
                noRowsMessage={t('table.noResults')}
                sortDirectionDefault="DESC"
                sortInternal={false}
                sorting="column"
              />
            </div>
            {loading && (
              <div className={classes.loader}>
                <Loader />
              </div>
            )}
          </div>
        ) : (
          <Loader />
        )}
      </div>
      {ServerErrorDialog}
    </Sheet>
  );
};

export default ExportFiles;
