// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import fecha from 'fecha';

export const getColumns = (t: i18next.TFunction, classes: any) =>
  [
    {
      name: 'name',
      flexGrow: 1,
      width: 200,
      cellRenderer: ({ rowData }: any) => (
        <a
          href={`/api/v2/cm/export/download_export_file?token=${rowData.token}`}
          rel="noreferrer"
          target="_blank"
        >
          {rowData.name}
        </a>
      ),
    },
    {
      name: 'expiresAt',
      width: 1,
      minWidth: 200,
      cellRenderer: ({ rowData }: any) => (
        <span>
          {fecha.format(new Date(rowData.expiresAt), 'DD-MM-YYYY HH:mm')}
        </span>
      ),
    },
    {
      name: 'downloadCount',
      width: 1,
      minWidth: 200,
      cellRenderer: ({ rowData }: any) => (
        <span className={classes.downloadCount}>{rowData.downloadCount}</span>
      ),
    },
  ].map(column => ({
    label: t(`table.${column.name}`),
    disableSort: true,
    minWidth: column.width,
    ...column,
  }));
