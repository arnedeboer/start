// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import ExportFiles from './ExportFiles';
import locale from './ExportFiles.locale';

const ExportFilesModule = () => {
  const [t] = useTranslation('exportFiles');

  return (
    <>
      <ExportFiles />
      <TopbarTitle title={t('exportFiles')} />
    </>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="exportFiles">
    <ExportFilesModule />
  </I18nResourceBundle>
);
