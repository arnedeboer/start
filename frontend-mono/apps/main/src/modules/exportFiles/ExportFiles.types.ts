// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

export type RequestParamsType =
  APICaseManagement.GetExportFileListRequestParams;
export type ResponseBodyType = APICaseManagement.GetExportFileListResponseBody;

export type FileType = {
  uuid: string;
  name: string;
  expiresAt: string;
  downloadCount: number;
};

export type FetchFilesType = (
  params: RequestParamsType
) => Promise<ResponseBodyType>;

export type FormatFilesType = (response: ResponseBodyType) => FileType[];

export type GetFilesType = (
  page: number,
  rowsPerPage: number
) => Promise<FileType[]>;
