// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { fetchFiles } from './ExportFiles.requests';
import { FormatFilesType, GetFilesType } from './ExportFiles.types';

const formatFiles: FormatFilesType = response =>
  response.data.map(
    ({ id, attributes: { name, expires_at, token, downloads } }) => ({
      uuid: id,
      name,
      expiresAt: expires_at,
      token,
      downloadCount: downloads,
    })
  ) || [];

export const getFiles: GetFilesType = async (page, rowsPerPage) => {
  const params = { page, page_size: rowsPerPage };

  const response = await fetchFiles(params);

  const files = formatFiles(response);

  return files;
};
