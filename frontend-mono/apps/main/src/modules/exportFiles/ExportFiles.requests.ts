// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchFilesType,
  RequestParamsType,
  ResponseBodyType,
} from './ExportFiles.types';

export const fetchFiles: FetchFilesType = async params => {
  const url = buildUrl<RequestParamsType>(
    '/api/v2/cm/export/get_exports',
    params
  );

  const response = await request<ResponseBodyType>('GET', url);

  return response;
};
