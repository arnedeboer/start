// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { SideMenuItemType } from '@mintlab/ui/App/Zaaksysteem/SideMenu';

const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
  ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
);
MenuItemLink.displayName = 'MenuItemLink';

type GetItemsType = (
  t: i18next.TFunction,
  view: 'attributes' | 'map' | 'relationships' | 'timeline'
) => SideMenuItemType[];

export const getItems: GetItemsType = (t, view) =>
  [
    {
      id: 'attributes',
      label: t('objectView:sideMenu.attributes'),
      icon: <Icon>{iconNames.list_alt}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'map',
      label: t('objectView:sideMenu.map'),
      icon: <Icon>{iconNames.map}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'relationships',
      label: t('objectView:sideMenu.relationships'),
      icon: <Icon>{iconNames.link}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'timeline',
      label: t('objectView:sideMenu.timeline'),
      icon: <Icon>{iconNames.schedule}</Icon>,
      component: MenuItemLink,
    },
  ].map<SideMenuItemType>(menuItem => ({
    ...menuItem,
    selected: view === menuItem.id,
    href: menuItem.id,
  }));
