// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import { RelatedSubjectRowType } from '../../../ObjectView.types';
import { getSubjectColumns } from '../Relationships.library';

type RelatedSubjectsPropsType = {
  subjects: RelatedSubjectRowType[];
};

const RelatedSubjects: React.FunctionComponent<RelatedSubjectsPropsType> = ({
  subjects,
}) => {
  const [t] = useTranslation('main');

  return subjects ? (
    <div
      style={{
        minHeight: 100,
        height: 25 + 50 + 50 * subjects.length,
      }}
    >
      <Typography variant="h4">
        {t('objectView:relationships.subjects.title')}
      </Typography>
      <SortableTable
        //@ts-ignore
        rows={subjects}
        columns={getSubjectColumns({ t })}
        noRowsMessage={t('common:general.noResults')}
        loading={false}
        rowHeight={50}
        onRowClick={({ rowData }: { rowData: RelatedSubjectRowType }) => {
          window.open(
            //@ts-ignore
            `/main/contact-view/${rowData.subjectType}/${rowData.uuid}`,
            '_blank'
          );
        }}
      />
    </div>
  ) : null;
};

export default RelatedSubjects;
