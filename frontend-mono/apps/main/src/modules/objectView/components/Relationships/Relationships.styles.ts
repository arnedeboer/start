// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useRelationshipsStyles = makeStyles((theme: Theme) => {
  return {
    wrapper: {
      padding: 30,
      height: '100%',
      '&>div': {
        marginBottom: 20,
      },
    },
    mainTitle: {
      marginBottom: 30,
    },
  };
});
