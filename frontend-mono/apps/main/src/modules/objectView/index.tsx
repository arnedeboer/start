// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import locale from './ObjectView.locale';
import ObjectView from './ObjectView';
import { ObjectType, ObjectTypeType } from './ObjectView.types';
import { getObject, getObjectType } from './ObjectView.library';

export type ObjectViewModuleParamsType = {
  uuid: string;
  ['*']: 'attributes' | 'timeline' | 'map' | 'relationships';
};

const ObjectViewModule: React.ComponentType = () => {
  const { uuid } = useParams<
    keyof ObjectViewModuleParamsType
  >() as ObjectViewModuleParamsType;
  const [object, setObject] = useState<ObjectType>();
  const [objectType, setObjectType] = useState<ObjectTypeType>();

  useEffect(() => {
    if (!object) {
      getObject(uuid, setObject);
    }

    if (object && !objectType) {
      getObjectType(object, setObjectType);
    }
  }, [object]);

  if (!object || !objectType) {
    return <Loader />;
  }

  const refreshObject = () => getObject(uuid, setObject);

  return (
    <>
      <ObjectView
        object={object}
        objectType={objectType}
        refreshObject={refreshObject}
      />
      <TopbarTitle title={object.title} />
    </>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="objectView">
    <ObjectViewModule />
  </I18nResourceBundle>
);
