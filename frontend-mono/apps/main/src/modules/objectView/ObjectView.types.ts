// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import { RowType } from '@mintlab/ui/App/Zaaksysteem/SortableTable';

export type CustomFieldType = { type: string; value: any };

export type ObjectType = {
  uuid: string;
  registrationDate?: string;
  lastModified?: string;
  status?: 'active' | 'inactive' | 'draft';
  customFieldsValues: { [key: string]: CustomFieldType };
  title: string;
  objectTypeVersionUuid: string;
  versionIndependentUuid: string;
  relatedCasesUuids?: string[];
  authorizations: ('read' | 'readwrite' | 'admin')[];
};

export type ObjectTypeType = {
  customFieldsDefinition: APICaseManagement.CustomObjectTypeCustomFieldDefinition[];
  catalog_folder_id?: number;
  date_created?: string;
  date_deleted?: string;
  external_reference?: string;
  is_active_version?: boolean;
  last_modified?: string;
  name?: string;
  status?: string;
  title?: string;
  version: number;
  version_independent_uuid?: string;
  authorizations: ('read' | 'readwrite' | 'admin')[];
};

export interface RelatedCaseRowType extends RowType {
  nr: number;
  progress: number;
  status: string;
  extra?: string;
  caseType: string;
  result?: string;
  assignee?: string;
}

export interface RelatedObjectRowType extends RowType {
  summary?: string;
}

export interface RelatedSubjectRowType extends RowType {
  name: string;
  subjectType?: string;
  roleType?: any;
}
