// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route } from 'react-router-dom';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useAdminBanner } from '../../library/auth';
import TransactionsWrapper from './Transactions/TransactionsWrapper';
import Transaction from './Transaction/Transaction';
import locale from './Transactions.locale';

type TransactionsModuleType = {};

const TransactionsModule: React.ComponentType<TransactionsModuleType> = () => {
  const banner = useAdminBanner.system();

  return (
    banner || (
      <>
        <Routes>
          <Route path={'/'} element={<TransactionsWrapper />} />
          <Route path={':transactionUuid'} element={<Transaction />} />
        </Routes>
      </>
    )
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="transactions">
    <TransactionsModule />
  </I18nResourceBundle>
);
