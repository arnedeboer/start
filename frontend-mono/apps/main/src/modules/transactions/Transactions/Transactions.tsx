// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { IntegrationType, FiltersType } from '../Transactions.types';
import { getTransactions } from './Transactions.library';
import ActionBar from './ActionBar/ActionBar';
import DataTable from './DataTable/DataTable';

const useDebounce = (value: FiltersType, delay: number) => {
  const [debouncedValue, setDebouncedValue] = React.useState(value);

  useEffect(() => {
    const handler: NodeJS.Timeout = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
};

type TransactionsPropsType = {
  integrations: IntegrationType[];
};

/* eslint complexity: [2, 7] */
const Transactions: React.ComponentType<TransactionsPropsType> = ({
  integrations,
}) => {
  const [t] = useTranslation('transactions');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const [searchParams] = useSearchParams();
  const { integrationId, withError } = Object.fromEntries([...searchParams]);

  const defaultIntegration = (integrations || []).find(
    integration => integration.value === integrationId
  );

  const [filters, setFilters] = useState<FiltersType>({
    integration: defaultIntegration || integrations[0],
    withError: Boolean(withError) && withError === '1',
    keyword: '',
    page: 0,
    numRows: 20,
  });

  const debouncedFilters = useDebounce(filters, 300);

  const { data, isLoading: loading } = useQuery(
    ['transactions', debouncedFilters],
    () => getTransactions(debouncedFilters),
    {
      onSuccess: () => selectionProps.resetAll(),
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
      refetchOnWindowFocus: true,
      staleTime: 0,
    }
  );

  const selectionProps = useSelectionBehaviour({
    rows: data?.rows,
    selectEverythingTranslations: t('table.selectEverything', {
      returnObjects: true,
    }),
    page: filters.page,
    resultsPerPage: filters.numRows,
  });

  const { selectedRows, everythingSelected, resetAll } = selectionProps;

  const queryClient = useQueryClient();
  const refreshTransactions = () =>
    queryClient.invalidateQueries(['transactions']);

  return (
    <Sheet>
      <ActionBar
        integrations={integrations}
        filters={filters}
        setFilters={setFilters}
        selectedRows={selectedRows}
        everythingSelected={everythingSelected}
        refreshTransactions={refreshTransactions}
        resetAll={resetAll}
      />
      <DataTable
        loading={loading}
        data={data}
        filters={filters}
        setFilters={setFilters}
        selectedRows={selectedRows}
        selectionProps={selectionProps}
      />
      {ServerErrorDialog}
      <TopbarTitle title={t('transactions')} />
    </Sheet>
  );
};

export default Transactions;
