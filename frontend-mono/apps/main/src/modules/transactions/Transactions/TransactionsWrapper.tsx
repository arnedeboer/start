// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { IntegrationType } from '../Transactions.types';
import { getIntegrations, getIntegrationTypes } from './Transactions.library';
import Transactions from './Transactions';

/* eslint complexity: [2, 7] */
const TransactionsWrapper = () => {
  const [t] = useTranslation('transactions');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const { data: integrationTypes } = useQuery(
    ['integrationTypes'],
    getIntegrationTypes,
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  const { data: integrationsData } = useQuery(
    ['integrations', integrationTypes],
    () => getIntegrations(integrationTypes),
    {
      enabled: Boolean(integrationTypes),
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  const everyIntegration: IntegrationType = {
    value: '0',
    manualTypes: ['unsupported'],
    label: t('integration.all'),
  };
  const integrations = [everyIntegration, ...(integrationsData || [])];

  return (
    <>
      {integrationsData ? (
        <Transactions integrations={integrations} />
      ) : (
        <Loader />
      )}
      {ServerErrorDialog}
    </>
  );
};

export default TransactionsWrapper;
