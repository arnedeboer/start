// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { performManualTransaction } from '../Transactions.library';
import { IntegrationType } from '../../Transactions.types';
import {
  getManualDialogFormDefinition,
  getManualDialogRules,
} from './Manual.formdefinition';

type ManualDialogPropsType = {
  integrations: IntegrationType[];
  setSnack: (text: string) => void;
  onClose: () => void;
  open: boolean;
};

const ManualDialog: React.ComponentType<ManualDialogPropsType> = ({
  integrations,
  setSnack,
  onClose,
  open,
}) => {
  const [t] = useTranslation('transactions');
  const dialogEl = useRef(null);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const title = t('manual.dialog.title');
  const formDefinition = getManualDialogFormDefinition(t, integrations);
  const rules = getManualDialogRules();

  let {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
    rules,
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'transactions-manual-dialog'}
        ref={dialogEl}
        fullWidth={true}
      >
        <DialogTitle
          elevated={true}
          icon="settings"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          {fields.map(
            ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
              const props = cloneWithout(rest, 'mode');

              return (
                <FormControlWrapper
                  {...props}
                  label={suppressLabel ? false : props.label}
                  compact={true}
                  key={`${props.name}-formcontrol-wrapper`}
                >
                  <FieldComponent
                    {...props}
                    t={t}
                    containerRef={dialogEl.current}
                  />
                </FormControlWrapper>
              );
            }
          )}
        </DialogContent>
        <>
          <Divider />
          <DialogActions>
            {createDialogActions(
              [
                {
                  text: t('common:verbs.send'),
                  onClick() {
                    performManualTransaction(values)
                      .then(() => {
                        setSnack(t('manual.snack'));
                        onClose();
                      })
                      .catch(openServerErrorDialog);
                  },
                },
                {
                  text: t('common:dialog.cancel'),
                  onClick: onClose,
                },
              ],
              'transactions-manual-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
      {ServerErrorDialog}
    </>
  );
};

export default ManualDialog;
