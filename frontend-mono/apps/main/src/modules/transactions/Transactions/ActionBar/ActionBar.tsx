// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import TextField from '@mintlab/ui/App/Material/TextField';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import Button from '@mintlab/ui/App/Material/Button';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { adminSnackbarPadding } from '../../../../admin.style';
import {
  IntegrationType,
  FiltersType,
  SetFiltersType,
  SelectedRowsType,
  EverythingSelectedType,
  RefreshTransactionsType,
} from '../../Transactions.types';
import { useActionBarStyles } from './ActionBar.styles';
import ActionDialog from './ActionDialog';
import ManualDialog from './ManualDialog';

type ActionBarPropsType = {
  integrations: IntegrationType[];
  filters: FiltersType;
  setFilters: SetFiltersType;
  selectedRows: SelectedRowsType;
  everythingSelected: EverythingSelectedType;
  refreshTransactions: RefreshTransactionsType;
  resetAll: useSelectionBehaviourReturnType['resetAll'];
};

const ActionBar: React.ComponentType<ActionBarPropsType> = ({
  integrations,
  filters,
  setFilters,
  selectedRows,
  everythingSelected,
  refreshTransactions,
  resetAll,
}) => {
  const classes = useActionBarStyles();
  const [t] = useTranslation('transactions');
  const [snack, setSnack] = useState('');
  const [dialogOpen, setDialogOpen] = useState<'manual' | 'action' | null>(
    null
  );

  return (
    <>
      <div className={classes.wrapper}>
        <Button
          name="actionDialog"
          action={() => {
            setDialogOpen('action');
          }}
          variant="contained"
          disabled={!selectedRows.length}
        >
          {t('action.button')}
        </Button>
        <Button
          name="manualTransactionDialog"
          action={() => {
            setDialogOpen('manual');
          }}
          variant="contained"
        >
          {t('manual.button')}
        </Button>
        <div className={classes.integrationTypeWrapper}>
          <Select
            value={filters.integration.value}
            name="data-type"
            isClearable={false}
            onChange={(event: any) => {
              const integration = integrations.find(
                integration => integration.value == event.target.value.value
              );

              if (!integration) return;

              setFilters({ ...filters, integration });
            }}
            placeholder={t('integration.placeholder')}
            choices={integrations}
          />
        </div>
        <div className={classes.integrationTypeWrapper}>
          <TextField
            value={filters.keyword}
            onChange={(event: any) =>
              setFilters({ ...filters, keyword: event.target.value })
            }
            placeholder={t('keyword')}
            closeAction={() => setFilters({ ...filters, keyword: '' })}
          />
        </div>
        <Checkbox
          name="action-bar-filters"
          label={t('withErrors')}
          onChange={() =>
            setFilters({ ...filters, withError: !filters.withError })
          }
          checked={filters.withError}
        />
      </div>
      {dialogOpen === 'action' && (
        <ActionDialog
          filters={filters}
          selectedRows={selectedRows}
          everythingSelected={everythingSelected}
          refreshTransactions={refreshTransactions}
          resetAll={resetAll}
          setSnack={setSnack}
          onClose={() => setDialogOpen(null)}
          open={dialogOpen === 'action'}
        />
      )}
      {dialogOpen === 'manual' && (
        <ManualDialog
          integrations={integrations}
          setSnack={setSnack}
          onClose={() => setDialogOpen(null)}
          open={dialogOpen === 'manual'}
        />
      )}
      <Snackbar
        scope="transactions-actions-snack"
        handleClose={() => setSnack('')}
        message={snack}
        open={Boolean(snack)}
        sx={adminSnackbarPadding}
      />
    </>
  );
};

export default ActionBar;
