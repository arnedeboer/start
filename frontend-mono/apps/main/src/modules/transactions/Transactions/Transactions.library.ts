// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  GetIntegrationType,
  GetIntegrationTypesType,
  TransactionType,
  GetTransactionsType,
  PerformActionActionType,
  PerformManualTransactionType,
} from '../Transactions.types';
import {
  fetchIntegrations,
  fetchIntegrationTypes,
  fetchTransactions,
  performAction,
  postManualTransaction,
} from './Transactions.requests';

export const getIntegrations: GetIntegrationType = async integrationTypes => {
  if (!integrationTypes) return [];

  const response = await fetchIntegrations();

  const integrations = response.map(({ id, module: moduleType, name }: any) => {
    const integrationType = integrationTypes.find(
      type => type.name === moduleType
    );
    const manualTypes = integrationType?.manualTypes.length
      ? integrationType?.manualTypes
      : ['unsupported'];

    return {
      value: id.toString(),
      manualTypes,
      label: `${name} (${id})`,
    };
  });

  return integrations;
};

export const getIntegrationTypes: GetIntegrationTypesType = async () => {
  const integrationTypes = await fetchIntegrationTypes();

  return integrationTypes.map(({ name, manual_type }: any) => ({
    name,
    manualTypes: manual_type,
  }));
};

export const sortSupportedColumnDict = {
  direction: 'me.direction',
  externalId: 'me.external_transaction_id',
  created: 'me.date_created',
};

export const formatTransaction = ({
  id,
  state,
  interface_id,
  result_preview,
  direction,
  external_transaction_id,
  date_next_retry,
  date_created,
  error_count,
  total_count,
  error_fatal,
  input_data,
  error_message,
}: any): TransactionType => ({
  uuid: id,
  name: id,
  status: state,
  interface: interface_id.name,
  record: {
    uuid: result_preview[0]?.id,
    preview: result_preview[0]?.preview_string?.trim() || '',
  },
  direction,
  externalId: external_transaction_id,
  nextAttempt: date_next_retry,
  created: date_created,
  records: total_count || 0,
  errors: error_count || error_fatal || 0,
  errorMessage: error_message,
  inputData: input_data,
});

export const getTransactions: GetTransactionsType = async ({
  keyword,
  integration,
  withError,
  numRows,
  page,
  sortBy,
  sortDirection,
}) => {
  const params = {
    freeform_filter: keyword || undefined,
    interface_id: integration.value === '0' ? undefined : integration.value,
    'records.is_error': withError ? 1 : '',
    zapi_num_rows: numRows,
    zapi_page: page + 1,
    // @ts-ignore
    zapi_order_by: sortBy ? sortSupportedColumnDict[sortBy] : undefined,
    zapi_order_by_direction: sortDirection?.toLowerCase(),
  };

  const response = await fetchTransactions(params);

  return {
    count: response.num_rows,
    rows: response.result.map(formatTransaction),
  };
};

export const performActionAction: PerformActionActionType = async (
  action,
  filters,
  selected,
  everythingSelected
) => {
  const data = {
    freeform_filter: filters.keyword,
    interface_id: Number(filters.integration.value) || undefined,
    'records.is_error': filters.withError ? 1 : '',
    selection_id: selected.map(Number),
    selection_type: everythingSelected ? 'all' : 'subset',
  };

  return await performAction(action, data);
};

/* eslint complexity: [2, 7] */
export const performManualTransaction: PerformManualTransactionType = async (
  values: any
) => {
  const {
    integration: { value, manualTypes },
    text,
    file: files,
    references,
  } = values;

  let data = {
    interface: Number(value),
    ...(manualTypes.includes('text') && text ? { input_data: text } : {}),
    ...(manualTypes.includes('file') && files
      ? {
          input_filestore_uuid: files.map(
            (file: { value: string }) => file.value
          ),
        }
      : {}),
    ...(manualTypes.includes('references') && references
      ? {
          input_references: references.map((reference: { value: string }) => ({
            reference: reference.value,
          })),
        }
      : {}),
  };

  return await postManualTransaction(data);
};
