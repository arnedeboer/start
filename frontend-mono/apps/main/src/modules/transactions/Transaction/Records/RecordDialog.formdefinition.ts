// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { RecordType } from '../../Transactions.types';
import { formatDate } from '../../Transactions.library';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  record: RecordType
) => AnyFormDefinitionField[];

export const getFormDefinition: GetFormDefinitionType = (t, record) =>
  [
    {
      name: 'recordUuid',
      value: record.uuid,
      type: fieldTypes.TEXT,
    },
    {
      name: 'transactionUuid',
      value: record.transactionId,
      type: fieldTypes.TEXT,
    },
    {
      name: 'executed',
      value: formatDate(record.created),
      type: fieldTypes.TEXT,
    },
    {
      name: 'input',
      value: record.input,
      type: fieldTypes.TEXTAREA,
      readOnly: true,
      maxRows: 10,
    },
    {
      name: 'output',
      value: record.output,
      type: fieldTypes.TEXTAREA,
      readOnly: true,
      maxRows: 10,
    },
  ].map(record => ({
    label: t(`record.${record.name}`),
    readOnly: true,
    ...record,
  }));
