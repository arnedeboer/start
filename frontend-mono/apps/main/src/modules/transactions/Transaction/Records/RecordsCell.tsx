// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { RecordType } from '../../Transactions.types';
import { formatDate } from '../../Transactions.library';

type CellRendererType = (
  rowData: {
    dataKey: any;
    rowData: RecordType;
  },
  t: i18next.TFunction,
  classes: any
) => React.ReactElement;

/* eslint complexity: [2, 11] */
export const cellRenderer: CellRendererType = (
  { dataKey, rowData },
  t,
  classes
) => {
  switch (dataKey) {
    case 'recordUuid': {
      return <span>{rowData.uuid}</span>;
    }
    case 'result': {
      return rowData.result ? (
        <Icon size="small" color="error">
          {iconNames.close}
        </Icon>
      ) : (
        <Icon size="small" color="secondary">
          {iconNames.done}
        </Icon>
      );
    }
    case 'executed': {
      return <span>{formatDate(rowData.created)}</span>;
    }
    default: {
      // @ts-ignore
      return <div className={classes.tableCell}>{rowData[dataKey] || '-'}</div>;
    }
  }
};
