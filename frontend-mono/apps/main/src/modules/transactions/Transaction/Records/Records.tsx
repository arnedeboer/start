// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Pagination from '@mintlab/ui/App/Material/Pagination';
import SortableTable, {
  useSortableTableStyles,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { useDataTableStyles, useTableStyles } from './Records.style';
import {
  RecordFiltersType,
  RecordsDataType,
  RecordType,
  SetRecordsFiltersType,
} from './../../Transactions.types';
import { getColumns } from './Records.library';
import RecordDialog from './RecordDialog';

type RecordsPropsType = {
  recordsData: RecordsDataType;
  filters: RecordFiltersType;
  setFilters: SetRecordsFiltersType;
};

const Records: React.ComponentType<RecordsPropsType> = ({
  recordsData,
  filters,
  setFilters,
}) => {
  const classes = useDataTableStyles();
  const tableStyles = useTableStyles();
  const [record, setRecord] = useState<RecordType | null>(null);
  const sortableTableStyles = useSortableTableStyles();
  const [t] = useTranslation('transactions');

  if (!recordsData) {
    return <Loader />;
  }

  const columns = getColumns(t, classes);
  const rows = recordsData.rows;

  return (
    <div
      className={classes.wrapper}
      style={{
        minHeight: `calc(${rows.length} * 42px + 50px + 45px + 42px)`,
      }}
    >
      <div
        style={{
          flex: '1 1 auto',
        }}
      >
        <SortableTable
          styles={{ ...sortableTableStyles, ...tableStyles }}
          rows={rows}
          //@ts-ignore
          columns={columns}
          rowHeight={42}
          loading={false}
          noRowsMessage={t('table.noResults')}
          onRowClick={({ rowData }: { rowData: RecordType }) =>
            setRecord(rowData)
          }
        />
        {record && (
          <RecordDialog
            record={record}
            onClose={() => setRecord(null)}
            open={Boolean(record)}
          />
        )}
      </div>
      <div className={classes.pagination}>
        <Pagination
          scope="transactions-records-pagination"
          component={'div'}
          count={recordsData.count}
          labelRowsPerPage={`${t('table.labelRowsPerPage')}:`}
          rowsPerPageOptions={[5, 10, 20, 50]}
          rowsPerPage={filters.numRows}
          changeRowsPerPage={(event: any) =>
            setFilters({
              page: 0,
              numRows: event.target.value,
            })
          }
          page={filters.page}
          getNewPage={(page: number) => setFilters({ ...filters, page })}
          labelDisplayedRows={({ from, to, count }: any) =>
            `${from}-${to} ${t('common:of')} ${count}`
          }
        />
      </div>
    </div>
  );
};

export default Records;
