// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { RecordFiltersType } from './../Transactions.types';
import { getTransaction, getRecords } from './Transaction.library';
import Form from './Form/Form';
import Records from './Records/Records';

type TransactionParamsType = {
  transactionUuid: string;
};

const Transaction: React.ComponentType = () => {
  const [t] = useTranslation('transactions');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const { transactionUuid } = useParams<
    keyof TransactionParamsType
  >() as TransactionParamsType;

  const [filters, setFilters] = useState<RecordFiltersType>({
    page: 0,
    numRows: 20,
  });

  const { data: transaction } = useQuery(
    ['transaction', transactionUuid],
    () => getTransaction(transactionUuid),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  const { data: recordsData } = useQuery(
    ['records', transactionUuid, filters],
    () => getRecords(transactionUuid, filters),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  if (!transaction || !recordsData) {
    return <Loader />;
  }

  return (
    <Sheet>
      <Form transaction={transaction} />
      <Records
        recordsData={recordsData}
        filters={filters}
        setFilters={setFilters}
      />
      {ServerErrorDialog}
      <TopbarTitle
        breadcrumbs={[
          { label: t('transactions'), path: '/main/transactions' },
          { label: transaction.interface },
        ]}
      />
    </Sheet>
  );
};

export default Transaction;
