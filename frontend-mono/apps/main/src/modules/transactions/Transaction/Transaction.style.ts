// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTransactionsStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    formWrapper: {
      padding: '20px 20px 0 20px',
      borderBottom: `1px solid ${greyscale.light}`,
    },
  })
);
