// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchTransactionType,
  FetchRecordsType,
} from './../Transactions.types';

export const fetchTransaction: FetchTransactionType = async transactionUuid => {
  const url = `/sysin/transaction/${transactionUuid}`;

  const response = await request('GET', url);

  return response.result[0];
};

export const fetchRecords: FetchRecordsType = async (
  transactionUuid,
  params
) => {
  const url = buildUrl(`/sysin/transaction/${transactionUuid}/records`, params);

  const response = await request('GET', url);

  return response;
};
