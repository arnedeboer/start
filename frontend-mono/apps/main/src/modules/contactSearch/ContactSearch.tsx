// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { useStyles } from './ContactSearch.styles';
import { ContactSearchSideMenu } from './ContactSearchSideMenu';
import { ContactType, ResultType, SearchType } from './ContactSearch.types';
import { performSearch } from './ContactSearch.library';
import SearchForm from './components/SearchForm';
import ResultsTable from './components/ResultsTable';

export interface ContactSearchPropsType {}

export type ContactSearchParamsType = {
  type: ContactType;
};

/* eslint complexity: [2, 8] */
const ContactSearch: React.FunctionComponent<ContactSearchPropsType> = () => {
  const { type } = useParams<
    keyof ContactSearchParamsType
  >() as ContactSearchParamsType;
  const classes = useStyles();
  const session = useSession();
  const countryCode = session.account.instance.country_code;
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [results, setResults] = useState<ResultType[]>();

  useEffect(() => {
    setResults(undefined);
  }, [type]);

  const search: SearchType = values => {
    performSearch(type, values, countryCode)
      .then((results: ResultType[]) => {
        setResults(results);
      })
      .catch(openServerErrorDialog);
  };

  return (
    <PanelLayout>
      <Panel type="side">
        <ContactSearchSideMenu type={type} />
      </Panel>
      <Panel key={type} className={classes.view}>
        {results ? (
          <ResultsTable type={type} results={results} setResults={setResults} />
        ) : (
          <SearchForm type={type} search={search} countryCode={countryCode} />
        )}
      </Panel>
      {ServerErrorDialog}
    </PanelLayout>
  );
};

export default ContactSearch;
