// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import ContactSearch from './ContactSearch';
import locale from './ContactSearch.locale';

const ContactSearchModule = () => {
  const [t] = useTranslation('contactSearch');

  return (
    <>
      <Routes>
        <Route path="" element={<Navigate to="person" replace={true} />} />
        <Route path={':type'} element={<ContactSearch />} />
      </Routes>
      <TopbarTitle title={t('contactSearch')} />
    </>
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="contactSearch">
    <ContactSearchModule />
  </I18nResourceBundle>
);
