// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  searchContactV1,
  searchContactV0,
  fetchDepartments,
} from './ContactSearch.requests';
import {
  FormatResultType,
  PerformSearchType,
  SearchEmployeeType,
  SearchOrganizationType,
  SearchPersonType,
} from './ContactSearch.types';

const formatValue = (key: string, value: any): string => {
  switch (key) {
    case 'zipcode': {
      return value.toUpperCase();
    }
    case 'city':
    case 'houseNumberLetter': {
      return value.toLowerCase();
    }
    default: {
      return value;
    }
  }
};

// PERSON

/* eslint complexity: [2, 8] */
const formatAddress = (
  residence: any,
  correspondence: any,
  countryCode?: string
): string => {
  if (!residence && !correspondence) return '';

  const leadingAddress = correspondence || residence;
  const isDomesticAddress =
    leadingAddress.instance.country.instance.dutch_code === countryCode;

  if (isDomesticAddress) {
    const {
      instance: {
        street,
        street_number: number,
        street_number_letter,
        street_number_suffix,
        zipcode: zip_code,
        city,
      },
    } = leadingAddress;

    const letter = street_number_letter || '';
    const suffix = street_number_suffix || '';
    const zipcode = zip_code ? ` ${zip_code}` : '';

    return `${street}\xA0${number}${letter}${suffix}${zipcode}  ${city}`;
  } else {
    const {
      instance: {
        foreign_address_line1,
        foreign_address_line2,
        foreign_address_line3,
      },
    } = leadingAddress;

    return [foreign_address_line1, foreign_address_line2, foreign_address_line3]
      .filter(Boolean)
      .join(', ');
  }
};

const formatPerson: FormatResultType = (
  {
    reference,
    instance: {
      display_name,
      external_subscription,
      subject: {
        instance: { date_of_birth, address_residence, address_correspondence },
      },
    },
  },
  countryCode
) => ({
  uuid: reference,
  name: display_name,
  address: formatAddress(
    address_residence,
    address_correspondence,
    countryCode
  ),
  dateOfBirth: date_of_birth,
  externalSubscription: Boolean(external_subscription),
});

const dataKeyPersonDict = {
  bsn: 'subject.personal_number',
  sedulaNumber: 'subject.persoonsnummer',
  dateOfBirth: 'subject.date_of_birth',
  prefix: 'subject.prefix',
  familyName: 'subject.family_name',
  zipcode: 'subject.address_residence.zipcode',
  houseNumber: 'subject.address_residence.street_number',
};

const formatPersonData = (values: any) =>
  Object.entries(values).reduce(
    (acc, [key, value]) => ({
      ...acc,
      // @ts-ignore
      ...(value ? { [dataKeyPersonDict[key]]: formatValue(key, value) } : {}),
    }),
    {}
  );

const searchPerson: SearchPersonType = async (values, countryCode) => {
  const data = {
    query: {
      match: {
        subject_type: 'person',
        ...formatPersonData(values),
      },
    },
  };
  const results = await searchContactV1(data);

  return results.map((person: any) => formatPerson(person, countryCode));
};

// ORGANIZATION

const formatOrganization: FormatResultType = (
  {
    reference,
    instance: {
      display_name,
      external_subscription,
      subject: {
        instance: { coc_number, address_residence, address_correspondence },
      },
    },
  },
  countryCode
) => ({
  uuid: reference,
  coc: coc_number,
  name: display_name,
  address: formatAddress(
    address_residence,
    address_correspondence,
    countryCode
  ),
  externalSubscription: Boolean(external_subscription),
});

const dataKeyOrganizationDict = {
  rsin: 'subject.rsin',
  coc: 'subject.coc_number',
  cocLocation: 'subject.coc_location_number',
  name: 'subject.company',
  street: 'subject.address_residence.street',
  houseNumber: 'subject.address_residence.street_number',
  houseNumberLetter: 'subject.address_residence.street_number_letter',
  houseNumberSuffix: 'subject.address_residence.street_number_suffix',
  zipcode: 'subject.address_residence.zipcode',
  city: 'subject.address_residence.city',
};

export const formatOrganizationData = (values: any) =>
  Object.entries(values).reduce((acc, [key, value]) => {
    return {
      ...acc,
      ...(value
        ? // @ts-ignore
          { [dataKeyOrganizationDict[key]]: formatValue(key, value) }
        : {}),
    };
  }, {});

const searchOrganization: SearchOrganizationType = async (
  values,
  countryCode
) => {
  const data = {
    query: {
      match: {
        subject_type: 'company',
        ...formatOrganizationData(values),
      },
    },
  };
  const results = await searchContactV1(data);

  return results.map((person: any) => formatOrganization(person, countryCode));
};

// EMPLOYEE

const formatEmployee = (employee: any, departments: any[]) => {
  const {
    label,
    object: { uuid, route },
  } = employee;
  const department = departments.find(
    ({ instance: { group_id } }) => group_id === route
  );

  return {
    uuid,
    name: label,
    department: department?.preview,
    phoneNumber: null,
  };
};

const searchEmployee: SearchEmployeeType = async ({ inactive, query }) => {
  const data = {
    active: 1,
    inactive: inactive ? 1 : 0,
    query,
  };
  const results = await searchContactV0(data);
  const departments = await fetchDepartments();

  return results.map((employee: any) => formatEmployee(employee, departments));
};

// GENERAL

const searchTypes = {
  person: searchPerson,
  organization: searchOrganization,
  employee: searchEmployee,
};

export const performSearch: PerformSearchType = async (
  type,
  values,
  countryCode
) => {
  const search = searchTypes[type];

  return await search(values, countryCode);
};
