// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { CaseTypeType } from '@zaaksysteem/common/src/components/form/fields/CaseTypeFinder/CaseTypeFinder.library';

export type FavCaseTypeType = {
  id: string;
  name: string;
  reference: string;
};

export const fetchFavoriteCasetypes = async () => {
  const result = await request('GET', '/api/v1/dashboard/favourite/casetype');

  return result.result.instance.rows.map((row: any) => ({
    id: row.instance.id,
    reference: row.instance.reference_id,
    name: row.instance.label,
  }));
};

export const addCasetypeToFavorites = (casetype: CaseTypeType) => {
  return request('POST', '/api/v1/dashboard/favourite/casetype/create', {
    reference_id: casetype.id,
  });
};

export const removeCasetypeFromFavorites = (casetype: FavCaseTypeType) => {
  return request(
    'POST',
    `https://development.zaaksysteem.nl/api/v1/dashboard/favourite/casetype/${casetype.id}/delete`
  );
};
