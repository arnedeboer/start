// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SortDirectionType } from 'react-virtualized';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { ItemValueType } from '@zaaksysteem/common/src/components/form/fields/Options/Options.types';
import { RowType } from '@mintlab/ui/App/Zaaksysteem/SortableTable';
import { useTasksStyles } from '../Tasks.style';

export type ClassesType = ReturnType<typeof useTasksStyles>;

export type Types =
  | 'assignee'
  | 'keyword'
  | 'case_number'
  | 'case_type'
  | 'department';

export type TasksPropsType = {
  onClose?: () => {};
  onParamsChange?: (params: any) => {};
  filters?: FilterType[];
  columns?: ItemValueType[];
};

export type TasksParamsType = {
  widgetUuid: string;
};

export type CasesRowType = {
  display_number: number;
  due_date: string | undefined;
  days_left: number | undefined;
};

export interface TasksRowType extends RowType {
  display_number?: number;
  display_name?: string;
  title: string;
  due_date: string;
}

export type rowType = APICaseManagement.GetTaskListResponseBody['data'];

export interface FilterDataType {
  type: Types;
}

export type FilterType = ValueType<string, FilterDataType>;

export type TasksFormDefinitionType = {
  assignee: FilterType;
  case_number: FilterType;
  keyword: FilterType;
  department: FilterType;
  case_type: FilterType;
};

export type FormValuesType = {
  [T in Types]: FilterType[];
};

export type GetDataParamsType = {
  pageLength: number;
  searchTerm: string;
  filters: null | FilterType[];
  sortBy: string;
  sortDirection: SortDirectionType;
  openServerErrorDialog: OpenServerErrorDialogType;
};
