// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable id-length */

import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { DashboardState, Layout, Widget } from './Dashboard.types';
import { widgetTypes } from './widgets/Widget.library';

const breakpoints = { lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 };
const columns = { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 };

export const getOccupiedPoints = (wid: Layout) =>
  Array(wid.w)
    .fill(null)
    .map((_, xIx) =>
      Array(wid.h)
        .fill(null)
        .map((_, yIx) => [xIx + wid.x, yIx + wid.y].toString())
    )
    .flat();

export const fitToLayout = (
  layout: Layout[],
  cols: number,
  hWI: { h: number; w: number; i: string }
): Layout => {
  const allOccupiedPoints = layout.map(getOccupiedPoints).flat();

  const allLocations = Array(100)
    .fill(null)
    .map((_, yIx) =>
      Array(cols)
        .fill(null)
        .map((_, xIx) => [xIx, yIx] as const)
    )
    .flat(1);

  const [xIx, yIx] = allLocations.find(loc =>
    getOccupiedPoints({
      i: '',
      x: loc[0],
      y: loc[1],
      h: hWI.h,
      w: hWI.w,
    }).every(
      point =>
        !allOccupiedPoints.includes(point) && Number(point.split(',')[0]) < cols
    )
  ) || [0, 0];

  return {
    ...hWI,
    x: xIx,
    y: yIx,
  };
};

export const getCurrentCols = () => {
  const w =
    //@ts-ignore
    document.querySelector('.react-grid-layout')?.offsetWidth ||
    window.innerWidth;

  return columns[
    w <= breakpoints.xs
      ? 'xxs'
      : w <= breakpoints.sm
      ? 'xs'
      : w <= breakpoints.md
      ? 'sm'
      : w <= breakpoints.lg
      ? 'md'
      : 'lg'
  ];
};

export const fetchDashboard = async () => {
  const result = await request<APICaseManagement.GetDashboardResponseBody>(
    'GET',
    '/api/v2/cm/dashboard/get_dashboard'
  );

  const widgets = result.data.attributes.widgets || [];

  return {
    components: widgets.map(widget => ({
      params: widget.parameters,
      component: widgetTypes[widget.parameters.type].component,
    })),
    layout: widgets.map((widget, ix) => ({
      i: ix.toString(),
      x: widget.dimensions.x,
      y: widget.dimensions.y,
      h: widget.dimensions.height,
      w: widget.dimensions.width,
      minH: widgetTypes[widget.parameters.type].gridDefaults.minH,
      minW: widgetTypes[widget.parameters.type].gridDefaults.minW,
    })),
    widgets,
  } as DashboardState;
};

export const updateDashboardWidgets = (uuid: string, widgets: Widget[]) => {
  const updatedWidgets = widgets.map(widget => ({
    ...widget,
    parameters: {
      ...widget.parameters,
      ...(widget.parameters.type === 'tasks'
        ? {
            columns: widget.parameters.columns.map(column => ({
              ...column,
              is_new: column.isNew || false,
            })),
          }
        : {}),
    },
  }));

  return request<APICaseManagement.UpdateDashboardResponseBody>(
    'POST',
    '/api/v2/cm/dashboard/update_dashboard',
    { uuid, widgets: updatedWidgets }
  );
};
