// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    caseCreate: 'Nieuwe zaak registreren',
    addToCase: 'Toevoegen aan zaak',
    assign: 'Toewijzen',
    delete: 'Verwijderen',
    properties: 'Eigenschappen',
    reject: 'Afwijzen',
  },
};

export default locale;
