// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import { DocumentItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './locale';

type MenuPropsType = {
  list: DocumentItemType[];
  actions: {
    addToCase: () => void;
    delete: () => void;
    properties: () => void;
    caseCreate: () => void;
    assign: () => void;
    reject: () => void;
  };
};

/* eslint complexity: [2, 14] */
const Menu = ({ list, actions }: MenuPropsType) => {
  const [t] = useTranslation('IntakeMenu');
  const selectedItems = list.filter(item => item.selected);
  const selectedSingleItem = selectedItems.length === 1;
  const hasSelectedItems = selectedItems.length > 0;
  const selectedOnlyDocuments = selectedItems.every(
    item => item.type === 'document'
  );
  const isAssigned = Object.values(
    (selectedItems[0]?.type === 'document' && selectedItems[0]?.assignment) ||
      {}
  ).some(obj => obj?.name !== null);

  const buttons = {
    caseCreate: selectedSingleItem && selectedOnlyDocuments && (
      <Tooltip key="caseCreate" title={t('caseCreate')}>
        <Button
          variant="default"
          name="intakeCaseCreate"
          action={actions.caseCreate}
        >
          {t('IntakeMenu:caseCreate')}
        </Button>
      </Tooltip>
    ),
    addToCase: hasSelectedItems && selectedOnlyDocuments && (
      <Tooltip key="addToCase" title={t('addToCase')}>
        <Button
          variant="default"
          name="intakeAddToCase"
          action={actions.addToCase}
        >
          {t('IntakeMenu:addToCase')}
        </Button>
      </Tooltip>
    ),
    assign: hasSelectedItems && (
      <Tooltip key="assign" title={t('assign')}>
        <Button variant="default" name="intakeAssign" action={actions.assign}>
          {t('IntakeMenu:assign')}
        </Button>
      </Tooltip>
    ),
    reject: selectedSingleItem && isAssigned && (
      <Tooltip key="reject" title={t('reject')}>
        <Button variant="default" name="intakeReject" action={actions.reject}>
          {t('IntakeMenu:reject')}
        </Button>
      </Tooltip>
    ),
    delete: hasSelectedItems && (
      <Tooltip key="delete" title={t('delete')}>
        <IconButton onClick={actions.delete} color="inherit">
          <Icon size="small">{iconNames.delete}</Icon>
        </IconButton>
      </Tooltip>
    ),
    properties: selectedSingleItem && (
      <Tooltip key="properties" title={t('properties')}>
        <IconButton onClick={actions.properties} color="inherit">
          <Icon size="small">{iconNames.info_outlined}</Icon>
        </IconButton>
      </Tooltip>
    ),
  };

  return <div style={{ display: 'flex' }}>{Object.values(buttons)}</div>;
};

/* eslint-disable-next-line */
export default (props: MenuPropsType) => (
  <I18nResourceBundle resource={locale} namespace="IntakeMenu">
    <Menu {...props} />
  </I18nResourceBundle>
);
