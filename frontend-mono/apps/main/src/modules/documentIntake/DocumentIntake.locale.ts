// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    title: 'Documentintake',
    linkToV1: 'Documentintake\u00A0v1',
    by: 'door',
    delete: {
      title: 'Verwijderen document',
      body: 'Weet u zeker dat u de geselecteerde document(en) wilt verwijderen? <br/> </br> Dit kan niet ongedaan worden gemaakt.',
      reason: 'Document verwijderd uit documentintake',
    },
    addToCase: {
      title: 'Toevoegen aan lopende zaak',
      name: {
        label: 'Document',
        labelMultiple: 'Documenten',
        placeholder: 'Bestandsnaam',
        hint: 'Voer de naam van het document in. Voeg geen bestandsextensie aan het einde toe.',
      },
      origin: {
        label: 'Richting',
        choices: {
          inkomend: 'Inkomend',
          uitgaand: 'Uitgaand',
          intern: 'Intern',
        },
      },
      description: {
        placeholder: 'Omschrijving',
        label: 'Omschrijving',
      },
      case_uuid: {
        label: 'Zaak',
        placeholder: 'Selecteer zaak…',
      },
      document_label_uuids: {
        label: 'Zaakdocument',
        placeholder: 'Selecteer zaakdocument',
        noOptions: 'Geen zaakdocumenten',
      },
    },
    properties: {
      title: 'Documenteigenschappen',
      name: {
        label: 'Documentnaam (zonder bestandsextensie)',
        hint: 'Voer de naam van het document in. Voeg geen bestandsextensie aan het einde toe.',
      },
      description: {
        label: 'Omschrijving',
      },
      category: {
        label: 'Documentcategorie',
      },
      confidentiality: {
        label: 'Vertrouwelijkheid',
      },
      origin: {
        label: 'Richting',
      },
      additional: {
        label: 'Overige eigenschappen',
        integrity: {
          label: 'Bestandsintegriteit',
          ok: 'Bestand geverifieerd',
          notOk: 'Bestand kon niet geverifieerd worden.',
        },
        version: 'Versie',
        mimetype: 'Soort',
        filesize: 'Bestandsgrootte',
        md5: 'MD-5 Hash',
        documentNumber: 'Documentnummer',
        date: {
          label: 'Datum',
          placeholder: 'Kies een datum…',
          outgoingDate: 'Verzenddatum',
          incomingDate: 'Ontvangstdatum',
        },
      },
    },
    assignDocument: {
      title: 'Document(en) toewijzen',
      contact: {
        user: 'Behandelaar',
        role: 'Afdeling',
        label: 'Behandelaar',
        placeholder: 'Zoek een behandelaar…',
      },
      type: 'Toewijzen aan',
      group: {
        label: 'Afdeling',
        placeholder: 'Kies een afdeling…',
      },
      role: {
        label: 'Rol',
        placeholder: 'Kies een rol…',
      },
    },
    rejectDocument: {
      title: 'Document afwijzen',
      reason: 'Reden',
      placeholder: 'Vul een reden in…',
    },
    previewPanel:
      'Selecteer een document. Als het een voorbeeld bevat, zal deze in dit paneel worden geopend.',
    assignment: {
      all: 'Alles',
      assigned: 'Toegewezen',
      assignedToMe: 'Toegewezen aan mij',
      unassigned: 'Niet toegewezen',
    },
    loadingMessage: 'Bezig met laden van de intake…',
    noRowsMessage: 'Geen documenten in de intake.',
  },
};

export default locale;
