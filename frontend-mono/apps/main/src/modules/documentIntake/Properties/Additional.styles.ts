// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAdditionalStyles = makeStyles(({ typography }: Theme) => {
  return {
    wrapper: {
      width: '100%',
      marginTop: 10,
    },
    row: {
      display: 'flex',
      '& > :nth-child(1)': {
        fontWeight: typography.fontWeightBold,
        width: 175,
      },
      '& > :nth-child(2)': {
        flex: 1,
        overflow: 'hidden',
      },
      marginBottom: 8,
    },
  };
});
