// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import {
  DOCUMENT_CATEGORIES,
  DOCUMENT_CONFIDENTIALITY,
  DOCUMENT_ORIGIN,
} from '@zaaksysteem/common/src/constants/document';
import { APIDocument } from '@zaaksysteem/generated';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';

type GetFormDefinitionPropsType = {
  t: i18next.TFunction;
  uuid: string;
};

export type FormDefinitionFieldsType = {
  basename: string;
  description: string;
  category: string;
  confidentiality: string;
  origin: string;
  date: string;
  additional: { [key: string]: any };
};

const strToChoice = (str: string) => ({ label: str, value: str });

/* eslint complexity: [2, 11] */
export const getFormDefinition = async ({
  t,
  uuid,
}: GetFormDefinitionPropsType): Promise<
  FormDefinition<FormDefinitionFieldsType>
> => {
  const response = await request<APIDocument.GetDocumentResponseBody>(
    'GET',
    buildUrl('/api/v2/document/get_document', {
      document_uuid: uuid,
    })
  );

  if (!response || !response.data) return [];

  const {
    attributes: {
      basename,
      mimetype,
      filesize,
      md5,
      current_version,
      security: { virus_status },
      metadata,
      extension,
    },
    meta: { document_number },
  } = response.data;

  if (!metadata) return [];

  const {
    description,
    document_category,
    origin,
    confidentiality,
    origin_date,
  } = metadata;

  return [
    {
      name: 'basename',
      type: 'SuffixTextField',
      label: t('documentIntake:properties.name.label'),
      value: basename || '',
      required: true,
      hint: t('documentIntake:properties.name.hint'),
      config: {
        suffix: extension,
      },
    },
    {
      name: 'description',
      type: fieldTypes.TEXTAREA,
      label: t('documentIntake:properties.description.label'),
      value: description || '',
      required: false,
      isMultiline: true,
      rows: 7,
    },
    {
      name: 'category',
      type: fieldTypes.SELECT,
      label: t('documentIntake:properties.category.label'),
      value: document_category || null,
      required: false,
      choices: DOCUMENT_CATEGORIES.map(strToChoice),
      placeholder: t('Common:forms.choose'),
    },
    {
      name: 'confidentiality',
      type: fieldTypes.SELECT,
      label: t('documentIntake:properties.confidentiality.label'),
      value: confidentiality || null,
      required: false,
      choices: DOCUMENT_CONFIDENTIALITY.map(strToChoice),
      placeholder: t('Common:forms.choose'),
    },
    {
      name: 'origin',
      type: fieldTypes.SELECT,
      label: t('documentIntake:properties.origin.label'),
      value: origin || null,
      required: false,
      choices: DOCUMENT_ORIGIN.map(strToChoice),
      placeholder: t('Common:forms.choose'),
    },
    {
      name: 'date',
      type: fieldTypes.DATEPICKER,
      value: origin_date || null,
      required: false,
      variant: 'inline',
      placeholder: t('documentIntake:properties.additional.date.placeholder'),
    },
    {
      name: 'additional',
      type: 'additional',
      label: t('documentIntake:properties.additional.label'),
      //@ts-ignore
      value: {
        document_number,
        version: current_version,
        mimetype,
        filesize,
        md5,
        status:
          virus_status === 'ok'
            ? t('documentIntake:properties.additional.integrity.ok')
            : t('documentIntake:properties.additional.integrity.notOk'),
      },
    },
  ] as FormDefinition<FormDefinitionFieldsType>;
};
