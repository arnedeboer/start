// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import CommunicationModule from '@zaaksysteem/communication-module/src';

const CustomerContact = () => (
  <CommunicationModule
    capabilities={{
      allowSplitScreen: true,
      canAddAttachmentToCase: false,
      canAddSourceFileToCase: false,
      canAddThreadToCase: true,
      canCreateContactMoment: false,
      canCreatePipMessage: false,
      canCreateEmail: false,
      canCreateNote: false,
      canDeleteMessage: true,
      canImportMessage: false,
      canSelectCase: false,
      canSelectContact: false,
      canFilter: true,
      canOpenPDFPreview: true,
    }}
    context="inbox"
  />
);

export default CustomerContact;
