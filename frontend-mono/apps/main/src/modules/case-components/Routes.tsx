// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Relations from '../case/views/relations';
import Timeline from '../case/views/timeline';
import Communication from '../case/views/communication';
import ObjectForm from '../case/views/phases/Dialogs/ObjectForm';
import { CaseObjType, CaseTypeType } from '../case/Case.types';
import TasksWrapper from './TasksWrapper';
import { refreshTasks } from './library';

export interface RoutesPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
}

const CaseComponentRoutes: React.ComponentType<RoutesPropsType> = ({
  caseObj,
  caseType,
}) => {
  return (
    <Routes>
      <Route
        path={`communication/*`}
        element={<Communication caseObj={caseObj} caseType={caseType} />}
      />
      <Route
        path={`phase/:phaseNumber/tasks/*`}
        element={
          <TasksWrapper
            externalTaskAssignment={
              caseType.settings.allow_external_task_assignment
            }
            caseObj={caseObj}
            setOpenTasksCount={refreshTasks}
            iframe={true}
          />
        }
      />
      <Route
        path={`relations`}
        element={<Relations caseObj={caseObj} caseType={caseType} />}
      />
      <Route path={`object/:type/*`}>
        <Route
          path=""
          element={
            <ObjectForm
              // we don't pass the caseObj here
              // because it needs to be up to date to prefill values
              // and the case can have changed in the mean time
              caseUuid={caseObj.uuid}
              caseType={caseType}
            />
          }
        />
        <Route
          path=":objectUuid"
          element={<ObjectForm caseUuid={caseObj.uuid} caseType={caseType} />}
        />
      </Route>
      <Route path={`timeline`} element={<Timeline caseObj={caseObj} />} />
    </Routes>
  );
};

export default CaseComponentRoutes;
