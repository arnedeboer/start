// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAttributeGroupStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      width: '100%',
      marginBottom: 20,
      padding: 0,
      '& > div': {
        borderBottom: `1px solid ${greyscale.darker}`,
        padding: '1em 0.7em',
      },
    },
  })
);
