// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { useAttributeGroupStyles } from './Attributes.style';

export type AttributeGroupPropsType = {
  formDefinition: AnyFormDefinitionField[];
  styles?: ReturnType<typeof useAttributeGroupStyles>;
};

export const Attributes: React.FunctionComponent<AttributeGroupPropsType> = ({
  formDefinition,
  styles,
}) => {
  const AttributeGroupStyles = useAttributeGroupStyles();
  const classes = styles || AttributeGroupStyles;
  const {
    fields,
    formik: { values },
  } = useForm({ formDefinition });

  return (
    <div className={classes.wrapper}>
      {fields.map(
        ({ FieldComponent, name, error, touched, value, ...rest }) => {
          const restValues = {
            ...cloneWithout(rest, 'type', 'classes'),
            disabled: values.completed,
          };

          return (
            <FormControlWrapper
              {...restValues}
              error={error}
              touched={touched}
              key={name}
            >
              <FieldComponent
                name={name}
                value={value}
                key={name}
                {...restValues}
              />
            </FormControlWrapper>
          );
        }
      )}
    </div>
  );
};
