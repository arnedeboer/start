// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import { normalizeValue } from '@zaaksysteem/common/src/library/normalizeValue';
import { APICaseManagement } from '@zaaksysteem/generated';
import { CustomFieldTypeType } from '@zaaksysteem/common/src/types/CustomFields';
import {
  fetchObjectType,
  submitObjectType,
} from './ObjectTypeManagement.requests';
import {
  FormatObjectType,
  GetObjectTypeType,
  SaveObjectTypeType,
  FormatDataType,
  CreateInitialValuesFromObjectType,
  MapCustomFieldsType,
  MapAuthorizationsType,
  MapRelationshipsType,
} from './ObjectTypeManagement.types';

const mapCustomFields: MapCustomFieldsType = (custom_field_definition: any) => {
  if (!custom_field_definition.custom_fields) return custom_field_definition;

  return {
    ...custom_field_definition,
    custom_fields: custom_field_definition.custom_fields.map(
      (thisField: any) => ({
        ...thisField,
        id: thisField.attribute_uuid,
      })
    ),
  };
};

const mapAuthorizations: MapAuthorizationsType = authorizations => {
  if (!authorizations) return [];

  return authorizations.map(thisAuthorization => {
    return {
      department: thisAuthorization.department?.uuid || null,
      role: thisAuthorization.role.uuid,
      permission: thisAuthorization.authorization,
    };
  });
};

const mapRelationships: MapRelationshipsType = (
  relationships: APICaseManagement.CustomObjectTypeRelationshipDefinition
) => {
  const objectTypeRelations: any[] = [];
  const caseTypeRelations: any[] = [];

  relationships?.relationships?.forEach(rel => {
    const { custom_object_type_uuid, case_type_uuid, name } = rel;
    if (custom_object_type_uuid) {
      objectTypeRelations.push({ id: custom_object_type_uuid, name });
    } else {
      caseTypeRelations.push({ id: case_type_uuid, name });
    }
  });

  return { objectTypeRelations, caseTypeRelations };
};

const formatObjectType: FormatObjectType = response => {
  const {
    id,
    attributes: {
      title,
      subtitle,
      status,
      name,
      custom_field_definition,
      authorization_definition,
      relationship_definition,
      external_reference,
    },
  } = response.data;

  return {
    uuid: id,
    title,
    subtitle,
    status,
    name,
    ...(custom_field_definition && {
      custom_field_definition: mapCustomFields(custom_field_definition),
    }),
    authorizations: mapAuthorizations(authorization_definition?.authorizations),
    external_reference: external_reference || '',
    ...mapRelationships(relationship_definition || []),
  };
};

export const getObjectType: GetObjectTypeType = async uuid => {
  const response = await fetchObjectType(uuid);

  const objectType = formatObjectType(response);

  return objectType;
};

export const createInitialValuesFromObjectType: CreateInitialValuesFromObjectType =
  ({
    name,
    title,
    subtitle,
    custom_field_definition,
    audit_log,
    external_reference,
    objectTypeRelations,
    caseTypeRelations,
    authorizations,
  }) => {
    return {
      name,
      title,
      subtitle,
      custom_fields: custom_field_definition?.custom_fields.map(
        (customField: any) => ({
          ...(customField?.custom_field_specification || {}),
          ...customField,
        })
      ),
      components_changed: audit_log?.updated_components,
      changes: audit_log?.description,
      external_reference,
      objectTypeRelations,
      caseTypeRelations,
      authorizations,
    };
  };

export const formatData: FormatDataType = (
  {
    name,
    title,
    subtitle,
    custom_fields,
    authorizations,
    caseTypeRelations,
    objectTypeRelations,
    changes,
    components_changed,
    external_reference,
  },
  folderUuid
) => {
  const relations = [
    ...(caseTypeRelations || []).map(({ id }) => ({ case_type_uuid: id })),
    ...(objectTypeRelations || []).map(({ id }) => ({
      custom_object_type_uuid: id,
    })),
  ];

  const data = {
    catalog_folder_uuid: folderUuid,
    name,
    title,
    subtitle,
    uuid: v4(),
    external_reference,
    custom_field_definition: custom_fields?.length
      ? {
          custom_fields: custom_fields.map(
            ({
              attribute_uuid,
              label,
              description,
              external_description,
              is_required,
              is_hidden_field,
              use_on_map,
              name,
            }) => {
              return {
                attribute_uuid,
                label,
                is_required,
                is_hidden_field,
                external_description,
                description,
                ...(use_on_map === undefined
                  ? {}
                  : { custom_field_specification: { use_on_map } }),
                custom_field_type: '' as CustomFieldTypeType,
                name,
              };
            }
          ),
        }
      : {},
    relationship_definition: relations.length
      ? { relationships: relations }
      : {},
    authorization_definition: authorizations?.length
      ? {
          authorizations: authorizations.map(
            ({ permission, department, role }) => ({
              authorization: permission,
              department: {
                uuid: (normalizeValue(department) as string) || '',
              },
              role: {
                uuid: role,
              },
            })
          ),
        }
      : {},
    audit_log: {
      description: changes,
      updated_components: components_changed,
    },
  };

  return data;
};

export const saveObjectType: SaveObjectTypeType = async (
  action,
  values,
  uuid,
  folderUuid
) => {
  const data = formatData(values, folderUuid);

  await submitObjectType(action, data, uuid);
};
