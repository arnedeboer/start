// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router';
import { useSteppedForm } from '@zaaksysteem/common/src/components/form/hooks/useSteppedForm';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import {
  ATTRIBUTE_LIST,
  CASETYPE_RELATION_LIST,
  OBJECTTYPE_RELATION_LIST,
} from '../../components/Form/Constants/fieldTypes';
import { useObjectTypeManagementStyles } from './ObjectTypeManagement.style';
import {
  ObjectTypeType,
  ActionType,
  ObjectTypeFormShapeType,
} from './ObjectTypeManagement.types';
import ObjectTypeStepControls from './ObjectTypeStepControls/ObjectTypeStepControls';
import { ObjectTypeProgressBar } from './ObjectTypeProgressBar/ObjectTypeProgressBar';
import { ObjectTypeFormStep } from './ObjectTypeFormStep/ObjectTypeFormStep';
import {
  getObjectTypeFormDefinition,
  getObjectTypeFormSteps,
} from './ObjectTypeForm.formDefinition';
import { AttributeList } from './FormFields/AttributeList/AttributeList';
import { AuthorizationList } from './FormFields/AuthorizationList/AuthorizationList';
import { CaseTypeRelationList } from './FormFields/CaseTypeRelationList/CaseTypeRelationList';
import { ObjectTypeRelationList } from './FormFields/ObjectTypeRelationList/ObjectTypeRelationList';
import {
  createInitialValuesFromObjectType,
  saveObjectType,
} from './ObjectTypeManagement.library';

type ObjectTypeManagementPropsType = {
  objectType: ObjectTypeType | null;
  action: ActionType;
  returnUrl: string;
  folderUuid?: string;
};

const ObjectTypeManagement: React.ComponentType<
  ObjectTypeManagementPropsType
> = ({ action, objectType, returnUrl, folderUuid }) => {
  const classes = useObjectTypeManagementStyles();
  const navigate = useNavigate();
  const [t] = useTranslation('objectTypeManagement');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const initialValues = objectType
    ? createInitialValuesFromObjectType(objectType)
    : {};

  const formDefinition = getObjectTypeFormDefinition(t);
  const stepsDefinition = getObjectTypeFormSteps(t);
  const formDefinitionWithValues = mapValuesToFormDefinition(
    initialValues,
    formDefinition
  );

  const onSubmit = (values: any) => {
    saveObjectType(action, values, objectType?.uuid, folderUuid)
      .then(() => {
        navigate(returnUrl);
      })
      .catch(openServerErrorDialog);
  };

  const {
    fields,
    activeStep,
    handleNextStep,
    handlePreviousStep,
    hasNextStep,
    hasPreviousStep,
    steps,
    formik: { submitForm },
  } = useSteppedForm<ObjectTypeFormShapeType>({
    formDefinition: formDefinitionWithValues,
    stepsDefinition,
    isInitialValid: true,
    onSubmit,
    onChange: () => {},
    fieldComponents: {
      [ATTRIBUTE_LIST]: AttributeList,
      [CASETYPE_RELATION_LIST]: CaseTypeRelationList,
      [OBJECTTYPE_RELATION_LIST]: ObjectTypeRelationList,
      AuthorizationList,
    },
  });

  return (
    <Sheet>
      {ServerErrorDialog}
      <div className={classes.wrapper}>
        <ObjectTypeProgressBar steps={steps} />
        <ObjectTypeFormStep step={activeStep} fields={fields} />
        <ObjectTypeStepControls
          activeStepValid={activeStep.isValid}
          handleNextStep={handleNextStep}
          handlePreviousStep={handlePreviousStep}
          hasNextStep={hasNextStep}
          hasPreviousStep={hasPreviousStep}
          submitForm={submitForm}
          busy={false}
        />
      </div>
    </Sheet>
  );
};

export default ObjectTypeManagement;
