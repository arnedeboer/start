// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect, createContext } from 'react';
import { useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { objectifyParams } from '@mintlab/kitchen-sink/source/url';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { useAdminBanner } from '../../library/auth';
import ObjecTypeManagement from './ObjectTypeManagement';
import locale from './ObjectTypeManagement.locale';
import { getObjectType } from './ObjectTypeManagement.library';
import { ObjectTypeType } from './ObjectTypeManagement.types';

export const ObjectTypeContext = createContext<ObjectTypeType | null>(null);

const ObjectTypeManagementModule: React.ComponentType = () => {
  const banner = useAdminBanner.catalog();
  const [t] = useTranslation('objectTypeManagement');
  const [objectType, setObjectType] = useState<ObjectTypeType | null>(null);
  const { pathname, search } = useLocation();

  const [, , , action, uuid] = pathname.split('/');

  const params = search.slice(1);
  const { return_url, folder_uuid: folderUuid } = objectifyParams(params);

  const returnUrl = decodeURIComponent(return_url || '/main/catalog');

  useEffect(() => {
    if (action === 'update') {
      getObjectType(uuid).then(objectType => {
        setObjectType(objectType);
      });
    }
  }, []);

  if (action === 'update' && !objectType) {
    return <Loader />;
  }

  const breadcrumbs = [
    {
      label: t('catalog'),
      path: returnUrl,
    },
    { label: objectType?.name || t('title') },
  ];

  return (
    banner || (
      <ObjectTypeContext.Provider value={objectType}>
        <ObjecTypeManagement
          /*
          // @ts-ignore */
          action={action}
          objectType={objectType}
          returnUrl={returnUrl}
          folderUuid={folderUuid}
        />
        <TopbarTitle breadcrumbs={breadcrumbs} />
      </ObjectTypeContext.Provider>
    )
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="objectTypeManagement">
    <ObjectTypeManagementModule />
  </I18nResourceBundle>
);
