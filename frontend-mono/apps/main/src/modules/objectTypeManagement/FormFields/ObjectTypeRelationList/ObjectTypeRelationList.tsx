// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import Select, {
  defaultFilterOption,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { SortableList, useListStyle } from '@mintlab/ui/App/Zaaksysteem/List';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import { ObjectTypeContext } from '../..';
import {
  extractSelectedValue,
  useObjectTypeRelationChoicesQuery,
} from '../FormFields.library';

type ObjectType = {
  id: string;
  name: string;
};

export const ObjectTypeRelationList: React.ComponentType<
  FormRendererFormField<any, any, ObjectType[]>
> = props => {
  const { setFieldValue, name, placeholder } = props;
  const classes = useListStyle();
  const { fields, add } = useMultiValueField<any, ObjectType>(props);
  const objectType = useContext(ObjectTypeContext);
  const editedObjectTypeUuid = objectType?.uuid;
  const value = fields.map(field => field.value);
  const [selectProps, ServerErrorDialog] = useObjectTypeRelationChoicesQuery();

  return (
    <div className={classes.listContainer}>
      <SortableList
        value={fields.map(field => ({ ...field, id: field.value.id }))}
        onReorder={reorderedFields =>
          setFieldValue(
            name,
            reorderedFields.map(field => field.value)
          )
        }
        renderItem={field => (
          <div className={classes.itemContainerSimple}>
            {field?.value?.name}
            <Button
              name="removeRelation"
              icon="close"
              iconSize="small"
              action={field.remove}
            />
          </div>
        )}
      />
      <Select
        variant="generic"
        {...selectProps}
        name={name}
        onChange={event => add(extractSelectedValue(event))}
        startAdornment={<Icon size="small">{iconNames.search}</Icon>}
        isMulti={false}
        value={null}
        isClearable={false}
        placeholder={placeholder}
        filterOption={(option, input) => {
          return (
            defaultFilterOption(option, input || '') &&
            value.every(item => item.id !== option.value.id) &&
            option.value.id !== editedObjectTypeUuid
          );
        }}
      />
      {ServerErrorDialog}
    </div>
  );
};
