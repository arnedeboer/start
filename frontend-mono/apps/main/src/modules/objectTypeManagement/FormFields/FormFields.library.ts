// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICatalog } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

export const extractSelectedValue = (event: any) => event.target.value.value;

export const useAttributeChoicesQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input);

  const data = useQuery(
    ['attributes', input],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<APICatalog.SearchAttributeByNameResponseBody>(
        'GET',
        buildUrl<APICatalog.SearchAttributeByNameRequestParams>(
          '/api/v2/admin/catalog/attribute_search',
          {
            search_string: keyword || '',
          }
        )
      ).catch(openServerErrorDialog);

      return body
        ? (body.data || []).map(
            ({ attributes: { name, value_type }, id }: any) => {
              return {
                value: {
                  draft: true,
                  id: id || '',
                  attribute_uuid: id || '',
                  name,
                  label: '',
                  is_required: false,
                  description: '',
                  external_description: '',
                  is_hidden_field: false,
                  custom_field_type: value_type,
                },
                label: name,
              };
            }
          )
        : [];
    },
    { enabled }
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog] as const;
};

export const useCaseTypeRelationChoicesQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input);

  const data = useQuery(
    ['caseTypes', input],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<APICatalog.SearchCatalogResponseBody>(
        'GET',
        buildUrl<APICatalog.SearchCatalogRequestParams>(
          '/api/v2/admin/catalog/search',
          { keyword, 'filter[type]': 'case_type' }
        )
      ).catch(openServerErrorDialog);

      return body
        ? (body.data || []).map(({ attributes: { name }, id }) => {
            return {
              value: {
                id: id || '',
                name,
              },
              label: name,
            };
          })
        : [];
    },
    { enabled }
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog] as const;
};

export const useObjectTypeRelationChoicesQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input);

  const data = useQuery(
    ['objectTypes', input],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<APICatalog.SearchCatalogResponseBody>(
        'GET',
        buildUrl<APICatalog.SearchCatalogRequestParams>(
          '/api/v2/admin/catalog/search',
          { keyword, 'filter[type]': 'custom_object_type' }
        )
      ).catch(openServerErrorDialog);

      return body
        ? (body.data || []).map(({ attributes: { name }, id }) => {
            return {
              value: {
                id: id || '',
                name,
              },
              label: name,
            };
          })
        : [];
    },
    { enabled }
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog] as const;
};
