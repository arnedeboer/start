// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  SECTION_CATALOG,
  SECTION_USERS,
  SECTION_LOG,
  SECTION_TRANSACTIONS,
  SECTION_CONNECTORS,
  SECTION_DATA_WAREHOUSE,
  SECTION_SYSTEM_CONFIG,
} from '../../../constants/section';

export const navigation = [
  {
    capability: 'beheer_zaaktype_admin',
    icon: 'chrome_reader_mode',
    label: 'Catalogus',
    path: '/main/catalog',
    section: SECTION_CATALOG,
  },
  {
    capability: 'useradmin',
    icon: 'people',
    label: 'Gebruikers',
    path: '/main/users',
    section: SECTION_USERS,
  },
  {
    capability: 'admin',
    icon: 'import_contacts',
    label: 'Logboek',
    path: '/main/log',
    section: SECTION_LOG,
  },
  {
    capability: 'admin',
    icon: 'poll',
    label: 'Transacties',
    path: '/main/transactions',
    section: SECTION_TRANSACTIONS,
  },
  {
    capability: 'admin',
    icon: 'all_inclusive',
    label: 'Koppelingen',
    path: '/main/integrations',
    section: SECTION_CONNECTORS,
  },
  {
    capability: 'admin',
    icon: 'view_comfy',
    label: 'Gegevens',
    path: '/main/datastore',
    section: SECTION_DATA_WAREHOUSE,
  },
  {
    capability: 'admin',
    icon: 'settings',
    label: 'Configuratie',
    path: '/main/configuration',
    section: SECTION_SYSTEM_CONFIG,
  },
];
