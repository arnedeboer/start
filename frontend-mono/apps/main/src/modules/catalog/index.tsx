// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useAdminBanner } from '../../library/auth';
import Catalog from './components/Catalog';
import locale from './Catalog.locale';

const CatalogModule = () => {
  const banner = useAdminBanner.catalog();

  return (
    <I18nResourceBundle resource={locale} namespace="catalog">
      {banner || <Catalog />}
    </I18nResourceBundle>
  );
};

export default CatalogModule;
