// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    add: 'aanmaken',
    actions: {
      import: 'Importeren',
      createCaseType: 'Zaaktype aanmaken',
      createObjectType: 'Objecttype aanmaken',
      createEmailTemplate: 'E-mailsjabloon aanmaken',
      createAttribute: 'Kenmerk aanmaken',
      createFolder: 'Map aanmaken',
      createDocumentTemplate: 'Sjabloon aanmaken',
    },
    buttonBar: {
      delete: 'Verwijderen',
      details: 'Details',
      duplicate: 'Dupliceren',
      edit: 'Bewerken',
      export: 'Exporteren',
      move: 'Verplaatsen',
      history: 'Versiebeheer',
    },
    column: {
      name: 'Naam',
      type: 'Type',
    },
    detailView: {
      enclosedDocument: 'Ingesloten document',
      itemsSelected: 'onderdelen geselecteerd',
      lastModified: 'Gewijzigd',
      valueFallback: 'Onbekend',
      magicString: 'Magicstring',
      used_in_case_types: 'Zaaktypen',
      used_in_object_types: 'Objecttypen',
      identification: 'Identificatie',
      uuid: 'UUID',
      versionUuid: 'Versie UUID',
      document: 'Document',
      valueType: 'Invoertype',
      versionTitle: 'Versie',
      relationsTitle: 'Gebruik',
      directLinksTitle: 'Directe links',
      copyToClipboard: 'Kopieer naar klembord.',
      copiedToClipboard: 'Gekopieerd naar klembord',
      locationFolder: 'Locatie',
      internalUrl: 'Intern registratieformulier',
      caseTypeApi: 'Zaaktype API',
      openInNewWindow: 'Open in nieuw venster',
      saveFile: 'Bestand opslaan',
      rootFolder: 'Hoofdmap',
      active: 'Actief',
      concept: 'Concept',
      status: 'Status',
    },
    items: {
      offline: 'Offline',
    },
    addElement: {
      title: 'Element toevoegen',
    },
    title: 'Catalogus',
    system: 'Zaaksysteem',
    search: 'Zoeken in de catalogus…',
    move: {
      label: 'Verplaats {{numToMove}} item',
      label_plural: 'Verplaatsen {{numToMove}} items',
      description: 'Ga naar de map waarnaar het item verplaatst moet worden.',
      description_plural:
        'Ga naar de map waarnaar de items verplaatst moeten worden.',
      moveButton: 'Verplaatsen naar deze map',
      success: 'De verplaatsing is succesvol uitgevoerd.',
    },
    delete: {
      success: 'Het item is succesvol verwijderd.',
      confirm: {
        title: 'Weet u het zeker?',
        placeholder: 'Reden',
        label: 'Geef hieronder een reden op.',
        description: 'U kunt deze actie niet ongedaan maken.',
        ok: 'Verwijderen',
      },
    },
    changeOnlineStatus: {
      online: 'online',
      offline: 'offline',
      reason: 'reden',
      title: 'Zaaktype {{ type }} zetten',
      menuTitle: '{{ type }} zetten',
    },
    activateCaseType: {
      reason: 'reden',
      title: 'Zaaktypeversie activeren',
      menuTitle: 'Zaaktypeversie activeren',
      description:
        'Geef hieronder de reden op voor het activeren van deze versie.',
      error: 'Geef een reden op.',
      success: 'Het zaaktype is succesvol geactiveerd.',
    },
    defaultCreate: 'Nieuw aangemaakt',
    defaultEdit: 'Wijziging',
    serverErrors: {
      'admin/catalog/not_found': 'Kon niet worden gevonden.',
      'case_type/not_found': 'Het zaaktype kon niet worden gevonden.',
      'case_type/status_already_true': 'Dit zaaktype is al online gezet.',
      'case_type/status_already_false': 'Dit zaaktype is al offline gezet.',
      'folder_entry/not_found': 'De item(s) konden niet worden gevonden.',
      'folder/not_found': 'De map kon niet worden gevonden.',
      "folder_entry/can't_move_to_child":
        'De item(s) kunnen niet naar deze map verplaatst worden.',
      'attribute/no_active_appointment_integrations':
        'Er konden geen actieve Kalender-koppelingen worden gevonden.',
      'attribute/not_found':
        'Het kenmerk met de opgegeven UUID kon niet worden gevonden.',
      'attribute/already_exists_with_uuid':
        'Een kenmerk met de opgegeven UUID bestaat al.',
      'attribute/invalid_magic_string':
        'De opgegeven Magic String is reeds in gebruik.',
      'attribute/missing_required_field':
        'Een verplicht veld is niet opgegeven.',
      'attribute/invalid_attribute_type':
        'Het opgegeven kenmerktype is ongeldig.',
      'bibliotheek_category/not_found':
        'De categorie met de opgegeven UUID kon niet worden gevonden.',
      'attribute/magic_string_cannot_be_generated':
        'Er kon geen geldige Magic String worden gegenereerd.',
      'folder/name_in_use':
        'Een map met de opgegeven naam bestaat al. Geef een andere naam op.',
      'document_template/not_found': 'Het sjabloon kon niet worden gevonden.',
      'document_template/already_exists_with_uuid':
        'Een sjabloon met het opgegeven UUID bestaat al.',
      'document_template/incorrect_template_name':
        'Ongeldige referentie opgegeven.',
      'folder/not_empty':
        'De map kan niet worden verwijderd omdat deze niet leeg is.',
      'case_type/used_in_cases':
        'Dit zaaktype kan niet verwijderd worden. Het wordt nog gebruikt door een bestaande zaak.',
      'case_type/used_in_case_types':
        'Dit zaaktype kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'attribute/used_in_case_types':
        'Het attribuut kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'email_template/used_in_case_types':
        'Het e-mailsjabloon kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'object_type/used_in_case_types':
        'Dit objecttype kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'document_template/used_in_case_types':
        'Dit documentsjabloon kan niet verwijderd worden. Het wordt nog gebruikt door een bestaand zaaktype.',
      'case_management/custom_object_type/can_not_delete':
        'Het object kan niet verwijderd worden.',
    },
  },
};
