// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import {
  useInfiniteQuery,
  useMutation,
  useQueryClient,
} from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICatalog } from '@zaaksysteem/generated';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { queryToObject } from '../library/searchQuery';
import { getPathToItem } from '../library/pathGetters';
import { CatalogItemType } from './Catalog.types';
import { CATALOG_ITEMS, invalidateAfterChange } from './Catalog.symbols';

const page_size = 50;

const organizeBreadcrumbs = (links: any) => {
  return [links.grandparent, links.parent, links.self]
    .filter(item => item && item.id)
    .map(({ id, name }) => ({
      path: getPathToItem(id),
      label: name,
    }));
};

type RowType = {
  active: boolean;
  type:
    | 'folder'
    | 'case_type'
    | 'object_type'
    | 'attribute'
    | 'email_template'
    | 'document_template'
    | 'custom_object_type';
  name: string;
  id: string;
};

export const useCatalogItemsQuery = (searchTerm: string, folderId: string) => {
  return useInfiniteQuery<
    [RowType[], { path: string; label: string }[]],
    V2ServerErrorsType
  >([CATALOG_ITEMS, searchTerm, folderId], async props => {
    const page = props.pageParam || 1;
    let params:
      | APICatalog.GetFolderContentsRequestParams
      | APICatalog.SearchCatalogRequestParams;
    let urlBase = '';

    if (searchTerm) {
      urlBase = '/api/v2/admin/catalog/search';
      params = {
        ...queryToObject(searchTerm),
        page,
        page_size,
      };
    } else {
      urlBase = '/api/v2/admin/catalog/get_folder_contents';
      params = {
        folder_id: folderId,
        page,
        page_size,
      };
    }

    return request<
      | APICatalog.SearchCatalogResponseBody
      | APICatalog.GetFolderContentsResponseBody
    >('GET', buildUrl(urlBase, params)).then(data => {
      const list = data.data.map(row => ({
        active: row.attributes.active,
        type: row.attributes.type,
        name: row.attributes.name,
        id: row.id,
      }));

      return [list, organizeBreadcrumbs(data.links)];
    });
  });
};

export const useMoveItemsMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    APICatalog.MoveFolderEntriesRequestBody,
    V2ServerErrorsType,
    {
      items: CatalogItemType[];
      folderId: string;
    }
  >(
    ['MOVE_ITEMS'],
    ({ items, folderId }) => {
      return request('POST', '/api/v2/admin/catalog/move_folder_entries', {
        destination_folder_id: folderId,
        folder_entries: items,
      });
    },
    {
      onSuccess: () => {
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};
