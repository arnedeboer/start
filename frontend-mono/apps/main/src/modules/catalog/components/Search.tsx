// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Search from '../../../components/Search/Search';

export default ({ query, refresh }: { refresh: () => void; query: string }) => {
  const [t] = useTranslation('catalog');

  return (
    <Search
      query={query}
      onSearch={term => {
        window.history.pushState(
          '',
          '',
          window.location.pathname + '?query=' + term
        );
        refresh();
      }}
      onClear={() => {
        if (window.location.search.length) {
          window.history.pushState('', '', window.location.pathname);
          refresh();
        }
      }}
      placeholder={t('search')}
      scope="catalog-search"
    />
  );
};
