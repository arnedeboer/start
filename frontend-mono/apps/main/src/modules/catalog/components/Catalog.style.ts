// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/styles';

const plusButtonSize = '146px';

export const catalogStyleSheet = () => {
  const {
    mintlab: { greyscale },
    // eslint-disable-next-line react-hooks/rules-of-hooks
  } = useTheme<Theme>();

  return {
    wrapper: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '20px',
      borderBottom: `1px solid ${greyscale.light}`,
    },
    searchWrapper: {
      width: '400px',
      minWidth: '200px',
    },
    addElement: {
      position: 'absolute',
      bottom: 0,
      left: `min(50vw + 825px, 100vw - ${plusButtonSize})`,
      zIndex: 1,
      padding: '40px 45px',
    },
    contentWrapper: {
      flexGrow: 1,
      height: 'calc(100% - 75px)',
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
    tableWrapper: {
      position: 'relative',
      flexGrow: 1,
      display: 'flex',
      flexDirection: 'column',
    },
    iconCell: {
      width: '50px',
    },
    loader: {
      position: 'absolute',
      top: '20px',
      right: '50%',
    },
    card: {
      position: 'relative',
      boxSizing: 'border-box',
      height: '100%',
    },
  };
};
