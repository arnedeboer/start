// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useRef } from 'react';
import { AutoSizer, Column, Table, InfiniteLoader } from 'react-virtualized';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useTranslation } from 'react-i18next';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import Render from '@mintlab/ui/App/Abstract/Render';
import Box from '@mui/material/Box';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import { IconCell, LinkCell } from './../../../../components/Table/cells';
import { useCatalogTableStyle } from './CatalogTable.style';

// If you remove this the table style will break
!!SortableTable;

type RowType = {
  path: string;
  isNavigable: boolean;
  icon: string;
  id: string;
  selected: boolean;
  beingMoved: boolean;
  active: boolean;
  type:
    | 'case_type'
    | 'folder'
    | 'object_type'
    | 'attribute'
    | 'email_template'
    | 'document_template'
    | 'custom_object_type';
  name: string;
};

type CatalogTablePropsType = {
  loadingItems: boolean;
  onRowNavigate: (
    path: string,
    navigable: boolean,
    event: React.SyntheticEvent
  ) => void;
  rows: RowType[];
  loadMore: () => Promise<any>;
  toggleItem: (id: string, multiselect: boolean) => void;
};

const CatalogTable = ({
  rows = [],
  toggleItem,
  onRowNavigate,
  loadMore,
  loadingItems,
}: CatalogTablePropsType) => {
  const style = useCatalogTableStyle();
  const [t] = useTranslation('catalog');
  const columnTranslations = t('column', { returnObjects: true });
  const columns = [
    { name: 'selected', width: 50 },
    { name: 'icon', width: 75 },
    { name: 'name', width: 1 },
    { name: 'type', width: 200 },
  ].map(column => ({
    ...column,
    //@ts-ignore
    label: columnTranslations[column.name],
  }));

  const infiniteLoaderRef = useRef<InfiniteLoader>(null);

  useEffect(() => {
    infiniteLoaderRef.current?.resetLoadMoreRowsCache();
    loadMore();
  }, []);

  const catalogItemScope = 'catalog-item';
  const cellContent = {
    selected(rowData: RowType) {
      return (
        <Checkbox
          name="catalog-table-item"
          scope={catalogItemScope}
          disabled={rowData.beingMoved}
          checked={rowData.selected}
        />
      );
    },
    icon(rowData: RowType) {
      return <IconCell value={rowData.icon} />;
    },
    name({ active, beingMoved, isNavigable, name, path, type }: RowType) {
      const shouldBeLink = isNavigable && !beingMoved;
      return (
        <Box data-scope={`${catalogItemScope}:name-wrapper`}>
          <Render condition={!shouldBeLink}>
            <span>{name}</span>
          </Render>
          <Render condition={shouldBeLink}>
            <LinkCell
              handleNavigate={(event: any) => {
                event.preventDefault();
                const tagName: string = get(
                  event,
                  'nativeEvent.target.tagName'
                );
                if (tagName && tagName.toLowerCase() === 'a') {
                  event.stopPropagation();
                }
                onRowNavigate(path, isNavigable, event);
              }}
              scope={`${catalogItemScope}:${type}`}
              path={path}
              value={name}
            />
          </Render>
          <Render condition={type === 'case_type' && !active}>
            <span style={style.activeLabel}>{t('items:offline')}</span>
          </Render>
        </Box>
      );
    },
    type(rowData: RowType) {
      const res = t(`common:entityType:${rowData.type}`);
      return res === 'Objecttype' ? res + ' v1' : res;
    },
  };

  return (
    <Box sx={style.tableWrapper}>
      <InfiniteLoader
        isRowLoaded={({ index }) => !!rows[index]}
        loadMoreRows={loadMore}
        rowCount={99999}
        threshold={10}
        ref={infiniteLoaderRef}
      >
        {({ onRowsRendered, registerChild }) => (
          <AutoSizer>
            {table => (
              <Table
                ref={registerChild}
                height={table.height}
                onRowsRendered={onRowsRendered}
                width={table.width}
                headerHeight={61}
                rowHeight={82}
                rowCount={rows.length}
                rowGetter={({ index }) => rows[index]}
                // eslint-disable-next-line complexity
                rowStyle={({ index }) =>
                  [
                    index === -1 && style.tableHeader,
                    index >= 0 && style.tableRow,
                    index >= 0 &&
                      rows[index] &&
                      !rows[index].beingMoved &&
                      style.tableRowHover,
                    index >= 0 &&
                      rows[index] &&
                      rows[index].beingMoved &&
                      style.tableRowDisabled,
                  ].reduce(
                    (prev, acc) => ({ ...(prev ? prev : {}), ...acc }),
                    {}
                  )
                }
                onRowClick={({ event, rowData }) => {
                  if (!rowData.beingMoved) {
                    //@ts-ignore
                    const multiSelect = event.target.type === 'checkbox';
                    toggleItem(rowData.id, multiSelect);
                  }
                }}
                onRowDoubleClick={({ rowData: { path, isNavigable }, event }) =>
                  onRowNavigate(path, isNavigable, event)
                }
              >
                {columns.map(({ label, name, width }) => (
                  <Column
                    key={name}
                    width={width}
                    flexGrow={width === 1 ? 1 : 0}
                    dataKey={name}
                    label={label}
                    headerRenderer={({ label }) => (
                      <Box sx={style.tableCell}>{label}</Box>
                    )}
                    cellRenderer={({ dataKey, rowData }) => (
                      <Box sx={style.tableCell}>
                        {/* @ts-ignore */}
                        {cellContent[dataKey](rowData)}
                      </Box>
                    )}
                  />
                ))}
              </Table>
            )}
          </AutoSizer>
        )}
      </InfiniteLoader>
    </Box>
  );
};

export default CatalogTable;
