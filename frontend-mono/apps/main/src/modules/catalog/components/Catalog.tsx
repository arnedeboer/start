// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router';
import Box from '@mui/material/Box';
import Fab from '@mintlab/ui/App/Material/Fab/Fab';
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import AddElement from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import {
  getPathToCaseType,
  getPathToCustomObjectType,
  getPathToImport,
  getPathToItem,
  getPathToNewCustomObjectType,
  getPathToObjectType,
} from '../library/pathGetters';
import { catalogStyleSheet } from './Catalog.style';
import CatalogTable from './table/CatalogTable';
import DetailView from './DetailView/DetailView';
import MoveBar from './MoveBar';
import Search from './Search';
import ButtonBar from './ButtonBar/ButtonBar';
import { CatalogItemType } from './Catalog.types';
import { useCatalogItemsQuery, useMoveItemsMutation } from './Catalog.library';
import { useEmailTemplateDialog } from './Dialogs/EmailTemplateDialog';
import { useDocumentTemplateDialog } from './Dialogs/DocumentTemplateDialog';
import { useAttributeDialog } from './Dialogs/Attribute/Attribute';
import { useFolderDialog } from './Dialogs/FolderDialog';
import { useDeleteDialog } from './Dialogs/DeleteDialog';
import { useHistoryDialog } from './Dialogs/History/HistoryDialog';
import { useChangeOnlineStatusDialog } from './Dialogs/ChangeOnlineStatusDialog';

/* eslint complexity: [2, 20] */
const Catalog = () => {
  const searchTerm = new URLSearchParams(window.location.search).get('query');
  const folderId = /catalog(\/)?$/.test(window.location.pathname)
    ? ''
    : window.location.pathname.split('/').reverse()[0];

  const [t] = useTranslation('catalog');
  const stylesheet = catalogStyleSheet();
  const navigate = useNavigate();
  const [movingFromId, setMovingFromId] = React.useState('');
  const [itemsForMoving, setMoving] = React.useState<CatalogItemType[]>([]);
  const [showDetailView, setShowDetailView] = React.useState(false);
  const toggleDetailView = () => setShowDetailView(!showDetailView);
  const [selectedIds, setSelectedIds] = React.useState<string[]>([]);

  const [counter, setCounter] = React.useState(0);

  const [addElementOpened, setAddElementOpened] = React.useState(false);

  const { openAttributeDialog, attributeDialog } = useAttributeDialog();
  const { openEmailTemplateDialog, emailTemplateDialog } =
    useEmailTemplateDialog(folderId);
  const { openDocumentTemplateDialog, documentTemplateDialog } =
    useDocumentTemplateDialog(folderId);
  const folderDialog = useFolderDialog(folderId);
  const { openDeleteDialog, deleteDialog } = useDeleteDialog();
  const { openHistoryDialog, historyDialog } = useHistoryDialog();
  const { openChangeOnlineStatusDialog, changeOnlineStatusDialog } =
    useChangeOnlineStatusDialog();

  const {
    mutate: moveItems,
    isLoading: movingItems,
    error: moveError,
  } = useMoveItemsMutation(() => {
    setMoving([]);
    setMovingFromId('');
  });

  const refreshToLoadNewHistory = () => {
    setCounter(counter + 1);
    setSelectedIds([]);
  };

  const {
    data,
    isLoading,
    fetchNextPage,
    isFetchingNextPage,
    error: itemsError,
  } = useCatalogItemsQuery(searchTerm || '', folderId);

  const error = itemsError || moveError;
  const catalogItems = data ? data.pages.map(page => page[0]).flat() : [];
  const breadcrumbs = data ? data.pages.map(page => page[1])[0] : [];
  const nextPage = (data?.pages.length || 0) + 1;
  const selected = catalogItems.filter(it => selectedIds.includes(it.id));
  const selectedItemId = selectedIds[0];

  const translatedBreadcrumbs = [
    {
      label: t('title'),
      path: '/main/catalog',
      id: 'root',
    },
    ...breadcrumbs.map(br => ({
      ...br,
      label: t(br.label),
    })),
  ].map(br => ({
    ...br,
    onItemClick: preventDefaultAndCall(() => {
      window.history.pushState('', br.label, br.path);
      refreshToLoadNewHistory();
    }),
  }));

  const rows = catalogItems.map(item => ({
    ...item,
    path: getPathToItem(item.id, item.type),
    isNavigable: [
      'folder',
      'case_type',
      'object_type',
      'custom_object_type',
    ].includes(item.type),
    icon: `entityType.${item.type}`,
    id: item.id,
    selected: Boolean(selected.find(it => it.id === item.id)),
    beingMoved: itemsForMoving.some(it => it.id === item.id),
  }));

  const goToEditOrCreate = {
    caseType: (id?: string) => {
      navigate(
        getPathToCaseType(id || selectedItemId, '/bewerken', true, {
          folder_uuid: folderId,
        })
      );
    },
    objectType: (id?: string) => {
      navigate(
        getPathToObjectType(id || selectedItemId, '/bewerken', true, {
          folder_uuid: folderId,
        })
      );
    },
  };

  return (
    <Sheet>
      {!searchTerm && !itemsForMoving.length && (
        <Box sx={stylesheet.addElement}>
          <Fab
            action={() => setAddElementOpened(true)}
            ariaLabel={t('add')}
            color="primary"
            iconName="add"
          />
        </Box>
      )}
      {error && <ServerErrorDialog err={error} />}
      <TopbarTitle breadcrumbs={translatedBreadcrumbs} />
      <Box sx={stylesheet.wrapper}>
        <Box sx={stylesheet.searchWrapper}>
          <Search query={searchTerm || ''} refresh={refreshToLoadNewHistory} />
        </Box>
        <ButtonBar
          exportCaseType={() => {
            window.open(
              window.location.origin +
                `/beheer/zaaktypen/${selectedItemId}/export`,
              '_blank'
            );
          }}
          changeOnlineStatus={() =>
            openChangeOnlineStatusDialog([selectedItemId, selected[0].active])
          }
          editCaseType={() => goToEditOrCreate.caseType(selectedItemId)}
          editObjectType={() => goToEditOrCreate.objectType(selectedItemId)}
          editCustomObjectType={() => {
            navigate(getPathToCustomObjectType(selectedItemId));
          }}
          editAttribute={() => openAttributeDialog({ id: selectedItemId })}
          editDocumentTemplate={() =>
            openDocumentTemplateDialog({
              id: selectedItemId,
            })
          }
          editEmailTemplate={() =>
            openEmailTemplateDialog({ id: selectedItemId })
          }
          editFolder={() =>
            folderDialog.open({
              id: selectedItemId,
              name: selected[0].name,
            })
          }
          duplicateCaseType={() => {
            navigate(
              getPathToCaseType(selectedItemId, '/clone', true, {
                folder_uuid: folderId,
              })
            );
          }}
          deleteItem={() => {
            selected[0] && openDeleteDialog(selected[0]);
          }}
          initHistory={() => openHistoryDialog(selectedItemId)}
          selectedItems={selected}
          moveItems={itemsForMoving}
          toggleDetailView={toggleDetailView}
          setMoveItems={items => {
            setSelectedIds([]);
            setMovingFromId(folderId);
            setMoving(items);
          }}
        />
      </Box>
      <Box sx={stylesheet.contentWrapper}>
        <Box sx={stylesheet.tableWrapper}>
          <CatalogTable
            rows={rows}
            onRowNavigate={(path, navigable, event) => {
              if (path && navigable) {
                navigate(path);
                event.preventDefault();
              }
            }}
            toggleItem={id => {
              const target = catalogItems.find(item => item.id === id);
              const update =
                target === undefined
                  ? selected
                  : selected.find(item => item.id === target.id)
                  ? selected.filter(item => item.id !== target.id)
                  : [...selected, target];
              setSelectedIds(update.map(it => it.id));
            }}
            loadMore={() => {
              const lastPageFull =
                (data?.pages[data?.pages.length - 1] || [[]])[0].length === 50;

              const alreadyFetchedNextPage = data?.pages.length === nextPage;

              return !data || (lastPageFull && !alreadyFetchedNextPage)
                ? fetchNextPage({ pageParam: nextPage })
                : Promise.resolve([]);
            }}
            loadingItems={isLoading || isFetchingNextPage}
          />
          {Boolean(itemsForMoving.length) && (
            <MoveBar
              numToMove={itemsForMoving.length}
              moveAction={() => {
                movingFromId !== folderId &&
                  moveItems({ items: itemsForMoving, folderId });
              }}
              cancelAction={() => setMoving([])}
              disabled={movingItems || Boolean(searchTerm)}
            />
          )}
        </Box>
        {showDetailView && (
          <DetailView
            currentFolderName={breadcrumbs[breadcrumbs.length - 1]?.label}
            toggleDetailView={toggleDetailView}
            items={catalogItems}
            selected={selected}
          />
        )}
      </Box>
      {attributeDialog}
      {emailTemplateDialog}
      {documentTemplateDialog}
      {folderDialog.dialog}
      {deleteDialog}
      {historyDialog}
      {changeOnlineStatusDialog}
      {addElementOpened && (
        <AddElement
          title={t('addElement.title')}
          t={t}
          hide={() => setAddElementOpened(false)}
          sections={[
            [
              {
                type: 'case_type',
                title: t('common:entityType.case_type'),
                icon: 'entityType.case_type',
                action: () => goToEditOrCreate.caseType('0'),
              },
              {
                type: 'object_type',
                title: t('common:entityType.object_type'),
                icon: 'entityType.object_type',
                action: () => goToEditOrCreate.objectType('0'),
              },
              {
                type: 'custom_object_type',
                icon: 'entityType.custom_object_type',
                title: t('common:entityType.custom_object_type'),
                action: () => navigate(getPathToNewCustomObjectType(folderId)),
              },
              {
                type: 'email_template',
                icon: 'entityType.email_template',
                title: t('common:entityType.email_template'),
                action: () => {
                  openEmailTemplateDialog({});
                  setAddElementOpened(false);
                },
              },
              {
                type: 'document_template',
                icon: 'entityType.document_template',
                title: t('common:entityType.document_template'),
                action: () => {
                  openDocumentTemplateDialog({});
                  setAddElementOpened(false);
                },
              },
              {
                type: 'attribute',
                icon: 'entityType.attribute',
                title: t('common:entityType.attribute'),
                action() {
                  openAttributeDialog({});
                  setAddElementOpened(false);
                },
              },
            ],
            [
              {
                type: 'folder',
                icon: 'entityType.folder',
                title: t('common:entityType.folder'),
                action() {
                  folderDialog.open({});
                  setAddElementOpened(false);
                },
              },
              {
                type: 'import',
                icon: 'actions.import',
                title: t('actions:import'),
                action() {
                  navigate(getPathToImport());
                },
              },
            ],
          ]}
        />
      )}
    </Sheet>
  );
};

export default Catalog;
