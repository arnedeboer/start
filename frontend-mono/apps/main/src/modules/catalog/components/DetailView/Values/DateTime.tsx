// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import { useValuesStyle } from './Values.style';

const DateTime = ({ value }: { value: any }) => {
  const dateValue = new Date(value);
  const style = useValuesStyle();

  return (
    <div>
      <span>{fecha.format(dateValue, 'Do MMMM YYYY')}</span>
      <span style={style.time}>{fecha.format(dateValue, 'shortTime')}</span>
    </div>
  );
};

export default DateTime;
