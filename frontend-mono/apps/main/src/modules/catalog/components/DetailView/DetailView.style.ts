// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/material';

export const useDetailViewStyle = () => {
  const {
    mintlab: { greyscale },
    typography,
  } = useTheme<Theme>();

  return {
    detailView: {
      height: '100%',
      width: '460px',
      display: 'flex',
      flexFlow: 'column',
      alignItems: 'stretch',
      fontFamily: typography.fontFamily,
      borderLeft: `1px solid ${greyscale.light}`,
      minWidth: '450px',
    },
    header: {
      display: 'flex',
      alignItems: 'center',
      padding: '10px',
    },
    headerIcon: {
      padding: '10px',
    },
    title: {
      flexGrow: '1',
    },
    close: {
      height: '60px',
    },
    subHeader: {
      display: 'flex',
      alignItems: 'center',
    },
    version: {
      margin: '0px 10px 10px 20px',
      padding: '0px 12px',
      border: `1px solid ${greyscale.darker}`,
      borderRadius: '13px',
      color: greyscale.darkest,
      fontSize: typography.subtitle2.fontSize,
    },
    content: {
      borderTop: `1px solid ${greyscale.dark}`,
      padding: '0px 20px 146px 20px',
      height: '100%',
      overflow: 'auto',
      overflowY: 'auto',
    },
  };
};
