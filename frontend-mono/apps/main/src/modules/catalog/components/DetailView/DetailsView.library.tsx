// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICatalog } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { CatalogItemDetailsType, CatalogItemType } from '../Catalog.types';
import { DETAILS_VIEW } from '../Catalog.symbols';

// eslint-disable-next-line react-hooks/rules-of-hooks
export const useDetailsQuery = (selectedItem?: CatalogItemType) =>
  useQuery(
    [DETAILS_VIEW, selectedItem && selectedItem.id],
    async () => {
      if (!selectedItem) {
        return;
      }

      const data = await request<APICatalog.GetEntryDetailResponseBody>(
        'GET',
        buildUrl<APICatalog.GetEntryDetailRequestParams>(
          '/api/v2/admin/catalog/get_entry_detail',
          {
            type: selectedItem.type,
            item_id: selectedItem.id,
          }
        )
      );

      const type = data.data.type;

      const res: CatalogItemDetailsType = {
        type,
        id: data.data.id,
        lastModified: data.data.attributes.last_modified,
        versionIndependentUuid: data.data.attributes.version_independent_uuid,
        status: data.data.attributes.status,
        identification: data.data.attributes.identification,
        caseTypeContext: data.data.attributes.context,
        version:
          data.data.attributes.version || data.data.attributes.current_version,
        magicString: data.data.attributes.magic_string,
        valueType: data.data.attributes.value_type,
        locationFolder:
          type === 'folder'
            ? {
                id: data.data.attributes.parent_id,
                name: data.data.attributes.parent_name,
              }
            : {
                id: data.data.relationships.folder.id,
                name: data.data.relationships.folder.attributes.name,
              },
        usedInCaseTypes: data.data.relationships?.used_in_case_types?.map(
          (ct: any) => ct.attributes.name
        ),
        enclosedDocument:
          type === 'document_template'
            ? {
                name: data.data.attributes.filename,
                link: data.data.links.download,
              }
            : undefined,
      };

      return res;
    },
    { enabled: Boolean(selectedItem) }
  );
