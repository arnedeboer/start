// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Button } from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { IconNameType } from '@mintlab/ui/App/Material/Icon';

type OpenLinkPropsType = {
  icon: IconNameType;
  link: string;
  title: string;
};

const OpenLink = ({ icon = 'open_in_new', link, title }: OpenLinkPropsType) => (
  <div>
    <Tooltip title={title}>
      <Button
        component="a"
        name="openLink"
        sx={{ padding: '4px', marginLeft: '6px' }}
        icon={icon}
        iconSize="extraSmall"
        //@ts-ignore
        href={link}
        target="_blank"
      />
    </Tooltip>
  </div>
);

export default OpenLink;
