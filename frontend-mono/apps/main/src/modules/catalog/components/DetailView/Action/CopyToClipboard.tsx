// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { TFunction } from 'i18next';
import { Button } from '@mintlab/ui/App/Material/Button';
import ActionFeedback from '@mintlab/ui/App/Material/ActionFeedback';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';

const CopyToClipboard = ({ value, t }: { t: TFunction; value: string }) => {
  const ref = useRef<HTMLInputElement>(null);

  const copy = () => {
    const current = ref.current;

    if (current) {
      current.type = 'text';
      current.select();
      document.execCommand('copy');
      current.type = 'hidden';
    }
  };

  return (
    <div>
      <input ref={ref} type="hidden" value={value} />
      <ActionFeedback
        title={t('catalog:detailView.copiedToClipboard')}
        type="default"
      >
        {handleOpen => (
          <div>
            <Tooltip title={t('catalog:detailView.copyToClipboard')}>
              <Button
                name="fileCopy"
                icon="file_copy"
                iconSize="extraSmall"
                sx={{ padding: '4px', marginLeft: '6px' }}
                action={() => {
                  copy();
                  handleOpen();
                }}
              />
            </Tooltip>
          </div>
        )}
      </ActionFeedback>
    </div>
  );
};

export default CopyToClipboard;
