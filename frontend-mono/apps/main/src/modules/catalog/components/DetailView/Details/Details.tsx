// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { buildUrl, get } from '@mintlab/kitchen-sink/source';
import Detail from '../Detail/Detail';
import { DateTime, Default, NavigateTo } from '../Values';
import Group from '../Group/Group';
import CopyToClipboard from '../Action/CopyToClipboard';
import OpenLink from '../Action/OpenLink';
import { getPathToItem } from '../../../library/pathGetters';
import { CatalogItemDetailsType } from '../../Catalog.types';

const parseMagicString = (str: string) => `[[${str}]]`;
const domain = get(window, 'top.location.origin', '');

/* eslint complexity: [2, 21] */
const Details = ({
  id,
  type,
  lastModified,
  magicString,
  valueType,
  locationFolder,
  identification,
  enclosedDocument,
  versionIndependentUuid,
  usedInCaseTypes,
  caseTypeContext,
  status,
}: CatalogItemDetailsType) => {
  const [t] = useTranslation('catalog');

  const caseTypeApi = `${domain}/api/v1/casetype/${id}`;
  const caseTypeIntern = buildUrl(`${domain}/intern/aanvragen/${id}/`, {
    contactkanaal: 'behandelaar',
  });

  return (
    <React.Fragment>
      {lastModified && (
        <Detail title={t('detailView.lastModified')} icon="access_time">
          <DateTime value={lastModified} />
        </Detail>
      )}
      <Detail icon="folder">
        <NavigateTo
          value={getPathToItem(locationFolder.id) || '/'}
          title={locationFolder.name || t('detailView.rootFolder')}
        />
      </Detail>
      {magicString && (
        <Detail title={t('detailView.magicString')} icon="photo_filter">
          <Default value={parseMagicString(magicString)} />
          <CopyToClipboard value={parseMagicString(magicString)} t={t} />
        </Detail>
      )}
      {identification && (
        <Detail title={t('detailView.identification')} icon="fingerprint">
          <Default value={identification} />
        </Detail>
      )}
      {valueType && (
        <Detail title={t('detailView.valueType')} icon="view_quilt">
          <Default
            value={t(`attribute:fields.attribute_type.choices:${valueType}`)}
          />
        </Detail>
      )}
      {enclosedDocument && (
        <Detail title={t('detailView.enclosedDocument')} icon="save_alt">
          <Default value={enclosedDocument.name} />
          <OpenLink
            icon="save_alt"
            title={t('detailView.saveFile')}
            link={`${domain}${enclosedDocument.link}`}
          />
        </Detail>
      )}
      {type === 'custom_object_type' && (
        <React.Fragment>
          <Detail title={t('detailView.status')} icon="verified_user">
            <Default
              value={
                (status || '').toLowerCase() === 'active'
                  ? t('detailView.active')
                  : t('detailView.concept')
              }
            />
          </Detail>
        </React.Fragment>
      )}
      {(type === 'case_type' ||
        type === 'object_type' ||
        type === 'custom_object_type') && (
        <React.Fragment>
          <Detail title={t('detailView.uuid')} icon="code">
            <Default value={versionIndependentUuid || id} />
            <CopyToClipboard value={versionIndependentUuid || id} t={t} />
          </Detail>
        </React.Fragment>
      )}
      {type === 'custom_object_type' && (
        <React.Fragment>
          <Detail title={t('detailView.versionUuid')} icon="code">
            <Default value={id} />
            <CopyToClipboard value={id} t={t} />
          </Detail>
        </React.Fragment>
      )}
      {type === 'case_type' && caseTypeContext && (
        <React.Fragment>
          <Group title={t('detailView.directLinksTitle')}>
            {caseTypeContext.some(element => element === 'internal') && (
              <Detail icon="link" offset={false}>
                <NavigateTo
                  value={caseTypeIntern}
                  title={t('detailView.internalUrl')}
                  internal={false}
                />
                <CopyToClipboard value={caseTypeIntern} t={t} />
              </Detail>
            )}
            <Detail icon="link" offset={false}>
              <NavigateTo
                value={caseTypeApi}
                title={t('detailView.caseTypeApi')}
                internal={false}
              />
              <CopyToClipboard value={caseTypeApi} t={t} />
            </Detail>
          </Group>
        </React.Fragment>
      )}
      {usedInCaseTypes?.length ? (
        <Group title={t('detailView.relationsTitle')}>
          <Detail icon="ballot" truncate={false}>
            <Default value={usedInCaseTypes} />
          </Detail>
        </Group>
      ) : null}
    </React.Fragment>
  );
};
export default Details;
