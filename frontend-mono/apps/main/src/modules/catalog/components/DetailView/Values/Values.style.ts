// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/material/styles';

export const useValuesStyle = () => {
  const {
    mintlab: { greyscale },
    palette: { primary },
  } = useTheme<Theme>();

  return {
    time: {
      marginLeft: '10px',
      color: greyscale.evenDarker,
    },
    link: {
      color: primary.main,
      display: 'flex',
      '& span': {
        marginLeft: '6px',
      },
    },
  };
};
