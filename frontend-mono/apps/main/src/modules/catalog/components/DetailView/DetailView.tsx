// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import Typography from '@mui/material/Typography';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import Box from '@mui/material/Box';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { CatalogItemType } from '../Catalog.types';
import Details from './Details/Details';
import { useDetailViewStyle } from './DetailView.style';
import { useDetailsQuery } from './DetailsView.library';

/* eslint complexity: [2, 14] */
const DetailView = ({
  toggleDetailView,
  items,
  currentFolderName,
  selected,
}: {
  toggleDetailView: () => void;
  items: CatalogItemType[];
  selected: CatalogItemType[];
  currentFolderName: string;
}) => {
  const [t] = useTranslation('catalog');
  const style = useDetailViewStyle();

  const selectedItem = selected[0];
  const selectedCount = items.length ? selected.length : 0;
  const title = t('title');

  const { data, isLoading, error } = useDetailsQuery(
    selectedCount === 1 ? selectedItem : undefined
  );
  const isLoadingAndEnabled = isLoading && selectedCount === 1;

  let detailInfo;

  if (selectedCount === 0) {
    detailInfo = {
      name: currentFolderName || title,
      icon: 'itemType.folder',
      showVersioning: false,
    };
  } else if (selectedCount === 1) {
    detailInfo = {
      name: selectedItem.name,
      icon: `entityType.${selectedItem.type}`,
      showVersioning:
        selectedItem.type === 'case_type' ||
        selectedItem.type === 'custom_object_type',
    };
  } else {
    detailInfo = {
      name: `${selectedCount} ${t('detailView:itemsSelected')}`,
      icon: 'actions.selectAll',
      showVersioning: false,
    };
  }
  return (
    <Box sx={style.detailView}>
      {error ? <ServerErrorDialog err={error as V2ServerErrorsType} /> : null}
      <Box sx={style.header}>
        <Box sx={style.headerIcon}>
          <ZsIcon size="large">{detailInfo.icon}</ZsIcon>
        </Box>
        <Typography variant="h3" sx={style.title}>
          {detailInfo.name}
        </Typography>
        <Box>
          <Button
            action={toggleDetailView}
            name="toggleDetailView"
            icon="close"
          />
        </Box>
      </Box>
      {detailInfo.showVersioning ? (
        <Box sx={style.subHeader}>
          <Box sx={style.version}>
            {`${t('detailView.versionTitle')} ${
              data && data.version ? data.version : ''
            }`}
          </Box>
        </Box>
      ) : null}
      <Box sx={style.content}>
        {isLoadingAndEnabled ? <Loader /> : data ? <Details {...data} /> : null}
      </Box>
    </Box>
  );
};

export default DetailView;
