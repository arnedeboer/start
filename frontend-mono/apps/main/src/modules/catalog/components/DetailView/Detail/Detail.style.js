// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const detailStyleSheet = ({ mintlab: { greyscale }, typography }) => ({
  wrapper: {
    margin: '30px 0px',
    position: 'relative',
    display: 'flex',
    alignItems: 'start',
    justifyContent: 'flex-start',
  },
  icon: {
    width: 50,
    height: 28,
    color: greyscale.evenDarker,
  },
  contentWrapper: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    minWidth: 0,
  },
  title: {
    color: greyscale.evenDarker,
    fontSize: typography.subtitle2.fontSize,
    marginBottom: 12,
  },
  content: {
    display: 'flex',
    alignItems: 'center',
  },
  values: {
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
  },
  truncate: {
    '&>*:first-child': {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
    },
  },
  offset: {
    marginTop: 6,
  },
});
