// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import { Box } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { capitalize } from '@mintlab/kitchen-sink/source';
import { CatalogItemType } from '../Catalog.types';
import { useButtonBarStyle } from './ButtonBar.style';

type ButtonBarPropsType = {
  selectedItems: CatalogItemType[];
  setMoveItems: (items: CatalogItemType[]) => void;
  toggleDetailView: () => void;
  moveItems: CatalogItemType[];
  editCaseType: () => void;
  editObjectType: () => void;
  editCustomObjectType: () => void;
  editAttribute: () => void;
  editDocumentTemplate: () => void;
  editEmailTemplate: () => void;
  editFolder: () => void;
  duplicateCaseType: () => void;
  deleteItem: () => void;
  initHistory: () => void;
  exportCaseType: () => void;
  changeOnlineStatus: () => void;
};

/* eslint complexity: [2, 21] */
const ButtonBar = ({
  selectedItems,
  setMoveItems,
  toggleDetailView,
  moveItems,
  editCaseType,
  editObjectType,
  editCustomObjectType,
  editAttribute,
  editDocumentTemplate,
  editEmailTemplate,
  editFolder,
  duplicateCaseType,
  deleteItem,
  initHistory,
  exportCaseType,
  changeOnlineStatus,
}: ButtonBarPropsType) => {
  const styles = useButtonBarStyle();
  const [t] = useTranslation('catalog');

  const selectedItem = selectedItems[0];
  const selectedItemType = selectedItem?.type;
  const selectedItemActive = selectedItem?.active;

  const singleSelected = selectedItems.length === 1;
  const selectedCaseType = singleSelected && selectedItemType === 'case_type';
  const selectedObjectType =
    singleSelected && selectedItemType === 'object_type';
  const editableType =
    singleSelected &&
    [
      'attribute',
      'email_template',
      'folder',
      'document_template',
      'custom_object_type',
    ].includes(selectedItemType);

  const actionButtons = [
    {
      type: 'edit',
      action: editCaseType,
      condition: selectedCaseType,
      tooltip: t('buttonBar.edit'),
      name: 'buttonBarEditCaseType',
    },
    {
      type: 'edit',
      action: editObjectType,
      condition: selectedObjectType,
      tooltip: t('buttonBar.edit'),
      name: 'buttonBarEditObjectType',
    },
    {
      type: 'file_copy',
      action: duplicateCaseType,
      condition: selectedCaseType,
      tooltip: t('buttonBar.duplicate'),
      name: 'buttonBarFileCopy',
    },
    {
      type: 'folder_move',
      action: () => setMoveItems(selectedItems),
      tooltip: t('buttonBar.move'),
      condition: selectedItems.length && !moveItems.length,
      name: 'buttonBarFolderMove',
    },
    {
      type: 'edit',
      action: () => {
        const action = {
          attribute: editAttribute,
          folder: editFolder,
          document_template: editDocumentTemplate,
          email_template: editEmailTemplate,
          custom_object_type: editCustomObjectType,
        }[selectedItemType as string];

        action && action();
      },
      tooltip: t('buttonBar.edit'),
      condition: editableType,
      name: 'buttonBarEditGeneric',
    },
    {
      type: 'history',
      action: initHistory,
      tooltip: t('buttonBar.history'),
      condition: selectedCaseType,
      name: 'buttonBarHistory',
    },
    {
      type: 'delete',
      action: deleteItem,
      tooltip: t('buttonBar.delete'),
      condition: singleSelected,
      name: 'buttonBarDelete',
    },
  ].filter(btn => btn.condition);

  const advancedActionButtons = [
    {
      action: exportCaseType,
      title: t('buttonBar.export'),
      condition: selectedCaseType,
      name: 'buttonBarExport',
    },
    {
      action: changeOnlineStatus,
      title: capitalize(
        t('changeOnlineStatus.menuTitle', {
          type:
            singleSelected && selectedItemActive
              ? t('changeOnlineStatus.offline')
              : t('changeOnlineStatus.online'),
        })
      ),
      condition: selectedCaseType,
      name: 'buttonBarChangeOnlineStatus',
    },
  ].filter(btn => btn.condition);

  const permanentButtons = [
    {
      type: 'info',
      action: toggleDetailView,
      active: true,
      tooltip: t('buttonBar.details'),
      name: 'buttonBarInfo',
    },
  ];

  return (
    <Box sx={styles.container}>
      {actionButtons.length || advancedActionButtons.length ? (
        <div style={styles.buttonList}>
          {actionButtons.length
            ? actionButtons.map(({ action, tooltip, type, name }: any) => (
                <Tooltip key={type} title={tooltip} placement="bottom">
                  <Button action={() => action()} name={name} icon={type} />
                </Tooltip>
              ))
            : null}
          {advancedActionButtons.length ? (
            <DropdownMenu
              transformOrigin={{
                vertical: -50,
                horizontal: 'center',
              }}
              trigger={<Button icon="more_vert" name="buttonBarMenuTrigger" />}
            >
              <DropdownMenuList
                items={advancedActionButtons.map(({ action, title }: any) => ({
                  action,
                  label: title,
                  scope: `catalog-header:button-bar:${title}`,
                }))}
              />
            </DropdownMenu>
          ) : null}
        </div>
      ) : null}
      <div style={styles.buttonList}>
        {permanentButtons.map(({ action, tooltip, type, name }: any) => (
          <Tooltip key={type} title={tooltip} placement="bottom">
            <Button action={action} icon={type} name={name} />
          </Tooltip>
        ))}
      </div>
    </Box>
  );
};

export default ButtonBar;
