// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICatalog } from '@zaaksysteem/generated';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { invalidateAfterChange } from '../Catalog.symbols';

const GET_EMAIL_TEMPLATE = 'GET_EMAIL_TEMPLATE';
const GET_DOCUMENT_TEMPLATE = 'GET_DOCUMENT_TEMPLATE';

export const useSaveEmailTemplateMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    APICatalog.CreateAnEmailTemplateRequestBody,
    V2ServerErrorsType,
    {
      payload: APICatalog.CreateAnEmailTemplateRequestBody;
      url: string;
    }
  >(
    ['SAVE_EMAIL_TEMPLATE'],
    payload => {
      return request<APICatalog.CreateAnEmailTemplateRequestBody>(
        'POST',
        payload.url,
        payload.payload
      );
    },
    {
      onSuccess: () => {
        client.invalidateQueries([GET_EMAIL_TEMPLATE]);
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

export const useEmailTemplateQuery = (enabled: boolean, id?: string) => {
  return useQuery<
    APICatalog.GetEmailTemplateDetailResponseBody,
    V2ServerErrorsType
  >(
    [GET_EMAIL_TEMPLATE, id],
    async () => {
      const res = await request<APICatalog.GetEmailTemplateDetailResponseBody>(
        'GET',
        buildUrl<APICatalog.GetEmailTemplateDetailRequestParams>(
          '/api/v2/admin/catalog/get_email_template_detail',
          { email_template_id: id || '' }
        )
      );

      return {
        ...res,
        data: {
          ...res.data,
          attributes: {
            ...res.data.attributes,
            attachments: res.data.attributes.attachments.map(
              ({ name, uuid }: { name: string; uuid: string }) => ({
                value: uuid,
                label: name,
              })
            ),
          },
        },
      };
    },
    { enabled }
  );
};

export const useSaveDocumentTemplateMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    APICatalog.CreateDocumentTemplateRequestBody,
    V2ServerErrorsType,
    {
      payload: APICatalog.CreateDocumentTemplateRequestBody;
      url: string;
    }
  >(
    ['SAVE_DOCUMENT_TEMPLATE'],
    payload => {
      return request<APICatalog.CreateDocumentTemplateRequestBody>(
        'POST',
        payload.url,
        payload.payload
      );
    },
    {
      onSuccess: () => {
        client.invalidateQueries([GET_DOCUMENT_TEMPLATE]);
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

export const useSaveFolderMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    APICatalog.CreateFolderRequestBody,
    V2ServerErrorsType,
    { payload: APICatalog.CreateFolderRequestBody; url: string }
  >(
    ['SAVE_DOCUMENT_TEMPLATE'],
    payload => {
      return request<APICatalog.CreateFolderRequestBody>(
        'POST',
        payload.url,
        payload.payload
      );
    },
    {
      onSuccess: () => {
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

export const useChangeOnlineStatusMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    any,
    V2ServerErrorsType,
    APICatalog.ChangeCaseTypeOnlineStatusRequestBody
  >(
    ['SAVE_DOCUMENT_TEMPLATE'],
    payload =>
      request(
        'POST',
        '/api/v2/admin/catalog/change_case_type_online_status',
        payload
      ),
    {
      onSuccess: () => {
        invalidateAfterChange(client);
        onSuccess();
      },
    }
  );
};

export const useDocumentTemplateQuery = (enabled: boolean, id?: string) => {
  return useQuery<
    APICatalog.GetDocumentTemplateDetailResponseBody,
    V2ServerErrorsType
  >(
    ['GET_DOCUMENT_TEMPLATE', id],
    () =>
      request(
        'GET',
        buildUrl<APICatalog.GetDocumentTemplateDetailRequestParams>(
          '/api/v2/admin/catalog/get_document_template_detail',
          { document_template_id: id || '' }
        )
      ),
    { enabled }
  );
};

export const fixFormDefinitionValues = (
  attributes: { [key: string]: string },
  formDefinition: FormDefinition
): FormDefinition => {
  return attributes
    ? formDefinition.map(field => ({ ...field, value: attributes[field.name] }))
    : [];
};
