// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useMutation, useQueryClient } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICatalog } from '@zaaksysteem/generated';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { CatalogItemType } from '../Catalog.types';
import { CATALOG_ITEMS } from '../Catalog.symbols';

/* eslint complexity: [2, 8] */
const getDeleteEndPointOptions = (item: CatalogItemType) => {
  const { id, type } = item;
  switch (type) {
    case 'folder':
      return {
        url: '/api/v2/admin/catalog/delete_folder',
        data: {
          folder_uuid: id,
        },
      };

    case 'attribute':
      return {
        url: '/api/v2/admin/catalog/delete_attribute',
        data: {
          attribute_uuid: id,
        },
      };

    case 'email_template':
      return {
        url: '/api/v2/admin/catalog/delete_email_template',
        data: {
          email_template_uuid: id,
        },
      };

    case 'document_template':
      return {
        url: '/api/v2/admin/catalog/delete_document_template',
        data: {
          document_template_uuid: id,
        },
      };

    case 'case_type':
      return {
        url: '/api/v2/admin/catalog/delete_case_type',
        data: {
          case_type_uuid: id,
        },
      };

    case 'object_type':
      return {
        url: '/api/v2/admin/catalog/delete_object_type',
        data: {
          object_type_uuid: id,
        },
      };

    case 'custom_object_type':
      return {
        url: '/api/v2/cm/custom_object_type/delete_custom_object_type',
        data: {
          version_independent_uuid: id,
        },
      };

    default:
      throw Error(`${type} is not supported`);
  }
};

export const useDeleteMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    APICatalog.DeleteAttributeRequestBody,
    V2ServerErrorsType,
    {
      item: CatalogItemType;
      reason: string;
    }
  >(
    ['DELETE_ITEM'],
    ({ item, reason }) => {
      const { url, data } = getDeleteEndPointOptions(item);

      return request('POST', url, {
        ...data,
        reason,
      });
    },
    {
      onSuccess: () => {
        client.invalidateQueries([CATALOG_ITEMS]);
        onSuccess();
      },
    }
  );
};
