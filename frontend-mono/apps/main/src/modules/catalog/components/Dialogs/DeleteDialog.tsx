// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { TEXT } from '../../../../components/Form/Constants/fieldTypes';
import { CatalogItemType } from '../Catalog.types';
import { useDeleteMutation } from './deleteDialog.library';

/* eslint complexity: [2, 11] */
export const useDeleteDialog = () => {
  const [t] = useTranslation('catalog');
  const [item, setOpenedData] = React.useState<false | CatalogItemType>(false);

  const {
    mutateAsync,
    isLoading: saving,
    error,
  } = useDeleteMutation(() => setOpenedData(false));

  const dialog = item && (
    <>
      {error && <ServerErrorDialog err={error} />}
      <FormDialog
        icon="error_outline"
        open={true}
        formDefinitionT={t}
        initializing={false}
        saving={saving}
        title={t('delete.confirm.title')}
        formDefinition={[
          {
            name: 'reason',
            type: TEXT,
            value: '',
            required: true,
            label: 'delete.confirm.label',
            hint: 'delete.confirm.description',
            placeholder: 'delete.confirm.placeholder',
          },
        ]}
        onClose={() => setOpenedData(false)}
        onSubmit={payload => {
          return mutateAsync({ reason: payload.reason, item });
        }}
      />
    </>
  );

  return {
    openDeleteDialog: setOpenedData,
    deleteDialog: dialog,
  };
};
