// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

export const useDocumentAttributeChoicesQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input);

  const data = useQuery(
    ['documentAttributes', input],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request(
        'GET',
        `/api/v2/admin/catalog/attribute_search?search_string=${keyword}&type=file`
      ).catch(openServerErrorDialog);

      return body
        ? (body.data || []).map((attribute: any) => ({
            value: attribute.id,
            label: attribute.attributes.name,
          }))
        : [];
    },
    { enabled }
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog] as const;
};
