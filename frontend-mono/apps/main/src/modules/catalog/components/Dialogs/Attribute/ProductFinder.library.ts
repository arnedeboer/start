// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';

export const useProductChoicesQuery = (
  appointmentUuid: string | undefined,
  locationUuid: string | undefined
) => {
  const data = useQuery<{ value: string; label: string }[], V2ServerErrorsType>(
    ['products', appointmentUuid, locationUuid],
    async ({ queryKey: [__, appointment_id, location_id] }) => {
      const body = await request<{
        result: { instance: { data: { id: string; label: string }[] } };
      }>(
        'GET',
        buildUrl(
          `/api/v1/sysin/interface/${appointment_id}/trigger/get_product_list`,
          {
            location_id,
          }
        )
      );

      return body.result.instance.data.map(product => ({
        value: product.id,
        label: product.label,
      }));
    }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
    isDisabled: data.data === undefined || data.data.length === 0,
  };

  return [selectProps, data.error] as const;
};
