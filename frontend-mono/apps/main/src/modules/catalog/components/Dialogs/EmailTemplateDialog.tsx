// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { v4 } from 'uuid';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { emailTemplateFormDefinition } from './emailTemplate.formDefinition';
import {
  useEmailTemplateQuery,
  useSaveEmailTemplateMutation,
} from './catalogDialogs.library';

/* eslint complexity: [2, 9] */
export const useEmailTemplateDialog = (folderId: string) => {
  const [t] = useTranslation('emailTemplate');
  const [openedData, setOpenedData] = React.useState<false | { id?: string }>();

  const {
    mutateAsync,
    isLoading: saving,
    error: mutationError,
  } = useSaveEmailTemplateMutation(() => {
    setOpenedData(false);
  });

  const id = openedData && openedData.id;

  const {
    data,
    isLoading,
    error: queryError,
  } = useEmailTemplateQuery(Boolean(id), id || '');

  const error = queryError || mutationError;

  const dialog = openedData && (
    <>
      {error && <ServerErrorDialog err={error} />}
      <FormDialog
        icon="email"
        open={true}
        saving={saving}
        initializing={Boolean(id) && isLoading}
        formDefinitionT={t}
        title={t('dialog.title')}
        initialValues={{
          ...(data?.data.attributes || {}),
          commit_message: id ? t('defaultEdit') : t('defaultCreate'),
        }}
        formDefinition={emailTemplateFormDefinition}
        onClose={() => {
          setOpenedData(false);
        }}
        onSubmit={async payload => {
          const url = id
            ? '/api/v2/admin/catalog/edit_email_template'
            : '/api/v2/admin/catalog/create_email_template';

          await mutateAsync({
            payload: {
              email_template_uuid: id || v4(),
              fields: {
                category_uuid: folderId || null,
                ...payload,
                attachments: payload.attachments.map(
                  ({ value, label }: { value: string; label: string }) => ({
                    name: label,
                    uuid: value,
                  })
                ),
              },
            },
            url,
          });
        }}
      />
    </>
  );

  return {
    openEmailTemplateDialog: setOpenedData,
    emailTemplateDialog: dialog,
  };
};
