// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { request } from '@zaaksysteem/common/src/library/request/request';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import { APICatalog } from '@zaaksysteem/generated';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';

const useChoices = () => {
  const [t] = useTranslation('catalog');

  const data = useQuery<any[], V2ServerErrorsType>(
    ['document_integrations'],
    async () => {
      const body =
        await request<APICatalog.GetActiveDocumentIntegrationsResponseBody>(
          'GET',
          '/api/v2/admin/integrations/get_active_document_integrations'
        );

      const integrations = body.data.map((int: any) => ({
        id: int.id,
        value: int.id,
        label: int.attributes.name,
        module: int.attributes.module,
      }));

      return [
        { id: 'default', value: 'default', label: t('system') },
        ...integrations,
      ];
    },
    { enabled: true }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
  };

  return [
    selectProps,
    data.error,
    data.status === 'success' && selectProps.choices.length === 0,
  ] as const;
};

export const DocumentIntegrationSelect: FormFieldComponentType<
  ValueType<string>,
  any
> = ({ value, multiValue, styles, config, ...restProps }) => {
  const [selectProps, error, emptyChoicesResult] = useChoices();

  return (
    <React.Fragment>
      <Select
        isClearable={false}
        {...restProps}
        {...selectProps}
        value={
          selectProps.choices.find(choice => choice.value === value) || value
        }
        disabled={Boolean(restProps?.disabled || emptyChoicesResult)}
        isMulti={multiValue}
      />
      {error && <ServerErrorDialog err={error} />}
    </React.Fragment>
  );
};
