// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';

export const useLocationChoicesQuery = (
  appointmentUuid: string | undefined
) => {
  const data = useQuery<{ value: string; label: string }[], V2ServerErrorsType>(
    ['locations', appointmentUuid],
    async ({ queryKey: [__, appointment_id] }) => {
      const body = await request<{
        result: { instance: { data: { id: string; label: string }[] } };
      }>(
        'GET',
        `/api/v1/sysin/interface/${appointment_id}/trigger/get_location_list`
      );
      return body
        ? body.result.instance.data.map(location => ({
            value: location.id,
            label: location.label,
          }))
        : [];
    }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
    isDisabled: data.data === undefined || data.data.length === 0,
  };

  return [selectProps, data.error] as const;
};
