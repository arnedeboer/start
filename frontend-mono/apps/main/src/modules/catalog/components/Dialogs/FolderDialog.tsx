// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { v4 } from 'uuid';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { useSaveFolderMutation } from './catalogDialogs.library';

export const useFolderDialog = (folderId: string) => {
  const [t] = useTranslation('catalog');
  const [openedData, setOpenedData] = React.useState<
    false | { id?: string; name?: string }
  >();

  const {
    mutateAsync,
    isLoading: saving,
    error,
  } = useSaveFolderMutation(() => {
    setOpenedData(false);
  });

  const id = openedData && openedData.id;

  const dialog = openedData && (
    <>
      {error && <ServerErrorDialog err={error} />}
      <FormDialog
        icon="folder"
        open={true}
        saving={saving}
        formDefinitionT={t}
        title={t('actions.createFolder')}
        initialValues={{ name: openedData.name || '' }}
        formDefinition={[
          {
            name: 'name',
            type: TEXT,
            value: '',
            required: true,
            label: 'folder:fields.name.label',
            placeholder: 'folder:fields.name.label',
          },
        ]}
        onClose={() => {
          setOpenedData(false);
        }}
        onSubmit={async payload => {
          const url = id
            ? '/api/v2/admin/catalog/rename_folder'
            : '/api/v2/admin/catalog/create_folder';

          await mutateAsync({
            payload: {
              name: payload.name,
              folder_uuid: id || v4(),
              //@ts-ignore
              parent_uuid: folderId || null,
            },
            url,
          });
        }}
      />
    </>
  );

  return { open: setOpenedData, dialog: dialog };
};
