// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery, useMutation, useQueryClient } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICatalog } from '@zaaksysteem/generated/types/APICatalog.types';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { CASE_TYPE_VERSION_HISTORY } from '../../Catalog.symbols';

export const useGetHistory = (id: string) =>
  useQuery<APICatalog.GetCaseTypeHistoryResponseBody, V2ServerErrorsType>(
    [CASE_TYPE_VERSION_HISTORY, id],
    () => {
      return request(
        'GET',
        buildUrl<APICatalog.GetCaseTypeHistoryRequestParams>(
          '/api/v2/admin/catalog/get_case_type_history',
          {
            case_type_id: id,
          }
        )
      );
    },
    { enabled: Boolean(id) }
  );

export const useHistoryReactivationMutation = (onSuccess: () => void) => {
  const client = useQueryClient();
  return useMutation<
    any,
    V2ServerErrorsType,
    APICatalog.UpdateCaseTypeVersionRequestBody
  >(
    ['HISTORY_REACTIVATE'],
    payload =>
      request(
        'POST',
        '/api/v2/admin/catalog/activate_case_type_version',
        payload
      ),
    {
      onSuccess: () => {
        client.invalidateQueries([CASE_TYPE_VERSION_HISTORY]);
        onSuccess();
      },
    }
  );
};
