// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import * as fieldTypes from '../../../../components/Form/Constants/fieldTypes';
import { DocumentIntegrationSelect } from './Fields/DocumentIntegrationSelect';

export const documentTemplateFormDefinition: (
  defaultCommitMessage: string
) => FormDefinition = defaultCommitMessage => [
  {
    name: 'name',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    label: 'documentTemplate:fields.name.label',
    placeholder: 'documentTemplate:fields.name.label',
  },
  {
    name: 'integration_uuid',
    component: DocumentIntegrationSelect,
    type: '',
    value: 'default',
    required: true,
    isClearable: false,
    label: 'documentTemplate:fields.integration_uuid.label',
    placeholder: 'documentTemplate:fields.integration_uuid.label',
    hint: 'form:cannotChangeLater',
  },
  {
    name: 'integration_reference',
    type: fieldTypes.TEXT,
    hidden: true,
    value: '',
    required: true,
    label: '',
  },
  {
    name: 'file',
    type: fieldTypes.UPLOAD,
    value: null,
    required: true,
    accept: ['.odt'],
    label: 'documentTemplate:fields.file.label',
    help: 'documentTemplate:fields.file.help',
    multiValue: false,
    format: 'file',
  },
  {
    name: 'help',
    type: fieldTypes.TEXT,
    isMultiline: true,
    value: '',
    required: false,
    label: 'documentTemplate:fields.help.label',
    placeholder: 'documentTemplate:fields.help.label',
  },
  {
    name: 'commit_message',
    type: fieldTypes.TEXT,
    value: defaultCommitMessage,
    required: true,
    label: 'documentTemplate:fields.commit_message.label',
    help: 'documentTemplate:fields.commit_message.help',
  },
];
