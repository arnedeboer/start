// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select, { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { useProductChoicesQuery } from './ProductFinder.library';

export const ProductFinder: FormFieldComponentType<
  ValueType<string>,
  { appointmentIntegrationUuid: string; locationUuid: string }
> = props => {
  const [selectProps, error] = useProductChoicesQuery(
    props.config?.appointmentIntegrationUuid,
    props.config?.locationUuid
  );

  return (
    <>
      <Select {...props} {...selectProps} isClearable={true} />
      {error && <ServerErrorDialog err={error} />}
    </>
  );
};
