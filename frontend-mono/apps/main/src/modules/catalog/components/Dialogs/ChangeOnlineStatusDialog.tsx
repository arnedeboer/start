// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { TEXT } from '../../../../components/Form/Constants/fieldTypes';
import { useChangeOnlineStatusMutation } from './catalogDialogs.library';

export const useChangeOnlineStatusDialog = () => {
  const [t] = useTranslation('changeOnlineStatus');
  const [openedData, setOpenedData] = React.useState<[string, boolean]>([
    '',
    false,
  ]);

  const {
    mutateAsync,
    isLoading: saving,
    error,
  } = useChangeOnlineStatusMutation(() => setOpenedData(['', false]));

  const type = openedData[1] ? 'offline' : 'online';

  const dialog = openedData[0] && (
    <>
      {error && <ServerErrorDialog err={error} />}
      <FormDialog
        icon="extension"
        open={true}
        formDefinitionT={t}
        saving={saving}
        title={t('fields.title', { type })}
        onClose={() => setOpenedData(['', false])}
        formDefinition={[
          {
            name: 'reason',
            type: TEXT,
            value: '',
            required: true,
            label: t('fields.reason.label', { type }),
            placeholder: t('fields.reason.placeholder', {
              type,
            }),
          },
        ]}
        onSubmit={values => {
          return mutateAsync({
            active: !openedData[1],
            case_type_uuid: openedData[0],
            reason: values.reason,
          });
        }}
      />
    </>
  );

  return {
    openChangeOnlineStatusDialog: setOpenedData,
    changeOnlineStatusDialog: dialog,
  };
};
