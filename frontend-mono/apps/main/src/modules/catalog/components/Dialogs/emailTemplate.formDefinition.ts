// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import * as fieldTypes from '../../../../components/Form/Constants/fieldTypes';
import DocumentAttributeFinder from './Fields/DocumentAttributeFinder';

export const emailTemplateFormDefinition: FormDefinition = [
  {
    name: 'label',
    type: fieldTypes.TEXT,
    value: name,
    required: true,
    label: 'fields.label.label',
    placeholder: 'fields.label.label',
  },
  {
    name: 'sender',
    type: fieldTypes.TEXT,
    value: '',
    required: false,
    label: 'fields.sender.label',
    placeholder: 'fields.sender.placeholder',
  },
  {
    name: 'sender_address',
    type: fieldTypes.TEXT,
    format: 'email',
    allowMagicString: true,
    value: '',
    required: false,
    label: 'fields.sender_address.label',
    placeholder: 'fields.sender_address.placeholder',
  },
  {
    name: 'subject',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    label: 'fields.subject.label',
    placeholder: 'fields.subject.label',
  },
  {
    name: 'message',
    type: fieldTypes.TEXTAREA,
    value: '',
    required: true,
    label: 'fields.message.label',
    isMultiline: true,
  },
  {
    name: 'attachments',
    component: DocumentAttributeFinder,
    type: '',
    placeholder: 'form:choose',
    value: [],
    required: false,
    label: 'fields.attachments.label',
    isMulti: true,
  },
  {
    name: 'commit_message',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    label: 'fields.commit_message.label',
    help: 'fields.commit_message.help',
  },
];
