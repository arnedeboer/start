// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material';
import Button from '@mintlab/ui/App/Material/Button';
import Typography from '@mui/material/Typography';
import { useTranslation } from 'react-i18next';

const SCOPE = 'catalog:move-bar';

//@ts-ignore
const MoveBar = ({ numToMove, moveAction, cancelAction, disabled = false }) => {
  const [t] = useTranslation('catalog');
  const {
    palette: { common, primary },
    typography,
    mintlab: { greyscale, shadows },
  } = useTheme<Theme>();

  return (
    <div
      style={{
        margin: 20,
        borderTop: `1px solid ${greyscale.light}`,
      }}
    >
      <div
        style={{
          margin: 'auto',
          display: 'flex',
          padding: '16px 24px',
          backgroundColor: primary.main,
          borderRadius: '8px',
          zIndex: 100,
          boxShadow: shadows.medium,
        }}
        data-scope={SCOPE}
      >
        <div
          style={{
            display: 'flex',
            width: 'auto',
            flexDirection: 'column',
            alignItems: 'normal',
            justifyContent: 'center',
          }}
        >
          <Typography
            variant="h5"
            sx={{
              color: common.white,
              fontWeight: typography.fontWeightRegular,
              marginBottom: '2px',
            }}
          >
            {t('move.label', { numToMove, count: numToMove })}
          </Typography>
          <Typography
            variant="caption"
            sx={{ color: common.white, fontWeight: typography.fontWeightLight }}
          >
            {t('move.description', { count: numToMove })}
          </Typography>
        </div>
        <div
          style={{
            display: 'flex',
            gap: 20,
            flex: 'auto',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}
        >
          <Button
            action={cancelAction}
            name="moveBarCancel"
            sx={{
              color: common.white,
              fontWeight: typography.fontWeightRegular,
            }}
          >
            {t('dialog:cancel')}
          </Button>
          <Button
            name="moveBarExecute"
            action={moveAction}
            icon="folder_move"
            iconSize="small"
            disabled={disabled}
            sx={{
              backgroundColor: common.white,
              color: primary.main,
              gap: 1,
              '&:hover': {
                backgroundColor: common.white,
              },
            }}
          >
            {t('move.moveButton')}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default MoveBar;
