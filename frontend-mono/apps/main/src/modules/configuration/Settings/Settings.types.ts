// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  SectionIdType,
  SettingsType,
  FieldSetType,
  FieldsType,
} from '../Configuration.types';

export type GetInitialSateType = (fields: FieldsType[]) => {
  [key: string]: any;
};

export type GetFieldSetsType = (
  sectionId: SectionIdType,
  settings: SettingsType
) => FieldSetType[];
