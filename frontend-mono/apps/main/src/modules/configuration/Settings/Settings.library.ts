// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SectionType } from '../Configuration.types';
import { GetInitialSateType, GetFieldSetsType } from './Settings.types';

export const getInitialState: GetInitialSateType = fields =>
  fields.reduce((acc, field) => ({ ...acc, [field.uuid]: field.value }), {});

export const getFieldSets: GetFieldSetsType = (sectionId, settings) => {
  const section = settings.find(
    section => section.id === sectionId
  ) as SectionType;

  return section.fieldSets;
};
