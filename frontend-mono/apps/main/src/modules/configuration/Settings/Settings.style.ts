// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useSettingsStyles = makeStyles(() => ({
  fieldSetsWrapper: {
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
    padding: 20,
  },
}));
