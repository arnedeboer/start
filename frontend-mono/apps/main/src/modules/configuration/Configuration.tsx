// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Sheet from '@mintlab/ui/App/Zaaksysteem/Sheet/Sheet';
import { PanelLayout, Panel } from '@mintlab/ui/App/Zaaksysteem/PanelLayout';
import { SectionIdType } from './Configuration.types';
import Navigation from './Navigation/Navigation';
import Settings from './Settings/Settings';
import { useSettingsQuery } from './Configuration.library';
import { useConfigurationStyles } from './Configuration.style';

type ConfigurationParamsType = {
  sectionId: SectionIdType;
};

const Configuration: React.ComponentType = () => {
  const classes = useConfigurationStyles();
  const { sectionId } = useParams<
    keyof ConfigurationParamsType
  >() as ConfigurationParamsType;

  const { data: settings, error } = useSettingsQuery();

  if (error) {
    return <ServerErrorDialog err={error} />;
  }

  if (!settings) {
    return <Loader />;
  }

  return (
    <Sheet classes={{ sheet: classes.sheet }}>
      <PanelLayout>
        <Panel type="side">
          <Navigation sectionId={sectionId} settings={settings} />
        </Panel>
        <Panel>
          <Settings sectionId={sectionId} settings={settings} />
        </Panel>
      </PanelLayout>
    </Sheet>
  );
};

export default Configuration;
