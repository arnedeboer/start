// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { fetchData, submitData } from './Configuration.requests';
import {
  FormatDefinitionsType,
  ConvertValueV1ObjectType,
  GetValueType,
  FormatItemsType,
  ItemType,
  CreateFieldsType,
  FlattenValueType,
  SaveSettingsType,
  FieldsType,
  GetFieldTypeType,
  ConvertValueV2ObjectType,
  SettingsType,
} from './Configuration.types';
import {
  creatables,
  choicesMap,
  nonClearables,
  settingsStructure,
} from './Configuration.fixtures';

/* eslint complexity: [2, 7] */
const getFieldType: GetFieldTypeType = (name, value_type_name, value_type) => {
  const type = value_type_name || value_type?.parent_type_name;

  if (creatables.includes(name)) {
    return 'creatable';
  } else if (name === 'allowed_templates' || type === 'object_ref') {
    return 'select';
  } else if (type === 'boolean') {
    return 'checkbox';
  } else if (value_type?.options?.format === 'html') {
    return 'richtext';
  } else {
    return 'text';
  }
};

const formatDefinitions: FormatDefinitionsType = definitionRows =>
  definitionRows.map(
    ({
      instance: {
        config_item_name: name,
        label,
        value_type_name,
        value_type,
        mutable,
        mvp,
      },
    }) => {
      const type = getFieldType(name, value_type_name, value_type);
      const disabled = !mutable;
      const isMultiline = value_type?.options?.style === 'paragraph';
      const choices = (
        value_type?.options?.choices || value_type?.options?.constraints?.name
      )?.map(value => ({ value, label: value }));
      // @ts-ignore
      const searchConfig = choicesMap[name];
      const isClearable = !nonClearables.includes(name);

      return {
        name,
        type,
        label,
        options: {
          isMultiline,
          rows: 10,
          disabled,
          choices,
          isMulti: mvp,
          searchConfig,
          isClearable,
        },
      };
    }
  );

export const convertValueV1Object: ConvertValueV1ObjectType = ({
  reference,
  instance: { label, name },
}) => ({
  label: label || name || '',
  value: reference,
});

export const ConvertValueV2Object: ConvertValueV2ObjectType = ({
  id,
  meta: { summary },
}) => ({
  label: summary,
  value: id,
});

/* eslint complexity: [2, 18] */
const getValue: GetValueType = (name, value) => {
  if (!value) return value;

  switch (name) {
    case 'signature_upload_role': {
      return value.instance.name;
    }

    case 'allocation_notification_template_id': // label
    case 'case_suspension_term_exceeded_notification_template_id':
    case 'case_term_exceeded_notification_template_id':
    case 'export_queue_email_template_id':
    case 'feedback_email_template_id':
    case 'new_assigned_case_notification_template_id':
    case 'new_attribute_proposal_notification_template_id':
    case 'new_document_notification_template_id':
    case 'new_ext_pip_message_notification_template_id':
    case 'new_int_pip_message_notification_template_id':
    case 'subject_pip_authorization_confirmation_template_id':
    case 'new_user_template':
    case 'case_distributor_group': // name
    case 'case_distributor_role': {
      return convertValueV1Object(value);
    }

    default: {
      return value;
    }
  }
};

const formatItems: FormatItemsType = itemRows =>
  itemRows.map(({ reference, instance: { name, value } }) => ({
    uuid: reference,
    name,
    value: getValue(name, value),
  }));

const createFields: CreateFieldsType = (definitions, items) =>
  definitions.map(definition => {
    const item = items.find(item => item.name === definition.name) as ItemType;

    return { ...definition, ...item };
  });

export const useSettingsQuery = () =>
  useQuery<SettingsType, V2ServerErrorsType>(['GET_SETTINGS'], async () => {
    const data = await fetchData();

    const definitions = formatDefinitions(data.definitions.instance.rows);
    const items = formatItems(data.items.instance.rows);
    const settings = createFields(definitions, items);

    return settingsStructure.map(({ id, icon, fieldSets }) => ({
      id,
      icon,
      fieldSets: fieldSets.map(({ title, description, fields }) => ({
        title,
        description,
        fields: fields.map(
          field =>
            settings.find(setting => setting.name === field) as FieldsType
        ),
      })),
    }));
  });

const flattenValue: FlattenValueType = value => {
  if (!value) return value;

  return typeof value === 'string' ? value : value.value;
};

export const saveSettings: SaveSettingsType = async values => {
  const data = Object.entries(values).reduce((acc, [key, value]) => {
    const flatValue = Array.isArray(value)
      ? value.map(flattenValue)
      : flattenValue(value);

    return { ...acc, [key]: { value: flatValue } };
  }, {});

  await submitData({ items: data });
};
