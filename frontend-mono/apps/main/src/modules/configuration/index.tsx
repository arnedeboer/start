// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import TopbarTitle from '@mintlab/ui/App/Zaaksysteem/Layout/Topbar/TopbarTitle/TopbarTitle';
import { useAdminBanner } from '../../library/auth';
import Configuration from './Configuration';
import locale from './Configuration.locale';

type ConfigurationRouteType = {};

const ConfigurationModule: React.ComponentType<ConfigurationRouteType> = () => {
  const [t] = useTranslation('configuration');
  const banner = useAdminBanner.system();

  return (
    banner || (
      <>
        <Configuration />
        <TopbarTitle title={t('configuration')} />
      </>
    )
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="configuration">
    <Routes>
      <Route path="" element={<Navigate to="cases" replace={true} />} />
      <Route path=":sectionId" element={<ConfigurationModule />} />
    </Routes>
  </I18nResourceBundle>
);
