// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RouteObject } from 'react-router-dom';
import { createBrowserRouter } from 'react-router-dom';
import ErrorBoundary from './components/App/ErrorBoundary';
import adminRoutes from './Routes.admin';

const lazifyComponents = (route: {
  path: string;
  Component: () => Promise<any>;
}): RouteObject => ({ ...route, Component: React.lazy(route.Component) });

const mainRoutes = {
  path: 'main/*',
  Component: React.lazy(() => import('@mintlab/ui/App/Zaaksysteem/Layout')),
  children: [
    {
      path: 'catalog/*',
      Component: () => import('./modules/catalog'),
    },
    {
      path: 'configuration/*',
      Component: () => import('./modules/configuration'),
    },
    {
      path: 'log/*',
      Component: () => import('./modules/log'),
    },
    {
      path: 'transactions/*',
      Component: () => import('./modules/transactions'),
    },
    {
      path: 'datastore/*',
      Component: () => import('./modules/datastore'),
    },
    {
      path: 'object-type/*',
      Component: () => import('./modules/objectTypeManagement'),
    },

    // {/* Admin modules iframing old frontend */}
    {
      path: 'integrations/*',
      Component: () => import('./modules/integrations'),
    },
    {
      path: 'users/*',
      Component: () => import('./modules/users'),
    },
    {
      path: 'case-type/*',
      Component: () => import('./modules/caseType'),
    },
    {
      path: 'object-type-v1/*',
      Component: () => import('./modules/objectType'),
    },
    {
      path: 'object-import/*',
      Component: () => import('./modules/objectImport'),
    },

    // {/* New modules */}
    {
      path: 'dashboard',
      Component: () => import('./modules/dashboard'),
    },
    {
      path: 'communication/*',
      Component: () => import('./modules/customerContact'),
    },
    {
      path: 'document-intake',
      Component: () => import('./modules/documentIntake'),
    },
    {
      path: 'advanced-search/*',
      Component: () => import('./modules/advancedSearch'),
    },
    {
      path: 'contact-search/*',
      Component: () => import('./modules/contactSearch'),
    },
    {
      path: 'export-files/*',
      Component: () => import('./modules/exportFiles'),
    },
    {
      path: 'contact-view/:type/:uuid/*',
      Component: () => import('./modules/contactView'),
    },
    {
      path: 'case/:caseUuid/*',
      Component: () => import('./modules/case'),
    },
    {
      path: 'object/:uuid/*',
      Component: () => import('./modules/objectView'),
    },
  ].map(lazifyComponents),
};

const routesExposedToInternIframes = {
  path: 'external-components/exposed/*',
  children: [
    {
      path: 'case/:caseUuid/*',
      Component: () => import('./modules/case-components'),
    },
    {
      path: 'tasks/:widgetUuid',
      Component: () =>
        import('./modules/dashboard/components/widgets/Tasks/Tasks'),
    },
    {
      path: 'email-template-preview',
      Component: () =>
        import('./components/EmailTemplatePreviewer/EmailTemplatePreviewer'),
    },
    {
      path: 'webodf',
      Component: () => import('./components/WebOdfEditor/WebOdfEditor'),
    },
  ].map(lazifyComponents),
};

const router = createBrowserRouter([
  {
    path: '*',
    children: [...adminRoutes, mainRoutes, routesExposedToInternIframes].map(
      (route: RouteObject) => ({
        ErrorBoundary: ErrorBoundary,
        ...route,
      })
    ),
  },
]);

export default router;
