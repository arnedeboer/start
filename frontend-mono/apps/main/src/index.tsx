// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/// <reference path='../../../packages/ui/types/Theme.d.ts'/>

import React from 'react';
import ReactDOM from 'react-dom';
import { setupI18n } from './i18n';
import App from './App';
import './index.css';

window.onerror = null;

const render = () =>
  setupI18n().then(
    () => void ReactDOM.render(<App />, document.getElementById('root'))
  );

try {
  const iframed = window.self !== window.top;
  const inExternalComps = window.location.pathname.includes(
    'external-components'
  );

  if (iframed && !inExternalComps) {
    const slug = window.location.href.split('/catalog')[1];
    window.top?.postMessage({ type: 'IFRAME:RELOAD', slug }, '*');
  } else {
    render();
  }
} catch (err) {
  render();
}
