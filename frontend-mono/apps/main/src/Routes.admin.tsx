// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';

const adminToMainDict = {
  configuratie: 'configuration',
  catalogus: 'catalog',
  logboek: 'log',
  transacties: 'transactions',
  gegevens: 'datastore',
  objecttype: 'object-type',
  objecttypen: 'object-type-v1',
  koppelingen: 'integrations',
  gebruikers: 'users',
  zaaktypen: 'case-type',
  object: 'object-import',
};

const RedirectAdmin = () => {
  const { pathname, search } = useLocation();
  const [, , adminModuleName, ...rest] = pathname.split('/');
  // @ts-ignore
  const mainModuleName = adminToMainDict[adminModuleName];
  const paths = rest.join('/');

  return (
    <Navigate
      to={`../main/${mainModuleName}/${paths}${search}`}
      replace={true}
    />
  );
};

const adminRoutes = [
  {
    path: 'admin/*',
    element: <RedirectAdmin />,
  },
];

export default adminRoutes;
