// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import ThemeProvider from '@mintlab/ui/App/Material/ThemeProvider/ThemeProvider';

import Routes from './Routes';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: Infinity,
      refetchOnWindowFocus: false,
      retry: 0,
    },
  },
});

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider>
        <Routes prefix={process.env.APP_CONTEXT_ROOT || '/my-pip'} />
      </ThemeProvider>
    </QueryClientProvider>
  );
}

export default App;
