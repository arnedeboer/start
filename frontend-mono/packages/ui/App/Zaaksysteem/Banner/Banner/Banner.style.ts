// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/material';
import { Theme } from '../../../../types/Theme';
// @ts-ignore
import secondarySvg from './svg/secondary.svg';
// @ts-ignore
import dangerSvg from './svg/danger.svg';

export const useBannnerStyle = (
  variant: 'primary' | 'secondary' | 'danger'
) => {
  const {
    palette,
    mintlab: { shadows, greyscale },
  } = useTheme<Theme>();

  const targetVariant = palette[variant];

  return {
    blackFont: { color: greyscale.black },
    outerContainer: {
      height: '36px',
      boxShadow: shadows.insetTop,
      backgroundImage: `linear-gradient(to right, ${targetVariant.light}, ${targetVariant.main})`,
    },
    innerContainer: {
      display: 'flex',
      height: '100%',
      'background-repeat': 'repeat',
      'justify-content': 'flex-end',
      'align-items': 'center',
      padding: '0px 16px',
      backgroundImage:
        variant === 'secondary'
          ? `url(${secondarySvg})`
          : variant === 'danger'
          ? `url(${dangerSvg})`
          : `linear-gradient(to right, ${targetVariant.light}, ${targetVariant.main})`,
      backgroundSize:
        variant === 'secondary'
          ? '70px 70px'
          : variant === 'danger'
          ? '12px 12px'
          : undefined,
      '&>*:not(:last-child)': {
        marginRight: '10px',
      },
    },
  };
};
