// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchKccUserStatusType,
  KccUserStatusResponseBodyType,
  ToggleKccUserStatusResponseBodyType,
  ToggleKccUserStatusType,
  FetchPhoneCallsType,
  PhoneCallsResponseBodyType,
  RejectPhoneCallsType,
  AcceptPhoneCallsType,
  FetchActivePhoneCallsType,
  ActivePhoneCallsResponseBodyType,
  ClearActivePhoneCallsType,
} from './Kcc.types';

export const fetchKccUserStatus: FetchKccUserStatusType = async () => {
  const url = '/api/kcc/user/status';

  const response = await request<KccUserStatusResponseBodyType>('GET', url);

  return response;
};

export const toggleKccUserStatus: ToggleKccUserStatusType =
  async toggleType => {
    const url = `/api/kcc/user/${toggleType}`;

    const response = await request<ToggleKccUserStatusResponseBodyType>(
      'POST',
      url
    );

    return response;
  };

export const fetchPhoneCalls: FetchPhoneCallsType = async () => {
  const url = '/api/kcc/call/list';

  const response = await request<PhoneCallsResponseBodyType>('GET', url);

  return response;
};

export const fetchActivePhoneCalls: FetchActivePhoneCallsType = async () => {
  const url = '/betrokkene/get_session';

  const response = await request<ActivePhoneCallsResponseBodyType>('GET', url);

  return response;
};

export const acceptPhoneCall: AcceptPhoneCallsType = async data => {
  const url = '/api/kcc/call/accept';

  await request('POST', url, data);
};

export const rejectPhoneCall: RejectPhoneCallsType = async data => {
  const url = '/api/kcc/call/reject';

  await request('POST', url, data);
};

export const clearActivePhoneCalls: ClearActivePhoneCallsType = async () => {
  const url = '/betrokkene/disable_session';

  await request('POST', url);
};
