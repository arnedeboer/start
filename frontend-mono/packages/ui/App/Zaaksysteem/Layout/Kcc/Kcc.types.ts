// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';

// KCC USER STATUS

export type KccUserStatusType = 1 | 0;

export type SetKccUserStatusType = (
  kccUserStatusType: KccUserStatusType
) => void;

export type KccUserStatusResponseBodyType = {
  result: { user_status: KccUserStatusType }[];
};

export type GetKccUserStatusType = () => Promise<KccUserStatusType>;

export type FetchKccUserStatusType =
  () => Promise<KccUserStatusResponseBodyType>;

// KCC TOGGLE STATUS

type ToggleType = 'enable' | 'disable';

export type ToggleKccUserStatusResponseBodyType = KccUserStatusResponseBodyType;

export type PerformToggleKccUserStatusType = (
  kccUserStatus: KccUserStatusType
) => Promise<KccUserStatusType>;

export type ToggleKccUserStatusType = (
  toggleType: ToggleType
) => Promise<ToggleKccUserStatusResponseBodyType>;

export type KccContextType = {
  hasKccIntegration: boolean;
  kccUserStatus: KccUserStatusType;
  setKccUserStatus: (kccUserStatus: KccUserStatusType) => void;
};

// KCC GET PHONE CALLS

type RawContactType = {
  uuid: string;
  type: 'natuurlijk_persoon' | 'bedrijf';
  name: string;
  telephone_numbers: string[];
  email_addresses: string[];
  street: string;
  postal_code: string;
  city: string;
};

type RawPhoneCallType = {
  id: number;
  betrokkene: RawContactType[];
};

export type PhoneCallsResponseBodyType = {
  result: RawPhoneCallType[];
};

export type PhoneCallType = {
  id: number | null;
  contact: {
    uuid: string;
    type: SubjectTypeType;
    name: string;
    phoneNumbers: string[];
    emails: string[];
    address: string;
  };
};

export type FormatContactType = (
  rawContactType: RawContactType
) => PhoneCallType['contact'];

export type FormatPhoneCallType = (
  rawPhoneCall: RawPhoneCallType
) => PhoneCallType;

export type GetPhoneCallsType = () => Promise<PhoneCallType[]>;

export type FetchPhoneCallsType = () => Promise<PhoneCallsResponseBodyType>;

// GET ACTIVE PHONE CALLS

type RawActivePhoneCallType = RawContactType;

export type ActivePhoneCallsResponseBodyType = {
  result: RawActivePhoneCallType[];
};

export type FormatActivePhoneCallType = (
  rowActivePhoneCall: RawActivePhoneCallType
) => PhoneCallType;

export type GetActivePhoneCallsType = () => Promise<PhoneCallType[]>;

export type FetchActivePhoneCallsType =
  () => Promise<ActivePhoneCallsResponseBodyType>;

// KCC HANDLE PHONE CALLS

export type PerformAcceptPhoneCallType = (id: number) => Promise<void>;

export type AcceptPhoneCallsType = (data: { call_id: number }) => Promise<void>;

export type PerformRejectPhoneCallType = (id: number) => Promise<void>;

export type RejectPhoneCallsType = (data: { call_id: number }) => Promise<void>;

export type PerformClearActivePhoneCallsType = () => Promise<void>;

export type ClearActivePhoneCallsType = () => Promise<void>;
