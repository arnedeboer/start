// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { PhoneCallType } from '../Kcc.types';
import {
  performAcceptPhoneCall,
  performRejectPhoneCall,
  performClearActivePhoneCalls,
} from '../Kcc.library';
import { usePhoneCallStyles } from './PhoneCall.styles';

type KccPropsType = {
  phoneCall: PhoneCallType;
  clearPhoneCall: (id: number) => void;
  clearActivePhoneCalls: () => void;
  setDialog: (dialog: any) => void;
};

/* eslint complexity: [2, 11] */
const PhoneCall: React.ComponentType<KccPropsType> = ({
  phoneCall,
  clearPhoneCall,
  clearActivePhoneCalls,
  setDialog,
}) => {
  const [t] = useTranslation('layout');
  const classes = usePhoneCallStyles();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [folded, setFolded] = useState<boolean>(false);

  const { id, contact } = phoneCall;
  const { uuid, type, name, address, phoneNumbers, emails } = contact;

  return (
    <div
      className={classNames(
        classes.wrapper,
        folded ? classes.folded : classes.unfolded
      )}
    >
      <div className={classes.header}>
        <Icon size="small">
          {iconNames[type === 'person' ? 'person' : 'domain']}
        </Icon>
        <span className={classes.name}>
          <a
            className={classes.link}
            href={`/main/contact-view/${type}/${uuid}`}
          >
            {name}
          </a>
        </span>

        {id && (
          <IconButton
            onClick={() =>
              performAcceptPhoneCall(id)
                .then(() => {
                  clearPhoneCall(id);
                })
                .catch(openServerErrorDialog)
            }
            color="inherit"
          >
            <Tooltip title={t('kcc.pickup')}>
              <Icon size="small">{iconNames.phone}</Icon>
            </Tooltip>
          </IconButton>
        )}
        {id && (
          <IconButton
            onClick={() =>
              performRejectPhoneCall(id)
                .then(() => {
                  clearPhoneCall(id);
                })
                .catch(openServerErrorDialog)
            }
            color="inherit"
          >
            <Tooltip title={t('kcc.hangup')}>
              <Icon size="small">{iconNames.call_end}</Icon>
            </Tooltip>
          </IconButton>
        )}

        {!id && (
          <IconButton
            onClick={() => setDialog({ type: 'case', contact })}
            color="inherit"
          >
            <Tooltip title={t('kcc.createCase')}>
              <Icon size="small">{iconNames.folder}</Icon>
            </Tooltip>
          </IconButton>
        )}
        {!id && (
          <IconButton
            onClick={() => setDialog({ type: 'contactMoment', contact })}
            color="inherit"
          >
            <Tooltip title={t('kcc.createContactMoment')}>
              <Icon size="small">{iconNames.import_contacts}</Icon>
            </Tooltip>
          </IconButton>
        )}
        {!id && (
          <IconButton
            onClick={() =>
              performClearActivePhoneCalls()
                .then(() => {
                  clearActivePhoneCalls();
                })
                .catch(openServerErrorDialog)
            }
            color="inherit"
          >
            <Tooltip title={t('kcc.deactivate')}>
              <Icon size="small">{iconNames.close}</Icon>
            </Tooltip>
          </IconButton>
        )}

        <IconButton onClick={() => setFolded(!folded)} color="inherit">
          <Tooltip title={folded ? t('kcc.unfold') : t('kcc.fold')}>
            <Icon size="small">
              {folded ? iconNames.web_asset : iconNames.remove}
            </Icon>
          </Tooltip>
        </IconButton>
      </div>
      <div className={classes.contentWrapper}>
        <div className={classes.content}>
          <div className={classes.item}>
            <Icon size="small">{iconNames.home}</Icon>
            <span>{address}</span>
          </div>
          <div className={classes.item}>
            <Icon size="small">{iconNames.phone}</Icon>
            <span>{phoneNumbers.join(', ')}</span>
          </div>
          <div className={classes.item}>
            <Icon size="small">{iconNames.email}</Icon>
            <span>{emails.join(', ') || '-'}</span>
          </div>
        </div>
      </div>
      {ServerErrorDialog}
    </div>
  );
};

export default PhoneCall;
