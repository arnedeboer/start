// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { KccContext } from '../Layout';
import { getPhoneCalls, getActivePhoneCalls } from './Kcc.library';
import Kcc from './Kcc';

// 5 seconds (yes, that's often)
const refetchInterval = 5 * 1000;

const PHONE_CALLS = 'phoneCalls';
const ACTIVE_PHONE_CALLS = 'activePhoneCalls';

const KccModule: React.ComponentType = () => {
  const { kccUserStatus } = useContext(KccContext);
  const queryClient = useQueryClient();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const onError = (error: V2ServerErrorsType) => openServerErrorDialog(error);

  const { data: phoneCalls } = useQuery(
    [PHONE_CALLS, [kccUserStatus]],
    getPhoneCalls,
    {
      onError,
      staleTime: 0,
      refetchInterval,
      initialData: [],
      enabled: Boolean(kccUserStatus),
    }
  );

  const { data: activePhoneCalls } = useQuery(
    [ACTIVE_PHONE_CALLS, kccUserStatus, phoneCalls],
    getActivePhoneCalls,
    {
      onError,
      staleTime: 0,
      initialData: [],
      enabled: Boolean(kccUserStatus),
    }
  );

  const clearPhoneCall = (id: number) => {
    queryClient.setQueriesData(
      [PHONE_CALLS],
      phoneCalls.filter(phoneCall => phoneCall.id !== id)
    );
  };

  const clearActivePhoneCalls = () => {
    queryClient.setQueriesData([ACTIVE_PHONE_CALLS], []);
  };

  return (
    <div>
      <Kcc
        phoneCalls={[...activePhoneCalls, ...phoneCalls]}
        clearPhoneCall={clearPhoneCall}
        clearActivePhoneCalls={clearActivePhoneCalls}
      />
      {ServerErrorDialog}
    </div>
  );
};

export default KccModule;
