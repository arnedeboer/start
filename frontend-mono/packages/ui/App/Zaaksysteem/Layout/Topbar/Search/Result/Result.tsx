// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { ResultType } from '../Search.types';
import { useResultStyles } from './Result.styles';
import ExtensionComponent from './ExtensionComponent';

type ResultPropsType = {
  result: ResultType;
};

/* eslint complexity: [2, 7] */
const Result: React.ComponentType<ResultPropsType> = ({ result }) => {
  const classes = useResultStyles();

  const { type, title, description, url } = result;

  return (
    <a className={classes.wrapper} href={url}>
      <div>
        <ZsIcon size="extraSmall">{`entityType.inverted.${type}`}</ZsIcon>
      </div>
      <div className={classes.content}>
        <span className={classes.title}>{title}</span>
        {description && type !== 'document' && (
          <span className={classes.description}>{description}</span>
        )}
      </div>
      <div>
        <ExtensionComponent url={url} />
      </div>
    </a>
  );
};

export default Result;
