// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    searchIn: 'Zoeken in {{system}}',
    filters: {
      all: 'Zoeken in alles',
      case: 'Zaken',
      contact: 'Contacten',
      person: 'Personen',
      organization: 'Organisaties',
      employee: 'Medewerkers',
      document: 'Documenten',
      saved_search: 'Zoekopdrachten',
      custom_object: 'Objecten',
      object_v1: 'Objecten (V1)',
      case_type: 'Zaaktypen',
      custom_object_type: 'Objecttypen',
    },
    preference: {
      set: "'{{preference}}' als voorkeur opgeslagen. Klik om te verwijderen.",
      overwrite:
        "'{{preference}}' als voorkeur opgeslagen. Klik om te overschrijven met '{{filter}}'.",
      empty: "Geen voorkeur opgeslagen. Klik om '{{filter}}' op te slaan.",
    },
    content: {
      noResults: 'Geen resultaten gevonden.',
    },
    savedSearch: {
      all: 'Alle zaken',
      mine: 'Mijn openstaande zaken',
      myDepartment: 'Mijn afdeling',
    },
  },
};

export default locale;
