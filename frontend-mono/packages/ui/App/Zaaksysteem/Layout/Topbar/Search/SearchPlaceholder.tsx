// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { useSearchStyles } from './Search.styles';

type PlaceholderPropsType = {
  setActive: () => void;
};

const SearchPlaceholder: React.ComponentType<PlaceholderPropsType> = ({
  setActive,
}) => {
  const classes = useSearchStyles();
  const [t] = useTranslation('search');

  React.useEffect(() => {
    const keyDownListener = (event: any) => {
      if (event.key === '/' && event.ctrlKey) {
        setActive();
      }
    };

    window.addEventListener('keydown', keyDownListener);

    return () => window.removeEventListener('keydown', keyDownListener);
  }, []);

  return (
    <div className={classes.placeholder}>
      <Select
        variant="generic"
        isClearable={false}
        isMulti={false}
        placeholder={t('searchIn', { system: t('common:systemName') })}
        freeSolo={true}
        onFocus={setActive}
        startAdornment={
          <Icon style={{ marginRight: '10px' }} size="small">
            {iconNames.search}
          </Icon>
        }
      />
    </div>
  );
};

export default SearchPlaceholder;
