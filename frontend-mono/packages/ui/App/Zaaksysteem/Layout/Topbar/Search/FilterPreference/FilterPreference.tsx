// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { PreferenceType, FilterType } from '../Search.types';
import { setFilterPreference } from './FilterPreference.library';

type FilterPreferencePropsType = {
  preference: PreferenceType;
  setPreference: (preference: PreferenceType) => void;
  filter: FilterType;
};

/* eslint complexity: [2, 7] */
const FilterPreference: React.ComponentType<FilterPreferencePropsType> = ({
  preference,
  setPreference,
  filter,
}) => {
  const [t] = useTranslation('search');

  const set = t('preference.set', {
    preference: t(`filters.${preference}`),
  });
  const overwrite = t('preference.overwrite', {
    preference: t(`filters.${preference}`),
    filter: t(`filters.${filter}`),
  });
  const empty = t('preference.empty', { filter: t(`filters.${filter}`) });

  return (
    <div>
      {preference && preference === filter && (
        <IconButton
          title={set}
          onClick={() => {
            setFilterPreference(null);
            setPreference(null);
          }}
        >
          <Icon size="small">{iconNames.lock}</Icon>
        </IconButton>
      )}
      {preference && preference !== filter && (
        <IconButton
          title={overwrite}
          onClick={() => {
            setFilterPreference(filter);
            setPreference(filter);
          }}
        >
          <Icon size="small">{iconNames.no_encryption}</Icon>
        </IconButton>
      )}
      {!preference && (
        <IconButton
          title={empty}
          onClick={() => {
            setFilterPreference(filter);
            setPreference(filter);
          }}
        >
          <Icon size="small">{iconNames.lock_open}</Icon>
        </IconButton>
      )}
    </div>
  );
};

export default FilterPreference;
