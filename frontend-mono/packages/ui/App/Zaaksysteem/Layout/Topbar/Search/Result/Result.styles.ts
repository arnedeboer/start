// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useResultStyles = makeStyles(
  ({
    palette: { thundercloud, cloud },
    mintlab: { greyscale, shadows },
  }: Theme) => ({
    wrapper: {
      width: 371,
      display: 'flex',
      gap: 15,
      borderRadius: 8,
      border: `1px solid ${greyscale.darker}`,
      boxShadow: shadows.medium,
      padding: 15,
      textDecoration: 'none',
      '&:hover': {
        backgroundColor: cloud.main,
      },
    },
    content: {
      flexGrow: 1,
      display: 'flex',
      gap: 15,
      flexDirection: 'column',
      overflow: 'hidden',
    },
    title: {},
    description: {
      color: thundercloud.dark,
      width: '100%',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
    extension: {
      width: 30,
      marginTop: '5px',
      border: 'none',
      background: 'none',
      cursor: 'pointer',
      alignSelf: 'baseline',
    },
  })
);
