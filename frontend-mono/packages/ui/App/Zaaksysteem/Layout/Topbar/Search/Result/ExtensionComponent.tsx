// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useResultStyles } from './Result.styles';

const ExtensionComponent: React.ComponentType<{ url: string }> = ({ url }) => {
  const classes = useResultStyles();
  const [t] = useTranslation();

  return (
    <button
      className={classes.extension}
      type="button"
      title={t('common:general.openInNewWindow')}
      onClick={event => {
        event.stopPropagation();
        event.preventDefault();
        if (url) {
          window.open(url, '_blank');
        }
      }}
    >
      <Icon size="extraSmall" color="primary">
        {iconNames.open_in_new}
      </Icon>
    </button>
  );
};

export default ExtensionComponent;
