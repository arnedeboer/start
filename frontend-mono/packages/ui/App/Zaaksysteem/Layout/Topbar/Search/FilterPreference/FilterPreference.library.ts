// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  GetFilterPreferenceType,
  SetFilterPreferenceType,
  FilterType,
} from './../Search.types';

const FILTER_PREFERENCE = 'filter-preference';

export const getFilterPreference: GetFilterPreferenceType = () =>
  (document.cookie
    .split('; ')
    .find(cookie => cookie.startsWith(FILTER_PREFERENCE))
    ?.split('=')[1] as FilterType) || null;

export const setFilterPreference: SetFilterPreferenceType = filter => {
  if (filter) {
    document.cookie = `${FILTER_PREFERENCE}=${filter}`;
  } else {
    document.cookie = `${FILTER_PREFERENCE}=remove;expires=Thu, 01 Jan 1970 00:00:01 GMT`;
  }
};
