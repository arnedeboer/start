// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Search from './Search';
import Placeholder from './SearchPlaceholder';
import locale from './Search.locale';
import { getFilterPreference } from './FilterPreference/FilterPreference.library';
import { focusOnSearch } from './Search.library';
import { PreferenceType, FilterType } from './Search.types';

const SearchModule = () => {
  const filterPreference = getFilterPreference();
  const [preference, setPreference] =
    useState<PreferenceType>(filterPreference);
  const [filter, setFilter] = useState<FilterType | null>(null);
  const active = Boolean(filter);

  const setActive = () => {
    setFilter(preference || 'case');
    focusOnSearch();
  };

  const setInactive = () => {
    setFilter(null);
  };

  return active && filter ? (
    <Search
      setInactive={setInactive}
      preference={preference}
      setPreference={setPreference}
      filter={filter}
      setFilter={setFilter}
    />
  ) : (
    <Placeholder setActive={setActive} />
  );
};

export default () => (
  <I18nResourceBundle resource={locale} namespace="search">
    <SearchModule />
  </I18nResourceBundle>
);
