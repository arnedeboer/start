// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type EventTypeType =
  | 'api/v1/update/attributes'
  | 'api/v1/update/documents'
  | 'case/contact_moment/created'
  | 'case/document/accept'
  | 'case/document/assign'
  | 'case/document/create'
  | 'case/document/reject'
  | 'case/email/created'
  | 'case/note/create'
  | 'case/note/created'
  | 'case/pip/feedback'
  | 'case/pip/updateField'
  | 'case/subject/add'
  | 'case/task/new_assignee'
  | 'case/task/set_completion'
  | 'case/update/field'
  | 'document/assign'
  | 'email/send'
  | 'user/apply'
  | 'xential/sjabloon';

type TypeType =
  | 'attribute'
  | 'contact'
  | 'contactMoment'
  | 'document'
  | 'email'
  | 'note'
  | 'pip'
  | 'task'
  | 'user'
  | 'other';

export type EventTypeDictType = { [key in EventTypeType]: TypeType };

type RawNotificationType = {
  id: number;
  is_read: 0 | 1;
  message: string;
  logging_id: {
    case_id: number;
    event_type: EventTypeType;
    timestamp: string;
  };
};

export type FetchNotificationResponseBodyType = {
  result: RawNotificationType[];
};

export type FetchNotificationsType =
  () => Promise<FetchNotificationResponseBodyType>;

export type NotificationType = {
  id: number;
  isRead: boolean;
  message: string;
  caseId?: number;
  type: TypeType;
  date: string;
  removed?: boolean;
};

export type SetNotificationsType = (notifications: NotificationType[]) => void;

export type FormatNotificationType = (
  notification: RawNotificationType
) => NotificationType;

export type GetNotificationsType = () => Promise<NotificationType[]>;

export type PerformArchiveType = (id: number) => Promise<void>;

export type ArchiveType = (body: { messages: number }) => Promise<void>;

export type PerformUndoArchiveType = (id: number) => Promise<void>;

export type UndoArchiveType = (body: { messages: number }) => Promise<void>;

export type PerformArchiveAllType = () => Promise<void>;

export type ArchiveAllType = () => Promise<void>;

export type PerformMarkReadType = (id: number) => Promise<void>;

export type MarkReadType = (body: { messages: number }) => Promise<void>;
