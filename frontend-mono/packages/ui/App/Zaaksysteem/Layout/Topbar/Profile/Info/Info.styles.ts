// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useInfoStyles = makeStyles(
  ({ typography, palette: { elephant } }: Theme) => ({
    info: {
      display: 'flex',
      flexDirection: 'column',
      gap: 20,
      padding: 20,
      overflow: 'auto',
    },
    infoItem: { display: 'flex', flexDirection: 'column', gap: 5 },
    infoLabel: {
      color: elephant.main,
      fontWeight: typography.fontWeightBold,
    },
    infoValue: {},
    roleList: { padding: '0 20px', margin: 0 },
  })
);
