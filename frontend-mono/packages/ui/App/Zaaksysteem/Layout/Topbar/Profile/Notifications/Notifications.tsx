// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import fecha from 'fecha';
import { useTheme } from '@mui/material';
import Typography from '@mui/material/Typography';
import { Button } from '@mintlab/ui/App/Material/Button';
import IconButton from '@mui/material/IconButton';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  performArchive,
  performUndoArchive,
  performArchiveAll,
  performMarkRead,
} from '../Profile.library';
import { NotificationType, SetNotificationsType } from '../Profile.types';
import { useNotificationsStyles } from './Notifications.styles';

export type NotificationsPropsType = {
  open: boolean;
  notifications?: NotificationType[];
  setNotifications: SetNotificationsType;
};

/* eslint complexity: [2, 9] */
export const Notifications: React.ComponentType<NotificationsPropsType> = ({
  open,
  notifications = [],
  setNotifications,
}) => {
  const [t] = useTranslation('layout');
  const classes = useNotificationsStyles();
  const {
    palette: { common, elephant },
  } = useTheme<Theme>();
  const [archiveAllOpen, setArchiveAllOpen] = useState<boolean>(false);
  const [snackSingle, setSnackSingle] = useState(0);
  const [snackAll, setSnackAll] = useState<boolean>(false);

  const updateNotification = (id: number, overwrite: any) => {
    setNotifications(
      notifications?.map(item =>
        item.id === id ? { ...item, ...overwrite } : item
      )
    );
  };

  const onClickArchive = async (id: number) => {
    updateNotification(id, { removed: true });

    await performArchive(id)
      .then(() => {
        setSnackSingle(id);
      })
      .catch(() => {
        updateNotification(id, { removed: false });
      });
  };

  const onClickUndoArchive = async (id: number) => {
    updateNotification(id, { removed: false });
    setSnackSingle(0);

    await performUndoArchive(id)
      .then(() => {})
      .catch(() => {
        updateNotification(id, { removed: true });
      });
  };

  const onConfirmArchiveAll = async () => {
    setNotifications(
      notifications.map(item => ({
        ...item,
        removed: true,
      }))
    );
    setArchiveAllOpen(false);

    await performArchiveAll()
      .then(() => {
        setSnackAll(true);
      })
      .catch(() => {
        setArchiveAllOpen(true);
        setNotifications(
          notifications.map(item => ({ ...item, removed: false }))
        );
      });
  };

  const onHover = async (event: any, id: number, isRead: boolean) => {
    if (event.target.id && !isRead) {
      updateNotification(id, { isRead: true });

      await performMarkRead(id).catch(() => {
        updateNotification(id, { isRead: false });
      });
    }
  };

  return (
    <div
      className={classNames(
        classes.wrapper,
        open ? classes.open : classes.close
      )}
    >
      <div className={classes.header}>
        <Typography
          variant="h3"
          color="inherit"
          classes={{ root: classes.title }}
        >
          {t('profile.notifications.title')}
        </Typography>
        <div>
          <Tooltip title={t('profile.archive.all.title')}>
            <IconButton
              name="archive-all"
              onClick={() => setArchiveAllOpen(true)}
              disabled={!notifications.length}
            >
              <Icon size="small">{iconNames.done_all}</Icon>
            </IconButton>
          </Tooltip>
        </div>
      </div>
      <div className={classes.content}>
        {!notifications && <Loader />}
        {notifications &&
          notifications.filter(item => !item.removed).length === 0 && (
            <div className={classes.noNotifications}>
              {t('profile.notifications.none')}
            </div>
          )}
        {notifications &&
          notifications.length > 0 &&
          notifications.map(
            ({ id, isRead, message, caseId, date, type, removed }) => (
              <div
                id={`${id}`}
                key={id}
                className={classNames(classes.notification, {
                  [classes.unread]: !isRead,
                  [classes.removed]: removed,
                })}
                onMouseOver={event => onHover(event, id, isRead)}
                onFocus={event => onHover(event, id, isRead)}
              >
                <div>
                  <ZsIcon size="small">{`eventTypes.inverted.${type}`}</ZsIcon>
                </div>
                <div className={classes.notificationContent}>
                  <div>
                    {caseId && (
                      <a href={`/intern/zaak/${caseId}`}>
                        {t('profile.notifications.link', { caseId })}
                      </a>
                    )}
                    {caseId && <>: </>}
                    <span>{message}</span>
                  </div>
                  <div className={classes.notificationDate}>
                    {fecha.format(new Date(date), 'DD-MM-YYYY HH:mm')}
                  </div>
                </div>
                <div className={classes.notificationAction}>
                  <IconButton name="archive" onClick={() => onClickArchive(id)}>
                    <Tooltip title={t('profile.archive.single.title')}>
                      <Icon size="small">{iconNames.done}</Icon>
                    </Tooltip>
                  </IconButton>
                </div>
              </div>
            )
          )}
      </div>
      <ConfirmDialog
        open={archiveAllOpen}
        onConfirm={onConfirmArchiveAll}
        onClose={() => setArchiveAllOpen(false)}
        title={t('profile.archive.all.title')}
        body={<div>{t('profile.archive.all.text')}</div>}
      />
      {Boolean(snackSingle) && snackSingle && (
        <Snackbar
          scope="archive-single-snack"
          handleClose={() => setSnackSingle(0)}
          message={t('profile.archive.single.snack')}
          open={Boolean(snackSingle)}
          action={
            <Button
              name="undo-archive"
              action={() => onClickUndoArchive(snackSingle)}
              iconSize="small"
              variant="outlined"
              sx={{
                color: common.white,
                border: `1px solid ${common.white}`,
                '&:hover': {
                  border: `1px solid ${common.white}`,
                  backgroundColor: elephant.darkest,
                },
              }}
            >
              {t('common:verbs.undo')}
            </Button>
          }
        />
      )}
      <Snackbar
        scope="archive-all-snack"
        handleClose={() => setSnackAll(false)}
        message={t('profile.archive.all.snack')}
        open={Boolean(snackAll)}
      />
    </div>
  );
};

export default Notifications;
