// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useNotificationsStyles = makeStyles(
  ({
    transitions,
    palette: { elephant, common, primary },
    mintlab: { greyscale },
  }: Theme) => ({
    wrapper: {
      position: 'absolute',
      height: '100%',
      zIndex: 100,
      top: 0,
      right: 320,
      backgroundColor: common.white,
      color: common.black,
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden',
    },
    open: {
      width: 320,
      borderLeft: `1px solid ${greyscale.dark}`,
      // @ts-ignore
      transition: transitions.create(['width'], {
        easing: transitions.easing.sharp,
        duration: transitions.duration.leavingScreen,
      }),
    },
    close: {
      width: 0,
      // @ts-ignore
      transition: transitions.create(['width'], {
        easing: transitions.easing.sharp,
        duration: transitions.duration.leavingScreen,
      }),
    },
    header: {
      height: 70,
      flexShrink: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      gap: 20,
      padding: '0 20px',
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    content: {
      flexGrow: 1,
      overflowX: 'hidden',
      flexDirection: 'column',
    },
    noNotifications: {
      width: '100%',
      padding: 40,
      textAlign: 'center',
    },
    title: {},
    notification: {
      width: 319,
      position: 'relative',
      display: 'flex',
      flexDirection: 'row',
      padding: 20,
      alignItems: 'flex-start',
      overflow: 'hidden',
      borderBottom: `1px solid ${greyscale.dark}`,
      '&:hover $notificationAction': { display: 'block' },
    },
    unread: {
      backgroundColor: primary.lightest,
      '& $notificationAction': {
        backgroundColor: primary.lightest,
      },
    },
    removed: {
      height: 0,
      padding: '0 20px',
      borderBottom: 0,
      // @ts-ignore
      transition: transitions.create(['height', 'padding'], {
        easing: transitions.easing.sharp,
        duration: transitions.duration.leavingScreen,
      }),
    },
    notificationContent: {
      flexGrow: 1,
      display: 'flex',
      paddingLeft: 20,
      flexDirection: 'column',
      gap: 5,
    },
    notificationDate: {
      color: elephant.main,
    },
    notificationAction: {
      position: 'absolute',
      top: 10,
      right: 10,
      padding: 10,
      backgroundColor: common.white,
      opacity: 0.9,
      borderRadius: '100%',
      display: 'none',
      '&>.MuiIconButton-root': {
        border: `1px solid ${greyscale.darker}`,
        borderRadius: '100%',
      },
    },
  })
);
