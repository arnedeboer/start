// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useButtonBarstyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { error, secondary } }: Theme) => ({
    buttonBar: {
      height: 70,
      flexShrink: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      borderBottom: `1px solid ${greyscale.dark}`,
      padding: '0 20px',
    },
    counter: {
      backgroundColor: error.main,
      top: -6,
      right: -6,
    },
    phoneIconActive: {
      color: secondary.main,
    },
  })
);
