// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useUserStyles = makeStyles(
  ({
    transitions,
    mintlab: { greyscale },
    palette: { common, error },
  }: Theme) => ({
    wrapper: {},
    open: {
      '& $panelWrapper': {
        height: '100%',
        width: 320,
        // @ts-ignore
        transition: transitions.create(['height', 'width'], {
          easing: transitions.easing.sharp,
          duration: transitions.duration.leavingScreen,
        }),
      },
    },
    close: {
      '& $title': {
        display: 'none',
      },
      '& $panelWrapper': {
        height: 70,
        width: 70,
        // @ts-ignore
        transition: transitions.create(['height', 'width'], {
          easing: transitions.easing.sharp,
          duration: transitions.duration.leavingScreen,
        }),
      },
    },
    counter: {
      backgroundColor: error.main,
      top: -4,
      right: -4,
    },
    panelWrapper: {
      position: 'absolute',
      zIndex: 100,
      top: 0,
      right: 0,
      backgroundColor: common.white,
      color: common.black,
      borderLeft: `1px solid ${greyscale.dark}`,
      display: 'flex',
      flexDirection: 'column',
    },
    panelHeader: {
      height: 70,
      flexShrink: 0,
      padding: '0 20px',
      borderBottom: `1px solid ${greyscale.dark}`,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    title: {},
    panelContent: {
      flexGrow: 1,
      overflow: 'hidden',
      display: 'flex',
      flexDirection: 'column',
    },
  })
);
