// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import { useInfoStyles } from './Info.styles';

export type InfoPropsType = {};

export const Info: React.ComponentType<InfoPropsType> = () => {
  const [t] = useTranslation('layout');
  const classes = useInfoStyles();
  const session = useSession();
  const {
    display_name,
    email,
    telephonenumber,
    organizational_unit,
    system_roles,
  } = session.logged_in_user;

  return (
    <div className={classes.info}>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.name')}</div>
        <div className={classes.infoValue}>{display_name}</div>
      </div>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.phonenumber')}</div>
        <div className={classes.infoValue}>{telephonenumber}</div>
      </div>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.email')}</div>
        <div className={classes.infoValue}>{email}</div>
      </div>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.department')}</div>
        <div className={classes.infoValue}>{organizational_unit}</div>
      </div>
      <div className={classes.infoItem}>
        <div className={classes.infoLabel}>{t('profile.info.roles')}</div>
        <div className={classes.infoValue}>
          <ul className={classes.roleList}>
            {system_roles.map(role => (
              <li key={role}>{role}</li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Info;
