// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchNotificationsType,
  FetchNotificationResponseBodyType,
  ArchiveType,
  UndoArchiveType,
  ArchiveAllType,
  MarkReadType,
} from './Profile.types';

export const fetchNotifications: FetchNotificationsType = async () => {
  const url = buildUrl('/api/message/get_for_user', { rows: 1000 });

  const response = await request<FetchNotificationResponseBodyType>('GET', url);

  return response;
};

export const archive: ArchiveType = async body => {
  const url = '/api/message/archive';

  await request('POST', url, body);
};

export const undoArchive: UndoArchiveType = async body => {
  const url = '/api/message/unarchive';

  await request('POST', url, body);
};

export const archiveAll: ArchiveAllType = async () => {
  const url = '/api/message/archive/all';

  await request('POST', url);
};

export const markRead: MarkReadType = async body => {
  const url = '/api/message/mark_read';

  await request('POST', url, body);
};
