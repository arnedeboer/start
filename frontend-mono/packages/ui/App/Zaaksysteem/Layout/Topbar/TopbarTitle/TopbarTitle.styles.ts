// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTopBarTitleStyles = makeStyles(
  ({ palette: { elephant } }: Theme) => ({
    breadCrumbs: {
      flexShrink: 0,
      overflow: 'hidden',
    },
    descriptionWrapper: {
      minWidth: 100,
      color: elephant.dark,
      overflow: 'hidden',
    },
    description: {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
    },
  })
);
