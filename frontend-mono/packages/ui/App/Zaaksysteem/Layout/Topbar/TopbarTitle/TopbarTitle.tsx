// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useContext } from 'react';
import { createPortal } from 'react-dom';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Breadcrumbs, {
  BreadcrumbItemType,
} from '../../../../Material/Breadcrumbs';
import { TopbarPortalContext } from '../../Layout';
import { useTopBarTitleStyles } from './TopbarTitle.styles';

type TopbarTitlePropsType = {
  title?: string;
  breadcrumbs?: BreadcrumbItemType[];
  description?: string;
  navigate?: (path: string) => void;
};

export const TopbarTitle: React.ComponentType<TopbarTitlePropsType> = ({
  title,
  breadcrumbs = [],
  description,
  navigate,
}) => {
  const [t] = useTranslation('layout');
  const classes = useTopBarTitleStyles();
  const topbarPortalRef = useContext(TopbarPortalContext);

  useEffect(() => {
    const pageTitle = breadcrumbs[0]?.label || title;
    const systemName = t('common:systemName');

    document.title = [pageTitle, systemName].filter(Boolean).join(' - ');
  }, []);

  const dashboardBreadcrumb: BreadcrumbItemType = {
    label: t('modules.dashboard'),
    path: '/intern',
    // Remove when intergating navigation with intern
    onItemClick: () => setTimeout(() => window.location.reload(), 0),
  };

  return topbarPortalRef?.current
    ? createPortal(
        <>
          <div className={classes.breadCrumbs}>
            <Breadcrumbs
              items={
                title
                  ? [dashboardBreadcrumb, { label: title }]
                  : [dashboardBreadcrumb, ...breadcrumbs]
              }
              maxItems={5}
              collapse={3}
            />
          </div>
          {description && (
            <div className={classes.descriptionWrapper}>
              <Tooltip title={description}>
                <Typography
                  variant="h6"
                  color="inherit"
                  classes={{ h6: classes.description }}
                >
                  {description}
                </Typography>
              </Tooltip>
            </div>
          )}
        </>,
        topbarPortalRef.current
      )
    : null;
};

export default TopbarTitle;
