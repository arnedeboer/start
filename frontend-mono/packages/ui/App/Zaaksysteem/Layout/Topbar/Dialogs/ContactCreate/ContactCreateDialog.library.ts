// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  createContact,
  fetchCountryCodes,
  fetchLegalEntityTypes,
} from './ContactCreateDialog.requests';
import {
  GetCountryCodesType,
  GetLegalEntityTypesType,
  GetErrorMessagesType,
  PerformCreateContactType,
  GetValuesType,
  FormatBooleanType,
} from './ContactCreateDialog.types';

export const getCountryCodes: GetCountryCodesType = async () => {
  const response = await fetchCountryCodes();

  const countryCodes = response.result.instance.rows.map(
    ({ instance: { dutch_code, label } }) => ({ value: dutch_code, label })
  );

  return countryCodes;
};

export const getLegalEntityTypes: GetLegalEntityTypesType = async () => {
  const response = await fetchLegalEntityTypes();

  const countryCodes = response.result.instance.rows
    .map(({ instance: { code, label, active } }) => ({
      value: code,
      label,
      active,
    }))
    .filter(({ active }) => active);

  return countryCodes;
};

const formatBoolean: FormatBooleanType = value => (value ? 1 : 0);

const getValues: GetValuesType = fields =>
  fields.reduce((acc, field) => {
    const { hidden, name, value } = field;

    if (hidden) return acc;

    const formattedValue =
      typeof value === 'boolean' ? formatBoolean(value) : value;

    return { ...acc, [name]: formattedValue };
  }, {});

const getErrorMessages: GetErrorMessagesType = (t, type, response) =>
  Object.entries(response.json.msgs || {}).reduce(
    // @ts-ignore
    (acc, [key, value]) => [...acc, `${t(`${type}.label.${key}`)}: ${value}.`],
    []
  );

export const performCreateContact: PerformCreateContactType = async (
  t,
  type,
  fields
) => {
  const params = {
    ...getValues(fields),
    betrokkene_type: type === 'person' ? 'natuurlijk_persoon' : 'bedrijf',
    confirmed: 1,
  };

  const validateResponse = await createContact({
    ...params,
    do_validation: 1,
  });

  if (validateResponse.json.success) {
    const submitResponse = await createContact(params);

    if (submitResponse.json.succes) {
      return {
        uuid: submitResponse.json.uuid,
      };
    } else {
      return {
        errors: getErrorMessages(t, type, submitResponse),
      };
    }
  } else {
    return {
      errors: getErrorMessages(t, type, validateResponse),
    };
  }
};
