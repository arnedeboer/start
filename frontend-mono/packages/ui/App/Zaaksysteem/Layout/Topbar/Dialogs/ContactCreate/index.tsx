// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@mui/material';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useErrorDialog } from '@zaaksysteem/common/src/hooks/useErrorDialog';
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar';
import { Button } from '@mintlab/ui/App/Material/Button';
import locale from './ContactCreateDialog.locale';
import {
  getCountryCodes,
  getLegalEntityTypes,
} from './ContactCreateDialog.library';
import {
  ContactCreateDialogModulePropsType,
  CountryCodeType,
  LegalEntityTypeType,
  SnackType,
} from './ContactCreateDialog.types';
import ContactCreateDialog from './ContactCreateDialog';

const ContactCreateDialogModule: React.ComponentType<
  ContactCreateDialogModulePropsType
> = props => {
  const [t] = useTranslation('contactCreate');
  const {
    palette: { common, elephant },
  } = useTheme<Theme>();
  const [errorDialog, openErrorDialog] = useErrorDialog();
  const [snack, setSnack] = useState<SnackType>();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [countryCodes, setCountryCodes] = useState<CountryCodeType[]>();
  const [legalEntityTypes, setLegalEntityTypes] =
    useState<LegalEntityTypeType[]>();

  const { open, type } = props;

  useEffect(() => {
    if (open) {
      getCountryCodes().then(setCountryCodes).catch(openServerErrorDialog);
      if (type === 'organization') {
        getLegalEntityTypes()
          .then(setLegalEntityTypes)
          .catch(openServerErrorDialog);
      }
    }
  }, [open]);

  return (
    <>
      {countryCodes && !(type === 'organization' && !legalEntityTypes) && (
        <ContactCreateDialog
          {...props}
          countryCodes={countryCodes}
          legalEntityTypes={legalEntityTypes}
          setSnack={setSnack}
          openErrorDialog={openErrorDialog}
          openServerErrorDialog={openServerErrorDialog}
        />
      )}
      {ServerErrorDialog}
      {errorDialog}
      <Snackbar
        scope="contact-created-snack"
        handleClose={() => setSnack(null)}
        message={t('snack.created')}
        open={Boolean(snack)}
        action={
          <Button
            name="open-contact"
            action={() => {
              if (snack) {
                window.location.href = snack.link;
              }
            }}
            iconSize="small"
            variant="outlined"
            sx={{
              color: common.white,
              border: `1px solid ${common.white}`,
              '&:hover': {
                border: `1px solid ${common.white}`,
                backgroundColor: elephant.darkest,
              },
            }}
          >
            {t('common:verbs.view')}
          </Button>
        }
      />
    </>
  );
};

export default (props: ContactCreateDialogModulePropsType) => (
  <I18nResourceBundle resource={locale} namespace="contactCreate">
    <ContactCreateDialogModule {...props} />
  </I18nResourceBundle>
);
