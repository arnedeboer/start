// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { SectionType } from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement.types';
import { DialogTypeType } from './Topbar.types';

export const getSections = ({
  t,
  contactAllowed,
  setDialogOpen,
}: {
  t: i18next.TFunction;
  contactAllowed: boolean;
  setDialogOpen: (dialog: DialogTypeType) => void;
}): SectionType[][] => [
  [
    {
      type: 'case',
      title: t('common:entityType.case'),
      icon: <ZsIcon>entityType.case</ZsIcon>,
      action: () => {
        setDialogOpen('case');
      },
    },
    {
      type: 'contact_moment',
      title: t('common:entityType.contact_moment'),
      icon: <ZsIcon>entityType.contact_moment</ZsIcon>,
      action() {
        setDialogOpen('contactMoment');
      },
    },
  ],
  [
    ...(contactAllowed
      ? [
          {
            type: 'person',
            title: t('common:entityType.person'),
            icon: <ZsIcon>entityType.person</ZsIcon>,
            action() {
              setDialogOpen('person');
            },
          },
          {
            type: 'organization',
            title: t('common:entityType.organization'),
            icon: <ZsIcon>entityType.organization</ZsIcon>,
            action() {
              setDialogOpen('organization');
            },
          },
        ]
      : []),
  ],
];

//@ts-ignore
export const getContactFromUrl: () => {
  uuid: string;
  type: SubjectTypeType;
} = () => {
  const parts = window.location.pathname.split('/');
  const [, , view, contactType, contactUuid] = parts;

  return view === 'contact-view'
    ? {
        type: contactType,
        uuid: contactUuid,
      }
    : null;
};
