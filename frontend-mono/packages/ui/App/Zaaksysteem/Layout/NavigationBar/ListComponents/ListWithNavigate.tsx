// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { Link, useLocation } from 'react-router-dom';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import { ListComponentPropstype } from './../NavigationBar.types';
import { useNavigationBarStyles } from './../NavigationBar.styles';
import ListItem from './ListItem';

const ListWithNavigate: React.ComponentType<ListComponentPropstype> = ({
  open,
  buttons,
}) => {
  const classes = useNavigationBarStyles();
  const currentLocation = useLocation();

  return (
    <List disablePadding>
      {/* eslint complexity: [2, 9] */}
      {buttons.map((button, index) => {
        const { link = '', action, external } = button;
        const isCurrentPage = link && currentLocation.pathname.includes(link);
        const linkWithinApp = /^\/(admin)|(main)\//.test(link);

        return (
          <ListItemButton
            key={index}
            LinkComponent={linkWithinApp ? Link : 'a'}
            {...{ to: link }}
            href={link}
            target={external ? '_new' : undefined}
            onClick={action}
            classes={{
              root: classNames(classes.button, {
                [classes.buttonActive]: isCurrentPage && !open,
                [classes.buttonOpenActive]: isCurrentPage && open,
              }),
            }}
          >
            <ListItem open={open} button={button} />
          </ListItemButton>
        );
      })}
    </List>
  );
};

export default ListWithNavigate;
