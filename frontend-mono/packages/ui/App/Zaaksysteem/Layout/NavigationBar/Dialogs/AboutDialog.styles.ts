// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAboutDialogStyles = makeStyles(({ typography }: Theme) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    gap: 30,
    margin: 10,
  },
  image: {
    width: 250,
  },
  text: {
    textAlign: 'center',
    width: 425,
  },
  table: {
    display: 'flex',
    flexDirection: 'column',
    gap: 10,
  },
  item: {
    display: 'flex',
    flexDirection: 'row',
    gap: 20,
  },
  label: {
    width: 205,
    fontWeight: typography.fontWeightBold,
    textAlign: 'right',
  },
  value: { width: 205, wordWrap: 'break-word' },
}));
