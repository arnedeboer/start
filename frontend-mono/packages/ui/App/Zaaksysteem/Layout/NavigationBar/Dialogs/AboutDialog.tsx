// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { useAboutDialogStyles } from './AboutDialog.styles';

type ItemType = {
  label: string;
  value: string;
  link?: string;
};

const fetchExternalItems = async (
  setExternalItems: (items: ItemType[]) => void
) => {
  const url = '/api/v1/general/meta';

  const response = await request('GET', url);

  const items = response.result.instance.rows
    .map(({ instance: { label, value } }: any) => ({ label, value }))
    .filter(({ value }: any) => typeof value === 'string');

  setExternalItems(items);
};

type AboutDialogPropsType = {
  onClose: () => void;
};

const AboutDialog: React.ComponentType<AboutDialogPropsType> = ({
  onClose,
}) => {
  const [t] = useTranslation('layout');
  const classes = useAboutDialogStyles();
  const dialogEl = useRef();
  const [externalItems, setExternalItems] = useState<ItemType[]>();

  useEffect(() => {
    fetchExternalItems(setExternalItems);
  }, []);

  if (!externalItems) {
    return <Loader />;
  }

  const title = t('dialog.about.title');

  const items = [
    {
      label: t('dialog.about.items.application.label'),
      value: 'xxllnc Zaken',
    },
    {
      label: t('dialog.about.items.supplier.label'),
      value: 'xxllnc Zaakgericht B.V.',
    },
    ...externalItems,
    {
      label: t('dialog.about.items.license.label'),
      value: 'EUPL',
    },
    { label: t('dialog.about.items.startDate.label'), value: '01-10-2009' },
    {
      label: t('dialog.about.items.endDate.label'),
      value: t('dialog.about.items.endDate.value'),
    },
    {
      label: t('dialog.about.items.documentation.label'),
      value: 'Zaaksysteem Wiki',
      link: 'http://wiki.zaaksysteem.nl',
    },
  ];

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={true}
        onClose={onClose}
        scope={'about-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="info"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.wrapper}>
            <img
              className={classes.image}
              alt="xxllnc-logo"
              src="/images/xxllnc-logo-zwart.png"
            />
            <span className={classes.text}>{t('dialog.about.text')}</span>
            <div className={classes.table}>
              {items.map(({ label, value, link }) => (
                <div key={value} className={classes.item}>
                  <div className={classes.label}>{label}:</div>
                  {link ? (
                    <a
                      className={classes.value}
                      rel="noreferrer"
                      target="_blank"
                      href={link}
                    >
                      {value}
                    </a>
                  ) : (
                    <div className={classes.value}>{value}</div>
                  )}
                </div>
              ))}
            </div>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default AboutDialog;
