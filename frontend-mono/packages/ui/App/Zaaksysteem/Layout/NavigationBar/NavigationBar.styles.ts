// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const widthClosed = 70;
const widthOpen = 320;

export const useNavigationBarStyles = makeStyles(
  ({ transitions, palette: { primary, cloud, common } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      overflowX: 'hidden',
      width: widthClosed,
      backgroundColor: primary.main,
    },
    open: {
      backgroundColor: cloud.light,
      color: common.black,
      width: widthOpen,
      transition: transitions.create(
        // @ts-ignore
        ['background-color', 'width', 'color'],
        {
          easing: transitions.easing.sharp,
          duration: transitions.duration.enteringScreen,
        }
      ),
      '& $menuButtonWrapper': {
        justifyContent: 'space-between',
      },
      '& $separator': {
        '&::after': {
          backgroundColor: common.black,
        },
      },
      '& $label': {
        width: '100%',
      },
      '& $environmentWrapper': {
        width: '100%',
        padding: '0 20px',
      },
    },
    close: {
      color: common.white,
      transition: transitions.create(
        // @ts-ignore
        ['background-color', 'width', 'color'],
        {
          easing: transitions.easing.sharp,
          duration: transitions.duration.leavingScreen,
        }
      ),
      '& $menuButtonWrapper': {
        justifyContent: 'center',
        borderBottom: `1px solid ${common.white}`,
      },
      '& $separator': {
        '&::after': {
          backgroundColor: common.white,
        },
      },
      '& $label': {
        width: 0,
        paddingLeft: 0,
        // @ts-ignore
        transition: transitions.create(['width'], {
          easing: transitions.easing.sharp,
          duration: transitions.duration.leavingScreen,
        }),
      },
      '& $environmentWrapper': {
        width: 0,
        paddingLeft: 0,
        // @ts-ignore
        transition: transitions.create(['width', 'padding-left'], {
          easing: transitions.easing.sharp,
          duration: transitions.duration.leavingScreen,
        }),
      },
    },
    label: {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
    },
    environmentWrapper: {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
    },
    menuButtonWrapper: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      color: common.white,
      backgroundColor: primary.main,
      height: 70,
    },
    listsWrapper: {
      display: 'flex',
      flexDirection: 'column',
      margin: '15px 0px',
    },
    separator: {
      height: 30,
      position: 'relative',
      '&::after': {
        content: '""',
        position: 'absolute',
        width: '100%',
        height: 1,
        top: 15,
      },
    },
    button: {
      '&&': {
        justifyContent: 'center',
        height: 40,
      },
    },
    buttonActive: {
      '&&': {
        backgroundColor: primary.light,
      },
    },
    buttonOpenActive: {
      '&&': {
        backgroundColor: primary.lighter,
      },
    },
    icon: {
      '&&': {
        color: 'inherit',
        justifyContent: 'center',
      },
    },
    environment: {
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
    },
  })
);
