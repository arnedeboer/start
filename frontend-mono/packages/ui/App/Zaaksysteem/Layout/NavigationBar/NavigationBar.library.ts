// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { iconNames } from '@mintlab/ui/App/Material/Icon/Icon';
import { hasCapability } from '@zaaksysteem/common/src/hooks/useSession';
import { ButtonListType, GetButtonListsType } from './NavigationBar.types';

/* eslint complexity: [2, 7] */
export const getButtons: GetButtonListsType = (t, session, setDialogOpen) => {
  const isGebruiker = hasCapability(session, 'gebruiker');
  const hasDashboard = hasCapability(session, 'dashboard');
  const internAllowed = isGebruiker && hasDashboard;

  const mainButtonList: ButtonListType = [
    {
      link: '/intern',
      icon: iconNames.home,
      label: t('modules.dashboard'),
      show: internAllowed,
    },
    {
      link: '/main/communication',
      icon: iconNames.chat_bubble,
      label: t('modules.communication'),
      show: internAllowed && hasCapability(session, 'message_intake'),
    },
    {
      link: '/main/document-intake',
      icon: iconNames.insert_drive_file,
      label: t('modules.documentIntake'),
      show:
        internAllowed && hasCapability(session, 'documenten_intake_subject'),
    },
    {
      link: '/search',
      icon: iconNames.search,
      label: t('modules.advancedSearch'),
      show: internAllowed && hasCapability(session, 'search'),
    },
    {
      link: '/main/contact-search',
      icon: iconNames.person_search,
      label: t('modules.contactSearch'),
      show: internAllowed && hasCapability(session, 'contact_search'),
    },
    {
      link: '/main/export-files',
      icon: iconNames.download,
      label: t('modules.export'),
      show: internAllowed,
    },
  ];

  const isZaaktypebeheerder = hasCapability(session, 'beheer_zaaktype_admin');
  const isGebruikersBeheerder = hasCapability(session, 'useradmin');
  const isAdmin = hasCapability(session, 'admin');

  const adminButtonList: ButtonListType = [
    {
      link: '/main/catalog',
      icon: iconNames.extension,
      label: t('modules.catalog'),
      show: isZaaktypebeheerder,
    },
    {
      link: '/main/users',
      icon: iconNames.people,
      label: t('modules.users'),
      show: isGebruikersBeheerder,
    },
    {
      link: '/main/log',
      icon: iconNames.terminal,
      label: t('modules.log'),
      show: isAdmin,
    },
    {
      link: '/main/transactions',
      icon: iconNames.import_export,
      label: t('modules.transactions'),
      show: isAdmin,
    },
    {
      link: '/main/integrations',
      icon: iconNames.link,
      label: t('modules.integrations'),
      show: isAdmin,
    },
    {
      link: '/main/datastore',
      icon: iconNames.share,
      label: t('modules.datastore'),
      show: isAdmin,
    },
    {
      link: '/main/configuration',
      icon: iconNames.settings,
      label: t('modules.configuration'),
      show: isAdmin,
    },
  ];

  const systemButtonList: ButtonListType = [
    {
      link: 'https://help.zaaksysteem.nl',
      icon: iconNames.help,
      label: t('links.help'),
      show: internAllowed,
      external: true,
    },
    {
      action: () => setDialogOpen('about'),
      icon: iconNames.info,
      label: t('links.about'),
      show: internAllowed,
    },
    {
      action: () => setDialogOpen('releaseNotes'),
      icon: iconNames.lightbulb,
      label: t('links.releaseNotes'),
      show: internAllowed,
    },
    {
      link: '/auth/logout',
      icon: iconNames.power_settings_new,
      label: t('links.logout'),
      show: true,
    },
  ];

  return [mainButtonList, adminButtonList, systemButtonList]
    .map(buttons => buttons.filter(button => button.show))
    .filter(buttons => buttons.length);
};
