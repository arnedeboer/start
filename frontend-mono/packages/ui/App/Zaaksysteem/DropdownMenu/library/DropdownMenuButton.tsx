// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import MuiButton from '@mui/material/Button';
import Icon, { IconNameType } from '../../../Material/Icon';
import { addScopeAttribute } from '../../../library/addScope';
import { useDropdownMenuButtonStyle } from '../DropdownMenu.style';

export type DropdownMenuButtonProps = {
  label: string;
  action: () => void;
  icon?: IconNameType;
  scope?: string;
};

const DropdownMenuButton = React.forwardRef(
  ({ action, icon, label, scope }: DropdownMenuButtonProps, ref) => {
    const style = useDropdownMenuButtonStyle();
    const labelComponent = icon ? (
      <Fragment>
        <Icon size="small">{icon}</Icon>
        {label}
      </Fragment>
    ) : (
      label
    );

    return (
      <MuiButton
        ref={ref as any}
        onClick={action}
        fullWidth={false}
        {...addScopeAttribute(scope, 'button')}
        sx={style}
      >
        {labelComponent}
      </MuiButton>
    );
  }
);

DropdownMenuButton.displayName = 'DropdownMenuButton';

export default DropdownMenuButton;
