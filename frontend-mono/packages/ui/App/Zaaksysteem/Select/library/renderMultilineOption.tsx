// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MultilineOption from '../Option/MultilineOption';

export const renderMultilineOption = (props: any, option: any) => {
  return <MultilineOption {...props} {...option} />;
};
