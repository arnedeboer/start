// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ValueType } from '../types/ValueType';

const clean = (value: any) => value.toString().trim().toLowerCase();

export const defaultFilterOption = <T>(
  option: string | ValueType<T>,
  input: string
) => {
  if (!input) {
    return true;
  }

  //@ts-ignore
  const { label, alternativeLabels } = option.data || {};
  const labels = [label].concat(alternativeLabels).filter(item => item);
  const matchByDataLabels = labels.some(thisLabel =>
    clean(thisLabel).includes(clean(input))
  );
  //@ts-ignore
  const matchByLabel = clean(option?.label || option).includes(clean(input));

  return matchByDataLabels || matchByLabel;
};

export default defaultFilterOption;
