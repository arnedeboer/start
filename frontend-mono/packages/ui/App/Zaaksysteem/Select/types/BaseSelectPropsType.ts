// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  AutocompleteInputChangeReason,
  AutocompleteProps,
} from '@mui/material/Autocomplete';
import { PopperProps } from '@mui/material/Popper';
import { ValueType } from './ValueType';

export type BaseSelectPropsType<T> = {
  disabled?: boolean;
  error?: boolean | string;
  loading?: boolean;
  isClearable?: boolean;
  isMulti?: boolean;
  value?: ValueType<T> | ValueType<T>[] | string | null;
  choices?: ValueType<T>[];
  onInputChange?:
    | ((
        event: React.SyntheticEvent<Element, Event>,
        value: string,
        reason: AutocompleteInputChangeReason,
        open: boolean,
        memValue: ValueType<T> | ValueType<T>[]
      ) => void)
    | undefined;
  filterOption?: (option: ValueType<T>, inputValue?: string) => boolean;
  placeholder?: string;
  name?: string;
  onBlur?: (event: React.SyntheticEvent) => void;
  onChange?: (event: React.ChangeEvent<any>) => void;
  onOpen?: (event: any) => void;
  onClose?: (event: any) => void;
  onFocus?: (event: React.FocusEvent<any>) => void;
  openMenuOnClick?: boolean;
  isOptionDisabled?: any;
  renderOption?: AutocompleteProps<
    T,
    boolean,
    boolean,
    boolean
  >['renderOption'];
  freeSolo?: AutocompleteProps<T, boolean, boolean, boolean>['freeSolo'];
  renderTags?: AutocompleteProps<T, boolean, boolean, boolean>['renderTags'];
  inputProps?: any;
  sx?: any;
  PopperComponent?: React.ElementType<PopperProps>;
};
