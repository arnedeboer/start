// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
// @ts-ignore
import sheet from './sheet.svg';

export const useSheetStyles = makeStyles(
  ({ mintlab: { shadows, greyscale }, palette: { common } }: Theme) => ({
    background: {
      'background-color': greyscale.light,
      height: '100%',
      width: '100%',
      overflowY: 'hidden',
      border: 'none',
      margin: 'auto',
      'background-image': `url(${sheet})`,
      'background-size': '18px',
      borderTopLeftRadius: 0,
      borderTopRightRadius: 0,
    },
    sheet: {
      maxWidth: 1600,
      margin: 'auto',
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
      backgroundColor: common.white,
      borderTop: `1px solid ${greyscale.dark}`,
      boxShadow: shadows.sharp,
      overflow: 'auto',
    },
  })
);
