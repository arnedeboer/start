// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import MuiButton from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';
import Icon from '../../../Material/Icon';
import { addScopeAttribute } from '../../../library/addScope';

/**
 * @param {Action} props
 * @param {Function} props.action
 * @param {string} props.icon
 * @param {string} props.label
 * @param {boolean} props.active
 * @return {ReactElement}
 */
export const VerticalMenuButton = ({ action, icon, label, active, scope }) => {
  const {
    palette: { primary },
    mintlab: { greyscale, radius },
  } = useTheme();

  const children = icon ? (
    <Fragment>
      <Icon size="small">{icon}</Icon>
      <span
        style={{
          width: '100%',
          paddingTop: '0.1rem',
          fontSize: '14px',
          textTransform: 'initial',
          marginRight: '12px',
          marginLeft: '15px',
        }}
      >
        {label}
      </span>
    </Fragment>
  ) : (
    label
  );

  return (
    <div>
      <MuiButton
        onClick={action}
        sx={{
          borderRadius: radius.horizontalMenuButton,
          backgroundColor: active ? primary.lighter : 'transparent',
          color: active ? primary.main : greyscale.darkest,
          '&:hover': {
            color: primary.main,
          },
          '$active&:hover': {
            backgroundColor: primary.lighter,
          },
          ':not($active)&:hover': {
            backgroundColor: `${greyscale.offBlack}0D`,
          },
          paddingRight: '17px',
          '& .MuiSvgIcon-root': {
            marginBottom: '5px',
          },
        }}
        {...addScopeAttribute(scope, 'button')}
      >
        {children}
      </MuiButton>
    </div>
  );
};

export default VerticalMenuButton;
