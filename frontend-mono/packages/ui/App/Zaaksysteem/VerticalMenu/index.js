// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as VerticalMenu } from './VerticalMenu';
export * from './VerticalMenu';
export default VerticalMenu;
