// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@mui/styles';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { bind, cloneWithout } from '@mintlab/kitchen-sink/source';
import equals from 'fast-deep-equal';
import { wysiwygStyleSheet } from './Wysiwyg.style';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

/**
 * Rich text editor facade for *React Draft Wysiwyg*.
 * - additional props are passed through to that component
 *
 * Dependencies {@link Render}
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Wysiwyg
 * @see /npm-mintlab-ui/documentation/consumer/manual/Wysiwyg.html
 * @see https://jpuri.github.io/react-draft-wysiwyg/#/docs
 *
 * @reactProps {boolean} disabled
 * @reactProps {string} [error]
 * @reactProps {string} name
 * @reactProps {Function} [onChange]
 * @reactProps {string} [value='']
 */
export class Wysiwyg extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      toolbar: {
        options: ['inline'],
      },
      value: '',
      disabled: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    this.state = {
      editorState: this.valueToEditorState(props.value),
      focus: false,
    };

    bind(this, 'handleBlur', 'handleChange', 'handleFocus');
  }

  // Lifecycle methods:

  /**
   * If the incoming value from props is different than that
   * of the internal state, reset the editor with the value
   * from props. This handles cases where the value needs to
   * be forcefully reset.
   *
   * @param {Object} prevProps
   * @param {Object} prevState
   */
  componentDidUpdate(prevProps, prevState) {
    const {
      valueToEditorState,
      getEditorValue,
      props: { value },
      state: { editorState },
    } = this;

    if (
      equals(editorState, prevState.editorState) &&
      !equals(getEditorValue(editorState), value)
    ) {
      this.setState({
        editorState: valueToEditorState(value),
      });
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        error,
        classes: { defaultClass, errorClass, editorClass },
        disabled,
        ...rest
      },
      state: { editorState },
      handleBlur,
      handleChange,
      handleFocus,
    } = this;

    return (
      <Editor
        editorState={editorState}
        onEditorStateChange={handleChange}
        onBlur={handleBlur}
        onFocus={handleFocus}
        wrapperClassName={classNames(defaultClass, {
          [errorClass]: error,
        })}
        toolbarClassName={classNames(defaultClass, {
          [errorClass]: error,
        })}
        editorClassName={classNames(defaultClass, editorClass, {
          [errorClass]: error,
        })}
        readOnly={disabled}
        {...cloneWithout(rest, 'onBlur', 'onChange', 'onFocus', 'value')}
      />
    );
  }

  // Custom methods:

  /**
   * @param {*} value
   * @return {*}
   */
  valueToEditorState(value) {
    const contentBlock = htmlToDraft(value);

    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(
        contentBlock.contentBlocks
      );
      const editorState = EditorState.createWithContent(contentState);

      return editorState;
    }
  }

  /**
   * @param {Object} editorState
   * @return {Object}
   */
  getEditorValue(editorState) {
    return draftToHtml(convertToRaw(editorState.getCurrentContent()));
  }

  /**
   * @param {Object} editorState
   */
  handleChange(editorState) {
    const value = this.getEditorValue(editorState);

    const { name, onChange } = this.props;

    this.setState({
      editorState,
    });

    onChange &&
      onChange({
        target: {
          name,
          value,
          type: 'html',
        },
      });
  }

  handleBlur() {
    const { onBlur, name } = this.props;
    this.setState({ focus: false });
    onBlur &&
      onBlur({
        target: {
          name,
        },
      });
  }

  /**
   * @param {Event} event
   */
  handleFocus() {
    this.setState({ focus: true });
  }
}

export default withStyles(wysiwygStyleSheet)(Wysiwyg);
