// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import {
  AutoSizer,
  Column,
  Table,
  CellMeasurerCache,
  CellMeasurer,
  Size,
} from 'react-virtualized';
import 'react-virtualized/styles.css';
import { SortDirectionType } from 'react-virtualized';
import {
  DragDropContext,
  Droppable,
  DroppableProvided,
  DraggableProvided,
  DraggableStateSnapshot,
  DraggableRubric,
} from 'react-beautiful-dnd';
//@ts-ignore
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import TableSelectionControls from '@zaaksysteem/common/src/components/TableSelectionControls/TableSelectionControls';
import { useSortableTableStyles } from './SortableTable.style';
import {
  SortableTablePropsType,
  ColumnType,
  CellRendererPropsType,
} from './SortableTable.types';
import { getSortedRows, getDefaultSorting } from './library/sorting';
import {
  getRowRenderer,
  getHeaderRenderer,
  Row,
  getVisibleColumns,
} from './SortableTable.components';

const DEFAULT_ROW_HEIGHT = 45;
const HEADER_HEIGHT = 50;

/* eslint complexity: [2, 12] */
const SortableTable: React.ComponentType<SortableTablePropsType> = ({
  rows,
  columns,
  onRowDoubleClick = () => {},
  onRowClick,
  noRowsMessage,
  noRowsProps,
  loading = false,
  rowHeight,
  selectable = false,
  styles,
  sortDirectionDefault = 'ASC',
  sorting = 'column',
  sortInternal = true,
  sortColumn,
  sortOrder,
  onSort,
  onDragEnd = () => {},
  draggingText = 'Drag and drop to re-order this row.',
  onRowsRendered,
  externalRef,
  onResize = () => {},
  headerHeight = HEADER_HEIGHT,
  onSelectPage,
  pageSelected,
  onSelectEverything,
  everythingSelected,
  selectEverythingTranslations,
  selectedRows,
}) => {
  const [sortBy, setSortBy] = useState(getDefaultSorting(columns));
  const [sortDirection, setSortDirection] = useState(
    sortDirectionDefault as SortDirectionType
  );
  const SortableTableStyles = useSortableTableStyles();
  const classes = styles || SortableTableStyles;
  const rowHeightPropOrDefault = rowHeight || DEFAULT_ROW_HEIGHT;
  const cache = new CellMeasurerCache({
    defaultHeight: rowHeightPropOrDefault,
    minHeight: rowHeightPropOrDefault,
    fixedHeight: Boolean(rowHeight),
    fixedWidth: false,
  });

  // If direction and order are provided as props, use
  // those to set state
  useEffect(() => setSortBy(sortColumn), [sortColumn]);
  useEffect(() => setSortDirection(sortOrder), [sortOrder]);

  const tableCellRenderer = (props: CellRendererPropsType) => {
    const { dataKey, rowData, parent, columnIndex, rowIndex } = props;
    const column = getVisibleColumns({
      width: props.parent.props.width,
      columns,
      sorting,
      selectable,
      classes,
    }).find((col: ColumnType) => col.name === dataKey);

    return (
      <CellMeasurer
        cache={cache}
        columnIndex={columnIndex}
        key={dataKey}
        parent={parent}
        rowIndex={rowIndex}
      >
        <div
          className={classNames(classes.tableCell, {
            [classes.tableCellFixedHeight]: Boolean(rowHeight),
          })}
        >
          {column && column.cellRenderer
            ? column.cellRenderer(props)
            : rowData[dataKey]}
        </div>
      </CellMeasurer>
    );
  };

  const rowClassName = ({ index, rowData }: any) => {
    const row = rows[index];

    return classNames(classes.flexContainer, classes.tableRow, {
      [classes.tableHeader]: index === -1,
      [classes.tableRowHover]: index >= 0 && Boolean(onRowClick),
      [classes.rowSelected]: row?.selected === true,
      [classes.rowInactive]: row?.active === false,
    });
  };

  const getRenderedColumns = (table: Size, rows: any) =>
    getVisibleColumns({
      width: table.width,
      columns,
      sorting,
      selectable,
      classes,
    }).map(({ name, width, cellRenderer, ...rest }: ColumnType) => {
      return (
        <Column
          key={name}
          width={width}
          dataKey={name}
          headerRenderer={getHeaderRenderer({
            classes,
            sorting,
            sortDirection,
            rows,
            disableSort: rest?.disableSort,
            onSelectPage,
            pageSelected,
            selectEverythingTranslations,
          })}
          cellRenderer={tableCellRenderer}
          {...rest}
        />
      );
    });

  const getSortingProps = () => {
    if (sorting === 'column') {
      return {
        sortBy,
        sortDirection,
        sort: ({
          sortBy,
          sortDirection,
        }: {
          sortBy: string;
          sortDirection: SortDirectionType;
        }) => {
          setSortBy(sortBy);
          setSortDirection(sortDirection);
          if (onSort) onSort(sortBy, sortDirection);
        },
      };
      // If column and direction are provided as props,
      // use values from local state
    } else if (sortColumn && sortOrder) {
      return {
        sortBy,
        sortDirection,
      };
    } else {
      return {};
    }
  };

  const sortedRows = getSortedRows(rows, sortBy, sortDirection, sortInternal);
  const parsedRows =
    selectable && selectedRows
      ? sortedRows.map(row => ({
          ...row,
          selected: selectedRows.includes(row.uuid),
        }))
      : sortedRows;
  const rowGetter = ({ index }: any) => parsedRows[index];
  const rowRenderer = getRowRenderer(sorting, classes);

  const baseProps = {
    headerHeight,
    rowHeight: cache.rowHeight,
    rowCount: rows.length,
    rowGetter,
    rowClassName,
    onRowDoubleClick,
    onRowClick,
    rowRenderer,
  };

  return (
    <React.Fragment>
      {loading ? (
        <Loader className={classes.loader} />
      ) : parsedRows.length ? (
        <>
          {onSelectEverything && (
            <div className={classes.tableSelectionControls}>
              <TableSelectionControls
                pageSelected={pageSelected}
                everythingSelected={everythingSelected}
                onSelectEverything={onSelectEverything}
                selectEverythingTranslations={selectEverythingTranslations}
              />
            </div>
          )}
          <AutoSizer onResize={onResize}>
            {table => {
              if (sorting === 'none' || sorting === 'column') {
                return (
                  <Table
                    {...baseProps}
                    width={table.width}
                    height={table.height}
                    onRowsRendered={onRowsRendered}
                    {...(externalRef ? { ref: externalRef } : {})}
                    {...getSortingProps()}
                  >
                    {getRenderedColumns(table, parsedRows)}
                  </Table>
                );
              } else if (sorting === 'dragdrop') {
                return (
                  <DragDropContext onDragEnd={onDragEnd}>
                    <Droppable
                      droppableId="droppable"
                      mode="virtual"
                      renderClone={(
                        provided: DraggableProvided,
                        snapshot: DraggableStateSnapshot,
                        rubric: DraggableRubric
                      ) => {
                        return (
                          <Row
                            provided={provided}
                            isDragging={snapshot.isDragging}
                            classes={classes}
                            draggingText={draggingText}
                          />
                        );
                      }}
                    >
                      {(droppableProvided: DroppableProvided) => (
                        /* do not remove this wrapper div */
                        <div style={{ width: table.width }}>
                          <Table
                            {...baseProps}
                            width={table.width}
                            height={table.height}
                            ref={ref => {
                              if (ref) {
                                // eslint-disable-next-line react/no-find-dom-node
                                const refNode = ReactDOM.findDOMNode(ref);
                                if (refNode instanceof HTMLElement) {
                                  droppableProvided.innerRef(refNode);
                                }
                              }
                            }}
                          >
                            {getRenderedColumns(table, parsedRows)}
                          </Table>
                        </div>
                      )}
                    </Droppable>
                  </DragDropContext>
                );
              }
            }}
          </AutoSizer>
        </>
      ) : (
        <div
          style={{ width: '100%' }}
          className={classes.noRowsMessage}
          {...noRowsProps}
        >
          {noRowsMessage}
        </div>
      )}
    </React.Fragment>
  );
};

export default SortableTable;
