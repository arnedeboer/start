// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
} from 'react-beautiful-dnd';
import classNames from 'classnames';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import {
  SortDirection,
  TableHeaderProps,
  SortDirectionType,
} from 'react-virtualized';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import defaultRowRenderer from './RowRenderer';
import {
  SortType,
  ColumnType,
  GetVisibleColumnsType,
  SortableTablePropsType,
  RowComponentType,
} from './SortableTable.types';

export const getRowRenderer =
  (
    sorting: SortableTablePropsType['sorting'],
    classes: SortableTablePropsType['classes']
  ) =>
  (props: any) => {
    const { index, rowData } = props;
    const normalizedId = rowData.uuid || rowData.id;

    if (sorting === 'none' || sorting === 'column') {
      return defaultRowRenderer(props);
    } else if (sorting === 'dragdrop') {
      return (
        <Draggable draggableId={normalizedId} index={index} key={normalizedId}>
          {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => {
            return (
              <Row
                provided={provided}
                isDragging={snapshot.isDragging}
                classes={classes}
                {...props}
              />
            );
          }}
        </Draggable>
      );
    }
  };

/* eslint complexity: [2, 7] */
type HeaderRendererPropsType = {
  classes: any;
  sorting: SortType;
  sortDirection: SortDirectionType;
  rows: any;
  disableSort?: boolean;
  onSelectPage?: SortableTablePropsType['onSelectPage'];
  pageSelected?: boolean;
  selectEverythingTranslations: SortableTablePropsType['selectEverythingTranslations'];
};
export const getHeaderRenderer =
  ({
    classes,
    sorting,
    sortDirection,
    rows,
    disableSort,
    onSelectPage,
    pageSelected,
    selectEverythingTranslations,
  }: HeaderRendererPropsType) =>
  ({ dataKey, label, sortBy }: TableHeaderProps) => {
    const SelectPageCmp = () => {
      const check = (
        <Checkbox
          onChange={event => {
            const { checked } = event.target;
            onSelectPage &&
              onSelectPage(
                event,
                checked,
                rows.map((row: any) => row.uuid)
              );
          }}
          checked={pageSelected}
          name={`checkbox-page`}
          color={'primary'}
        />
      );

      return selectEverythingTranslations?.selectEverything &&
        selectEverythingTranslations?.deselectEverything ? (
        <Tooltip
          placement="bottom"
          title={
            pageSelected
              ? selectEverythingTranslations.deselectEverything
              : selectEverythingTranslations.selectEverything
          }
        >
          {check}
        </Tooltip>
      ) : (
        check
      );
    };

    if (dataKey === 'checked' && onSelectPage) {
      return (
        <div className={classes.selectPage}>
          <SelectPageCmp />
        </div>
      );
    } else {
      const sortHeader = sortBy === dataKey;

      return (
        <div
          className={classNames(classes.tableCell, classes.tableCellHeader, {
            [classes.sortHeader]: sortHeader,
            [classes.tableCellHeaderDisabled]:
              disableSort === true || sorting !== 'column',
          })}
        >
          {label}
          {sortHeader ? (
            <Icon
              size="extraSmall"
              classes={{
                wrapper:
                  sortDirection === SortDirection.ASC
                    ? classes.sortAsc
                    : classes.sortDesc,
              }}
            >
              {iconNames.arrow_back}
            </Icon>
          ) : null}
        </div>
      );
    }
  };

export const Row = ({
  provided,
  isDragging,
  isScrolling,
  style = {},
  classes,
  draggingText,
  ...rest
}: RowComponentType) => {
  const newProps = {
    ...cloneWithout(rest, ''),
    ...cloneWithout(provided.draggableProps, 'style'),
    ...provided.dragHandleProps,
    ref: provided.innerRef,
    style: {
      ...style,
      ...provided.draggableProps.style,
    },
  };

  if (!isDragging) {
    return defaultRowRenderer(newProps);
  } else {
    return (
      <div {...newProps} className={classes.draggingRow}>
        {draggingText}
      </div>
    );
  }
};

export const getVisibleColumns = ({
  columns,
  width,
  sorting,
  selectable,
  classes,
}: GetVisibleColumnsType): ColumnType[] => {
  const baseColumns = [];

  if (sorting === 'dragdrop') {
    baseColumns.push({
      label: '',
      name: 'draghandle',
      width: 36,
      disableSort: true,
      flexShrink: 0,
      cellRenderer: () => (
        <Icon color="basalt" size="extraSmall">
          {iconNames.drag_indicator}
        </Icon>
      ),
    });
  }

  if (selectable) {
    baseColumns.push({
      label: '',
      name: 'checked',
      width: 40,
      disableSort: true,
      flexShrink: 0,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: any) => (
        <div className={classes.selectCell}>
          {typeof rowData.selected !== 'undefined' ? (
            <Checkbox
              checked={rowData.selected}
              name={`checkbox-${rowData.uuid}`}
              color={'primary'}
            />
          ) : null}
        </div>
      ),
    });
  }

  return [
    ...baseColumns,
    ...columns.filter(
      (thisColumn: ColumnType) =>
        !thisColumn.showFromWidth || thisColumn.showFromWidth <= width
    ),
  ];
};
