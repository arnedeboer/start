// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import classNames from 'classnames';
import Typography from '@mui/material/Typography';
import { chipStylesheet } from './Chip.style';

const Chip = ({
  label,
  renderIcon,
  classes,
  selected,
  error,
  onClick,
  onFocus,
  onBlur,
}) => (
  <button
    type="button"
    onClick={onClick}
    onFocus={onFocus}
    onBlur={onBlur}
    className={classNames(
      classes.chip,
      selected && classes.chipSelected,
      error && classes.chipError,
      renderIcon && classes.chipHasIcon
    )}
  >
    {renderIcon && typeof renderIcon === 'function' && (
      <span className={classes.icon}>{renderIcon()}</span>
    )}
    <Typography variant="body2" classes={{ root: classes.label }}>
      {label}
    </Typography>
  </button>
);

export default withStyles(chipStylesheet)(Chip);
