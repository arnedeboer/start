// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import classNames from 'classnames';
import React from 'react';
import { useCounterStyles } from './Counter.style';

type CountPropsType = {
  count?: number;
  classes?: Record<string, string>;
  children?: any;
};

const Counter: React.FunctionComponent<CountPropsType> = ({
  count,
  children,
  classes = {},
}) => {
  const counterClasses = useCounterStyles();

  if (!count) {
    return <>{children}</>;
  }

  return (
    <div className={classNames(counterClasses.wrapper, classes.wrapper)}>
      {children}
      <div className={classNames(counterClasses.counter, classes.counter)}>
        {Math.min(count, 99)}
      </div>
    </div>
  );
};

export default Counter;
