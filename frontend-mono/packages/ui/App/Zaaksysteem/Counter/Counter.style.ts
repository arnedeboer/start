// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const size = 16;

export const useCounterStyles = makeStyles(
  ({
    palette: { common, primary },
    typography: { caption, fontWeightBold },
  }: Theme) => ({
    wrapper: {
      position: 'relative',
    },
    counter: {
      width: size,
      height: size,
      borderRadius: size / 2,
      fontSize: caption.fontSize,
      fontWeight: fontWeightBold,
      position: 'absolute',
      top: size / -2,
      right: size * -1,
      textAlign: 'center',
      backgroundColor: primary.main,
      color: common.white,
      lineHeight: 1.5,
    },
  })
);
