// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/styles';

export const useSnackbarStyle = () => {
  const {
    mintlab: { greyscale, radius },
  } = useTheme<Theme>();

  return {
    root: {
      padding: '0 16px 0 24px',
      borderRadius: radius.snackbar,
      minHeight: '50px',
      display: 'flex',

      '& .MuiSnackbarContent-message': {
        flex: 1,
      },
    },
    iconButton: {
      width: '36px',
      height: '36px',
      padding: 0,
      '&:hover': {
        backgroundColor: `${greyscale.lighter}33`,
      },
    },
  };
};
