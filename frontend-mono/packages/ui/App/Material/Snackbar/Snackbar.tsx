// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiSnackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '../Icon/Icon';
import { useSnackbarStyle } from './Snackbar.style';

type SnackbarPropsType = {
  sx?: React.ComponentProps<typeof MuiSnackbar>['sx'];
  anchorOrigin?: React.ComponentProps<typeof MuiSnackbar>['anchorOrigin'];
  action?: React.ReactNode;
  autoHideDuration?: number;
  handleClose: () => void;
  closeLabel?: string;
  scope: string;
  open: boolean;
  message?: string;
  color?: string;
};

const Snackbar = ({
  scope,
  handleClose,
  autoHideDuration,
  anchorOrigin,
  message,
  closeLabel,
  action,
  open,
  color,
  sx,
}: SnackbarPropsType) => {
  const style = useSnackbarStyle();
  return (
    <MuiSnackbar
      sx={{ ...style.root, ...(color ? { color } : {}), ...sx }}
      onClose={(ev, reason) => reason !== 'clickaway' && handleClose()}
      anchorOrigin={anchorOrigin}
      open={open}
      message={message || ''}
      autoHideDuration={autoHideDuration || 5000}
      data-scope={scope}
      action={
        action || [
          <IconButton
            key="close"
            aria-label={closeLabel}
            color="inherit"
            className="close"
            onClick={handleClose}
            sx={style.iconButton}
          >
            <Icon size="small">{iconNames.close}</Icon>
          </IconButton>,
        ]
      }
    />
  );
};

export default Snackbar;
