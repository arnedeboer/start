// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as ActionFeedback } from './ActionFeedback';
export * from './ActionFeedback';
export default ActionFeedback;
