// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';
import { addScopeAttribute } from '../../../library/addScope';

/**
 * Facade for *Material-UI* `FormControlLabel`.
 *
 * @param {Object} props
 * @param {boolean} [props.disabled=false]
 * @param {string} props.label
 * @param {string} props.value
 * @param {number} index
 * @return {ReactElement}
 */
export const Choice = ({ index, disabled = false, label, value, scope }) => (
  <span key={index}>
    <FormControlLabel
      control={<Radio color={'primary'} />}
      disabled={disabled}
      label={label}
      value={value}
      {...addScopeAttribute(scope, 'choice')}
    />
  </span>
);
