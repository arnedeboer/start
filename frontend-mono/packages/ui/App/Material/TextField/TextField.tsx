// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useState } from 'react';
import classNames from 'classnames';
import { v4 as unique } from 'uuid';
import MuiTextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import { addScopeAttribute } from '../../library/addScope';
import { getEndAdornment, NumberFormatWrapper } from './TextField.library';
import { TextFieldPropsType } from './TextField.types';

const DEFAULT_ROWS = 3;

/* eslint-disable complexity */
const TextField: React.FunctionComponent<TextFieldPropsType> = ({
  classes,
  disabled = false,
  type = 'text',
  error = false,
  placeholder,
  name,
  required = false,
  rows = DEFAULT_ROWS,
  maxRows = 999,
  value,
  onChange,
  onKeyDown,
  onKeyPress,
  scope,
  InputProps,
  inputProps,
  isMultiline = false,
  autoFocus = false,
  readOnly = false,
  onBlur,
  submit,
  startAdornment,
  endAdornment,
  closeAction,
  closeName,
  disableUnderline = false,
  formatType,
  displayType = 'input',
  id,
  ariaLabel,
  sx,
}) => {
  const [focus, setFocus] = useState(false);

  const handleFocus = () => {
    setFocus(true);
  };

  const handleBlur = (event: any) => {
    setFocus(false);
    onBlur && onBlur(event);
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (onKeyPress) return onKeyPress(event);

    if (submit && event.key.toLowerCase() === 'enter' && !isMultiline) {
      submit(event);
    }
  };

  return (
    <MuiTextField
      variant="filled"
      error={error}
      type={type}
      value={value || ''}
      onChange={onChange}
      onFocus={handleFocus}
      onBlur={handleBlur}
      onKeyPress={handleKeyPress}
      onKeyDown={onKeyDown}
      disabled={disabled}
      id={id || unique()}
      placeholder={placeholder}
      name={name}
      required={required}
      multiline={isMultiline}
      minRows={rows}
      maxRows={maxRows}
      // eslint-disable-next-line
      autoFocus={autoFocus}
      sx={{
        borderRadius: '8px',
        backgroundColor: focus ? '#f1f1f3' : '#F7F7F8',
        '&:hover': disabled
          ? {}
          : {
              backgroundColor: '#f1f1f3',
            },
        '& .MuiInputBase-input': {
          padding: `10px 15px 8px ${startAdornment ? '0' : '15px'}`,
          borderRadius: '8px',
        },
        '& .MuiInputBase-root': {
          backgroundColor: 'transparent',
          '&:hover': disabled
            ? {}
            : {
                backgroundColor: 'transparent',
              },
        },
        ...(sx || {}),
      }}
      classes={{
        root: classNames(
          readOnly ? classes?.formControlReadonly : classes?.formControl,
          {
            [classes?.focus as any]: focus,
            [classes?.formControlError as any]: Boolean(error),
          }
        ),
      }}
      InputProps={{
        readOnly,
        disableUnderline,
        classes: {
          root: classNames(classes?.inputRoot, {
            [classes?.inputRootError as any]: Boolean(error),
            [classes?.inputRootErrorFocus as any]: Boolean(error) && focus,
          }),
          input: classNames(classes?.input, {
            [classes?.inputError as any]: Boolean(error),
          }),
        },
        startAdornment: startAdornment ? (
          <InputAdornment sx={{ marginBottom: '14px' }} position="start">
            {startAdornment}
          </InputAdornment>
        ) : null,
        ...(Boolean(closeAction || endAdornment) && {
          endAdornment: getEndAdornment({
            focusClass: classes?.focus,
            closeAction,
            closeName,
            endAdornment,
            endAdornmentClass: classes?.endAdornment,
            focus,
          }),
        }),
        ...(formatType && { inputComponent: NumberFormatWrapper as any }),
        ...InputProps,
      }}
      inputProps={{
        'aria-label': ariaLabel || placeholder || '',
        ...addScopeAttribute(scope, 'text-field-input'),
        ...(formatType && { formatType, displayType }),
        ...(inputProps || {}),
      }}
      {...addScopeAttribute(scope, 'text-field')}
    />
  );
};

export default TextField;
