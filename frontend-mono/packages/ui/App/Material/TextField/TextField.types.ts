// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { NumberFormatProps } from 'react-number-format';
import { StandardTextFieldProps } from '@mui/material/TextField';
import { formatTypes } from './TextField.library';

type ClassNames =
  | 'input'
  | 'inputError'
  | 'inputRoot'
  | 'inputRootError'
  | 'inputRootErrorFocus'
  | 'formControl'
  | 'formControlError'
  | 'formControlReadonly'
  | 'focus'
  | 'endAdornment'
  | 'startAdornment';

export interface TextFieldPropsType
  extends Omit<StandardTextFieldProps, 'classes' | 'variant'> {
  scope?: string;
  readOnly?: boolean;
  formatType?: NumberFormatProps;
  displayType?: 'text' | 'input' | 'password';
  isMultiline?: boolean;
  submit?: (event: React.ChangeEvent | React.KeyboardEvent<any>) => void;
  startAdornment?: React.ReactChild;
  endAdornment?: React.ReactChild;
  closeAction?: () => void;
  closeName?: string;
  disableUnderline?: boolean;
  classes: { [P in ClassNames]?: string };
  ariaLabel?: string;
}

export interface EntrypointPropsType
  extends Omit<TextFieldPropsType, 'formatType' | 'error' | 'classes'> {
  classes?: { [P in ClassNames]?: string };
  variant?: 'form' | 'generic1' | 'generic2';
  formatType?: keyof typeof formatTypes | NumberFormatProps;
  error?: boolean | string;
}
