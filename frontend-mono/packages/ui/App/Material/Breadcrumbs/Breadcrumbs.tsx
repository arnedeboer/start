// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link } from 'react-router-dom';
import MUIBreadcrumbs from '@mui/material/Breadcrumbs';
import { addScopeAttribute, addScopeProp } from '../../library/addScope';
import Icon, { iconNames } from '../Icon/Icon';
import { useBreadcrumbStyles } from './Breadcrumbs.style';
import { getItemsToRender } from './library/functions';
import {
  BreadcrumbRendererType,
  BreadcrumbsPropsType,
  BreadcrumbItemPropsType,
} from './Breadcrumbs.types';

const ZERO = 0;
const ONE = 1;

const Separator = ({ classes }: { classes: any }) => (
  <Icon
    size="small"
    classes={{
      root: classes.separator,
    }}
  >
    {iconNames.navigate_next}
  </Icon>
);

const defaultItemRenderer: BreadcrumbRendererType = (
  { classes, onItemClick, item, index, scope },
  isLastItem
) => {
  const className = isLastItem ? classes.last : classes.link;
  const onClick = item.onItemClick || onItemClick;

  return item.path ? (
    <Link
      key={index}
      to={item.path}
      onClick={onClick}
      className={className}
      {...(isLastItem && { 'aria-current': 'page' })}
      {...addScopeAttribute(scope, `item-${index === ZERO ? 'first' : index}`)}
    >
      {item.label}
    </Link>
  ) : (
    <div className={className} key={index}>
      {item.label}
    </div>
  );
};

export const defaultLastItemRenderer: BreadcrumbRendererType = ({
  item,
  classes,
  index,
  scope,
}) => (
  <span
    key={index}
    className={classes.last}
    {...addScopeAttribute(scope, 'item-last')}
  >
    {item.label}
  </span>
);

export const Breadcrumbs: React.ComponentType<BreadcrumbsPropsType> = ({
  maxItems,
  items,
  collapse = 2,
  onItemClick,
  itemRenderer = defaultItemRenderer,
  lastItemRenderer = defaultItemRenderer,
  scope,
  ...restProps
}) => {
  const classes = useBreadcrumbStyles();
  const itemsToRender = maxItems
    ? getItemsToRender(items, maxItems, collapse)
    : items;

  return (
    <MUIBreadcrumbs
      separator={<Separator classes={classes} />}
      {...addScopeAttribute(scope, 'breadcrumbs')}
      {...restProps}
      classes={{ root: classes.wrapper }}
    >
      {itemsToRender &&
        itemsToRender.map((item, index) => {
          const itemProps: BreadcrumbItemPropsType = {
            item,
            index,
            onItemClick,
            classes,
            ...addScopeProp(scope, 'breadcrumbs'),
          };

          return index < itemsToRender.length - ONE
            ? itemRenderer(itemProps, false)
            : lastItemRenderer(itemProps, true);
        })}
    </MUIBreadcrumbs>
  );
};

export default Breadcrumbs;
