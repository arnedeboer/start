// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, action, text, boolean } from '../../story';
import Button from '../Button/Button';
import { Dialog } from './Dialog/Dialog';
import { DialogContent } from './DialogContent/DialogContent';
import { DialogTitle } from './DialogTitle/DialogTitle';
import DialogActions from './DialogActions/DialogActions';
import Divider from './Divider/Divider';

stories(module, __dirname, {
  /* eslint complexity: [2, 5] */
  Default() {
    const elevated = boolean('Elevate header', true);
    const showDivider = boolean('Add divider', true);

    return (
      <Dialog open={true} scope="story">
        <DialogTitle
          title={text('Title', 'Dialog title')}
          scope="story"
          elevated={elevated}
          icon={boolean('Icon', true) ? 'alarm' : ''}
          onCloseClick={boolean('Close button', true) ? () => {} : undefined}
        />
        <DialogContent>
          {text(
            'Text',
            [
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
              'Vivamus vitae urna dignissim lacus auctor euismod.',
              'Suspendisse auctor vehicula tellus, quis iaculis dolor.',
              'Curabitur commodo lorem eget ex egestas cursus.',
              'Integer dolor nibh, tincidunt mattis pharetra et, gravida ornare mi.',
              'Cras blandit nisi nec efficitur commodo.',
            ].join(' ')
          )}
        </DialogContent>
        {showDivider && <Divider />}
        <DialogActions elevated={elevated}>
          <Button onClick={action('Cancel')}>Cancel</Button>
          <Button onClick={action('OK')}>OK</Button>
        </DialogActions>
      </Dialog>
    );
  },
});
