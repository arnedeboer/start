// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export { Dialog } from './Dialog/Dialog';
export { DialogTitle } from './DialogTitle/DialogTitle';
