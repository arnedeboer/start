// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import { withTheme } from '@mui/styles';
import Icon from './../Icon';
import { iconRoundedStyleSheet } from './IconRounded.style';

/**
 * *Material Design* **RoundedIcon**.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/RoundedIcon
 * @see /npm-mintlab-ui/documentation/consumer/manual/Icon.html
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export const IconRounded = props => {
  const { classes, theme, size, backgroundColor } = props;
  const iconSize = Number.isInteger(size) ? size : theme.mintlab.icon[size];
  const backgroundSize = iconSize * 1.25 * 1.25; // two size iterations bigger

  return (
    <Icon
      {...props}
      classes={{ root: classes.icon, wrapper: classes.wrapper }}
      size={iconSize}
      style={{
        borderRadius: '50%',
        width: `${backgroundSize}px`,
        height: `${backgroundSize}px`,
        backgroundColor,
        ...props.style,
      }}
    >
      {props.children}
    </Icon>
  );
};

export default withTheme(withStyles(iconRoundedStyleSheet)(IconRounded));
