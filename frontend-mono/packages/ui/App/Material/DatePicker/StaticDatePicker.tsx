// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import { StaticDatePicker as DatePicker } from '@mui/x-date-pickers/StaticDatePicker';
import TextField from '../TextField';
import { DatePickerPropsType } from './DatePicker.types';
import LocalizationProvider from './LocalizationProvider';

export const StaticDatePicker = ({
  value,
  onChange,
  name,
  clearable = true,
  disabled = false,
  displayFormat = 'dd/mm/yyyy',
  placeholder = 'dd/mm/jjjj',
  outputFormat = 'ISO',
  sx,
  ...rest
}: DatePickerPropsType) => {
  const handleChange = (newValue: any) => {
    let returnValue;
    if (outputFormat === 'ISO') {
      //@ts-ignore
      const dateObj = new Date(newValue);
      returnValue = dateObj.toISOString();
    } else if (outputFormat === 'DATE') {
      returnValue = newValue;
    } else {
      returnValue = fecha.format(newValue, outputFormat);
    }

    onChange &&
      onChange({
        target: {
          name: name || '',
          value: returnValue,
        },
      });
  };

  return (
    <LocalizationProvider>
      <DatePicker
        disabled={disabled}
        value={value}
        onChange={handleChange}
        onAccept={handleChange}
        inputFormat={displayFormat}
        components={{ ActionBar: () => null }}
        renderInput={params => (
          //@ts-ignore
          <TextField
            {...params}
            inputProps={{ ...params.inputProps, placeholder, name }}
          />
        )}
        {...rest}
      />
    </LocalizationProvider>
  );
};

export default StaticDatePicker;
