// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { stories, select, text } from '../../story';
import LoadableTimePicker from '.';

stories(module, __dirname, {
  Default() {
    const [value, setValue] = useState(null);
    return (
      <div style={{ width: '300px', backgroundColor: '#f5f5f5' }}>
        <LoadableTimePicker
          name="story"
          value={value}
          onClose={() => {}}
          variant={select(
            'Type',
            ['dialog', '@mui/x-date-pickers', 'static'],
            '@mui/x-date-pickers'
          )}
          placeholder={text('Placeholder', 'Tijd toevoegen')}
          fullWidth
          ampm={false}
          onChange={value => {
            const dateObj = new Date(value);
            setValue(dateObj.toISOString());
          }}
        />
      </div>
    );
  },
});
