// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import IconButton from '@mui/material/IconButton';
import MuiButton from '@mui/material/Button';
import { SxProps, useTheme } from '@mui/material';
import Icon, { IconSizeType, IconNameType } from '../Icon';

type ButtonPropsType = {
  name: string;
  action?: (ev: React.MouseEvent) => any;
  onClick?: (ev: React.MouseEvent) => any;
  component?:
    | React.ReactElement
    | React.ForwardRefExoticComponent<any>
    | string;
  sx?: SxProps<Theme>;
  disabled?: boolean;
  label?: string;
  scope?: string;
  icon?: IconNameType;
  iconSize?: IconSizeType;
  variant?: 'text' | 'outlined' | 'contained' | 'default' | 'link';
  className?: string;
  title?: string;
  fullWidth?: boolean;
  endIcon?: any;
  iconTransform?: string;
  children?: any;
  color?: any;
};

// To make a button with just text, pass text as children
// To make an icon with button effects, pass icon prop
// If you pass both pass icon prop and pass text as children the
// icon will be rendered as adornment to the button text

/* eslint complexity: [2, 12] */
export const Button: React.ComponentType<ButtonPropsType> = ({
  action,
  children = null,
  disabled = false,
  icon,
  label,
  scope,
  iconTransform = '',
  iconSize,
  variant = 'contained',
  sx = {},
  color,
  ...rest
}) => {
  const theme = useTheme<Theme>();
  const iconStyle = rest.endIcon
    ? { marginTop: '-7px', marginRight: '20px' }
    : {};
  const size = iconSize || (children ? 18 : 28);

  const muiVariant =
    {
      text: 'text',
      outlined: 'outlined',
      contained: 'contained',
      default: 'contained',
      link: 'text',
    }[variant] || variant;

  const muiStyleOverrides = {
    text: color ? {} : { color: 'rgba(0, 0, 0, 0.87)' },
    link: {},
    outlined: {},
    contained: {},
    default: {
      backgroundColor: theme.mintlab.greyscale.dark,
      boxShadow: 'none',
      marginRight: '14px',
      whiteSpace: 'nowrap',
      color: 'rgba(0, 0, 0, 0.87)',
      '&:hover': {
        backgroundColor: theme.mintlab.greyscale.darker,
        boxShadow: 'none',
      },
    },
  }[variant];

  return React.createElement(
    //@ts-ignore
    icon && !children ? IconButton : MuiButton,
    {
      'aria-label': label,
      disabled,
      color,
      onClick: typeof action === 'function' ? action : undefined,
      variant: muiVariant,
      sx: { ...muiStyleOverrides, ...sx },
      ...rest,
    },
    <>
      {icon ? (
        <Icon style={iconStyle} size={size}>
          {icon}
        </Icon>
      ) : null}
      {children}
    </>
  );
};

export default Button;
