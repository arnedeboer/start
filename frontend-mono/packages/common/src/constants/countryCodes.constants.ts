// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const COUNTRY_CODE_NETHERLANDS = '6030';
export const COUNTRY_CODE_CURACAO = '5107';
