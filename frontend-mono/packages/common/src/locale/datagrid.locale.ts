// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    datagrid: {
      // Root
      noRowsLabel: 'Geen rijen',
      noResultsOverlayLabel: 'Geen resultaten gevonden.',

      // Density selector toolbar button text
      toolbarDensity: 'Density',
      toolbarDensityLabel: 'Density',
      toolbarDensityCompact: 'Compact',
      toolbarDensityStandard: 'Standard',
      toolbarDensityComfortable: 'Comfortable',

      // Columns selector toolbar button text
      toolbarColumns: 'Kolommen',
      toolbarColumnsLabel: 'Selecteer kolommen',

      // Filters toolbar button text
      toolbarFilters: 'Filters',
      toolbarFiltersLabel: 'Toon filters',
      toolbarFiltersTooltipHide: 'Verberg filters',
      toolbarFiltersTooltipShow: 'Toon filters',
      toolbarFiltersTooltipActive: (count: any) =>
        count !== 1 ? `${count} actieve filters` : `${count} actieve filter`,

      // Quick filter toolbar field
      toolbarQuickFilterPlaceholder: 'Zoeken…',
      toolbarQuickFilterLabel: 'Zoeken',
      toolbarQuickFilterDeleteIconLabel: 'Verwijderen',

      // Export selector toolbar button text
      toolbarExport: 'Export',
      toolbarExportLabel: 'Export',
      toolbarExportCSV: 'Download als CSV',
      toolbarExportPrint: 'Printen',
      toolbarExportExcel: 'Download als Excel',

      // Columns panel text
      columnsPanelTextFieldLabel: 'Vind kolom',
      columnsPanelTextFieldPlaceholder: 'Kolom titel',
      columnsPanelDragIconLabel: 'Herorder kolom',
      columnsPanelShowAllButton: 'Toon alles',
      columnsPanelHideAllButton: 'Verberg alles',

      // Filter panel text
      filterPanelAddFilter: 'Filter toevoegen',
      filterPanelRemoveAll: 'Alles verwijderen',
      filterPanelDeleteIconLabel: 'Verwijderen',
      filterPanelLogicOperator: 'Logica Operator',
      filterPanelOperator: 'Operator',
      filterPanelOperatorAnd: 'En',
      filterPanelOperatorOr: 'Of',
      filterPanelColumns: 'Kolommen',
      filterPanelInputLabel: 'Waarde',
      filterPanelInputPlaceholder: 'Filter waarde',

      // Filter operators text
      filterOperatorContains: 'bevat',
      filterOperatorEquals: 'gelijk aan',
      filterOperatorStartsWith: 'begint met',
      filterOperatorEndsWith: 'eindigt met',
      filterOperatorIs: 'is',
      filterOperatorNot: 'is niet',
      filterOperatorAfter: 'is na',
      filterOperatorOnOrAfter: 'is op of na',
      filterOperatorBefore: 'is voor',
      filterOperatorOnOrBefore: 'is op of voor',
      filterOperatorIsEmpty: 'is leeg',
      filterOperatorIsNotEmpty: 'is niet leeg',
      filterOperatorIsAnyOf: 'is een van',
      'filterOperator=': '=',
      'filterOperator!=': '!=',
      'filterOperator>': '>',
      'filterOperator>=': '>=',
      'filterOperator<': '<',
      'filterOperator<=': '<=',

      // Header filter operators text
      headerFilterOperatorContains: 'Bevat',
      headerFilterOperatorEquals: 'Gelijk aan',
      headerFilterOperatorStartsWith: 'Begint met',
      headerFilterOperatorEndsWith: 'Eindigt met',
      headerFilterOperatorIs: 'Is',
      headerFilterOperatorNot: 'Is niet',
      headerFilterOperatorAfter: 'Is na',
      headerFilterOperatorOnOrAfter: 'Is op of na',
      headerFilterOperatorBefore: 'Is voor',
      headerFilterOperatorOnOrBefore: 'Is op of voor',
      headerFilterOperatorIsEmpty: 'Is leeg',
      headerFilterOperatorIsNotEmpty: 'Is niet leeg',
      headerFilterOperatorIsAnyOf: 'Is een van',
      'headerFilterOperator=': 'Gelijk aan',
      'headerFilterOperator!=': 'Ongelijk aan',
      'headerFilterOperator>': 'Is groter dan',
      'headerFilterOperator>=': 'Is groter dan of gelijk aan',
      'headerFilterOperator<': 'Is kleiner dan',
      'headerFilterOperator<=': 'Is kleiner dan of gelijk aan',

      // Filter values text
      filterValueAny: 'any',
      filterValueTrue: 'waar',
      filterValueFalse: 'onwaar',

      // Column menu text
      columnMenuLabel: 'Menu',
      columnMenuShowColumns: 'Toon kolommen',
      columnMenuManageColumns: 'Kolommen beheren',
      columnMenuFilter: 'Filter',
      columnMenuHideColumn: 'Verberg kolommen',
      columnMenuUnsort: 'Onsorteren',
      columnMenuSortAsc: 'Sorteren via ASC',
      columnMenuSortDesc: 'Sorteren via DESC',

      // Column header text
      columnHeaderFiltersTooltipActive: (count: any) =>
        count !== 1 ? `${count} actieve filters` : `${count} actieve filter`,
      columnHeaderFiltersLabel: 'Toon filters',
      columnHeaderSortIconLabel: 'Sorteren',

      // Rows selected footer text
      footerRowSelected: (count: any) =>
        count !== 1
          ? `${count.toLocaleString()} rijen geselecteerd`
          : `${count.toLocaleString()} rij geselecteerd`,

      // Total row amount footer text
      footerTotalRows: 'Aantal rijen:',

      // Total visible row amount footer text
      footerTotalVisibleRows: (visibleCount: any, totalCount: any) =>
        `${visibleCount.toLocaleString()} of ${totalCount.toLocaleString()}`,

      // Checkbox selection text
      checkboxSelectionHeaderName: 'Checkbox selectie',
      checkboxSelectionSelectAllRows: 'Selecteer alle rijen',
      checkboxSelectionUnselectAllRows: 'Deselecteer alle rijen',
      checkboxSelectionSelectRow: 'Selecteer rij',
      checkboxSelectionUnselectRow: 'Deselecteer rij',

      // Boolean cell text
      booleanCellTrueLabel: 'ja',
      booleanCellFalseLabel: 'nee',

      // Actions cell more text
      actionsCellMore: 'meer',

      // Column pinning text
      pinToLeft: 'Links vastzetten',
      pinToRight: 'Rechts vastzetten',
      unpin: 'Loslaten',

      // Tree Data
      treeDataGroupingHeaderName: 'Groeperen',
      treeDataExpand: 'zie kinderen',
      treeDataCollapse: 'verberg kinderen',

      // Grouping columns
      groupingColumnHeaderName: 'Groeperen',
      groupColumn: (name: any) => `Groeperen via ${name}`,
      unGroupColumn: (name: any) => `Stop groepering via ${name}`,

      // Master/detail
      detailPanelToggle: 'Detail paneel aan/uit',
      expandDetailPanel: 'Uitklappen',
      collapseDetailPanel: 'Inklappen',

      // Used core components translation keys
      MuiTablePagination: {},

      // Row reordering text
      rowReorderingHeaderName: 'Rij herordenen',

      // Aggregation
      aggregationMenuItemHeader: 'Aggregatie',
      aggregationFunctionLabelSum: 'sum',
      aggregationFunctionLabelAvg: 'avg',
      aggregationFunctionLabelMin: 'min',
      aggregationFunctionLabelMax: 'max',
      aggregationFunctionLabelSize: 'afmeting',
    },
  },
};
