// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type CaseAttributeFieldChangeEventType =
  React.ChangeEvent<HTMLInputElement>;

export const createChangeEvent = (value: any, name: string) =>
  ({ target: { value, name } }) as CaseAttributeFieldChangeEventType;

const updateOnIndex: <T>(xs: T[], index: number, value: T) => T[] = (
  xs,
  index,
  value
) => {
  return [...xs.slice(0, index), value, ...xs.slice(index + 1)];
};

const identity = (item: any) => item;

export const useMultiFieldHandlers = <T>({
  multiValue,
  value,
  onChange,
  processValue = identity,
  defaultValue,
}: {
  multiValue: boolean | undefined;
  value: T | T[];
  onChange: (ev: React.ChangeEvent<any>) => void;
  processValue?: (inputValue: string | null) => T;
  defaultValue: string | null;
}) => {
  const isMultiple = (val: unknown): val is T[] => {
    return Boolean(multiValue);
  };

  const updateField = (nextValue: string | null, index?: number) => {
    const nextValueNormalized =
      typeof index === 'number' && isMultiple(value)
        ? updateOnIndex(value, index, processValue(nextValue))
        : processValue(nextValue);
    onChangeFlat(nextValueNormalized);
  };

  const removeOrClearFirstField = (index: number) => {
    if (!multiValue) {
      updateField(defaultValue);
    } else if (index === 0 && isMultiple(value) && value.length > 1) {
      removeField(0);
    } else if (index === 0) {
      updateField(defaultValue, 0);
    } else {
      removeField(index);
    }
  };

  const onChangeFlat = (nextValue: T | T[]) =>
    onChange(createChangeEvent(nextValue, name));

  const removeField = (targetIndex: number) =>
    isMultiple(value) &&
    onChangeFlat(value.filter((__, ix) => ix !== targetIndex));

  const addField = () => {
    isMultiple(value) && onChangeFlat([...value, processValue(defaultValue)]);
  };

  return {
    isMultiple,
    updateField,
    addField,
    removeOrClearFirstField,
  };
};
