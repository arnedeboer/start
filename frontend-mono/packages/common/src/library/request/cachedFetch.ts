// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const cache = new Map<any, Promise<Response>>();

export const cachedFetch: typeof fetch = requestInfo => {
  const match = cache.get(requestInfo) || fetch(requestInfo);
  cache.set(requestInfo, match);
  return match.then(resp => resp.clone());
};
