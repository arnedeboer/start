// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import fecha from 'fecha';
import * as i18next from 'i18next';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { TimelineItemType, FiltersOptionType } from './Timeline.types';

export const getFormattedDate = (date: Date, t: i18next.TFunction) => {
  const now = fecha.format(new Date(), 'YYYYMMDD');
  const targetDate = fecha.format(date, 'YYYYMMDD');

  return now === targetDate ? t('today') : fecha.format(date, 'D MMMM');
};

const getIcon = (type: string) => {
  if (type.includes('document')) {
    return 'file';
  } else if (type.includes('object')) {
    return 'custom_object_type';
  } else if (type.includes('custom_object')) {
    return 'custom_object_type';
  } else if (type.includes('subject')) {
    return 'contact';
  } else if (type.includes('email') || type.includes('pip')) {
    return 'email_template';
  } else if (type.includes('case')) {
    return 'case';
  } else {
    return 'other';
  }
};

/* eslint complexity: [2, 9] */
export const getItemProperties = (
  timelineItem: TimelineItemType,
  t: i18next.TFunction
) => {
  const type = timelineItem.type;
  const [objectType] = type.split('/');
  const icon = getIcon(type);
  const description = timelineItem.description;
  const content = timelineItem.content;

  const itemProperties = {
    icon: <ZsIcon size="small">{`entityType.inverted.${icon}`}</ZsIcon>,
    title: t(`eventType.${type}`),
    description,
    content,
    exception: timelineItem.exception,
  };

  switch (objectType) {
    case 'case': {
      const caseNumber = timelineItem.caseNumber;
      const descriptionContainsCaseNumber = Boolean(
        description.match(RegExp(caseNumber || ''))
      );

      return {
        ...itemProperties,
        description: descriptionContainsCaseNumber
          ? description
          : `${t('common:entityType.case')} ${caseNumber}: ${description} `,
      };
    }
    case 'auth': {
      return {
        ...itemProperties,
        icon: <Icon>{iconNames.eye}</Icon>,
      };
    }
    // case 'object':
    // case 'subject':
    // case 'document':
    // case 'email':
    default:
      return itemProperties;
  }
};

export const Checkboxes = ({
  filters,
  handleFilterChange,
}: {
  filters: FiltersOptionType[];
  handleFilterChange: any;
}) => {
  return (
    <ul>
      {filters &&
        filters.map(({ label, value, checked }) => (
          <li key={value}>
            <Checkbox
              label={label}
              name={value.toString()}
              onChange={handleFilterChange}
              checked={checked}
            />
          </li>
        ))}
    </ul>
  );
};
