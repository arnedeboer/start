// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
// @ts-ignore
import { login } from '@zaaksysteem/common/src/library/auth';
import { TranslatedErrorMessage } from '../../library/request/TranslatedErrorMessage';
import Alert from '../dialogs/Alert/Alert';
import { V2ServerErrorsType } from '../../types/ServerError';

export const ServerErrorDialog = ({ err }: { err: V2ServerErrorsType }) => {
  const [t] = useTranslation();
  const [opened, setOpened] = React.useState(true);

  useEffect(() => {
    setOpened(true);
  }, [err]);

  const {
    response: { status },
  } = err;

  const title = t('dialog.error.title');

  const close = () => setOpened(false);
  const closeButton = { text: t('dialog.close'), action: close };

  const goToLogin = () => {
    login(window.location.pathname);
  };
  const loginButton = { text: t('login'), action: goToLogin };

  return status === 401 ? (
    <Alert
      primaryButton={loginButton}
      secondaryButton={closeButton}
      onClose={close}
      title={title}
      open={opened}
    >
      {t('serverErrors.status.401')}
    </Alert>
  ) : (
    <Alert
      primaryButton={closeButton}
      onClose={close}
      title={title}
      open={opened}
    >
      <TranslatedErrorMessage errorObj={err} />
    </Alert>
  );
};

export default ServerErrorDialog;
