// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import deepEqual from 'fast-deep-equal';
import { normalizeValue } from '@zaaksysteem/common/src/library/normalizeValue';
import {
  AnyFormDefinitionField,
  FormShapeType,
} from '../../types/formDefinition.types';

type CompareValue = string | number | string[] | number[];

export type RuleEngineAllFieldsCondition<
  FieldNames extends string | number | symbol,
> = (formDefinitionFieldsMap: FormShapeType<FieldNames>) => boolean;
export type RuleEngineCondition = (field: AnyFormDefinitionField) => boolean;
export type RuleEngineCompareCondition = (
  conditionValue: CompareValue
) => RuleEngineCondition;

export type RuleEngineCreateCondition =
  | RuleEngineCondition
  | RuleEngineCompareCondition;

export const valueEquals: RuleEngineCompareCondition = equals => field =>
  deepEqual(equals, normalizeValue(field.value));

export const hasValue: RuleEngineCondition = field =>
  normalizeValue(field.value).toString().trim().length > 0;

export const valueOneOf: RuleEngineCompareCondition = equals => field => {
  const conditionIsArray = Array.isArray(equals);
  const valueIsArray = Array.isArray(field.value);

  if (!conditionIsArray) return false;

  return valueIsArray
    ? Boolean(
        (equals as Array<string | number | boolean>).find(condition =>
          // @ts-ignore
          field.value.includes(condition)
        )
      )
    : (equals as Array<string | number | boolean>).includes(
        normalizeValue(field.value)
      );
};

export const isTruthy: RuleEngineCondition = field =>
  Boolean(normalizeValue(field.value));
