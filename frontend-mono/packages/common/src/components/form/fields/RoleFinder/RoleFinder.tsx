// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import Select, { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { useRoleChoicesQuery } from './RoleFinder.library';

type RoleFinderPropsType = {
  value: ValueType<string>;
  name: string;
  config?: any;
  freeSolo?: boolean;
  [key: string]: any;
};

const RoleFinder: FunctionComponent<RoleFinderPropsType> = ({
  value,
  name,
  config,
  freeSolo = true,
  ...restProps
}) => {
  const parentRoleUuid = config?.parentRoleUuid;
  const [selectProps, ServerErrorDialog, emptyChoicesResult] =
    useRoleChoicesQuery(parentRoleUuid);

  return (
    <>
      <Select
        {...restProps}
        {...selectProps}
        name={name}
        value={value}
        nestedValue={true}
        disabled={Boolean(restProps?.disabled) || emptyChoicesResult}
        freeSolo={freeSolo}
      />
      {ServerErrorDialog}
    </>
  );
};

export default RoleFinder;
