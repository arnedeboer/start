// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Component } from 'react';
import { v4 as unique } from 'uuid';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
//@ts-ignore
import { FileSelect } from '@mintlab/ui/App/Zaaksysteem/FileSelect';
import { STATUS_PENDING, STATUS_FAILED } from '../../constants/fileStatus';
import { ResponseBodyType, sendAsFile, sendAsDocument } from './Upload.library';
import { FileObject } from './Upload.types';

type UploadPropsType = {
  accept?: string[];
  selectInstructions: string;
  dragInstructions: string;
  dropInstructions: string;
  orLabel: string;
  value: any[];
  multiValue: boolean;
  list: React.ReactNode;
  addFiles: (files: FileObject[]) => void;
  setFileStatus: (key: string, properties: Partial<FileObject>) => void;
  fileType: 'file' | 'document';
};

class Upload extends Component<UploadPropsType> {
  render() {
    const {
      accept,
      selectInstructions,
      dragInstructions,
      dropInstructions,
      value = [],
      orLabel,
      multiValue = true,
      list = [],
    } = this.props;

    return (
      <FileSelect
        onDrop={this.handleDrop}
        accept={accept}
        dragInstructions={dragInstructions}
        dropInstructions={dropInstructions}
        selectInstructions={selectInstructions}
        orLabel={orLabel}
        hasFiles={value && value.length}
        multiValue={multiValue}
        fileList={list}
        error={this.error()}
      />
    );
  }

  fileCompleted(key: string, response: ResponseBodyType) {
    if (!response.data) return;
    const { setFileStatus } = this.props;
    setFileStatus(key, { value: response.data.id });
  }

  fileError(key: string, errorCode: string) {
    const { setFileStatus } = this.props;
    setFileStatus(key, { errorCode, status: STATUS_FAILED });
  }

  /**
   * @param {Array} acceptedFiles
   */
  /* eslint complexity: [2, 8] */
  handleDrop = async (acceptedFiles: File[]) => {
    const { value, multiValue, addFiles, fileType } = this.props;

    if (!acceptedFiles || !acceptedFiles.length) return;
    if (!multiValue && value.length) return;

    const fileList = multiValue ? acceptedFiles : acceptedFiles.slice(0, 1);
    let filesData = fileList.map<{ file: File; object: FileObject }>(
      (file: File) => ({
        file,
        object: {
          key: unique(),
          value: null,
          label: file.name,
          status: STATUS_PENDING,
          file,
        },
      })
    );
    const fileObjects = filesData.map(file => file.object);

    addFiles(fileObjects);

    for (const file of filesData) {
      await (fileType === 'document' ? sendAsDocument : sendAsFile)(file.file)
        .then((response: ResponseBodyType) =>
          this.fileCompleted(file.object.key, response)
        )
        .catch((errorObj: V2ServerErrorsType) => {
          const errors = errorObj.errors;
          if (errors && errors[0]) {
            this.fileError(file.object.key, errors[0].code);
          }
        });
    }
  };

  /**
   * @return {Boolean}
   */
  error() {
    const { value } = this.props;
    const failedFiles = value.filter(
      (thisFile: FileObject) => thisFile.status === STATUS_FAILED
    );
    return Boolean(failedFiles.length);
  }
}

export default Upload;
