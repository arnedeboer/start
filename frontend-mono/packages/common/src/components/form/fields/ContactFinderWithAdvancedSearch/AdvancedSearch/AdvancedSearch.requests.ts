// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchStufConfigIntegrationsType,
  StufConfigIntegrationsResponseBodyType,
  ImportContactType,
  SearchType,
  SearchResponseBodyType,
  ImportContactResponseBodyType,
} from './AdvancedSearch.types';

export const fetchStufConfig: FetchStufConfigIntegrationsType = async () => {
  const url = '/api/v1/sysin/interface/get_by_module_name/stufconfig';

  const response = await request<StufConfigIntegrationsResponseBodyType>(
    'GET',
    url
  );

  return response;
};

export const search: SearchType = async (data, id) => {
  let url = '/api/v1/subject';

  if (id === 'extern') url += '/remote_search';
  else if (id !== 'intern') url += `/remote_search/${id}`;

  const response = await request<SearchResponseBodyType>('POST', url, data);

  return response;
};

export const importContact: ImportContactType = async (
  data,
  id,
  contactType
) => {
  let url = '/api/v1/subject/remote_import';

  if (contactType === 'person') url += `/${id}`;

  const response = await request<ImportContactResponseBodyType>(
    'POST',
    url,
    data
  );

  return response;
};
