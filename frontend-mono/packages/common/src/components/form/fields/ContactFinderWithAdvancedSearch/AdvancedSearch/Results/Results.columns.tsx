// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames, IconNameType } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { ResultType } from '../AdvancedSearch.types';

export const getWarnings = ({
  rowData,
  t,
}: {
  rowData: ResultType;
  t: i18next.TFunction;
}): React.ReactElement | null => {
  let warnings: string[] = [];
  if (rowData.isDeceased) warnings.push(t('warnings.isDeceased'));
  if (rowData.isSecret) warnings.push(t('warnings.isSecret'));
  if (rowData.hasCorrespondenceAddress)
    warnings.push(t('warnings.hasCorrespondenceAddress'));

  return warnings.length ? (
    <div>
      <Tooltip
        title={
          <>
            {warnings.map((warning, index) => (
              <div key={index}>{warning}</div>
            ))}
          </>
        }
      >
        <Icon size="extraSmall">{iconNames.warning}</Icon>
      </Tooltip>
    </div>
  ) : null;
};

export const getPersonColumns = (t: i18next.TFunction, classes: any) => [
  {
    name: 'gender',
    width: 30,
    label: '',
    disableSort: true,
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }: { rowData: ResultType }) => (
      <div className={classes.icon}>
        <Icon size="small">{iconNames[rowData.gender as IconNameType]}</Icon>
      </div>
    ),
  },
  {
    name: 'name',
    width: 200,
    label: t('columns.name'),
    /* eslint-disable-next-line */
    cellRenderer: ({ rowData }: { rowData: ResultType }) => {
      return (
        <div className={classes.nameCell}>
          <span className={classes.name}>{rowData.name}</span>
          {getWarnings({ rowData, t })}
        </div>
      );
    },
  },
  {
    name: 'dateOfBirth',
    width: 100,
    label: t('columns.dateOfBirth'),
  },
  {
    name: 'address',
    width: 1,
    flexGrow: 1,
    label: t('columns.address'),
  },
];

export const getOrganizationColumns = (t: i18next.TFunction, classes: any) => [
  {
    name: 'cocNumber',
    width: 80,
    label: t('columns.cocNumber'),
  },
  {
    name: 'cocLocationNumber',
    width: 100,
    label: t('columns.cocLocationNumber'),
  },
  {
    name: 'name',
    width: 1,
    flexGrow: 1,
    label: t('columns.tradeName'),
  },
  {
    name: 'address',
    width: 1,
    flexGrow: 1,
    label: t('columns.address'),
  },
];
