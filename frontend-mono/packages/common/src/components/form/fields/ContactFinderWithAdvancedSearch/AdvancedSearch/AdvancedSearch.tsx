// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { v4 as unique } from 'uuid';
import { useTheme } from '@mui/material';
import Chip from '@mui/material/Chip';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import useSession from '@zaaksysteem/common/src/hooks/useSession';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  ContactTypeType,
  ResultType,
  ResultsType,
  StufConfigIntegrationType,
  ValuesType,
} from './AdvancedSearch.types';
import { useAdvancedSearchStyles } from './AdvancedSearch.styles';
import { performSearch, performImportContact } from './AdvancedSearch.library';
import PersonForm from './Form/PersonForm';
import OrganizationForm from './Form/OrganizationForm';
import Results from './Results/Results';

export type AdvancedSearchPropsType = {
  stufConfigIntegrations: StufConfigIntegrationType[];
  onSelectRow: (param: ValueType<String>) => void;
  onClose: () => void;
  contactTypes: ContactTypeType[];
};

const AdvancedSearch: React.ComponentType<AdvancedSearchPropsType> = ({
  stufConfigIntegrations,
  onSelectRow,
  onClose,
  contactTypes,
}) => {
  const {
    palette: { common },
  } = useTheme<Theme>();
  const classes = useAdvancedSearchStyles();
  const [t] = useTranslation('ContactFinderWithAdvancedSearch');
  const dialogEl = useRef(null);

  const [formType, setFormType] = useState<ContactTypeType>(contactTypes[0]);
  const [results, setResults] = useState<ResultsType>(null);
  const [contactToImport, setContactToImport] = useState<ResultType | null>(
    null
  );

  const session = useSession();
  const countryCode = session.account.instance.country_code;
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const onSubmit = (values: ValuesType) =>
    performSearch(formType, values as ValuesType)
      .then(setResults)
      .catch(openServerErrorDialog);

  const onRowClick = ({ rowData }: { rowData: ResultType }) => {
    if (rowData.integrationId === 'intern') {
      onSelectRow({
        label: rowData.name,
        value: rowData.uuid,
        type: rowData.type,
      });
    } else {
      setContactToImport(rowData);
    }
  };

  return (
    <Dialog
      aria-label={t('advancedSearch')}
      open={true}
      onClose={onClose}
      ref={dialogEl}
      disableBackdropClick={true}
      fullWidth={true}
      maxWidth="lg"
    >
      <DialogTitle
        elevated={true}
        id={unique()}
        title={t('advancedSearch')}
        onCloseClick={onClose}
        scope={'external-contact'}
      />

      <DialogContent>
        <div className={classes.wrapper}>
          <div className={classes.searchWrapper}>
            {contactTypes.length > 1 && (
              <div className={classes.searchHeader}>
                {contactTypes.map(contactType => (
                  <Chip
                    key={contactType}
                    classes={{
                      root: classNames(classes.chipRoot, {
                        [classes.chipActive]: formType === contactType,
                      }),
                      label: classes.chipLabel,
                    }}
                    onClick={() => {
                      setFormType(contactType);
                      setResults(null);
                    }}
                    icon={
                      <ZsIcon
                        size="tiny"
                        color={common.white}
                      >{`entityType.${contactType}`}</ZsIcon>
                    }
                    label={t(`types.${contactType}`)}
                  />
                ))}
              </div>
            )}
            {formType === 'person' ? (
              <PersonForm
                onSubmit={onSubmit}
                countryCode={countryCode}
                stufConfigIntegrations={stufConfigIntegrations}
              />
            ) : (
              <OrganizationForm onSubmit={onSubmit} />
            )}
          </div>
          <div className={classes.resultsWrapper}>
            <Results
              formType={formType}
              countryCode={countryCode}
              results={results}
              onRowClick={onRowClick}
            />
          </div>
        </div>
      </DialogContent>

      {ServerErrorDialog}

      {contactToImport && (
        <ConfirmDialog
          open={Boolean(contactToImport)}
          onConfirm={() => {
            performImportContact(contactToImport, formType)
              .then(response => {
                onSelectRow({
                  label: contactToImport.name,
                  value: response.result.reference,
                  type: contactToImport.type,
                });
              })
              .catch(openServerErrorDialog);
          }}
          onClose={() => setContactToImport(null)}
          title={t('import')}
          body={
            <div>
              {t('confirm', {
                preview: contactToImport?.name,
              })}
            </div>
          }
        />
      )}
      <>
        <Divider />
        <DialogActions>
          <Button name="cancel" action={onClose}>
            {t('common:dialog.cancel')}
          </Button>
        </DialogActions>
      </>
    </Dialog>
  );
};

export default AdvancedSearch;
