// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import useServerErrorDialog, {
  OpenServerErrorDialogType,
} from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  AddressValueType,
  PdokAddressFreeSearchResponseType,
  PdokAddressFull,
  PdokAddressSuggestion,
  PdokAddressSuggestionResponseType,
} from './Address.types';

const pdokAdressToZsAddress = (doc: PdokAddressFull): AddressValueType => {
  const [, lon, lat] =
    /POINT\((.+)\s(.+)\)/.exec(doc.centroide_ll) || ([null, '', ''] as const);

  return {
    geojson: {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {},
          geometry: {
            type: 'Point',
            coordinates: [Number(lon), Number(lat)],
          },
        },
      ],
    },
    address: { full: doc.weergavenaam },
    bag: {
      type: 'nummeraanduiding',
      id: doc.nummeraanduiding_id,
    },
  };
};

const pdokAdressSuggestionToZsSuggestion = (
  doc: PdokAddressSuggestion
): ValueType<string> => {
  return {
    label: doc.weergavenaam,
    value: doc.id,
  };
};

export const useAddressChoicesQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input);

  const data = useQuery(
    ['addresses', input],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<PdokAddressSuggestionResponseType>(
        'GET',
        buildUrl('https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest', {
          // eslint-disable-next-line id-length
          q: input,
          fq: 'type:adres',
        })
      ).catch(openServerErrorDialog);

      return body
        ? body.response.docs.map(pdokAdressSuggestionToZsSuggestion)
        : [];
    },
    { enabled }
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog, openServerErrorDialog] as const;
};

export const fetchAddressLookup = async (id: string) => {
  const body = await request<PdokAddressFreeSearchResponseType>(
    'GET',
    buildUrl('https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup', { id })
  );

  return pdokAdressToZsAddress(body.response.docs[0]);
};

export const fetchClosestAddress = async (
  point: GeoJSON.Point,
  onError: OpenServerErrorDialogType
): Promise<AddressValueType | null> => {
  const body = await request<PdokAddressSuggestionResponseType>(
    'GET',
    buildUrl('https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest', {
      lat: point.coordinates[1],
      lon: point.coordinates[0],
      // eslint-disable-next-line id-length
      q: 'type:adres',
    })
  ).catch(onError);

  const id = body?.response.docs[0].id;

  if (id) {
    const innerBody = await request<PdokAddressFreeSearchResponseType>(
      'GET',
      buildUrl('https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup', {
        id,
      })
    ).catch(onError);
    const top = innerBody?.response.docs[0];

    return top ? pdokAdressToZsAddress(top) : null;
  } else {
    return null;
  }
};

export const fetchCoordinatesForAddress =
  (onError: OpenServerErrorDialogType) =>
  async (address: string): Promise<[number, number] | null> => {
    const body = await request<PdokAddressSuggestionResponseType>(
      'GET',
      buildUrl('https://api.pdok.nl/bzk/locatieserver/search/v3_1/suggest', {
        // eslint-disable-next-line id-length
        q: address,
        fq: 'type:adres',
      })
    ).catch(onError);

    const innerBody = await request<PdokAddressFreeSearchResponseType>(
      'GET',
      buildUrl('https://api.pdok.nl/bzk/locatieserver/search/v3_1/lookup', {
        id: body?.response.docs[0]?.id || 0,
      })
    ).catch(onError);

    const doc = innerBody?.response.docs[0];

    if (!doc) {
      return null;
    } else {
      const [, lon, lat] =
        /POINT\((.+)\s(.+)\)/.exec(doc.centroide_ll) ||
        ([null, '', ''] as const);
      return [Number(lon), Number(lat)];
    }
  };
