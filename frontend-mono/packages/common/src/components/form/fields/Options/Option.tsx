// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DraggableStateSnapshot } from 'react-beautiful-dnd';
import classNames from 'classnames';
import { useTheme } from '@mui/material';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Render from '@mintlab/ui/App/Abstract/Render';
//@ts-ignore
import Switch from '@mintlab/ui/App/Material/Switch';
import { useOptionStylesheet } from './Option.style';
import { OptionPropsType } from './Options.types';

const Option: React.FunctionComponent<OptionPropsType> = ({
  item,
  provided,
  snapshot,
  onDelete,
  onSwitchActive,
  t,
}) => {
  const {
    mintlab: { greyscale },
  } = useTheme<Theme>();
  const classes = useOptionStylesheet();

  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      className={classNames(classes.wrapper, {
        [classes.dragging as any]: snapshot.isDragging,
      })}
      style={getItemStyle(snapshot, provided.draggableProps.style)}
    >
      <Icon
        classes={{
          root: classes.handle,
        }}
      >
        {iconNames.drag_indicator}
      </Icon>
      <Switch
        checked={item.active}
        onChange={() => onSwitchActive(item)}
        variant="iOS"
      />

      <div className={classes.label}>{item.value}</div>
      <Render condition={item.isNew}>
        <Button
          action={() => onDelete(item)}
          name="deleteOption"
          label={t('attribute:dialog.deleteOption')}
          icon="close"
          sx={{
            color: greyscale.darkest,
          }}
        />
      </Render>
    </div>
  );
};

const getItemStyle = (snapshot: DraggableStateSnapshot, style: any) => {
  const transition = `all 0.03s`;

  if (snapshot.isDropAnimating) {
    return {
      ...style,
      transition,
    };
  } else if (snapshot.isDragging) {
    return {
      ...style,
      transform: `rotate(3deg) ${style.transform}`,
      transition,
    };
  }

  return {
    ...style,
  };
};

export default Option;
