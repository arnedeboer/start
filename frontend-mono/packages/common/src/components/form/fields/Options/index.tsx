// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { OptionsComponentType } from './Options.types';

const Comp = React.lazy(() => import('./Options'));

export default (props: React.ComponentProps<OptionsComponentType>) => (
  <React.Suspense>
    {/* @ts-ignore */}
    <Comp {...props} />
  </React.Suspense>
);
