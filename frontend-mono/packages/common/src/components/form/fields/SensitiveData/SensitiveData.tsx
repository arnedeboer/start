// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../../types/Form2.types';
import { usePersonNumberStylesheet } from './SensitiveData.style';
import { getSensitiveData } from './getSensitiveData';

const bulletPoint = '\u25CF';
const personalNumberMask = bulletPoint.repeat(9);

const PersonalNumber: FormFieldComponentType<string> = props => {
  const [value, setValue] = useState(personalNumberMask);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [t] = useTranslation('common');
  const classes = usePersonNumberStylesheet();
  const {
    readOnly,
    value: updatedValue,
    definition: {
      config: { uuid, authenticated, type },
    },
  } = props;

  const updateValue = (newValue: string | undefined) => {
    if (newValue) {
      setValue(newValue);
    } else {
      setValue('');
    }
  };

  return !readOnly ? (
    <div className={classes.wrapper}>
      {ServerErrorDialog}
      <TextField
        {...props}
        value={updatedValue ?? value}
        disabled={value === personalNumberMask || authenticated}
      />
      {value === personalNumberMask && (
        <Button
          name="getSensitiveData"
          sx={{ marginLeft: '20px' }}
          action={() =>
            getSensitiveData(updateValue, uuid, type).catch(
              openServerErrorDialog
            )
          }
        >
          {t('forms.view')}
        </Button>
      )}
    </div>
  ) : (
    <ReadonlyValuesContainer value={personalNumberMask} />
  );
};

export default PersonalNumber;
