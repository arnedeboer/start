// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { AnyFormDefinitionField } from './fieldDefinition.types';

// These exports are placed here for backwards compatibility purposes
export * from './generic.types';
export * from './fieldComponent.types';
export * from './fieldDefinition.types';
export * from './formRenderer.types';

export type FormDefinitionStep<FormShape> = {
  title: string;
  description?: string;
  fields: AnyFormDefinitionField<FormShape>[];
};

export type FormShapeType<FieldNames extends string | number | symbol> = Record<
  FieldNames,
  AnyFormDefinitionField
>;

export type StepsDefinition = {
  title: string;
  description?: string;
  fieldNames: string[];
}[];

export type FormDefinition<FormShape = any> =
  AnyFormDefinitionField<FormShape>[];
