// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ComponentType, useMemo } from 'react';
import { FormikProps, FormikErrors, FormikTouched } from 'formik';
import { get } from '@mintlab/kitchen-sink/source';
import {
  FormRendererFormField,
  AnyFormDefinitionField,
  RegisterValidationForNamespaceType,
} from '../types/formDefinition.types';
import { FormFields } from '../fields';
import { setTouchedAndHandleChange } from '../library/formHelpers';
import { useNamespace } from './useNamespace';

export type UseFormFieldsType<FormShape> = {
  formDefinitionFields: AnyFormDefinitionField<FormShape>[];
  values: FormShape;
  formik: FormikProps<FormShape>;
  validateForm: () => void;
  registerValidation: RegisterValidationForNamespaceType;
  customFields?: FieldMapType;
  namespace?: string;
};

export type UseFormFieldsReturnType<FormShape> =
  FormRendererFormField<FormShape>[];

type FieldMapType = { [key: string]: ComponentType<any> };

function getError<FormShape>(
  errors: FormikErrors<FormShape>,
  name: keyof FormShape
): string | undefined {
  if (typeof errors[name] === 'undefined') {
    return undefined;
  }

  if (typeof errors[name] === 'string') {
    return errors[name] as string;
  }
}

function isFieldTouched<FormShape>(
  touched: FormikTouched<FormShape>,
  name: keyof FormShape & string
): boolean {
  return get(touched, name) === true || get(touched, `${name}.__base`) === true;
}

function getFieldError<FormShape>(
  field: AnyFormDefinitionField<FormShape>,
  name: keyof FormShape & string,
  errors: FormikErrors<FormShape>
): string | undefined {
  return field.getError
    ? field.getError(errors, name)
    : getError<FormShape>(errors, name);
}

export function useFormFields<FormShape = any>({
  formDefinitionFields,
  values,
  formik,
  registerValidation,
  customFields,
  namespace,
  validateForm,
}: UseFormFieldsType<FormShape>): UseFormFieldsReturnType<FormShape> {
  const { withNameSpace } = useNamespace<FormShape>(namespace);
  const {
    errors,
    touched,
    handleChange,
    handleBlur,
    setFieldTouched,
    setFieldValue,
    registerField,
  } = formik;

  const Fields: FieldMapType = {
    ...FormFields,
    ...customFields,
  };

  const fields = useMemo(() => {
    return formDefinitionFields
      .filter(field => !field.hidden)
      .reduce<FormRendererFormField<FormShape>[]>((acc, field) => {
        const FieldComponent = field.type
          ? Fields[field.type]
          : field.component;

        if (FieldComponent) {
          const nameWithNamespace = withNameSpace(field.name);
          const isTouched = isFieldTouched(touched, nameWithNamespace);
          const error = isTouched
            ? getFieldError(field, nameWithNamespace, errors)
            : undefined;

          return [
            ...acc,
            {
              ...field,
              name: nameWithNamespace,
              formik,
              error,
              registerValidation,
              registerField,
              setFieldValue,
              setFieldTouched,
              validateForm,
              definition: field,
              FieldComponent,
              value: get(values, field.name) || null,
              checked: get(values, field.name, false),
              touched: isTouched,
              key: `field-${nameWithNamespace}`,
              onChange: setTouchedAndHandleChange({
                setFieldTouched,
                handleChange,
              }),
              onBlur: handleBlur,
            },
          ];
        }

        return acc;
      }, []);
  }, [formDefinitionFields, values, errors, touched]);

  return fields;
}
