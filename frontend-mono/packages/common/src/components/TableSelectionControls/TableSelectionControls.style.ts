// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTableSelectionControlsStyles = makeStyles(
  ({
    palette: { primary, basalt, cloud, elephant },
    typography,
    mintlab: { greyscale, shadows },
  }: any) => ({
    selectAll: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      gap: 20,
    },
  })
);
