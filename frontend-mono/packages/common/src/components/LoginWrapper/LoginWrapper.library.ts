// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';

export const navToLogin = () => {
  document.location.href = buildUrl('/auth/login', {
    referer: document.location.pathname,
  });
};
