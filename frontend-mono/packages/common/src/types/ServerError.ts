// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type V2ServerErrorType = {
  title?: string[];
  detail?: number | string;
  source?: string | string[];
  code: string;
};

export type V2ServerErrorsType = {
  response: Response;
  errors?: V2ServerErrorType[];
};
