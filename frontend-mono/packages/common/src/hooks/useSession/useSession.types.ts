// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type AccountType = {
  instance: {
    country_code: string;
    company: string;
  };
};

export type ActiveIntegrationType =
  | 'auth_twofactor'
  | 'kcc'
  | 'koppelapp_appointments'
  | 'koppelapp_dso_swf'
  | 'koppelapp_objections'
  | 'koppelapp_signer'
  | 'stuf_dcr'
  | 'supersaas'
  | 'mijnoverheid'
  | 'controlpanel'
  | 'next2know'
  | 'stuf_dcr'
  | 'stuf_dcr'
  | 'emailconfiguration'
  | 'kvkapi'
  | 'koppelapp_document_masker'
  | 'postex'
  | 'stuf_dcr'
  | 'email'
  | 'stufconfig'
  | 'xential';

export type SignatureUploadRoleType = 'behandelaar' | 'zaaksysteembeheerder';

type ConfigurableType = {
  bag_local_only: boolean;
  show_object_v1: boolean;
  show_object_v2: boolean;
  signature_upload_role: SignatureUploadRoleType;
  contact_channel_enabled: boolean;
};

export type CapabilitiesType =
  | 'admin'
  | 'beheer'
  | 'beheer_gegevens_admin'
  | 'beheer_plugin_admin'
  | 'beheer_zaaktype_admin'
  | 'case_allocation'
  | 'case_registration_allow_partial'
  | 'contact_edit_subset'
  | 'contact_nieuw'
  | 'contact_search'
  | 'contact_search_extern'
  | 'dashboard'
  | 'documenten_intake_all'
  | 'documenten_intake_subject'
  | 'gebruiker'
  | 'message_intake'
  | 'owner_signatures'
  | 'plugin_mgmt'
  | 'search'
  | 'view_sensitive_contact_data'
  | 'view_sensitive_data'
  | 'useradmin'
  | 'vernietigingslijst'
  | 'zaak_afdeling'
  | 'zaak_create_skip_required'
  | 'zaak_beheer'
  | 'zaak_eigen';

type CustomRoleType = string;
export type SystemRoleType =
  | 'Administrator'
  | 'Basisregistratiebeheerder'
  | 'Behandelaar'
  | 'BRP externe bevrager'
  | 'Contactbeheerder'
  | 'Documentintaker'
  | 'Gebruikersbeheerder'
  | 'Kcc-medewerker'
  | 'Klantcontacter'
  | 'Persoonsverwerker'
  | 'Zaakbeheerder'
  | 'Zaaktypebeheerder'
  | 'Zaakverdeler'
  | CustomRoleType;

type LoggedInUserType = {
  display_name: string;
  id: string;
  email: string | null;
  telephonenumber: string | null;
  organizational_unit: string;
  system_roles: SystemRoleType[];
  uuid?: string;
  capabilities: CapabilitiesType[];
};

export type SessionResponseBodyType = {
  result: {
    instance: {
      account: AccountType;
      active_interfaces: ActiveIntegrationType;
      configurable: ConfigurableType;
      logged_in_user: LoggedInUserType;
    };
  };
};

export type SessionType = SessionResponseBodyType['result']['instance'];

export type FormatSessiontype = (
  response: SessionResponseBodyType
) => SessionType;
