// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '../components/ServerErrorDialog/ServerErrorDialog';

export type OpenServerErrorDialogType = (errorObj: V2ServerErrorsType) => void;

const useServerErrorDialog = () => {
  const [dialog, setDialog] = React.useState<React.ReactNode | null>(null);

  const openServerErrorDialog = (errorObj: V2ServerErrorsType) => {
    setDialog(<ServerErrorDialog err={errorObj} />);
    return undefined;
  };

  return [dialog, openServerErrorDialog] as const;
};

export default useServerErrorDialog;
