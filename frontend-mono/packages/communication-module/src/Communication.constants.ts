// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// QUERY KEYS
export const GET_THREADS = ['getThreads'];
export const GET_MESSAGES = ['getMessages'];

// CONTACT CHANNEL TYPES
export const TYPE_CHANNEL_FRONTDESK = 'frontdesk';
export const TYPE_CHANNEL_ASSIGNEE = 'assignee';
export const TYPE_CHANNEL_MAIL = 'mail';
export const TYPE_CHANNEL_PHONE = 'phone';
export const TYPE_CHANNEL_EMAIL = 'email';
export const TYPE_CHANNEL_WEBFORM = 'webform';
export const TYPE_CHANNEL_SOCIALMEDIA = 'social_media';
export const TYPE_CHANNEL_EXTERNAL = 'external_application';
export const TYPE_CHANNEL_POSTEX = 'postex';

// FIELDS TYPES
export const CONTACT_FINDER = 'contactFinder';
export const CASE_FINDER = 'caseFinder';
export const CASE_SELECTOR = 'caseSelector';
export const UPLOAD = 'file';

// MESSAGE TYPES
export const TYPE_CONTACT_MOMENT = 'contact_moment';
export const TYPE_NOTE = 'note';
export const TYPE_EMAIL = 'email';
export const TYPE_PIP_MESSAGE = 'pip';
export const TYPE_MIJN_OVERHEID = 'mijn_overheid';
export const TYPE_POSTEX = 'postex';
export const TYPE_EXTERNAL_MESSAGE = 'external_message';
export const TYPES_EXTERNAL_MESSAGE = [
  TYPE_EMAIL,
  TYPE_PIP_MESSAGE,
  TYPE_MIJN_OVERHEID,
];
