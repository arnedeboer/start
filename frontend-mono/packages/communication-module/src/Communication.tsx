// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Route, Routes } from 'react-router-dom';
import classNames from 'classnames';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import { CommunicationContext } from './Communication.context';
import { useCommunicationStyle } from './Communication.style';
import Threads from './components/Threads/Threads';
import Thread from './components/Thread/Thread';
import MessageForm from './components/MessageForm/MessageForm';

const Communication = () => {
  const [t] = useTranslation('communication');
  const classes = useCommunicationStyle();
  const {
    capabilities: { allowSplitScreen },
  } = useContext(CommunicationContext);

  const width = useWidth();
  const threadListBreakpoint = allowSplitScreen
    ? ['md', 'lg', 'xl'].includes(width)
    : false;

  return (
    <div
      className={classNames(
        classes.wrapper,
        allowSplitScreen && classes.wrapperResponsive
      )}
    >
      {threadListBreakpoint && (
        <div className={classes.threadListWrapper}>
          <Threads />
        </div>
      )}

      <div className={classes.contentOuterWrapper}>
        <Routes>
          <Route
            path={'new/:type/*'}
            element={
              <MessageForm
                alwaysShowBackButton={allowSplitScreen === false}
                width={width}
              />
            }
          />

          <Route path={'view/:threadId'} element={<Thread width={width} />} />

          {threadListBreakpoint ? (
            <Route
              path=""
              element={
                <div className={classes.placeholder}>{t('placeholder')}</div>
              }
            />
          ) : (
            <Route
              path=""
              element={
                <div className={classes.threadListWrapper}>
                  <Threads />
                </div>
              }
            />
          )}
        </Routes>
      </div>
    </div>
  );
};

export default Communication;
