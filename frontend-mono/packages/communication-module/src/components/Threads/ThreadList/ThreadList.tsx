// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  AutoSizer,
  InfiniteLoader,
  List,
  ListRowProps,
} from 'react-virtualized';
import { ThreadType } from '../../Thread/Thread.types';
import { CommunicationContextContextType } from '../../../types/Context.types';
import NoteThreadListItem from './ThreadListItem/NoteThreadListItem';
import ContactMomentThreadListItem from './ThreadListItem/ContactMomentThreadListItem';
import PipMessageThreadListItem from './ThreadListItem/PipMessageThreadListItem';
import EmailThreadListItem from './ThreadListItem/EmailThreadListItem';
import PostexThreadListItem from './ThreadListItem/PostexThreadListItem';

export interface ThreadListPropsType {
  threads: ThreadType[];
  selectedThreadId?: string;
  showLinkToCase: boolean;
  context: CommunicationContextContextType;
  loadMoreRows: () => Promise<any>;
  isFetchingNextPage: boolean;
  hasMorePages: boolean;
}

/* eslint complexity: [2, 9] */
const ThreadList: React.ComponentType<ThreadListPropsType> = ({
  threads,
  selectedThreadId,
  showLinkToCase,
  loadMoreRows,
  isFetchingNextPage,
  hasMorePages,
}) => {
  const rowRenderer = ({ index, style }: ListRowProps) => {
    const data = threads[index];
    const lastMessage = data.lastMessage;
    const { id } = data;
    const selected = selectedThreadId === id;
    const props = {
      selected,
      style,
      showLinkToCase,
      caseNumber: data.caseNumber,
      createdByName: lastMessage.createdByName,
      isUnread: data.unread,
      ...data,
    };

    switch (lastMessage.type) {
      case 'contact_moment':
        return (
          <ContactMomentThreadListItem
            {...props}
            key={id}
            withName={lastMessage.withName}
            channel={lastMessage.channel}
          />
        );

      case 'email':
        return (
          <EmailThreadListItem
            {...props}
            key={id}
            messageCount={data.numberOfMessages}
            subject={lastMessage.subject}
            failureReason={lastMessage.failureReason || ''}
          />
        );

      case 'note':
        return <NoteThreadListItem {...props} key={id} />;

      case 'postex':
        return (
          <PostexThreadListItem
            {...props}
            key={id}
            messageCount={data.numberOfMessages}
            subject={lastMessage.subject}
            failureReason={lastMessage.failureReason || ''}
          />
        );

      case 'pip_message':
        return (
          <PipMessageThreadListItem
            {...props}
            key={id}
            messageCount={data.numberOfMessages}
            subject={lastMessage.subject}
          />
        );
    }
  };

  return (
    <AutoSizer>
      {stats => (
        <InfiniteLoader
          isRowLoaded={({ index }) => index < threads.length}
          loadMoreRows={({ stopIndex }) => {
            return stopIndex > threads.length &&
              !isFetchingNextPage &&
              hasMorePages
              ? loadMoreRows()
              : Promise.resolve();
          }}
          rowCount={99999}
          threshold={10}
        >
          {({ onRowsRendered, registerChild }) => (
            <List
              ref={registerChild}
              onRowsRendered={onRowsRendered}
              height={stats.height}
              width={stats.width}
              rowCount={threads.length}
              rowHeight={110}
              rowRenderer={rowRenderer}
            />
          )}
        </InfiniteLoader>
      )}
    </AutoSizer>
  );
};

export default ThreadList;
