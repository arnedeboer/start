// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { boolean, number, text, stories } from '@mintlab/ui/App/story';
import PipMessageThreadListItem from './PipMessageThreadListItem';

const Wrapper: React.ComponentType = ({ children }) => (
  <div style={{ maxWidth: 400 }}>{children}</div>
);

const getProps = () => ({
  createdByName: 'Foo bar',
  date: new Date(),
  id: '1',
  messageCount: number('Number of messages', 2),
  isUnread: boolean('Unread', false),
  selected: boolean('Selected', false),
  showLinkToCase: false,
  subject: text('Subject', 'Lorum ipsum dolar sit amet'),
  type: 'pip_message',
  summary: 'Lorum ipsum dolar sit amet',
  hasAttachment: boolean('Has attachment', false),
  style: {},
  numberOfMessages: 0,
});

stories(module, `${__dirname}/PipMessageThreadListItem`, {
  Default() {
    return (
      <Wrapper>
        <PipMessageThreadListItem {...getProps()} />
      </Wrapper>
    );
  },

  Selected() {
    const selectedProps = {
      ...getProps(),
      selected: true,
    };
    return (
      <Wrapper>
        <PipMessageThreadListItem {...selectedProps} />
      </Wrapper>
    );
  },

  Unread() {
    const unreadProps = {
      ...getProps(),
      isUnread: true,
    };
    return (
      <Wrapper>
        <PipMessageThreadListItem {...unreadProps} />
      </Wrapper>
    );
  },
});
