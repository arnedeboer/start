// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { boolean, select, stories } from '@mintlab/ui/App/story';
import * as CHANNELS from '../../../../Communication.constants';
import ContactMomentThreadListItem from './ContactMomentThreadListItem';

const Wrapper: React.ComponentType = ({ children }) => (
  <div style={{ maxWidth: 400 }}>{children}</div>
);

const getProps = () => {
  const channel = select(
    'Channel',
    Object.values(CHANNELS),
    CHANNELS.TYPE_CHANNEL_FRONTDESK
  );

  return {
    channel: channel,
    createdByName: 'Foo bar',
    withName: 'Baz',
    date: new Date(),
    id: '1',
    isUnread: false,
    selected: boolean('Selected', false),
    showLinkToCase: false,
    type: 'email',
    summary: '',
    style: {},
    numberOfMessages: 0,
  };
};

stories(module, `${__dirname}/ContactMomentThreadListItem`, {
  Default() {
    return (
      <Wrapper>
        <ContactMomentThreadListItem {...getProps()} />
      </Wrapper>
    );
  },

  Selected() {
    const selectedProps = {
      ...getProps(),
      selected: true,
    };

    return (
      <Wrapper>
        <ContactMomentThreadListItem {...selectedProps} />
      </Wrapper>
    );
  },
});
