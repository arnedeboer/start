// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useThreadsHeaderStyle = makeStyles(
  ({ typography, mintlab: { greyscale, shadows }, breakpoints }: any) => ({
    wrapper: {
      padding: '15px 12px',
      display: 'flex',
      flexDirection: 'column',
      boxShadow: shadows.flat,
      [breakpoints.up('sm')]: {
        padding: '15px 20px',
      },
    },
    actionsWrapper: {
      display: 'flex',
      alignItems: 'center',
      paddingBottom: 15,
    },
    buttonActionWrapper: {
      flexGrow: 1,
    },
    filterActionWrapper: {
      width: 200,
    },
    listWrapper: {
      flexGrow: 1,
    },
    searchIcon: {
      color: greyscale.darkest,
    },
    label: {
      marginBottom: 10,
      display: 'block',
      ...typography.subtitle1,
    },
  })
);
