// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { v4 as unique } from 'uuid';
import Button from '@mintlab/ui/App/Material/Button';
import TextField from '@mintlab/ui/App/Material/TextField';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import {
  addScopeProp,
  addScopeAttribute,
} from '@mintlab/ui/App/library/addScope';
import { CommunicationContext } from '../../../Communication.context';
import ActionMenu from '../../shared/ActionMenu/ActionMenu';
import { FilterType } from '../Threads.library';
import { useThreadsHeaderStyle } from './ThreadsHeader.style';
import {
  canCreate,
  getDefaultCreateTypeFromCapabilities,
} from './ThreadsHeader.library';

export interface ThreadsHeaderPropsType {
  filter?: FilterType;
  onFilterChange: (filter?: FilterType) => void;
  searchTerm: string;
  onSearchTermChange: (searchTerm: string) => void;
}

/* eslint complexity: [2, 9] */
const ThreadsHeader: React.ComponentType<ThreadsHeaderPropsType> = ({
  filter,
  searchTerm,
  onFilterChange,
  onSearchTermChange,
}) => {
  const { capabilities, setImportMessageDialogOpened } =
    React.useContext(CommunicationContext);
  const classes = useThreadsHeaderStyle();
  const [t] = useTranslation(['communication']);

  const addThreadPath = `new/${getDefaultCreateTypeFromCapabilities(
    capabilities
  )}`;
  const LinkComponent = React.forwardRef<HTMLAnchorElement>(
    //@ts-ignore
    ({ children, ...restProps }, ref) => (
      <Link {...restProps} to={addThreadPath}>
        {children}
      </Link>
    )
  );
  LinkComponent.displayName = 'AddButtonLink';

  const actionButtons = [
    [
      {
        action: () => setImportMessageDialogOpened(true),
        label: t('threads.actions.import'),
        condition: capabilities.canImportMessage,
      },
    ],
  ];

  const displayActionWrapper =
    canCreate(capabilities) ||
    capabilities.canFilter ||
    actionButtons.length > 0;
  const searchId = unique();

  return (
    <div className={classes.wrapper}>
      {displayActionWrapper && (
        <div className={classes.actionsWrapper}>
          {canCreate(capabilities) && (
            <div className={classes.buttonActionWrapper}>
              <Button
                name="addThread"
                component={LinkComponent}
                icon="add"
                {...addScopeProp('add-thread')}
              >
                {t('forms.new')}
              </Button>
            </div>
          )}
          <div
            className={classes.filterActionWrapper}
            {...addScopeAttribute('filter-thread')}
          >
            {capabilities.canFilter && (
              <FlatValueSelect
                value={filter}
                isClearable={true}
                isSearchable={false}
                placeholder="Filter"
                choices={[
                  'note',
                  'contact_moment',
                  'email',
                  'pip_message',
                  'postex',
                ].map(type => ({
                  label: t(`threadTypes.${type}`),
                  value: type,
                }))}
                onChange={(ev: React.ChangeEvent<HTMLSelectElement>) =>
                  onFilterChange(ev.target.value as FilterType)
                }
              />
            )}
          </div>
          {actionButtons.length > 0 && (
            <ActionMenu actions={actionButtons} scope={'threadList:header'} />
          )}
        </div>
      )}
      <div>
        <label htmlFor={searchId} className={classes.label}>
          {t('thread.searchLabel')}
        </label>
        <TextField
          id={searchId}
          variant="generic2"
          name={t('thread.searchPlaceholder')}
          startAdornment={
            <div className={classes.searchIcon}>
              <Icon size="small" color="inherit">
                {iconNames.search}
              </Icon>
            </div>
          }
          placeholder={t('thread.searchPlaceholder')}
          closeName={t('thread.searchClose')}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            onSearchTermChange(event.currentTarget.value)
          }
          value={searchTerm}
          {...(searchTerm && { closeAction: () => onSearchTermChange('') })}
          {...addScopeProp('search-thread')}
        />
      </div>
    </div>
  );
};

export default ThreadsHeader;
