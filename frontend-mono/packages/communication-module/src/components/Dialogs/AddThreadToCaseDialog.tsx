// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useMutation } from '@tanstack/react-query';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { useTranslation } from 'react-i18next';
import { AddThreadToCaseValuesType } from '../Thread/Thread.types';
import { addThreadToCase } from '../../Communication.requests';

const formDefinition: FormDefinition<AddThreadToCaseValuesType> = [
  {
    name: 'case_uuid',
    type: fieldTypes.CASE_FINDER,
    value: null,
    required: true,
    nestedValue: true,
    filter: {
      status: ['new', 'open', 'stalled'],
    },
    placeholder: 'dialogs.addThreadToCase.select',
    label: 'dialogs.addThreadToCase.select',
  },
];

const AddThreadToCaseDialog = ({
  threadUuid,
  onClose,
}: {
  threadUuid: string;
  onClose: () => void;
}) => {
  const [t] = useTranslation('communication');

  const { mutateAsync, isLoading, error } = useMutation<
    any,
    V2ServerErrorsType
  >((values: any) => addThreadToCase(values.case_uuid, threadUuid), {
    onSuccess: () => {
      onClose();
    },
  });

  return (
    <>
      {error && <ServerErrorDialog err={error} />}
      <FormDialog
        saving={isLoading}
        //@ts-ignore
        formDefinition={formDefinition}
        formDefinitionT={t}
        onSubmit={mutateAsync}
        onClose={onClose}
        title={t('dialogs.addThreadToCase.title')}
        scope="case-select-dialog"
      />
    </>
  );
};

export default AddThreadToCaseDialog;
