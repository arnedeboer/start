// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { TFunction } from 'i18next';
import {
  ParticipantsType,
  ParticipantType,
} from '../../../types/Message.types';
import { SubjectRelationType } from '../../../types/SubjectRelations';
import { GetHtmlEmailTemplateDataType } from '../MessageForm.types';

export const getRecipientTypeChoices = () =>
  ['requestor', 'coordinator', 'colleague', 'role', 'authorized', 'other'].map(
    value => ({ label: `addFields.recipientTypes.${value}`, value })
  );

export const getRequestorValue = (
  t: TFunction,
  subjectRelations?: SubjectRelationType[]
) => {
  const requestor = subjectRelations?.find(
    relation => relation.role === 'aanvrager'
  ) as SubjectRelationType;

  if (requestor.isPresetClient) {
    return {
      label: t('addFields.requestor.presetClient', {
        requestor: requestor.label,
      }),
    };
  } else if (!requestor.value) {
    return {
      label: t('addFields.requestor.noEmail', { requestor: requestor.label }),
    };
  } else {
    return requestor;
  }
};

export const getCoordinatorValue = (
  t: TFunction,
  subjectRelations?: SubjectRelationType[]
) => {
  const coordinator = subjectRelations?.find(
    relation => relation.role === 'coordinator'
  ) as SubjectRelationType;

  if (coordinator) {
    return coordinator;
  } else {
    return {
      label: t('addFields.coordinator.none'),
    };
  }
};

const excludedRoles = ['aanvrager', 'behandelaar', 'coordinator'];

export const getRoleChoices = (subjectRelations?: SubjectRelationType[]) => {
  return subjectRelations?.filter(
    subject => !excludedRoles.includes(subject.role)
  );
};

export const getAuthorizedChoices = (
  subjectRelations?: SubjectRelationType[]
) => {
  return subjectRelations?.filter(
    subject => subject.authorized && !excludedRoles.includes(subject.role)
  );
};

const convertParticipantToValue = ({ name, email }: ParticipantType) => {
  const label = name === email ? name : `${name} (${email})`;

  return {
    label,
    value: email,
  };
};

export const convertParticipantsToValues = ({
  from,
  to,
  cc,
  bcc,
}: ParticipantsType) => ({
  other: [...from, ...to].map(convertParticipantToValue),
  cc: cc.map(convertParticipantToValue),
  bcc: bcc.map(convertParticipantToValue),
});

export const getHtmlEmailTemplateData: GetHtmlEmailTemplateDataType = (
  emailIntegrations,
  htmlEmailTemplateName
) => {
  // the admin could potentially configure multiple email integrations
  //   but we only support configuring one, so we pick the first
  const emailIntegration = emailIntegrations[0];

  if (!emailIntegration || !htmlEmailTemplateName) {
    return;
  }

  const htmlEmailTemplatesSettings =
    emailIntegration.instance.interface_config.rich_email_templates;

  // the only way to know which template is relevant for the case (if any)
  //   is to match it to the label returned in the case call
  // note: the template set in the case might have been removed from the integration
  const htmlEmailTemplateSettings = htmlEmailTemplatesSettings.find(
    config => config.label === htmlEmailTemplateName
  );

  if (!htmlEmailTemplateSettings) {
    return;
  }

  const { label, template, image } = htmlEmailTemplateSettings;
  // only one image can be uploaded, but it's returned as an array
  const imageUuid = image ? image[0].uuid : '';

  return {
    label,
    template,
    imageUuid,
  };
};
