// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useMemo, useEffect, useState, useContext, useRef } from 'react';
import { useNavigate } from 'react-router';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import GenericForm from '../GenericForm/GenericForm';
import { CommunicationContext } from '../../../Communication.context';
import {
  ComponentPropsType,
  HtmlEmailTemplateData,
} from '../MessageForm.types';
import { GET_MESSAGES, GET_THREADS } from '../../../Communication.constants';
import {
  getEmailIntegrations,
  getPreview,
  saveEmailAction,
} from './../MessageForm.library';
import {
  getEmailFormDefinition,
  getEmailRules,
  SaveEmailValuesType,
} from './Email.formDefinition';
import {
  convertParticipantsToValues,
  getHtmlEmailTemplateData,
} from './Email.library';

const Email: React.ComponentType<ComponentPropsType> = ({
  onSubmit,
  cancel,
  caseUuid,
  contactUuid,
  threadUuid,
  firstMessage,
  lastMessage,
}) => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  const [t] = useTranslation('communication');
  const prevRef = useRef();

  const commContext = useContext(CommunicationContext);
  const { htmlEmailTemplateName, emailTemplates, subjectRelations } =
    commContext;

  const resolvedCaseUuid = commContext.caseUuid || caseUuid;

  const [htmlEmailTemplateData, setHtmlEmailTemplateData] = useState<
    HtmlEmailTemplateData | undefined
  >(undefined);

  useEffect(() => {
    async function fetchHtmlEmailTemplateSettings() {
      const emailIntegrations = await getEmailIntegrations();

      setHtmlEmailTemplateData(
        getHtmlEmailTemplateData(emailIntegrations, htmlEmailTemplateName)
      );
    }

    fetchHtmlEmailTemplateSettings();
  }, []);

  const { mutate, isLoading } = useMutation({
    mutationKey: ['saveContactMoment'],
    mutationFn: async (values: any) => {
      const resolvedValues = await getPreview(values, values.case_uuid);

      return saveEmailAction(resolvedValues, threadUuid);
    },
    onSuccess: () => {
      queryClient.refetchQueries(GET_THREADS);

      if (threadUuid && onSubmit) {
        queryClient.refetchQueries(GET_MESSAGES);
        onSubmit();
      } else {
        navigate('..');
      }
    },
  });

  const htmlEmailTemplateDescription = htmlEmailTemplateName
    ? t('addFields.htmlEmailTemplateDescription', {
        htmlEmailTemplateName,
      })
    : undefined;
  const formDefinition = useMemo(() => {
    return getEmailFormDefinition(
      t,
      resolvedCaseUuid,
      contactUuid,
      htmlEmailTemplateDescription,
      emailTemplates,
      subjectRelations
    );
  }, [resolvedCaseUuid, contactUuid]);

  const formDefinitionWithValues = useMemo(() => {
    return firstMessage && lastMessage
      ? mapValuesToFormDefinition<SaveEmailValuesType>(
          {
            recipient_type: 'other',
            ...convertParticipantsToValues(lastMessage.participants),
            subject: `RE: ${firstMessage.subject}`,
          },
          formDefinition
        )
      : formDefinition;
  }, []);

  const rules = getEmailRules(prevRef, subjectRelations);

  const customValidator = (values: any) => {
    const { recipient_type, requestor, coordinator } = values;

    if (
      (recipient_type.value === 'requestor' && !requestor[0].value) ||
      (recipient_type.value === 'coordinator' && !coordinator[0].value)
    ) {
      return false;
    }

    return true;
  };

  return (
    <GenericForm
      cancel={cancel}
      busy={isLoading}
      customValidator={customValidator}
      save={mutate}
      formDefinition={formDefinitionWithValues}
      formName="email-form"
      rules={rules}
      primaryButtonLabelKey={t('forms.send')}
      htmlEmailTemplateData={htmlEmailTemplateData}
      enablePreview={true}
    />
  );
};

export default Email;
