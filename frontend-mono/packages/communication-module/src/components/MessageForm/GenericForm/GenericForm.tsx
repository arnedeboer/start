// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import CaseFinder from '@zaaksysteem/common/src/components/form/fields/CaseFinder/CaseFinder';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { CASE_FINDER, CASE_SELECTOR } from '../../../Communication.constants';
import { MessageFormComponentPropsType } from '../MessageForm.types';
import CaseSelector from '../Fields/CaseSelector';
import { useGenericFormStyle } from './GenericForm.styles';
import { renderField } from './library/renderField';
import Preview from './Preview';

const typeMap = {
  [CASE_FINDER]: CaseFinder,
  [CASE_SELECTOR]: CaseSelector,
};

/* eslint complexity: [2, 8] */
const GenericForm: React.ComponentType<MessageFormComponentPropsType> = ({
  save,
  cancel,
  busy,
  customValidator,
  formDefinition,
  rules = [],
  htmlEmailTemplateData,
  formName,
  top,
  enablePreview = false,
  primaryButtonLabelKey = 'forms.add',
  labels = false,
}) => {
  const [t] = useTranslation('communication');
  const [previewActive, setPreviewActive] = React.useState(false);
  const classes = useGenericFormStyle();

  const {
    fields,
    formik: { values, isValid },
  } = useForm({
    t,
    formDefinition,
    rules,
    fieldComponents: typeMap,
    isInitialValid: true,
  });

  const isCustomValid = customValidator ? customValidator(values) : true;

  return (
    <div className={classes.formWrapper}>
      {top}
      {previewActive ? (
        <Preview
          formName={formName}
          values={values}
          fields={fields}
          htmlEmailTemplateData={htmlEmailTemplateData}
        />
      ) : (
        fields.map(renderField({ classes, formName }))
      )}

      <div className={classes.buttons}>
        <Button
          disabled={!isValid || !isCustomValid || busy}
          action={() => {
            save(values);
          }}
          name="saveGenericMessage"
        >
          {t(primaryButtonLabelKey)}
        </Button>
        {enablePreview && (
          <Button
            action={() => {
              setPreviewActive(!previewActive);
            }}
            name="previewGenericMessage"
            variant="text"
          >
            {previewActive ? t('forms.closePreview') : t('forms.openPreview')}
          </Button>
        )}
        <Button action={cancel} name="cancelGenericMessage" variant="text">
          {t('forms.cancel')}
        </Button>
      </div>
    </div>
  );
};

export default GenericForm;
