// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useNavigate } from 'react-router';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { CommunicationContext } from '../../../Communication.context';
import { GET_THREADS } from '../../../Communication.constants';
import GenericMessageForm from '../GenericForm/GenericForm';
import { ComponentPropsType } from '../MessageForm.types';
import { saveContactMomentAction } from '../MessageForm.library';
import { getContactMomentFormDefinition } from './ContactMoment.formDefinition';

const ContactMoment: React.ComponentType<ComponentPropsType> = ({ cancel }) => {
  const context = React.useContext(CommunicationContext);
  const navigate = useNavigate();
  const queryClient = useQueryClient();

  const { mutate, isLoading, error } = useMutation<any, V2ServerErrorsType>({
    mutationKey: ['saveContactMoment'],
    mutationFn: (values: any) => saveContactMomentAction(values),
    onSuccess: () => {
      queryClient.refetchQueries(GET_THREADS);
      navigate('..');
    },
  });

  const formDefinition = getContactMomentFormDefinition(context);

  return (
    <>
      {error && <ServerErrorDialog err={error} />}
      <GenericMessageForm
        cancel={cancel}
        busy={isLoading}
        formDefinition={formDefinition}
        formName="contact-moment"
        save={mutate}
      />
    </>
  );
};

export default ContactMoment;
