// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { NoteRequestBody } from '../MessageForm.types';

export type SaveNoteFormValuesType = Pick<NoteRequestBody, 'content'> &
  ({ case_uuid: string } | { contact_uuid: string });

type NoteFormDefinitionType = FormDefinition<SaveNoteFormValuesType>;

export const noteFormDefinition: NoteFormDefinitionType = [
  {
    name: 'content',
    label: 'addFields.note',
    type: fieldTypes.TEXT,
    multi: true,
    value: '',
    required: true,
    placeholder: 'addFields.note',
    isMultiline: true,
    rows: 7,
  },
];
