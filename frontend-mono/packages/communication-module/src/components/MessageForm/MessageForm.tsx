// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import PlusButtonSpaceWrapper from '@zaaksysteem/common/src/components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import {
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from '../../Communication.constants';
import SwitchViewButton from '../shared/SwitchViewButton/SwitchViewButton';
import { TypeOfCreateMessage } from '../../types/Message.types';
import ScrollWrapper from '../shared/ScrollWrapper/ScrollWrapper';
import { useMessageFormStyle } from './MessageForm.styles';
import Pip from './Pip/Pip';
import Email from './Email/Email';
import ContactMoment from './ContactMoment/ContactMoment';
import Note from './Note/Note';
import Tabs from './Tabs';

const components = {
  [TYPE_PIP_MESSAGE]: Pip,
  [TYPE_EMAIL]: Email,
  [TYPE_CONTACT_MOMENT]: ContactMoment,
  [TYPE_NOTE]: Note,
};

export type MessageFormPropsType = {
  width: string;
  alwaysShowBackButton: boolean;
};

type MessageFormParamsType = {
  type: TypeOfCreateMessage;
};

const MessageForm: React.FunctionComponent<MessageFormPropsType> = ({
  width,
  alwaysShowBackButton,
}) => {
  const classes = useMessageFormStyle();

  const navigate = useNavigate();
  const { type } = useParams<
    keyof MessageFormParamsType
  >() as MessageFormParamsType;

  const ContainerElement = components[type];

  const showBackButton = alwaysShowBackButton || ['xs', 'sm'].includes(width);
  const cancel = () => navigate('..');

  return (
    <React.Fragment>
      {showBackButton && <SwitchViewButton />}
      <ScrollWrapper>
        <div className={classes.addFormWrapper}>
          <Tabs type={type} />
          <ErrorBoundary>
            <PlusButtonSpaceWrapper>
              <ContainerElement cancel={cancel} />
            </PlusButtonSpaceWrapper>
          </ErrorBoundary>
        </div>
      </ScrollWrapper>
    </React.Fragment>
  );
};

export default MessageForm;
