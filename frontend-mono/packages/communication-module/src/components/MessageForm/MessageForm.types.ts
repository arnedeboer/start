// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ReactElement } from 'react';
import { APICommunication } from '@zaaksysteem/generated';
import {
  FormDefinition,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { Rule } from '@zaaksysteem/common/src/components/form/rules';

// PREVIEW

type PreviewRequestBodyType = {
  body: string;
  case_uuid: string;
};

type PreviewResponseBodyType = { body: string };

export type FetchPreviewType = (
  data: PreviewRequestBodyType
) => Promise<PreviewResponseBodyType>;

export type GetPreviewType = (
  values: FormValuesType,
  case_uuid: string
) => Promise<FormValuesType>;

// PIP

type PipRequestBody = APICommunication.CreateExternalMessageRequestBody;

export type SavePipType = (data: PipRequestBody) => any;

export type SavePipActionType = (values: any, thread_uuid?: string) => any;

// EMAIL INTEGRATION

export type HtmlEmailTemplateData = {
  label: string;
  template: string;
  imageUuid: string;
};

type HtmlEmailTemplateSettingsType = {
  label: string;
  template: string;
  image: {
    uuid: string;
  }[];
};

type EmailIntegration = {
  instance: {
    interface_config: { rich_email_templates: HtmlEmailTemplateSettingsType[] };
  };
};

type EmailIntegrationResponseBody = {
  result: {
    instance: {
      rows: EmailIntegration[];
    };
  };
};

export type GetEmailIntegrationsType = () => Promise<EmailIntegration[]>;

export type FetchEmailIntegrationsType =
  () => Promise<EmailIntegrationResponseBody>;

// EMAIL

type ParticipantType = APICommunication.MessageParticipant;

export type CreateParticipantType = (
  subject: { value: string; label: string; uuid?: string },
  role: 'to' | 'cc' | 'bcc'
) => ParticipantType;

export type GetParticipantsType = (values: any) => ParticipantType[];

type EmailRequestBody = APICommunication.CreateExternalMessageRequestBody;

export type SaveEmailType = (data: EmailRequestBody) => any;

export type SaveEmailActionType = (values: any, thread_uuid?: string) => any;

export type GetHtmlEmailTemplateDataType = (
  emailIntegrations: EmailIntegration[],
  htmlEmailTemplateName?: string
) => HtmlEmailTemplateData | undefined;

// CONTACT_MOMENT

export type ContactMomentRequestBody =
  APICommunication.CreateContactMomentRequestBody;

export type SaveContactMomentType = (data: ContactMomentRequestBody) => any;

export type SaveContactMomentActionType = (values: any) => any;

// NOTE

export type NoteRequestBody = APICommunication.CreateNoteRequestBody;

export type SaveNoteType = (data: NoteRequestBody) => any;

export type SaveNoteActionType = (
  values: any,
  caseUuid?: string,
  contactUuid?: string
) => any;

// REST

export type ComponentPropsType = Pick<
  MessageFormComponentPropsType,
  | 'onSubmit'
  | 'cancel'
  | 'caseUuid'
  | 'contactUuid'
  | 'threadUuid'
  | 'firstMessage'
  | 'lastMessage'
>;

export interface MessageFormComponentPropsType<Values = any> {
  onSubmit?: () => void;
  cancel: () => void;
  caseUuid?: string;
  contactUuid?: string;
  threadUuid?: string;
  firstMessage?: any;
  lastMessage?: any;
  save: (values: Values) => void;
  customValidator?: (values: Values) => boolean;
  busy: boolean;
  enablePreview?: boolean;
  formDefinition: FormDefinition<Values>;
  htmlEmailTemplateData?: HtmlEmailTemplateData;
  formName: string;
  top?: ReactElement;
  primaryButtonLabelKey?: string;
  rules?: Rule[];
  labels?: boolean;
}
