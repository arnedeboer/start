// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { asArray } from '@mintlab/kitchen-sink/source';
import { v4 } from 'uuid';
import {
  fetchEmailIntegrations,
  fetchPreview,
  saveContactMoment,
  saveEmail,
  saveNote,
  savePip,
} from './MessageForm.requests';
import {
  CreateParticipantType,
  GetEmailIntegrationsType,
  GetParticipantsType,
  GetPreviewType,
  SaveContactMomentActionType,
  SaveEmailActionType,
  SaveNoteActionType,
  SavePipActionType,
} from './MessageForm.types';

export const getEmailIntegrations: GetEmailIntegrationsType = async () => {
  const response = await fetchEmailIntegrations();

  const integrations = response.result.instance.rows;

  return integrations;
};

export const getPreview: GetPreviewType = async (values, caseUuid) => {
  const data = {
    case_uuid: caseUuid,
    body: JSON.stringify(values),
  };

  const response = await fetchPreview(data);

  const parsedValues = JSON.parse(response.body);

  // magic string values that refer to empty values need to be filtered out
  const resolvedValues = Object.entries(parsedValues).reduce<any>(
    (acc, [key, value]) => {
      if (Array.isArray(value)) {
        const sanitizedValue = value.filter(val => val.value);

        return {
          ...acc,
          [key]: sanitizedValue,
        };
      }

      return {
        ...acc,
        [key]: value,
      };
    },
    {}
  );

  return resolvedValues;
};

export const savePipAction: SavePipActionType = (values, thread_uuid) => {
  const { case_uuid, participants, subject, content, attachments } = values;

  const data = {
    message_type: 'pip' as 'pip',
    case_uuid,
    content,
    subject,
    participants,
    thread_uuid: thread_uuid || v4(),
    message_uuid: v4(),
    attachments: attachments
      ? asArray(attachments).map(attachment => ({
          id: attachment.value.toString(),
          filename: attachment.label,
        }))
      : [],
  };

  return savePip(data);
};

const createParticipant: CreateParticipantType = (
  { value, label, uuid },
  role
) => ({
  address: value,
  role,
  display_name: label || value,
  ...(uuid ? { uuid } : {}),
});

const getParticipants: GetParticipantsType = values => {
  const { recipient_type, cc, bcc } = values;

  const to = values[recipient_type.value || recipient_type];
  const toValues = to.reduce(
    (acc: { value: string }[], subject: { value: string }) => [
      ...acc,
      ...subject.value
        .split(',')
        .map((val: string) => ({ value: val.replace(' ', '') })),
    ],
    []
  );

  return [
    ...toValues.map((subject: any) => createParticipant(subject, 'to')),
    ...cc.map((subject: any) => createParticipant(subject, 'cc')),
    ...bcc.map((subject: any) => createParticipant(subject, 'bcc')),
  ];
};

export const saveEmailAction: SaveEmailActionType = (values, threadUuid) => {
  const { case_uuid, subject, content, attachments } = values;
  const participants = getParticipants(values);

  const data = {
    message_type: 'email' as 'email',
    direction: 'outgoing' as 'outgoing',
    message_uuid: v4(),
    thread_uuid: threadUuid || v4(),
    case_uuid,
    participants,
    subject,
    content,
    attachments: attachments
      ? asArray(attachments).map(attachment => ({
          id: attachment.value.toString(),
          filename: attachment.label,
        }))
      : [],
  };

  return saveEmail(data);
};

export const saveContactMomentAction: SaveContactMomentActionType = ({
  case_uuid,
  contact_uuid,
  channel,
  content,
  direction,
}) => {
  const data = {
    thread_uuid: v4(),
    contact_moment_uuid: v4(),
    //@ts-ignore
    case_uuid: case_uuid?.value || case_uuid || null,
    contact_uuid: contact_uuid.value,
    channel,
    content,
    direction,
  };

  return saveContactMoment(data);
};

export const saveNoteAction: SaveNoteActionType = (
  { content },
  caseUuid,
  contactUuid
) => {
  const data = {
    thread_uuid: v4(),
    note_uuid: v4(),
    content,
    ...(caseUuid
      ? { case_uuid: caseUuid }
      : { contact_uuid: contactUuid || '' }),
  };

  return saveNote(data);
};
