// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import MUITabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { useTheme } from '@mui/material';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
} from '../../Communication.constants';
import { CommunicationContext } from '../../Communication.context';

export type TabsPropsType = {
  type: string;
};

type LinkComponentPropsType = {
  linktype: string;
};

const LinkComponent = React.forwardRef<
  HTMLAnchorElement,
  LinkComponentPropsType
>((props, ref) => <Link to={`../new/${props.linktype}`} {...props} />);

/* eslint-disable complexity */
const Tabs: React.FunctionComponent<TabsPropsType> = ({ type }) => {
  const [t] = useTranslation('communication');
  const {
    typography,
    palette: { primary },
    mintlab: { greyscale },
  } = useTheme<Theme>();

  const {
    capabilities: {
      canCreatePipMessage,
      canCreateEmail,
      canCreateContactMoment,
      canCreateNote,
    },
  } = useContext(CommunicationContext);

  LinkComponent.displayName = 'TabLink';

  const tabStyle = {
    textTransform: 'none',
    flexBasis: 'auto',
    flexDirection: 'row',
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '18px',
    padding: '3px 20px',
    backgroundColor: 'transparent',
    color: greyscale.darkest,
    ...typography.subtitle1,

    '&& .Mui-selected': {
      color: primary.main,
    },
    '&&& > span': {
      margin: 0,
      marginRight: '12px',
    },
  };

  const tabs = [
    {
      label: 'pip_message',
      value: TYPE_PIP_MESSAGE,
      icon: iconNames.mail_lock,
      when: canCreatePipMessage,
    },
    {
      label: 'email',
      value: TYPE_EMAIL,
      icon: iconNames.email,
      when: canCreateEmail,
    },
    {
      label: 'contact_moment',
      value: TYPE_CONTACT_MOMENT,
      icon: iconNames.chat_bubble,
      when: canCreateContactMoment,
    },
    {
      label: 'note',
      value: TYPE_NOTE,
      icon: iconNames.sort,
      when: canCreateNote,
    },
  ].filter(tab => tab.when);

  return tabs.length < 2 ? null : (
    <React.Fragment>
      <MUITabs
        sx={{
          marginBottom: '36px',
          minHeight: '60px',
          '& .MuiTabs-flexContainer': {
            '&>:nth-child(2)': {
              margin: '0px 20px',
              textWrap: 'nowrap',
            },
          },
        }}
        TabIndicatorProps={{
          sx: { display: 'none' },
        }}
        value={type}
        variant="fullWidth"
        indicatorColor="primary"
      >
        {tabs.map(({ label, value, icon }) => (
          <Tab
            key={label}
            label={t(`threadTypes.${label}`)}
            value={value}
            linktype={value}
            icon={<Icon size="small">{icon}</Icon>}
            sx={tabStyle}
            disableTouchRipple={true}
            component={LinkComponent}
            {...addScopeAttribute('add-form', 'tab', value)}
          />
        ))}
      </MUITabs>
    </React.Fragment>
  );
};

export default Tabs;
