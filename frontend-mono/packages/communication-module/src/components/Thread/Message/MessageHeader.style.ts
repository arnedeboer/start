// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const messageHeaderStyle = (
  { mintlab: { greyscale }, palette: { primary } }: Theme,
  isUnread: boolean
) =>
  ({
    header: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'flex-start',
    },
    icon: {
      marginTop: 5,
      minWidth: 50,
      display: 'flex',
      justifyContent: 'left',
    },
    meta: {
      flex: '1 1 auto',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      minWidth: 0,
    },
    title: {
      display: 'flex',
      justifyContent: 'flex-start',
    },
    titleMain: {
      whiteSpace: 'nowrap',
      fontWeight: isUnread ? 700 : 500,
    },
    titleSub: {
      color: greyscale.darker,
      marginLeft: 10,
    },
    info: {
      minWidth: 0,
      color: isUnread ? primary.main : greyscale.darkest,
    },
    dateAndMenu: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      '&>span>svg': {
        paddingBottom: 4,
      },
    },
    date: {
      marginLeft: 5,
      color: isUnread ? primary.main : greyscale.darkest,
    },
    link: {
      color: primary.main,

      '&:hover, &:focus': {
        color: primary.darker,
      },
    },
  }) as const;
