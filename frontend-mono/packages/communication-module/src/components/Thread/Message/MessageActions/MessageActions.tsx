// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useConfirmDialog } from '@zaaksysteem/common/src/hooks/useConfirmDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import {
  TYPES_EXTERNAL_MESSAGE,
  GET_MESSAGES,
  GET_THREADS,
} from '../../../../Communication.constants';
import { CommunicationContext } from '../../../../Communication.context';
import { MessageType } from '../../../../types/Message.types';
import { ThreadType } from '../../Thread.types';
import ActionMenu from '../../../shared/ActionMenu/ActionMenu';
import {
  markMessageAsUnread,
  postCommunicationReadCountChange,
  deleteMessage,
  addSourceFileToCase,
} from './MessageActions.library';

/* eslint complexity: [2, 11]*/
export const MessageActions: React.ComponentType<{
  message: MessageType;
  thread: ThreadType;
}> = ({ message, thread }) => {
  const navigate = useNavigate();
  const { context, capabilities } = useContext(CommunicationContext);
  const client = useQueryClient();
  const { mutate: markAsUnreadMutation } = useMutation({
    mutationKey: ['markAsUnread'],
    mutationFn: () => markMessageAsUnread([message.id], context),
    onSuccess: () => {
      client.refetchQueries(GET_THREADS);
      client.refetchQueries(GET_MESSAGES);
      postCommunicationReadCountChange();
    },
  });

  const { mutate: deleteMutation, error: deleteError } = useMutation<
    any,
    V2ServerErrorsType
  >(() => deleteMessage(message.id), {
    mutationKey: ['deleteMessage'],
    onSuccess: () => {
      if (thread.numberOfMessages === 1) {
        client.refetchQueries(GET_THREADS);

        navigate('..');
      } else {
        client.refetchQueries(GET_MESSAGES);
      }
    },
  });

  const { mutate: addOriginalFileToCase, error: addOriginalError } =
    useMutation<void, V2ServerErrorsType>(
      () => addSourceFileToCase(message.id, message.type, 'original'),
      { mutationKey: ['addOriginalSourceFileToCase'] }
    );

  const { mutate: addPdfFileToCase, error: addPdfError } = useMutation<
    void,
    V2ServerErrorsType
  >(() => addSourceFileToCase(message.id, message.type, 'pdf'), {
    mutationKey: ['addPdfSourceFileToCase'],
  });

  const isExternalMessage = TYPES_EXTERNAL_MESSAGE.includes(message.type);
  const [t] = useTranslation('communication');
  const [confirmDeleteDialog, confirmDelete] = useConfirmDialog(
    t('thread.deleteMessage.title'),
    t('thread.deleteMessage.body'),
    deleteMutation
  );

  const mainActionGroup = [
    {
      label: t('thread.messageActions.delete'),
      action: confirmDelete,
      condition: capabilities.canDeleteMessage && message.type !== 'pip',
    },
    {
      label: t('thread.messageActions.addSourceFileToCaseOriginal'),
      action: addOriginalFileToCase,
      condition:
        isExternalMessage &&
        capabilities.canAddSourceFileToCase &&
        thread.relatedCaseActive &&
        message.hasSourceFile,
    },
    {
      label: t('thread.messageActions.addSourceFileToCasePDF'),
      action: addPdfFileToCase,
      condition: isExternalMessage && capabilities.canAddSourceFileToCase,
    },
    {
      label: t('thread.messageActions.markAsUnread'),
      action: markAsUnreadMutation,
      condition: isExternalMessage && !message.isUnread,
    },
  ];
  const actions = [mainActionGroup];
  const error = addPdfError || addOriginalError || deleteError;

  return mainActionGroup.length > 0 ? (
    <React.Fragment>
      {error && <ServerErrorDialog err={error} />}
      <ActionMenu actions={actions} scope={'thread:message:header'} />
      {confirmDeleteDialog}
    </React.Fragment>
  ) : null;
};
