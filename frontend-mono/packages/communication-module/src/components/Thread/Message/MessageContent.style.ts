// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useMessageContentStyle = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    content: {
      color: greyscale.offblack,
      marginTop: 20,
      marginLeft: '37px',
      marginBottom: 15,
      whiteSpace: 'pre-line',
    },
    body: {
      lineHeight: 1.9,
    },
    background: {
      backgroundColor: greyscale.dark,
      borderRadius: 8,
      padding: 20,
    },
  })
);
