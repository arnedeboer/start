// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import PlusButtonSpaceWrapper from '@zaaksysteem/common/src/components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { get } from '@mintlab/kitchen-sink/source';
import { CommunicationContext } from '../../Communication.context';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
  TYPE_POSTEX,
} from '../../Communication.constants';
import ReplyForm from '../MessageForm/ReplyForm';
import ScrollWrapper from '../shared/ScrollWrapper/ScrollWrapper';
import SwitchViewButton from '../shared/SwitchViewButton/SwitchViewButton';
import MarkAsUnreadSharedVariable from '../../library/MarkAsUnreadSharedVariable';
import ContactMomentMessage from './Message/ContactMomentMessage';
import NoteMessage from './Message/NoteMessage';
import PipMessageMessage from './Message/PipMessage';
import EmailMessage from './Message/EmailMessage';
import PostexMessage from './Message/PostexMessage';
import ContactMomentThreadTitle from './ThreadTitle/ContactMomentThreadTitle';
import NoteThreadTitle from './ThreadTitle/NoteThreadTitle';
import PipMessageThreadTitle from './ThreadTitle/PipMessageThreadTitle';
import EmailThreadTitle from './ThreadTitle/EmailThreadTitle';
import PostexThreadTitle from './ThreadTitle/PostexThreadTitle';
import { useThreadStyles } from './Thread.style';
import {
  useMessagesQuery,
  getThreadReplyability,
  useMarkMessagesAsReadMutation,
} from './Thread.library';

type ElementType = {
  [key: string]: any;
};

const messageElement: ElementType = {
  [TYPE_CONTACT_MOMENT]: ContactMomentMessage,
  [TYPE_NOTE]: NoteMessage,
  [TYPE_PIP_MESSAGE]: PipMessageMessage,
  [TYPE_EMAIL]: EmailMessage,
  [TYPE_POSTEX]: PostexMessage,
};

const titleElement: ElementType = {
  [TYPE_CONTACT_MOMENT]: ContactMomentThreadTitle,
  [TYPE_NOTE]: NoteThreadTitle,
  [TYPE_PIP_MESSAGE]: PipMessageThreadTitle,
  [TYPE_EMAIL]: EmailThreadTitle,
  [TYPE_POSTEX]: PostexThreadTitle,
};

type ThreadPropsType = {
  width: string;
};

/* eslint complexity: [2, 14] */
const Thread = ({ width }: ThreadPropsType) => {
  const context = React.useContext(CommunicationContext);
  const [t] = useTranslation('communication');
  const { threadId } = useParams();
  const classes = useThreadStyles();

  const { mutate: markAsRead } = useMarkMessagesAsReadMutation(context.context);

  const { data, isLoading } = useMessagesQuery(threadId);
  const unreadMessageUuids = (data?.messages || [])
    .filter(message => message.isUnread)
    .map(message => message.id);
  const messageUuidsAggregate = unreadMessageUuids.join();

  React.useEffect(() => {
    messageUuidsAggregate &&
      // What is sharing of what last message was marked by user as unread
      // used for? Its to prevent after marking as unread the refetch of
      // thread triggering this effect and thus calling messsage to be read
      !unreadMessageUuids.includes(MarkAsUnreadSharedVariable.value) &&
      markAsRead(unreadMessageUuids);
  }, [messageUuidsAggregate, MarkAsUnreadSharedVariable.value]);

  const messages = data?.messages;
  const caseRelation = data?.caseRelation;
  const { isReplyable, canSendReply } = getThreadReplyability(
    messages,
    caseRelation,
    data,
    context
  );

  const showBackButton =
    context.capabilities.allowSplitScreen === false ||
    ['xs', 'sm'].includes(width);

  if (!messages || isLoading) {
    return <Loader delay={200} />;
  }

  const [firstMessage] = messages;
  const lastMessage = messages[messages.length - 1];
  const indexingString = firstMessage.type;
  const TitleComponent = titleElement[indexingString];
  const caseNumber = get(caseRelation, 'caseNumber');

  return (
    <ErrorBoundary>
      {showBackButton && <SwitchViewButton />}
      <ScrollWrapper>
        <div className={classes.threadWrapper}>
          <PlusButtonSpaceWrapper>
            <div className={classes.threadTitle}>
              <TitleComponent
                message={firstMessage}
                context={context}
                caseNumber={caseNumber}
                showLinkToCase={context.capabilities.canSelectCase}
                showAddThreadToCaseDialog={() =>
                  context.setAddThreadToCaseDialogOpened(threadId)
                }
                canAddThreadToCase={context.capabilities.canAddThreadToCase}
              />
            </div>
            <div className={classes.threadItemsWrapper}>
              {messages.map((message, index) => {
                const MessageComponent = messageElement[indexingString];

                return (
                  <MessageComponent
                    key={message.id}
                    message={message}
                    thread={data}
                    expanded={index === messages.length - 1}
                  />
                );
              })}
            </div>
            <div className={classes.replyForm}>
              {/* note: caseRelation check is only because typescript doesn't understand
              that canSendReply is only true when there is a caseRelation*/}
              {isReplyable && canSendReply && caseRelation && (
                <ReplyForm
                  key={threadId}
                  threadUuid={threadId || ''}
                  caseUuid={caseRelation.id}
                  firstMessage={firstMessage}
                  lastMessage={lastMessage}
                  type={lastMessage.type}
                />
              )}
              {isReplyable && !canSendReply && (
                <span>{t('replyForm.placeholder')}</span>
              )}
            </div>
          </PlusButtonSpaceWrapper>
        </div>
      </ScrollWrapper>
    </ErrorBoundary>
  );
};

export default Thread;
