// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import ServerErrorDialog from '@zaaksysteem/common/src/components/ServerErrorDialog/ServerErrorDialog';
import { APIDocument } from '@zaaksysteem/generated/types/APIDocument.types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { useMutation } from '@tanstack/react-query';
import { useTheme } from '@mui/material';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { AttachmentType } from '../../../../types/Message.types';
import ActionMenu from '../../../shared/ActionMenu/ActionMenu';
import { useAttachmentStyle } from './Attachment.style';

export type AttachmentPropsType = AttachmentType & {
  canBeAddedToCase: boolean;
  handlePreviewAction?: () => void;
};

const addAttachmentToCase = (id: string) => {
  const data: APIDocument.CreateDocumentFromAttachmentRequestBody = {
    attachment_uuid: id,
  };

  return request(
    'POST',
    '/api/v2/document/create_document_from_attachment',
    data
  );
};

const Attachment: React.FunctionComponent<AttachmentPropsType> = ({
  name,
  size,
  id,
  download,
  canBeAddedToCase,
  handlePreviewAction,
}) => {
  const { mutate, error } = useMutation<any, V2ServerErrorsType, string>(
    addAttachmentToCase
  );
  const theme = useTheme();
  const [t] = useTranslation('communication');
  const classes = useAttachmentStyle();
  const addAttachmentAction = canBeAddedToCase
    ? [
        {
          action: () => mutate(id),
          label: t('attachments.actions.addToCaseDocuments'),
        },
      ]
    : [];
  const actions = [
    [
      {
        action: () => download?.url && window.open(download.url, '_blank'),
        label: t('attachments.actions.download'),
      },
      ...addAttachmentAction,
    ],
  ];
  const KB = Math.max(Math.round(size / 1024), 1);
  const MB = Math.max(Math.round(size / 1024 / 1024), 1);
  const fileSize = KB >= 1024 ? `${MB}MB` : `${KB}KB`;

  return (
    <div className={classes.wrapper}>
      {error && <ServerErrorDialog err={error} />}
      <div className={classes.metaWrapper}>
        <Icon sx={{ color: theme.palette.primary.dark }}>
          {iconNames.attachment}
        </Icon>
        <div className={classes.name}>
          {handlePreviewAction ? (
            <Button
              name="handleAttachementPreview"
              variant="text"
              action={handlePreviewAction}
              sx={{
                color: theme.palette.primary.main,
                textDecoration: 'underline',
                '&:hover': {
                  textDecoration: 'underline',
                },
              }}
            >
              {name}
            </Button>
          ) : (
            <Fragment>{name}</Fragment>
          )}
        </div>
        <div>{fileSize}</div>
      </div>
      <ActionMenu actions={actions} scope="attachment" />
    </div>
  );
};

export default Attachment;
