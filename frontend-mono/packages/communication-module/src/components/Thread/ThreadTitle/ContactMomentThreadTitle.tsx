// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { ContactMomentMessageType } from '../../../types/Message.types';
import ThreadTitle, { ThreadTitlePropsType } from './ThreadTitle';

type ContactMomentThreadTitlePropsType = {
  message: ContactMomentMessageType;
} & Pick<ThreadTitlePropsType, 'context' | 'showLinkToCase'>;

const ContactMomentThreadTitle: React.FunctionComponent<
  ContactMomentThreadTitlePropsType
> = ({ message: { channel }, ...rest }) => {
  const [t] = useTranslation('communication');
  const title = t('thread.contactMoment.title', {
    channel: t(`channels.${channel}`),
  });

  return <ThreadTitle {...rest} title={title} />;
};

export default ContactMomentThreadTitle;
