// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { NestedActionType } from '@mintlab/ui/App/Zaaksysteem/DropdownMenu/library/DropdownMenuList';
import Typography from '@mui/material/Typography';
import Tag from '../../shared/Tag/Tag';
import TopLink from '../../shared/TopLink/TopLink';
import ActionMenu from '../../shared/ActionMenu/ActionMenu';
import { CommunicationContextType } from '../../../types/Context.types';
import { useThreadTitleStyle } from './ThreadTitle.style';

export type ThreadTitlePropsType = {
  title: string;
  context: CommunicationContextType;
  caseNumber?: number;
  showLinkToCase?: boolean;
  actionButtons?: NestedActionType;
};

/* eslint complexity: [2, 8] */
const ThreadTitle: React.FunctionComponent<ThreadTitlePropsType> = ({
  title,
  context,
  caseNumber,
  showLinkToCase,
  actionButtons = [],
}) => {
  const classes = useThreadTitleStyle();
  const [t] = useTranslation('communication');

  return (
    <div className={classes.wrapper}>
      <Typography variant="h4" classes={{ root: classes.title }}>
        {title}
      </Typography>
      {showLinkToCase && caseNumber && context && context.context && (
        <Tag style={classes.tag}>
          <TopLink
            style={classes.link}
            href={`/${
              context.context === 'pip' ? 'pip' : 'intern'
            }/zaak/${caseNumber}`}
          >
            {`${t('thread.tags.case')} ${caseNumber}`}
          </TopLink>
        </Tag>
      )}
      {Boolean(actionButtons.length) && (
        <ActionMenu actions={actionButtons} scope={'thread:message:header'} />
      )}
    </div>
  );
};

export default ThreadTitle;
