// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import Communication from './Communication';
import locale from './Communication.locale';
import { CommunicationContextType } from './types/Context.types';
import { getSubjectRelations } from './Communication.library';
import { CommunicationContext } from './Communication.context';
import AddThreadToCaseDialog from './components/Dialogs/AddThreadToCaseDialog';
import ImportMessageDialog from './components/Dialogs/ImportMessageDialog';

export type CommunicationModulePropsType = Omit<
  CommunicationContextType,
  'setAddThreadToCaseDialogOpened' | 'setImportMessageDialogOpened'
>;

type CommunicationParamsType = {
  contactUuid?: string;
};

/* eslint complexity: [2, 7] */
const CommunicationModule = (options: CommunicationModulePropsType) => {
  const [t] = useTranslation('communication');
  const [, addMessages, removeMessages] = useMessages();
  const [addThreadToCaseDialogOpened, setAddThreadToCaseDialogOpened] =
    useState<string>();
  const [importMessageDialogOpened, setImportMessageDialogOpened] =
    useState<boolean>(false);
  const { contactUuid } = useParams() as CommunicationParamsType;

  useEffect(() => {
    const messages: { [key: string]: any } = {
      ...t('serverErrors', {
        returnObjects: true,
      }),
      ...t('snackMessages', {
        returnObjects: true,
      }),
    };

    addMessages(messages);

    return () => removeMessages(messages);
  }, []);

  const { context, caseUuid } = options;
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const { data: subjectRelations, isLoading } = useQuery(
    ['subjectRelations'],
    () => (context === 'case' && caseUuid ? getSubjectRelations(caseUuid) : []),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  const optionsWithParams = {
    ...options,
    subjectRelations,
    setAddThreadToCaseDialogOpened,
    setImportMessageDialogOpened,
    contactUuid: contactUuid || options.contactUuid || '',
  };

  return (
    <I18nResourceBundle resource={locale} namespace="communication">
      <CommunicationContext.Provider value={optionsWithParams}>
        {isLoading ? <Loader /> : <Communication />}
        {addThreadToCaseDialogOpened && (
          <AddThreadToCaseDialog
            threadUuid={addThreadToCaseDialogOpened}
            onClose={() => setAddThreadToCaseDialogOpened(undefined)}
          />
        )}
        {importMessageDialogOpened && (
          <ImportMessageDialog
            onClose={() => setImportMessageDialogOpened(false)}
            caseUuid={options.caseUuid || ''}
          />
        )}
      </CommunicationContext.Provider>
      {ServerErrorDialog}
    </I18nResourceBundle>
  );
};

export default CommunicationModule;
