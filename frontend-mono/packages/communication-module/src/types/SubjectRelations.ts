// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

export type SubjectRelationType = {
  uuid: string;
  value: string;
  label: string;
  role: string;
  isPresetClient: boolean;
  authorized: boolean;
};

export type SubjectRelationsRequestParams =
  APICaseManagement.GetSubjectRelationsRequestParams;
export type SubjectRelationsResponseBody =
  APICaseManagement.GetSubjectRelationsResponseBody;

export type FetchSubjectRelationsType = (
  caseUuid: string
) => Promise<SubjectRelationsResponseBody>;

export type GetSubjectRelationsType = (
  caseUuid: string
) => Promise<SubjectRelationType[]>;

export type FormatSubjectRelationType = (
  subject: SubjectRelationsResponseBody['data'][0],
  subjects: SubjectRelationsResponseBody['included']
) => SubjectRelationType;
