// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const publicApi = require('.');

test('📦 @mintlab/kitchen-sink', assert => {
  assert.equal(typeof publicApi.asArray, 'function', 'has a `asArray` method');
  assert.equal(typeof publicApi.bind, 'function', 'has a `bind` method');
  assert.equal(
    typeof publicApi.buildParamString,
    'function',
    'has an `buildParamString` method'
  );
  assert.equal(
    typeof publicApi.buildUrl,
    'function',
    'has an `buildUrl` method'
  );
  assert.equal(
    typeof publicApi.cloneWithout,
    'function',
    'has a `cloneWithout` method'
  );
  assert.equal(
    typeof publicApi.dictionary,
    'function',
    'has a `dictionary` method'
  );
  assert.equal(typeof publicApi.expose, 'function', 'has an `expose` method');
  assert.equal(typeof publicApi.extract, 'function', 'has an `extract` method');
  assert.equal(
    typeof publicApi.isObject,
    'function',
    'has an `isObject` method'
  );
  assert.equal(
    typeof publicApi.isPopulatedArray,
    'function',
    'has an `isPopulatedArray` method'
  );
  assert.equal(
    typeof publicApi.filterProperties,
    'function',
    'has an `filterProperties` method'
  );
  assert.equal(
    typeof publicApi.filterPropertiesOnValues,
    'function',
    'has an `filterPropertiesOnValues` method'
  );
  assert.equal(typeof publicApi.get, 'function', 'has a `get` method');
  assert.equal(
    typeof publicApi.getObject,
    'function',
    'has a `getObject` method'
  );
  assert.equal(
    typeof publicApi.getSegment,
    'function',
    'has an `getSegment` method'
  );
  assert.equal(
    typeof publicApi.objectifyParams,
    'function',
    'has an `objectifyParams` method'
  );
  assert.equal(
    typeof publicApi.performGetOnProperties,
    'function',
    'has a `performGetOnProperties` method'
  );
  assert.equal(typeof publicApi.purge, 'function', 'has a `purge` method');
  assert.equal(
    typeof publicApi.reduceMap,
    'function',
    'has a `reduceMap` method'
  );
  assert.equal(
    typeof publicApi.removeFromArray,
    'function',
    'has a `removeFromArray` method'
  );
  assert.equal(
    typeof publicApi.toggleItem,
    'function',
    'has a `toggleItem` method'
  );
  assert.equal(typeof publicApi.unique, 'function', 'has a `unique` method');
  assert.end();
});
